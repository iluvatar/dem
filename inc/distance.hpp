#ifndef __DISTANCE_DEM__
#define __DISTANCE_DEM__

#include <iostream>
#include <cmath>
#include <cstdlib>
#include "particle.hpp"

struct DistanceToSphere {
public: // function
  DistanceToSphere() { periodic_[0] = periodic_[1] = periodic_[2] = 0; L_[0] = L_[1] = L_[2] = 0; };

  void set_periodic(const bool periodic[3], const double & Lx, const double & Ly, const double & Lz) { periodic_[0] = periodic[0]; periodic_[1] = periodic[1]; periodic_[2] = periodic[2]; L_[0] = Lx; L_[1] = Ly; L_[2] = Lz; }

  void operator()(Particle & grainid, Particle & grainjd, double & delta, Vec3d_t & Normal) {
    delta = -1.0; // negative delta means no penetration
    Normal = Vecnull;
    Vec3d_t Rij(grainid.R - grainjd.R);
    // Periodic Boundary, see Allen-Tildesley page 30 
    for (int dir = 0; dir <= 2; ++dir) {
      if (true == periodic_[dir]) { 
	double change = L_[dir]*lrint(Rij[dir]/L_[dir]); 
	Rij[dir] -= change;
      } 
    }
    double sumradii = grainid.rad + grainjd.rad;
    double dist = Rij.squaredNorm(); // equal to dist*dist !
    if(dist < sumradii*sumradii) {
      dist = sqrt(dist);
      delta = sumradii - dist;
      Normal = Rij/dist;
    }
  }
  
private: // data
  bool periodic_[3] = {false, false, false};
  double L_[3] = {0};
}; 

struct DistanceToWall {
public: // function
  void operator()(const Particle & grainid, const Particle & grainjd, double & delta, const Vec3d_t & Normal) {
    //Vec3d_t Rij((Normal.dot(grainid.R - grainjd.R))*Normal);
    //delta = grainid.rad - Rij.dot(Normal); // penetration
    delta = grainid.rad - Normal.dot(grainid.R - grainjd.R); // penetration
  }
}; 



#endif // __DISTANCE_DEM__
