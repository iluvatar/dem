#ifndef __INTERACTION_HH__
#define __INTERACTION_HH__

#include "contact.hpp"
#include "workspace_dem_config.hpp"
#include "cell_list.hpp"
#include "utility.hpp"
#include "distance.hpp"
#include "timer.hpp"
#include <iostream>
#include <vector>
#include <iterator>
#include <utility>
#include <map>
#include <unordered_map>
#include <omp.h>

#define NEW_CONTACTS 0

namespace DEM {
  
  //---------------------------------------------------------------
  // class Interaction : for handling contacts interactions
  //---------------------------------------------------------------
  class Interaction {
  public:  // typedefs
#if NEW_CONTACTS
    typedef std::vector<std::unordered_map<int, Contact> > Interaction_t; 
#else 
#ifdef DEBUG 
    typedef std::map<long, Contact> Interaction_t; 
#else 
    typedef std::unordered_map<long, Contact> Interaction_t; 
#endif
#endif
  public: // data
    Interaction_t contacts_;
    DistanceToSphere distance_ss_;
    DistanceToWall distance_sw_;
    
  public : // methods
    void start(const Materials & materials, const int & nbodies) { 
      materials_ = materials;
      (void) nbodies; // avoid unused warning
#if NEW_CONTACTS
      contacts_.resize(nbodies); 
#endif
    }
    void compute_collisions(CellList & cell_list, std::vector<Particle> & particle, const double & dt, const int nlistmode);                   //< nlistmode: 0 = n^2 square algorithm, 1 = celllist, 2 = neighbor list (default) 
    void pre_reset(void);
    void particle_particle_activation(CellList & cell_list, std::vector<Particle> & particle, const int nlistmode);                   //< nlistmode: 0 = n^2 square algorithm, 1 = celllist, 2 = neighbor list (default)                 
    void particle_wall_activation(std::vector<Particle> & particle); 
    void init_pointers(const std::vector<Particle> & particle);
    bool check_forces(const std::vector<Particle> & particle);
    // util
    void max_mean_penetration(double & max, double & mean);
    int  size(void);
    int  full_size(void);
    int  ncontacts(void);
    void clear(void);
    void clear_non_active(void);
    void sum_force_torque(double & sumFc, double & sumTc); // returns the sum of forces and torques on contacts 
    bool check_small_penetrations(const double & l);
    int  nsliding(const std::vector<Particle> & particle, const int & ncmin);
    void reset_residual_flag(const double & time0);
    // i/o functions
    void print(const std::string & filename);
    void read( const std::string & filename);
    int memory_footprint(void) {
      return sizeof(Interaction) + materials_.memory_footprint() + sizeof(Contact)*full_size();
    }

  private: // data
    Materials materials_;
    static constexpr double EPS_DELTA_ = 1.0e-32; //< Smallest delta accepted
  };
  

} // end of namespace DEM

#endif //  __INTERACTION_HH__
