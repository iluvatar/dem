#ifndef __WORKSPACE_DEM_UTIL__
#define __WORKSPACE_DEM_UTIL__

#include "particle.hpp"
#include "utility.hpp"
#include <vector>

namespace DEM {
  namespace util {
    //-----------------------------------------------------------
    // util functions declarations
    //-----------------------------------------------------------
    double unbalanced_force_torque( const std::vector<Particle> & particle, 
				    const int & ngrains,
				    const double & sumFc, const double & sumTc, 
				    const bool & print_info_screen ) ;
    template <typename List>
    void sum_force_torque_contacts( const List & contacts, double & sumFc, double & sumTc)
    {
      sumFc = sumTc = 0;
      for(typename List::const_iterator it = contacts.begin(); it != contacts.end(); ++it) {
	if (it->delta() > 0) {
	  sumFc += it->F().norm();
	  sumTc += it->T().norm();
	}
      }
    }

  } // end namesapce util
} // end namespace DEM

#endif // __WORKSPACE_DEM_UTIL__

