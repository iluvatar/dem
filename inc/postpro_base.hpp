/** This file implements base routines to perform post-processing on the simulation data */

#ifndef __POSTPRO_BASE__
#define __POSTPRO_BASE__


#include "common_config.hpp"
#include "interaction.hpp"
#include "particle.hpp"
#include <iostream>
#include <set>
#include <fstream>
#include <vector>
#include <algorithm>
#include <iterator>
#include <functional>
#include "yaml-cpp/yaml.h"

namespace DEM {
  
  class Postpro {
  public: // methods
    Postpro(); // default constructor, reads config
    void read_config(void); 

  protected: // data
    // read from config files
    int dim_ = 0;
    int stride_ = 1; //< Process every stride iteration
    bool periodic_[3] = {false, false, false};
    double SCREEN_FACTOR_ = 0.0, NC_MIN_ = 0, DL_ = 0.0;
    double time = 0, max_rad_ = 0, min_rad_ = 0, tmin_ = 0, tmax_ = 0, min_vol_ = 0, max_vol_ = 0, d_ = 0, d3_ = 0;
    Vec3d_t G_;
    double PROJECTION_EPS_ = 0.0; // A vector is accepted as in a plane only if N*V/norm(V) < PROJECTION_EPS_, where N is the plane normal
    double VMIN_THRESHOLD_ = 0.98; // A cell volume is accepted only when larger than VMIN_THRESHOLD*Vmin 

    // other data
    int ngrains_ = 0;
    double wmin_[3] = {0};
    double wmax_[3] = {0};
    std::vector<std::string> contactFilenames, bodiesFilenames; 
    std::vector<Particle> particle;       //< current info of particles
    std::vector<Contact> contacts;        //< current info of contacts
    std::vector<Particle> particle_init_; //< first info of particles, for relative computations, stored at the beginning
    std::vector<Contact> contacts_init_;  //< first info of contacts, for relative computations, stored at the beginning
    std::vector<Particle> particle_end_;  //< last info of particles, for relative computations, stored at the beginning
    std::vector<Contact> contacts_end_;   //< last info of contacts, for relative computations, stored at the beginning
    std::set<int> grain_ids_, filtered_grain_ids_, filtered_by_volume_ids_, filtered_by_nc_ids_;
    std::vector<std::reference_wrapper<Contact> > inside_contacts_refs_;
 
  protected: // methods
    // utility functions
    double get_time(const std::string & name, const std::string & moreToRemove);
    void file_names(const std::string & pattern, std::vector<std::string> & filenames);
    void read_contacts(const std::string & filename);
    void read_particles(const std::string & filename);
    void filter_inside_particles_id(const double & screen_lenght);
    void filter_inside_contacts(const double & screen_length);
    void filter_particles_nc_ge(const int nc_ref);
    Vec3d_t location(const Contact & contact);
  };
  
  
} // namespace DEM

#endif // __POSTPRO_BASE__
