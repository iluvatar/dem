#ifndef __BOUNDARY_FIX_HPP__
#define __BOUNDARY_FIX_HPP__
/**
   Allows to handle fixes for velocity and stress for walls
   NOTE: If you want to extend this to handle also particles, generalize 
   the indexes stuff. Currently, this class uses only walls indexes 
   starting from 0 up to DEM::NWALLS.
*/

#include "utility.hpp"
#include "file_util.hpp"
#include "functions.hpp"
#include "workspace_dem_config.hpp"
#include <vector>

namespace DEM {
  namespace Fixes {

    
    //------------------------------------
    // Boundary fix class 
    //------------------------------------
    class BoundaryFix {
    public : // functions
      BoundaryFix() {
	fout_.open("OUTPUT/bcfixes.dat");
	fout_.print("# time id dir fix_mode Fix_function current_value id fix_mode Fix_function current_value ..."); 
      };
      /** Sets a boundary fix of a given type on a given direction**/
      void set_boundary_fix(const int & id, const int & dir, const int & fix_mode, 
			    const int & fix_function_code, const std::vector<double> & params);
      /** Returns the fixed value if there is some fix, status is true ir false**/
      double get_fixed_value(const int & id, const int & dir, const int & iter, const double refval = 0);
      /** True if the id is velocity fixed **/
      bool is_velocity_controlled(const int & id, const int & dir) ;
      /** True if the body id is velocity controlled but the value  depends on stress **/
      bool is_velocity_stress_controlled(const int & id, const int & dir) ;
      /** True if the id is stress fixed **/
      bool is_stress_controlled(const int & id, const int & dir) ;
      /** True if controlled in any way **/
      bool is_controlled(const int & id, const int & dir);
      /** Checks if all ramps are finished **/
      bool ramps_done(const int & iter);
      /** Prints current fix value **/
      void print_info(const int & iter, const double & time); 

    public : // data
      enum FIX_TYPE {FREE=0, VEL, SIGMA, SIGMA_VEL, TOTAL_FIXES};
      enum FIX_FUNCTION {RAMP=0, HAR, STAIR, DYN_ALTER, TOTAL_FUNCTION};
      
    private : // data
      int fix_mode_[DEM::NWALLS][3] = {{0}};             //< free by default
      int fix_function_[DEM::NWALLS][3] = {{0}};         //< ramp by default
      std::vector<double> params_[DEM::NWALLS][3];	 //< params for the fix function on each direction
      // stair helper variables
      StairFunctor stairs_[DEM::NWALLS][3];              //< Functors to manage stairs
      UTIL::OFileStream fout_;
    };

  } // namespace Fixes
} // namespace DEM


#endif //__BOUNDARY_FIX_HPP__
