#ifndef __FILE_UTIL_HPP__
#define __FILE_UTIL_HPP__

/**
   Defines a struct to help handling all file streams with the same format
*/

#include <fstream>
#include "utility.hpp"

namespace UTIL {

  class OFileStream {
  public  : // methods
    OFileStream()  {};
    OFileStream(const std::string & name, const bool & append = false)  { open(name, append); };
    OFileStream(const OFileStream & other)  { (void)(other);};
    ~OFileStream() { /*if (fout_.is_open()) fout_.close();*/ };
    
    int open(const std::string & name, const bool & append = false);  //< opens the ofstream with filename = name
    int close(void);                                      //< closes the ofstream 
    int print_newline(void);                              //< prints a new line 
    template <typename T> int print_val (const T & val);  //< prints the value 
    template <typename T> int print(const T & val);       //< prints the value and new line character 
    template <typename U, typename... T> int print_val( const U & firstval, const T & ... vals); //< variadic generalizations
    template <typename U, typename... T> int print(const U & firstval, const T & ... vals);      //< variadic generalization
    
  private : // data
    std::ofstream fout_;
  };

  //---------------- IMPLEMENTATION --------------------------    
  template <typename T> 
  int 
  OFileStream::print_val(const T & val) {
    ASSERT(fout_.is_open()); 
    fout_ << std::setw(28) << val; 
    return EXIT_SUCCESS;
  }
  
  template <typename T> 
  int 
  OFileStream::print(const T & val) {
    print_val(val); 
    print_newline();
    return EXIT_SUCCESS;    
  }

  template <typename U, typename... T> 
  int 
  OFileStream::print_val(const U & firstval, const T & ... vals) {
    print_val(firstval);
    print_val(vals...);
    return EXIT_SUCCESS;
  }

  template <typename U, typename... T> 
  int 
  OFileStream::print(const U & firstval, const T & ... vals) {
    print_val(firstval, vals...);
    print_newline();
    return EXIT_SUCCESS;
  }
    
} // namespace UTIL

#endif // __FILE_UTIL_HPP__
