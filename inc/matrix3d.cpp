#include "matrix3d.hpp"

void eigen_values(const Mat3d_t & matrix, double & l0, double & l1, double & l2, const bool & abs)
{
  l0 = l1 = l2 = 0; 
  if (!matrix.isZero()) {
    Eigen::EigenSolver<Mat3d_t> solver(matrix, true); // argument #2 : true/false == yes/no compute eigenvec
    Vec3d_t evals(solver.eigenvalues().real()); 
    // detect latest eigen value by comparing the largest z component
    // among all eigen vectors
    int index2 = 0;
    double max = std::fabs(solver.eigenvectors().normalized().col(0).real()[2]);
    for (int ii = 1; ii < 3; ++ii) {
      double val = solver.eigenvectors().normalized().col(ii).real()[2];
      if (std::fabs(val) > max ) { // 10% tolerance
	max = std::fabs(val);
	index2 = ii;
      }
    }
    double tmp_vec[3] = {0.0};
    if (0 == index2) {
      tmp_vec[0] = evals[1]; tmp_vec[1] = evals[2];  tmp_vec[2] = evals[0]; 
    } else if (1 == index2) { 
      tmp_vec[0] = evals[0]; tmp_vec[1] = evals[2];  tmp_vec[2] = evals[1]; 
    } else {
      tmp_vec[0] = evals[0]; tmp_vec[1] = evals[1];  tmp_vec[2] = evals[2]; 
    }
    if (true == abs) for (auto & val : tmp_vec) val = std::fabs(val);
    //std::sort(tmp_vec, tmp_vec + 3); // sort the eigen values
    l0 = tmp_vec[0];
    l1 = tmp_vec[1];
    l2 = tmp_vec[2];
    //Eigen::Vector3d evec0, evec1, evec2;
    //evec0 = solver.eigenvectors().col(0).normalized().real();
    //evec1 = solver.eigenvectors().col(1).normalized().real();
    //evec2 = solver.eigenvectors().col(2).normalized().real();
    // std::cout << "evec  0 = " << evec0 << std::endl;
    // std::cout << "evec  1 = " << evec1 << std::endl;
    // std::cout << "evec  2 = " << evec2 << std::endl;
    // std::cout << "evals 0 = " << evals[0] << std::endl;
    // std::cout << "evals 1 = " << evals[1] << std::endl;
    // std::cout << "evals 2 = " << evals[2] << std::endl << std::endl;
  }
}

double deviation_from_symmetric(const Mat3d_t & matrix)
{
  if (!matrix.isZero()) return (matrix - matrix.transpose()).norm()/matrix.norm();
  else return 0;
}
