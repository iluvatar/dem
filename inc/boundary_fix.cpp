#include "boundary_fix.hpp"

namespace DEM {
  namespace Fixes {

    void BoundaryFix::set_boundary_fix(const int & id, const int & dir, const int & fix_mode, 
				       const int & fix_function_code, const std::vector<double> & params)
    {
      // print some info
      print_log("Setting boundary fix:");
      print_log("id           = ", id);
      print_log("dir          = ", dir);
      print_log("fix_mode     = ", fix_mode);
      print_log("fix_function = ", fix_function_code);
      print_log("nparams      = ", params.size());
      for (unsigned int ii = 0; ii < params.size(); ++ii) {
	std::clog << "####  Param[" << ii << "] = " << params[ii] << std::endl;
      }
      std::clog << std::endl;

      // check input
      ASSERT(0 <= id && id < DEM::NWALLS);
      ASSERT(0 <= dir && dir < 3);
      ASSERT(VEL <= fix_mode && fix_mode < TOTAL_FIXES);
      ASSERT(0 <= fix_function_code && fix_function_code < TOTAL_FUNCTION);
      switch (fix_function_code) {
      case RAMP:
	ASSERT(3 == params.size());
	ASSERT(0 <= params[2]);            //< 0 <= nramp
	break;
      case HAR:
	ASSERT(4 == params.size());
	ASSERT(0 <= params[1]);            //< 0 <= omega
	break;
      case STAIR:
	// checked at the stair functor
	break;
      case DYN_ALTER:                     //< params = {V, qmin, qmax}
	ASSERT(3 == params.size());
	ASSERT(params[1] < params[2]);            //< qmin < qmax
	break;
      }
      
      // set info
      fix_mode_[id][dir] = fix_mode;
      fix_function_[id][dir] = fix_function_code;
      params_[id][dir].resize(params.size());
      std::copy(params.begin(), params.end(), params_[id][dir].begin());
      // stair ramp info
      if (STAIR == fix_function_code) {
	stairs_[id][dir].start(params_[id][dir]);
      }
    }

    double BoundaryFix::get_fixed_value(const int & id, const int & dir, const int & iter, const double refval)
    {
      // check input
      ASSERT(0 <= id && id < DEM::NWALLS);
      ASSERT(0 <= dir && dir < 3);
      ASSERT(0 <= iter);

      // return the fixed value according to the fix type
      if (FIX_TYPE::FREE != fix_mode_[id][dir]) {
	switch (fix_function_[id][dir]) {
	case RAMP:
	  return (iter < params_[id][dir][2]) ? (1.0*(params_[id][dir][1] - params_[id][dir][0])*iter/params_[id][dir][2]) : (params_[id][dir][1]);
	  break;
	case HAR:
	  return params_[id][dir][3] + params_[id][dir][0]*std::sin(params_[id][dir][1]*iter + params_[id][dir][2]);
	  break;
	case STAIR:
	  return stairs_[id][dir](iter);
	  break;
	case DYN_ALTER:
	  if (refval >= params_[id][dir][2]) params_[id][dir][0] = +fabs(params_[id][dir][0]);
	  if (refval <= params_[id][dir][1]) params_[id][dir][0] = -fabs(params_[id][dir][0]);
	  return params_[id][dir][0];
	  break;
	}
      }
      return 0.0; // WARNING : Why 0?
    }

    bool BoundaryFix::ramps_done(const int & iter)
    {
      // NO time limit imposed to oscillatory function
      for (int id = 0; id < DEM::NWALLS; ++id) {
	for (int dir = 0; dir < 3; ++dir) {
	  if (fix_mode_[id][dir]) { // 0 != fix_mode
	    switch (fix_function_[id][dir]) {
	    case RAMP:
	      if (iter < params_[id][dir][2]) return false;
	      break;
	    case STAIR:
	      return stairs_[id][dir].ramps_done(iter);
	      break;
	    }
	  }
	}
      }

      return true;
    }

    bool BoundaryFix::is_velocity_controlled(const int & id, const int & dir)
    {
      // check input
      ASSERT(0 <= id && id < DEM::NWALLS);
      ASSERT(0 <= dir && dir < 3);
      
      return (FIX_TYPE::VEL == fix_mode_[id][dir]);
    }

    bool BoundaryFix::is_velocity_stress_controlled(const int & id, const int & dir)
    {
      // check input
      ASSERT(0 <= id && id < DEM::NWALLS);
      ASSERT(0 <= dir && dir < 3);

      return (FIX_TYPE::SIGMA_VEL == fix_mode_[id][dir]);
    }

    bool BoundaryFix::is_stress_controlled(const int & id, const int & dir)
    {
      // check input
      ASSERT(0 <= id && id < DEM::NWALLS);
      ASSERT(0 <= dir && dir < 3);
      
      return (FIX_TYPE::SIGMA == fix_mode_[id][dir]);
    }

    bool BoundaryFix::is_controlled(const int & id, const int & dir)
    {
      ASSERT(0 <= id && id < DEM::NWALLS);
      ASSERT(0 <= dir && dir < 3);
      return (is_velocity_controlled(id, dir) || is_stress_controlled(id, dir));
    }

    void BoundaryFix::print_info(const int & iter, const double & time)
    {
      fout_.print_val(time);
      for (int id = 0; id < DEM::NWALLS; ++id) {
	for (int dir = 0; dir <= 2; ++dir) {
	  if (is_controlled(id, dir)) {
	    fout_.print_val(id, dir, fix_mode_[id][dir], fix_function_[id][dir], get_fixed_value(id, dir, iter));
	  }
	}
      }
      fout_.print_newline();
    }


  } // namespace Fixes
} // namespace DEM

