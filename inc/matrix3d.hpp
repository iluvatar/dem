#pragma once

#include "vector3d.hpp"
#include <eigen3/Eigen/Core>
#include <eigen3/Eigen/Eigenvalues>

typedef Eigen::Matrix<double, 3, 3, Eigen::RowMajor> Mat3d_t; // Eigen Matrix type ROW MAJOR
//typedef Eigen::Matrix<long double, 3, 3, Eigen::RowMajor> Mat3d_t; // Eigen Matrix type ROW MAJOR

void eigen_values(const Mat3d_t & matrix, double & l0, double & l1, double & l2, const bool & abs = false);
double deviation_from_symmetric(const Mat3d_t & matrix);

  
