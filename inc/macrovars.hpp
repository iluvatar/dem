#ifndef __MACROVARS__
#define __MACROVARS__

#include "matrix3d.hpp"
#include "vector3d.hpp"
#include "utility.hpp"
#include "file_util.hpp"
#include "stress_tensor.hpp"
#include <iostream>
#include <fstream>
#include <iomanip>
#include <vector>
#include <cmath>

/**
   Class to compute macroscopic stress and fabric tensors, and related
   quantities
 */

class MacroVars {
public: // data
  Mat3d_t Sigma_;		    //< from internal with F > 0.01*<F>
  Mat3d_t Fabric_;		    //< from internal contacts F > 0.01*<F>
private : // data
  UTIL::OFileStream sigma_fout_, fabric_fout_,
    a_fout_; //< fabric anisotropy

public :  // methods
  MacroVars();
  ~MacroVars();
  void reset(void); 
  void stress_invariants(double & I1, double & I2, double & I3, double & J1, double & J2, double & J3);
  void p_and_q(const int dim, double & p, double & q);
  void print_macrovar(const double & time, const Mat3d_t & M, UTIL::OFileStream & fout);
  void print(const double & time);
  template <class ContactPtrList_t, class ParticleList_t> 
  void process(const ContactPtrList_t & contacts_ptrs, // list of internal contacts
	       const ParticleList_t & particles,       // particles
	       const double & volume) {		       // volume to normalize stress (from internal contacts)
    Sigma_.setZero();
    Fabric_.setZero();
    const auto ncontacts = contacts_ptrs.size();
    if (0 == ncontacts) return;
    // compute mean force, to use as threshold
    double mean_f = 0;
    for (const auto & cptr : contacts_ptrs) {
      mean_f += cptr.get().F().norm(); 
    }
    mean_f /= ncontacts;
    // add to stress tensor
    for (const auto & cptr : contacts_ptrs) {
      if (cptr.get().F().norm() < 0.01*mean_f) continue; // ignore contacts with too small force
      Vec3d_t Branch = cptr.get().Normal()*(particles[cptr.get().uid1()].rad + 
					    particles[cptr.get().uid2()].rad - 
					    cptr.get().delta()); // wall.rad == 0!
      Sigma_ += (cptr.get().F())*Branch.transpose();			   
      Fabric_ += (cptr.get().Normal())*cptr.get().Normal().transpose();
    } 
    Sigma_ /= volume;
    Fabric_ /= ncontacts;
  }

}; 


#endif // __MACROVARS__
