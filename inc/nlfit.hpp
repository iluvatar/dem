#ifndef __NLFIT_HPP__
#define __NLFIT_HPP__

/**
   Uses the (unsupported) NonLinearOptimization Eigen module
   to compute a non-linear fit using the LevenbergMarquardt alg.
   Functions to be fitted should follow the form of the typedef.
   The key function to use is : nlfit
*/

#include <unsupported/Eigen/NonLinearOptimization>
#include <iostream>
#include <vector>
#include <algorithm>

namespace NLFIT {
    
  // typedef for the function to be fitted
  typedef double (*Function_t)(const double & x, const double * par);
  
  
  //------------------------------------------------------------------------------------
  // Generic functor
  template<typename _Scalar, int NX=Eigen::Dynamic, int NY=Eigen::Dynamic>
  struct Functor
  {
    typedef _Scalar Scalar;
    enum {
      InputsAtCompileTime = NX,
      ValuesAtCompileTime = NY
    };
    typedef Eigen::Matrix<Scalar,InputsAtCompileTime,1> InputType;
    typedef Eigen::Matrix<Scalar,ValuesAtCompileTime,1> ValueType;
    typedef Eigen::Matrix<Scalar,ValuesAtCompileTime,InputsAtCompileTime> JacobianType;

    int m_inputs, m_values;

    Functor() : m_inputs(InputsAtCompileTime), m_values(ValuesAtCompileTime) {}
    Functor(int inputs, int values) : m_inputs(inputs), m_values(values) {}

    int inputs() const { return m_inputs; }
    int values() const { return m_values; }

    // you should define that in the subclass :
    //  void operator() (const InputType& x, ValueType* v, JacobianType* _j=0) const;
  };

  //------------------------------------------------------------------------------------
  // actual functor to use to fit
  struct lmdif_functor : Functor<double>
  {
    lmdif_functor(void) : Functor<double>(0, 0) {F_ = nullptr;}
    lmdif_functor(const int & npar, const int & ndata) : Functor<double>(npar, ndata) {F_ = nullptr;}
    ~lmdif_functor() {};
    
    int init(const int & npar, const int & ndata, const double *xdata, const double *ydata,
	     Function_t F) {
      m_inputs = npar; m_values = ndata;
      xdata_.resize(ndata, 0);
      ydata_.resize(ndata, 0);
      std::copy(xdata, xdata + ndata, xdata_.begin());
      std::copy(ydata, ydata + ndata, ydata_.begin());
      F_ = F;
      ready_ = true;
     
      return EXIT_SUCCESS;
    }

    int operator()(const Eigen::VectorXd &param, Eigen::VectorXd &fvec) const
    {
      if (!ready_) return EXIT_FAILURE;
      for (int ii = 0; ii < m_values; ii++) {
	fvec[ii] = ydata_[ii] - F_(xdata_[ii], param.data());
      }
      return EXIT_SUCCESS;;
    }

  private :
    bool ready_ = false;
    std::vector<double> xdata_; 
    std::vector<double> ydata_;
    Function_t F_ = nullptr;
  };

  //------------------------------------------------------------------------------------
  // Helper struct derived from eigen vector to allow to fit a subset of the parameters
  // The vector has data : {0, 1, 2, 3, ..., public_size_ - 1, public_size_, ..., actual_size_ - 1}
  // Only parameters in [0, public_size_ - 1] will be plublicy exposed (fitted)
  struct MaskedVector : public Eigen::VectorXd {
    int public_size_ = 0;
    int size() {return public_size_;};
  };

  //------------------------------------------------------------------------------------
  /** 
      @param npar - Number of parameters (to be optimized)
      @param ndata - Number of data on xi or yi
      @param *xdata - Data on x axis
      @param *ydata - Data on y axis
      @param function - Function to be fitted. Its type should follow the pattern of the typedef (see below)
      @param *param - Initial guess for the parameters and where their final values are going to be stored
      @param *param_err - Array to store the errors for the fitted parameters
      @param npartotal - Total number of parameters. Only the first npar are modified, the remaining are kept constant
  */
  int nlfit(const int & npar, const int & ndata, const double *xdata, const double *ydata, 
	    Function_t function, double *param, double *param_err, const int & npartotal);
  
} // namespace NLFIT

#endif // __NLFIT_HPP__
