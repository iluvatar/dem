#ifndef __WORKSPACE_DEM_HELPER__
#define __WORKSPACE_DEM_HELPER__

#include "workspace_dem_config.hpp"
#include "workspace_dem.hpp"
#include "distance.hpp"
#include "netCDF_io.hpp"
#include <numeric>
#include <vector>
#include <algorithm>
#include <iterator>

namespace DEM{
  namespace helper {
    //-----------------------------------------------------------
    // helper functions declarations
    //-----------------------------------------------------------
    void freeze(WorkspaceDEM & dom);
    void reset_R0(WorkspaceDEM & dom);
    void shift_grains_by(const Vec3d_t & Shift);
    int remove_penetrations_by_scaling(WorkspaceDEM & dom);
    int create_cubic_internal_nucleus(const int & TAG0, const int & TAG1, const double & FRACTION);
    int create_right_nucleus(const int & TAG0, const int & TAG1, const double & FRACTION);
    int create_mixed_nucleus(const int & TAG0, const int & TAG1, const double & FRACTION);

  } // namespace : helper
} // namespace : DEM

#endif // _DEM_HELPER__
