#ifndef __MATERIALS_DEM__
#define __MATERIALS_DEM__

/*
  This module is intended to describe material properties by means of tags. 
  Composite material properties are automatically computed at creation of each tag
 */

#include "file_util.hpp"
#include "particle.hpp"
#include <iostream>
#include <vector>
#include <string>
#include "yaml-cpp/yaml.h" // for parsing yaml files

namespace DEM { 
  enum MatVar {RHO = 0, K, E, GAMMA, KTF, GAMMATF, MUSF, MUKF, KTR, GAMMATR, MUSR, MUKR, KTO, GAMMATO, MUSO, MUKO, MASS, STRESSC, OMEGAC, TOTALMatVar};

  class Materials {
  public:
    //-------------------------
    // types
    typedef std::vector<double> BasicMat_t; 
    //-------------------------
    // methods
    Materials(); // constructor
    void read_config(void);
    void start(const std::vector<Particle> & particle, const std::string & outputfn);
    const BasicMat_t & operator()(int idtag, int jdtag) const { return compMaterial[idtag][jdtag]; }
    const BasicMat_t & operator()(int idtag, int jdtag, const bool & residual) const { 
      if (false == residual)  
	return compMaterial[idtag][jdtag]; 
      else // (true == residual)  
	return compMaterial[residual_tag_[idtag]][residual_tag_[jdtag]]; 
    }
    double operator()(int idtag, int jdtag, MatVar name) { return compMaterial[idtag][jdtag][name]; };
    double operator()(int idtag, MatVar name) { return compMaterial[idtag][idtag][name]; };
    double get_rho(int idtag) {ASSERT(0 <= idtag && idtag < nmaterials_); return rho_[idtag];}
    bool check_existence(const int idtag) { return (0 <= idtag && idtag < nmaterials_); }
    void print_materials_info(const std::string & output);
    int memory_footprint(void) {
      return sizeof(Materials) + sizeof(MatVar)*TOTALMatVar + sizeof(Mat_t)*material.size() + nmaterials_*sizeof(CompMat_t)*compMaterial.size() 
	+ sizeof(int)*tag_.size() + sizeof(double)*(6*rho_.size() + en_table_.size());
    }
    
  private:
    //-------------------------
    // types
    typedef std::vector< std::vector<double> > Mat_t;
    typedef std::vector< std::vector<std::vector<double> > > CompMat_t;
    //-------------------------
    // data
    int nmaterials_ = 0;
    Mat_t material; // stores each material by idtag, name, value, access: material[idtag][NAME] =  value
    CompMat_t compMaterial; // composite materials among idtag and jdtag (pair), access: compMaterial[id][jd][NAME] = value 
    //Mat_t::iterator it;
    std::vector<int> tag_;
    std::vector<double> rho_, k_, e_, musf_, musr_, mass_;
    std::vector<int> residual_tag_;
    // cohesion
    std::vector<double> stressc_, omegac_;

    // for normal restitution
    std::vector<double> en_table_; // table to comupte the inverse
    double deltaen_ = 0;
    //-------------------------
    // methods
    //    void create_material(int idtag, double rho, double k, double e, double mass, double musf, double musr);
    void create_material(int idtag, double rho, double k, double e, double mass, double musf, double musr, double stressc, double omegac);
    void compute_comp_material(int idtag, int jdtag);
    template <typename T> void set_additional_mat_vars(T & mat); 
    void create_composite_materials(void);
    double get_x_for_en(const double & en);
  };
    
} // namespace DEM 

#endif
