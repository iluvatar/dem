#include "colors.inc"
#include "textures.inc"
#include "stones2.inc"


/*
       y
       |    z   
       |   /   
       |  / 
       | /
       |------- x
*/



// -----------------------------------------------------------------------
// camera
camera {
  //location <(XF-X0)/3, 1.2*(YF-Y0), -8*(ZF-Z0)>    // position <x, y(height), z>
  location <CAM_POS_X, CAM_POS_Y, CAM_POS_Z>    // position <x, y(height), z>
  //look_at <0, 0, 0>    // view at origin
  look_at <X0 + 2.0*(XF-X0)/3.0, Y0 + 0.9*(YF-Y0), Z0 + 0.5*(ZF-Z0)>    // view at center
  //direction 1.5*z
  // right     x*image_width/image_height
  //rotate <0,20,0>
  //rotate <85,0,0>
 //sky <0,0,1>
  angle 85    // vision field
}

// -----------------------------------------------------------------------
// lights
//light_source { <(XF-X0)/2, 1.4*(YF-Y0), -4*(ZF-Z0)> color White*1.0 shadowless parallel  }
light_source { <CAM_POS_X, CAM_POS_Y, CAM_POS_Z> color White*2.0 shadowless parallel  }
//light_source { <-10*(XF-X0)/2, 1.4*(YF-Y0), 10> color White*1.1 shadowless }
background {Black}
global_settings { 
  ambient_light White 
  assumed_gamma 1.0
}

// -----------------------------------------------------------------------
/*
sky_sphere {
  pigment {
    gradient y
    color_map {
      [0.0 rgb <0.6,0.7,1.0>]
      [0.7 rgb <0.0,0.1,0.8>]
    }
  }
}
*/

/*
sky_sphere {
  pigment { Blue_Sky }
  //pigment { Blood_Sky }
  //pigment { Clouds }
}
*/

// -----------------------------------------------------------------------
/*
// floor 
plane {
  y,-0.01
  //pigment { color rgb <0.7,0.5,0.3> }
  //pigment { gradient z color_map { [0.0 color Red] [1.0 color Yellow] } }
  pigment { color rgb <246/255,252/255,198/255> }
  //pigment { color Cyan*0.7 }
  //pigment { color Blue*0.9 }
  //pigment { color Orange*1.2 }
  //pigment { color White*1.8 }
  no_shadow
}
*/


// -----------------------------------------------------------------------

#declare Pigm = pigment {
   // //color rgb<112/255,52/255,0/255>
  // //color rgb<252/255,128/255,128/255>
  //color rgbf <1,0.5,0.2,0.1>
  //transmit 0.3
  /*
  marble
  turbulence 0.5 
  color_map {
    [0.0 color Gray90] // 90% gray
    [0.8 color Gray60] // 60% gray
    [1.0 color Gray20] // 20% gray
    //[0.0 color Yellow] 
    //[0.5 color rgb<120/255,60/255,0/255>] 
    //[1.0 color rgb<104/255,44/255,0/255>] 
  }
  */
  /*
  gradient <0, 1, 0>
  turbulence 0.0
    color_map {
      //[0.3 color Red]
      //[0.6 color Blue]
      //[1.0 color Green]
      [0.5 color Red]
      [1.0 color Yellow]
  }
  */
   checker
   color rgb 1, color rgb 0
}

#declare Normal = normal {
  bumps 0.4  // controls depth of bumps
  scale .05  // controls width of bumps
} 

#declare Texture = texture {
  //Ruby_Glass
  //Red_Marble
  //Blue_Agate
  //T_Stone2
  //T_Stone25
  //Brown_Agate scale 0.85
  //Aluminum
  //Brass_Metal
  //Brass_Valley scale 0.25
  pigment { Pigm }
  //normal { Normal }
}


#declare MySphere = 
  sphere {	
    <0,0,0>, 1.0
    texture { Texture }  
    //finish { phong 0.9 } 
    no_shadow
  }

#declare MyCube =
  box {
    <X0, Y0, Z0>,<XF, YF, ZF>
    texture {
      pigment {
	//color Green
	//color Blue
	color Red
	transmit 0.90
      }
      //finish { phong 0.9 } 
      finish { reflection 0.0 } 
    }
    // give object no shadow
    no_shadow
  }

/*
#declare MyCylinder =
  cylinder {
    <CYL_X, Y0, CYL_Z> ,
    <CYL_X, YF, CYL_Z> ,
    CYL_RAD
    texture {
      pigment {
        Cyan
        transmit 0.82
      }
    }
  }
*/

///*
// -----------------------------------------------------------------------
// the coordinate system
#declare SCALE_FACTOR=0.2*(YF-Y0);
#declare small_rad = 0.05*SCALE_FACTOR;
cylinder {
  <0,0,0,> , 
  x*SCALE_FACTOR , 
  small_rad
  texture {
    pigment {
      Red
      transmit 0.1
    }
  }
}
cylinder {
  <0,0,0,> , 
  y*SCALE_FACTOR , 
  small_rad
  texture {
    pigment {
      Blue
      transmit 0.1
    }
  }
}
cylinder {
  <0,0,0,> , 
  z*SCALE_FACTOR , 
  small_rad
  texture {
    pigment {
      Green
      transmit 0.1
    }
  }
}
//*/
