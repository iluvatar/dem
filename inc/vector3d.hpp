#ifndef __VECTOR3D_SIMPLE_H__
#define __VECTOR3D_SIMPLE_H__

#include "util/assert.hpp"
#include <iostream>
#include <iomanip>
#include <cmath>

#define EIGEN 1

#if EIGEN
#include <eigen3/Eigen/Dense>
typedef Eigen::Vector3d Vec3d_t; // Eigen
//typedef Eigen::Matrix<long double, 3, 1> Vec3d_t;
#endif

#if !(EIGEN) 
class Vector3D; typedef Vector3D Vec3d_t; // my
typedef double REAL;
//---------------------- my class Vector3D --------------------
class Vector3D{
public:
  REAL x, y, z;
public:
  // constructors
  // default constructor
  explicit Vector3D() : x(0), y(0), z(0) {;};
  // with REALs
  explicit Vector3D(const REAL x0, const REAL y0, const REAL z0) : 
    x(x0), y(y0), z(z0) {;};
  // with vectors
  explicit Vector3D(Vector3D & V2) : x(V2.x), y(V2.y), z(V2.z) {;};
  Vector3D(const Vector3D & V2) { x = V2.x; y = V2.y; z = V2.z;};
  
  // get functions
  REAL getX(void) const { return x; };
  REAL getY(void) const { return y; };
  REAL getZ(void) const { return z; };
  
  // set functions
  void normalize(void);
  
  // sobregarga de operators
  // Operaciones vectoriales
  Vector3D & operator=(const REAL num);
  // Producto por escalar
  Vector3D operator*(const REAL a) const;
  friend Vector3D operator*(const REAL a, const Vector3D & vec2);
  // división por escalar
  Vector3D operator/(const REAL a) const;
  // suma y resta
  Vector3D operator+(const Vector3D & vec2) const;
  Vector3D operator-(const Vector3D & vec2) const;
  // Producto punto
  REAL dot(const Vector3D & vec2) const;
  // Producto cruz
  Vector3D cross(const Vector3D & vec2) const;
  // con asignación
  Vector3D & operator*=(const REAL a);
  Vector3D & operator/=(const REAL a);
  Vector3D & operator+=(const Vector3D & vec2);
  inline Vector3D & operator-=(const Vector3D & vec2);
  // subscript, returns l-value
  REAL & operator[](const int index);
  REAL operator[](const int index) const;
  // check if at least one component is greater than value
  bool operator>(const REAL value);
  // check if all comp. are less than value
  bool operator<(const REAL value);
  // check equality
  bool operator==(const Vector3D & rhs) const {
    if (x != rhs.x) return false;
    if (y != rhs.y) return false;
    if (z != rhs.z) return false;
    return true;
  }
  bool operator!=(const Vector3D & rhs) const {
    return !(*this==rhs);
  }

  // utility functions
  REAL norm(void) const;
  REAL squaredNorm(void) const;
  REAL norm2XY(void) const;
  Vector3D normalized(void) const;
  Vector3D signVec(void) const;
  Vector3D xyVec(void) const;
  void print(void);
};
// typedef 
typedef Vector3D Vec3_t;



#endif // if !EIGEN


const Vec3d_t Vec_uz(0,0,1);
const Vec3d_t Vec_uy(0,1,0);
const Vec3d_t Vec_ux(1,0,0);
const Vec3d_t Vec_null(0,0,0);
const Vec3d_t Vecx(1,0,0);
const Vec3d_t Vecy(0,1,0);
const Vec3d_t Vecz(0,0,1);
const Vec3d_t Veci(1,0,0);
const Vec3d_t Vecj(0,1,0);
const Vec3d_t Veck(0,0,1);
const Vec3d_t Vecnull(0,0,0);

// para escribir y leer los vectores
std::ostream & operator<<(std::ostream & output, const Vec3d_t & vec);

std::istream & operator>>(std::istream & is, Vec3d_t & vec) ;


#endif // __VECTOR3D_SIMPLE_H__
