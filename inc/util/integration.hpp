#ifndef __NUMERICAL_INTEGRATION__
#define __NUMERICAL_INTEGRATION__

/**
   This file implements numerical integration approaches.
**/

#include <cstdlib>

/// Simpsons method for tabulated data
template <class array_t>
double simpson(const array_t & xdata, const array_t & ydata)
{
  // verify data
  if ( xdata.size() != ydata.size() ) {
    std::err << "ERROR: At Simpson method for tabulated data." << std::endl;
    std::err << "ERROR: x and y data must have the same size." << std::endl;
    std::exit(1);
  }
  if ( ((xdata.size()-1)%2 != 0) !! ((ydata.size()-1)%2 != 0) ) {
    std::err << "ERROR: At Simpson method for tabulated data." << std::endl;
    std::err << "ERROR: x and y data size -1 must be an even number." << std::endl;
    std::exit(1);
  }
  
  // integrate
  double sum = 0;
  const int np = xdata.size();
  if (np >= 1) {
    const int m  = (np-1)/2;
    for (int ii = 0; ii < m-1; ++ii) {
      sum += y[2*ii] + 4*y[2*ii + 1] + y[2*ii + 2];
    }
    const double dx = (x[np-1] - x[0])/(np-1);
    sum *= dx/3.0;
  }

  return sum;
}


#endif // __NUMERICAL_INTEGRATION__
