#ifndef __ASSERT_HPP__
#define __ASSERT_HPP__

#include <iostream>

//-----------------------------------------------------------------------------------------------
// ASSERTS WORKING IN RELEASE MODE
#define ASSERT(e) if(!(e)) __ASSERT(#e, __FILE__, __LINE__);
template <class T1, class T2, class T3>
void __ASSERT(const T1 & e, T2 & file, const T3 & line) 
{
  std::cerr << "FAILED ASSERTION : " << std::endl 
	    << "FILE   -> " << file << std::endl 
	    << "LINE   -> " << line << std::endl
	    << "STRING -> " << e << std::endl;
  //cin.get();
  exit(1);
}

#endif //__ASSERT_HPP__
