#pragma once

#include "vector3d.hpp"
#include "matrix3d.hpp"
#include "geometry.hpp" // for box volume
#include "particle.hpp"
#include "contact.hpp"

void
get_p_q(const Mat3d_t & tensor, double & p, double & q, const int & mode = 0);

template <class ParticleList_t, class ContactList_t> 
void
stress_tensor(const ParticleList_t & bodies, const ContactList_t & contacts, Mat3d_t & tensor)
{
  tensor.setZero();
  
  for (const auto & cpair : contacts) {
    const auto &  c = cpair.second; 
    if (c.delta() >= 0) {
      Vec3d_t Branch = c.Normal()*(bodies[c.uid1()].rad + bodies[c.uid2()].rad - c.delta()); // NOTE: Assumes wall.rad == 0!
      tensor += (c.F())*Branch.transpose();
    }
  }
  tensor /= box_volume(bodies);  
}

