/** 
    This file implements derived routines to perform post-processing on the simulation data 
*/

#ifndef __POSTPRO_ANALYSIS__
#define __POSTPRO_ANALYSIS__ 1


#include "postpro_base.hpp"
#include "histogram1d.hpp"
#include "macrovars.hpp"
#include "materials.hpp"
#include "random.hpp"
#include "workspace_dem_util.hpp"
// postpro functions
#include "postpro/crystallization.hpp"
#include "postpro/delaunay_tetgen.hpp"
#include "postpro/contacts_delaunay.hpp"
#include "postpro/histograms_force_torque.hpp"
#include "postpro/stress_histograms_grains.hpp"
#include "postpro/homogeinity.hpp"
#include "postpro/statmech_internal_equilibrium.hpp"
#include "postpro/statmech_per_material.hpp"
#include "postpro/statmech_along_x.hpp"
#include "postpro/voronoi_multiscale.hpp"
#include "postpro/voronoi_nparticles.hpp"
#include "postpro/voronoi_z.hpp"
#include "postpro/voronoi-no_floating.hpp"
#include "postpro/centroidal_voronoi_tessellation.hpp"
#include "postpro/random_radical_voronoi.hpp"
#include "postpro/contacts_voronoi.hpp"
#include "postpro/residual_contacts.hpp"
#include "postpro/contact_anisotropy_2D.hpp"
#include "postpro/fn_ft_anisotropy_2D.hpp"
#include "postpro/anisotropy_3D_triaxial.hpp"
#include "file_util.hpp"
#include "nlfit.hpp"
#include "functions.hpp"
#include "voro++/voro++.hh"
#include <string>
#include <sstream>
#include <functional>
#include <algorithm>
#include <cmath>
#include <array>
#include "yaml-cpp/yaml.h"


enum BC_FILTER_t {BY_LIMITS = 0, BY_CENTERS = 1};

namespace DEM {
  class PostproAnalysis : public Postpro {
  public: // methods
    PostproAnalysis(); // default constructor, reads config
    void process(void);
    
  private: // data
    int niter_ = 0; // number of files to read = iterations to do
    std::vector<int> selected_iter_; 
    Random ran2_;
    MacroVars Macro_;
    Materials MaterialVars_;
    bool voronoi_ready_ = false;
    std::vector<double> voronoi_volumes_;
    UTIL::OFileStream zfile, floatingfile, energyfile, displacementfile, meandeltafile, brownianfile, pqfile, 
      gammaqoverpfile, voidratiofile, packfracfile,  wallsinfofile, wallsposfile, 
      wallsvelfile, wallsforcefile, rotinfofile, deformfile, eqepfile, epeqpqfile, eqqoverpfile, eq_packfracfile, eqfile,
      nccorrfile, cundallfile, 
      ekpartfile, ekpartnormfile, vmincubicfile,
      internalfrictionfile,
      voronoiXfile, voronoiKfile,  voronoiSfile, voronoiSuKfile, voronoiCfile,
      voronoiCuNfile, voronoiphifile, voronoiSTfile, voronoiVTfile,
      voronoiVminfile, voronoiVmeanfile, voronoiVsigmafile,
      voronoiVsigma2file, voronoiPfile;
    Histogram1D histoTotalVol_, histoVoroVol_, histoVoroVol_scaled_;
    Histogram1D P_contact_xy_, P_contact_xz_, P_contact_yz_; // accumulates for current time step
    Histogram1D P_contact_xy_full_, P_contact_xz_full_, P_contact_yz_full_; // accumulating for several time steps
    Histogram1D mean_Vx_profile_normalized_;
    Histogram1D P_nc_, P_nc_full_; // accumulates computes for internal particless, full for all
    Histogram1D P_delta_, P_delta2_;
    Histogram1D P_packingfraction_, P_qoverp_; 
    Histogram1D P_X;
    std::vector<std::vector<int> > neigh_; // neighbors of each particle from voronoi tessellation
    std::set<int> ref_contacts_; // for correlation of nc
    std::vector<double> time_vec, p_vec, q_vec, qoverp_vec, eq_vec, gamma_vec; // values as a function of time
    DEM::POSTPRO::HistogramsForceTorque forcetorque_distro;

    DEM::POSTPRO::Crystallization qcrystal; 
    DEM::POSTPRO::Delaunay delaunay; 
    DEM::POSTPRO::StressHistogram stress_per_grain_histograms;
    DEM::POSTPRO::Homogeinity homogeinity;
    DEM::POSTPRO::StatMechInternal statmech_internal;
    DEM::POSTPRO::StatMechMaterials statmech_material;
    DEM::POSTPRO::StatMechAlongXAxis statmech_alongxaxis;
    DEM::POSTPRO::VoronoiMultiscale voro_multiscale;
    DEM::POSTPRO::VoronoiNparticles voro_nparticles;
    DEM::POSTPRO::CentroidalVoronoi centroidalvoro_;
    DEM::POSTPRO::VoronoiZ voro_z;
    DEM::POSTPRO::VoronoiNoFloating vorofiltered;
    DEM::POSTPRO::RandomRadicalVoronoi randomradicalvoro_;
    DEM::POSTPRO::ContactsDelaunay contactsdelaunay; 
    DEM::POSTPRO::ContactsVoronoi contactsvoronoi; 
    DEM::POSTPRO::ResidualContacts residualcontacts_; 
    DEM::POSTPRO::ContactAnisotropy2D ac_2D_;
    DEM::POSTPRO::FnFtAnisotropy2D an_at_2D_;
    DEM::POSTPRO::Anisotropy3DTriaxial ani_3D_triaxial_;

  private: // methods
    // utility functions
    void startup_tasks(void);
    void open_file_streams(void);
    void close_file_streams(void);
    // contact utility function
    double Epot(const Contact & contact);
    Vec3d_t branch_vector(const Contact & contact);
    double volume_from_inside_contacts(void);
    // postpro functions
    void common_tasks(int i);
    double internal_volume(const BC_FILTER_t & criteria);
    double volume_from_dl(const double & dl);
    double grain_volume(const Particle & body);
    double volume_grains(const bool & filter = true);
    double get_void_ratio(void);
    double get_packing_fraction(const bool & filter = true);
    double mean_velocity_trans(void);
    double mean_velocity_rot(void);
    double rotational_energy(void);
    double translational_energy(void);
    double kinetic_energy(void);
    double gravitational_energy(void); 
    void coordination_number(void);
    void energy(void);
    void start_histograms(void);
    void print_void_ratio(void);
    void stress_fabric_tensors(void);
    // void mobility_brownian_displacement(const int id, const double f);
    void mean_max_penetration(void);
    // void displacement(int id);
    void mean_x_translation_profile(const int & iter);
    void mean_Vx_profile(const int & iter);
    void grain_size_profile(void);
    void process_histograms(const int & iter);
    void total_volume_histogram(void);
    void voronoi_volume_histogram(void);
    void contacts_orientation_histograms(const int & iter);
    void number_of_contacts_histograms(void);
    void penetration_histograms(void); 
    void walls_info(void);
    // void rotational_info(void);
    void print_deformations(void);
    void compute_defs(double & ex, double & ey, double & ez, double & ep, double & eq, double & gamma);
    void compute_gamma_def(double & gamma);
    double inner_voronoi_volume(const bool & filter = true);
    void voronoi_compute_volumes(std::vector<double> & volume_array);
    void voronoi_print_partition_parameters(void);
    void voronoi_print_scaled_histogram(Histogram1D & histo, const std::string & filename);
    void correlation_nc(void);
    void init_correlation_nc(void);
    void time_correlation_qoverp(void);
    void print_histograms(void);
    void cundall_param(void);
    void print_vmin_cubic(void); 
    bool print_snapshot(const int & iter);
    void walls_limits(double wmin[3], double wmax[3]);
  };
  

} // namespace DEM

#endif // POST_PRO
