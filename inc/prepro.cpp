#include "prepro.hpp"
#include "walls_normalvectors.hpp"

namespace DEM {

  Initialization::Initialization(void) 
  { 
    read_config();

    // initializations
    ran2.seed(seed_);
    lx_ = (1.0 + 1.0e-12)*ncolumns_*voxel_size_;
    ly_ = (1.0 + 1.0e-12)*nyboxes_*voxel_size_;
    if (2 == dim_) ly_ = (1.0 + 1.0e-12)*nyboxes_*2*std::max(max_rad1_, max_rad2_);
    lz_ = (1.0 + 1.0e-12)*nrows_*voxel_size_;
    nwalls_ = DEM::NWALLS; // DEFAULT
    //particle.resize(ngrains_ + nwalls_); // moved to read config to allow to read positions
    for (auto id = 0; id < ngrains_ + nwalls_; ++id) particle[id].uid = id;
    set_walls_positions(lx_, ly_, lz_);
    // parallel generators
    UTIL::print_start("Initializing parallel random generators ...");
#pragma omp parallel
    {
      int tid = omp_get_thread_num();
      if (0 == tid) {
	std::cout << "# Number of threads : " << omp_get_num_threads() << std::endl;
	randomgen_.resize(omp_get_num_threads());
	for (int ii = 0; ii < omp_get_num_threads(); ++ii)
	  randomgen_[ii].seed(seed_ + ii);
      }
    }
    UTIL::print_done("Initializing parallel random generators ...");
  }

  void 
  Initialization::read_config(void) 
  { 
    UTIL::print_start("Reading prepro config. ");
    YAML::Node config = YAML::LoadFile("INPUT/prepro_conf.yaml");
    ngrains_         = config["ngrains"].as<int>();
    ncolumns_        = config["ncolumns"].as<int>();
    nrows_           = config["nrows"].as<int>();
    nyboxes_         = config["nyboxes"].as<int>();
    voxel_size_      = config["voxel_size"].as<double>();
    min_rad1_        = config["granu1"][0].as<double>();
    max_rad1_        = config["granu1"][1].as<double>();
    prop1_           = config["granu1"][2].as<double>();
    min_rad2_        = config["granu2"][0].as<double>();
    max_rad2_        = config["granu2"][1].as<double>();
    prop2_           = config["granu2"][2].as<double>();
    V0_              = config["V0"].as<double>();
    W0_              = config["W0"].as<double>();
    seed_            = config["seed"].as<int>();
    alpha_           = config["VOXEL_DISORDER"].as<double>();
    WALL_MASS_SCALE_ = config["WALL_MASS_SCALE"].as<double>(5.0);
    densify_         = config["GEOMETRIC_DENSIFY"].as<int>();
    NTRIAL_          = config["NTRIAL"].as<int>(10); 
    cube_densify_    = config["CUBE_DENSIFY"].as<int>(0); 
    surface_factor_  = config["DENSIFY_SURFACE_FACTOR"].as<double>(1.9); 
    std::string granu_file = config["GRANULOMETRY_FILE"].as<std::string>("");
    std::string positions_file = config["POSITIONS_FILE"].as<std::string>("");

    UTIL::print_done("Reading prepro config. ");

    // print read config
    std::ofstream fout("OUTPUT/prepro_conf.yaml");
    fout << config;
    fout.close();
    print_log("Printed read config to OUTPUT/prepro_conf.yaml");

    // resize particle to get memeory
    particle.resize(ngrains_ + nwalls_);

    // possibly, read granulometry
    if (false == granu_file.empty()) {
      read_granulometry_file(granu_file);
    }

    // possibly, read the positions of particles
    npositions_read_ = 0;
    if (false == positions_file.empty()) {
      particle.resize(ngrains_ + nwalls_); // resize positions vector
      npositions_read_ = read_positions(positions_file);
      std::string ofile="OUTPUT/read_pos_file.txt";
      write_positions(ofile, npositions_read_);
    }

    // general config
    UTIL::print_start("PREPRO: Reading general_conf.yaml");
    config = YAML::LoadFile("INPUT/general_conf.yaml");
    dim_ = config["dim"].as<int>(); ASSERT(2 <= dim_ && dim_ <= 3);
    periodic_[0] = config["periodic"][0].as<int>();
    periodic_[1] = config["periodic"][1].as<int>();
    periodic_[2] = config["periodic"][2].as<int>();
    UTIL::print_done("PREPRO: Reading general_conf.yaml");

    // materials config
    UTIL::print_start("PREPRO: Reading materials_conf.yaml (reading tag 0 -> RHO)");
    config = YAML::LoadFile("INPUT/materials_conf.yaml");
    rho_ = config[0]["RHO"].as<double>(); // RHO
    UTIL::print_done("PREPRO: Reading materials_conf.yaml");
    
    // checks
    UTIL::print_start("Checking read parameters ... ");
    ASSERT(1 <= nyboxes_); 
    if (2 == dim_) ASSERT (1 == nyboxes_);   
    if (true == granu_file.empty()) {
      ASSERT(min_rad1_ > 0 && min_rad1_ <= max_rad1_);
      ASSERT(min_rad2_ > 0 && min_rad2_ <= max_rad2_);
    } else {
      ASSERT(int(granu_values_.size()) == ngrains_);
      std::cout << "# min_rad1_ : " << min_rad1_ << std::endl;
      std::cout << "# max_rad1_ : " << max_rad1_ << std::endl;
      ASSERT(min_rad1_ > 0 && min_rad1_ <= max_rad1_);
    }
    ASSERT(prop1_ >= 0 && prop2_ >= 0);
    ASSERT(prop1_ + prop2_ <= 1.0);
    if (false == densify_ && 0 == cube_densify_){
      if (ngrains_ > ncolumns_*nrows_*nyboxes_) {
	print_error_and_exit("ERROR: ngrains_ > ncolumns_*nrows_*nyboxes_");
      };
      if (prop1_ > 0) ASSERT(voxel_size_ > 2*max_rad1_);
      if (prop2_ > 0) ASSERT(voxel_size_ > 2*max_rad2_);
    }
    ASSERT(0 <= alpha_  && alpha_ <= 1);
    ASSERT(WALL_MASS_SCALE_ > 0);
    ASSERT(NTRIAL_ > 0);
    ASSERT(cube_densify_ >= 0);
    UTIL::print_done("Checking read parameters ");
  }

  void
  Initialization::read_granulometry_file(const std::string & fname)
  {
    UTIL::print_start("PREPRO: Reading granulometry file : " + fname);
    std::ifstream fin(fname);
    if (!fin) {
      print_error_and_exit("Cannot open granulometry file : " + fname);
    }
    min_rad1_ = 1.0e10;
    max_rad1_ = -1.0e10;
    // Read granulometry file and then compare the number of read data 
    granu_values_.clear();
    double tmprad;
    while(fin >> tmprad) {
      granu_values_.push_back(tmprad);
      min_rad1_ = std::min(min_rad1_, tmprad);
      max_rad1_ = std::max(max_rad1_, tmprad);
    }
    fin.close();
    if (ngrains_ != granu_values_.size()) {
      print_error("ngrains from prepro_conf.yaml:", ngrains_);
      print_error("ngrains from granulo_file    :", granu_values_.size());
      print_error_and_exit("Exiting.");
    }
    // print read data
    std::ofstream fout("OUTPUT/radii.txt");
    for(const auto & r : granu_values_) {
      fout << r << "\n";
    }
    fout.close();
    print_log("Read granulometry wrote to OUTPUT/radii.txt");
    UTIL::print_done("PREPRO: Reading granulometry file : " + fname);
  }


  int
  Initialization::read_positions(const std::string & fname)
  {
    /// This function reads the starting position of particles
    /// in case one needs to fix their position and not densified them.
    /// This is useful, for instance, to store partially densified positions
    /// and to restart the algorithm from that point.  But can also be
    /// useful in other cases, like to read just positions and then assing
    /// masses and other stuff locally.
    /// The file should be formatted as
    /// N_TOTAL_PARTICLES
    /// N_PARTICLES_TOREAD
    /// x1 y1 z1
    /// x2 y2 z2
    /// ...
    /// It is assumed that N_PARTICLES_TOREAD <= N_TOTAL_PARTICLES
    ///
    
    UTIL::print_start("PREPRO: Reading positions file : " + fname);
    std::ifstream fin(fname);
    if (!fin) {
      print_error_and_exit("Cannot open positions file : " + fname);
    }
    int NTOTAL = 0, NTOREAD = 0;
    fin >> NTOTAL; // first line
    fin >> NTOREAD; // second line
    print_log("NTOTAL:", NTOTAL);
    print_log("NTOREAD:", NTOREAD);
    print_log("particle size:", particle.size());
    ASSERT(NTOREAD <= NTOTAL);
    ASSERT(0 <= NTOTAL);
    ASSERT(NTOTAL <= particle.size());
    for (int ii = 0; ii < NTOREAD; ++ii) {
      fin >> particle[ii].R[0] >> particle[ii].R[1] >> particle[ii].R[2];
    }
    fin.close();
    UTIL::print_done("PREPRO: Reading positions file : " + fname);
    return NTOREAD;
  }

  void
  Initialization::write_positions(const std::string & fname, const int ntowrite)
  {
    /// This function writes the positions of particles up to ntowrite, following
    /// the format expected by read_positions
    /// The file should be formatted as
    /// N_TOTAL_PARTICLES
    /// N_PARTICLES_TOREAD
    /// x1 y1 z1
    /// x2 y2 z2
    /// ...
    /// It is assumed that N_PARTICLES_TOREAD <= N_TOTAL_PARTICLES
    ///
    UTIL::print_start("PREPRO: Writing positions file : " + fname);
    print_log("ntowrite: ", ntowrite);
    print_log("particlesize: ", particle.size());
    ASSERT(ntowrite <= particle.size());
    std::ofstream fout(fname);
    if (!fout) {
      print_error_and_exit("Cannot open positions file : " + fname);
    }
    // header writing
    fout << particle.size() << std::endl; // first line
    fout << ntowrite << std::endl; // second line
    // positions writing
    for (int ii = 0; ii < ntowrite; ++ii) {
      fout << particle[ii].R[0] << "\t" << particle[ii].R[1] << "\t" << particle[ii].R[2] << std::endl;
    }
    fout.close();
    UTIL::print_done("PREPRO: Writing positions file : " + fname);
  }
  
  void 
  Initialization::build_and_print()
  {
    set_features(); // IMPORTANT: before initial_conditions (radius is needed)
    initial_conditions(); // R(0), V(0)
    print();
  }


  void 
  Initialization::set_features(void)
  {
    set_grain_features();
    // walls
    ASSERT(total_mass_grains_ > 0);
    for (auto id = ngrains_; id < ngrains_ + nwalls_; ++id) {
      particle[id].type = Particle::WALL;
      particle[id].materialTag = 1;
      particle[id].rad = 0;
      particle[id].mass = WALL_MASS_SCALE_*total_mass_grains_;    
      //particle[id].mass = WALL_MASS_SCALE_*total_mass_grains_/ngrains_;  // Average grain mass  
      //particle[id].mass = 10*std::pow(ngrains_, 2.0/dim_)*total_mass_grains_/ngrains_;  // Radjai book, corrected 
      particle[id].inerI = 1.0e100; // large, plane walls do not rotate
    }
    if (2 == dim_) {
      particle[ngrains_ + BACK].alive = false;
      particle[ngrains_ + FRONT].alive = false;
    }
  }
  void
  Initialization::set_walls_positions(double lx, double ly, double lz)
  {
    particle[ngrains_ + RIGHT].R0[0] = particle[ngrains_ + RIGHT].R[0] = 1.0*lx;
    particle[ngrains_ + FRONT].R0[1] = particle[ngrains_ + FRONT].R[1] = 1.0*ly;
    particle[ngrains_ + TOP  ].R0[2] = particle[ngrains_ + TOP  ].R[2] = 1.0*lz;
  }
  
  void 
  Initialization::set_grain_features(void)
  {
    total_mass_grains_ = 0;
    double newrad = 0;
    for (auto id = 0; id < ngrains_; ++id) {
      if (0 == granu_values_.size()) {
	double min_rad = min_rad1_; 
	double max_rad = max_rad1_;
	if (ran2.r() > prop1_/(prop1_ + prop2_)) {
	  min_rad = min_rad2_; max_rad = max_rad2_;
	}
	// Uniform in mass, no size dominates
	//if(2 == dom.dim) newrad = min_rad*max_rad/(max_rad - ran2.r()*(max_rad - min_rad)); 
	//else if(3 == dom.dim) newrad = min_rad*max_rad/sqrt(max_rad*max_rad - ran2.r()*(max_rad*max_rad - min_rad*min_rad)); 
	newrad = min_rad/sqrt(1.0 - ran2.r()*(1 - min_rad*min_rad/(max_rad*max_rad))); // 3D
      } else { // granulometry read from file
	newrad = granu_values_[id];
      }
      particle[id].rad = newrad;
      particle[id].materialTag = 0;
      particle[id].mass = rho_*4.0/3.0*M_PI*pow(particle[id].rad, 3); // 3D
      particle[id].inerI = (2.0/5.0)*particle[id].mass*pow(particle[id].rad, 2); // 3D 
      total_mass_grains_ += particle[id].mass;
    }
  }

  void
  Initialization::initial_conditions(void) // R(0), V(0)
  {
    int id;
    Vec3d_t tmpVec;
    
    // PLANEWALLS
    // R will be fixed on initial position grains
    for (id = ngrains_; id < ngrains_ + nwalls_; ++id) {
      particle[id].R0 = particle[id].R = Vecnull;
      particle[id].V = Vecnull;
      particle[id].PHI = Vecnull;
      particle[id].W = Vecnull;
      particle[id].F = particle[id].T = Vecnull;
    }
    set_walls_positions(lx_, ly_, lz_);

    // GRAINS 
    // positions
    initial_positions(); 
    if(check_penetration()) { // check_penetration returns true if interpenetrated
      print_error("Attention!!!!!! Interpenetration detected!!!!");
    }
    
    // velocities: chosen at random with fixed modulus
    for (id = 0; id < ngrains_; ++id) {
      tmpVec[0] = ran2.uniform(-1.0, 1.0);
      tmpVec[1] = ran2.uniform(-1.0, 1.0); if (2 == dim_) tmpVec[1] = 0;
      tmpVec[2] = ran2.uniform(-1.0, 1.0);
      particle[id].V = V0_*tmpVec.normalized();
    }
    // angles
    for (id = 0; id < ngrains_; ++id) {
      particle[id].PHI = Vec3d_t(0, 0, 0);
    }
    // angular velocities 
    for (id = 0; id < ngrains_; ++id) {
      tmpVec[0] = ran2.uniform(-1.0, 1.0); if (2 == dim_) tmpVec[0] = 0;
      tmpVec[1] = ran2.uniform(-1.0, 1.0); 
      tmpVec[2] = ran2.uniform(-1.0, 1.0); if (2 == dim_) tmpVec[2] = 0;
      particle[id].W = W0_*tmpVec.normalized();
    }
    // forces and torques to null
    for (id = 0; id < ngrains_; ++id) {
      particle[id].F = particle[id].T = Vecnull;
    }
  }

  void 
  Initialization::initial_positions(void) /// R(0)
  {
    double max_z;
    if (false == densify_ && 0 == cube_densify_) {
      put_particles_inside_box(max_z); 
      print_log("zmax after box = ", max_z);
    }
    else if (densify_ || cube_densify_) {
      // computing volume and possible cubic dimensions
      double vol = 0.0;
      for (const auto & b : particle) {
	vol += 0.95*std::pow(2*b.rad, 3); // 0.95 because is slightly smaller than the cube
	//vol += 4*M_PI*std::pow(b.rad, 3)/3;
      }
      lx_ = ly_ = lz_ = std::cbrt(vol); // 3D
      set_walls_positions(lx_, ly_, lz_);
      if(densify_ && !cube_densify_) {
	densify_pack_geometric_event_driven(max_z); 
      }
      if (cube_densify_) {
	std::cout << "# Making " << cube_densify_ << " rounds for densifying to cube ...\n";
	densify_to_cube(cube_densify_);
      }
    }
    print_log("Box final dimensions: ");
    print_log("xmin = ", particle[ngrains_ + LEFT  ].R[0]);
    print_log("xmax = ", particle[ngrains_ + RIGHT ].R[0]);
    print_log("ymin = ", particle[ngrains_ + BACK  ].R[1]);
    print_log("ymax = ", particle[ngrains_ + FRONT ].R[1]);
    print_log("zmin = ", particle[ngrains_ + BOTTOM].R[2]);
    print_log("zmax = ", particle[ngrains_ + TOP   ].R[2]);
    check_penetration(); 
  }

  void 
  Initialization::put_particles_inside_box(double & max_z)
  {
    print_log("# Setting particles inside box ... ");
    double x0 = 0.0, y0 = 0.0, z0 = 0.0; // in voxel_size units 
    if (periodic_[0]) { x0 -= 0.45; }
    if (periodic_[1] && 3 == dim_) { y0 -= 0.45; }
    if (periodic_[2]) { z0 -= 0.45; }
    auto id = 0;
    for (auto row = 0; row < nrows_; ++row) { // z
      for (auto ybox = 0; ybox < nyboxes_; ++ybox) { // y
	for (auto column = 0; column < ncolumns_; ++column) { // x
	  //auto id = (ncolumns_*nyboxes_)*row + ncolumns_*ybox + column;
	  if (id >= ngrains_) break;
	  Vec3d_t Pos(x0 + column%ncolumns_ + 0.5, y0 + ybox%nyboxes_ + 0.5, z0 + row%nrows_ + 0.5);
	  Pos *= voxel_size_;
	  double d = 2*particle[id].rad; 
	  double f = voxel_size_ - d; // free space
	  f *= alpha_;
	  Pos += Vec3d_t(f*ran2.uniform(-0.5, 0.5), f*ran2.uniform(-0.5, 0.5), f*ran2.uniform(-0.5, 0.5));
	  if (2 == dim_) Pos[1] = (ybox%nyboxes_ + 0.5)*2*std::max(max_rad1_, max_rad2_);
	  particle[id].R0 = particle[id].R = Pos; 
	  ++id;
	}
      }
    }
    max_z = particle[0].max_limit(2);
    for (auto id = 1; id < ngrains_; ++id) { 
      if (particle[id].max_limit(2) > max_z) max_z = particle[id].max_limit(2);
    }
    print_log("# DONE. ");
  }
  

  bool
  Initialization::check_penetration(void) 
  {
    print_log("Checking penetrations ...");
    DistanceToSphere distance_ss;
    distance_ss.set_periodic(periodic_, lx_, ly_, lz_);
    DistanceToWall distance_sw;
    Vec3d_t Normal(Vecnull);
    double delta;
    int id, jd; 
    for(id = 0; id < ngrains_; ++id) {
      for(jd = id+1; jd < ngrains_; ++jd) {
	delta = 0;
	Normal = Vecnull;
	distance_ss(particle[id], particle[jd], delta, Normal);
	if(delta > 1.0e-10) { 
	  std::cerr << "# ERROR: Particle-particle Penetration detected for id = " << id << " and jd = " << jd << std::endl; 
	  std::cerr << "# Types: id.type = " << particle[id].type << ", and jd.type = " << particle[jd].type << std::endl; 
	  print_error("delta         = ", delta);
	  print_error("grain[id].rad = ", particle[id].rad);
	  print_error("grain[id].R   = ", particle[id].R);
	  print_error("grain[jd].rad = ", particle[jd].rad);
	  print_error("grain[jd].R   = ", particle[jd].R);
	  print_error("Normal Vector = ", Normal);
	  print_error("(EXITING ON FIRST ERROR) ");
	  return true; 
	}
      }    
    }
    for(id = 0; id < ngrains_; ++id) {
      for(jd = ngrains_; jd < ngrains_ + nwalls_; ++jd) {
	if (periodic_[0] && ((jd == ngrains_ + LEFT) || (jd == ngrains_ + RIGHT))) continue;
	if (periodic_[1] && ((jd == ngrains_ + BACK) || (jd == ngrains_ + FRONT))) continue;
	if (periodic_[2] && ((jd == ngrains_ + TOP) || (jd == ngrains_ + BOTTOM))) continue;
	Normal = NORMAL_VEC_WALL[jd-ngrains_];
	delta = 0;
	distance_sw(particle[id], particle[jd], delta, Normal);
	if(delta > 1.0e-10) { 
	  std::cerr << "# ERROR: Particle-wall Penetration detected for id = " << id << " and jd = " << jd << std::endl; 
	  std::cerr << "# Types: id.type = " << particle[id].type << ", and jd.type = " << particle[jd].type << std::endl; 
	  print_error("delta         = ", delta);
	  print_error("grain[id].rad = ", particle[id].rad);
	  print_error("grain[id].R   = ", particle[id].R);
	  print_error("grain[jd].rad = ", particle[jd].rad);
	  print_error("grain[jd].R   = ", particle[jd].R);
	  print_error("Normal Vector = ", Normal);
	  print_error("(EXITING ON FIRST ERROR) ");
	  return true; 
	}
      }    
    }
    print_log("DONE :: Checking penetrations ...");
    return false;
  }

  // OPENMP helper
  struct xyz {
    double x; double y; double z;
  };

  struct xyz xyz_min2(struct xyz a, struct xyz b) {
    return a.z < b.z ? a : b; 
  }

#pragma omp declare reduction(xyz_min: struct xyz: omp_out=xyz_min2(omp_out, omp_in)) \
  initializer(omp_priv={0, 0, DBL_MAX})
  
  
  void 
  Initialization::densify_pack_geometric_event_driven(double & max_z, int start_id)
  {
    //-------------------------------------------------------------------
    // perform small event driven simulation to densify the system
    //-------------------------------------------------------------------
    max_z = -10;
    print_log("# Performing small densification with fictitious event driven sim ... ");
    double xA = 0.0, yA = 0.0; 
    double xB = lx_, yB = ly_;
    if (periodic_[0]) { xA = -0.4*std::min(max_rad1_, max_rad2_); xB = lx_ + 0.4*std::min(max_rad1_, max_rad2_); } 
    if (periodic_[1] && 3 == dim_) { yA = -0.4*std::min(max_rad1_, max_rad2_); yB = ly_ + 0.4*std::min(max_rad1_, max_rad2_); } 

    // surface
    std::set<int> surface;
    // Put fixed particles  into the surface
    // This takes into account if some positions have been read
    for (int ii = 0; ii < start_id; ++ii) {
      surface.insert(ii);
    }
    if (start_id > 0) 
      update_surface(start_id-1, surface);

    const double scale = 1.0001; // scale radius to slightly large to more separation for the particles 
    particle[ngrains_ + TOP].R[2] = 2*ngrains_*std::max(max_rad1_, max_rad2_);

    
    print_log("# Particle : ");
    for(int id = start_id; id < ngrains_; ++id) {
      std::cout << "# " << id << std::flush << "\r"; 
      //if ( TOP_GRAIN_GROUP_TAG == particle[id].groupTag || TOP_GRAIN_SINGLE_TAG == particle[id].bcTag) continue; 
      double xf = 0, yf = 0, zf = 0.0;
      double zfmin = particle[ngrains_ + TOP].R[2]; 

      struct xyz value = {0, 0, zfmin}; // struct to save the final dxyz data for the minimum z

      // Here I am using a custom reduction after help : https://stackoverflow.com/questions/52390526/openmp-argmin-reduction-for-multiple-values
#pragma omp parallel
      {
	int tid = omp_get_thread_num();
#pragma omp for reduction(xyz_min:value) 
	for(int trial = 0; trial < NTRIAL_; ++trial) {
	  Vec3d_t V = Vec3d_t(0, 0, -0.1); // magnitude does not matter. vertical velocity only
	  if (2 == dim_) V[1] = 0 ; // 2D fix 
	  double x0 = randomgen_[tid].uniform(xA + particle[id].rad, xB - particle[id].rad);  
	  double y0 = randomgen_[tid].uniform(yA + particle[id].rad, yB - particle[id].rad); 

	  const double DY = (yB - yA - 2*particle[id].rad)/omp_get_num_threads(); 
	  int thread_id = omp_get_thread_num();
	  y0 = randomgen_[tid].uniform(thread_id*DY, (thread_id+1)*DY); 


	  if (periodic_[0]) x0 = randomgen_[tid].uniform(xA, xB); 
	  if (periodic_[1]) y0 = randomgen_[tid].uniform(yA, yB); 
	  if (2 == dim_) y0 = ly_/2.0;  // 2D fix
	  double z0 = particle[ngrains_ + TOP].R[2]; 
	  //--------------------------------------------------------------------------------------------
	  // compute collision times
	  // with bottom
	  double tmin = 0, t = 0;
	  tmin = t = (z0 - particle[id].rad*scale)/fabs(V[2]); 
	  // with other particles 
	  for(std::set<int>::iterator it = surface.begin(); it != surface.end(); ++it) {
	    auto jd = *it;
	    Vec3d_t Rij = Vec3d_t(x0, y0, z0) - particle[jd].R;
	    // periodic correction
	    if (periodic_[0]) Rij[0] -= (particle[ngrains_ + RIGHT].R0[0])*lrint(Rij[0]/particle[ngrains_ + RIGHT].R0[0]); // WARNING: Assumes left starts at 0.0
	    if (periodic_[1]) Rij[1] -= (particle[ngrains_ + FRONT].R0[1])*lrint(Rij[1]/particle[ngrains_ + FRONT].R0[1]); // WARNING: Assumes back starts at 0.0
	    double radij = (particle[id].rad*scale + particle[jd].rad);
	    double scalar = Rij.dot(V);
	    if(scalar < 0) {
	      double Rij2 = Rij.squaredNorm();
	      double det =  scalar*scalar + V.squaredNorm()*(radij*radij - Rij2); 
	      if(det >= 0) {
		t = (Rij2 - radij*radij)/(std::sqrt(det) - scalar);
		if(t > 0 && t < tmin) tmin = t; // select the minimum time for this trial
	      } // det >= 0 
	    } // scalar < 0
	  } // jd
	  zf = z0 + V[2]*tmin;
	  if (zf <= zfmin) {
	    zfmin = zf;
	    xf = x0 + V[0]*tmin;
	    yf = y0 + V[1]*tmin; 
	    if (2 == dim_) yf = ly_/2; // 2D fix
	    struct xyz new_value = {xf, yf, zfmin};
	    value = xyz_min2(value, new_value);
	  } // zf < zfold
	} // end trials // end omp for parallel with openmp
      } // end omp parallel
      xf = value.x;
      yf = value.y;
      zfmin = value.z;
      
      // move to the minimum height found after trials
      particle[id].R[0] = xf; ASSERT((0 <= xf - particle[id].rad) && (xf + particle[id].rad <= lx_));
      particle[id].R[1] = yf; ASSERT((0 <= yf - particle[id].rad) && (yf + particle[id].rad <= ly_));
      particle[id].R[2] = zfmin;
      particle[id].R0 = particle[id].R;
      // surface update
      surface.insert(id);
      if(id > 0 && 0 == id%50) update_surface(id, surface);
      // save max_z
      if (zfmin + particle[id].rad > max_z) max_z = zfmin + particle[id].rad;

      // save particle positions to be reused in case of problem
      if (id%100 == 0) {
	std::string oposfile="OUTPUT/particle_positions.txt";
	write_positions(oposfile, id+1);
      }
    } // end id
    print_log("# Done trials");
    // correct positions of tagged particles at the top 
    //double max_z_old = max_z;
    //for (int id = 0; id < ngrains_; ++id) {
    //if ( TOP_GRAIN_GROUP_TAG == particle[id].groupTag || TOP_GRAIN_SINGLE_TAG == particle[id].bcTag) {
    //particle[id].R[2] = max_z_old + 2*std::max(max_rad1_, max_rad2_) - particle[id].rad;
    //max_z = (particle[id].max_limit(2) > max_z) ? particle[id].max_limit(2) : max_z;
    //}
    //}
    print_log("zmax after first densification = ", max_z);
    particle[ngrains_ + TOP].R0[2] = particle[ngrains_ + TOP].R[2] = max_z;
    lz_ = max_z;

    print_log(" DONE. ");
  }
  
  void 
  Initialization::update_surface(int id_last_added, std::set<int> & surface) 
  {
    std::cout << "# Starting surface.size : " << surface.size() << std::endl;
    const double mean_rad = 0.5*std::max(min_rad1_ + max_rad1_, min_rad2_ + max_rad2_);
    for (std::set<int>::iterator it = surface.begin(); it != surface.end();) {
      double distance = particle[id_last_added].R[2] - particle[id_last_added].rad - particle[*it].R[2] - particle[*it].rad; 
      if (particle[id_last_added].R[2] > particle[*it].R[2] && distance > surface_factor_*mean_rad) surface.erase(it++); // WARNING : surface_factor_ is a read magic number
      else ++it;
    }    
    std::cout << "# Final surface.size : " << surface.size() << std::endl;
    

  }

  void
  Initialization::densify_to_cube(int ntries)
  {
    std::cout << "# Densifying to an approximate cube ...\n";
    double max_z = 0;
    for (int count = 0; count < ntries; ++count) {
      std::cout << "# Cubic densification try : " << count << std::endl;
      std::cout << "# Original lx : " << lx_ << std::endl;
      std::cout << "# Original ly : " << ly_ << std::endl;
      std::cout << "# Original lz : " << lz_ << std::endl;
      packing_fraction();
      lx_ = ly_ = lz_ = std::cbrt(lx_*ly_*lz_);
      std::cout << "# New possible lx : " << lx_ << std::endl;
      std::cout << "# New possible ly : " << ly_ << std::endl;
      std::cout << "# New possible lz : " << lz_ << std::endl;
      int start_id = 0;
      if (0 == count && npositions_read_ > 0) start_id = npositions_read_-1;
      densify_pack_geometric_event_driven(max_z, start_id);
      write_positions("positions-try_"+std::to_string(count)+".txt", particle.size());
      std::cout << "# Final lx : " << lx_ << std::endl;
      std::cout << "# Final ly : " << ly_ << std::endl;
      std::cout << "# Final lz : " << lz_ << std::endl;
      packing_fraction();
    }
    // update walls positions
    particle[ngrains_ + RIGHT].R0[0] = particle[ngrains_ + RIGHT].R[0] = 1.0*lx_;
    particle[ngrains_ + FRONT].R0[1] = particle[ngrains_ + FRONT].R[1] = 1.0*ly_;
    particle[ngrains_ + TOP].R0[2] = particle[ngrains_ + TOP].R[2] = 1.0*lz_;
    std::cout << "Done.\n"; 
  }
  
  void 
  Initialization::print(void) 
  {
#if WITH_NETCDF
    
    int status;
    std::cout << " # PRINTING with netcdf!!!" << std::endl;
    status = write_particles_netcdf(particle, "OUTPUT/" BODIES_INFO_FILE); ASSERT(0 == status);
    std::vector<Contact> tmp_vec;
    status = write_contacts_netcdf(tmp_vec, "OUTPUT/" CONTACTS_INFO_FILE); ASSERT(0 == status);

#else
    // c style
    std::FILE * fp = std::fopen("OUTPUT/" BODIES_INFO_FILE, "w"); ASSERT(fp);
    fprintf(fp, "%10d\t%10d\t%10d\n\n", ngrains_ + nwalls_, ngrains_, nwalls_);
    for (int i = 0; i < ngrains_ + nwalls_; ++i) {
      particle[i].print(fp);
    }
    std::fclose(fp);
    // print empty contacts file
    fp = std::fopen("OUTPUT/" CONTACTS_INFO_FILE, "w"); ASSERT(fp);
    std::fclose(fp);


#endif
  }

  void
  Initialization::packing_fraction(void)
  {
    double Vol = lx_*ly_*lz_;
    double Vp = 0;
    for (int id = 0; id < ngrains_; ++id) {
      Vp += std::pow(particle[id].rad, 3);
    }
    Vp *= 4*M_PI/3.0;
    std::cout << "Packing fraction : " << Vp/Vol << std::endl;
  }

} // namespace DEM

