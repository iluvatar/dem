#include "netCDF_io.hpp"

//-------------------------------------
// implementations
//-------------------------------------

#if WITH_NETCDF

//----------------------------------------------------------------------------------------
// WRITING
//----------------------------------------------------------------------------------------

int write_particles_netcdf(const std::vector<Particle> & particles, const std::string & filename)
{
  // The default behavior of the C++ API is to throw an exception i
  // an error occurs. A try catch block is necessary.
  try {
    // local arrays to store individual data
    std::vector<int> ivec;
    std::vector<double> dvecx, dvecy, dvecz;
    
    // get size and create the arrays to store the data
    const int SIZE = particles.size();
    ivec.resize(SIZE, 0);
    dvecx.resize(SIZE, 0.0);
    dvecy.resize(SIZE, 0.0);
    dvecz.resize(SIZE, 0.0);
    
    // create data file
    netCDF::NcFile dataFile(filename, netCDF::NcFile::replace);
    
    // create dimension id
    netCDF::NcDim idDim = dataFile.addDim("id", SIZE);
    
    // add uid
    for (int id = 0; id < SIZE; ++id) { ivec[id] = particles[id].uid; } 
    netCDF::NcVar uid = dataFile.addVar("uid", netCDF::ncInt, idDim);
    uid.setCompression(true, true, 3);
    uid.putVar(ivec.data());
    // add alive
    for (int id = 0; id < SIZE; ++id) { ivec[id] = particles[id].alive; }
    netCDF::NcVar alive = dataFile.addVar("alive", netCDF::ncInt, idDim);
    alive.setCompression(true, true, 3);
    alive.putVar(ivec.data());
    // add type
    for (int id = 0; id < SIZE; ++id) { ivec[id] = particles[id].type; }
    netCDF::NcVar type = dataFile.addVar("type", netCDF::ncInt, idDim);
    type.setCompression(true, true, 3);
    type.putVar(ivec.data());
    // add bcTag
    for (int id = 0; id < SIZE; ++id) { ivec[id] = particles[id].bcTag; }
    netCDF::NcVar bcTag = dataFile.addVar("bcTag", netCDF::ncInt, idDim);
    bcTag.setCompression(true, true, 3);
    bcTag.putVar(ivec.data());
    // add groupTag
    for (int id = 0; id < SIZE; ++id) { ivec[id] = particles[id].groupTag; }
    netCDF::NcVar groupTag = dataFile.addVar("groupTag", netCDF::ncInt, idDim);
    groupTag.setCompression(true, true, 3);
    groupTag.putVar(ivec.data());
    // add materialTag
    for (int id = 0; id < SIZE; ++id) { ivec[id] = particles[id].materialTag; }
    netCDF::NcVar materialTag = dataFile.addVar("materialTag", netCDF::ncInt, idDim);
    materialTag.setCompression(true, true, 3);
    materialTag.putVar(ivec.data());
    // add R0[0], R0[1], R0[2]
    for (int id = 0; id < SIZE; ++id) { dvecx[id] = particles[id].R0[0]; dvecy[id] = particles[id].R0[1]; dvecz[id] = particles[id].R0[2]; }
    netCDF::NcVar R0x = dataFile.addVar("R0x", netCDF::ncDouble, idDim);
    netCDF::NcVar R0y = dataFile.addVar("R0y", netCDF::ncDouble, idDim);
    netCDF::NcVar R0z = dataFile.addVar("R0z", netCDF::ncDouble, idDim);
    R0x.setCompression(true, true, 3);
    R0y.setCompression(true, true, 3);
    R0z.setCompression(true, true, 3);
    R0x.putVar(dvecx.data()); R0y.putVar(dvecy.data()); R0z.putVar(dvecz.data());
    // add R[0], R[1], R[2]
    for (int id = 0; id < SIZE; ++id) { dvecx[id] = particles[id].R[0]; dvecy[id] = particles[id].R[1]; dvecz[id] = particles[id].R[2]; }
    netCDF::NcVar Rx = dataFile.addVar("Rx", netCDF::ncDouble, idDim);
    netCDF::NcVar Ry = dataFile.addVar("Ry", netCDF::ncDouble, idDim);
    netCDF::NcVar Rz = dataFile.addVar("Rz", netCDF::ncDouble, idDim);
    Rx.setCompression(true, true, 3);
    Ry.setCompression(true, true, 3);
    Rz.setCompression(true, true, 3);
    Rx.putVar(dvecx.data()); Ry.putVar(dvecy.data()); Rz.putVar(dvecz.data());
    // add V[0], V[1], V[2]
    for (int id = 0; id < SIZE; ++id) { dvecx[id] = particles[id].V[0]; dvecy[id] = particles[id].V[1]; dvecz[id] = particles[id].V[2]; }
    netCDF::NcVar Vx = dataFile.addVar("Vx", netCDF::ncDouble, idDim);
    netCDF::NcVar Vy = dataFile.addVar("Vy", netCDF::ncDouble, idDim);
    netCDF::NcVar Vz = dataFile.addVar("Vz", netCDF::ncDouble, idDim);
    Vx.setCompression(true, true, 3);
    Vy.setCompression(true, true, 3);
    Vz.setCompression(true, true, 3);
    Vx.putVar(dvecx.data()); Vy.putVar(dvecy.data()); Vz.putVar(dvecz.data());
    // add F[0], F[1], F[2]
    for (int id = 0; id < SIZE; ++id) { dvecx[id] = particles[id].F[0]; dvecy[id] = particles[id].F[1]; dvecz[id] = particles[id].F[2]; }
    netCDF::NcVar Fx = dataFile.addVar("Fx", netCDF::ncDouble, idDim);
    netCDF::NcVar Fy = dataFile.addVar("Fy", netCDF::ncDouble, idDim);
    netCDF::NcVar Fz = dataFile.addVar("Fz", netCDF::ncDouble, idDim);
    Fx.setCompression(true, true, 3);
    Fy.setCompression(true, true, 3);
    Fz.setCompression(true, true, 3);
    Fx.putVar(dvecx.data()); Fy.putVar(dvecy.data()); Fz.putVar(dvecz.data());
    // add F_fixed[0], F_fixed[1], F_fixed[2]
    for (int id = 0; id < SIZE; ++id) { dvecx[id] = particles[id].F_fixed[0]; dvecy[id] = particles[id].F_fixed[1]; dvecz[id] = particles[id].F_fixed[2]; }
    netCDF::NcVar F_fixedx = dataFile.addVar("F_fixedx", netCDF::ncDouble, idDim);
    netCDF::NcVar F_fixedy = dataFile.addVar("F_fixedy", netCDF::ncDouble, idDim);
    netCDF::NcVar F_fixedz = dataFile.addVar("F_fixedz", netCDF::ncDouble, idDim);
    F_fixedx.setCompression(true, true, 3);
    F_fixedy.setCompression(true, true, 3);
    F_fixedz.setCompression(true, true, 3);
    F_fixedx.putVar(dvecx.data()); F_fixedy.putVar(dvecy.data()); F_fixedz.putVar(dvecz.data());
    // add PHI[0], PHI[1], PHI[2]
    for (int id = 0; id < SIZE; ++id) { dvecx[id] = particles[id].PHI[0]; dvecy[id] = particles[id].PHI[1]; dvecz[id] = particles[id].PHI[2]; }
    netCDF::NcVar PHIx = dataFile.addVar("PHIx", netCDF::ncDouble, idDim);
    netCDF::NcVar PHIy = dataFile.addVar("PHIy", netCDF::ncDouble, idDim);
    netCDF::NcVar PHIz = dataFile.addVar("PHIz", netCDF::ncDouble, idDim);
    PHIx.setCompression(true, true, 3);
    PHIy.setCompression(true, true, 3);
    PHIz.setCompression(true, true, 3);
    PHIx.putVar(dvecx.data()); PHIy.putVar(dvecy.data()); PHIz.putVar(dvecz.data());
    // add W[0], W[1], W[2]
    for (int id = 0; id < SIZE; ++id) { dvecx[id] = particles[id].W[0]; dvecy[id] = particles[id].W[1]; dvecz[id] = particles[id].W[2]; }
    netCDF::NcVar Wx = dataFile.addVar("Wx", netCDF::ncDouble, idDim);
    netCDF::NcVar Wy = dataFile.addVar("Wy", netCDF::ncDouble, idDim);
    netCDF::NcVar Wz = dataFile.addVar("Wz", netCDF::ncDouble, idDim);
    Wx.setCompression(true, true, 3);
    Wy.setCompression(true, true, 3);
    Wz.setCompression(true, true, 3);
    Wx.putVar(dvecx.data()); Wy.putVar(dvecy.data()); Wz.putVar(dvecz.data());
    // add T[0], T[1], T[2]
    for (int id = 0; id < SIZE; ++id) { dvecx[id] = particles[id].T[0]; dvecy[id] = particles[id].T[1]; dvecz[id] = particles[id].T[2]; }
    netCDF::NcVar Tx = dataFile.addVar("Tx", netCDF::ncDouble, idDim);
    netCDF::NcVar Ty = dataFile.addVar("Ty", netCDF::ncDouble, idDim);
    netCDF::NcVar Tz = dataFile.addVar("Tz", netCDF::ncDouble, idDim);
    Tx.setCompression(true, true, 3);
    Ty.setCompression(true, true, 3);
    Tz.setCompression(true, true, 3);
    Tx.putVar(dvecx.data()); Ty.putVar(dvecy.data()); Tz.putVar(dvecz.data());
    // add rad
    for (int id = 0; id < SIZE; ++id) { dvecx[id] = particles[id].rad; }
    netCDF::NcVar rad = dataFile.addVar("rad", netCDF::ncDouble, idDim);
    rad.setCompression(true, true, 3);
    rad.putVar(dvecx.data());
    // add mass
    for (int id = 0; id < SIZE; ++id) { dvecx[id] = particles[id].mass; }
    netCDF::NcVar mass = dataFile.addVar("mass", netCDF::ncDouble, idDim);
    mass.setCompression(true, true, 3);
    mass.putVar(dvecx.data());
    // add inerI
    for (int id = 0; id < SIZE; ++id) { dvecx[id] = particles[id].inerI; }
    netCDF::NcVar inerI = dataFile.addVar("inerI", netCDF::ncDouble, idDim);
    inerI.setCompression(true, true, 3);
    inerI.putVar(dvecx.data());
    // add nc
    for (int id = 0; id < SIZE; ++id) { ivec[id] = particles[id].nc; } 
    netCDF::NcVar nc = dataFile.addVar("nc", netCDF::ncInt, idDim);
    nc.setCompression(true, true, 3);
    nc.putVar(ivec.data());

    return 0;
  }
  catch(netCDF::exceptions::NcException & e) {
    e.what();
    return netCDF::NC_ERR;
  }
}

int write_contacts_netcdf(const DEM::Interaction::Interaction_t & contacts, const std::string & filename)
{
  std::vector<DEM::Contact> contacts_vec;
  contacts_vec.resize(contacts.size());
  int id = 0;
#if NEW_CONTACTS  
  for (auto & clist : contacts) {
    for (auto it = clist.begin(); it != clist.end(); ++it) {
      contacts_vec[id] = it->second; 
    }
  }
#else
  for (auto it = contacts.begin(); it != contacts.end(); ++it, ++id) { 
    contacts_vec[id] = it->second; 
  }
#endif
  return write_contacts_netcdf(contacts_vec, filename);
}

int write_contacts_netcdf(const std::vector<DEM::Contact> & contacts, const std::string & filename)
{
  // The default behavior of the C++ API is to throw an exception i
  // an error occurs. A try catch block is necessary.
  try {
    // local arrays to store individual data
    static std::vector<int> ivec;
    static std::vector<double> dvecx, dvecy, dvecz;
 
    // get size and create the arrays to store the data
    const int SIZE = contacts.size();
    ivec.resize(SIZE, 0);
    dvecx.resize(SIZE, 0.0);
    dvecy.resize(SIZE, 0.0);
    dvecz.resize(SIZE, 0.0);
    
    // create data file
    netCDF::NcFile dataFile(filename, netCDF::NcFile::replace);

    // create dimension id
    const netCDF::NcDim idDim = dataFile.addDim("id", SIZE);
    
    // add uid1_
    for (int id = 0; id < SIZE; ++id) { ivec[id] = contacts[id].uid1_; }
    netCDF::NcVar uid1_ = dataFile.addVar("uid1_", netCDF::ncInt, idDim);
    uid1_.setCompression(true, true, 3);
    if (SIZE > 0) uid1_.putVar(ivec.data());

    // add uid2_
    for (int id = 0; id < SIZE; ++id) { ivec[id] = contacts[id].uid2_; }
    netCDF::NcVar uid2_ = dataFile.addVar("uid2_", netCDF::ncInt, idDim);
    uid2_.setCompression(true, true, 3);
    if (SIZE > 0) uid2_.putVar(ivec.data());

    // add uid_
    for (int id = 0; id < SIZE; ++id) { ivec[id] = contacts[id].uid_; }
    netCDF::NcVar uid_ = dataFile.addVar("uid_", netCDF::ncInt, idDim);
    uid_.setCompression(true, true, 3);
    if (SIZE > 0) uid_.putVar(ivec.data()); 

    // add sliding_
    for (int id = 0; id < SIZE; ++id) { ivec[id] = contacts[id].sliding_; }
    netCDF::NcVar sliding_ = dataFile.addVar("sliding_", netCDF::ncInt, idDim);
    sliding_.setCompression(true, true, 3);
    if (SIZE > 0) uid_.putVar(ivec.data()); 

    // add delta 
    for (int id = 0; id < SIZE; ++id) { dvecx[id] = contacts[id].delta_; }
    netCDF::NcVar delta_ = dataFile.addVar("delta_", netCDF::ncDouble, idDim);
    delta_.setCompression(true, true, 3);
    if (SIZE > 0) delta_.putVar(dvecx.data());

    // add normFn
    for (int id = 0; id < SIZE; ++id) { dvecx[id] = contacts[id].normFn_; }
    netCDF::NcVar normFn_ = dataFile.addVar("normFn_", netCDF::ncDouble, idDim);
    normFn_.setCompression(true, true, 3);
    if (SIZE > 0) normFn_.putVar(dvecx.data());

    // add residual_ status
    for (int id = 0; id < SIZE; ++id) { ivec[id] = contacts[id].residual_; }
    netCDF::NcVar residual_ = dataFile.addVar("residual_", netCDF::ncInt, idDim);
    residual_.setCompression(true, true, 3);
    if (SIZE > 0) residual_.putVar(ivec.data());

    // add Normal_
    for (int id = 0; id < SIZE; ++id) { dvecx[id] = contacts[id].Normal_[0]; dvecy[id] = contacts[id].Normal_[1]; dvecz[id] = contacts[id].Normal_[2]; }
    netCDF::NcVar Normal_x = dataFile.addVar("Normal_x", netCDF::ncDouble, idDim);
    netCDF::NcVar Normal_y = dataFile.addVar("Normal_y", netCDF::ncDouble, idDim);
    netCDF::NcVar Normal_z = dataFile.addVar("Normal_z", netCDF::ncDouble, idDim);
    Normal_x.setCompression(true, true, 3);
    Normal_y.setCompression(true, true, 3);
    Normal_z.setCompression(true, true, 3);
    if (SIZE > 0) Normal_x.putVar(dvecx.data());
    if (SIZE > 0) Normal_y.putVar(dvecy.data());
    if (SIZE > 0) Normal_z.putVar(dvecz.data());
    // add Normal force
    for (int id = 0; id < SIZE; ++id) { 
      const Vec3d_t Fn(contacts[id].normFn_*contacts[id].Normal_);
      dvecx[id] = Fn[0]; dvecy[id] = Fn[1]; dvecz[id] = Fn[2]; 
    }
    netCDF::NcVar Fn_x = dataFile.addVar("Fn_x", netCDF::ncDouble, idDim);
    netCDF::NcVar Fn_y = dataFile.addVar("Fn_y", netCDF::ncDouble, idDim);
    netCDF::NcVar Fn_z = dataFile.addVar("Fn_z", netCDF::ncDouble, idDim);
    Fn_x.setCompression(true, true, 3);
    Fn_y.setCompression(true, true, 3);
    Fn_z.setCompression(true, true, 3);
    if (SIZE > 0) Fn_x.putVar(dvecx.data());
    if (SIZE > 0) Fn_y.putVar(dvecy.data());
    if (SIZE > 0) Fn_z.putVar(dvecz.data());
    // add Ft_
    for (int id = 0; id < SIZE; ++id) { dvecx[id] = contacts[id].Ft_[0]; dvecy[id] = contacts[id].Ft_[1]; dvecz[id] = contacts[id].Ft_[2]; }
    netCDF::NcVar Ft_x = dataFile.addVar("Ft_x", netCDF::ncDouble, idDim);
    netCDF::NcVar Ft_y = dataFile.addVar("Ft_y", netCDF::ncDouble, idDim);
    netCDF::NcVar Ft_z = dataFile.addVar("Ft_z", netCDF::ncDouble, idDim);
    Ft_x.setCompression(true, true, 3);
    Ft_y.setCompression(true, true, 3);
    Ft_z.setCompression(true, true, 3);
    if (SIZE > 0) Ft_x.putVar(dvecx.data());
    if (SIZE > 0) Ft_y.putVar(dvecy.data());
    if (SIZE > 0) Ft_z.putVar(dvecz.data());
    // add T_
    for (int id = 0; id < SIZE; ++id) { dvecx[id] = contacts[id].T_[0]; dvecy[id] = contacts[id].T_[1]; dvecz[id] = contacts[id].T_[2]; }
    netCDF::NcVar T_x = dataFile.addVar("T_x", netCDF::ncDouble, idDim);
    netCDF::NcVar T_y = dataFile.addVar("T_y", netCDF::ncDouble, idDim);
    netCDF::NcVar T_z = dataFile.addVar("T_z", netCDF::ncDouble, idDim);
    T_x.setCompression(true, true, 3);
    T_y.setCompression(true, true, 3);
    T_z.setCompression(true, true, 3);
    if (SIZE > 0) T_x.putVar(dvecx.data());
    if (SIZE > 0) T_y.putVar(dvecy.data());
    if (SIZE > 0) T_z.putVar(dvecz.data());
    // add SpringTorsion_
    for (int id = 0; id < SIZE; ++id) { dvecx[id] = contacts[id].SpringTorsion_[0]; dvecy[id] = contacts[id].SpringTorsion_[1]; dvecz[id] = contacts[id].SpringTorsion_[2]; }
    netCDF::NcVar SpringTorsion_x = dataFile.addVar("SpringTorsion_x", netCDF::ncDouble, idDim);
    netCDF::NcVar SpringTorsion_y = dataFile.addVar("SpringTorsion_y", netCDF::ncDouble, idDim);
    netCDF::NcVar SpringTorsion_z = dataFile.addVar("SpringTorsion_z", netCDF::ncDouble, idDim);
    SpringTorsion_x.setCompression(true, true, 3);
    SpringTorsion_y.setCompression(true, true, 3);
    SpringTorsion_z.setCompression(true, true, 3);
    if (SIZE > 0) SpringTorsion_x.putVar(dvecx.data());
    if (SIZE > 0) SpringTorsion_y.putVar(dvecy.data());
    if (SIZE > 0) SpringTorsion_z.putVar(dvecz.data());
    // add SpringSlide_
    for (int id = 0; id < SIZE; ++id) { dvecx[id] = contacts[id].SpringSlide_[0]; dvecy[id] = contacts[id].SpringSlide_[1]; dvecz[id] = contacts[id].SpringSlide_[2]; }
    netCDF::NcVar SpringSlide_x = dataFile.addVar("SpringSlide_x", netCDF::ncDouble, idDim);
    netCDF::NcVar SpringSlide_y = dataFile.addVar("SpringSlide_y", netCDF::ncDouble, idDim);
    netCDF::NcVar SpringSlide_z = dataFile.addVar("SpringSlide_z", netCDF::ncDouble, idDim);
    SpringSlide_x.setCompression(true, true, 3);
    SpringSlide_y.setCompression(true, true, 3);
    SpringSlide_z.setCompression(true, true, 3);
    if (SIZE > 0) SpringSlide_x.putVar(dvecx.data());
    if (SIZE > 0) SpringSlide_y.putVar(dvecy.data());
    if (SIZE > 0) SpringSlide_z.putVar(dvecz.data());
    // add SpringRolling_
    for (int id = 0; id < SIZE; ++id) { dvecx[id] = contacts[id].SpringRolling_[0]; dvecy[id] = contacts[id].SpringRolling_[1]; dvecz[id] = contacts[id].SpringRolling_[2]; }
    netCDF::NcVar SpringRolling_x = dataFile.addVar("SpringRolling_x", netCDF::ncDouble, idDim);
    netCDF::NcVar SpringRolling_y = dataFile.addVar("SpringRolling_y", netCDF::ncDouble, idDim);
    netCDF::NcVar SpringRolling_z = dataFile.addVar("SpringRolling_z", netCDF::ncDouble, idDim);
    SpringRolling_x.setCompression(true, true, 3);
    SpringRolling_y.setCompression(true, true, 3);
    SpringRolling_z.setCompression(true, true, 3);
    if (SIZE > 0) SpringRolling_x.putVar(dvecx.data());
    if (SIZE > 0) SpringRolling_y.putVar(dvecy.data());
    if (SIZE > 0) SpringRolling_z.putVar(dvecz.data());
    return 0;
  }
  catch(netCDF::exceptions::NcException & e) {
    e.what();
    return netCDF::NC_ERR;
  }
}


//----------------------------------------------------------------------------------------
// READING
//----------------------------------------------------------------------------------------

int read_particles_netcdf(const std::string & filename, std::vector<Particle> & particles) {
  // The default behavior of the C++ API is to throw an exception i
  // an error occurs. A try catch block is necessary.
  try {
    // create data file
    netCDF::NcFile dataFile(filename, netCDF::NcFile::read);
    
    // dimension
    netCDF::NcDim idDim = dataFile.getDim("id");
    const int SIZE = idDim.getSize();
    particles.resize(SIZE);
    if ( 0 >= SIZE ) { // no particles to read
      std::cerr << "ERROR: No particles to read from " << filename << std::endl;
      std::cerr << "At least 6 particles should be present (the six plane walls)." << filename << std::endl;
      return 1; // this is an error since at least 6 particles (the walls) should be read  
    }
    else {
      std::cout << "Total particles to read =  " << SIZE << std::endl; 
    }
    
    // local arrays to store individual data
    static std::vector<int> ivec;
    static std::vector<double> dvecx, dvecy, dvecz;
   
    // get size and create the arrays to store the data
    ivec.resize(SIZE, 0);
    dvecx.resize(SIZE, 0.0);
    dvecy.resize(SIZE, 0.0);
    dvecz.resize(SIZE, 0.0);
  
    // get uid
    netCDF::NcVar uid = dataFile.getVar("uid"); if(uid.isNull()) return netCDF::NC_ERR;
    uid.getVar(ivec.data());
    for (int id = 0; id < SIZE; ++id) { particles[id].uid = ivec[id]; } 
    // get alive
    netCDF::NcVar alive = dataFile.getVar("alive"); if(alive.isNull()) return netCDF::NC_ERR;
    alive.getVar(ivec.data());
    for (int id = 0; id < SIZE; ++id) { particles[id].alive = ivec[id]; }
    // get type
    netCDF::NcVar type = dataFile.getVar("type"); if(type.isNull()) return netCDF::NC_ERR;
    type.getVar(ivec.data());
    for (int id = 0; id < SIZE; ++id) { particles[id].type = ivec[id]; }
    // get bcTag
    netCDF::NcVar bcTag = dataFile.getVar("bcTag"); if(bcTag.isNull()) return netCDF::NC_ERR;
    bcTag.getVar(ivec.data());
    for (int id = 0; id < SIZE; ++id) { particles[id].bcTag = ivec[id]; }
    // get groupTag
    netCDF::NcVar groupTag = dataFile.getVar("groupTag"); if(groupTag.isNull()) return netCDF::NC_ERR;
    groupTag.getVar(ivec.data());
    for (int id = 0; id < SIZE; ++id) { particles[id].groupTag = ivec[id]; }
    // get materialTag
    netCDF::NcVar materialTag = dataFile.getVar("materialTag"); if(materialTag.isNull()) return netCDF::NC_ERR;
    materialTag.getVar(ivec.data());
    for (int id = 0; id < SIZE; ++id) { particles[id].materialTag = ivec[id]; }
    // get R0[0], R0[1], R0[2]
    netCDF::NcVar R0x = dataFile.getVar("R0x"); if(R0x.isNull()) return netCDF::NC_ERR;
    netCDF::NcVar R0y = dataFile.getVar("R0y"); if(R0y.isNull()) return netCDF::NC_ERR;
    netCDF::NcVar R0z = dataFile.getVar("R0z"); if(R0z.isNull()) return netCDF::NC_ERR;
    R0x.getVar(dvecx.data()); R0y.getVar(dvecy.data()); R0z.getVar(dvecz.data());
    for (int id = 0; id < SIZE; ++id) { particles[id].R0[0] = dvecx[id]; particles[id].R0[1] = dvecy[id]; particles[id].R0[2] = dvecz[id]; }
    // get R[0], R[1], R[2]
    netCDF::NcVar Rx = dataFile.getVar("Rx"); if(Rx.isNull()) return netCDF::NC_ERR;
    netCDF::NcVar Ry = dataFile.getVar("Ry"); if(Ry.isNull()) return netCDF::NC_ERR;
    netCDF::NcVar Rz = dataFile.getVar("Rz"); if(Rz.isNull()) return netCDF::NC_ERR;
    Rx.getVar(dvecx.data()); Ry.getVar(dvecy.data()); Rz.getVar(dvecz.data());
    for (int id = 0; id < SIZE; ++id) { particles[id].R[0] = dvecx[id]; particles[id].R[1] = dvecy[id]; particles[id].R[2] = dvecz[id]; }
    // get V[0], V[1], V[2]
    netCDF::NcVar Vx = dataFile.getVar("Vx"); if(Vx.isNull()) return netCDF::NC_ERR;
    netCDF::NcVar Vy = dataFile.getVar("Vy"); if(Vy.isNull()) return netCDF::NC_ERR;
    netCDF::NcVar Vz = dataFile.getVar("Vz"); if(Vz.isNull()) return netCDF::NC_ERR;
    Vx.getVar(dvecx.data()); Vy.getVar(dvecy.data()); Vz.getVar(dvecz.data());
    for (int id = 0; id < SIZE; ++id) { particles[id].V[0] = dvecx[id]; particles[id].V[1] = dvecy[id]; particles[id].V[2] = dvecz[id]; }
    // get F[0], F[1], F[2]
    netCDF::NcVar Fx = dataFile.getVar("Fx"); if(Fx.isNull()) return netCDF::NC_ERR;
    netCDF::NcVar Fy = dataFile.getVar("Fy"); if(Fy.isNull()) return netCDF::NC_ERR;
    netCDF::NcVar Fz = dataFile.getVar("Fz"); if(Fz.isNull()) return netCDF::NC_ERR;
    Fx.getVar(dvecx.data()); Fy.getVar(dvecy.data()); Fz.getVar(dvecz.data());
    for (int id = 0; id < SIZE; ++id) { particles[id].F[0] = dvecx[id]; particles[id].F[1] = dvecy[id]; particles[id].F[2] = dvecz[id]; }
    // get F_fixed[0], F_fixed[1], F_fixed[2]
    netCDF::NcVar F_fixedx = dataFile.getVar("F_fixedx"); if(F_fixedx.isNull()) return netCDF::NC_ERR;
    netCDF::NcVar F_fixedy = dataFile.getVar("F_fixedy"); if(F_fixedy.isNull()) return netCDF::NC_ERR;
    netCDF::NcVar F_fixedz = dataFile.getVar("F_fixedz"); if(F_fixedz.isNull()) return netCDF::NC_ERR;
    F_fixedx.getVar(dvecx.data()); F_fixedy.getVar(dvecy.data()); F_fixedz.getVar(dvecz.data());
    for (int id = 0; id < SIZE; ++id) { particles[id].F_fixed[0] = dvecx[id]; particles[id].F_fixed[1] = dvecy[id]; particles[id].F_fixed[2] = dvecz[id]; }
    // get PHI[0], PHI[1], PHI[2]
    netCDF::NcVar PHIx = dataFile.getVar("PHIx"); if(PHIx.isNull()) return netCDF::NC_ERR;
    netCDF::NcVar PHIy = dataFile.getVar("PHIy"); if(PHIy.isNull()) return netCDF::NC_ERR;
    netCDF::NcVar PHIz = dataFile.getVar("PHIz"); if(PHIz.isNull()) return netCDF::NC_ERR;
    PHIx.getVar(dvecx.data()); PHIy.getVar(dvecy.data()); PHIz.getVar(dvecz.data());
    for (int id = 0; id < SIZE; ++id) { particles[id].PHI[0] = dvecx[id]; particles[id].PHI[1] = dvecy[id]; particles[id].PHI[2] = dvecz[id]; }
    // get W[0], W[1], W[2]
    netCDF::NcVar Wx = dataFile.getVar("Wx"); if(Wx.isNull()) return netCDF::NC_ERR;
    netCDF::NcVar Wy = dataFile.getVar("Wy"); if(Wy.isNull()) return netCDF::NC_ERR;
    netCDF::NcVar Wz = dataFile.getVar("Wz"); if(Wz.isNull()) return netCDF::NC_ERR;
    Wx.getVar(dvecx.data()); Wy.getVar(dvecy.data()); Wz.getVar(dvecz.data());
    for (int id = 0; id < SIZE; ++id) { particles[id].W[0] = dvecx[id]; particles[id].W[1] = dvecy[id]; particles[id].W[2] = dvecz[id]; }
    // get T[0], T[1], T[2]
    netCDF::NcVar Tx = dataFile.getVar("Tx"); if(Tx.isNull()) return netCDF::NC_ERR;
    netCDF::NcVar Ty = dataFile.getVar("Ty"); if(Ty.isNull()) return netCDF::NC_ERR;
    netCDF::NcVar Tz = dataFile.getVar("Tz"); if(Tz.isNull()) return netCDF::NC_ERR;
    Tx.getVar(dvecx.data()); Ty.getVar(dvecy.data()); Tz.getVar(dvecz.data());
    for (int id = 0; id < SIZE; ++id) { particles[id].T[0] = dvecx[id]; particles[id].T[1] = dvecy[id]; particles[id].T[2] = dvecz[id]; }
    // get rad
    netCDF::NcVar rad = dataFile.getVar("rad"); if(rad.isNull()) return netCDF::NC_ERR;
    rad.getVar(dvecx.data());
    for (int id = 0; id < SIZE; ++id) { particles[id].rad = dvecx[id]; }
    // get mass
    netCDF::NcVar mass = dataFile.getVar("mass"); if(mass.isNull()) return netCDF::NC_ERR;
    mass.getVar(dvecx.data());
    for (int id = 0; id < SIZE; ++id) { particles[id].mass = dvecx[id]; }
    // get inerI
    netCDF::NcVar inerI = dataFile.getVar("inerI"); if(inerI.isNull()) return netCDF::NC_ERR;
    inerI.getVar(dvecx.data());
    for (int id = 0; id < SIZE; ++id) { particles[id].inerI = dvecx[id]; }
    // get nc
    netCDF::NcVar nc = dataFile.getVar("nc"); if(nc.isNull()) return netCDF::NC_ERR;
    nc.getVar(ivec.data());
    for (int id = 0; id < SIZE; ++id) { particles[id].nc = ivec[id]; } 

    return 0;
  }
  catch(netCDF::exceptions::NcException & e) {
    e.what();
    return netCDF::NC_ERR;
  }
}

int read_contacts_netcdf(const std::string & filename, std::vector<DEM::Contact> & contacts) 
{
  // The default behavior of the C++ API is to throw an exception i
  // an error occurs. A try catch block is necessary.
  try {
    UTIL::print_start("Reading contacts with netcdf and writing to vector");

    // create data file
    netCDF::NcFile dataFile(filename, netCDF::NcFile::read);
    
    // dimension
    netCDF::NcDim idDim = dataFile.getDim("id");
    const int SIZE = idDim.getSize();
    contacts.resize(SIZE);
    if ( 0 == SIZE ) { // no contacts to read
      std::cout << "No contacts to read from " << filename << std::endl;
      return 0; 
    }
    for (int id = 0; id < SIZE; ++id) { contacts[id].pre_reset(); } 

    // local arrays to store individual data
    static std::vector<int> ivec;
    static std::vector<double> dvecx, dvecy, dvecz;
   
    // get size and create the arrays to store the data
    ivec.resize(SIZE, 0);
    dvecx.resize(SIZE, 0.0);
    dvecy.resize(SIZE, 0.0);
    dvecz.resize(SIZE, 0.0);
    
    // get uid1_
    netCDF::NcVar uid1_ = dataFile.getVar("uid1_"); if(uid1_.isNull()) return netCDF::NC_ERR;
    uid1_.getVar(ivec.data());
    for (int id = 0; id < SIZE; ++id) { contacts[id].uid1_ = ivec[id]; } 
    // get uid2_
    netCDF::NcVar uid2_ = dataFile.getVar("uid2_"); if(uid2_.isNull()) return netCDF::NC_ERR;
    uid2_.getVar(ivec.data());
    for (int id = 0; id < SIZE; ++id) { contacts[id].uid2_ = ivec[id]; } 
    // get uid_
    netCDF::NcVar uid_ = dataFile.getVar("uid_"); if(uid_.isNull()) return netCDF::NC_ERR;
    uid_.getVar(ivec.data());
    for (int id = 0; id < SIZE; ++id) { contacts[id].uid_ = ivec[id]; } 
    // get delta_
    netCDF::NcVar delta_ = dataFile.getVar("delta_"); if(delta_.isNull()) return netCDF::NC_ERR;
    delta_.getVar(dvecx.data());
    for (int id = 0; id < SIZE; ++id) { contacts[id].delta_ = dvecx[id]; } 
    // get normFn_
    netCDF::NcVar normFn_ = dataFile.getVar("normFn_"); if(normFn_.isNull()) return netCDF::NC_ERR;
    normFn_.getVar(dvecx.data());
    for (int id = 0; id < SIZE; ++id) { contacts[id].normFn_ = dvecx[id]; } 
    // get residual_
    netCDF::NcVar residual_ = dataFile.getVar("residual_"); 
    // allows to read old data with no residual data. Default, if not present , is false (all are initial)
    if(residual_.isNull()) std::fill(ivec.begin(), ivec.end(), 0); 
    else residual_.getVar(ivec.data());
    for (int id = 0; id < SIZE; ++id) { contacts[id].residual_ = ivec[id]; }
    // get Normal_[0], Normal_[1], Normal_[2]
    netCDF::NcVar Normal_x = dataFile.getVar("Normal_x"); if(Normal_x.isNull()) return netCDF::NC_ERR;
    netCDF::NcVar Normal_y = dataFile.getVar("Normal_y"); if(Normal_y.isNull()) return netCDF::NC_ERR;
    netCDF::NcVar Normal_z = dataFile.getVar("Normal_z"); if(Normal_z.isNull()) return netCDF::NC_ERR;
    Normal_x.getVar(dvecx.data()); Normal_y.getVar(dvecy.data()); Normal_z.getVar(dvecz.data());
    for (int id = 0; id < SIZE; ++id) { contacts[id].Normal_[0] = dvecx[id]; contacts[id].Normal_[1] = dvecy[id]; contacts[id].Normal_[2] = dvecz[id]; }
    // get Ft_[0], Ft_[1], Ft_[2]
    netCDF::NcVar Ft_x = dataFile.getVar("Ft_x"); if(Ft_x.isNull()) return netCDF::NC_ERR;
    netCDF::NcVar Ft_y = dataFile.getVar("Ft_y"); if(Ft_y.isNull()) return netCDF::NC_ERR;
    netCDF::NcVar Ft_z = dataFile.getVar("Ft_z"); if(Ft_z.isNull()) return netCDF::NC_ERR;
    Ft_x.getVar(dvecx.data()); Ft_y.getVar(dvecy.data()); Ft_z.getVar(dvecz.data());
    for (int id = 0; id < SIZE; ++id) { contacts[id].Ft_[0] = dvecx[id]; contacts[id].Ft_[1] = dvecy[id]; contacts[id].Ft_[2] = dvecz[id]; }
    // get T_[0], T_[1], T_[2]
    netCDF::NcVar T_x = dataFile.getVar("T_x"); if(T_x.isNull()) return netCDF::NC_ERR;
    netCDF::NcVar T_y = dataFile.getVar("T_y"); if(T_y.isNull()) return netCDF::NC_ERR;
    netCDF::NcVar T_z = dataFile.getVar("T_z"); if(T_z.isNull()) return netCDF::NC_ERR;
    T_x.getVar(dvecx.data()); T_y.getVar(dvecy.data()); T_z.getVar(dvecz.data());
    for (int id = 0; id < SIZE; ++id) { contacts[id].T_[0] = dvecx[id]; contacts[id].T_[1] = dvecy[id]; contacts[id].T_[2] = dvecz[id]; }
    // get SpringTorsion_[0], SpringTorsion_[1], SpringTorsion_[2]
    netCDF::NcVar SpringTorsion_x = dataFile.getVar("SpringTorsion_x"); if(SpringTorsion_x.isNull()) return netCDF::NC_ERR;
    netCDF::NcVar SpringTorsion_y = dataFile.getVar("SpringTorsion_y"); if(SpringTorsion_y.isNull()) return netCDF::NC_ERR;
    netCDF::NcVar SpringTorsion_z = dataFile.getVar("SpringTorsion_z"); if(SpringTorsion_z.isNull()) return netCDF::NC_ERR;
    SpringTorsion_x.getVar(dvecx.data()); SpringTorsion_y.getVar(dvecy.data()); SpringTorsion_z.getVar(dvecz.data());
    for (int id = 0; id < SIZE; ++id) { contacts[id].SpringTorsion_[0] = dvecx[id]; contacts[id].SpringTorsion_[1] = dvecy[id]; contacts[id].SpringTorsion_[2] = dvecz[id]; }
    // get SpringSlide_[0], SpringSlide_[1], SpringSlide_[2]
    netCDF::NcVar SpringSlide_x = dataFile.getVar("SpringSlide_x"); if(SpringSlide_x.isNull()) return netCDF::NC_ERR;
    netCDF::NcVar SpringSlide_y = dataFile.getVar("SpringSlide_y"); if(SpringSlide_y.isNull()) return netCDF::NC_ERR;
    netCDF::NcVar SpringSlide_z = dataFile.getVar("SpringSlide_z"); if(SpringSlide_z.isNull()) return netCDF::NC_ERR;
    SpringSlide_x.getVar(dvecx.data()); SpringSlide_y.getVar(dvecy.data()); SpringSlide_z.getVar(dvecz.data());
    for (int id = 0; id < SIZE; ++id) { contacts[id].SpringSlide_[0] = dvecx[id]; contacts[id].SpringSlide_[1] = dvecy[id]; contacts[id].SpringSlide_[2] = dvecz[id]; }
    // get SpringRolling_[0], SpringRolling_[1], SpringRolling_[2]
    netCDF::NcVar SpringRolling_x = dataFile.getVar("SpringRolling_x"); if(SpringRolling_x.isNull()) return netCDF::NC_ERR;
    netCDF::NcVar SpringRolling_y = dataFile.getVar("SpringRolling_y"); if(SpringRolling_y.isNull()) return netCDF::NC_ERR;
    netCDF::NcVar SpringRolling_z = dataFile.getVar("SpringRolling_z"); if(SpringRolling_z.isNull()) return netCDF::NC_ERR;
    SpringRolling_x.getVar(dvecx.data()); SpringRolling_y.getVar(dvecy.data()); SpringRolling_z.getVar(dvecz.data());
    for (int id = 0; id < SIZE; ++id) { contacts[id].SpringRolling_[0] = dvecx[id]; contacts[id].SpringRolling_[1] = dvecy[id]; contacts[id].SpringRolling_[2] = dvecz[id]; }

    UTIL::print_done("Reading contacts with netcdf and writing to vector");

    return 0;
  }
  catch(netCDF::exceptions::NcException & e) {
    e.what();
    return netCDF::NC_ERR;
  }
}

int read_contacts_netcdf(const std::string & filename, DEM::Interaction::Interaction_t & contacts) 
{
  UTIL::print_start("Reading contacts with netcdf and saving to interaction contacts");
  print_log("Filename = ", filename); 
  // create a vector and read to the vector
  static std::vector<DEM::Contact> contacts_read;
  int status = read_contacts_netcdf(filename, contacts_read);
  if (0 != status) {
    std::cerr << "ERROR READING CONTACT DATA from file " << filename << std::endl;
    return status;
  }
  
  // copy all the vector contacts into the interaction map, and also the iuid
#if NEW_CONTACTS
  for (auto & clist : contacts) 
    clist.clear();
#else
  contacts.clear();
#endif
  const int ncontacts = contacts_read.size();
  for (int id = 0; id < ncontacts; ++id) {
    contacts_read[id].sliding_ = false; // Not sliding until computed 
    contacts_read[id].updated_ = true; // They are active by default 
#if NEW_CONTACTS  
    const int ii = contacts_read[id].uid1_;
    const int jj = contacts_read[id].uid2_;
    const int current_size = contacts.size(); 
    if (current_size < std::max(ii, jj)) contacts.resize(std::max(ii, jj));
    contacts[ii][jj] = contacts_read[id];
#else
    long uid = contacts_read[id].uid_;
    ASSERT(contacts.end() == contacts.find(uid)); // assert non-repeated keys
    contacts[uid] = contacts_read[id];
#endif
  }
  contacts_read.clear(); contacts_read.shrink_to_fit();


  UTIL::print_done("Reading contacts with netcdf and writing to interaction contacts.");

  return 0;
}
#endif // WITH_NETCDF

