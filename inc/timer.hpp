#ifndef __TIMER__
#define __TIMER__

#include <iostream>
#include <string>
#include <iomanip>
//#include <sys/time.h> // will not work in windows
#include <chrono> // will not work in windows without c++11

class Timer {
public: // typedef
  typedef std::chrono::high_resolution_clock clock;
  typedef std::chrono::microseconds microseconds;
  
public: // methods
  Timer() : elapsed_time_(0.0), name_("No name timer") {};
  ~Timer() { print(std::cout); };

  void set_name(const char * name) { name_ = name; }
  void set_name(const std::string & name) { name_ = name; }

  void start_timing(const bool & print_info = false) { 
    //if (print_info) { std::cout << "TIMER STARTED. Start_time = " << t1_.tv_sec << std::endl; } 
    t1_ = clock::now(); 
    if (print_info) { std::cout << "TIMER STARTED. " << std::endl; } 
  } ;
  void stop_timing() { 
    //elapsed_time_ += (t2_.tv_sec - t1_.tv_sec); elapsed_time_ += (t2_.tv_usec - t1_.tv_usec)/1000000.0; 
    t2_ = clock::now(); 
    elapsed_time_ += (std::chrono::duration_cast<microseconds>(t2_ - t1_)).count()/1000000.0;
  } ;
  double elapsed_time() { return elapsed_time_; };
  void reset() { elapsed_time_ = 0; t2_ = t1_; } ;
  void print(std::ostream & os = std::cout) { 
    os.setf(std::ios::scientific); 
    os << "# elapsed : " << std::setw(25) << std::setprecision(16) << elapsed_time()
       << " sec -> " << std::setw(25) << name_ << std::endl; 
  }
  
private:
  //timeval t1_, t2_;
  clock::time_point t1_, t2_;
  double elapsed_time_;
  std::string name_;
};

#endif // __TIMER__
