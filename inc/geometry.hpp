# pragma once

template <class ParticleList_t>
void 
walls_limits(const ParticleList_t & bodies, double wmin[3], double wmax[3]) {
  const auto ngrains = bodies.size() - 6; 
  wmin[0] = bodies[ngrains + 2].R[0]; // LEFT
  wmin[1] = bodies[ngrains + 4].R[1]; // BACK
  wmin[2] = bodies[ngrains + 0].R[2]; // BOTTOM
  wmax[0] = bodies[ngrains + 3].R[0]; // RIGHT
  wmax[1] = bodies[ngrains + 5].R[1]; // FRONT
  wmax[2] = bodies[ngrains + 1].R[2]; // TOP
}

template <class ParticleList_t>
double 
box_volume(const ParticleList_t & bodies) 
{
  double wmin[3], wmax[3];
  walls_limits(bodies, wmin, wmax);
  return (wmax[0] - wmin[0])*(wmax[1] - wmin[1])*(wmax[2] - wmin[2]);
}

