#include "postpro_base.hpp"

namespace DEM {
    
  //--------------------------------------------------------------------------
  // Read the configuration file, exits if error
  //--------------------------------------------------------------------------
  Postpro::Postpro()
  {
    wmin_[0] = wmin_[1] = wmin_[2] = wmax_[0] = wmax_[1] = wmax_[2] = 0.0; 
    read_config();
  }

  void Postpro::read_config(void)
  {
    UTIL::print_start("Reading postpro and related config");
    
    // postpro config
    print_log("Reading postpro config");
    YAML::Node config = YAML::LoadFile("INPUT/postpro_conf.yaml");
    if (config["tmin"]) { tmin_ = config["tmin"].as<double>(); }
    if (config["tmax"]) { tmax_ = config["tmax"].as<double>(); }
    if (config["SCREEN_FACTOR"]) { SCREEN_FACTOR_ = config["SCREEN_FACTOR"].as<double>(); }
    if (config["NC_MIN"]) { NC_MIN_ = config["NC_MIN"].as<int>(); }
    if (config["stride"]) { stride_ = config["stride"].as<int>(); ASSERT(stride_ > 0);}
    PROJECTION_EPS_ = config["PROJECTION_EPS"] ? config["PROJECTION_EPS"].as<double>() : 0.08716;
    VMIN_THRESHOLD_ = config["VMIN_THRESHOLD"] ? config["VMIN_THRESHOLD"].as<double>() : 0.98;

    // print read config
    print_log("Printing read postpro config");
    std::ofstream fout("OUTPUT/postpro_conf.yaml");
    fout << config;
    fout.close();
    
    // general config
    print_log("Reading general config");
    config = YAML::LoadFile("INPUT/general_conf.yaml");
    dim_ = config["dim"].as<int>();
    periodic_[0] = config["periodic"][0].as<int>();
    periodic_[1] = config["periodic"][1].as<int>();
    periodic_[2] = config["periodic"][2].as<int>();
    G_ = Vec3d_t(config["Gravity"][0].as<double>(), config["Gravity"][1].as<double>(), config["Gravity"][2].as<double>());

    // prepro config 
    print_log("Reading prepro config");
    config = YAML::LoadFile("INPUT/prepro_conf.yaml");
    ngrains_ = config["ngrains"].as<int>();

    UTIL::print_done("Reading postpro and related config");
  }

  //--------------------------------------------------------------------------
  // Returns a std::vector containing the filename std::string that match a given pattern
  //--------------------------------------------------------------------------
  void Postpro::file_names(const std::string & pattern, std::vector<std::string> & filenames)
  {
    const std::string msg = "Selecting filenames for postpro."; UTIL::print_start(msg);
    print_log("PATTERN = ", pattern);
    int status;
    std::stringstream command; command.str(""); command << "find OUTPUT -maxdepth 1 -type f -iname '" << pattern << "*' | sort > ./filelist.txt";
    status = std::system(command.str().c_str()); ASSERT( 0 == status );
    std::ifstream fin("./filelist.txt");
    filenames.clear(); filenames.shrink_to_fit();
    copy(std::istream_iterator<std::string>( fin ), std::istream_iterator<std::string>(), std::back_inserter(filenames));
    fin.close();
    status = std::system("rm -f ./filelist.txt"); ASSERT( 0 == status );
    // filter to select times between tmin and tmax
    print_log("POSTPRO: Number of files BEFORE selecting time window : ", filenames.size());
    std::vector<std::string> tmp(filenames);
    filenames.clear(); filenames.shrink_to_fit();
    //std::cout << tmin_ << std::endl;
    //std::cout << tmax_ << std::endl;
    for (std::vector<std::string>::iterator it = tmp.begin(); it != tmp.end(); ++it) {
      //std::cout << "time: " << get_time(*it, pattern) << std::endl;
      if (tmin_ <= get_time(*it, pattern) && get_time(*it, pattern) <= tmax_) {
	//std::cout << "selected " << std::endl;
	filenames.push_back(*it);
      }
    }
    print_log("POSTPRO: Number of files AFTER selecting time window : ", filenames.size());
    UTIL::print_done(msg);
  }

  //--------------------------------------------------------------------------
  // Reads the contacts from a given filename
  //--------------------------------------------------------------------------
  void Postpro::read_contacts(const std::string & filename)
  {
    // start
    std::string message = "Reading contacts from " + filename;
    UTIL::print_start(message);

    // clear old data
    contacts.clear(); contacts.shrink_to_fit();  // REVIEW: huge changes in memory every time step
    
    // READING
    ASSERT(std::string::npos != filename.find(CONTACTS_INFO));
#if !(WITH_NETCDF)
    // //// c++-style
    // std::ifstream fin(filename.c_str());
    // copy(std::istream_iterator<Contact>( fin ), std::istream_iterator<Contact>(), std::back_inserter(contacts));
    // fin.close();
    //// c-style
    Contact contact;
    std::FILE * fp = std::fopen(filename.c_str(), "r");
    while (!std::feof(fp)) {
      contact.read(fp);
      if (!std::feof(fp)) contacts.push_back(contact);
    }
    std::fclose(fp);
#else
    const auto status = read_contacts_netcdf(filename, contacts);
    if (netCDF::NC_ERR == status) print_error_and_exit("Cannot read file : " + filename);
#endif

    // info
    print_log("Number of read contacts: ", contacts.size());
    UTIL::print_done(message);
  }

  //--------------------------------------------------------------------------
  // Reads the particles info from a given filename
  //--------------------------------------------------------------------------
  void Postpro::read_particles(const std::string & filename)
  {
    // info start
    std::string message = "Reading particles info from " + filename;
    UTIL::print_start(message);
    // init
    ASSERT(std::string::npos != filename.find(BODIES_INFO));

    //// INPUT
    particle.clear(); particle.shrink_to_fit();              // REVIEW: are all particles re-read? clearing all time???
#if !(WITH_NETCDF)
    int nparticles, ngrains, nwalls;
    //// c++ style
    //std::ifstream fin(filename.c_str()); ASSERT(fin);
    //fin >> nparticles >> ngrains >> nwalls;
    //copy(std::istream_iterator<Particle>( fin ), std::istream_iterator<Particle>(), std::back_inserter(particle));
    //fin.close();
    // c style
    int count;
    std::FILE * fp = std::fopen(filename.c_str(), "r"); ASSERT(fp);
    count = std::fscanf(fp, "%d%d%d", &nparticles, &ngrains, &nwalls); ASSERT(3 == count);
    Particle body;
    for (int i = 0; i < nparticles; ++i) {
      body.read(fp);
      particle.push_back(body);
    }
    std::fclose(fp);
    // checks
    ASSERT(ngrains_ == ngrains);
    ASSERT(nparticles == particle.size());
#else
    const auto status = read_particles_netcdf(filename, particle);
    if (netCDF::NC_ERR == status) print_error_and_exit("Cannot read file : " + filename);
#endif

    // compute useful vars
    min_rad_ = max_rad_ = particle[0].rad;
    for (int id = 1; id < ngrains_; ++id) {
      if (particle[id].rad > max_rad_) max_rad_ = particle[id].rad;
      if (particle[id].rad < min_rad_) min_rad_ = particle[id].rad;
    }
    //d_ = min_rad_ + max_rad_; // = 2*(0.5*(min + max))
    d_ = 2*min_rad_; // = 2*(0.5*(min + max))
    d3_ = d_*d_*d_;
    DL_ = std::max(0.0, SCREEN_FACTOR_)*max_rad_;
    min_vol_ = M_PI*std::pow(min_rad_, dim_); if (3 == dim_) min_vol_ *= 4.0/3.0;
    max_vol_ = M_PI*std::pow(max_rad_, dim_); if (3 == dim_) max_vol_ *= 4.0/3.0;
    // end
    UTIL::print_done(message);
  }

  //--------------------------------------------------------------------------
  // Returns the time, extracted by removing .txt, .nc, ouput. and wathever moreToRemove is
  //--------------------------------------------------------------------------
  double Postpro::get_time(const std::string & name, const std::string & moreToRemove)
  {
    std::string tmp_name = name;
    std::vector<std::string> pattern(5); 
    pattern[0] = ".dat"; pattern[1] = ".txt"; pattern[2] = ".nc"; pattern[3] = "OUTPUT/"; pattern[4] = moreToRemove;
    const int npattern = pattern.size();
    for (int i = 0; i < npattern; ++i) {
      auto pos = tmp_name.find(pattern[i]); 
      while (std::string::npos != pos) { tmp_name.erase(pos, pattern[i].size()); pos = tmp_name.find(pattern[i]); }
    }
    //std::cout << tmp_name << std::endl;
    if (tmp_name == "LAST") {
      return -1;
    }
    return std::stod(tmp_name); 
  }

  //--------------------------------------------------------------------------
  // Selects the id of the particles away  screen_lenght from the walls
  // If there is periodicity on some direction, screen lenght is ignored on that 
  // Select if CENTER of particle is INSIDE domain
  //--------------------------------------------------------------------------
  void 
  Postpro::filter_inside_particles_id(const double & screen_length) 
  {
    if(0 == grain_ids_.size()) {
      for (int id = 0 ; id < ngrains_; ++id)
	grain_ids_.insert(id);
    }

    filtered_by_volume_ids_.clear();
    for(int id = 0; id < ngrains_; ++id) {
      filtered_by_volume_ids_.insert(id);
    }
    if (screen_length > 0) {
      for(int id = 0; id < ngrains_; ++id) {
	const Vec3d_t P = particle[id].R; 
	bool erase = false;
	if ( !periodic_[0] && (P[0] < particle[ngrains_ + LEFT].R[0]   + screen_length) ) erase = true;
	if ( !periodic_[0] && (P[0] > particle[ngrains_ + RIGHT].R[0]  - screen_length) ) erase = true; 
	if ( !periodic_[2] && (P[2] < particle[ngrains_ + BOTTOM].R[2] + screen_length) ) erase = true; 
	if ( !periodic_[2] && (P[2] > particle[ngrains_ + TOP].R[2]    - screen_length) ) erase = true; 
	if (3 == dim_) {
	  if ( !periodic_[1] && (P[1] < particle[ngrains_ + BACK].R[1]  + screen_length) ) erase = true;
	  if ( !periodic_[1] && (P[1] > particle[ngrains_ + FRONT].R[1] - screen_length) ) erase = true; 
	}
	if (true == erase) { 
	  filtered_by_volume_ids_.erase(id);	
	}
      }
    }
    print_log("Filtering with screen length =  ", screen_length);
    print_log("Total number of particles (from bodies): ", ngrains_);
    print_log("Internal particles     : ", filtered_by_volume_ids_.size());
    print_log("Non-internal particles : ", ngrains_ - filtered_by_volume_ids_.size());
  }

  //--------------------------------------------------------------------------
  // Select particles with number of contacts greater or equal to threshold
  //--------------------------------------------------------------------------
  void Postpro::filter_particles_nc_ge(const int nc_ref)
  {
    filtered_by_nc_ids_.clear();
    for(int id = 0; id < ngrains_; ++id) {
      if (particle[id].nc >= nc_ref) {
	filtered_by_nc_ids_.insert(id);
      }
    }
  }

  // location of Contact
  // The contact normal vector points from uid2 to uid1, where 
  // uid2 > uid1, and is assumed at the middle if the penetration
  Vec3d_t 
  Postpro::location(const Contact & contact)
  {
    //print_debug("particle.size() = ", particle.size());
    //print_debug("ngrains_        = ", ngrains_);
    //print_debug("&contact        = ", &contact);
    //print_debug("uid1_           = ", contact.uid1_);
    //print_debug("uid2_           = ", contact.uid2_);
#if DEBUG
    ASSERT(contact.uid1_ >= 0 && contact.uid1_ < ngrains_);
    ASSERT(contact.uid2_ >= 0 && contact.uid2_ < ngrains_ + DEM::NWALLS);
    ASSERT(int(particle.size() - DEM::NWALLS) == ngrains_);
#endif // DEBUG
    if (Particle::SPHERE == particle[contact.uid2_].type)
      return particle[contact.uid1_].R - contact.Normal_*(particle[contact.uid1_].rad - 0.5*contact.delta_);
    else // wall
      return particle[contact.uid1_].R - contact.Normal_*(particle[contact.uid1_].rad - 1.0*contact.delta_);
  }


  //--------------------------------------------------------------------------
  // Selects only the inside contacts
  // Inside means: the contact position is inside the selected modified (by dl) volume
  // Periodicity ignores dl limit
  //--------------------------------------------------------------------------
  void 
  Postpro::filter_inside_contacts(const double & screen_length)
  {
    std::string message = "Selecting inside contacts";
    UTIL::print_start(message);

    /*
    // old approach: using location of particles
    std::vector<Contact> inside_contacts;
    for (std::vector<Contact>::iterator it = contacts.begin(); it != contacts.end(); ++it ) {
      if (filtered_by_volume_ids_.end() != filtered_by_volume_ids_.find(it->uid1_) || 
    	  filtered_by_volume_ids_.end() != filtered_by_volume_ids_.find(it->uid2_) ) {
    	inside_contacts.push_back(*it);
      }
    }
    print_log("Using particles information (OLD APPROACH)");
    print_log("Original number of contacts = ", contacts.size());
    print_log("Final number of contacts    = ", inside_contacts.size());
    */

    // current approach: using location of contact
    inside_contacts_refs_.clear(); // inside_contacts_refs_.shrink_to_fit();
    // inside_contacts_refs_.reserve(contacts.size());
    double lmin[3] = {particle[ngrains_ + LEFT  ].R[0], particle[ngrains_ + BACK ].R[1], particle[ngrains_ + BOTTOM].R[2]};
    double lmax[3] = {particle[ngrains_ + RIGHT ].R[0], particle[ngrains_ + FRONT].R[1], particle[ngrains_ + TOP   ].R[2]};
    for (auto dir = 0; dir <= 2; ++dir) { if (false == periodic_[dir]) { lmin[dir] += screen_length; lmax[dir] -= screen_length;} };
    for (auto & contact : contacts) {
      Vec3d_t P = location(contact);
      const double eps = 1.0e-08;
      if ( UTIL::is_inside(P[0], lmin[0], lmax[0], eps) && UTIL::is_inside(P[2], lmin[2], lmax[2], eps) ) {
	if (2 == dim_ || (3 == dim_ && UTIL::is_inside(P[1], lmin[1], lmax[1], eps)) ) 
	  inside_contacts_refs_.push_back(std::ref(contact));
      }
    }
    print_log("Using contact location: ");
    print_log("Original number of contacts = ", contacts.size());
    print_log("Final number of contacts    = ", inside_contacts_refs_.size());
    
    UTIL::print_done(message);
  }
  
} // namespace DEM

