#include "workspace_dem.hpp"
#include "walls_normalvectors.hpp"

namespace DEM {

  
  //------------------------------------------------------
  // INIT
  //------------------------------------------------------
  WorkspaceDEM::WorkspaceDEM(void)
  {
    #pragma omp parallel
    {
      if(0 == omp_get_thread_num()) {
	std::cout << "# OMP num threads = " << omp_get_num_threads() << "\n";
	//std::cin.get();
      }
    }

    // check if the simulation has not started yet
    std::ifstream testfin;
    testfin.open("EXIT_STATUS_OK"); 
    if (testfin) { print_error_and_exit("EXITS_STATUS_OK file found. This means an already finnished simulation.");}
    testfin.close();
    testfin.open("STARTED"); 
    if (testfin) { print_error_and_exit("STARTED file found. This means an already started but not finnished simulation.\n"
					"Consider using the setup_coninuing_simul.sh script.");}
    testfin.close();
    // initializations
    total_time_counter_.set_name("Total time");
    total_time_counter_.start_timing();
    timer_force_.set_name("Force time");
    timer_printing_.set_name("Printing time");
    timer_integration_.set_name("Integ time");
    timer_nlist_.set_name("nlist time");
    srand48(0);
    // default constructions if not done at declaration (c++11)
    nplanewall_ = DEM::NWALLS; // default
    periodic_[0] = periodic_[1] = periodic_[2] = false;
    // start reading config
    read_config(); 
    time_ = time0_; // Important for first printing of bodies and contacts at the correct time
    iter_ = iter0_ = lrint(time0_/dt_); // Important for first printing of bodies and contacts at the correct time
    read_bodies_info(); 
    set_uid(); // init
    MaterialVars.read_config();
    MaterialVars.start(particle, "OUTPUT/MATERIALS_INFO.txt");
    Interactor_.start(MaterialVars, particle.size());
    read_contacts_info();
    Interactor_.reset_residual_flag(time0_);
    read_walls_properties();
    // process alive particles
    activator_.start(particle, ngrain_);
    // info
    print_log("Printing read body and contact data to OUTPUT/" BODIES_INFO "_INPUT and OUTPUT/" CONTACTS_INFO "_INPUT");
    print_bodies_info("OUTPUT/" BODIES_INFO "_INPUT"); print_contacts_info("OUTPUT/" CONTACTS_INFO "_INPUT");
    //print_bodies_info(); print_contacts_info();
    print_log("Printing materials to OUTPUT/MATERIALS_INPUT.txt");
    MaterialVars.print_materials_info("OUTPUT/MATERIALS_INPUT");
    int status = std::system("date > STARTED"); if (0 != status) print_warning("Problem creating STARTED file");
    flog_.open("OUTPUT/log.txt");
  }
  
  WorkspaceDEM::~WorkspaceDEM() 
  {
    print_log("In workspace destructor");
    UTIL::print_start("Printing files ... ");
    print_bodies_info(); print_contacts_info();
    print_bodies_info("OUTPUT/" BODIES_INFO_FILE); print_contacts_info("OUTPUT/" CONTACTS_INFO_FILE);
    UTIL::print_done("Printing files ... ");
    total_time_counter_.stop_timing();
    std::ofstream fout("EXIT_STATUS_OK");
    total_time_counter_.print(fout);
    fout.close();
    flog_.close();
    int status = std::system("date >> EXIT_STATUS_OK"); if (0 != status) print_warning("Problem adding date to EXIT_STATUS_OK");
  }


  //------------------------------------------------------
  // TIME INTEGRATION
  //------------------------------------------------------
  void
  WorkspaceDEM::do_time_step(void)
  {
    // compute packing fraction to control maxvell on walls
    change_global_vars(); // should do nothing in production codes
    activator_.activate(iter_, particle); // activate not alive particles at given frecuency
    
    // stress tensor, q and p
    double q = 0, p = 0;
    static Mat3d_t Stress;
    stress_tensor(particle, Interactor_.contacts_, Stress);
    get_p_q(Stress, p, q);
    
    timer_integration_.start_timing();
    // integration
    bool updatenlist = false;
    updatenlist = tintegrator_.LeapFrogStep(particle, dt_, iter_, packing_fraction(), bcfixer_, p, q, max_rad_);
    apply_periodicity();
    timer_integration_.stop_timing();

    double lmin[3] = {0}, lmax[3] = {0}; // for walls limits
    store_box_limits(lmin, lmax);
    if (0 == iter_%nlistupdate_ || true == updatenlist) { // update cell list every nlistupdate steps: Default value is 100
    //if (true == updatenlist) { // update cell list only when displacement is enough
      timer_nlist_.start_timing();
      int status = cell_list_.update(particle, lmin, lmax);
      cell_list_.update_nlist(particle, 2*max_rad_);
      timer_nlist_.stop_timing();
      if (EXIT_FAILURE == status) { // END SIMULATION
	print_bodies_info(); print_contacts_info(); 
	print_error_and_exit("Exiting after cell list error");
      } 
    }

    // compute force // for the next time step
    compute_total_force_torque(); 
    check_simul_state();
  }

  void 
  WorkspaceDEM::apply_periodicity(void)
  { 
    double wmin[3] = {0}, wmax[3] = {0}; // for walls limits
    store_box_limits(wmin, wmax);
    for (int dir = 0; dir <= 2; ++dir) {
      if(true == periodic_[dir]) {
	for (int id = 0; id < ngrain_; ++id) {
	  while (particle[id].R[dir] < wmin[dir]) {
	    particle[id].R[dir]  += wmax[dir] - wmin[dir];
	    particle[id].R0[dir] += wmax[dir] - wmin[dir]; // shift the reference R0 position
	  }
	  while (particle[id].R[dir] > wmax[dir]) {
	    particle[id].R[dir]  -= wmax[dir] - wmin[dir];
	    particle[id].R0[dir] -= wmax[dir] - wmin[dir]; // shift the reference R0 position
	  }
	}  
      }
    }
  }

  void 
  WorkspaceDEM::start(void)
  {
    if (true == started_) {
      print_warning("SYSTEM ALREADY STARTED");
      return;
    }

    UTIL::print_start("Starting system");
    iter_ = iter0_;
    time_ = iter_*dt_;
    double test_dt = M_PI*sqrt(particle[0].mass/(2*MaterialVars(particle[0].materialTag, particle[0].materialTag, K)))/200; 
    print_log("mass_0 = ", particle[0].mass);
    print_log("Kn = ", MaterialVars(particle[0].materialTag, particle[0].materialTag, K));
    print_log("Suggested dt = ", test_dt);
    print_log("Read dt_ = ", dt_);
    if ( (dt_ > test_dt) && ((dt_ - test_dt) > 0.10*dt_) ) { // relative difference 
      print_error_and_exit("Given dt too large. Exiting.");
      //dt_ = test_dt;
      //print_warning("Changing dt_ to suggested!");
    }
    set_uid(); 
    // check alive/dead for periodicity
    set_alive_walls();
    // cell list
    apply_periodicity();
    double lmin[3] = {0}, lmax[3] = {0};
    store_box_limits(lmin, lmax);
    cell_list_.start(ngrain_, lmin, lmax, 2*max_rad_, periodic_, nneighbormax_); 
    cell_list_.update(particle, lmin, lmax);
    cell_list_.check_state();
    Interactor_.distance_ss_.set_periodic(periodic_, particle[ngrain_ + RIGHT].R[0], particle[ngrain_ + FRONT].R[1], particle[ngrain_ + TOP].R[2]); // NOTE: WARNING : assumes start lengths ar ate zero ; TODO : Replace by lmax lenghts
    if (false == Interactor_.check_forces(particle) && GLOBAL_DAMPING_GRAINS_ < 1.0e-10) {
      print_warning("There could be an error on reading particles");
    }
    
    // time integration
    tintegrator_.LeapFrogStart(particle, dt_, iter_, bcfixer_);
    tintegrator_.phithreshold(PHI_THRESHOLD_);
    apply_periodicity();
    store_box_limits(lmin, lmax);
    cell_list_.update(particle, lmin, lmax);
    cell_list_.update_nlist(particle, 2*max_rad_);
    cell_list_.check_state();
    print_bodies_info(); print_contacts_info();

    started_ = true; 
    print_log("END   : Starting system");
  }
  
  void 
  WorkspaceDEM::evolve(void)
  {
    if (true == keep_running_ && niter_ >= 1 && (iter0_ < niter_ || run_until_relaxed_) ) {
      if (false == started_) start();
      print_bodies_info(); print_contacts_info(); 
      // first time step
      print_log("DOING FIRST TIME STEP");
      print_log("nc                            = ", Interactor_.size());
      print_log("nc_memoized                   = ", Interactor_.full_size());
      do_time_step();
      print_log("nc                            = ", Interactor_.size());
      print_log("nc_memoized                   = ", Interactor_.full_size());
      print_log("END : DOING FIRST TIME STEP");
      ++iter_;
      time_ = iter_*dt_; // better than just adding dt_, to avoid cumulating numerical errors
      int progress_fraction = int(0.01*niter_); //percentage 
      if (progress_fraction <= 0) progress_fraction = 1;
      for (int print_counter = 1, run_counter = 1, progress_counter = 1;
	   ; // not ending condition, see at the end of the loop
	   ++iter_, ++print_counter, ++run_counter, ++progress_counter) {
	time_ = iter_*dt_;
	// checks and printing
	if ( ((iter0_ + 1) == iter_) || progress_counter >= progress_fraction) {  
	  print_logfile_info();
	  print_screen_info();
	  bcfixer_.print_info(iter_, time_);
	  cell_list_.check_state();
	  if (true == run_until_relaxed_) is_relaxed(bcfixer_.ramps_done(iter_)); // print relaxing info, details only if ramps done
	  progress_counter = 1;
	}
	if (true == run_until_relaxed_ && bcfixer_.ramps_done(iter_) && iter_ > 0.15*niter_ && true == is_relaxed(false)) {
	  print_log(" ");
	  print_log("********* SYSTEM RELAXED *********.");
	  print_screen_info();
	  print_bodies_info(); print_contacts_info(); 
	  break;
	}
	if (print_counter >= nprint_) { 
	  print_bodies_info(); print_contacts_info(); 
	  print_counter = 1;
	}
	if (run_counter >= nread_runtime_info_) {
	  read_config(); // re-read parameters
	  if (false == keep_running_) break;
	  run_counter = 1;
	}
	// time step
	do_time_step();

	// end loop if done time steps and not waiting for equilibrium
	if (iter_ >= niter_ && false == run_until_relaxed_) {
	  break;
	} 
      }
      compute_total_force_torque();
      print_log("# Evolution Done. Total Simulated Time = ", time_);
      if (true == run_until_relaxed_ && false == is_relaxed(true)) print_warning("System still not (necessarily) relaxed.");
      print_log("# Printing bodies Info ...");
      print_bodies_info("OUTPUT/" BODIES_INFO_FILE); print_contacts_info("OUTPUT/" CONTACTS_INFO_FILE);
      print_bodies_info("last_" BODIES_INFO_FILE); print_contacts_info("last_" CONTACTS_INFO_FILE); // print in local directory
      print_bodies_info(); print_contacts_info(); 
      UTIL::print_done("Printing bodies Info.");
    }
  }


  //------------------------------------------------------
  // Force related functions
  //------------------------------------------------------
  Vec3d_t 
  WorkspaceDEM::get_fixed_stress(const int & wd) 
  {
    ASSERT(BOTTOM <= wd && wd <= FRONT);
    static Vec3d_t Val; 
    Val = NORMAL_VEC_WALL[wd]*std::fabs(bcfixer_.get_fixed_value(wd, DEM::WALL_DIR[wd], iter_)); // the sign is encoded in the normal vector
    return Val;
  }
  
  Vec3d_t 
  WorkspaceDEM::get_force_from_stress(const int & wd) 
  {
    ASSERT(BOTTOM <= wd && wd <= FRONT);
    return get_fixed_stress(wd)*area_planewall(wd); // uses current area
    //return get_fixed_stress(wd)*SQUARE(std::cbrt(ngrain_)*2*max_rad_); // uses smaller area of a cubic sample, fixed force
    //return get_fixed_stress(wd)*std::min(area_planewall(wd), SQUARE(std::cbrt(ngrain_)*2*max_rad_)); // smaller area, compared with cubic sample
  }

  void
  WorkspaceDEM::compute_total_force_torque(void) 
  {
    // add fixes stresses on walls
    for (int wd = BOTTOM; wd <= FRONT; ++wd) {
      if(true == particle[ngrain_ + wd].alive) 
	particle[ngrain_ + wd].F_fixed = get_force_from_stress(wd);
    }

    // reset internal force (equal it to F_fixed), torque, and nc
    for_each(particle.begin(), particle.end(), std::mem_fun_ref(&Particle::reset_force_torque));

    // add gravity to all particles
    for (auto & body : particle) {
      if (false == body.alive) continue;
      body.F += G*(body.mass);
    }

    timer_force_.start_timing();
    // COMPUTE THE CONTACT FORCES
    Interactor_.compute_collisions(cell_list_, particle, dt_, nlistmode_);
    timer_force_.stop_timing();

    // small global damping  :  DAMPING UNITS ARE 1/T
    // Damping for grains should be  smaller than for walls, to avoid high damping issues
    // grains
    if (GLOBAL_DAMPING_GRAINS_ > 0.0) {
      for (int id = 0; id < ngrain_; ++id) {
	if (false == particle[id].alive) continue;	
	particle[id].F -= GLOBAL_DAMPING_GRAINS_*particle[id].mass*particle[id].V; // fact*particle[id].V;
	particle[id].T -= GLOBAL_DAMPING_GRAINS_*particle[id].inerI*particle[id].W; // fact*SQUARE(particle[id].rad)*particle[id].W;
      }
    }
      // walls
    if (GLOBAL_DAMPING_WALLS_ > 0.0) {
      for (int id = ngrain_; id < ngrain_ + nplanewall_; ++id) {
	if (false == particle[id].alive) continue;	
	particle[id].F -= GLOBAL_DAMPING_WALLS_*particle[id].mass*particle[id].V; // fact*particle[id].V;
      }
    }
  }

  //------------------------------------------------------
  // UTILITY FUNCTIONS
  //------------------------------------------------------
  void
  WorkspaceDEM::change_radius_mass_inerI(Particle & grain, double rad) 
  {
    double rho = MaterialVars(grain.materialTag, RHO); 
    grain.rad = rad;
    grain.mass = rho*4.0/3.0*M_PI*pow(grain.rad, 3); 
    grain.inerI = (2.0/5.0)*grain.mass*pow(grain.rad, 2); 
  }

  void 
  WorkspaceDEM::set_uid(void) 
  {
    for (int id = 0; id < ngrain_ + nplanewall_; ++id) particle[id].uid = id;
  }

  void
  WorkspaceDEM::print_stress_info(void)
  {
    double q = 0, p = 0;
    static Mat3d_t Stress;
    stress_tensor(particle, Interactor_.contacts_, Stress);
    get_p_q(Stress, p, q);
    std::clog << Stress << std::endl;
    print_log("p , q = ", p, q);
  }
  
  void
  WorkspaceDEM::print_mean_vel_info(void) 
  {
    double sumw, sumv;
    // verify walls 
    int count = 0;
    Vec3d_t tmpVwall = Vecnull;
    sumw = sumv = 0;
    for (int id = ngrain_; id < ngrain_ + nplanewall_; ++id) {
      if ( particle[id].alive ) {
	sumv += particle[id].V.norm();
	sumw += particle[id].W.norm();
	tmpVwall += particle[id].V;
	++count;
      }
    }
    //print_log("Walls mean V           = ", tmpVwall/nplanewall_);
    print_log("Walls mean ||V|| and ||W||    = ", sumv/count, sumw/count);
    // verify particles
    count = 0;
#if DEBUG
    double sumphi;
    sumw = sumv = sumphi = 0;
#endif
    Vec3d_t tmpV = Vecnull;
    Vec3d_t tmpW = Vecnull;
    for (int id = 0; id < ngrain_; ++id) {
      if(particle[id].nc >= 1) {
#if DEBUG
	sumphi += particle[id].PHI.norm();
	sumv += particle[id].V.norm();
	sumw += particle[id].W.norm();
#endif
	tmpV += particle[id].V;
	tmpW += particle[id].W;
	++count;
      }
    }
    print_log("Particles (nc >= 1) count     = ", count);
    if (count > 0) {
      print_log("   norm of mean V, W          = ", tmpV.norm()/count, tmpW.norm()/count);
#if DEBUG
      print_log("   mean sum of ||V||, ||W||   = ", sumv/count, sumw/count);
      print_log("   mean ||PHI||               = ", sumphi/count);
#endif
    }
  }

  /**
     @brief Check if the system is relaxed
     
     This function checks if the system is relaxed after applying several criteria. There should be
     some minimum structure, so a minimum amount of contacts is expected.

     @param print_info - True if relax info should be printed to the screen
   */
  bool 
  WorkspaceDEM::is_relaxed(const bool & print_info) 
  {
    // check number of contacts
    if (0 == Interactor_.size()) { 
      if (true == print_info) print_warning("RELAX :: No contacts"); 
      return false; 
    }
    if (Interactor_.size() < ngrain_/15.0) { 
      if (true == print_info) print_warning("RELAX :: contacts too small (< ngrain/15)"); 
      return false; 
    }
    
    // check number of sliding contacts 
    if (Interactor_.nsliding(particle, 3) >= 0.05*Interactor_.size()) { 
      if (true == print_info) print_warning("RELAX :: nsliding > 1%"); 
      return false; 
    }

    // check mean velocity : In this case, accept if the velocities are small enough
    Vec3d_t Vavg = Vecnull, VWavg = Vecnull;
    for (const auto & body : particle) {
      Vavg += body.V; 
      VWavg += body.rad*body.W;
    }
    Vavg /= particle.size(); 
    VWavg /= particle.size();
    if (Vavg.squaredNorm() > 1.0e-8 || VWavg.squaredNorm() > 1.0e-8) {
      if (true == print_info) print_warning("RELAX :: Vavg > 1.0-8 || VWavg > 1.0-8"); 
      if (true == print_info) print_warning("RELAX :: Vavg  = ", Vavg ); 
      if (true == print_info) print_warning("RELAX :: VWavg = ", VWavg); 
      return false;
    }

    // check Cundall parameter
    double sumFc = 0, sumTc = 0;
    Interactor_.sum_force_torque(sumFc, sumTc);
    if (DEM::util::unbalanced_force_torque(particle, ngrain_, sumFc, sumTc, false) >= 5.0e-5) { 
      if (true == print_info) print_warning("RELAX :: Large Cundall. REF = ", 5.0e-5); 
      return false; 
    }
    if (sumFc == 0) { 
      if (true == print_info) print_warning("RELAX :: sumFc = 0"); 
      return false; 
    }
    

    // check mean velocity of walls respecto to geomertical center GC
    Vec3d_t VmeanGC = Vecnull;
    int count = 0; 
    for (int wd = BOTTOM; wd <= FRONT; ++wd) {
      int jd = ngrain_ + wd;
      if ( true == particle[jd].alive ) {
	VmeanGC += particle[jd].V;
	++count;
      }
    }
    VmeanGC /= (count > 0) ? count : 1;
    double Vmean = 0;
    for (int wd = BOTTOM; wd <= FRONT; ++wd) {
      int jd = ngrain_ + wd;
      if ( true == particle[jd].alive ) {
	Vmean += (particle[jd].V - VmeanGC).squaredNorm();
      }
    }
    Vmean /= (count > 0) ? count : 1;
    if (Vmean > 1.0e-4) { 
      if (true == print_info) {
	print_warning("RELAX : Vmean_w respec to GC", Vmean); 
	print_warning("RELAX : VGC = ", VmeanGC); 
      }
      return false; 
    }

    // check Kinetic energy of grains respect to the center of mass
    double total_mass = 0;
    Vec3d_t Vcm(Vecnull), Wcm(Vecnull);
    for (int id = 0; id < ngrain_; ++id) { 
      Vcm += particle[id].mass*particle[id].V;
      Wcm += particle[id].mass*particle[id].W;
      total_mass += particle[id].mass;
    }
    Vcm /= total_mass;
    Wcm /= total_mass;
    double ek_cm = 0;
    for (int id = 0; id < ngrain_; ++id) { 
      ek_cm += 0.5*particle[id].mass*(particle[id].V - Vcm).squaredNorm();
      ek_cm += 0.5*particle[id].inerI*(particle[id].W - Wcm).squaredNorm();
    }
    ek_cm /= ngrain_;
    if (ek_cm > 1.0e-6*(sumFc*2*mean_rad_)) { // not relaxed in kinetic energy
      if (true == print_info) print_warning("RELAX : ek_cm/sumFc*d = ", ek_cm/(sumFc*2*mean_rad_));
      return false;
    }

    return true;
  }

  /*
  bool
  WorkspaceDEM::ramps_done(void) 
  {
    for (int wd = BOTTOM; wd <= FRONT; ++wd) {
      if (iter_ <= N_STEPS_FIXED_STRESS[wd])
	return false;
    }
    return (tintegrator_.ramps_done(iter_) && true);
  }
  */

  double 
  WorkspaceDEM::thickness_for_wall(const int & wd)
  {
    ASSERT(BOTTOM <= wd && wd <= FRONT);  
    double l = 0;
    switch (wd) {
    case LEFT: case RIGHT:
      l = particle[ngrain_ + RIGHT].R[0] - particle[ngrain_ + LEFT].R[0]; 
      break;
    case BOTTOM: case TOP:
      l = particle[ngrain_ + TOP].R[2] - particle[ngrain_ + BOTTOM].R[2]; 
      break;
    case FRONT: case BACK:
      l =  particle[ngrain_ + FRONT].R[1] - particle[ngrain_ + BACK].R[1]; 
      break;
    default:
      print_error("Wd not found : ", wd);
      print_error_and_exit("Wall wd not found");
    }
    return l;
  }

  double 
  WorkspaceDEM::area_planewall(const int & wd)
  {
    ASSERT(BOTTOM <= wd && wd <= FRONT);  
    double area = 0;
    switch (wd) {
    case LEFT: case RIGHT:
      area = particle[ngrain_ + TOP].R[2] - particle[ngrain_ + BOTTOM].R[2];  
      if(3 == dim_) area *= particle[ngrain_ + FRONT].R[1] - particle[ngrain_ + BACK].R[1];
      break;
    case BOTTOM: case TOP:
      area = particle[ngrain_ + RIGHT].R[0] - particle[ngrain_ + LEFT].R[0]; 
      if(3 == dim_) area *= particle[ngrain_ + FRONT].R[1] - particle[ngrain_ + BACK].R[1];
      break;
    case FRONT: case BACK:
      area =  particle[ngrain_ + TOP].R[2] - particle[ngrain_ + BOTTOM].R[2]; 
      area *= particle[ngrain_ + RIGHT].R[0] - particle[ngrain_ + LEFT].R[0]; 
      break;
    default:
      print_error("Wd not found : ", wd);
      print_error_and_exit("Wall wd not found");
    }
    return area;
  }

  void 
  WorkspaceDEM::copy_periodic_flags(bool array[3])
  {
    array[0] = periodic_[0];
    array[1] = periodic_[1];
    array[2] = periodic_[2];
  }
  
  void 
  WorkspaceDEM::set_alive_walls() 
  {
    if (2 == dim_) {
      particle[ngrain_ + FRONT].alive = false;
      particle[ngrain_ + BACK ].alive = false;
    }
    if (true == periodic_[0]) {
      print_log("Setting periodic on x");
      particle[ngrain_ + LEFT ].alive = false;
      particle[ngrain_ + RIGHT].alive = false;
    }
    if (true == periodic_[1]) {
      print_log("Setting periodic on y");
      particle[ngrain_ + BACK ].alive = false;
      particle[ngrain_ + FRONT].alive = false;
    }
    if (true == periodic_[2]) {
      print_log("Setting periodic on z");
      particle[ngrain_ + TOP   ].alive = false;
      particle[ngrain_ + BOTTOM].alive = false;
    }
  }

  void 
  WorkspaceDEM::store_box_limits(double lmin[3], double lmax[3])
  {
    // Since this function is used for cell list, walls limits are going to be used
    // This avoids, for example, having negative zmin due to interpenetrations
    //
    // wall limits
    lmin[0] = particle[ngrain_ + LEFT].x(); 
    lmax[0] = particle[ngrain_ + RIGHT].x();
    lmin[1] = particle[ngrain_ + BACK].y(); 
    lmax[1] = particle[ngrain_ + FRONT].y();
    lmin[2] = particle[ngrain_ + BOTTOM].z(); 
    lmax[2] = particle[ngrain_ + TOP].z();
  }
  

  void 
  WorkspaceDEM::check_simul_state(void)
  {
#ifdef DEBUG
    if (0 == iter_%10000) { 
      print_warning("Checking simulation (action donde every time step, message printed every 10000 steps)"); 
      print_warning("iter = ", iter_);
    } 
    // check if all the grains are inside the walls
    double lmin[3] = {0}, lmax[3] = {0};
    store_box_limits(lmin, lmax);
    for (int id = 0; id < ngrain_; ++id) {
      if (particle[id].max_limit(0) < lmin[0] || 
	  particle[id].min_limit(0) > lmax[0] ||
	  particle[id].max_limit(1) < lmin[1] || 
	  particle[id].min_limit(1) > lmax[1] ||
	  particle[id].max_limit(2) < lmin[2] || 
	  particle[id].min_limit(2) > lmax[2] 
	  ) {
	print_error("Particle out of simulation domain : id -> ", id);
	print_error("particle.R : ", particle[id].R);
	print_error("particle.rad : ", particle[id].rad);
	print_error("xmin = ", lmin[0]);
	print_error("xmax = ", lmax[0]);
	print_error("ymin = ", lmin[1]);
	print_error("ymax = ", lmax[1]);
	print_error("zmin = ", lmin[2]);
	print_error("zmax = ", lmax[2]);
	print_bodies_info(); print_contacts_info();
	print_error_and_exit("Exiting.");
      }
    }
#endif
    // add a function to check that interpenetrations are smaller than the maximum radius
    if (false == Interactor_.check_small_penetrations(0.3*min_rad_)) {
      //print_error_and_exit("Interpenetrations too high. Exiting.");
      print_error("d = ", 2*mean_rad_);
      print_error(" ");
    }
  }

  double 
  WorkspaceDEM::packing_fraction(void)
  {
    double lmin[3] = {0}, lmax[3] = {0}; // for walls limits
    store_box_limits(lmin, lmax);
    double Vs = 0, V = 0;
    for (int ii = 0; ii < ngrain_; ++ii) {
      if (2 == dim_) Vs += (4.0*M_PI)*SQUARE(particle[ii].rad); 
      else if (3 == dim_) Vs += (4.0*M_PI/3.0)*CUBE(particle[ii].rad);
    }
    V = (lmax[0]-lmin[0])*(lmax[2]-lmin[2]);
    if (3 == dim_) V *= (lmax[1]-lmin[1]); 
    return Vs/V;
  }

  void 
  WorkspaceDEM::change_global_vars(void)
  {
    // this functions is just for very special tests and should do NOTHING on production codes
    /*
    print_error_and_exit("This function should not be active. Exiting.")
    // change the gravity (to simulate a growing tilting)
    const int nsteps = niter_;
    double theta = M_PI/2.0;
    //double theta = 0.40;
    if (iter_ < niter_) {
      theta *= double(iter_)/nsteps;
    } 
    if (fabs(particle[0].V[0]) < 1.0e-5)
    //if (fabs(particle[0].W[1]) < 1.0e-3)
      G = -G.norm()*Vec3d_t(-std::sin(theta), 0, std::cos(theta));  
    //print_log("Theta = ", theta);
    */
  }

  //-----------------------------------------------------------
  // printing and reading functions
  //-----------------------------------------------------------
  template <class T>
  void 
  WorkspaceDEM::check_read_param(T & param, const T & new_val, const std::string & name, const bool & can_change) {
    if (false == started_) { 
      param = new_val;
      print_log("" + name + " = ", param);
    }
    else { 
      if (true == can_change && new_val != param) { // already started
	print_warning("Param => " + name + " <= has changed.");
	print_warning("old val = ", param);
	print_warning("new val = ", new_val);
	param = new_val;
      }
    }
  }

  void 
  WorkspaceDEM::read_config(void)
  {
    if (false == started_) {
      UTIL::print_start("Reading initial CONFIGURATION for general simulation");  
    }
    else {
      print_debug("Re-reading configuration for general simulation -> Done every (iters) =  ", nread_runtime_info_);  
    }
    
    YAML::Node config = YAML::LoadFile("INPUT/general_conf.yaml");
    check_read_param(dim_, config["dim"].as<int>(), "dim", false);
    check_read_param(periodic_[0], bool(config["periodic"][0].as<int>()), "periodic_x", false);
    check_read_param(periodic_[1], bool(config["periodic"][1].as<int>()), "periodic_y", false);
    check_read_param(periodic_[2], bool(config["periodic"][2].as<int>()), "periodic_z", false);
    check_read_param(time0_, config["time0"].as<double>(), "time0", false);
    check_read_param(dt_, config["dt"].as<double>(), "dt", false);
    check_read_param(niter_, config["niter"].as<int>(), "niter", true);
    check_read_param(nprint_, config["nprint"].as<int>(), "nprint", true);
    check_read_param(run_until_relaxed_, bool(config["run_until_relaxed"].as<int>()), "run_until_relaxed", true);
    const Vec3d_t Vtmp(config["Gravity"][0].as<double>(), config["Gravity"][1].as<double>(), config["Gravity"][2].as<double>());
    check_read_param(G, Vtmp, "G", true);
    try { check_read_param(GLOBAL_DAMPING_GRAINS_, config["GLOBAL_DAMPING_GRAINS"].as<double>(), "GLOBAL_DAMPING_GRAINS", true);} 
    catch (...) {check_read_param(GLOBAL_DAMPING_GRAINS_, config["GLOBAL_DAMPING"].as<double>(), "GLOBAL_DAMPING_GRAINS", true);} 
    try { check_read_param(GLOBAL_DAMPING_WALLS_, config["GLOBAL_DAMPING_WALLS"].as<double>(), "GLOBAL_DAMPING_WALLS", true); }
    catch (...) {check_read_param(GLOBAL_DAMPING_WALLS_, 10.0*config["GLOBAL_DAMPING"].as<double>(), "GLOBAL_DAMPING_WALLS", true);} 
    check_read_param(WALLS_MAX_VEL_, config["WALLS_MAX_VEL"].as<double>(), "WMAX_VEL", true);
    tintegrator_.wmaxvel_ = WALLS_MAX_VEL_;
    check_read_param(PHI_THRESHOLD_, config["PHI_THRESHOLD"].as<double>(), "PHI_THRESHOLD", true);
    keep_running_ = config["KEEP_RUNNING"].as<int>();
    nread_runtime_info_ = config["REREAD_STEPS"].as<int>();
    nlistmode_ = config["NLISTMODE"].as<int>(2);   // default value 2 : neighborlist
    nlistupdate_ = config["NLISTUPDATE"].as<int>(100);
    nneighbormax_ = config["NNEIGHBORMAX"].as<int>(18);
    
    // Print the configuration just read
    std::ofstream fout("OUTPUT/general_conf.yaml");
    fout << config;
    fout.close();

    // some checks and sets on the read values
    ASSERT(2 <= dim_ && dim_ <= 3);
    ASSERT(dt_ > 0); 
    ASSERT(nprint_ >= 1);
    
    if ( false == started_ ) {
      if ((periodic_[0] && periodic_[1]) || (periodic_[0] && periodic_[2]) || (periodic_[1] && periodic_[2])) {
	print_warning_wait("Biperiodic conditions detected: Be sure to put at least three layers of particles for biperiodicity,"
			   " otherwise this code wont work correctly: Contacts of a given particle (id) with another (jd) and its "
			   "periodic image (jd') are not handled correctly since contacts are determined only by indexes (and "
			   "therefore only one contact must exists between two particles) ", 3.0);
      }
    } 
    if(time0_ > 1.0e-16 && false == started_) {
      print_warning_wait("Continuing simulation ... DO NOT FORGET TO COPY THE LAST PREVIOUS STATE TO INPUT !!! ", 5.0);
      ready_to_print_ = true;
    }
    if(false == started_) {
      iter0_ = lrint(time0_/dt_); // current iteration
      print_log("iter0_ = ", iter0_);
    }

    if (false == started_) {
      print_log("END :  Reading CONFIGURATION for general simulation");
    }
    else {
      print_debug("END :  Re-reading config for general simulation");
    }
  }

  void 
  WorkspaceDEM::read_bodies_info(void)
  {
    // info start
    UTIL::print_start("Reading bodies info"); 
    // init
    particle.clear(); // Should be cleared since the back_inserter will insert at the end
#if !(WITH_NETCDF) 
    int nparticles;
    // INPUT
    // c 
    int count;
    print_log("Reading file : ", "INPUT/" BODIES_INFO_FILE);
    std::FILE * fp = std::fopen("INPUT/" BODIES_INFO_FILE, "r"); ASSERT(fp);
    count = std::fscanf(fp, "%d%d%d", &nparticles, &ngrain_, &nplanewall_); ASSERT(fp); ASSERT(3 == count);
    print_log("nparticles = ", nparticles);
    Particle body;
    for(int i = 0; i < ngrain_ + nplanewall_; ++i) {
      body.read(fp);
      particle.push_back(body);
    }
    std::fclose(fp);
    // check
    ASSERT(ngrain_ + nplanewall_ == int(particle.size()));
    // print some info
#else
    print_log("Reading file : ", "INPUT/" BODIES_INFO_FILE);
    const int status = read_particles_netcdf("INPUT/" BODIES_INFO_FILE, particle);
    if (0 != status) print_error("Error reading file with netcdf");
    ngrain_ = particle.size() - DEM::NWALLS;
    nplanewall_ = DEM::NWALLS;
#endif
    
    print_log("Total read  = ", particle.size());
    print_log("ngrain_ = ", ngrain_);
    print_log("nplanewall_ = ", nplanewall_);
    ASSERT(ngrain_ >= 0);

    // set radii limits
    mean_rad_ = min_rad_ = max_rad_ = particle[0].rad;
    for (int id = 1; id < ngrain_; ++id) {
      double rad = particle[id].rad;
      if (rad < min_rad_) min_rad_ = rad;
      if (rad > max_rad_) max_rad_ = rad;
      mean_rad_ += rad;
    }
    mean_rad_ /= ngrain_;

    // end
    UTIL::print_done("Reading bodies info"); 
  }

  void 
  WorkspaceDEM::read_walls_properties(void)
  {
    print_log("Reading Walls properties");
    YAML::Node config = YAML::LoadFile("INPUT/walls_conf.yaml");
    for (int jd = BOTTOM; jd <= FRONT; ++jd) {
      int tag = config["MATERIAL_TAG"][WallName[jd]].as<int>(); ASSERT(MaterialVars.check_existence(tag)); 
      particle[ngrain_ + jd].materialTag = tag;
    }
    // read boundary conditions
    const std::string dir_name[3] = {"x", "y", "z"};
    for (int jd = BOTTOM; jd <= FRONT; ++jd) {
      ASSERT(Particle::WALL == particle[ngrain_ + jd].type);
      print_log("wall = ", WallName[jd]);
      for (int dir = 0; dir <= 2; ++dir) {
	print_log("Reading info on direction : ", dir_name[dir]);
	const int flag = config["BOUNDARY"][WallName[jd]][dir_name[dir]][0].as<int>(); 
	ASSERT(0 <= flag && flag <= Fixes::BoundaryFix::FIX_TYPE::TOTAL_FIXES); 
	// verify if not-old config file
	if (3 == config["BOUNDARY"][WallName[jd]][dir_name[dir]].size()) {
	  print_error("Old walls config file detected. You should updated your INPUT/walls_conf.yaml file.");
	  print_error_and_exit("You can use the update_walls_conf_yaml.sh script to update your file.");
	}
	// read fix_function and parameteres
	std::vector<double> par; 
	int fix_function = -1;
	if (config["BOUNDARY"][WallName[jd]][dir_name[dir]][1]) fix_function = config["BOUNDARY"][WallName[jd]][dir_name[dir]][1].as<int>();
	else print_error_and_exit("No fix boundary function setting found");
	ASSERT(0 <= fix_function && fix_function < Fixes::BoundaryFix::FIX_FUNCTION::TOTAL_FUNCTION); 
	for (int opt = 2; opt <= 6; ++opt) {
	  if (Fixes::BoundaryFix::FIX_FUNCTION::RAMP == fix_function && opt >= 5) continue; 
	  if (Fixes::BoundaryFix::FIX_FUNCTION::HAR  == fix_function && opt >= 6) continue; 
	  if (Fixes::BoundaryFix::FIX_FUNCTION::DYN_ALTER  == fix_function && opt >= 5) continue;
	  if (config["BOUNDARY"][WallName[jd]][dir_name[dir]][opt]) par.push_back(config["BOUNDARY"][WallName[jd]][dir_name[dir]][opt].as<double>()); 
	  else print_error_and_exit("bcfix function: missing argument ", opt-1);
	}
	std::clog << "# Parameters read:" << std::endl;
	for (const auto & val : par) std::clog << "# " << val << std::endl;
	
	if (1 == flag) { // velocity controlled
	  bcfixer_.set_boundary_fix(jd, dir, Fixes::BoundaryFix::FIX_TYPE::VEL, fix_function, par);
	}
	if (2 == flag) { // stress controlled
	  if ((0 == dir && (LEFT   == jd || RIGHT == jd)) || 
	      (1 == dir && (FRONT  == jd || BACK  == jd)) ||
	      (2 == dir && (BOTTOM == jd || TOP   == jd)) 
	      ) {
	    bcfixer_.set_boundary_fix(jd, dir, Fixes::BoundaryFix::FIX_TYPE::SIGMA, fix_function, par);
	  }
	}
	if (3 == flag) { // stress-velocity controlled (velocity depends on value of q)
	  bcfixer_.set_boundary_fix(jd, dir, Fixes::BoundaryFix::FIX_TYPE::SIGMA_VEL, fix_function, par);
	}
      }
    }    

    // print read config
    std::ofstream fout("OUTPUT/walls_conf.yaml");
    fout << config;
    fout.close();

    UTIL::print_done("Reading Walls properties");
  }
  
  void 
  WorkspaceDEM::read_contacts_info(void)
  {
    UTIL::print_start("Reading contacts info from file : INPUT/" CONTACTS_INFO_FILE); 
    
    // init variables and clean memory
    Interactor_.clear(); 
    
#if !(WITH_NETCDF)    
    // c
    Interactor_.read("INPUT/" CONTACTS_INFO_FILE);
#else
    read_contacts_netcdf("INPUT/" CONTACTS_INFO_FILE, Interactor_.contacts_);
#endif
    
    // initialization of pointers
    Interactor_.init_pointers(particle);
    if(false == Interactor_.check_forces(particle) && GLOBAL_DAMPING_GRAINS_ < 1.0e-10 && G.squaredNorm() < 1.0e-10) {
      print_warning("There could be an error on reading particles");
    }

    // print info
    print_log("Contacts read = ", Interactor_.size());
    print_contacts_info("OUTPUT/" CONTACTS_INFO "_INITIAL." EXT);
    UTIL::print_done("Reading contacts info from file : INPUT/" CONTACTS_INFO_FILE); 
  }
 
  void 
  WorkspaceDEM::print_mean_max_penetration_info(void) 
  {
    double mean, max;
    Interactor_.max_mean_penetration(max, mean);
    print_log("mean_ov, max_ov (/min_rad) = ", mean/min_rad_, max/min_rad_);
    if (max/min_rad_ > 0.3) {
      print_warning("ATTENTION: High interpentration! -> ", max/min_rad_);
    }
  }

  void 
  WorkspaceDEM::print_screen_info(void) 
  {
    print_log(""); 
    print_log("Progress ( % )                = ", iter_*100.0/niter_);
    print_log("Time step, Total, Time        = ", iter_, niter_, time_); 
    print_log("nc, nc + memoized             = ", Interactor_.size(), Interactor_.full_size());
    double sumFc, sumTc;
    Interactor_.sum_force_torque(sumFc, sumTc);
    print_log("Unbalanced Force and Torque   = ", DEM::util::unbalanced_force_torque(particle, ngrain_, sumFc, sumTc, true));
#if DEBUG
    if (G.squaredNorm() > 0) print_log("G = ", G); 
    if (G[2] > 0) print_log("G->theta = ", std::atan(-G[0]/G[2])); 
#endif
    print_mean_max_penetration_info();
    print_mean_vel_info();
    print_log("ns (particles with nc >= 3)   = ", Interactor_.nsliding(particle, 3));
    print_log("Packing fraction", packing_fraction()); 
    print_stress_info();
    print_memory_info();
  }

  void 
  WorkspaceDEM::print_memory_info(void) {
    std::cout << "# --------------------------------INFO MEMORY (BYTES)--------------------------------" << std::endl;
    std::printf("#%12s  %12s  %12s  %12s  %12s  %12s\n", "workspace", "particles", "contacts", "materials", "cell_list", "time_integ");
    std::printf("#%12d  %12d  %12d  %12d  %12d  %12d\n", 
		int(sizeof(WorkspaceDEM)), 
		int(sizeof(particle) + sizeof(Particle)*particle.size()), 
		Interactor_.memory_footprint(), 
		MaterialVars.memory_footprint(), 
		cell_list_.memory_footprint(), 
		tintegrator_.memory_footprint());
    std::cout << "# ----------------------------------END INFO MEMORY----------------------------------" << std::endl;
    
  }

  void 
  WorkspaceDEM::print_logfile_info(void) 
  {
    double sumFc, sumTc;
    Interactor_.sum_force_torque(sumFc, sumTc);
    flog_.print(time_, iter_, DEM::util::unbalanced_force_torque(particle, ngrain_, sumFc, sumTc, false));  
  }

  void 
  WorkspaceDEM::print_bodies_info(std::string filename)
  {
    timer_printing_.start_timing();
    // filename settings
    if(filename.empty()) {
      std::stringstream sout;
      sout.str(""); sout.fill('0'); sout.setf(std::ios::fixed, std::ios::floatfield); sout.setf(std::ios::showpoint);
      sout << "OUTPUT/" BODIES_INFO "-" << std::setw(22) << std::setprecision(16) << time_ << "." EXT; 
      filename = sout.str();
    }
    ASSERT(std::string::npos != filename.find(BODIES_INFO));

#if !(WITH_NETCDF)
    // c
    std::FILE * fp = std::fopen(filename.c_str(), "w"); ASSERT(fp); 
    std::fprintf(fp, "%25d\t%25d\t%25d\n\n", ngrain_ + nplanewall_, ngrain_, nplanewall_);
    for (int i = 0; i < ngrain_ + nplanewall_; ++i) {
      particle[i].print(fp);
    }
    std::fclose(fp);
#else
    write_particles_netcdf(particle, filename);
#endif
    timer_printing_.stop_timing();
  }

  void 
  WorkspaceDEM::print_contacts_info(std::string filename)
  {
    timer_printing_.start_timing();

    Interactor_.clear_non_active();

    // set the filename
    if(filename.empty()) {
      std::stringstream sout;
      sout.str(""); sout.fill('0'); sout.setf(std::ios::fixed, std::ios::floatfield); sout.setf(std::ios::showpoint); 
      sout << "OUTPUT/" CONTACTS_INFO "-" << std::setw(22) << std::setprecision(16) << time_ << "." EXT; 
      filename = sout.str();
    }    
    ASSERT(std::string::npos != filename.find(CONTACTS_INFO));

#if !(WITH_NETCDF)
    // c
    Interactor_.print(filename); // print using c-style functions
#else
    write_contacts_netcdf(Interactor_.contacts_, filename);
#endif
    timer_printing_.stop_timing();
  }

} // end namespace DEM

