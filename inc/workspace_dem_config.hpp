#ifndef __WORKSPACE_DEM_CONFIG__
#define __WORKSPACE_DEM_CONFIG__

#include "vector3d.hpp"
#include "utility.hpp"
#include <string>

// labels and DEM globals
namespace DEM{ 
  // Constants
  const int NWALLS = 6;

  // walls indexes
  enum WallLabel {BOTTOM=0, TOP, LEFT, RIGHT, BACK, FRONT}; 

  // walls names
  const std::string WallName[NWALLS] = { "BOTTOM", "TOP", "LEFT", "RIGHT", "BACK", "FRONT" };

  //  // walls normal vectors
  //const Vec3d_t NORMAL_VEC_WALL[NWALLS] = {Vec_uz, -1*Vec_uz, Vec_ux, -1*Vec_ux, Vec_uy, -1*Vec_uy};
  
  // walls associated normal directions
  const int WALL_DIR[NWALLS] = {2, 2, 0, 0, 1, 1};
}

#endif
