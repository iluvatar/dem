#ifndef __CELL_LIST__
#define __CELL_LIST__

#include "utility.hpp"
#include "particle.hpp"
#include <iostream>
#include <vector>
#include <algorithm>
#include <fstream>
#include <cmath>
#include <set>
#include <unordered_set>

//-----------------------------------------------------------------------------
// class CellList
/**
   @brief To keep an shortened list of neighbors per particle through the linked cell list method
   
   This class implements the cell list method to keep a list of particle sper cell and therefore
   allows an efficient access to particle neighbors.
   
   The main goal, for monodisperse systems, is to to keep, in average, one particle per cell.

   Operates on an std::vector of particles of arbitrary type (if you have an array, change it to a std::vector)
   Each object IS REQUIRED to have an internal function x() returning its x position (same for y and z)
   Resolution on each dimensions is just a suggestion, could be slightly larger.
*/
//-----------------------------------------------------------------------------
class CellList 
{
public: // data
  int NNEIGHMAX_ = 18;                           ///< Maximum number of neighbors per particle + 1 place for storing the actual number of neighbors. Might be increased when polydispersity is large
  typedef std::set<int> list_t;                       ///< typedef for list
  typedef std::unordered_set<int> blist_t;            ///< typedef for fast list where order is not important
  std::vector<int> body_cell_;                        ///< body_cell_[id] = index of the cell of the particle  id, size = NBodies
  std::vector<list_t> cell_list_;                     ///< cell_list_[cellID] = List of particles id on cell cellID, size ncells_
  std::vector<blist_t> next_cell_;                    ///< next_cell_[cellID] = list of ncellID neighbor for the cell cellID, size ncells_
  std::vector<int> nlist_;                            ///< Neighbor list: [nneigh0|j0|j1|...|jnneigh0-1|nnneigh1|j0|j1|...|jnneigh1-1|...]
  
public: // methods
  CellList() = default;  ///< does nothing
  void start(const int & nbodies, const double lmin[3], const double lmax[3],
	     const double & dmin, const bool periodic[3], const int nneighbormax); ///< starts the cell list with given dimensions
  int update(std::vector<Particle> & bodyArray, const double wmin[3], const double wmax[3]);    ///< checks if particles have moved 
  void update_nnlist(void);
  bool check_state(void);                                 ///< checks the list for possible inconsistencies
  int memory_footprint(void) { ///< returns the memory compsumption for the internal data structurtes
    return sizeof(CellList) + sizeof(int)*(body_cell_.size() + 27*next_cell_.size() + 5*cell_list_.size() + nlist_.size()); 
  }
  void print_neighbors(const std::string & fname);
  void update_nlist(const std::vector<Particle> & particle, const double rskincut);

private: // data
  const int INVALID_ = -1;                            ///< INVALID is a flag for invalid indexes
  int nbodies_ = 0;				      ///< Number of bodies 
  int ncells_ = 0;				      ///< Number of cells
  int n_[3] = {0};                                    ///< Number if cells on x, y and z
  int nynz_ = 0;                                      ///< optimization, ny_*nz_
  double d_[3] = {0};                                 ///< box size per direction
  double dmin_ = 0;                                   ///< minimun allowed box size. Roughly, max diammeter of particles
  double ud_[3] = {0};                                ///< optimization, ud_ = 1.0/d_
  double lmin_[3] = {0};			      ///< domain minimum rectangular dimensions
  double lmax_[3] = {0};			      ///< domain maximum rectangular dimensions
  bool periodic_[3] = {false};			      ///< periodic flags indicating perodiciy on a given direction
  
private: // methods
  void get_indexes(const int & cellId, int & ix, int & iy, int & iz);              ///< Returns the indexes on each axis for a given cellId 
  int get_cell_id(const int & ix, const int & iy, const int & iz);                 ///< Returns the cellId for given indexes  
  void set_neighbor_id(void);					                   ///< Sets the neighbors ids for each cell 
  bool ignore_neighbor_step(const int & dim, const int & ii, const int & Dii);     ///< Helper to ignore some neighbors given periodicity
};


#endif // __CELL_LIST__
