#ifndef __HISTOGRAM_1_D_HH__
#define __HISTOGRAM_1_D_HH__

#include "utility.hpp"
#include <iostream>
#include <fstream>
#include <iomanip>
#include <cmath>
#include <vector>
#include <algorithm>
#include <numeric>
#include <string>

/**
   UNIFORM-bins Histogram in 1D, estimates probability density
   xval[]    = Bin limits: x_i \in [xval[i], xval[i+1])
   counter[] = Counter for bin i

   @TODO : Create enum for printing modes to easy usage
*/

#if DEBUG
#define _GLIBCXX_DEBUG 
#define _GLIBCXX_DEBUG_PEDANTIC
#endif

class Histogram1D {
public: // methods
  // Construction
  Histogram1D();
  ~Histogram1D();
  void construct(const double & xmin0, const double & xmax0, const long & bins0, const std::string & name = "no_name");
  void generate_range(void);
  // reading data
  void increment(const double & x); //< increment counter
  void increment(const double * x, const int & size); //< load an array of values
  void increment(const std::vector<double> & x); //< load a vector of values
  void accumulate(const double & x, const double & w); //< add weights, for histograms were each bin is like an histogram
  void set(const double & x, const double & val);
  // read from filename
  int read_data(const std::string & fname);
  // statistics
  void compute_pdf(void);
  void compute_prob(void);
  double get_area(void);
  void mean_and_sigma(double & mean, double & sigma) ; 
  double skewness(void);
  double mean(void);
  double sigma(void);
  // state test
  bool empty(void);
  void reset(void); //< Resets all samples, but keeps the current array sizes and limits
  void release_memory(void); //< Resets all samples and release all memory
  // utility
  int  get_valid_bins_size(void);
  void get_valid_data(double *x, double *y);
  void get_valid_counters(double *x, int *c);
  double get_first_valid_x(void);
  // ccdf stuff
  void save_ccdf(std::vector<double> & x, std::vector<double> & y); 
  void print_ccdf(const std::string & fname); 
  // print (possibly) shifted and scaled data as : xnew = mx*x + bx, ynew = my*y + by
  void print(const std::string & fileName,			   //< filename to print
	     const double & mx = 1, const double & bx = 0,	   //< linear map for x : xprint = mx*x + bx
	     const double & my = 1, const double & by = 0,	   //< linear map for x : yprint = my*y + by
	     const bool & print_full_info = false,                 //< print all info, even info per mean
	     const bool & print_only_valid = false, 	           //< print only bins with counter != 0
	     const int & mode = 0);				   //< 0 = pdf, 1 = prob, 2 = counter, 3 = area always equal to 1
  void print_mean_sigma(const std::string & fileName);
  void print_as_mean_per_bin(const std::string & fileName);

private: // methods
  int get_bin(const double & x);
  bool check(void) ;

private: // data
  std::vector<double> xval_;
  std::vector<double> counter_;
  std::vector<double> sum_bin_, sum2_bin_; //< to compute the mean and the sigma per bin, for histograms were each bin es like an histogram
  std::vector<double> pdf_;  //< pdf from data
  std::vector<double> prob_; //< probability from data
  double xmin_ = 0, dx_ = 0, xmax_ = 0, samples_ = 0, total_samples_ = 0, u_dx_ = 0;
  int bins_ = 0;
  static const int BIN_OUT_OF_RANGE = -1; 
  double dropped_xmin_ = 1.0e100, dropped_xmax_ = -1.0e100;
  long double sum_ = 0, sum2_ = 0, sum3_ = 0;
  std::string name_ = "no_name";
};


#if DEBUG
#undef _GLIBCXX_DEBUG 
#undef _GLIBCXX_DEBUG_PEDANTIC
#endif

#endif // __HISTOGRAM_1_D_HH__
