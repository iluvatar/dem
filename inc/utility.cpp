#include "utility.hpp"

//-----------------------------------------------------------------------------------------------
// printing some vector
template <typename type>
void print(const std::vector<type> & v)
{
  UTIL::set_print_format(std::cout);
  std::cout << "# Vector contents: " << std::endl;
  for (typename std::vector<type>::const_iterator it = v.begin(); it != v.end(); ++it) {
    std::cout << *it << "  ";
  }
  std::cout << std::endl;
}
template void print<double>(const std::vector<double> &);

//-----------------------------------------------------------------------------------------------
// printing some array
template <typename type>
void print(const type * a, const int & size)
{
  UTIL::set_print_format(std::cout);
  std::cout << "# Array contents: " << std::endl;
  for (int i = 0; i < size; ++i) {
    std::cout << a[i] << "  ";
  }
  std::cout << std::endl;
}

//-----------------------------------------------------------------------------------------------
// ramp function
//-----------------------------------------------------------------------------------------------
namespace UTIL {
  template <class T1, class T2>
  double ramp_value(const T1 & iter, const T2 & maxiter, const double & val, const double & val0) 
  {
    return (((T2)iter >= maxiter) ? val : ((val - val0)*iter)/(maxiter) + val0);
  }
  template double ramp_value<double, double> (const double & iter, const double & maxiter, const double & val, const double & val0);
}

//-----------------------------------------------------------------------------------------------
// pairing function
//-----------------------------------------------------------------------------------------------
long get_uid(const long & i1, const long & i2) 
{ 
#if DEBUG
  ASSERT(i1 >= 0);
  ASSERT(i2 >= 0);
  ASSERT( (i1+i2) < std::sqrt(std::numeric_limits<long>::max()) ); // avoids overflow
#endif
  
  return (i1+i2)*((i1+i2+1)/2) + i1; // Cantor function
}


long get_uid(const int & i1, const int & i2, const int & k1) 
{ // important: k1 > i2 , for all values of i2
  // k1 = 2^20 \simeq 10^6
  //return (i1<<10) + i2;
  return (i1*k1) + i2;
}


//-----------------------------------------------------------------------------------------------
namespace UTIL {
  template <class T>
  inline
  T maximum(T a, T b, T c)
  {
    return std::max(a, std::max(b, c));
  }
  template <class T>
  inline
  T minimum(T a, T b, T c)
  {
    return std::min(a, std::min(b, c));
  }
}

//-----------------------------------------------------------------------------------------------
template <class T>
inline
T min(T a, T b, T c)
{
  return std::min(a, std::min(b, c));
}

//-----------------------------------------------------------------------------------------------
template <class Real>
inline
Real reduced_value(Real a, Real b)    // get reduced of two values
{
  if (b < 1.0e-16) return 0;
  else if (a < 1.0e-16) return 0;
  else if (a > 1.0e5*b) return b;
  else if (b > 1.0e5*a) return a;
  else return (a*b)/(a+b);
}
template double reduced_value<double>(double, double);

//-----------------------------------------------------------------------------------------------
// set string filename with or other stuff at the end
namespace UTIL {
  std::string composite_name(const std::string & base, const double & suffix, const std::string & ext) {
    const std::string sep = "-"; 
    std::stringstream s; 
    s << base << sep << suffix << ext;
    return s.str();
  }
  std::string composite_name(const std::string & base, const std::string & suffix, const std::string & ext) {
    std::stringstream s; 
    std::string sep = "-"; if (suffix.empty()) sep = "";
    s << base << sep << suffix << ext ;
    return s.str();
  }


} // namespace UTIL

//-----------------------------------------------------------------------------------------------
namespace UTIL {
  
  std::complex<double> spherical_harmonic(const int & l, const int & m, const double & theta, const double & phi)
  {
    if (m > l) return 0;
    static const std::complex<double> I(0.0, 1.0); 
    return std::tr1::sph_legendre(l, std::abs(m), theta)*std::exp(I*(m*phi));
  }

} // namespace UTIL



//-----------------------------------------------------------------------------------------------
// bool to locate a point inside domain
namespace UTIL {
  
  bool is_inside(const double & val, const double & min, const double & max, const double & eps) {
    return ((min - eps) < val && val <= (max + eps)); 
  }

  bool is_vector_inside(const Vec3d_t & V, const Vec3d_t & LeftBottom, const Vec3d_t & RightTop, const double & eps) {
    return ( UTIL::is_inside(V[0], LeftBottom[0], RightTop[0], eps) && 
	     UTIL::is_inside(V[1], LeftBottom[1], RightTop[1], eps) && 
	     UTIL::is_inside(V[2], LeftBottom[2], RightTop[2], eps) );
  }

  double max_component(const Vec3d_t & V) {
    return UTIL::maximum(V[0], V[1], V[2]);
  }
  double min_component(const Vec3d_t & V) {
    return UTIL::minimum(V[0], V[1], V[2]);
  }
  double min_component(const Vec3d_t & V, const int & dim) {
    ASSERT(2 == dim || 3 == dim);
    if (2 == dim) return std::min(V[0], V[2]);
    return UTIL::minimum(V[0], V[1], V[2]);
  }
}

//-----------------------------------------------------------------------------------------------
void mean_and_sigma(const double * vec, const int & size, double & mean_out, double & sigma_out)
{
  double sum1 = 0, sum2 = 0;
  if (size > 0) {
    for(int i = 0; i < size; i++) {
      sum1 += vec[i];
      sum2 += vec[i]*vec[i];
    }
    mean_out = sum1/size;
    sigma_out = (sum2/size - mean_out*mean_out); sigma_out = sqrt(sigma_out);
  }
  else { 
    mean_out = sigma_out = 0;
  }
}

//-----------------------------------------------------------------------------------------------
void mean_and_sigma_vector(const std::vector<double> & vec, double & mean_out, double & sigma_out)
{
  mean_out = sigma_out = 0;
  mean_and_sigma(&vec[0], vec.size(), mean_out, sigma_out);
}

//-----------------------------------------------------------------------------------------------
// stat mech utils
namespace UTIL {
  double 
  voronoi_vmin(const int & dim) 
  {
    // normalized by d^dim
    ASSERT(2 == dim || 3 == dim);
    double vmin = 0;
    // theoretical definitions
    if (2 == dim) vmin = sqrt(3.0)/2.0;
    else if (3 == dim) vmin = 0.694;

    return vmin;
  }

  template <class Histogram>
  double 
  voronoi_vmin(const int & dim, Histogram & histo) 
  {
    // normalized by d3_
    ASSERT(2 == dim || 3 == dim);
    double vmin = UTIL::voronoi_vmin(dim);
    // from skewness 
    double mean, sigma, skew; 
    histo.mean_and_sigma(mean, sigma);
    skew = histo.skewness();
    vmin = std::min(vmin, (mean - 2*sigma/skew));
    // from first x valid value
    vmin = std::min(vmin, histo.get_first_valid_x());

    return vmin;
  }
  
  double statmech_X(const double & mean, const double & sigma, const double & min)
  {
    return ((mean > min) ? sigma*sigma/(mean - min) : 0.0);
  }

  double statmech_K(const double & mean, const double & sigma, const double & min)
  {
    return ((sigma > 0) ? std::pow((mean - min)/sigma, 2) : 0.0);
  }
  double statmech_S(const double & mean, const double & K, const double & min, const double & lambda_cube)
  {
    return K*(1.0 + std::log((mean - min)/(K*lambda_cube)));
  }

}

//-----------------------------------------------------------------------------------------------
// voro++ tessellation
namespace UTIL {
  int compute_voronoi_volumes(const std::vector<Vec3d_t> & points,     // input: array of particles to process
			      const std::vector<double> & weigths,     // input: weights for radical voronoi
			      const double walls_min[3],	       // input: minimum walls limits
			      const double walls_max[3],	       // input: maximum walls limits
			      const bool periodic[3],		       // input: periodicity flags on each direction
			      const int & dim,	         	       // input: number of dimensions (2 or 3)
			      std::vector<double> & voronoi_volumes,   // output: Voronoi volumes per particle 
			      std::vector<Vec3d_t> & centroids,        // output: centroids of the voronoi cells 
			      std::vector<std::vector<int> > & neigh,  // output: to save neighbor information
			      const bool & print)              // output: print system to povray
  {
    std::string msg = "Processing Voronoi container";
    print_start(msg);
    print_log("neigh.size() =  ", neigh.size());
    
    // voro++ polydisperse container
    voro::container_poly conp(walls_min[0], walls_max[0],
			      walls_min[1], walls_max[1],
			      walls_min[2], walls_max[2],
			      20, 20 , 20,
			      periodic[0], periodic[1], periodic[2], 8); 
    print_done("  container declaration.");
    conp.clear(); 
    const int npoints = points.size();
    // container filling
    for(int idx = 0; idx < npoints; ++idx) {
      conp.put(idx, points[idx][0], points[idx][1], points[idx][2], weigths[idx]);
    }
    print_done("  container filling.");

    // Setup memory and arrays
    voronoi_volumes.resize(npoints); std::fill(voronoi_volumes.begin(), voronoi_volumes.end(), 0.0); 
    centroids.resize(npoints); std::fill(centroids.begin(), centroids.end(), Vecnull); 
    neigh.resize(npoints);
    for (auto & n : neigh) { n.clear(); n.shrink_to_fit(); }

    // process voronoi cells
    double Cvol = 0;
    voro::voronoicell_neighbor c; // cell which can store neighbor information
    voro::c_loop_all cla(conp);
    if(cla.start()) do if(conp.compute_cell(c, cla)) {
	  auto id = cla.pid();
	  c.neighbors(neigh[id]); // write # of neighbors
	  voronoi_volumes[id] = c.volume();
	  Cvol += voronoi_volumes[id];
	  c.centroid(centroids[id][0], centroids[id][1], centroids[id][2]);
	} while (cla.inc());

    // Normalize volume for 2D
    if (2 == dim) { 
      const double ly = walls_max[1] - walls_min[1];
      for(auto & vol : voronoi_volumes) vol /= ly;
    }
    double pvol = 0;
    for (const auto & rad : weigths) {
      pvol += rad*rad*rad;
    }
    pvol *= 4.0*M_PI/3.0;
    print_log("-> -> Voronoi total volume: ", Cvol);
    print_done("  voronoi cells processing.");
    
    // print container
    if (true == print) { // UNCOMMENT
    conp.draw_cells_pov("DISPLAY/voronoi_c.pov");
    conp.draw_particles_pov("DISPLAY/voronoi_p.pov");
    print_done("  Printing voronoi cells.");
    }
    //std::cin.get(); // DELETE
 
    print_done(msg);
    return EXIT_SUCCESS;
  }

  int compute_voronoi_volumes(const std::vector<Particle> & particles, // input: array of particles to process
			      const double walls_min[3],	     // input: minimum walls limits
			      const double walls_max[3],	     // input: maximum walls limits
			      const bool periodic[3],		     // input: periodicity flags on each direction
			      const int & dim,	         	     // input: number of dimensions (2 or 3)
			      std::vector<double> & voronoi_volumes, // output: Voronoi volumes per particle 
			      std::vector<std::vector<int> > & neigh)  // output: to save neighbor information
  {
    std::string msg = "Processing Voronoi container from particles array";
    print_start(msg);
    //print_log("neigh.size() =  ", neigh.size());
    
    // Fixed size : Including floating particles
    const int npoints = particles.size() - 6;  // 6 = 6 plane walls
    std::vector<Vec3d_t> points; points.resize(npoints);
    std::vector<double> weights; weights.resize(npoints);
    for(int idx = 0; idx < npoints; ++idx) {
      points[idx]  = particles[idx].R;
      weights[idx] = particles[idx].rad;
    }
    
    /*// Not including floating : NCMIN == 2
    std::vector<Vec3d_t> points; 
    std::vector<double> weights; 
    int npoints = 0;
    for(int idx = 0; idx < particles.size(); ++idx) {
      if (particles[idx].nc >= 0) {
	points.push_back(particles[idx].R);
	weights.push_back(particles[idx].rad);
	npoints++;
      }
      }*/
    
    std::vector<Vec3d_t> centroids; centroids.resize(npoints);
    return compute_voronoi_volumes(points, weights, walls_min, walls_max, periodic, dim, 
				   voronoi_volumes, centroids, neigh, false /*print system*/);
  }



} // namespace UTIL



namespace UTIL {
  //-----------------------------------------------------------------------------------------------
  // compute coronas limits. RT= RightTop, LB = LeftBottom
  //-----------------------------------------------------------------------------------------------
  int compute_coronas_limits_constant_volume(const Vec3d_t & LB,                     // LeftBottom coordinates of full box
					     const Vec3d_t & RT,		           // RigthTop coordinates of full box
					     const double & deltamin,		   // Minimum size of corona
					     const int & dim,			   // Dimension of domain, 2 or 3
					     int & m, 				   // output : Number of coronas
					     std::vector<Vec3d_t> & corona_LB,	   // output : Coronas limits LB
					     std::vector<Vec3d_t> & corona_RT) {     // output : Coronas limits RT
    ASSERT(2 == dim || 3 == dim);
    ASSERT(deltamin > 0);
    
    // compute m
    m = 0;
    const auto L = (RT-LB);
    const double Lmin = (dim == 2) ? std::min(L[0], L[2]) : UTIL::min_component(L);
    ASSERT(Lmin > 0);
    if (Lmin < deltamin) return EXIT_FAILURE;
    m = 1.0/(1.0 - std::pow(1.0 - 2*deltamin/Lmin, dim));
    if (m < 1) return EXIT_FAILURE;
    
    // Compute coronas limits
    corona_RT.resize(m);
    corona_LB.resize(m);
    const auto C = 0.5*(RT+LB);
#if DEBUG
    print_log("\t\t C = ", C);
    const double Vtot = (2 == dim) ? L[0]*L[2] : L[0]*L[1]*L[2]; 
    print_log("\t\t Vtot = ", Vtot);
    const double V0 = Vtot/m;
    print_log("\t\t V0 = ", V0);
#endif
    for (int im = 0; im < m; ++im) {
      corona_RT[im] = C + L*std::pow((im+1.0)/m, 1.0/dim)/2.0;
      corona_LB[im] = C - L*std::pow((im+1.0)/m, 1.0/dim)/2.0;
    }
    if (m >= 2) {
      print_log("Minimum corona size x = ", corona_RT[m-1][0] - corona_RT[m-2][0]);
      print_log("Minimum corona size y = ", corona_RT[m-1][1] - corona_RT[m-2][1]);
      print_log("Minimum corona size z = ", corona_RT[m-1][2] - corona_RT[m-2][2]);
      ASSERT((corona_RT[m-1][0] - corona_RT[m-2][0]) >= deltamin);
    }
   
    return EXIT_SUCCESS;
  }

  int compute_coronas_limits_constant_width(const Vec3d_t & LB,                      // LeftBottom coordinates of full box
					    const Vec3d_t & RT,		           // RigthTop coordinates of full box
					    const int & dim,			   // Dimension of domain, 2 or 3
					    const int & m, 			   // Fixed number of coronas
					    std::vector<Vec3d_t> & corona_LB,	   // output : Coronas limits LB
					    std::vector<Vec3d_t> & corona_RT) {      // output : Coronas limits RT
    ASSERT(2 == dim || 3 == dim);
    ASSERT(m >= 1);
    
    // compute delta (widht)
    double delta = 0;
    const auto L = (RT-LB);
    const double Lmin = (dim == 2) ? std::min(L[0], L[2]) : UTIL::min_component(L);
    ASSERT(Lmin > 0);
    delta = 0.5*Lmin/m;
    if (0.5*Lmin < delta) return EXIT_FAILURE;
    
    // Compute coronas limits
    corona_RT.resize(m);
    corona_LB.resize(m);
    const auto C = 0.5*(RT+LB);
#if DEBUG
    print_log("\t\t C = ", C);
    const double Vtot = (2 == dim) ? L[0]*L[2] : L[0]*L[1]*L[2]; 
    print_log("\t\t Vtot = ", Vtot);
#endif
    for (int im = 0; im < m; ++im) {
      corona_RT[im] = C + delta*(im+1.0)*L/Lmin;
      corona_LB[im] = C - delta*(im+1.0)*L/Lmin;
    }
   
    return EXIT_SUCCESS;
  }

  double box_volume(const Vec3d_t & LeftBottom, const Vec3d_t & RightTop, const int & dim) {
    const Vec3d_t L = RightTop - LeftBottom; 
    double vol = L[0]*L[2];
    if (3 == dim) vol *= L[1];
    return vol;
  }
  
  
} // namspace UTIL


