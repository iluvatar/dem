#include "postpro_display.hpp"

namespace DEM {
  
  //--------------------------------------------------------------------------
  // Read the configuration file, exits if error
  //--------------------------------------------------------------------------
  PostproDisplay::PostproDisplay() : Postpro()
  {
  }
  
  //--------------------------------------------------------------------------
  // draw for gnuplot
  //--------------------------------------------------------------------------
  void PostproDisplay::draw_gnuplot(void) 
  {
    std::string name = "gnuplot"; UTIL::print_start(name);

    file_names(BODIES_INFO "-", bodiesFilenames);
    print_log("Total files to read : ", bodiesFilenames.size());
    if (0 == bodiesFilenames.size()) {
      print_error_and_exit("No files inside time window to read. Exiting");
    }
    
    Visualization gnuplotAnimator;
    const int size = bodiesFilenames.size();
    for(int ii = 0; ii < size; ii += stride_) {
      double ltime = get_time(bodiesFilenames[ii], BODIES_INFO "-");
      read_particles(bodiesFilenames[ii]);
      if(0 == ii) gnuplotAnimator.print_header_gnuplot(particle, "DISPLAY/animation.gif");
      gnuplotAnimator.print_system_gnuplot(particle, ltime);
      print_log("Current Time = ", ltime);
      print_log("Final Time = ", get_time(bodiesFilenames[size-1], BODIES_INFO "-"));
    }    
    read_particles("OUTPUT/" BODIES_INFO_FILE);
    gnuplotAnimator.print_header_gnuplot(particle, "DISPLAY/final.gif");
    gnuplotAnimator.print_system_gnuplot(particle, -1.0);
    
    UTIL::print_done(name);
  }


  //--------------------------------------------------------------------------
  // draw for povray
  //--------------------------------------------------------------------------
  void PostproDisplay::draw_povray(void) 
  {
    std::string name = "povray"; UTIL::print_start(name);

    file_names(BODIES_INFO "-", bodiesFilenames);
    print_log("Total files to read : ", bodiesFilenames.size());
    if (0 == bodiesFilenames.size()) {
      print_error_and_exit("No files inside time window to read. Exiting");
    }

    std::stringstream filename; 
    Visualization povrayAnimator;
    const int size = bodiesFilenames.size();
    for(int ii = 0; ii < size; ii += stride_) {
      double ltime = get_time(bodiesFilenames[ii], BODIES_INFO "-");
      print_log("Current Time = ", ltime);
      print_log("Final Time = ", get_time(bodiesFilenames[size-1], BODIES_INFO "-"));
      read_particles(bodiesFilenames[ii]);
      // compute domain limits
      const int ngrains = particle.size() - DEM::NWALLS; 
      double min[3] = {particle[ngrains + LEFT ].R[0], particle[ngrains + BACK ].R[1], particle[ngrains + BOTTOM].R[2]};
      double max[3] = {particle[ngrains + RIGHT].R[0], particle[ngrains + FRONT].R[1], particle[ngrains + TOP   ].R[2]};
      double aux;
      for (int id = 0; id < ngrains_; ++id) {
	for (int dim = 0; dim < 3; ++dim) {
	  aux = particle[id].min_limit(dim); if (aux < min[dim]) min[dim] = aux;
	  aux = particle[id].max_limit(dim); if (aux > max[dim]) max[dim] = aux;
	}
      }
      double campos_x = 0, campos_y = 0, campos_z = 0; 
      // set pos for camera
      //if(0 == i) {
      //campos_x = particle[ngrains_ + RIGHT].R[0] + (0.35)*(particle[ngrains_ + RIGHT].R[0] - particle[ngrains_ + LEFT].R[0]); // xpov = xsim
      //campos_y = particle[ngrains_ + TOP].R[2]   + (0.40)*(particle[ngrains_ + TOP].R[2] - particle[ngrains_ + BOTTOM].R[2]);  // ypov (vertical) = zsim  
      //campos_z = particle[ngrains_ + BACK].R[1]  - (1.45)*(particle[ngrains_ + FRONT].R[1] - particle[ngrains_ + BACK].R[1]);  // zpov = ysim 
      //}
      campos_x = max[0] + (0.35)*(max[0] - min[0]); // xpov = xsim
      campos_y = max[2] + (0.40)*(max[2] - min[2]); // ypov (vertical) = zsim  
      campos_z = min[1] - (1.45)*(max[1] - min[1]); // zpov = ysim 
      // set filename
      filename.str(""); filename.fill('0'); filename.setf(std::ios::fixed); filename << "DISPLAY/povray-data-" << std::setw(16) << ii << ".pov";
      // print system to povray 
      povrayAnimator.print_system_povray(particle, filename.str(), min[0], max[0], min[1], max[1], min[2], max[2], campos_x, campos_y, campos_z);
    }    
    
    UTIL::print_done(name);
  }

  //--------------------------------------------------------------------------
  // draw for paraview
  //--------------------------------------------------------------------------
  void PostproDisplay::draw_paraview(void) 
  {
    std::string name = "paraview"; UTIL::print_start(name);

    Visualization drawer;

    // print input system
    read_contacts("INPUT/" CONTACTS_INFO_FILE);
    read_particles("INPUT/" BODIES_INFO_FILE);
    drawer.print_paraview_vtp(particle, contacts, particle, "DISPLAY/initial.vtp", -1.0, periodic_);

    // print time dependent data
    file_names(CONTACTS_INFO "-", contactFilenames);
    file_names(BODIES_INFO "-", bodiesFilenames);
    if (0 == bodiesFilenames.size()) {
      print_error_and_exit("No BODIES files inside time window to read. Exiting");
    }
    if (0 == contactFilenames.size()) {
      print_error_and_exit("No CONTACTS files inside time window to read. Exiting");
    }
    print_log("Total files to read : ", bodiesFilenames.size());
    read_particles(bodiesFilenames[0]); particle_init_ = particle;

    std::stringstream filename; 
    drawer.start_paraview_pvd();
    const int size = bodiesFilenames.size();
    for(int ii = 0; ii < size; ii += stride_) {
      double ltime = get_time(bodiesFilenames[ii], BODIES_INFO"-");
      print_log("");
      print_log("Current Time = ", ltime);
      print_log("Final Time   = ", get_time(bodiesFilenames[size-1], BODIES_INFO"-"));
      print_log("Reading from : ", contactFilenames[ii]);
      print_log("Reading from : ", bodiesFilenames[ii]);
      read_contacts(contactFilenames[ii]);
      read_particles(bodiesFilenames[ii]);
      filename.str(""); filename.fill('0'); filename.setf(std::ios::fixed); filename << "DISPLAY/data-" << std::setw(16) << ii << ".vtp";
      drawer.print_paraview_vtp(particle, contacts, particle_init_, filename.str(), ltime, periodic_);
    }    
    drawer.finish_paraview_pvd();

    // print last state
    read_contacts("OUTPUT/" CONTACTS_INFO_FILE);
    read_particles("OUTPUT/" BODIES_INFO_FILE);
    drawer.print_paraview_vtp(particle, contacts, particle_init_, "DISPLAY/final.vtp", -1.0, periodic_);
    
    UTIL::print_done(name);
  }

  //--------------------------------------------------------------------------
  // draw for paraview
  //--------------------------------------------------------------------------
  void PostproDisplay::draw_voronoi(void) 
  {
    std::string name = "voronoi drawing -> gnuplot and povray"; UTIL::print_start(name);

    file_names(CONTACTS_INFO "-", contactFilenames);
    file_names(BODIES_INFO "-", bodiesFilenames);
    print_log("Total files to read : ", bodiesFilenames.size());
    if (0 == bodiesFilenames.size()) {
      print_error_and_exit("No files inside time window to read. Exiting");
    }

    std::stringstream filename; 
    const int size = bodiesFilenames.size();
    for(int ii = 0; ii < size; ++ii) {
      double ltime = get_time(bodiesFilenames[ii], BODIES_INFO"-");
      print_log("Current Time = ", ltime);
      print_log("Final Time = ", get_time(bodiesFilenames[size-1], BODIES_INFO"-"));
      read_particles(bodiesFilenames[ii]);
      voro::container_poly conp(particle[ngrains_ + LEFT].R[0], particle[ngrains_ + RIGHT].R[0], 
				particle[ngrains_ + BACK].R[1], particle[ngrains_ + FRONT].R[1],  
				particle[ngrains_ + BOTTOM].R[2], particle[ngrains_ + TOP].R[2], 
				20, 20 , 20,
				periodic_[0], periodic_[1], periodic_[2], 8); 
      conp.clear();
      for(int id = 0; id < ngrains_; ++id) {
	conp.put(id, particle[id].R[0], particle[id].R[1], particle[id].R[2], particle[id].rad);
      }
      filename.fill('0'); filename.setf(std::ios::fixed, std::ios::floatfield); filename.setf(std::ios::showpoint);
      filename.str(""); filename << "DISPLAY/voronoi_p_" << std::setw(16) << ltime << ".gnu";
      conp.draw_particles(filename.str().c_str());     // OUTPUT the particle positions in gnuplot format
      filename.str(""); filename << "DISPLAY/voronoi_c_" << std::setw(16) << ltime << ".gnu";
      conp.draw_cells_gnuplot(filename.str().c_str()); // OUTPUT the Voronoi cells in gnuplot format
      filename.str(""); filename << "DISPLAY/voronoi_p_" << std::setw(16) << ltime << ".pov";
      conp.draw_particles_pov(filename.str().c_str()); // OUTPUT the particle positions in povray format
      filename.str(""); filename << "DISPLAY/voronoi_c_" << std::setw(16) << ltime << ".pov";
      conp.draw_cells_pov(filename.str().c_str()); // OUTPUT the Voronoi cells in povray format
    }    
    UTIL::print_done(name);
  }

} // namespace DEM
