#include "particle_activator.hpp"

/**
   Reads nactivation_ and niter from INPUT/general_conf. Assures that 
   niter/ngrains >= nactivation (ecnough activation steps).

   Process particles, adding every dead one to indexes_ .

   @param particles - Vector of particles
   @param ngrains - number of grains (particles.size() == ngrains + nwalls)
 */
void 
ParticleActivator::start(const std::vector<Particle> & particles, const int & ngrains)
{
  // read config
  YAML::Node config = YAML::LoadFile("INPUT/general_conf.yaml");
  nactivation_ = std::max(1, config["nactivation"].as<int>());

  // add dead particles to list
  for (int ii = 0; ii < ngrains; ++ii) {
    if (false == particles[ii].alive)
      indexes_.push(ii);
  } 
  print_log("ParticleActivator : Total number of dead grains to be activated = ", indexes_.size());
#if DEBUG
  const int niter = config["niter"].as<int>();
  if (niter < nactivation_*int(indexes_.size())) {
    print_error_and_exit("ParticleActivator : Not enough iterations to activate all dead particles");
  }
#endif // DEBUG
} 

/**
   Activates dead particles, at a given nactivation_ frecuency

   @param iter - Current time iteration
   @param particles - Container of particles
 */

void 
ParticleActivator::activate(const int & iter, std::vector<Particle> & particles)
{
  if (false == indexes_.empty()) {
    if (0 == iter%nactivation_) {
      print_log("Activating idx = ", indexes_.front()); 
      particles[indexes_.front()].alive = true;
      indexes_.pop(); // removes element 
    }
  }
}



