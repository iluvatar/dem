#ifndef __PARTICLE_ACTIVATOR_HPP__
#define __PARTICLE_ACTIVATOR_HPP__

#include "particle.hpp"
#include "utility.hpp"
#include "workspace_dem_config.hpp"
#include "yaml-cpp/yaml.h" // for parsing yaml files
#include <vector>
#include <queue>

/**
   @brief Activates dead particles at a given frecuency 

   This manager activates the dead particles (not walls), each at a time, at a given frecuency 
   (every nactivation_ steps). Configuration is read from INPUT/general_conf . 

   @author William Oquendo, woquendo@gmail.com
 */

class ParticleActivator {
public : // methods
  ParticleActivator()  {};  ///< Default constructor. Does nothing.
  ~ParticleActivator() {};  ///< Default destructor. Does nothing.
  void start(const std::vector<Particle> & particles, const int & ngrains); ///< Initializes the indexes lists and reads configuration
  void activate(const int & iter, std::vector<Particle> & particles);       ///< Activate particles

private : // data
  int nactivation_ = 0;     ///< Activation frecuency (number of iterations)
  std::queue<int> indexes_; ///< Indexes to be activated
};


#endif //__PARTICLE_ACTIVATOR_HPP__


