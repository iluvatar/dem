#ifndef __TIME_INTEGRATION_HPP__
#define __TIME_INTEGRATION_HPP__
/**
   Code for time integration of bodies 
   Leag-frog is used, because its simplicity, its precision, and its 
   ability to control constraints like fixed velocity  
   Maximum walls velocities are also controlled
   translational: requires R, V, F vectors and mass on each object
   rotational: requires PHI, W, T vectors and inerI on each object
*/

//--------------------------------------------------------------------
// headers
#include "utility.hpp"
#include "workspace_dem_config.hpp"
#include "particle.hpp"
#include "boundary_fix.hpp"
#include <fstream>
#include <vector>
#include "yaml-cpp/yaml.h" // for parsing yaml files

namespace DEM {
  namespace time_integ {
    
    //----------------------------------------------------------------
    // TimeIntegrator : Integrates system, applies restrictions to walls
    //----------------------------------------------------------------
    class TimeIntegrator{
    public: // methods
      // default constructor
      TimeIntegrator();
      // integration
      int LeapFrogStart(std::vector<Particle> & bodies, const double & dt, const int & iter, DEM::Fixes::BoundaryFix & bcfixer);
      bool LeapFrogStep(std::vector<Particle> & bodies, const double & dt, const int & iter, const double & phi, DEM::Fixes::BoundaryFix & bcfixer, const double & p, const double & q, const double & dupdate);
      int apply_bcfix(std::vector<Particle> & bodies, const double & dt, const int & iter, const double & phi, DEM::Fixes::BoundaryFix & bcfixer, const double p = 0, const double q = 0);
      int global_fix(std::vector<Particle> & bodies);
      void phithreshold(const double & phithreshold);
      int memory_footprint(void) {
	return sizeof(TimeIntegrator) + sizeof(GLOBAL_DOF_CONTROL_TYPE)*SIZE_DOF_CONTROL + sizeof(bool)*global_dof_control_.size();
      }
   public: // data
      double wmaxvel_ = 1.0e100;
    private: // data
      enum GLOBAL_DOF_CONTROL_TYPE {TYPE01 = 0, TYPE02, TYPE03, SIZE_DOF_CONTROL};
      std::vector<bool> global_dof_control_; 
      double phithreshold_ = 0;
      std::vector<double> dR; ///< to accumulate displacements and test if neighbor list must be updated
    };

  } // namespace time_integ
} // namespace DEM

#endif // __TIME_INTEGRATION_HPP__
