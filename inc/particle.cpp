#include "particle.hpp"

/**
   Reads the particle data by using c-style printing. 
   Checks successfull reading (number of reads, not content)
   @param pf - std::FILE pointer 
 */
void 
Particle::read(std::FILE * pf)
{
  int count;
  count = std::fscanf(pf, "%d%d%d%d%d%d", &uid, &alive, &type, &bcTag, &groupTag, &materialTag); ASSERT(6 == count);
  count = std::fscanf(pf, "%lf%lf%lf", &R0[0], &R0[1], &R0[2]); ASSERT(3 == count);
  count = std::fscanf(pf, "%lf%lf%lf", &R[0], &R[1], &R[2]); ASSERT(3 == count);
  count = std::fscanf(pf, "%lf%lf%lf", &V[0], &V[1], &V[2]); ASSERT(3 == count);
  count = std::fscanf(pf, "%lf%lf%lf", &F[0], &F[1], &F[2]); ASSERT(3 == count);
  count = std::fscanf(pf, "%lf%lf%lf", &F_fixed[0], &F_fixed[1], &F_fixed[2]); ASSERT(3 == count);
  count = std::fscanf(pf, "%lf%lf%lf", &PHI[0], &PHI[1], &PHI[2]); ASSERT(3 == count);
  count = std::fscanf(pf, "%lf%lf%lf", &W[0], &W[1], &W[2]); ASSERT(3 == count);
  count = std::fscanf(pf, "%lf%lf%lf", &T[0], &T[1], &T[2]); ASSERT(3 == count);
  count = std::fscanf(pf, "%lf", &rad); ASSERT(1 == count);
  count = std::fscanf(pf, "%lf", &mass); ASSERT(1 == count);
  count = std::fscanf(pf, "%lf", &inerI); ASSERT(1 == count);
  count = std::fscanf(pf, "%d", &nc); ASSERT(1 == count);
}

/**
   Prints the particle data by using c-style printing
   @param pf - std::FILE pointer 
 */
void 
Particle::print(std::FILE * pf)
{
  std::fprintf(pf, "%10d\t%10d\t%10d\t%10d\t%10d\t%10d\n", uid, alive, type, bcTag, groupTag, materialTag); 
  std::fprintf(pf, "%25.17le\t%25.17le\t%25.17le\n", R0[0], R0[1], R0[2]);
  std::fprintf(pf, "%25.17le\t%25.17le\t%25.17le\n", R[0], R[1], R[2]);
  std::fprintf(pf, "%25.17le\t%25.17le\t%25.17le\n", V[0], V[1], V[2]); 
  std::fprintf(pf, "%25.17le\t%25.17le\t%25.17le\n", F[0], F[1], F[2]);
  std::fprintf(pf, "%25.17le\t%25.17le\t%25.17le\n", F_fixed[0], F_fixed[1], F_fixed[2]);
  std::fprintf(pf, "%25.17le\t%25.17le\t%25.17le\n", PHI[0], PHI[1], PHI[2]); 
  std::fprintf(pf, "%25.17le\t%25.17le\t%25.17le\n", W[0], W[1], W[2]); 
  std::fprintf(pf, "%25.17le\t%25.17le\t%25.17le\n", T[0], T[1], T[2]);
  std::fprintf(pf, "%25.17le\n", rad); 
  std::fprintf(pf, "%25.17le\n", mass);
  std::fprintf(pf, "%25.17le\n", inerI);
  std::fprintf(pf, "%25d\n", nc); 
}

bool 
Particle::operator==(const Particle & other) 
{
  return uid == other.uid &&
    alive == other.alive &&
    type == other.bcTag &&
    groupTag == other.groupTag &&
    materialTag == other.materialTag &&
    R0 == other.R0 &&
    R == other.R &&
    V == other.V &&
    F == other.F &&
    F_fixed == other.F_fixed &&
    PHI == other.PHI &&
    W == other.W &&
    T == other.T &&
    rad == other.rad &&
    mass == other.mass &&
    inerI == other.inerI &&
    nc == other.nc;
}


