#ifndef __FUNCTIONS_HPP__
#define __FUNCTIONS_HPP__
/**
   Defines some common function to be shared 
**/

#include "util/assert.hpp"
#include <vector>
#include <cmath>
#include <tr1/cmath>

double
kgamma_Vmin(const double & x, const double *par);

double 
kgamma_k_X_Vm(const double & x, const double *par);

double 
kgamma_k_Vmean_Vm(const double & x, const double *par);

double 
kgamma_scaled_k(const double & x, const double *par);

double 
kgamma_k_Vm_Vmean(const double & x, const double *par);

double 
kgamma_k_Vm(const double & x, const double *par);

// k = shape, beta = scale
double 
igamma(const double & x, const double * par); 

double 
pgamma(const double & x, const double * par);

double 
qgamma(const double & x, const double * par);

double 
exponential_ramp(const double & x, const double *par);

//------------------------------------
// Stair functor
//------------------------------------
class StairFunctor {
public :
  StairFunctor() {};
  void start(std::vector<double> & par);
  double operator()(const int & iter);
  bool ramps_done(const int & iter);

private:
  double min_     = 0.0;					   //< start value for the stair
  double max_     = 0.0;					   //< Maximum value = min*alpha^{nsteps}
  double alpha_   = 0.0;					   //< Scale factor (typically is equal to 2)
  double deltax_  = 0.0;					   //< Size of each bin on the iter axis
  int nsteps_     = 0;					   //< Number of steps (increments) for the stair
  int niter_      = 0;					   //< Number of total iterations for the whole stair 
  int stair_step_ = 0;					   //< At which step we currently are
  int stair_      = 0;					   //< At which stair we currently are
  int nstair_     = 0;					   //< An array storing the values of the stair, to avoid computing it again and again
  std::vector<double> values;
};


#endif // __FUNCTIONS_HPP__

