#include "stress_tensor.hpp"


void
get_p_q(const Mat3d_t & tensor, double & p, double & q, const int & mode)
{
  // By default, mode == 0, which means default values
  // mode = 0 : default 3d values
  // mode = 1 : 2d values biaxial
  q = p = 0;
  double evals[3] = {0.0};
  eigen_values(tensor, evals[0], evals[1], evals[2]); // at matrix3d.hpp
  if (0 == mode){
    p = (evals[0] + evals[1] + evals[2])/3.0;
    //q = sqrt((pow(evals[0] - evals[1], 2) + pow(evals[0] - evals[2], 2) + pow(evals[1] - evals[2], 2))/2.0);
    // q = (evals[2] - 0.5*(evals[0]+evals[1]))/3.0;
    q = (evals[2] - 0.5*(evals[0]+evals[1])); // removed /3 after suggestion from Andres Barrero, 2017/09/06
  }
  else if (1 == mode) {
    p = 0.5*(evals[2] + evals[0]);
    q = 0.5*(evals[2] - evals[0]);
  } 
}

