#ifndef __PARTICLE__
#define __PARTICLE__

#include "vector3d.hpp"
#include "util/assert.hpp"
#include <iostream>
#include <iomanip>
#include <utility>

/**
   @brief Simulate a simple spherical particle

   The particle have several attributes, going from mechanical (radius, mass, inertia moment),
   to vectorial (position R, velociy F, etc), to some flags (materialTag, groupTag, etc).
   The enum ParticleType allows to use this same particle structure for a planewall with the 
   special radius = 0. 

   @author William Oquendo, woquendo@gmail.com
 */

class Particle {
public:  // data
  enum ParticleType {SPHERE = 0, WALL, NUM_OF_TYPES};   /**< Particle types. Last one stores the number of types, so is not a type */
  int uid           = 0;       /**< Unique id, set by workspace */
  int alive         = 1;       /**< alive (interacting) or not?, default 1 */
  int nc            = 0;       /**< Number of contacts */
  int bcTag         = 0;       /**< for individual boundary conditions, default 0 */
  int groupTag      = 0;       /**< group tag, default 0 (no group) */
  int materialTag   = 0;       /**< material tag, default 0 */
  int type          = SPHERE;  /**< SPHERE, PLANE, default SPHERE */
  double rad        = 0;       /**< particle radius */
  double mass       = 0;       /**< mass */
  double inerI      = 0;       /**< inertia moment */
  Vec3d_t R0;  /**< stores some reference initial position */
  Vec3d_t R;   /**< Position */
  Vec3d_t V;   /**< Velocity */
  Vec3d_t F;   /**< Force */
  Vec3d_t PHI; /**< Angle */
  Vec3d_t W;   /**< Angular velocity  */
  Vec3d_t T;   /**< Torque */
  Vec3d_t F_fixed;   /**< fixed force */
  
  // methods
  /**
     Default constructor. Creates a particle of type SPHERE. Start all default variables.
   */
  Particle() {
    uid = 0; nc = 0; bcTag = 0; groupTag = 0; materialTag = 0; 
    alive = true;
    type = SPHERE;
    R0 = R = V = F = PHI = W = T = F_fixed = Vecnull;
    rad = mass = inerI = 0;
  } ;
  // get functions
  double x(void) {return R[0];};
  double y(void) {return R[1];}; 
  double z(void) {return R[2];};
  double x(void) const {return R[0];};
  double y(void) const {return R[1];};
  double z(void) const {return R[2];};
  double dx(void) const {return R[0] - R0[0];};
  double dy(void) const {return R[1] - R0[1];};
  double dz(void) const {return R[2] - R0[2];};
  // util 
  double max_limit(int dir) { return R[dir] + rad; }; /** Returns the upper boundary value for a given coordinate */
  double min_limit(int dir) { return R[dir] - rad; }; /** Returns the lower boundary value for a given coordinate */
  void reset_force_torque(void) {F = F_fixed; T = Vecnull; nc = 0;};
  // logical
  bool operator<(const Particle & other) const { return uid < other.uid;}
  bool operator<(const Particle & other) { return uid < other.uid;}
  //bool operator==(const Particle & other) const { return uid == other.uid;}
  bool operator==(const Particle & other);
  // i/o c style
  void print(std::FILE * pf);
  void read(std::FILE * pf);

};

#endif
