#include "file_util.hpp"

namespace UTIL {
  
  int 
  OFileStream::open(const std::string & name, const bool & append) {
    if (append) fout_.open(name, std::ios::app);
    else fout_.open(name);
    ASSERT(fout_); 
    ASSERT(fout_.good()); 
    UTIL::set_print_format(fout_);
    return EXIT_SUCCESS;
  }
  
  int 
  OFileStream::close(void) {
    if (fout_.is_open()) fout_.close();
    return EXIT_SUCCESS;
  }

  int 
  OFileStream::print_newline(void) {
    //fout_ << "\n";
    fout_ << std::endl;
    return EXIT_SUCCESS;
  }

} // namespace UTIL
