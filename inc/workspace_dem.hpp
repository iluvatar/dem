#ifndef __WORKSPACE_DEM__
#define __WORKSPACE_DEM__

#include "common_config.hpp"
#include "utility.hpp"
#include "workspace_dem_config.hpp"
#include "particle.hpp"
#include "time_integration.hpp"
#include "cell_list.hpp"
#include "materials.hpp"
#include "interaction.hpp"
#include "workspace_dem_util.hpp"
#include "timer.hpp"
#include "file_util.hpp"
#include "particle_activator.hpp"
#include "boundary_fix.hpp"
#include "stress_tensor.hpp"
#include "yaml-cpp/yaml.h" // for parsing yaml files
#include <iterator>
#include <functional>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <omp.h>

/*
  z
  |       y
  |     /
  |   /
  | /
  |------- x
*/


namespace DEM {

  class WorkspaceDEM{

  public: // methods
    explicit WorkspaceDEM(void);
    ~WorkspaceDEM();
    void evolve(void); 
    void change_radius_mass_inerI(Particle & grain, double rad); 
    double area_planewall(const int & label);
    double thickness_for_wall(const int & label);
    void store_box_limits(double lmin[3], double lmax[3]);
    void copy_periodic_flags(bool array[3]);
    void set_alive_walls(void);
    void apply_periodicity(void); 
    void clear_contacts(void) { Interactor_.clear(); }; 
    int ncontacts(void) { return Interactor_.size(); }; 
    int ngrains(void) { return particle.size() - DEM::NWALLS; }; 

  public: // data 
    std::vector<Particle> particle; // grains + walls
    /// Material constants
    Materials MaterialVars;

  private: // methods
    /// singleton stuff
    WorkspaceDEM(const WorkspaceDEM &);
    WorkspaceDEM & operator=(const WorkspaceDEM &);
    /// Initialization : reading config
    void read_config(void); 
    void read_walls_properties(void); 
    void read_bodies_info(void); 
    void read_contacts_info(void);  
    template <class T> 
    void check_read_param(T & param, const T & new_val, 
			  const std::string & name, const bool & can_change);
    /// printing data
    void print_bodies_info(std::string  filename = "");  
    void print_contacts_info(std::string  filename = ""); 
    /// Initialization 
    void start(void); 
    /// time integration
    void do_time_step(void); 
    /// force 
    void compute_total_force_torque(void); 
    /// utility
    void set_uid(void); 
    bool is_relaxed(const bool & print_info); 
    void print_mean_vel_info(void);
    void print_stress_info(void);
    void print_mean_max_penetration_info(void); 
    void print_screen_info(void);
    void print_memory_info(void);
    void print_logfile_info(void);
    Vec3d_t get_force_from_stress(const int & wd);
    Vec3d_t get_fixed_stress(const int & wd);
    //bool ramps_done(void);
    void check_simul_state(void); // make some test, like particles inside domains
    void change_global_vars(void); // do not activate in production codes
    double packing_fraction(void);

  private: // data
    int dim_ = 0; // only 2 or 3
    int ngrain_ = 0, nplanewall_ = 0, niter_ = 0, nprint_ = 1;
    int nread_runtime_info_ = 1;
    int iter_ = 0, iter0_ = 0;
    double min_rad_ = 0, max_rad_ = 0, mean_rad_ = 0;
    CellList cell_list_; // Cell list object
    /// global dynamic variables
    double dt_ = 0, time_ = 0, time0_ = 0;  
    Vec3d_t G; // gravity
    double GLOBAL_DAMPING_GRAINS_ = 0; //< Units 1/T
    double GLOBAL_DAMPING_WALLS_ = 0;  //< Units 1/T
    double WALLS_MAX_VEL_ = 1.0e100;   //< Maximum velocity for walls
    //double GAMMA_CM_ = 0; // Damping of the center of mass
    double PHI_THRESHOLD_ = -1.0; // Phi threshold to start controlling max velocities after this value
    /// flags
    bool started_ = false, ready_to_print_ = false, run_until_relaxed_ = false;
    bool periodic_[3] = {false};
    bool keep_running_ = false;
    // neighbor list stuff
    int nlistmode_ = 2;  //< nlistmode: 0 = n^2 square algorithm, 1 = celllist, 2 = neighbor list (default) 
    int nlistupdate_ = 100; //< How frequently to update the neighbor / cell lists
    int nneighbormax_ = 18; //< Default  maximum number of neighbors
    // interactions among bodies
    Interaction Interactor_; 
    // Time integration
    time_integ::TimeIntegrator tintegrator_;
    // timer to have the info of total running time
    Timer total_time_counter_, timer_force_, timer_printing_, timer_integration_, timer_nlist_;
    // info logging
    UTIL::OFileStream flog_; 
    // Particles activation, from dead to alive
    ParticleActivator activator_;
    // Boundary conditions fixes, for walls
    Fixes::BoundaryFix bcfixer_;
  };
  
  
} // end namespace DEM

#endif
