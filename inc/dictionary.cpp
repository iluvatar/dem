#include "dictionary.hpp"

namespace dictionary {
  //-----------------------------------------
  // implementations 
  //-----------------------------------------
  void read_file_or_die(BasicDictionary & dict, std::string filename) 
  {
    std::ifstream fin(filename.c_str()); 
    if(!fin) { print_error_and_exit("# BasicDict was unable to read file:  " + filename); }
    dict.clear();
    std::string tmp_key;
    while (fin >> tmp_key) {
      if ('#' == tmp_key[0]) {
	fin.ignore(1024, '\n'); // ignore commented line
	continue;
      }
      else {
	fin >> dict[tmp_key];
	print_log("Read key        = ", tmp_key);
	print_log("Read value[key] = ", dict[tmp_key]);
	fin.ignore(1024, '\n'); // ignore rest of line
      }
    } 
  }
 
  void read_file_or_die(TagDictionary & dict, std::string filename) 
  {
    std::ifstream fin(filename.c_str()); 
    if(!fin) { print_error_and_exit("# TagDict was unable to read file : " + filename); }
    dict.clear();
    std::string tag, conditions;
    while (fin >> tag) {
      if ('#' == tag[0]) {
	fin.ignore(1024, '\n'); // ignore commented line
	continue;
      }
      else {
	getline(fin, conditions, '\n');
	print_log("Read tag        -> ", tag);
	print_log("Read conditions -> ", conditions);
	//fin.ignore(1024, '\n'); // ignore rest of line
	dictionary::add_tagged_values(dict, atoi(tag.c_str()), conditions);
      }
    } 
  }
 
  const double & get_value_or_die(BasicDictionary & dict, const std::string & key)
  {
    BasicDictionary::iterator it = dict.find(key);
    if(dict.end() == it) { print_error("# Key does not exist. Exiting. Key =  ", key); exit(1);}
    return it->second;
  }
  
  void tokenize_or_die(const int tag, std::string & s, std::vector<std::string> & tokenVec)
  {
    tokenVec.clear(); tokenVec.shrink_to_fit();
    replace(s.begin(), s.end(), '=', ' '); 
    transform(s.begin(), s.end(), s.begin(), ::toupper);
    std::stringstream ss(s); std::string buff; 
    while (ss >> buff) tokenVec.push_back(buff);
    if(0 != tokenVec.size()%2) {
      std::cerr << red_init 
	   << "\nTags mus be given in pairs as in \nRx = 0.123  Vy 6.789" << std::endl
	   << "You can ommit the symbol =" << std::endl
	   << "Please check your tag " << tag << " std::string \n" << s << std::endl
	   << "Exiting ..." << color_end << std::endl << std::endl;
      exit(1);
    }
  }
  
  void add_tagged_values(TagDictionary & tag_dict, const int & tag, std::string s)
  {
    std::vector<std::string> tokenVec;
    tokenize_or_die(tag, s, tokenVec);
    tag_dict[tag].clear(); // Clear in case of already existing tag
    const int limit = tokenVec.size() - 2;
    for(int i = 0; i <= limit; i+=2) {
      tag_dict[tag][tokenVec[i]]=std::stod(tokenVec[i+1]);
    }
  }
    
  void print(BasicDictionary & dict)
  {
    for (BasicDictionary::iterator it = dict.begin(); it != dict.end(); ++it) {
      std::cout << std::setw(28) << it->first << std::setw(28) << it->second << std::endl;
    } 
  }

  void print(const TagDictionary & dict, const std::string filename)
  {
    UTIL::OFileStream fout(filename.c_str()); 
    for (TagDictionary::const_iterator it = dict.begin(); it != dict.end(); ++it) {
      fout.print_val(it->first);
      for (BasicDictionary::const_iterator it2 = it->second.begin(); it2 != it->second.end(); ++it2) {
	fout.print_val(it2->first, it2->second);
      } 
      fout.print_newline();
    }
    fout.close();
  }


} // namespace dictionary
