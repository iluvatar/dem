#include "cell_list.hpp"

int
CellList::get_cell_id(const int & ix, const int & iy, const int & iz)
{
  /*
    cellID = ix*ny_*nz_ + iy*nz_ + iz
    ix = cellID/(ny_*nz_);
    iy = (cellID/nz_)%ny_;
    iz = cellID%(nz_);
  */
  if(0 <= ix && ix < n_[0] && 0 <= iy && iy < n_[1] && 0 <= iz && iz < n_[2])
    return ix*nynz_ + iy*n_[2] + iz;
  else
    return INVALID_;
}


void
CellList::get_indexes(const int & cellID, int & ix, int & iy, int & iz)
{
  ix = cellID/(nynz_);
  iy = (cellID/n_[2])%n_[1];
  iz = cellID%(n_[2]);
}


void
CellList::start(const int & nbodies, const double lmin[3], const double lmax[3],
		const double & dmin, const bool periodic[3], const int nneighbormax)
{
  UTIL::print_start("Starting cell list");
  
  // check args
  ASSERT(nbodies > 0);
  for (int dim = 0; dim < 3; ++dim) 
    ASSERT(lmax[dim] > lmin[dim]);

  // print some info
  print_log("dmin = ", dmin);
  print_log_array("current lmin_ = ", lmin_, 3);
  print_log_array("actual  lmin  = ", lmin,  3);
  print_log_array("current lmax_ = ", lmax_, 3);
  print_log_array("actual  lmax  = ", lmax,  3);

  // assign vars
  dmin_ = dmin;
  nbodies_ = nbodies;
  for (int ii = 0; ii < 3; ++ii) {
    periodic_[ii] = periodic[ii];
    lmin_[ii] = lmin[ii];
    lmax_[ii] = lmax[ii];
  }
  NNEIGHMAX_ = nneighbormax;

  // compute internal vars
  // resolution on each dimension
  d_[0] = d_[1] = d_[2] = std::max(dmin_, std::pow((lmax_[0]-lmin_[0])*(lmax_[1]-lmin_[1])*(lmax_[2]-lmin_[2])/nbodies_, 1.0/3.0));
  for (int dim = 0; dim < 3; ++dim) if ( periodic_[dim] ) d_[dim] = dmin_;    // keeps constant resolution on periodic cells // REALLY NEEDED FOR BIPERIODIC !
  print_log_array("d_ = ", d_, 3);
  for (int dim = 0; dim < 3; ++dim) ud_[dim] = 1.0/d_[dim];
  // number of cells per dimension
  for (int dim = 0; dim < 3; ++dim) n_[dim] = std::ceil((lmax_[dim] - lmin_[dim])/d_[dim]);
  print_log_array("n_ = ", n_, 3);
  ASSERT(n_[0] >= 1); ASSERT(n_[1] >= 1); ASSERT(n_[2] >= 1);
  for (int ii = 0; ii < 3; ++ii) if (true == periodic_[ii]) ASSERT(n_[ii] >= 3); // REALLY NEEDED : THIS IS TO FORCE TOO SMALL BI-PERIODIC SYSTEMS
  // number of cells and arrays
  ncells_ = n_[0]*n_[1]*n_[2];
  nynz_ = n_[1]*n_[2];
  print_log("INVALID_ = ", INVALID_);
  print_log("nbodies_ = ", nbodies_);
  print_log("ncells_  = ", ncells_);
  body_cell_.clear(); cell_list_.clear(); next_cell_.clear(); 
  body_cell_.resize(nbodies_, INVALID_); std::fill(body_cell_.begin(), body_cell_.end(), INVALID_);
  cell_list_.resize(ncells_); std::for_each(cell_list_.begin(), cell_list_.end(), std::mem_fun_ref(&list_t::clear));
  next_cell_.resize(ncells_); std::for_each(next_cell_.begin(), next_cell_.end(), std::mem_fun_ref(&blist_t::clear));
  nlist_.resize(nbodies_*NNEIGHMAX_, INVALID_);
  // neighbors
  set_neighbor_id();

  UTIL::print_done("Starting cell list");
}


bool
CellList::ignore_neighbor_step(const int & dim, const int & ii, const int & Dii)
{
  if (false == periodic_[dim]) {                                                // not periodic on dim
    if ( (Dii <= -2) || (Dii >= 2) ) return true;                           // ignore many neighbors when non periodic
    if ( (0 == ii) && (Dii <= -1)  ) return true;                           // do not add right-periodic neighbors
    if ( (n_[dim]-1 == ii) && (Dii >= 1) ) return true;                     // do not add left-periodic neighbors
  }
  else  {                                                //  periodic on dim
    // only add extra neighbors for boundary cells
    if ( (1 <= ii) && (ii <= n_[dim] - 2) && (Dii < -2 || Dii > 2) ) return true;
  }
  return false;
}


/**
   @brief Sets neighbors cells given periodicity 
   cubic neighborhood
   z varies first, then y and then x!!!!
   Let sx, sy and sz be the shifts for the index on each dimension.
   The index for an arbitrary neighbor shifted cell is
   cellIDNEW(sx, sy, sz) = cellID + sx*ny_*nz_ + sy*nz_ + sz
*/
void
CellList::set_neighbor_id(void)
{
  UTIL::print_start("INIT: Setting neighbor list per cell");
  print_log_array("periodic_ = ", periodic_, 3);
  int Dx, Dy, Dz;
  int newix, newiy, newiz;
  int ix, iy, iz;
  const int dxmin = -2; const int dxmax = 2;
  const int dymin = -2; const int dymax = 2;
  const int dzmin = -2; const int dzmax = 2;
  next_cell_.clear();
  next_cell_.resize(ncells_);
  for (ix = 0; ix < n_[0]; ++ix) {
    for (iy = 0; iy < n_[1]; ++iy) {
      for (iz = 0; iz < n_[2]; ++iz) {
	const int cellID = get_cell_id(ix, iy, iz);
	ASSERT(0 <= cellID && cellID < ncells_);
	for (Dx = dxmin; Dx <= dxmax; ++Dx) {
	  if ( ignore_neighbor_step(0, ix, Dx) ) continue;
	  for (Dy = dymin; Dy <= dymax; ++Dy) {
	    if ( ignore_neighbor_step(1, iy, Dy) ) continue;
	    for (Dz = dzmin; Dz <= dzmax; ++Dz) {
	      if ( ignore_neighbor_step(2, iz, Dz) ) continue;
	      newix = (ix + Dx + n_[0])%n_[0];
	      newiy = (iy + Dy + n_[1])%n_[1];
	      newiz = (iz + Dz + n_[2])%n_[2];
	      const int cellIDnext = get_cell_id(newix, newiy, newiz);
	      if (INVALID_ != cellIDnext) {
		ASSERT(0 <= cellIDnext && cellIDnext < ncells_);
		next_cell_[cellIDnext].insert(cellID);
		next_cell_[cellID].insert(cellIDnext);
	      }
	    }
	  }
	}
      }
    }
    print_neighbors("OUTPUT/cellList_neighbors.txt");
  }
  
  print_log("END: Setting neighbor list per cell");
}


int
CellList::update(std::vector<Particle> & bodyArray, const double wmin[3], const double wmax[3])
{
  bool flag_resize = false;
  for (int dir = 0; dir < 3; ++dir) {
    // maximum limits
    if (wmax[dir] > lmax_[dir]) { // expansion
      print_log("max - exp");
      print_log("dir = ", dir);
      print_log("lmax_[dir] = ", lmax_[dir]);
      print_log("wmax[dir] = ", wmax[dir]);
      lmax_[dir] = wmax[dir] + dmin_;
      flag_resize = true;
    }
    else if (wmax[dir] < lmax_[dir] - 3*dmin_) { // contraction
      print_log("max - con");
      print_log("dir = ", dir);
      print_log("lmax_[dir] = ", lmax_[dir]);
      print_log("wmax[dir] = ", wmax[dir]);
      lmax_[dir] = wmax[dir] + 2*dmin_;
      flag_resize = true;
    }
    // minimum limits
    if (wmin[dir] < lmin_[dir]) { // expansion
      print_log("min - exp");
      print_log("dir = ", dir);
      print_log("lmin_[dir] = ", lmin_[dir]);
      print_log("wmin[dir] = ", wmin[dir]);
      lmin_[dir] = wmin[dir] - dmin_;
      flag_resize = true;
    }
    else if (wmin[dir] > lmin_[dir] + 3*dmin_) { // contraction
      print_log("min - con");
      print_log("dir = ", dir);
      print_log("lmin_[dir] = ", lmin_[dir]);
      print_log("wmin[dir] = ", wmin[dir]);
      lmin_[dir] = wmin[dir] - 2*dmin_;
      flag_resize = true;
    }
  }
  // if resizing needed, restart system
  if (true == flag_resize) {
    print_log("Restarting cell list because of re-sizing.");
    start(nbodies_, lmin_, lmax_, dmin_, periodic_, NNEIGHMAX_);
  }
  
  // update cell lists
  for(int bodieID = 0; bodieID < nbodies_; ++bodieID) {
    const int ix = static_cast<int>((bodyArray[bodieID].x() - lmin_[0])*ud_[0]);
    const int iy = static_cast<int>((bodyArray[bodieID].y() - lmin_[1])*ud_[1]);
    const int iz = static_cast<int>((bodyArray[bodieID].z() - lmin_[2])*ud_[2]);
    const int cellID = ix*nynz_ + iy*n_[2] + iz;
    const int oldcellID = body_cell_[bodieID];
    if(cellID < 0 || cellID >= ncells_) {
      print_error("Bad id for particle in cell list.");
      print_error("R = ", bodyArray[bodieID].R);
      print_error("lmin_ = ", lmin_, 3);
      print_error("lmax_ = ", lmax_, 3);
      print_error("Returning error.");
      return EXIT_FAILURE;
    }
    else if(oldcellID != cellID) {
      body_cell_[bodieID] = cellID;                                       // update the cell id
      if (INVALID_ != oldcellID) cell_list_[oldcellID].erase(bodieID);    // remove from old cell
      cell_list_[cellID].insert(bodieID);                                 // put in the cell
    }
  }
  
  return EXIT_SUCCESS;
}



bool CellList::check_state(void)
{
  bool status = true;
  // check body cell id different to invalid
  for(auto & cid : body_cell_) {
    if (INVALID_ == cid) {
      status = false;
      print_error("Body cell id for a body is INVALID!");
      print_error_and_exit("Exiting.");
      break;
    }
  }
  
  int sum = 0;
  for(auto & vec : cell_list_) {
    sum += vec.size();
  }
  if (sum != nbodies_) {
    status = false;
    print_error("Number of bodies inside cells does not equal total number of bodies.");
    print_error("SUM = ", sum);
    print_error_and_exit("Exiting.");
  }
  
  return status;
}

void CellList::print_neighbors(const std::string & fname)
{
  std::ofstream fout(fname); ASSERT(fout);
  fout << "# cid\tix\tiy\tiz\tlist-of-neighbors-id's" << std::endl;
  for(int cid = 0; cid < ncells_; ++cid) {
    int ix, iy, iz;
    get_indexes(cid, ix, iy, iz);
    fout << cid << "\t" << ix << "\t" << iy << "\t" << iz << "\t"; 
    for(const auto & nid : next_cell_[cid]) {
      fout << nid << "\t"; 
    }
    fout << "\n"; 
  }
  fout.close();
}

void CellList::update_nlist(const std::vector<Particle> & particle, const double rskincut)
{
  bool success = true; // this flag allows to check if resize was needed or not

  do {
    std::fill(nlist_.begin(), nlist_.end(), INVALID_);
    success = true;
    
    int start = 0, nneigh = 0;
    for(int id = 0; id < nbodies_; ++id, start += NNEIGHMAX_) {
      nneigh = 0;
      if (false == particle[id].alive) {
	nlist_[start] = 0;
	continue;
      }
      for (const auto & cellid : next_cell_[body_cell_[id]]) {
	for (const auto & jd : cell_list_[cellid]) {
	  if (false == particle[jd].alive) continue;
	  if (jd <= id || jd == INVALID_) continue; // third Newton law will be used
	  Vec3d_t Rij = particle[id].R - particle[jd].R;
	  // Periodic Boundary, see Allen-Tildesley page 30 
	  for (int dir = 0; dir <= 2; ++dir) {
	    if (true == periodic_[dir]) { 
	      double l = lmax_[dir] - lmin_[dir];
	      double change = l*lrint(Rij[dir]/l); 
	      Rij[dir] -= change;
	    } 
	  }
	  if(Rij.squaredNorm() < rskincut*rskincut) {
	    nneigh++;
	    nlist_[id*NNEIGHMAX_+ nneigh] = jd;
	  }
	}
      }
      nlist_[start] = nneigh; // write number of neighbors
      if(nneigh > NNEIGHMAX_) {
	print_warning("WARNING: More neighbors than allowed");
	print_warning("NNEIGHMAX : ", NNEIGHMAX_);
	print_warning("nneigh : ", nneigh);
	NNEIGHMAX_ += 5*(nneigh-NNEIGHMAX_);
	print_warning("Changing NNEIGHMAX to : ", NNEIGHMAX_);
	nlist_.resize(nbodies_*NNEIGHMAX_, INVALID_);
	success = false;
	break; // breaks the int id = 0 loop
      }
    }
  } while (false == success);
}
