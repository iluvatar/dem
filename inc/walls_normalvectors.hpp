#ifndef __WORKSPACE_DEM_WALLS_CONFIG__
#define __WORKSPACE_DEM_WALLS_CONFIG__

#include "workspace_dem_config.hpp"

namespace DEM{ 
  // walls normal vectors
  const Vec3d_t NORMAL_VEC_WALL[NWALLS] = {Vec_uz, -1*Vec_uz, Vec_ux, -1*Vec_ux, Vec_uy, -1*Vec_uy};
}

#endif
