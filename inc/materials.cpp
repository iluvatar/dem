#include "materials.hpp"
#include "utility.hpp"

namespace DEM { 
  double 
  Materials::get_x_for_en(const double & en) {
    if (1 == en) return 0;
    ASSERT(0 <= en && en < 1);
    // NOTE: I need to use reverse operator since the array is sorted in reverse order by definition
    auto lower = std::lower_bound(en_table_.rbegin(), en_table_.rend(), en); 
    if (en_table_.rend() == lower) print_error_and_exit("Cannot find inverse value for en = ", en);
    const auto index = en_table_.size() - std::distance(en_table_.rbegin(), lower);
    return 0.0 + deltaen_*index;
  }

  
  Materials::Materials()
  {
    read_config();
  }

  void 
  Materials::read_config(void) 
  {
    UTIL::print_start("Reading materials info");
    print_log("ATTENTION: Linear force model assumed to compute the damping constant computed from restitution coefficient.");

    YAML::Node config = YAML::LoadFile("INPUT/materials_conf.yaml");
    nmaterials_ = config.size(); ASSERT(nmaterials_ >= 1);

    // resize vectors 
    material.resize(nmaterials_); compMaterial.resize(nmaterials_);
    for (int i = 0; i < nmaterials_; ++i) {
      material[i].resize(TOTALMatVar);
      compMaterial[i].resize(nmaterials_);
    }
    for (int i = 0; i < nmaterials_; ++i) {
      for (int j = 0; j < nmaterials_; ++j) {
	compMaterial[i][j].resize(TOTALMatVar);
      } 
    }
    tag_.resize(nmaterials_, 0); 
    rho_.resize(nmaterials_, 0.0); 
    k_.resize(nmaterials_, 0.0); 
    e_.resize(nmaterials_, 0.0); 
    musf_.resize(nmaterials_, 0.0); 
    musr_.resize(nmaterials_, 0.0); 
    mass_.resize(nmaterials_, 0.0); 
    residual_tag_.resize(nmaterials_, 0); 
    //cohesion
    stressc_.resize(nmaterials_, 0.0); 
    omegac_.resize(nmaterials_, 0.0); 

    // read remaining config
    for (int count = 0; count < nmaterials_; ++count) {
      if (!config[count]["TAG"]) {
	print_error_and_exit("Old materials config found. Please update it by using the script: update_materials_conf_yaml.py which you can find inside the addons subdirectory of the repo. Exiting.");
      }
      tag_[count]  = config[count]["TAG"].as<int>(); 
      rho_[count]  = config[count]["RHO"].as<double>(); 
      k_[count]    = config[count]["K"].as<double>();
      e_[count]    = config[count]["E"].as<double>(); 
      musf_[count] = config[count]["MUSF"].as<double>(); 
      musr_[count] = config[count]["MUSR"].as<double>();
      residual_tag_[count] = (config[count]["RES"]) ?  config[count]["RES"].as<int>() : tag_[count]; // By default, residual = original (same tag, same material)
      //cohesion
      stressc_[count]   = (config[count]["STRESSC"])   ?  config[count]["STRESSC"].as<double>()   : 0.0; // By default, no cohesion
      if (stressc_[count] < 0) print_error_and_exit("Negative cohesive stress detected: IT MUST BE POSITIVE");
      omegac_[count]    = (config[count]["OMEGAC"]    && (stressc_[count] > 0)) ?  config[count]["OMEGAC"].as<double>()    : 0.0; // By default, no cohesion
    }

    // print read config 
    std::ofstream fout("OUTPUT/materials_conf.yaml");
    fout << config;
    fout.close();

    UTIL::print_done("Materials read. Do not forget to start the materials class with the particles.");
  }

  void 
  Materials::start(const std::vector<Particle> & particle, const std::string & outputfn) 
  {
    UTIL::print_start("Starting material class ...");    

    // Improving calculation of restitution coefficient
    const int nbins = 50000;			  // table resolution 
    en_table_.resize(nbins, 0);
    deltaen_ = (M_SQRT1_2 - 0.0)/nbins;
    for (int ii = 0; ii < nbins; ++ii) {
      double x = 0.0 + ii*deltaen_;
      double aux = std::sqrt(1 - (x*x));
      en_table_[ii]  = std::exp(-x*M_PI/aux);     // Original simple value: see Radjai book
      // correction from Poeschel : Coefficient of restitution and linear--dashpot model revisited
      en_table_[ii] *= std::exp((x/aux)*std::atan(2.0*x*aux/sqrt(1-(2.0*x*x)))); 
    }

    // compute the average mass per material tag
    std::vector<int> counter(nmaterials_, 0);
    for (const auto & body : particle) {
      mass_[body.materialTag] += body.mass;
      ++counter[body.materialTag];
    }
    for (int count = 0; count < nmaterials_; ++count) {
      if (counter[count] > 0) mass_[count] /= counter[count];
      else mass_[count] = 1.0e-16;
    }
    // create single materials
    for (int count = 0; count < nmaterials_; ++count) {
      create_material(tag_[count], rho_[count], k_[count], e_[count], mass_[count], musf_[count], musr_[count], stressc_[count], omegac_[count]);
    }
    // Assert existence of default, tags, and residual materials 
    ASSERT(check_existence(0)); // assures existence of the default bodies tag 
    for (const auto & ii : tag_)
      ASSERT(check_existence(ii)); 
    for (const auto & ii : residual_tag_)
      ASSERT(check_existence(ii)); 
    // create composite materials
    create_composite_materials();
    print_materials_info(outputfn);
    UTIL::print_done("Starting material class.");
    UTIL::print_done("Read material data printed to " + outputfn);
    en_table_.clear(); en_table_.shrink_to_fit();// free memory
  }

  void 
  Materials::create_material(int idtag, double rho, double k, double e, double mass, double musf, double musr, double stressc, double omegac)
  {
    UTIL::print_start("Materials:: Creating material ...");
    print_log("     id = ", idtag);
    // reserve memory
    material[idtag].resize(TOTALMatVar);
    // new values
    ASSERT(0 < e && e <= 1);
    // see RADJAI : Dimensional Analysis, page 202
    //const double zita = std::pow(1.0 + M_PI*M_PI/SQUARE(2*std::log(e)), -0.5);
    //double gamma = (e < 1) ? zita*std::sqrt(2.0*mass*k) : 0;    
    //print_log("gamma   = ", gamma);
    //ASSERT(0 <= gamma && gamma < std::sqrt(2.0*k*mass));
    double gamma = 2*get_x_for_en(e)*std::sqrt(2.0*k*mass); // Poeschel Correction: Coefficient of restitution and linear--dashpot model revisited 
    print_log("gamma   = ", gamma);
    print_log("idtag   = ", idtag);
    print_log("mass    = ", mass);
    print_log("k       = ", k);
    print_log("e       = ", e);
    print_log("gamma   = ", gamma);
    print_log("gamma_c = ", std::sqrt(2.0*mass*k));
    material[idtag][RHO] = rho;
    material[idtag][K] = k;
    material[idtag][E] = e;
    material[idtag][MASS] = mass;
    material[idtag][GAMMA] = gamma;
    ASSERT(musf >= 0);
    ASSERT(musr >= 0);
    material[idtag][MUSF] = musf;  
    material[idtag][MUSR] = musr;
    material[idtag][STRESSC] = stressc;
    material[idtag][OMEGAC] = omegac;
    set_additional_mat_vars(material[idtag]); 
    UTIL::print_done("Materials:: Creating material.");
  }

  void
  Materials::create_composite_materials(void) 
  {
    UTIL::print_start("Materials:: Creating composite materials");
    for(int idtag = 0; idtag < nmaterials_; ++idtag) {
      for(int jdtag = idtag; jdtag < nmaterials_; ++jdtag) {
	compute_comp_material(idtag, jdtag);
	compMaterial[jdtag][idtag] = compMaterial[idtag][jdtag];
      }
    }
    UTIL::print_done("Materials:: Creating composite materials");
  }

  void 
  Materials::compute_comp_material(int idtag, int jdtag)
  {
    MatVar s;
    s = RHO;   compMaterial[idtag][jdtag][s] = 2*reduced_value(material[idtag][s], material[jdtag][s]);
    s = K;     compMaterial[idtag][jdtag][s] = 2*reduced_value(material[idtag][s], material[jdtag][s]);
    s = E;     compMaterial[idtag][jdtag][s] = std::min(material[idtag][s], material[jdtag][s]);
    s = MASS;  compMaterial[idtag][jdtag][s] = 2*reduced_value(material[idtag][s], material[jdtag][s]);
    // s = GAMMA; compMaterial[idtag][jdtag][s] = 0.5*(material[idtag][s] + material[jdtag][s]);
    // Poeschel Correction: Coefficient of restitution and linear-dashpot model revisited 
    s = GAMMA; compMaterial[idtag][jdtag][s] = 2*get_x_for_en(compMaterial[idtag][jdtag][E])*
		 std::sqrt(2.0*compMaterial[idtag][jdtag][K]*compMaterial[idtag][jdtag][MASS]);
    s = MUSF;    
    if (jdtag > idtag && 1.0e-6 > material[jdtag][s]) // only zero if walls (jdtag in general) has zero friction
      compMaterial[idtag][jdtag][s] = 0; 
    //if (1.0e-6 > material[idtag][s] || 1.0e-6 > material[jdtag][s]) 
    //compMaterial[idtag][jdtag][s] = 0; 
    else 
      compMaterial[idtag][jdtag][s] = std::max(material[idtag][s], material[jdtag][s]);
    s = MUSR;  
    if (jdtag > idtag && 1.0e-6 > material[jdtag][s]) // only zero if walls (jdtag in general) has zero friction
      compMaterial[idtag][jdtag][s] = 0; 
    //if (1.0e-6 > material[idtag][s] || 1.0e-6 > material[jdtag][s]) 
    //compMaterial[idtag][jdtag][s] = 0; 
    else 
      compMaterial[idtag][jdtag][s] = std::max(material[idtag][s], material[jdtag][s]);
    
    // cohesion
    s = STRESSC;   compMaterial[idtag][jdtag][s] = 2*reduced_value(material[idtag][s], material[jdtag][s]);
    s = OMEGAC;    compMaterial[idtag][jdtag][s] = 2*reduced_value(material[idtag][s], material[jdtag][s]);

    set_additional_mat_vars(compMaterial[idtag][jdtag]);
  }
  
  template <typename T>
  void
  Materials::set_additional_mat_vars(T & mat) 
  {    
    UTIL::print_start("Materials::     Setting additional material variables ...");
    // tangential hardness
    //const double scale1 = 4.0/5.0; // (2-2v)/(2-v) = 1 - v/(2-v) Agnolin, Roux, et al, Phys. Rev. E. 76, 061302, 2007
    const double scale1 = 0.2; // Luding, Cohesive, frictional powders: contact models for tension, Granular Matter, 2008
    const double scale2 = scale1/2; // Luding, Cohesive, frictional powders: contact models for tension, Granular Matter, 2008
    mat[KTF] = mat[K]*scale1; 
    mat[KTR] = mat[K]*scale2; 

    const double ratio_mukmus = 1.0; // if smaller, the effective limit friction angle is larger, this should be investigated
    mat[MUKF] = mat[MUSF]*ratio_mukmus; 
    mat[MUKR] = mat[MUSR]*ratio_mukmus; 

    // Tangential Restitution coefficient -> gammat
    // et = en
    //mat[GAMMATF] = mat[GAMMA]*std::sqrt(scale1); // fact = sqrt(K_factor), rest_tangential = rest_normal
    //mat[GAMMATR] = mat[GAMMA]*std::sqrt(scale2); // fact = sqrt(K_factor), rest_tangential = rest_normal
    // et < en, scaled with muk
    mat[GAMMATF] = mat[MUKF]*mat[GAMMA]*std::sqrt(scale1); // fact = sqrt(K_factor), rest_tangential = rest_normal
    mat[GAMMATR] = mat[MUKF]*mat[GAMMA]*std::sqrt(scale2); // fact = sqrt(K_factor), rest_tangential = rest_normal
    // FIXED et min
    //const double et = std::min(0.10, mat[E]); // min(0.10, en)
    //double gamma; 
    //gamma = (et < 1) ? std::sqrt(2.0*mat[MASS]*mat[KTF]/(1.0 + M_PI*M_PI/(4*std::log(et)*std::log(et)))) : 0;
    //mat[GAMMATF] = std::max(gamma, mat[GAMMA]);
    //gamma = (et < 1) ? std::sqrt(2.0*mat[MASS]*mat[KTR]/(1.0 + M_PI*M_PI/(4*std::log(et)*std::log(et)))) : 0;
    //mat[GAMMATR] = std::max(gamma, mat[GAMMA]);

    // Torsion : DEACTIVATED at contact forces, it is too unstable
    mat[KTO] = mat[KTF]; //std::max(mat[KTF], mat[KTR]);
    mat[GAMMATO] = mat[GAMMATF]; // std::max(mat[GAMMATF], mat[GAMMATR]);
    mat[MUSO] = mat[MUSF]; //std::max(mat[MUSF], mat[MUSR]);  
    mat[MUKO] = mat[MUSO]*ratio_mukmus; //mat[MUSO]*ratio_mukmus; 

    UTIL::print_done("Materials::     Setting additional material variables ...");
  }
  template void Materials::set_additional_mat_vars<Materials::BasicMat_t>(Materials::BasicMat_t &); 

  void 
  Materials::print_materials_info(const std::string & filename) 
  {
    UTIL::OFileStream fout(filename.c_str());
    fout.print("# START : MATERIALS INFO");
    for (int i = 0; i < nmaterials_; ++i) {
      fout.print("Simple Material Tag : ", i);
      fout.print("RHO   = ", material[i][RHO]);
      fout.print("K     = ", material[i][K]);
      fout.print("E     = ", material[i][E]);
      fout.print("GAMMA = ", material[i][GAMMA]);
      fout.print("MUSF  = ", material[i][MUSF]);
      fout.print("MUSR  = ", material[i][MUSR]);
      fout.print("STRESSC  = ", material[i][STRESSC]);
      fout.print("OMEGAC  = ", material[i][OMEGAC]);
    }
    for (int i = 0; i < nmaterials_; ++i) {
      for (int j = 0; j < nmaterials_; ++j) {
	fout.print(" Composite Material Tag i : ", i);
	fout.print(" Composite Material Tag j : ", j);
	fout.print("RHO   = ", compMaterial[i][j][RHO]);
	fout.print("K     = ", compMaterial[i][j][K]);
	fout.print("E     = ", compMaterial[i][j][E]); 
	fout.print("GAMMA = ", compMaterial[i][j][GAMMA]); 
	fout.print("MUSF  = ", compMaterial[i][j][MUSF]); 
	fout.print("MUSR  = ", compMaterial[i][j][MUSR]);
	fout.print("STRESSC  = ", compMaterial[i][j][STRESSC]);
	fout.print("OMEGAC  = ", compMaterial[i][j][OMEGAC]);
      } 
    } 
    fout.print("# END   : MATERIALS INFO");
    fout.close();
  }
  
} // namespace DEM 

