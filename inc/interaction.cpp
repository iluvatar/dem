#include "interaction.hpp"
#include "walls_normalvectors.hpp"

namespace DEM {
  
  
  int Interaction::size(void) 
  { 
#if NEW_CONTACTS
    int sum = 0; 
    for (const auto & clist : contacts_) 
      for (const auto & c : clist)
	if (c.second.delta_ > EPS_DELTA_) sum++;
    return sum; 
#else
    return contacts_.size(); 
#endif
  } 

  int Interaction::full_size(void) { 
#if NEW_CONTACTS
    return ncontacts();
#else 
    return contacts_.size(); 
#endif
  }
  
  int Interaction::ncontacts(void) { 
#if NEW_CONTACTS  
    int sum = 0; 
    for (const auto & clist : contacts_) 
      sum += clist.size(); 
    return sum; 
#else 
    return contacts_.size();
#endif
  }
  
  void Interaction::clear(void) { 
#if NEW_CONTACTS  
    for (auto & clist : contacts_) 
      clist.clear();
#else
    contacts_.clear(); 
#endif
  } 
 
  void Interaction::init_pointers(const std::vector<Particle> & particle) 
  {
#if NEW_CONTACTS  
    for (auto & clist : contacts_) {
      for (auto & c : clist) {
	c.second.ptr1_ = const_cast<Particle *>(&particle[c.second.uid1_]);
	c.second.ptr2_ = const_cast<Particle *>(&particle[c.second.uid2_]);
      }
    }
#else
    for (auto it = contacts_.begin(); it != contacts_.end(); ++it) {
      it->second.ptr1_ = const_cast<Particle *>(&particle[it->second.uid1_]);
      it->second.ptr2_ = const_cast<Particle *>(&particle[it->second.uid2_]);
    }
#endif
  }
  
  void Interaction::pre_reset(void)
  {
#if NEW_CONTACTS  
    for (auto & clist : contacts_) {
      for (auto & c : clist) {
	c.second.pre_reset(); // delta = 0, updated = false
      }
    }
#else
    for (auto & c : contacts_) {
      c.second.pre_reset(); // delta = 0, updated = false
    }
#endif
  }

  void Interaction::particle_particle_activation(CellList & cell_list, std::vector<Particle> & particle, const int nlistmode)
  {
    int id;
    const int ngrains = particle.size() - DEM::NWALLS; 
    double delta;
    Vec3d_t Normal; 

    if (0 == nlistmode) {
      // particle - particle (all particles asumed alive)
      ///*// ALL TO ALL : O(N*N)
      for ( id = 0; id < ngrains; ++id ) {
	if (false == particle[id].alive) continue;
	for ( int jd = id + 1; jd < ngrains; ++jd ) {
	  if (false == particle[jd].alive) continue;
	  distance_ss_(particle[id], particle[jd], delta, Normal);
	  if (delta > EPS_DELTA_) { 
#if NEW_CONTACTS  
	    contacts_[id][jd].pre_setup(&particle[id], &particle[jd], delta, Normal);
#else
	    contacts_[get_uid(id, jd)].pre_setup(&particle[id], &particle[jd], delta, Normal);
#endif
	  }
	}      
      }//*/
    }
    else if (1 == nlistmode) {
      ///*// CELL LISTS FOR PARTICLE - PARTICLE : O (N*log(N)) 
      // Parallel openmp not used because is slower.
      // The main problem is in the access to the presetup, it modifies too much info, making it critical
      for(int id = 0; id < ngrains; ++id) {
	if (false == particle[id].alive) continue;
	for (const auto & cellid : cell_list.next_cell_[cell_list.body_cell_[id]]) {
	  for (const auto & jd : cell_list.cell_list_[cellid]) {
	    if (false == particle[jd].alive) continue;
	    if (jd <= id) continue;
	    delta = 0;
	    distance_ss_(particle[id], particle[jd], delta, Normal);
	    if (delta > EPS_DELTA_ ) { 
#ifdef DEBUG
	      ASSERT(id == particle[id].uid);
	      ASSERT(jd == particle[jd].uid);
#endif
#if NEW_CONTACTS  
	      contacts_[id][jd].pre_setup(&particle[id], &particle[jd], delta, Normal);
#else
	      contacts_[get_uid(id, jd)].pre_setup(&particle[id], &particle[jd], delta, Normal);
#endif
	    }
	  }
	}
      }
      //*/
    }
    else if (2 == nlistmode) {
      ///*
      // using nlist
      // omp stuff commented, not clear if this works., the modification to both the contact and particles make this cumbersome for omp
      // #pragma omp parallel for private(delta, Normal) 
      for(id = 0; id < ngrains; ++id) {
	const int nneigh = cell_list.nlist_[id*cell_list.NNEIGHMAX_];
	for(int jdx = 1; jdx <= nneigh; ++jdx){
	  int jd = cell_list.nlist_[id*cell_list.NNEIGHMAX_ + jdx];
	  distance_ss_(particle[id], particle[jd], delta, Normal);
	  if (delta > EPS_DELTA_ ) { 
	    contacts_[get_uid(id, jd)].pre_setup(&particle[id], &particle[jd], delta, Normal);
	  }
	}
      }
      //*/
    }
  }

  void Interaction::particle_wall_activation(std::vector<Particle> & particle)
  {
    //#ifdef DEBUG
    static Timer timer2_;
    timer2_.set_name("Particle-wall contact activation");
    timer2_.start_timing();
    //#endif

    int id, jd;
    const int ngrains = particle.size() - DEM::NWALLS; 
    double delta;
    Vec3d_t Normal; 
    
    ///*// particle - wall
    for (jd = ngrains + BOTTOM; jd <= ngrains + FRONT; ++jd) {  // add walls to list
      if (false == particle[jd].alive) continue;
      Normal = NORMAL_VEC_WALL[jd-ngrains];
      for (id = 0; id < ngrains; ++id) { 
	if (false == particle[id].alive) continue;
	delta = 0;
	distance_sw_(particle[id], particle[jd], delta, Normal);
	if (delta > EPS_DELTA_) {
#if NEW_CONTACTS  
	  contacts_[id][jd].pre_setup(&particle[id], &particle[jd], delta, Normal); 
#else
	  contacts_[get_uid(id, jd)].pre_setup(&particle[id], &particle[jd], delta, Normal); 
#endif
	}
      }
    }//*/
    
    //#ifdef DEBUG
    timer2_.stop_timing();
    //#endif
  }
  
  void Interaction::compute_collisions(CellList & cell_list, std::vector<Particle> & particle, const double & dt, const int nlistmode)
  {
    static Timer timer_distances_; timer_distances_.set_name("Force::Distance time - particles");
    static Timer timer_distances_walls_; timer_distances_walls_.set_name("Force::Distance time - walls");
    static Timer timer_inner_force_; timer_inner_force_.set_name("Force::Inner force");

    // reset contacts
    pre_reset();
    
    // DISTANCE COMPUTATIONS : Activation of contacts
    timer_distances_.start_timing();
    particle_particle_activation(cell_list, particle, nlistmode);
    timer_distances_.stop_timing();
    timer_distances_walls_.start_timing();
    particle_wall_activation(particle);
    timer_distances_walls_.stop_timing();
    
    // FORCE COMPUTATIONS with activated contacts
    //#ifdef DEBUG
    timer_inner_force_.start_timing();
    //#endif

    int active_contacts = 0;
#if NEW_CONTACTS  
    for (auto & clist : contacts_) {
      for (auto & c : clist) {
	active_contacts += c.second.collide(materials_, dt);
      }
    }
#else
    for(auto & c : contacts_) {
      active_contacts += c.second.collide(materials_, dt);
    }
    // try with buckets
    // see: https://stackoverflow.com/questions/26034642/openmp-gnu-parallel-for-an-unordered-map
    // #pragma omp parallel for reduction(+:active_contacts) // some test shows this does not improve the time
    //for (unsigned int bid = 0; bid < contacts_.bucket_count(); ++bid) {
    //  for(auto it = contacts_.begin(bid); it != contacts_.end(bid); it++) {
    //active_contacts += it->second.collide(materials_, dt);
    //}
    //}
 
 #endif

    timer_inner_force_.stop_timing();
    
    // delete not updated contacts!
#if NEW_CONTACTS  
    if ((ncontacts() > 10) && (active_contacts <= 0.90*ncontacts())) {
      clear_non_active();
    }
#else
    if ((contacts_.size() > 10) && (active_contacts <= 0.90*contacts_.size())) { 
      clear_non_active();
    }
#endif
  }

  void Interaction::clear_non_active(void) 
  { 
#if NEW_CONTACTS  
    for (auto & clist : contacts_) {
      for (auto it = clist.begin(); it != clist.end(); ) {
	if ((false == it->second.updated_) || (EPS_DELTA_ >= it->second.delta_) ) { 
	  clist.erase(it++);
	} else {
	  ++it;
	}
      }
    }
#else
    for (auto it = contacts_.begin(); it != contacts_.end(); ) {
      if ((false == it->second.updated_) || (EPS_DELTA_ >= it->second.delta_) ) { 
	contacts_.erase(it++);
      } else {
	++it;
      }
    }
#endif
  } 


  void Interaction::max_mean_penetration(double & max, double & mean)
  {
    mean = 0; max = 0; 
    int ncontacts = 0;
#if NEW_CONTACTS  
    for (auto & clist : contacts_) {
      for (auto it = clist.begin(); it != clist.end(); ++it) {
	const double delta = it->second.delta();
	if (delta > EPS_DELTA_) {
	  mean += delta;
	  if (delta > max) max = delta;
	  ++ncontacts;
	}
      }
    }
#else
    for(auto it = contacts_.begin(); it != contacts_.end(); ++it) {
      const double delta = it->second.delta();
      if (delta > EPS_DELTA_) {
	mean += delta;
	if (delta > max) max = delta;
	++ncontacts;
      }
    }
#endif

    if (ncontacts > 0) mean /= ncontacts;
  }
  
  bool Interaction::check_small_penetrations(const double & l) 
  {
#if NEW_CONTACTS  
    for (auto & clist : contacts_) {
      for (auto it = clist.begin(); it != clist.end(); ++it) {
	if (it->second.delta() > l) {
	  print_error("Printing only first ocurrence ...");
	  print_error("l     = ", l);
	  print_error("delta = ", it->second.delta());
	  print_error("delta/l = ", it->second.delta()/l);
	  print_error("id     = ", it->second.uid1_);
	  print_error("jd     = ", it->second.uid2_);
	  return false;
	}
      }
    }
#else
    for(auto it = contacts_.begin(); it != contacts_.end(); ++it) {
      if (it->second.delta() > l) {
	print_error("Printing only first ocurrence ...");
	print_error("l     = ", l);
	print_error("delta = ", it->second.delta());
	print_error("delta/l = ", it->second.delta()/l);
	print_error("id     = ", it->second.uid1_);
	print_error("jd     = ", it->second.uid2_);
	return false;
      }
    }
#endif
    return true;
  }

  void Interaction::sum_force_torque(double & sumFc, double & sumTc) 
  {
    sumFc = sumTc = 0;
#if NEW_CONTACTS  
    for (auto & clist : contacts_) {
      for (auto it = clist.begin(); it != clist.end(); ++it) {
	if (it->second.delta() > EPS_DELTA_) {
	  sumFc += it->second.F().norm();
	  sumTc += it->second.T().norm();
	}
      }
    }
#else
    double avgFc = 0, avgTc = 0;
    int count = 0;
    for (const auto & pair: contacts_) {
      if (pair.second.delta() > EPS_DELTA_) {
	avgFc += pair.second.F().norm();
	avgTc += pair.second.T().norm();
	count++;
      }
    }
    avgFc /= (count > 1) ? count : 1;
    avgTc /= (count > 1) ? count : 1;

    for (const auto & pair: contacts_) {
      if (pair.second.delta() > EPS_DELTA_) {
	if (pair.second.F().norm() > 1.0e-5*avgFc) sumFc += pair.second.F().norm();
	if (pair.second.T().norm() > 1.0e-5*avgTc) sumTc += pair.second.T().norm();
      }
    }
#endif
  }
  
  // count sliding contacts
  int Interaction::nsliding(const std::vector<Particle> & particle, const int & ncmin) 
  {
    if (0 == ncontacts()) return 0;
    int ns = 0; 

    // compute mean force
    double meanFn = 0, meanFt = 0;
#if NEW_CONTACTS  
    for (auto & clist : contacts_) {
      for (auto it = clist.begin(); it != clist.end(); ++it) {
	if (it->second.delta() > EPS_DELTA_) {
	  meanFn += it->second.normFn_;
	  meanFt += it->second.Ft().norm();
	}
      }
    }
#else
    for (const auto & pair: contacts_) {
      if (pair.second.delta() > EPS_DELTA_) {
	meanFn += pair.second.normFn_;
	meanFt += pair.second.Ft().norm();
      }
    }
#endif
    meanFn /= contacts_.size();
    meanFt /= contacts_.size();
    
    // Check contacts marked as sliding candidates but only count those with large force and nc
#if NEW_CONTACTS  
    for (auto & clist : contacts_) {
      for (const auto & pair : clist) {
	if (particle[pair.second.uid1_].nc >= ncmin && particle[pair.second.uid2_].nc >= ncmin) { // not a floating particle
	  if (pair.second.normFn_ > 1.0e-2*meanFn && pair.second.Ft().norm() > 1.0e-2*meanFt) { // ignores small forces  
	    if (pair.second.sliding_) { // contact has been marked as sliding
	      ++ns;
	    }
	  }
	}
      }
    }
#else
    for (const auto & pair : contacts_) {
      if (particle[pair.second.uid1_].nc >= ncmin && particle[pair.second.uid2_].nc >= ncmin) { // not a floating particle
	if (pair.second.normFn_ > 1.0e-2*meanFn && pair.second.Ft().norm() > 1.0e-2*meanFt) { // ignores small forces  
	  if (pair.second.sliding_) { // contact has been marked as sliding
	    ++ns;
	  }
	}
      }
    }
#endif
    return ns; 
  }
    

  /**
     @brief Resets the residual of contacts if this is an initial simulation, deduced from time0_ == 0 
   **/
  void 
  Interaction::reset_residual_flag(const double & time0)
  {
    if (std::fabs(time0) < 1.0e-6) {
      print_log("Resetting residual flag since this simulaiton is starting from time 0");
      for (auto & c : contacts_)
	c.second.residual_ = false;
    }
  }

  bool Interaction::check_forces(const std::vector<Particle> & particle)
  {
    bool status = true;
#if NEW_CONTACTS  
#else
    std::vector<Vec3d_t> F(particle.size()); std::fill(F.begin(), F.end(), Vecnull);
    for (auto it = contacts_.begin(); it != contacts_.end(); ++it) {
      F[it->second.uid1_] += it->second.F();
      F[it->second.uid2_] -= it->second.F();
    }
    const int ngrains = particle.size() - DEM::NWALLS;
    for (int id = 0; id < ngrains; ++id) {
      if (particle[id].F.squaredNorm() > 1.0e-16 && (particle[id].F - F[id]).squaredNorm() > 1.0e-8*particle[id].F.squaredNorm()) {
	status = false;
#if DEBUG 
	print_warning("Forces are not correctly read and set.");
	print_warning("ID = ", id);
	print_warning("particle[ID].F = ", particle[id].F);
	print_warning("F[ID]          = ", F[id]);
	print_warning("DIFFERENCE     = ", (particle[id].F - F[id]).norm());
#endif
      }
    }
#endif
    return status;
  }



  // I/O FUNCTIONS - C-Style
#if !(WITH_NETCDF)
  void 
  Interaction::print(const std::string & filename)
  {
    std::FILE * fp = std::fopen(filename.c_str(), "w");
#if NEW_CONTACTS  
    for (auto & clist : contacts_) {
      for (auto it = clist.begin(); it != clist.end(); ++it) {
	it->second.print(fp);
      }
    }	
#else
    for(auto it = contacts_.begin(); it != contacts_.end(); ++it) {
      it->second.print(fp);
    }
#endif
    std::fclose(fp);
  }

  void 
  Interaction::read(const std::string & filename)
  {						
    // c-style
    std::FILE * fp = std::fopen(filename.c_str(), "r");
    Contact contact;
    while (!std::feof(fp)) {
      contact.read(fp);
      if (!std::feof(fp)) {
#if NEW_CONTACTS  
	contacts_[contact.uid1_][contact.uid2_] = contact;
#else
	long uid = get_uid(contact.uid1_, contact.uid2_); 
	contacts_[uid] = contact; 
	ASSERT(contact.uid() == uid);
#endif
      }
    }
    std::fclose(fp);
  }
#endif // !(WITH_NETCDF)



} // end of namespace DEM

