/**
   Uses the (unsupported) NonLinearOptimization Eigen module
   to compute a non-linear fit using the LevenbergMarquardt alg.
   Functions to be fitted should follow the form of the typedef.
   The key function to use is : nlfit
*/

#include "nlfit.hpp"

namespace NLFIT {
  //------------------------------------------------------------------------------------
  /** 
      @param npar - Number of parameters (to be optimized)
      @param ndata - Number of data on xi or yi
      @param *xdata - Data on x axis
      @param *ydata - Data on y axis
      @param function - Function to be fitted. Its type should follow the pattern of the typedef (see below)
      @param *param - Initial guess for the parameters and where their final values are going to be stored
      @param *param_err - Array to store the errors for the fitted parameters
      @param npartotal - Total number of parameters. Only the first npartotal - npar + 1 are modified, the remaining are kept constant
  */
  int nlfit(const int & npar, const int & ndata, const double *xdata, const double *ydata, 
	    Function_t function, double *param, double *param_err, const int & npartotal)
  {
    if (npar > npartotal) {
      std::cerr << "NLFIT : ERROR : npar > npartotal " << std::endl;
      return EXIT_FAILURE;
    }
    if (ndata <= 0) {
      std::cerr << "NLFIT : ERROR : ndata <= 0 (ndata =  " << ndata << ")" << std::endl;
      return EXIT_FAILURE;
    }
      
    try {
      MaskedVector xparam;
      xparam.resize(npartotal);
      xparam.public_size_ = npar;
      for (int ii = 0; ii < npartotal; ++ii) 
	xparam[ii] = param[ii];

      // do the computation
      lmdif_functor functor;
      functor.init(npar, ndata, xdata, ydata, function);
      Eigen::NumericalDiff<lmdif_functor> numDiff(functor);
      Eigen::LevenbergMarquardt<Eigen::NumericalDiff<lmdif_functor> > lm(numDiff);
      lm.minimize(xparam);
      for (int ii = 0; ii < npar; ++ii) 
	param[ii] = xparam[ii];
      
      std::cout << "# FITTING: Blue norm = " << lm.fvec.blueNorm() << std::endl;
      
      // compute errors with covariance matrix
      double fnorm, covfac;
      fnorm = lm.fvec.blueNorm();
      covfac = fnorm*fnorm/(ndata-npar);
      Eigen::internal::covar(lm.fjac, lm.permutation.indices()); // TODO : move this as a function of lm
      for (int ii = 0; ii < npar; ++ii) 
	param_err[ii] = covfac*lm.fjac(ii, ii);
      
      // CHECKINGS
      // check if small blue norm
      if (lm.fvec.blueNorm() > 1.0) return EXIT_FAILURE;
      // check for  small errors: maximum of 40%
      for (int ii = 0; ii < npar; ++ii) {
	if (std::fabs(param_err[ii]/param[ii]) > 0.40)
	  return EXIT_FAILURE;
      }
      // check if all errors are zero (error)
      bool all_zero = true;
      for (int ii = 0; ii < npar; ++ii) {
	if (std::fabs(param_err[ii]) != 0) {
	  all_zero = false;
	  break;
	}
      }
      if (true == all_zero) return EXIT_FAILURE;
      
      return EXIT_SUCCESS;
    } // try
    catch (const std::exception & e) {
      std::cerr << "Exception raised on nlfit" << std::endl;
      std::cerr << e.what() << std::endl;
    }
    return EXIT_FAILURE; 
  }
} // namespace NLFIT

