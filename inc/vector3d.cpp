#include "vector3d.hpp"

#if !(EIGEN) 

//inline functions MUST be defined here
// Metodos de la clase Vector3D

inline
Vector3D & 
Vector3D::operator=(const REAL num)
{
  z = y = x = num;
  return *this;
}

inline
Vector3D & 
Vector3D::operator+=(const Vector3D & vec2)
{
  x += vec2.x; y += vec2.y; z += vec2.z;
  return *this;
}

inline
Vector3D 
Vector3D::operator+(const Vector3D & vec2) const
{
  //return Vector3D(x + vec2.x, y + vec2.y, z + vec2.z);
  return Vector3D(*this) += vec2;
}

inline
Vector3D & 
Vector3D::operator-=(const Vector3D & vec2)
{
  x -= vec2.x; y -= vec2.y; z -= vec2.z;
  return *this;
}

inline
Vector3D 
Vector3D::operator-(const Vector3D & vec2) const
{
  //return Vector3D(x - vec2.x, y - vec2.y, z - vec2.z);
  return Vector3D(*this) -= vec2;
}

inline
Vector3D
Vector3D::cross(const Vector3D & vec2) const
{
  return Vector3D(y*vec2.z - z*vec2.y, 
  		  z*vec2.x - x*vec2.z, 
  		  x*vec2.y - y*vec2.x);
}

inline
Vector3D  
Vector3D::operator*(const REAL a) const 
{
  return Vector3D(a*x, a*y, a*z);
}

inline
Vector3D 
operator*(const REAL a, const Vector3D & vec2)  
{
  return Vector3D(vec2*a);
}

inline
Vector3D & 
Vector3D::operator*=(const REAL a)
{
  x *= a; y *= a; z *= a; 
  return *this;
}

inline
Vector3D 
Vector3D::operator/(const REAL a) const
{
  //ASSERT(a > 0.0 || a < 0.0);
  REAL ua =  1.0/a;
  //REAL ua;
  //if (a != 0) ua = 1.0/a;
  //else ua = 0;
  return Vector3D(x*ua, y*ua, z*ua);
}

inline
Vector3D & 
Vector3D::operator/=(const REAL a)
{
  //ASSERT(a > 0.0 || a < 0.0);
  REAL ua = 1.0/a;
  //REAL ua;
  //if (a != 0) ua = 1.0/a;
  //else ua = 0;
  x *= ua; y *= ua; z *= ua; 
  return *this;
}

inline
REAL 
Vector3D::dot(const Vector3D & vec2) const
{
  return (x*(vec2.x) + y*(vec2.y) + z*(vec2.z));
}

inline
REAL & 
Vector3D::operator[](const int index) 
{
  if(0 == index) return x;
  else if(1 == index) return y;
  else return z;
}

inline
REAL  
Vector3D::operator[](const int index) const 
{
  if(0 == index) return x;
  else if(1 == index) return y;
  else return z;
}

inline
bool 
Vector3D::operator>(const REAL value)
{
  if (x > value || x < -value) return true;
  if (y > value || y < -value) return true;
  if (z > value || z < -value) return true;
  return false;
}

inline
bool 
Vector3D::operator<(const REAL value)
{
  return !(*this > value);
}

inline
REAL 
Vector3D::norm(void) const
{
  return sqrt(x*x + y*y + z*z);
}

inline
REAL 
Vector3D::squaredNorm(void) const
{
  return (x*x + y*y + z*z);
}

inline
REAL 
Vector3D::norm2XY(void) const
{
  return (x*x + y*y);
}

inline
Vector3D 
Vector3D::normalized(void) const
{
  REAL unorm(1.0/norm());
  return Vector3D(x*unorm, y*unorm, z*unorm);
}

inline
Vector3D 
Vector3D::xyVec(void) const
{
  return Vector3D(x, y, 0);
}

inline
Vector3D 
Vector3D::signVec(void) const
{
  REAL a0, a1, a2;
  if (x != 0) a0 = (x > 0) ? 1: -1;
  else a0 = 0;
  if (y != 0) a1 = (y > 0) ? 1: -1;
  else a1 = 0;
  if (z != 0) a2 = (z > 0) ? 1: -1;
  else a2 = 0;
  return Vector3D(a0, a1, a2);
}

inline
void 
Vector3D::normalize(void)
{
  REAL unorm(1.0/norm());
  x *= unorm; y *= unorm; z *= unorm; 
}


void 
Vector3D::print(void)
{
  printf("# ( %+06.3f, %+06.3f, %+06.3f )\n", x, y, z);
  fflush(stdout);
}


#endif // if !EIGEN


// para escribir y leer los vectores
std::ostream & operator<<(std::ostream & output, const Vec3d_t & vec)
{
  output.precision(19);
  //output.setf(std::ios_base::fixed | std::ios_base::scientific, std::ios_base::floatfield);
  output.setf(std::ios::scientific);
  //return output << std::setw(28) << vec[0]<< "\t" 
  // 		<< std::setw(28) << vec[1] << "\t" 
  // 		<< std::setw(28) << vec[2] ; 
  return output << std::setw(28) << ((std::fabs(vec[0]) < 1.0e-300) ? 0.0 : vec[0]) << "\t"  // take care of subnormal numbers
   		<< std::setw(28) << ((std::fabs(vec[1]) < 1.0e-300) ? 0.0 : vec[1]) << "\t"  // take care of subnormal numbers
   		<< std::setw(28) << ((std::fabs(vec[2]) < 1.0e-300) ? 0.0 : vec[2]) ;        // take care of subnormal numbers
}

std::istream & operator>>(std::istream & is, Vec3d_t & vec) {
  return is >> vec[0] >> vec[1] >> vec[2];
}


