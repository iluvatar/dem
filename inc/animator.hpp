#ifndef __ANIMATOR__
#define __ANIMATOR__

#include "workspace_dem_config.hpp"
#include "particle.hpp"
#include "interaction.hpp"
#include "utility.hpp"
#include <map>
#include <iterator>
#ifdef USE_VTK
#include <vtkSmartPointer.h>
#include <vtkLine.h>
#include <vtkLineSource.h>
#include <vtkPolyData.h>
#include <vtkVersion.h>
#include <vtkCellArray.h>
#include <vtkDoubleArray.h>
#include <vtkPoints.h>
#include <vtkXMLPolyDataWriter.h>
#include <vtkCellData.h>
#include <vtkPointData.h>
#endif // USE_VTK

namespace DEM {
  class Visualization {
  public:
    Visualization() : pvd_started_(false) {};
    // gnuplot
    void print_system_gnuplot(const std::vector<Particle> & particle, double time); 
    int  get_color_gnuplot(int tag);
    void print_header_gnuplot(const std::vector<Particle> & particle, std::string filename);
    void print_grain_gnuplot(const Particle & grain, int colorTag, double xshift);
    // paraview
    void start_paraview_pvd(void);
    void finish_paraview_pvd(void);
    void print_paraview_vtp_header(std::ofstream & fout);
    void print_paraview_vtp_footer(std::ofstream & fout);
    void print_paraview_vtp(const std::vector<Particle> & particle, const std::vector<Contact> & contacts, 
			    const std::vector<Particle> & particle_init, const std::string & filename, 
			    const double time, const bool periodic[]);
    void print_paraview_vtp_planes(const std::vector<Particle> & particle, const std::vector<Particle> & particle_init, std::ofstream & fout);
    void print_paraview_vtp_planebox(const double lmin[3], const double lmax[3], const std::vector<Particle> & particle, 
				     const std::vector<Particle> & particle_init, std::ofstream & fout, const int & wall_id);
    void print_paraview_vtp_body_data(const std::vector<Particle> & particle, const std::vector<Particle> & particle_init, std::ofstream & fout, 
				      const int & start, const int & end, const int & reps = 1);
    // VTK or ascii
#ifdef USE_VTK
    int print_VTK_vtp_contact_lines(const std::vector<Particle> & particle, const std::vector<Contact> & contacts, const bool periodic[], const std::string & filename, double time);
    int print_VTK_vtp_points(const std::vector<Particle> & particle, const std::vector<Particle> & particle_init, std::string & filename, double time);
#else
    void print_paraview_vtp_points(const std::vector<Particle> & particle, const std::vector<Particle> & particle_init, std::ofstream & fout);
    void print_paraview_vtp_contact_lines(const std::vector<Particle> & particle, const std::vector<Contact> & contacts, 
					  std::ofstream & fout, const bool periodic[]);
#endif
    // povray
    void print_system_povray(const std::vector<Particle> & particle, const std::string & filename, 
			     const double & xi, const double & xf, const double & yi, const double & yf, const double & zi, const double & zf,  
			     const double & campos_x, const double & campos_y, const double & campos_z); 
  private:
    bool pvd_started_;
    std::ofstream fpvd_, fpvd_contacts_, fpvd_planes_;
  };

} // end namespace DEM

#endif // animator_hpp
