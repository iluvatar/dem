#ifndef __INITIALIZER__
#define __INITIALIZER__

#include "common_config.hpp"
#include "particle.hpp"
#include "random.hpp"
#include "distance.hpp"
#include "utility.hpp"
#include "workspace_dem_config.hpp"
#include <set>
#include <string>
#include <vector>
#include <iterator>
#include <fstream>
#include <cfloat>
#include "yaml-cpp/yaml.h" // for parsing yaml files
#include <tetgen.h>

namespace DEM {
  class Initialization {
  public:
    explicit Initialization(void);
    void build_and_print();
    void read_config(void);

  private: //methods
    void initial_conditions(void); // R(0), V(0)
    void set_features(void);
    void print(void);
    void set_grain_features(void);
    void initial_positions(void); /// R(0)
    void put_particles_inside_box(double & max_z);
    void densify_pack_geometric_event_driven(double & max_z, int start_id = 0);
    void densify_to_cube(int ntries);
    bool check_penetration(void);
    void update_surface(int id_last_added, std::set<int> & surface);
    void read_granulometry_file(const std::string & fname);
    int  read_positions(const std::string & fname);
    void write_positions(const std::string & fname, const int ntowrite);
    void set_walls_positions(double lx, double ly, double lz);
    void packing_fraction(void);
    
  private: // data
    Random ran2;
    std::vector<Random> randomgen_; ///< vector of random generators to be used on openmp parallel densification
    int dim_ = 0, seed_ = 0;
    double voxel_size_ = 0, lx_ = 0, ly_ = 0, lz_ = 0;
    double min_rad1_ = 0, max_rad1_ = 0, prop1_ = 0, min_rad2_ = 0, max_rad2_ = 0, prop2_ = 0, V0_ = 0, W0_ = 0;
    double rho_ = 0.0;
    double alpha_ = 0; // factor for randomization inside box
    double WALL_MASS_SCALE_ = 0;
    int nrows_ = 0, ncolumns_ = 0, nyboxes_ = 0, ngrains_ = 0, nwalls_ = 6, NTRIAL_ = 0; 
    bool densify_ = false, periodic_[3] = {false, false, false};
    int cube_densify_ = 0; ///< number of times to try a densification for an cubic aspect ratio
    double surface_factor_ = 1.9; ///< factor to multiply the mean rad, 0.5*(minrad + maxrad), to check for particles belonging to a surface 
    std::vector<Particle> particle; // grains + walls
    double total_mass_grains_ = 0;
    std::vector<double> granu_values_; // to save the granulometry if read from a file
    int npositions_read_ = 0; ///< number of positions read and to be kept fixed
  };
} // namespace DEM

#endif //__INITIALIZER__
