#ifndef __NETCDF_IO_HPP__
#define __NETCDF_IO_HPP__ 1

#if WITH_NETCDF

/**
   This functions implements reading/writing to netcdf files
*/

#include "particle.hpp"
#include "contact.hpp"
#include "interaction.hpp"
#include <iostream>
#include <algorithm>
#include <string>
#include <cstdlib>
#include <netcdf>
using namespace netCDF;
using namespace netCDF::exceptions;

// Return this in event of a problem.
namespace netCDF {
  static const int NC_ERR = 2;
}

//-------------------------------------
// declarations
//-------------------------------------
int write_particles_netcdf(const std::vector<Particle> & particles, const std::string & filename);
int read_particles_netcdf(const std::string & filename, std::vector<Particle> & particles);
int write_contacts_netcdf(const std::vector<DEM::Contact> & contacts, const std::string & filename);
int write_contacts_netcdf(const DEM::Interaction::Interaction_t & contacts, const std::string & filename);
int read_contacts_netcdf(const std::string & filename, std::vector<DEM::Contact> & contacts);
int read_contacts_netcdf(const std::string & filename, DEM::Interaction::Interaction_t & contacts);

#endif // WITH_NETCDF

#endif // __NETCDF_IO_HPP__
