#include <functions.hpp>

double
kgamma_Vmin(const double & x, const double *par)
{
  // par[0] = Vmin, par[1] = Vmean, par[2] = Vstd
  const double k = std::pow((par[1] - par[0])/par[2], 2);
  const double aux = k*(x - par[0])/(par[1] - par[0]);
  if ((x <= par[0]) || (par[1] <= par[0]) || (k <= 1) || (aux <= 0))
    return 0.0;
  else
    return std::exp(-aux + k*std::log(aux))/((x - par[0])*std::tgamma(k));
}

double 
kgamma_k_X_Vm(const double & x, const double *par)
{
  // par[0] = K, par[1] = X, par[2] = Vmin
  double var = (x - par[2])/par[1];
  if (x > par[2] && par[1] > 0) return std::exp(par[0]*std::log(var) - var)/((x - par[2])*std::tgamma(par[0]));
  else return 0;
}

double 
kgamma_k_Vmean_Vm(const double & x, const double *par)
{
  // par[0] = K, par[1] = mean, par[2] = Vmin
  double var = par[0]*(x - par[2])/(par[1] - par[2]);
  if (x > par[2] && par[1] > par[2] && par[0] > 0) return std::exp(par[0]*std::log(var) - var)/((x - par[2])*std::tgamma(par[0]));
  else return 0;
}

double 
kgamma_scaled_k(const double & x, const double *par)
{
  // par[0] = K
  double var = par[0]*x;
  if (x > 0 && par[0] > 0) return std::exp(par[0]*std::log(var) - var)/(x*std::tgamma(par[0]));
  else return 0;
}

double 
kgamma_k_Vm_Vmean(const double & x, const double *par)
{
  // par[0] = K, par[1] = Vmin, par[2] = Vmean
  double var = par[0]*(x - par[1])/(par[2] - par[1]);
  if (x > par[1] && x > 0 && par[2] > par[1] && par[0] > 0 &&
      par[0] < 50 && par[1] < 10 && par[2] < 20) return std::exp(par[0]*std::log(var) - var)/((x - par[1])*std::tgamma(par[0]));
  else return 0;
}

double 
kgamma_k_Vm(const double & x, const double *par)
{
  // par[0] = K, par[1] = Vmin, par[2] = Vmean
  static const double p2 = par[2]; // fixes par[2]
  double var = par[0]*(x - par[1])/(p2 - par[1]);
  if (x > par[1] && x > 0 && p2 > par[1] && par[0] > 0 ) return std::exp(par[0]*std::log(var) - var)/((x - par[1])*std::tgamma(par[0]));
  else return 0;
}


// k = shape, beta = scale
double 
igamma(const double & x, const double * par) 
{
  // par[0] = k
  //if (par[0] > 0 && x > 0) return std::pow(x, par[0])*std::tr1::conf_hyperg(par[0], par[0]+1, -x)/par[0];
  if (par[0] > 0 && x > 0) return std::pow(x, par[0])*std::tr1::conf_hyperg(par[0], par[0]+1, -x)/par[0];
  else return 0;
}

double 
pgamma(const double & x, const double * par)
{
  // par[0] = k, par[1] = Vmin, par[2] = Vmean
  if(par[0] > 0 && x > par[1] && par[1] >= 0 && par[2] > par[1]) 
    return igamma(par[0]*(x-par[1])/(par[2] - par[1]), par)/std::tgamma(par[0]);
  else return 0.0;
}

double 
qgamma(const double & x, const double * par)
{
  // par[0] = k, par[1] = Vmin, par[2] = Vmean
  return 1.0 - pgamma(x, par);
}

double 
exponential_ramp(const double & x, const double *par)
{
  return par[2] - (par[2] - par[1])*std::exp(-x/par[0]); // exponential saturates to par[2], starts at par[1], scale is par[0]
}

void StairFunctor::start(std::vector<double> & par)
{
  ASSERT(5 == par.size());
  ASSERT(0 <  par[0]);             //< 0 <  min
  ASSERT(0 <  par[1]);             //< 0 <  nsteps
  ASSERT(0 <= par[3]);             //< 0 <= niter
  ASSERT(1 <= par[4]);             //< 1 <= nstair
  min_    = par[0];
  nsteps_ = par[1];
  alpha_  = par[2];
  max_    = std::pow(2, nsteps_)*min_;
  niter_  = par[3];
  nstair_ = par[4];
  deltax_ = double(niter_)/(nsteps_ + 1.0);
  stair_step_ = 0;
  stair_      = 0;
  values.resize(nsteps_ + 1);
  // fill values 
  for (int ii = 0; ii <= nsteps_; ++ii) {
    values[ii] = min_*std::pow(alpha_, ii);
  }
}
    
double StairFunctor::operator()(const int & iter)
{
  ASSERT(0 <= iter);
  stair_ = std::min(nstair_-1, int(iter/niter_));
  stair_step_ = int(iter/deltax_)%(nsteps_+1); if (iter > niter_*nstair_) stair_step_ = nsteps_;
  const int idx = std::pow(-1, stair_%2)*(stair_step_ - (stair_%2)*nsteps_); 
  ASSERT(0 <= stair_ && stair_ < nstair_);
  ASSERT(0 <= stair_step_ && stair_step_ <= nsteps_);
  ASSERT(0 <= idx && idx <= nsteps_);
  return values[idx];
}

bool StairFunctor::ramps_done(const int & iter)
{
  if (stair_ < nstair_) return false;
  if (iter < nstair_*niter_) return false;
  return true;
}



