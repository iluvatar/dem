#include "histogram1d.hpp"

Histogram1D::Histogram1D() 
{
  reset();
}

Histogram1D::~Histogram1D(void) 
{
  reset();
  release_memory();
}

void
Histogram1D::reset(void)
{
  std::fill(counter_.begin(), counter_.end(), 0);
  std::fill(sum_bin_.begin(), sum_bin_.end(), 0);
  std::fill(sum2_bin_.begin(), sum2_bin_.end(), 0);
  samples_ = total_samples_ = 0;
  samples_ = total_samples_ = 0;
  dropped_xmin_ = 1.0e100;
  dropped_xmax_ = -1.0e100;
  sum_ = sum2_ = sum3_ = 0;
}

void 
Histogram1D::release_memory(void)
{
  xval_.clear(); xval_.shrink_to_fit(); 
  counter_.clear(); counter_.shrink_to_fit(); 
  sum_bin_.clear(); sum_bin_.shrink_to_fit();
  sum2_bin_.clear(); sum2_bin_.shrink_to_fit();
  pdf_.clear(); pdf_.shrink_to_fit();
  prob_.clear(); prob_.shrink_to_fit();  
}

void 
Histogram1D::generate_range(void)
{
  for(int ii = 0; ii < bins_+1; ++ii) 
    xval_[ii] = xmin_ + dx_*ii;
}

void
Histogram1D::construct(const double & xmin0, const double & xmax0, const long & bins0, const std::string & name) 
{
  name_ = name;
  release_memory();
#if DEBUG
  std::cout << "#\t Constructing histogram  " << name_ << std::endl;
#endif
  if (xmax0 <= xmin0) {
    std::cerr << "#\t Histogram construct error: xmax <= xmin -> NOT allowed. Exiting." << std::endl; 
    std::cerr << "#\t xmin = " << xmin0 << std::endl;
    std::cerr << "#\t xmax = " << xmax0 << std::endl;
    std::cerr << "#\t name = " << name << std::endl;
    std::exit(1); 
  }
  if (bins0 <= 0) { 
    std::cerr << "#\t Histogram construct error: bins0 <= 0 -> NOT allowed. Exiting." << std::endl; 
    std::cerr << "#\t name = " << name << std::endl;
    std::exit(1); 
  } 
  xmin_ = xmin0;
  xmax_ = xmax0;
  bins_ = bins0;
  dx_ = (xmax_ - xmin_)/bins_;
  if (dx_ <= 0)  {
    std::cerr << "#\t Histogram construct error: dx <= 0 -> NOT allowed. Exiting." << std::endl; 
    std::cerr << "#\t name = " << name << std::endl;
    std::exit(1); 
  }
  u_dx_ = 1.0/dx_;
  counter_.resize(bins_, 0);
  xval_.resize(bins_+1, 0);
  generate_range();
  sum_bin_.resize(bins_);
  sum2_bin_.resize(bins_);
  pdf_.resize(bins_, 0);
  prob_.resize(bins_, 0);
  samples_ = 0;
  total_samples_ = 0;
  sum_ = sum2_ = sum3_ = 0;
#if DEBUG
  std::cout << "#\t Histogram " << name_ << " ready. Limits: xmin = " << xmin_ << "\t xmax = " << xmax_ << "\t bins = " << bins_ << std::endl; 
#endif
}

int 
Histogram1D::get_bin(const double & x)
{
  if(xmin_ <= x && x < xmax_) { 
    const int bin = static_cast<int>((x - xmin_)*u_dx_);
    if (0 <= bin && bin < bins_) 
      return bin;
  }
  else {
    if (x < dropped_xmin_) dropped_xmin_ = x;
    if (x > dropped_xmax_) dropped_xmax_ = x;
  }
  return BIN_OUT_OF_RANGE;
}


// count sample
void
Histogram1D::increment(const double & x)
{
  const int bin = get_bin(x);
  if (BIN_OUT_OF_RANGE != bin) {
    ++counter_[bin]; 
    ++samples_;
    sum_ += x;
    sum2_ += x*x;
    sum3_ += x*x*x;
  }
  ++total_samples_;
}

void
Histogram1D::increment(const double * x, const int & size)
{
  print_debug("Histogram1D :: Reading an array of size ->  ", size);
  for(int ii = 0; ii < size; ii++ ) 
    increment(x[ii]);
}

void
Histogram1D::increment(const std::vector<double> & x)
{
  const int size = x.size();
  print_debug("Histogram1D :: Reading a vector of size ->  ", size);
  for(int ii = 0; ii < size; ii++ ) 
    increment(x[ii]);
}

void 
Histogram1D::accumulate(const double & x, const double & w) // add w to bin of x
{
  const int bin = get_bin(x);
  if (BIN_OUT_OF_RANGE != bin) {
    ++counter_[bin]; ++samples_;
    sum_ += x; sum2_ += x*x; sum3_ += x*x*x;
    sum_bin_[bin] += w;
    sum2_bin_[bin] += w*w;
  }  
  ++total_samples_;
}

void
Histogram1D::set(const double & x, const double & val)
{
  const int bin = get_bin(x);
  if (BIN_OUT_OF_RANGE != bin) {
    counter_[bin] = val;
  } 
}


bool
Histogram1D::empty(void) 
{
  for(const auto & val : counter_) {
    if (val > 0) return false;
  }
  return true;
}

bool 
Histogram1D::check(void) 
{
  for(int bin = 0; bin < bins_ + 1; ++bin) {
    if(std::fabs(xval_[bin] - (xmin_ + bin*dx_)) > 1.0e-10) { 
      std::cerr << "ERROR :: Histogram      " << name_ << std::endl;
      std::cerr << "ERROR ON xval HISTOGRAM " << std::endl;
      std::cerr << "xmin_ = " << xmin_ << std::endl;
      std::cerr << "xmax_ = " << xmax_ << std::endl;
      std::cerr << "dx_   = " << dx_   << std::endl;
      std::cerr << "bin   = " << bin   << std::endl;
      std::cerr << "xval[bin]       = " << xval_[bin]      << std::endl;
      std::cerr << "xmin_ + bin*dx_ = " << xmin_ + bin*dx_ << std::endl;
      return false;
    }
  }
  return true;
}


void 
Histogram1D::print_ccdf(const std::string & fname)
{
  std::vector<double> x, ccdf;
  save_ccdf(x, ccdf);
  std::cout << "# Printing ccdf to filename -> " << fname << std::endl;
  std::ofstream fout(fname); ASSERT(fout); fout.setf(std::ios::scientific); fout.precision(16);
  for(int bin = 0; bin < bins_; ++bin) {
    fout << x[bin] << "\t" << ccdf[bin] << std::endl;
  }
  fout.close();
}

void 
Histogram1D::save_ccdf(std::vector<double> & x, std::vector<double> & y)
{
  x.resize(bins_); y.resize(bins_);
  std::fill(x.begin(), x.end(), 0.0); std::fill(y.begin(), y.end(), 1.0);
  double sum = 0;
  for(int bin = 0; bin < bins_; ++bin) {
    x[bin] = xval_[bin];
    sum += counter_[bin]/(samples_);
    y[bin] = 1.0 - sum;
  }
}

void
Histogram1D::compute_pdf(void) 
{
  std::fill(pdf_.begin(), pdf_.end(), 0.0);
  if (samples_ > 0) {
    std::copy(counter_.begin(), counter_.end(), pdf_.begin());
    for(int ii = 0; ii < bins_; ii++) {
      pdf_[ii] /= (xval_[ii+1]-xval_[ii])*samples_;
    }
  }
}

double
Histogram1D::get_area(void) 
{
  double area = 0;
  if (samples_ > 0) {
    for(int ii = 0; ii < bins_; ii++) {
      area += counter_[ii]*(xval_[ii+1]-xval_[ii]);
    }
  }
  return area;
}

void
Histogram1D::compute_prob(void) 
{
  std::fill(prob_.begin(), prob_.end(), 0.0);
  if (samples_ > 0) {
    std::copy(counter_.begin(), counter_.end(), prob_.begin());
    for (auto & x : prob_) x /= samples_;
  }
}


double
Histogram1D::skewness(void)
{
  double gamma1 = 0;  // skewness

  if (samples_ >= 1) {
    // mean and sigma
    double mean, sigma; mean_and_sigma(mean, sigma);
    // compute gamma1 (skewness)
    double mean3 = sum3_/samples_;
    gamma1 = (mean3 - 3*mean*sigma*sigma - mean*mean*mean)/std::pow(sigma, 3.0);
  }
  
  return gamma1;
}

double
Histogram1D::mean(void)
{
  double mean = 0, sigma = 0;
  mean_and_sigma(mean, sigma);
  return mean;
}

double
Histogram1D::sigma(void)
{
  double mean = 0, sigma = 0;
  mean_and_sigma(mean, sigma);
  return sigma;
}

void
Histogram1D::mean_and_sigma(double & mean, double & sigma) 
{
  mean = sigma = 0;

  ///*
  // naive approach 1
  if (samples_ > 0) {
    mean = sum_/samples_;
    sigma = std::sqrt(sum2_/samples_ - mean*mean);
  }
  //*/
 
  /*
  // naive approach 2
  if (samples_ > 0) {
    compute_prob();
    double mean2 = 0;
    for (int ii = 0; ii < bins_; ii++) {
      mean  += prob_[ii]*xval_[ii]; 
      mean2 += prob_[ii]*xval_[ii]*xval_[ii];
    }
    sigma = std::sqrt(mean2 - mean*mean);
  }
  */

  // West wieghted incremental algorithm:
  //  D. H. D. West (1979). Communications of the ACM, 22, 9, 532-535: Updating Mean and Variance Estimates: An Improved Method
  // http://en.wikipedia.org/wiki/Algorithms_for_calculating_variance
  /*
  // Q      -> delta
  // SUMW   -> sumweight
  // M      -> mean
  // T      -> M2
  if (samples_ > 0) {
    double sumweight = 0, M2 = 0, M = 0;
    double temp, delta, R;
    int count = 0;
    for(int i = 0; i < bins_; i++) {
      if (counter_[i] > 0) {
	delta = xval_[i] - M;              // Q = Xi - M
	temp = counter_[i] + sumweight;    // TEMP = SUMW + Wi
	R = delta*counter_[i]/temp;        // R = Q*Wi/TEMP
	M = M + R;                         // M = M + R
	M2 = M2 + sumweight*delta*R;       // T = T + SUMW*Q*R
	sumweight = temp;                  // SUMW = TEMP
	count++;
      }
    }
    if (count > 0) {
      //mean = sum_/samples_;                // XBAR = Original formula which works even for one sample
      mean = M;                          // XBAR = M
      if (sumweight > 0 && count > 1) {
	sigma = M2/sumweight;                // S2 = (T/SUMW)
	sigma = sigma*count/(count-1.0);     // S2 = (T/SUMW)*(n/n-1)
	sigma = std::sqrt(sigma);
      }
    }
  }*/
}

void
Histogram1D::print_mean_sigma(const std::string & fileName)
{
  double mean, sigma;
  mean_and_sigma(mean, sigma);
  // c i/o
  std::FILE * fp = std::fopen(fileName.c_str(), "w");
  std::fprintf(fp, "%25.17le\t%25.17le\n", mean, sigma);
  std::fclose(fp);
}


int  
Histogram1D::get_valid_bins_size(void)
{
  int count = 0;
  for (const auto & val : counter_)
    if (val > 0) ++count;
  return count;
}

void 
Histogram1D::get_valid_data(double *x, double *y)
{
  int idx = 0;
  for(int ii = 0; ii < bins_; ii++) {
    if (counter_[ii] > 0) {
      x[idx] = xval_[ii];
      y[idx] = counter_[ii]/((xval_[ii+1]-xval_[ii])*samples_);
      ++idx;
    }
  }
}

void 
Histogram1D::get_valid_counters(double *x, int *c)
{
  int idx = 0;
  for(int ii = 0; ii < bins_; ii++) {
    if (counter_[ii] > 0) {
      x[idx] = xval_[ii];
      c[idx] = counter_[ii];
      ++idx;
    }
  }
}

double
Histogram1D::get_first_valid_x(void)
{
  if (samples_ > 0) {
    for(int ii = 0; ii < bins_; ii++) {
      if (counter_[ii] > 0) {
	if (ii >= 1)
	  return 0.5*(xval_[ii] + xval_[ii-1]);
	else
	  return xval_[ii];
      }
    }
  }
  return +1.0e100;
}


void
Histogram1D::print_as_mean_per_bin(const std::string & filename) 
{
  // c i/o
  std::FILE * fp = std::fopen(filename.c_str(), "w");
  for(int i = 0; i < bins_; i++) {
    const double x = xval_[i]; 
    const double val1 = ( (counter_[i] >= 1) ? sum_bin_[i]/counter_[i] : 0 );
    const double val2 = ( (counter_[i] >= 2) ? std::sqrt((sum2_bin_[i] - sum_bin_[i]*sum_bin_[i]/counter_[i])/(counter_[i]*(counter_[i]-1))) : 0 );
    std::fprintf(fp, "%25.17le\t%25.17le\t%25.17le\n", x, val1, val2);
  }
  std::fclose(fp);
}

// read from data file
int 
Histogram1D::read_data(const std::string & fname)
{
  std::ifstream fin(fname); if(!fin) { print_error_and_exit("# Histogram was unable to open-read file : " + fname); }
  double x;  
  while (fin >> x) { increment(x); }
  return EXIT_SUCCESS;
}


// print
void
Histogram1D::print(const std::string & filename, 
		   const double & mx, const double & bx, 
		   const double & my, const double & by, 
		   const bool & print_full_info,
		   const bool & print_only_valid,
		   const int & mode) 
{
  ASSERT(0 <= mode && mode <= 4);
  check();
  // mean and sigma
  double mean, sigma, x, y;
  mean_and_sigma(mean, sigma);
  const double dropped_samples = total_samples_ - samples_;
#if DEBUG
  std::cout << "# Printing histogram to filename -> " << filename << std::endl;
#endif
  std::FILE * fp = std::fopen(filename.c_str(), "w");
  // header
  std::fprintf(fp, "# samples         = %25.17le\n", samples_);
  std::fprintf(fp, "# total samples   = %25.17le\n", total_samples_);
  std::fprintf(fp, "# bins            = %25d\n",     bins_);
  std::fprintf(fp, "# xmin            = %25.17le\n", xmin_);
  std::fprintf(fp, "# xmax            = %25.17le\n", xmax_);
  std::fprintf(fp, "# dx              = %25.17le\n", dx_);
  std::fprintf(fp, "# dropped_samples = %25.17le  (%5.2lf %%)\n", dropped_samples, 
	       (samples_ > 0) ? (dropped_samples/samples_)*100.0 : 100);
  std::fprintf(fp, "# dropped_xmin    = %25.17le\n", dropped_xmin_);
  std::fprintf(fp, "# dropped_xmax    = %25.17le\n", dropped_xmax_);
  std::fprintf(fp, "# skew            = %25.17le\n", skewness());
  std::fprintf(fp, "# mean            = %25.17le\n", mean);
  std::fprintf(fp, "# sigma           = %25.17le\n", sigma);
  std::fprintf(fp, "# sum_            = %25.17Le\n", sum_);
  std::fprintf(fp, "# sum2_           = %25.17Le\n", sum2_);
  std::fprintf(fp, "# sum3_           = %25.17Le\n", sum3_);
  std::fprintf(fp, "# mx              = %25.17le\n", mx);
  std::fprintf(fp, "# bx              = %25.17le\n", bx);
  std::fprintf(fp, "# my              = %25.17le\n", my);
  std::fprintf(fp, "# by              = %25.17le\n", by);
  std::fprintf(fp, "#%20s\t%25s\t", "x", "pdf");
  if (true == print_full_info) {
    std::fprintf(fp, "#%25s\t%25s\t%25s\t", "raw_counter[x]", "mean_at_bin", "sigma_at_bin");
  }
  std::fprintf(fp, "\n");
  // compute usefull quantities
  compute_pdf();
  compute_prob();
  const double area = get_area(); _UNUSED(area); // _UNUSED avoids unused warnings
  const double maximum = *std::max_element(counter_.begin(), counter_.end()); 
  // actual printing
  for(int ii = 0; ii < bins_; ii++) {
    if (true == print_only_valid && 0 == counter_[ii]) continue;
    ////x = 0.5*(xval[ii+1] + xval[ii]);
    x = xval_[ii]; 
    y = 0;
    if (0 == mode) y = pdf_[ii] ; // normalize to pdf : sum area = 1 if all data range is included
    if (1 == mode) y = prob_[ii] ; // normalize to sum bin 1 -> To coincide with ASTE article
    if (2 == mode) y = counter_[ii] ; // Not normalized
    if (3 == mode) y = counter_[ii]/maximum; // Max = 1
    if (4 == mode) y = counter_[ii]/area; // area always equal to 1, in spite of data range
    x = mx*x + bx;
    y = my*y + by;
    std::fprintf(fp, "%25.17le\t%25.17le\t", x, y);
    if (true == print_full_info) {
      std::fprintf(fp, "%25.17le\t%25.17le\t%25.17le\t",
		   counter_[ii], 
		   ( (counter_[ii] >= 1) ? sum_bin_[ii]/counter_[ii] : 0 ),
		   ( (counter_[ii] >= 2) ? std::sqrt((sum2_bin_[ii] - sum_bin_[ii]*sum_bin_[ii]/counter_[ii])/(counter_[ii]*(counter_[ii]-1))) : 0 ) );
    }
    std::fprintf(fp, "\n");
  }
  std::fclose(fp);
}

#if DEBUG
#undef _GLIBCXX_DEBUG 
#undef _GLIBCXX_DEBUG_PEDANTIC
#endif

