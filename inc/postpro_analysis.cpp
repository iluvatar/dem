#include "postpro_analysis.hpp"

namespace DEM {
  //--------------------------------------------------------------------------
  // Read the configuration file, exits if error
  //--------------------------------------------------------------------------
  PostproAnalysis::PostproAnalysis() : Postpro()
  {
    niter_ = 0;
     
    UTIL::print_start("Reading more postpro config for histogrmas info . ");
    YAML::Node config = YAML::LoadFile("INPUT/postpro_conf.yaml");
    // read histograms info
    forcetorque_distro.init_tasks(config);
    P_contact_xy_.construct((M_PI/180.0)*config["P_contact_xy"][0].as<double>(), (M_PI/180.0)*config["P_contact_xy"][1].as<double>(), 
			   config["P_contact_xy"][2].as<int>(), "P_contact_xy"); 
    P_contact_xz_.construct((M_PI/180.0)*config["P_contact_xz"][0].as<double>(), (M_PI/180.0)*config["P_contact_xz"][1].as<double>(), 
			   config["P_contact_xz"][2].as<int>(), "P_contact_xz"); 
    P_contact_yz_.construct((M_PI/180.0)*config["P_contact_yz"][0].as<double>(), (M_PI/180.0)*config["P_contact_yz"][1].as<double>(), 
			   config["P_contact_yz"][2].as<int>(), "P_contact_yz"); 
    P_contact_xy_full_.construct((M_PI/180.0)*config["P_contact_xy"][0].as<double>(), (M_PI/180.0)*config["P_contact_xy"][1].as<double>(), 
				 config["P_contact_xy"][2].as<int>(), "P_contact_xy_full"); 
    P_contact_xz_full_.construct((M_PI/180.0)*config["P_contact_xz"][0].as<double>(), (M_PI/180.0)*config["P_contact_xz"][1].as<double>(), 
				 config["P_contact_xz"][2].as<int>(), "P_contact_xz_full"); 
    P_contact_yz_full_.construct((M_PI/180.0)*config["P_contact_yz"][0].as<double>(), (M_PI/180.0)*config["P_contact_yz"][1].as<double>(), 
				 config["P_contact_yz"][2].as<int>(), "P_contact_yz_full"); 

    UTIL::print_done("Reading more postpro config for histograms info.");
    
    // start
    startup_tasks();
  }


  //--------------------------------------------------------------------------
  // open file streams
  //--------------------------------------------------------------------------
  void PostproAnalysis::open_file_streams(void)
  {
    const std::string msg = "Openning file streams"; UTIL::print_start(msg);
    int status;
    status = std::system("mkdir POSTPRO/velocity_profiles"); if (0 != status) print_warning("Possible error creating directory POSTPRO/velocity_profiles");
    status = std::system("mkdir POSTPRO/translation_profiles"); if (0 != status) print_warning("Possible error creating directory POSTPRO/translation_profiles");
    status = std::system("mkdir POSTPRO/histograms_contacts"); if (0 != status) print_warning("Possible error creating directory POSTPRO/histograms_contacts");
    zfile.open("POSTPRO/z.dat");
    floatingfile.open("POSTPRO/floating.dat");
    energyfile.open("POSTPRO/energy.dat"); 
    //displacementfile.open("POSTPRO/square_displacement.dat"); 
    //brownianfile.open("POSTPRO/mobility_brownian_displacements.dat"); 
    meandeltafile.open("POSTPRO/meanPenetration.dat"); 
    pqfile.open("POSTPRO/pq.dat"); pqfile.print("# time", "gamma", "p", "q"); 
    gammaqoverpfile.open("POSTPRO/gamma_qoverp.dat"); 
    packfracfile.open("POSTPRO/packing_fraction.dat"); packfracfile.print("# gamma", "packing_fraction_filtered", "time", "full_packing_fraction");
    eqfile.open("POSTPRO/time-eq.dat"); eqfile.print("# time", "eq"); 
    eq_packfracfile.open("POSTPRO/eq_packingfraction.dat"); eq_packfracfile.print("# eq", "packingfraction"); 
    voidratiofile.open("POSTPRO/voidratio.dat"); 
    wallsinfofile.open("POSTPRO/walls_pos_vel.dat"); wallsinfofile.print("# time BOTTOM TOP LEFT RIGHT BACK FRONT - Rx Ry Rz - B T L R B F Vx Vy Vz");
    wallsposfile.open("POSTPRO/walls_pos.dat"); wallsposfile.print("# time BOTTOM TOP LEFT RIGHT BACK FRONT - Rx Ry Rz");
    wallsvelfile.open("POSTPRO/walls_vel.dat"); wallsvelfile.print("# time BOTTOM TOP LEFT RIGHT BACK FRONT - Vx Vy Vz");
    wallsforcefile.open("POSTPRO/walls_force.dat"); 
    rotinfofile.open("POSTPRO/rot_info.dat"); 
    deformfile.open("POSTPRO/deformations.dat"); 
    epeqpqfile.open("POSTPRO/ep_eq_p_q.dat"); 
    eqepfile.open("POSTPRO/eq_ep.dat"); eqepfile.print("# eq", "ep"); 
    eqqoverpfile.open("POSTPRO/eq_qoverp.dat"); eqqoverpfile.print("# eq", "q/p", "q", "p");
    internalfrictionfile.open("POSTPRO/internalfriction.dat"); internalfrictionfile.print("# time", "phi", "sin(phi)"); 
    nccorrfile.open("POSTPRO/nc_correlation.dat"); 
    cundallfile.open("POSTPRO/cundall_param.dat"); 
    ekpartfile.open("POSTPRO/ek_particles.dat"); 
    ekpartnormfile.open("POSTPRO/ek_normalized_particles.dat"); 
    vmincubicfile.open("POSTPRO/vmin_cubic.dat"); 
    // voronoi
    voronoiXfile.open("POSTPRO/voronoi_X.dat");
    voronoiKfile.open("POSTPRO/voronoi_K.dat");
    voronoiSfile.open("POSTPRO/voronoi_S.dat");
    voronoiSuKfile.open("POSTPRO/voronoi_SuK.dat");
    voronoiCfile.open("POSTPRO/voronoi_C.dat");
    voronoiCuNfile.open("POSTPRO/voronoi_CuN.dat");
    voronoiphifile.open("POSTPRO/voronoi_phi.dat");
    voronoiSTfile.open("POSTPRO/voronoi_ST.dat");
    voronoiVTfile.open("POSTPRO/voronoi_VT.dat");
    voronoiVminfile.open("POSTPRO/voronoi_Vmin.dat");
    voronoiVmeanfile.open("POSTPRO/voronoi_Vmean.dat");
    voronoiVsigmafile.open("POSTPRO/voronoi_Vsigma.dat");
    voronoiVsigma2file.open("POSTPRO/voronoi_Vsigma2.dat");
    voronoiPfile.open("POSTPRO/voronoi_P.dat");
    // task done message
    UTIL::print_done(msg);
  }

  //--------------------------------------------------------------------------
  // Close file streams
  //--------------------------------------------------------------------------
  void PostproAnalysis::close_file_streams(void)
  {
    zfile.close();
    floatingfile.close();
    energyfile.close();
    //displacementfile.close();
    meandeltafile.close();
    //brownianfile.close();
    pqfile.close();
    gammaqoverpfile.close();
    packfracfile.close();
    voidratiofile.close();
    wallsinfofile.close();
    wallsposfile.close();
    wallsvelfile.close();
    wallsforcefile.close();
    rotinfofile.close();
    deformfile.close();
    eqepfile.close();
    eq_packfracfile.close();
    epeqpqfile.close();
    eqqoverpfile.close();
    internalfrictionfile.close();
    nccorrfile.close();
    cundallfile.close();
    ekpartfile.close();
    ekpartnormfile.close();
    vmincubicfile.close();
    // voronoi
    voronoiXfile.close();
    voronoiKfile.close();
    voronoiSfile.close();
    voronoiSuKfile.close();
    voronoiCfile.close();
    voronoiCuNfile.close();
    voronoiphifile.close();
    voronoiSTfile.close();
    voronoiVTfile.close();
    voronoiVminfile.close();
    voronoiVmeanfile.close();
    voronoiVsigmafile.close();
    voronoiVsigma2file.close();
    voronoiPfile.close();

  }
  

  bool PostproAnalysis::print_snapshot(const int & iter) 
  {
    return std::binary_search(selected_iter_.begin(), selected_iter_.end(), iter); 
  }

  void PostproAnalysis::startup_tasks(void)
  {
    // init tasks
    const std::string msg = "Init tasks for POSTPRO"; UTIL::print_start(msg);
   
    voronoi_volumes_.resize(ngrains_, 0.0);
    MaterialVars_.read_config();
    ran2_.seed(0);
    neigh_.resize(ngrains_);

    file_names(CONTACTS_INFO"-", contactFilenames);
    file_names(BODIES_INFO"-", bodiesFilenames);
    niter_ = contactFilenames.size();
    print_log("Total files to read : ", niter_);
    if (0 == niter_) {
      print_error_and_exit("No files inside time window to read. Exiting");
    }
    
    // select iterations for time snapshots
    int NITER_FRAMES_ = 20;
    if (niter_ < NITER_FRAMES_) NITER_FRAMES_ = niter_;
    selected_iter_.resize(NITER_FRAMES_);
    print_log("Time snapshots NITER_FRAMES_ = ", NITER_FRAMES_); 
    int delta = lrint(double(niter_)/NITER_FRAMES_); if (delta < 1) delta = 1;
    for (int count = 0; count < NITER_FRAMES_; ++count) {
      selected_iter_[count] = count*delta;
    }
    selected_iter_[NITER_FRAMES_ - 1] = niter_ - 1;
    std::sort(selected_iter_.begin(), selected_iter_.end());
 
    // other startup tasks
    open_file_streams();
    UTIL::print_start("Reading and storing first bodies and contact info inside time window ...");
    read_particles(bodiesFilenames[0]); particle_init_ = particle;
    read_contacts(contactFilenames[0]); contacts_init_ = contacts;
    UTIL::print_done("Reading and storing first bodies and contact info inside time window.");
    print_log("Contacts init size = ", contacts_init_.size());
    UTIL::print_start("Reading and storing last bodies and contact info inside time windows ... ");
    read_particles(bodiesFilenames.back()); particle_end_ = particle;
    read_contacts(contactFilenames.back()); contacts_end_ = contacts;
    UTIL::print_done("Reading last bodies and contact info inside time windows.");
    MaterialVars_.start(particle_init_, "POSTPRO/MATERIALS_INPUT_POSTPRO");
    common_tasks(0);
    start_histograms(); // needs common_tasks to have finnished (employs voro volume) 
    init_correlation_nc();

    qcrystal.init_tasks();
    delaunay.init_tasks(min_rad_, max_rad_);
    stress_per_grain_histograms.init_tasks(ngrains_);
    homogeinity.init_tasks(particle_end_, ngrains_, 2*min_rad_, dim_);
    statmech_internal.init_tasks(particle_end_, ngrains_, 2*min_rad_, 2*max_rad_, dim_);
    statmech_material.init_tasks(particle_init_, ngrains_, 2*min_rad_, dim_);
    statmech_alongxaxis.init_tasks(particle_end_, ngrains_, 2*min_rad_, 2*max_rad_, dim_);
    voro_multiscale.init_tasks(ngrains_, 2*min_rad_, dim_, voronoi_volumes_);
    voro_nparticles.init_tasks(ngrains_, 2*min_rad_, dim_);
    centroidalvoro_.init_tasks(ngrains_, 2*min_rad_, dim_);
    voro_z.init_tasks(ngrains_, 2*min_rad_, dim_);
    //vorofiltered.init_tasks(ngrains_, 2*min_rad_, dim_);
    randomradicalvoro_.init_tasks(ngrains_, 2*min_rad_, dim_);
    contactsvoronoi.init_tasks(min_rad_, dim_);
    contactsdelaunay.init_tasks(min_rad_);
    residualcontacts_.init_tasks();

    UTIL::print_done(msg);
  }


  //--------------------------------------------------------------------------
  // Main process function, call the remaining processes
  //--------------------------------------------------------------------------
  void PostproAnalysis::process(void) 
  {
    // time tasks
    for (int iter = 0; iter < niter_; iter += stride_) {
      print_log(""); print_log(""); print_log(""); print_log(""); 
      print_log("Current iter index : ", iter);
      print_log("Current iter : ", iter+1);
      print_log("Total iter  : ", niter_);
      print_log("stride  : ", stride_);
      if (iter > 0) { common_tasks(iter); }; // iter = 0 common tasks already processed
      print_log("Current time : ", time);
      // call each postpro function
      coordination_number();
      energy();
      forcetorque_distro(time, inside_contacts_refs_, print_snapshot(iter), PROJECTION_EPS_);
      mean_max_penetration();
      //displacement(500); // check this "magic" id
      //mobility_brownian_displacement(500, 10.0); // check this magic constants
      stress_fabric_tensors();
      print_void_ratio();
      walls_info();
      mean_x_translation_profile(iter);
      print_deformations();
      correlation_nc();
      //rotational_info();
      cundall_param();
      print_vmin_cubic();      
      qcrystal(time, contacts);        // contacts are already filtered
      ASSERT(true == voronoi_ready_); // make sure voronoi volumes are computed
      //delaunay(particle, grain_ids_, DEM::NWALLS, NC_MIN_, inner_voronoi_volume(), VMIN_THRESHOLD_); 
      delaunay(particle, filtered_by_volume_ids_, DEM::NWALLS, NC_MIN_, inner_voronoi_volume(), VMIN_THRESHOLD_); 
      //delaunay(particle, filtered_by_nc_ids_, DEM::NWALLS, NC_MIN_, inner_voronoi_volume(), VMIN_THRESHOLD_); 
      //delaunay(particle, filtered_grain_ids_, DEM::NWALLS, NC_MIN_, inner_voronoi_volume(), VMIN_THRESHOLD_); 
      process_histograms(iter); // process histograms
      voronoi_print_partition_parameters(); // requires values already stored on voronoi histogram
      stress_per_grain_histograms(contacts, particle, dim_, NC_MIN_, filtered_by_volume_ids_, voronoi_volumes_);   
      ac_2D_(inside_contacts_refs_, PROJECTION_EPS_, time);
      an_at_2D_(inside_contacts_refs_, PROJECTION_EPS_, time);
      ani_3D_triaxial_(inside_contacts_refs_, particle, PROJECTION_EPS_, time);
      homogeinity(contacts, particle, periodic_, dim_, NC_MIN_);		   
      //statmech_internal(particle, dim_, NC_MIN_, voronoi_volumes_);		   
      //statmech_material(particle, dim_, NC_MIN_, filtered_by_volume_ids_, voronoi_volumes_);		
      //statmech_alongxaxis(particle, dim_, NC_MIN_, filtered_by_volume_ids_, voronoi_volumes_);		
      //voro_multiscale(dim_, NC_MIN_, filtered_by_volume_ids_, voronoi_volumes_, neigh_);	
      //voro_nparticles(dim_, NC_MIN_, filtered_by_volume_ids_, voronoi_volumes_);	
      //centroidalvoro_(particle, wmin_, wmax_, periodic_, dim_, NC_MIN_, filtered_by_volume_ids_);		
      voro_z(dim_, filtered_by_volume_ids_, particle, voronoi_volumes_);	
      //vorofiltered(filtered_grain_ids_, particle, wmin_, wmax_, periodic_, dim_);	
      //vorofiltered(filtered_by_nc_ids_, particle, wmin_, wmax_, periodic_, dim_);	
      //randomradicalvoro_(particle, wmin_, wmax_, periodic_, dim_, NC_MIN_, filtered_by_volume_ids_);		
      std::vector<Vec3d_t> cpoints; cpoints.reserve(contacts.size());
      for (const auto & cptr : inside_contacts_refs_) cpoints.push_back(location(cptr));
      //contactsdelaunay(cpoints); 
      //contactsvoronoi(cpoints, wmin_, wmax_, periodic_, dim_); 
      residualcontacts_(time, contacts);   // contacts are already filtered

      // print histograms every time step, just in case
      print_histograms();
      
      // end settings 
      voronoi_ready_ = false;
    }
    // call postpro functions processing only once 
    UTIL::print_done("step-by-step postpro functions.");
    print_log("FINISHED ITERATIONS");
    UTIL::print_start("Postpro function called only once.");
    grain_size_profile();
    time_correlation_qoverp();
    UTIL::print_done("Postpro function called only once.");

    // final prints
    print_histograms();
    close_file_streams();

    // end tasks
    forcetorque_distro.end_tasks();
    qcrystal.end_tasks();
    delaunay.end_tasks();
    stress_per_grain_histograms.end_tasks();
    homogeinity.end_tasks();
    statmech_internal.end_tasks();
    statmech_material.end_tasks();
    statmech_alongxaxis.end_tasks();
    voro_multiscale.end_tasks();
    voro_nparticles.end_tasks();
    centroidalvoro_.end_tasks();
    voro_z.end_tasks();
    //vorofiltered.end_tasks();
    randomradicalvoro_.end_tasks();
    contactsdelaunay.end_tasks();
    contactsvoronoi.end_tasks();
    residualcontacts_.end_tasks();
  }
  
  /*
  //--------------------------------------------------------------------------
  // Just a model function to create more
  //--------------------------------------------------------------------------
  template <class T>
  void PostproAnalysis::model(T & dom)
  {
    std::string name = "Model"; UTIL::print_start(name);
    UTIL::print_done(name);
  }
  */

  //--------------------------------------------------------------------------
  // Common task for all functions called in process
  //--------------------------------------------------------------------------
  void PostproAnalysis::common_tasks(int i)
  {
    voronoi_ready_ = false;

    print_log("");
    std::string name = "Common tasks POSTPRO"; UTIL::print_start(name);
    std::cout << "# Processing files : \n# " << contactFilenames[i] << "\n# " << bodiesFilenames[i] << std::endl;
    // read the bodies and contacts info
    UTIL::print_start("Reading current time step bodies and contact info ...");
    read_particles(bodiesFilenames[i]);
    read_contacts(contactFilenames[i]);
    UTIL::print_done("Reading current time step bodies and contact info .");
    // time
    time = get_time(contactFilenames[i], CONTACTS_INFO"-");
    //time_vec[i] = time;
    time_vec.push_back(time); // NEW
    print_log("TIME = ", time);
    // start processing
    
    UTIL::OFileStream fout; 
    // filter particles by distance from walls
    filter_inside_particles_id(DL_); 
    std::cout << "# Number of Inner Particles                     = " << filtered_by_volume_ids_.size() << std::endl; 
    fout.open("POSTPRO/ngrains_filtered_by_volume.dat", true); 
    fout.print(time, filtered_by_volume_ids_.size());
    fout.close();
    // filter by nc
    filter_particles_nc_ge(NC_MIN_); 
    std::cout << "# Number of Particles with nc >= "<< NC_MIN_ << " = " << filtered_by_nc_ids_.size() << std::endl; 
    fout.open("POSTPRO/ngrains_filtered_by_nc.dat", true); 
    fout.print(time, filtered_by_nc_ids_.size());
    fout.close();
    // intersect filtered ids: volume, nc
    filtered_grain_ids_.clear();
    std::set_intersection(filtered_by_volume_ids_.begin(), filtered_by_volume_ids_.end(), 
    			  filtered_by_nc_ids_.begin(), filtered_by_nc_ids_.end(), 
    			  std::inserter(filtered_grain_ids_, filtered_grain_ids_.end()));
    std::cout << "# Number of Inner particles with nc >= "<< NC_MIN_ << " = " << filtered_grain_ids_.size() << std::endl; 
    fout.open("POSTPRO/ngrains_filtered_by_volume_and_nc.dat"); 
    fout.print(time, filtered_grain_ids_.size());
    fout.close();
    // filter contacts : Selects contacts inside selected volume, stores on inside_contacts_refs_ array
    filter_inside_contacts(DL_); 
    // compute volume enclosed by contacts
    const double vol_inner_contacts = volume_from_inside_contacts();
    // compute voronoi volumes 
    voronoi_ready_ = false;
    voronoi_compute_volumes(voronoi_volumes_); 
    ASSERT(true == voronoi_ready_);
    // macro stuff: stress and fabric tensors
    Macro_.process(inside_contacts_refs_, particle, vol_inner_contacts);
    ASSERT(true == voronoi_ready_);
    double inner_volume = 0;
    for (auto & idx : filtered_by_volume_ids_) inner_volume += voronoi_volumes_[idx];
    print_log("Volume of inner particles (voronoi) = ", inner_volume);
    print_log("Volume ( from internal_volume(by_limits) ) = ", internal_volume(BY_LIMITS)/d3_);
    print_log("Volume ( from internal_volume(by_center) ) = ", internal_volume(BY_CENTERS)/d3_);
    print_log("Volume ( from dl ) = ", volume_from_dl(DL_)/d3_);
    print_log("Volume of inner contacts = ", vol_inner_contacts/d3_);
    print_log("filtered_by_volume_ids_.size() = ", filtered_by_volume_ids_.size());
    print_log("filtered_grain_ids_.size() = ", filtered_grain_ids_.size());
    print_log("contacts.size() = ", contacts.size());
    walls_limits(wmin_, wmax_);

    UTIL::print_done(name);
  }
  
  //--------------------------------------------------------------------------
  // CONTACTS UTILITY FUNCTIONS
  //--------------------------------------------------------------------------
  double 
  PostproAnalysis::Epot(const Contact & contact)
  { // linear law assumed
    return 0.5*MaterialVars_(particle[contact.uid1_].materialTag, particle[contact.uid2_].materialTag, K)*(contact.delta_)*(contact.delta_);
  }
   Vec3d_t 
  PostproAnalysis::branch_vector(const Contact & contact)
  {
    if (Particle::SPHERE == particle[contact.uid2_].type) 
      return contact.Normal_*(particle[contact.uid1_].rad + particle[contact.uid2_].rad - contact.delta_);
    else // wall
      return contact.Normal_*(particle[contact.uid1_].rad - contact.delta_);
  }


  //--------------------------------------------------------------------------
  // Volume from inside voronoi particles
  //--------------------------------------------------------------------------
  double 
  PostproAnalysis::inner_voronoi_volume(const bool & filter)
  {
    ASSERT(true == voronoi_ready_);
    double VT = 0;
    if (true == filter){
      for (auto & idx : filtered_by_volume_ids_) VT += voronoi_volumes_[idx];
    }
    else {
      for (const auto & vol : voronoi_volumes_) VT += vol;
    }
    return VT;
  }

  //--------------------------------------------------------------------------
  // Volume from inside contacts
  //--------------------------------------------------------------------------
  double 
  PostproAnalysis::volume_from_inside_contacts(void)
  {
    double vol = 0;
    if (inside_contacts_refs_.size() > 0) {
      Vec3d_t P;
      P = location(*inside_contacts_refs_.begin());
      double lmin[3] = {P[0], P[1], P[2]};
      double lmax[3] = {P[0], P[1], P[2]};
      for (const auto & c : inside_contacts_refs_) {
	P = location(c);
	for (int dir = 0; dir < 3; ++dir) {
	  if (P[dir] < lmin[dir]) lmin[dir] = P[dir];
	  if (P[dir] > lmax[dir]) lmax[dir] = P[dir];
	} 
      }
      vol = (lmax[0] - lmin[0])*(lmax[2] - lmin[2]);
      if (dim_ == 3) vol *= (lmax[1] - lmin[1]);
    }
    return vol;
  }

  //--------------------------------------------------------------------------
  // Domain volume
  //--------------------------------------------------------------------------
  double 
  PostproAnalysis::internal_volume(const BC_FILTER_t & criteria) 
  { 
    if (filtered_by_volume_ids_.size() == 0) return 0;
    double tmp; 
    std::array<double, 3> minl, maxl;
    minl.fill(1.0e100); maxl.fill(-1.0e100);
    for (auto & id : filtered_by_volume_ids_) {
      for (int dir = 0; dir < 3; ++dir) {
	if (true == periodic_[dir]) continue;
	tmp = 0;
	if (BY_CENTERS == criteria && SCREEN_FACTOR_ > 1.0) tmp = particle[id].R[dir];
	else if (BY_LIMITS == criteria || SCREEN_FACTOR_ <= 1.0) tmp = particle[id].min_limit(dir); 
	if (tmp < minl[dir]) minl[dir] = tmp; 
	tmp = 0;
	if (BY_CENTERS == criteria && SCREEN_FACTOR_ > 1.0) tmp = particle[id].R[dir];
	else if (BY_LIMITS == criteria || SCREEN_FACTOR_ <= 1.0) tmp = particle[id].max_limit(dir); 
	if (tmp > maxl[dir]) maxl[dir] = tmp;  
      }
    }
    // fix periodicity
    if (true == periodic_[0]) {
      minl[0] = particle[ngrains_ + LEFT  ].R[0]; 
      maxl[0] = particle[ngrains_ + RIGHT ].R[0]; 
    }
    if (true == periodic_[1]) {
      minl[1] = particle[ngrains_ + BACK  ].R[1]; 
      maxl[1] = particle[ngrains_ + FRONT ].R[1]; 
    }
    if (true == periodic_[2]) {
      minl[2] = particle[ngrains_ + BOTTOM].R[2]; 
      maxl[2] = particle[ngrains_ + TOP   ].R[2]; 
    }
    // the folowwing is needed not just to check, but to avoid a bug with gcc when optimizing :(
    for (int dir = 0; dir < 3; ++dir) {
      ASSERT(minl[dir] != 1.0e100); ASSERT(maxl[dir] != -1.0e100);
    }

    return (maxl[0] - minl[0])*(maxl[1] - minl[1])*(maxl[2] - minl[2]); 
  }
  
  double 
  PostproAnalysis::volume_from_dl(const double & dl) 
  { 
      // Use the screen length directly
    double dltmp, vol = 0;
    dltmp = dl; if (true == periodic_[0]) dltmp = 0;
    vol  = (particle[ngrains_ + RIGHT].R[0] - particle[ngrains_ + LEFT].R[0] - 2*dltmp); // x length
    dltmp = dl; if (true == periodic_[2]) dltmp = 0;
    vol *= (particle[ngrains_ + TOP].R[2] - particle[ngrains_ + BOTTOM].R[2] - 2*dltmp); // z length
    dltmp = dl; if (true == periodic_[1]) dltmp = 0;
    if (3 == dim_) vol *= (particle[ngrains_ + FRONT].R[1] - particle[ngrains_ + BACK].R[1] - 2*dltmp); // y length
    if (vol < 0) vol = 0;
    return vol;
  }

  //--------------------------------------------------------------------------
  // Computes walls limits
  //--------------------------------------------------------------------------
  void 
  PostproAnalysis::walls_limits(double wmin[3], double wmax[3]) {
    wmin[0] = particle[ngrains_ + LEFT  ].R[0];
    wmin[1] = particle[ngrains_ + BACK  ].R[1]; 
    wmin[2] = particle[ngrains_ + BOTTOM].R[2];
    wmax[0] = particle[ngrains_ + RIGHT ].R[0];
    wmax[1] = particle[ngrains_ + FRONT ].R[1];
    wmax[2] = particle[ngrains_ + TOP   ].R[2];
  }


  //--------------------------------------------------------------------------
  // Creates the voronoi container, computes and stores it
  //--------------------------------------------------------------------------
  void 
  PostproAnalysis::voronoi_compute_volumes(std::vector<double> & volume_array) {
    voronoi_ready_ = false;
    std::string message = "Computing Voronoi Volumes ";
    UTIL::print_start(message);
    // process all particles
    double wmin[3], wmax[3];
    walls_limits(wmin, wmax);
    UTIL::compute_voronoi_volumes(particle, wmin, wmax, periodic_, dim_, volume_array, neigh_);
    for(auto & vol : volume_array) vol /= d3_; // ALWAYS Normalize
    voronoi_ready_ = true;
    UTIL::print_start("\t Printing voronoi volume raw and filtered data: Already normalized");
    std::ofstream fout;
    fout.open("POSTPRO/voronoi_volumes.dat");
    for (const auto & vol : volume_array)
      fout << vol << std::endl;
    fout.close();
    fout.open("POSTPRO/voronoi_volumes_filtered_by_volume.dat");
    for (const auto & id : filtered_by_volume_ids_) {
      fout << voronoi_volumes_[id] << std::endl;
    }
    fout.close();
    UTIL::print_done("\t Printing voronoi volume raw and filtered data:");
    
    UTIL::print_done(message);
  }
    

  //--------------------------------------------------------------------------
  // Grains volume
  //--------------------------------------------------------------------------
  double 
  PostproAnalysis::grain_volume(const Particle & body)
  { 
    double rad = body.rad;
    double Vg = 0;
    Vg = std::pow(rad, dim_);
    Vg *= M_PI;
    if (3 == dim_) Vg *= 4.0/3.0;
    return Vg;
  }

  double 
  PostproAnalysis::volume_grains(const bool & filter)
  { 
    double Vs = 0;
    if (true == filter) {
      for(auto & id : filtered_by_volume_ids_) {
	Vs += grain_volume(particle[id]);
      }
    }
    else {
      for(const auto & grain : particle) {
	Vs += grain_volume(grain);
      }
    }
    return Vs;
  }

  //--------------------------------------------------------------------------
  // Void ratio and small energy functions
  //--------------------------------------------------------------------------
  double PostproAnalysis::get_void_ratio(void) 
  {
    return (1.0/get_packing_fraction()) - 1.0; // fixed thanks to Andres Barrero.
  }

  double PostproAnalysis::get_packing_fraction(const bool & filter) 
  {
    ASSERT(true == voronoi_ready_);
    const double vg  = volume_grains(filter);
    std::cout << "Volume grains:" << vg << std::endl;
    //const double vol = volume_from_dl(DL_);
    const double vol = inner_voronoi_volume(filter)*d3_;
    std::cout << "Total vol:" << vol << std::endl;
    return ((vol > 0) ? vg/vol : 0);
  }
  
  double PostproAnalysis::mean_velocity_trans(void)
  {
    double aver = 0;
    if (filtered_grain_ids_.size() >= 1.0) {
      for (auto & id : filtered_grain_ids_) { 
	aver += particle[id].V.norm();
      }
      aver /= filtered_grain_ids_.size();
    }
    return aver;
  }

  double PostproAnalysis::mean_velocity_rot(void)
  {
    double aver = 0;
    if (filtered_grain_ids_.size() >= 1.0) {
      for (const auto & id : filtered_grain_ids_) {
	//aver += (particle[id].rad)*(particle[id].W.norm());
	aver += (particle[id].W.norm());
      }
      aver /= filtered_grain_ids_.size();
    }
    return aver;
  }
  
  double PostproAnalysis::rotational_energy(void)
  {
    double Ek = 0;
    for (const auto & id : filtered_grain_ids_) {
      Ek += particle[id].inerI*(particle[id].W.squaredNorm());
    }
    return 0.5*Ek;
  }

  double PostproAnalysis::translational_energy(void)
  {
    double Ek = 0;
    for (const auto & id : filtered_grain_ids_) {
      Ek += particle[id].mass*(particle[id].V.squaredNorm());
    }
    return 0.5*Ek;
  }
    
  double PostproAnalysis::kinetic_energy(void) // NOTE: Respect to center of mass ?  No, the system should be stopped at the end
  {
    return translational_energy() + rotational_energy();
  }
    
  double PostproAnalysis::gravitational_energy(void) 
  {
    double Ep = 0;
    for (const auto & id : filtered_grain_ids_) {
      Ep += particle[id].mass*G_.norm()*(particle[id].z()); // NOTE: definition is not general, depends on direction of gravity
    }
    return Ep;
  }


  //--------------------------------------------------------------------------
  // Print scaled distribution
  //--------------------------------------------------------------------------
  void PostproAnalysis::voronoi_print_scaled_histogram(Histogram1D & histo, const std::string & filename) 
  {
    // compute voronoi parameters
    double mean, sigma; 
    histo.mean_and_sigma(mean, sigma);
    double vmin = UTIL::voronoi_vmin(dim_);
    double mx = 1.0, bx = 0.0, my = 1.0, by = 0.0;
    if (mean > vmin) {
      mx = 1.0/(mean-vmin), bx = -vmin/(mean-vmin), my = mean-vmin, by = 0.0;
    }
    // print
    histo.print(filename.c_str(), mx, bx, my, by);
  }

  void PostproAnalysis::voronoi_print_partition_parameters(void)
  {
    ASSERT(true == voronoi_ready_);
    std::string name = "Voronoi partition parameters"; UTIL::print_start(name);
    // computations
    double Vmin, meanVoro, sigmaVoro, K, X; 
    Vmin = UTIL::voronoi_vmin(dim_);
    histoVoroVol_.mean_and_sigma(meanVoro, sigmaVoro);
    X = UTIL::statmech_X(meanVoro, sigmaVoro, Vmin);
    K = UTIL::statmech_K(meanVoro, sigmaVoro, Vmin);
    print_log("Original Voronoi StatMech vars:");
    print_log("Vmin   = ", Vmin);
    print_log("Vmean  = ", meanVoro);
    print_log("Vsigma = ", sigmaVoro);
    print_log("Xvoro  = ", X);
    print_log("Kvoro  = ", K);
    // improve computation of Vmin 
    Vmin = std::min(Vmin, histoVoroVol_.get_first_valid_x());
    print_log("NEW Vmin after comparing with first non-null bin = ", Vmin);
    // improve computation by fit
    /*
    // fit the ccdf = 1 - cdf to estimate parameters 
    // This is better than fitting the pdf, since it "cleans" the data
    double param_err[3] = {0};
    double param[3] = {K, 0.95*Vmin, meanVoro}; 
    // OPTION 1 : fits K and Vmin directly from histo
    const int size = histoVoroVol_.get_valid_bins_size(); 
    double *xdata, *ydata; xdata = new double [size]; ydata = new double [size]; 
    histoVoroVol_.get_valid_data(xdata, ydata);
    int status = NLFIT::nlfit(2, size, xdata, ydata, kgamma_k_Vm_Vmean, param, param_err, 3); 
    Vmin = param[1];
    X = UTIL::statmech_X(meanVoro, sigmaVoro, Vmin);
    K = UTIL::statmech_K(meanVoro, sigmaVoro, Vmin);
    print_log("NEW Voronoi StatMech vars after fixing Vmin:");
    print_log("Vmin   = ", Vmin);
    print_log("Vmean  = ", meanVoro);
    print_log("Vsigma = ", sigmaVoro);
    print_log("Xvoro  = ", X);
    print_log("Kvoro  = ", K);
    //// OPTION 2 : fits K, Vmin and mean from ccdf
    //std::vector<double> xval, ccdf;
    //histoVoroVol_.save_ccdf(xval, ccdf);
    //int status = NLFIT::nlfit(3, xval.size(), &xval[0], &ccdf[0], qgamma, param, param_err, 3); 
    if (EXIT_SUCCESS == status) { // update parameters
      //print_log("New (after fit) Voronoi StatMech vars:");
      //K = param[0]; 
      Vmin = param[1]; 
      //meanVoro = param[2]; 
      X = UTIL::statmech_X(meanVoro, sigmaVoro, Vmin);
      K = UTIL::statmech_K(meanVoro, sigmaVoro, Vmin);
#if DEBUG
      std::cout << "Vmin  new = " << param[1] << " +- " << param_err[1] << std::endl;
      std::cout << "Vmean new = " << param[2] << " +- " << param_err[2] << std::endl;
      std::cout << "X     new = " << X << std::endl;
      std::cout << "K     new = " << param[0] << " +- " << param_err[0] << std::endl;
#endif
    }
    else {
#if DEBUG
      for(int ii = 0; ii < 3; ++ii) {
	std::cerr << "par[" << ii << "] = " << param[ii] << " +- " << param_err[ii] << std::endl;
      }
#endif
      print_error("Bad fitting on Voronoi.");
      print_error("Keeping the old estimated values for Voronoi parameters.");
    }
    //*/      


    // increment X on histogram
    P_X.increment(X);

    // other constants
    const double lambda_cube = M_E/100.0;; // = e (pi d^3/6 phiRCP - Vmin)/kRCP
    const double S = UTIL::statmech_S(meanVoro, K, Vmin, lambda_cube);
    //const double vmin_elemental = Vmin/K;
    const double VT = inner_voronoi_volume(); 
    const double C = K*VT/(meanVoro); // Number of elementary cells at the level of the packing
    //const double C = K*volume_from_dl(DL_)/(meanVoro*d3_); // Number of elementary cells at the level of the packing
    // printing
    // print compactivity X
    voronoiXfile.print(X, d3_);
    // print K
    voronoiKfile.print(K);
    // print entropy S
    voronoiSfile.print(S);
    // print entropy S/K
    voronoiSuKfile.print( ((K>0) ? S/K : 0) ); 
    // print C and C/N : Num of el. cells for the whole packing CuN or C/N
    voronoiCfile.print(C);
    voronoiCuNfile.print(C/filtered_by_volume_ids_.size()); 
    // print packing_fraction from stat_mech
    voronoiphifile.print((M_PI/6.0)/(K*X + Vmin)); 
    // print global entropy = C*S/k
    voronoiSTfile.print(C*S/K);
    // print VT
    voronoiVTfile.print(VT);
    // print compactivity Vmin
    voronoiVminfile.print(Vmin, d3_); 
    // print compactivity Vmean
    voronoiVmeanfile.print(meanVoro, d3_); 
    // print compactivity SigmaV
    voronoiVsigmafile.print(sigmaVoro, d3_); 
    // print compactivity SigmaV^2
    voronoiVsigma2file.print(SQUARE(sigmaVoro), SQUARE(d3_)); 
    // print "pressure"
    voronoiPfile.print(-std::log(X/lambda_cube)); 

    UTIL::OFileStream fout;
    // print theoretical distribution data 
    fout.open("POSTPRO/voronoi_distro.dat"); 
    double xmin = Vmin;
    double xmax = 3.5*Vmin;
    double dx = (xmax-xmin)/150;
    for(double x = xmin+dx; x <= xmax; x += dx) {
      double var = (x-Vmin)*d3_/(meanVoro - Vmin);
      fout.print(x, (std::exp(-K*(var - std::log(K*var))))/(tgamma(K)*(x - Vmin)));
    }
    fout.close();
    fout.open("POSTPRO/voronoi_distro_scaled.dat"); 
    xmin = 0;
    xmax = 3.5; 
    dx = (xmax-xmin)/150;
    for(double x = xmin+dx; x <= xmax; x += dx) {
      double var = x;
      fout.print(x, (std::exp(-K*(var - std::log(K*var))))/(tgamma(K)*x));
    }
    fout.close();

    // end
    UTIL::print_done(name);
  }


  //--------------------------------------------------------------------------
  // Start some internal histograms: volume, voronoi volume, nc, etc
  //--------------------------------------------------------------------------
  void PostproAnalysis::start_histograms(void)
  {
    std::string name = "Starting histograms"; UTIL::print_start(name);
    // nc histogram
    P_nc_.construct(0, 10, 10, "P_nc");
    P_nc_full_.construct(0, 10, 10, "P_nc_full");
    // historical histogram for Vx profile
    mean_Vx_profile_normalized_.construct(0.0, 1.0, 15, "mean_Vx_profile");
    // volume of the whole sample histogram : normalized by n*vmin = n*4*pi*rmin^3/3
    histoTotalVol_.construct(0.5, 2.0, 100, "histoTotalVol");
    // voronoi volume histogram
    ASSERT(true == voronoi_ready_);
    double mean_voro, sigma_voro;
    mean_and_sigma_vector(voronoi_volumes_, mean_voro, sigma_voro); // voronoi volumes already computed by common_tasks
    const double minval = 0.650*min_rad_/max_rad_;
    histoVoroVol_.construct(minval, 3.5*minval, 80, "histoVoroVol");
    histoVoroVol_scaled_.construct(0.0, 2.5, 80, "histoVoroVol_scaled");
    // compactivity histogram
    P_X.construct(0.01, 0.4, 20, "P_X");
    // penetration (delta) histograms
    double mind = 0.0, maxd = -1.0e100, mind2 = 0.0, maxd2 = -1.0e100, delta, delta2;
    // check with init values
    for (const auto & c : contacts_init_) {
      delta  = c.delta()*1000/(2*min_rad_); // 1000 = scale factor
      delta2 = delta*delta;
      if (delta  < mind ) mind  = delta  ; 
      if (delta  > maxd ) maxd  = delta  ; 
      if (delta2 < mind2) mind2 = delta2 ; 
      if (delta2 > maxd2) maxd2 = delta2 ; 
    }
    // check with last values
    for (const auto & c : contacts_end_) {
      delta  = c.delta()*1000/(2*min_rad_); // 1000 = scale factor
      delta2 = delta*delta;
      if (delta  < mind ) mind  = delta  ; 
      if (delta  > maxd ) maxd  = delta  ; 
      if (delta2 < mind2) mind2 = delta2 ; 
      if (delta2 > maxd2) maxd2 = delta2 ; 
    }
    if(maxd == -1.0e100 || maxd2 == -1.0e100) maxd = maxd2 = 1.0e-10; 
    ASSERT(mind < maxd); ASSERT(mind2 < maxd2) ;  
    P_delta_.construct( mind , maxd     , 120, "P_delta" ); // 120 bins
    P_delta2_.construct(mind2, 0.5*maxd2, 120, "P_delta2"); // 120 bins
    
    P_packingfraction_.construct(0.3, 0.9, 50, "P_packingfraction");
    P_qoverp_.construct(0.1, 1.1, 120, "P_packingfraction");

    // end
    UTIL::print_done(name);
  }


  //--------------------------------------------------------------------------
  // Voronoi volume per particle histogram
  //--------------------------------------------------------------------------
  void PostproAnalysis::voronoi_volume_histogram(void)
  {
    ASSERT(true == voronoi_ready_);
    std::string name = "Voronoi volume histogram"; UTIL::print_start(name);
    for (auto & id : filtered_by_volume_ids_) {
      //if (particle[id].nc < NC_MIN_) continue; // use only internal with enough contacts
      //if (voronoi_volumes_[id] >= UTIL::voronoi_vmin(dim_))
	histoVoroVol_.increment(voronoi_volumes_[id]);
    }
    // accumulate scaled voronoi volumes
    const double Vmin_ = UTIL::voronoi_vmin(dim_);
    double Vmean, Vsigma;
    histoVoroVol_.mean_and_sigma(Vmean, Vsigma);
    for (auto & id : filtered_by_volume_ids_) {
      const double aux = (voronoi_volumes_[id] - Vmin_)/(Vmean - Vmin_);
      histoVoroVol_scaled_.increment(aux);
    }
    UTIL::print_done(name);
  }

  //--------------------------------------------------------------------------
  // Volume histogram of the whole sample
  //--------------------------------------------------------------------------
  void PostproAnalysis::total_volume_histogram(void)
  {
    std::string name = "Volume histogram"; UTIL::print_start(name);
    histoTotalVol_.increment(internal_volume(BY_CENTERS)/(filtered_by_volume_ids_.size()*min_vol_));
    UTIL::print_done(name);
  }

  //--------------------------------------------------------------------------
  // Void ratio printing
  //--------------------------------------------------------------------------
  void PostproAnalysis::print_void_ratio(void)
  {
    std::string name = "Void ratio and packing fraction"; UTIL::print_start(name);
    double gamma; compute_gamma_def(gamma);
    const double void_ratio = get_void_ratio(); 
    const double packing_fraction = get_packing_fraction();
    voidratiofile.print(gamma, void_ratio, packing_fraction, time);
    packfracfile.print(gamma, packing_fraction, time, get_packing_fraction(false));
    P_packingfraction_.increment(packing_fraction);
    UTIL::print_done(name);
  }

  //--------------------------------------------------------------------------
  // Gamma shear deformation
  //--------------------------------------------------------------------------
  void PostproAnalysis::compute_gamma_def(double & gamma)
  {
    gamma = fabs(particle[ngrains_ + TOP].R[0] - particle_init_[ngrains_ + TOP].R[0])/particle[ngrains_ + TOP].R[2];
  }

  //--------------------------------------------------------------------------
  // Iso and deviatoric deformations: ep, eq
  //--------------------------------------------------------------------------
  void PostproAnalysis::compute_defs(double & ex, double & ey, double & ez, double & ep, double & eq, double & gamma)
  {
    std::string name = "ep and eq"; UTIL::print_start(name);

    const double lxi = particle_init_[ngrains_ + RIGHT].R[0] - particle_init_[ngrains_ + LEFT].R[0]; 
    const double lyi = particle_init_[ngrains_ + FRONT].R[1] - particle_init_[ngrains_ + BACK].R[1]; 
    const double lzi = particle_init_[ngrains_ + TOP].R[2] - particle_init_[ngrains_ + BOTTOM].R[2]; 
    const double lxf = particle[ngrains_ + RIGHT].R[0] - particle[ngrains_ + LEFT].R[0]; 
    const double lyf = particle[ngrains_ + FRONT].R[1] - particle[ngrains_ + BACK].R[1]; 
    const double lzf = particle[ngrains_ + TOP].R[2] - particle[ngrains_ + BOTTOM].R[2]; 
    compute_gamma_def(gamma);
    ep = eq = 0;
    const double vi = (3 == dim_) ? lxi*lzi*lyi : lxi*lzi; 
    const double vf = (3 == dim_) ? lxf*lzf*lyf : lxf*lzf; 
    ex = (lxi - lxf)/lxi;
    ey = (lyi - lyf)/lyi;
    ez = (lzi - lzf)/lzi;
    ep = (vi - vf)/vi;
    eq = ez - ex;

    UTIL::print_done(name);
  }

  void PostproAnalysis::print_deformations(void)
  {
    std::string name = "Printing deformations"; UTIL::print_start(name);

    double ep, eq, ex, ey, ez, gamma;
    compute_defs(ex, ey, ez, ep, eq, gamma);
    deformfile.print(time, ex, ey, ez, ep, eq, gamma);
    eqfile.print(time, eq);

    UTIL::print_done(name);
  }


  //--------------------------------------------------------------------------
  // Stress and fabric tensors
  //--------------------------------------------------------------------------
  void PostproAnalysis::stress_fabric_tensors(void)
  {
    std::string name = "Stress and Fabric tensors"; UTIL::print_start(name);
    double ex, ey, ez, ep, eq, p, q, gamma;
    compute_defs(ex, ey, ez, ep, eq, gamma);
    Macro_.print(time);
    Macro_.p_and_q(dim_, p, q); 
    pqfile.print(time, gamma, p, q);
    gammaqoverpfile.print(gamma, ((p != 0.0) ? q/p : 0));
    eqepfile.print(eq, ep);
    epeqpqfile.print(ep, eq, p, q);
    eqqoverpfile.print(eq, ((p != 0.0) ?  q/p : 0), q, p);
    eq_packfracfile.print(eq, get_packing_fraction());
    double sinphi = (q > 0) ? 3.0*(q/p)/(2.0 + q/p) : 0; 
    internalfrictionfile.print(time, std::asin(sinphi), sinphi);
     // add to historic array
    p_vec.push_back(p);
    q_vec.push_back(q);
    qoverp_vec.push_back(((p!=0) ? q/p: 0));
    eq_vec.push_back(eq);
    gamma_vec.push_back(gamma);
    // histograms
    P_qoverp_.increment(((p != 0.0) ?  q/p : 0));

    UTIL::print_done(name);
  }

  //--------------------------------------------------------------------------
  // Walls info: position
  //--------------------------------------------------------------------------
  void PostproAnalysis::walls_info(void)
  {
    std::string name = "Walls info: position"; UTIL::print_start(name);
    wallsinfofile.print_val(time);
    wallsposfile.print_val(time);
    wallsvelfile.print_val(time);
    wallsforcefile.print_val(time);
    const int LIM = ngrains_ + DEM::NWALLS;
    for (int id = ngrains_; id < LIM; ++id) {
      wallsinfofile.print_val(particle[id].R);
      wallsposfile.print_val(particle[id].R);
    }
    for (int id = ngrains_; id < LIM; ++id) { 
      wallsinfofile.print_val(particle[id].V);
      wallsvelfile.print_val(particle[id].V);
      wallsforcefile.print_val(particle[id].F);
    }
    wallsinfofile.print_newline();
    wallsposfile.print_newline();
    wallsvelfile.print_newline();
    wallsforcefile.print_newline();
    UTIL::print_done(name);
  }
  
  //--------------------------------------------------------------------------
  // Rotational info: PHI and W
  //--------------------------------------------------------------------------
  /*
  void PostproAnalysis::rotational_info(void)
  {
    std::string name = "Rotational info: position"; UTIL::print_start(name);
    rotinfofile.print_val(time);
    for (int id = 0; id < ngrains_ + DEM::NWALLS; ++id) {
      rotinfofile.print_val(particle[id].PHI, particle[id].W);
    }
    rotinfofile.print_newline();
    UTIL::print_done(name);
  }
  //*/
  
  //--------------------------------------------------------------------------
  // Mobolity and brownian displacement: Still needs to know which id to process
  //--------------------------------------------------------------------------
  /*
  void PostproAnalysis::mobility_brownian_displacement(const int id, const double f)
  {
    std::string name = "Mobility and brownian displacement"; UTIL::print_start(name);
    if (filtered_by_volume_ids_.end() == filtered_by_volume_ids_.find(id)) {
      print_error("Id not found inside sample.");
      return;
    }
    double z_min = particle[id].R[2] - 2*max_rad_;
    double z_max = particle[id].R[2] + 2*max_rad_;
    double dx_mean = 0;
    int counter = 0;
    for (int jd = 0; jd < ngrains_; ++jd) {
      if (particle[jd].R[2] >= z_min && particle[jd].R[2] <= z_max) {
	dx_mean += particle[jd].dx() - particle_init_[jd].dx();
	++counter;
      }
    }
    ASSERT(counter >= 1);	
    dx_mean /= counter; 
    brownianfile.print(time, (particle[id].R[0] - particle_init_[id].R[0] - dx_mean)/f, 
			    (particle[id].R[0] - particle_init_[id].R[0] - dx_mean) ); 
    UTIL::print_done(name);
  }
  //*/

  //--------------------------------------------------------------------------
  // Profile: x_translation vs height
  //--------------------------------------------------------------------------
  void PostproAnalysis::mean_x_translation_profile(const int & iter)
  {
    if ( !print_snapshot(iter) ) return;
    std::string name = "Mean translation profile"; UTIL::print_start(name);

    const double xi = particle[ngrains_ + LEFT  ].R[0];
    const double xf = particle[ngrains_ + RIGHT ].R[0];
    const double zi = particle[ngrains_ + BOTTOM].R[2];
    const double zf = particle[ngrains_ + TOP   ].R[2];

    // this is not a historic histogram, the hystory is inside the dx() function which calls particle.R0
    const int bins = 15;
    Histogram1D x_trans_profile;
    x_trans_profile.construct(0.0, 1.0, bins, "x_trans_profile");
    
    // add the x positions to the given heigth
    for(int id = 0; id < ngrains_; ++id) {
      double z = (particle[id].R[2] - zi)/(zf - zi);
      double dx = (particle[id].dx() - particle_init_[id].dx())/(xf - xi); 
      //double dx = particle[id].dx();
      x_trans_profile.accumulate(z, dx);
    }
    std::stringstream filename;
    filename.setf(std::ios::fixed); filename.fill('0');
    
    //// print full histogram
    //filename.str(""); filename << "POSTPRO/translation_profiles/mean_x_translation_FULL_" << std::setprecision(16) << time << ".dat" ;
    //x_trans_profile.print(filename.str());
    //filename.str(""); filename << "POSTPRO/mean_x_translation_FULL.dat" ; // last, overwritten every time
    //x_trans_profile.print(filename.str());
    
    // print means-per-bin histogram
    filename.str(""); filename << "POSTPRO/translation_profiles/mean_x_translation_" << std::setprecision(16) << time << ".dat" ;
    x_trans_profile.print_as_mean_per_bin(filename.str());
    filename.str(""); filename << "POSTPRO/mean_x_translation.dat" ; // last, overwritten every time
    x_trans_profile.print_as_mean_per_bin(filename.str());
    UTIL::print_done(name);
  }

  //--------------------------------------------------------------------------
  // Profile: Vx vs height
  //--------------------------------------------------------------------------
  void PostproAnalysis::mean_Vx_profile(const int & iter)
  {
    if ( !print_snapshot(iter) ) return;
   
    std::string name = "Mean Vx profile"; UTIL::print_start(name);

    // create histogram for this particular time 
    const double zi = particle[ngrains_ + BOTTOM].R[2]; 
    const double zf = particle[ngrains_ + TOP   ].R[2]; 
    const int bins = 15;
    const double VxWall = particle[ngrains_ + TOP].V[0];
    print_log("Current velocity profile : Histogram bins = ", bins);
    Histogram1D mean_Vx_profile, mean_Vx_profile_normalized;
    mean_Vx_profile.construct(zi, zf, bins, "mean_VX_profile");
    mean_Vx_profile_normalized.construct(0.0, 1.0, bins, "mean Vx profile normalized");

    // add the x positions to the given heigth
    for(int id = 0; id < ngrains_ + DEM::NWALLS; ++id) {
      double z = particle[id].R[2];
      double vel = particle[id].V[0];
      mean_Vx_profile.accumulate(z, vel);
      z = (z - zi)/(zf - zi);
      vel = (VxWall != 0) ? vel/VxWall : 0;
      mean_Vx_profile_normalized.accumulate( z, vel);
      mean_Vx_profile_normalized_.accumulate(z, vel); // accumulates for all time
    }
    // PRINT
    std::stringstream filename;
    filename.setf(std::ios::fixed); filename.fill('0');

    //// print full histogram
    //filename.str(""); filename << "POSTPRO/velocity_profiles/mean_Vx_FULL_" << std::setprecision(16) << time << ".dat" ;
    //mean_Vx_profile_.print(filename.str());
    //filename.str(""); filename << "POSTPRO/mean_Vx_FULL.dat" ; // last, overwritten every time
    //mean_Vx_profile_.print(filename.str());

    // print means-per-bin histogram
    filename.str(""); filename << "POSTPRO/velocity_profiles/mean_Vx_" << std::setprecision(16) << time << ".dat" ;
    mean_Vx_profile.print_as_mean_per_bin(filename.str());
    filename.str(""); filename << "POSTPRO/mean_Vx.dat" ; // last, overwritten every time
    mean_Vx_profile.print_as_mean_per_bin(filename.str());
    filename.str(""); filename << "POSTPRO/velocity_profiles/mean_Vx_normalized_" << std::setprecision(16) << time << ".dat" ;
    mean_Vx_profile_normalized.print_as_mean_per_bin(filename.str());
    filename.str(""); filename << "POSTPRO/mean_Vx_normalized.dat" ; // last, overwritten every time
    mean_Vx_profile_normalized.print_as_mean_per_bin(filename.str());
    filename.str(""); filename << "POSTPRO/velocity_profiles/mean_Vx_normalized_alltime_" << std::setprecision(16) << time << ".dat" ;
    mean_Vx_profile_normalized_.print_as_mean_per_bin(filename.str());
    filename.str(""); filename << "POSTPRO/mean_Vx_normalized_alltime.dat" ; // last, overwritten every time
    mean_Vx_profile_normalized_.print_as_mean_per_bin(filename.str());

    // end
    UTIL::print_done(name);
  }


  //--------------------------------------------------------------------------
  // Control: average and maximum penetration among particles
  //--------------------------------------------------------------------------
  void PostproAnalysis::mean_max_penetration(void)
  {
    std::string name = "Mean and maximum penetration"; UTIL::print_start(name);
    double mean = 0, max = 0;
    if(contacts.size() > 0) {
      for(std::vector<Contact>::iterator it = contacts.begin(); it != contacts.end(); ++it) {
	const double S = 0.5*(it->delta())/max_rad_; // normalize by max_d = 2 * max_rad
	//S = (it->delta());
	mean += S;
	if (S > max) max = S;
      }
      mean /= contacts.size();
    }
    meandeltafile.print(time, mean, max, min_rad_);
    UTIL::print_done(name);
  }
  
  //--------------------------------------------------------------------------
  // Displacement as a function of time for a given particle
  //--------------------------------------------------------------------------
/*
  void PostproAnalysis::displacement(int id)
  {
    std::string name = "Displacement"; UTIL::print_start(name);
    if (filtered_by_volume_ids_.end() == filtered_by_volume_ids_.find(id)) {
      print_warning("Id not found inside sample. Changing it...");
      id = ngrains_/2;
    }
    double z_min = particle[id].R[2] - 2*max_rad_;
    double z_max = particle[id].R[2] + 2*max_rad_;
    double dx_mean = 0;
    int counter = 0;
    for (int jd = 0; jd < ngrains_; ++jd) {
      if (particle[jd].R[2] >= z_min && particle[jd].R[2] <= z_max) {
	dx_mean += particle[jd].dx() - particle_init_[jd].dx();
	++counter;
      }
    }
    ASSERT(counter >= 1);	
    dx_mean /= counter; 
    displacementfile.print(time, particle[id].dx() - particle_init_[id].dx(), dx_mean);
    UTIL::print_done(name);
  }
//*/

  //--------------------------------------------------------------------------
  // Grain size profile across the sample, to check segregation
  //--------------------------------------------------------------------------
  void PostproAnalysis::grain_size_profile(void)
  {
    std::string name = "Grain size profile"; UTIL::print_start(name);
    double z0, zf;
    long bins;
    z0 = 0;
    zf = particle_end_[ngrains_ + TOP].R[2];
    bins = zf/(1.9*max_rad_);
    if (bins > 0) {
      Histogram1D histo;
      //histo.construct(z0/zf, zf/zf, bins);
      histo.construct(z0, zf, bins);
      // add the grain size (normalized by maxRad) to the given heigth
      for(const auto & id : filtered_by_volume_ids_) {
	//const double z = particle_end_[id].R[2]/zf;
	const double z = particle_end_[id].R[2];
	//data = particle_end_[id].rad/max_rad_;
	const double data = particle_end_[id].rad;
	histo.accumulate(z, data);
      }
      histo.print("POSTPRO/grain_size_profile_FULL.dat");
      histo.print_as_mean_per_bin("POSTPRO/grain_size_profile.dat");
    }
    else {
      print_warning("No enough bins to compute histogram.");
    }
    UTIL::print_done(name);
  }
  
  
  //--------------------------------------------------------------------------
  // Histograms processing : Calls specific histogram processing functions
  //--------------------------------------------------------------------------
  void PostproAnalysis::process_histograms(const int & iter)
  {
    total_volume_histogram();
    voronoi_volume_histogram();
    contacts_orientation_histograms(iter);
    mean_Vx_profile(iter);
    number_of_contacts_histograms();
    penetration_histograms();
  }

  //--------------------------------------------------------------------------
  // Histograms of contact orientation on different planes: xy, xz, yz
  // Vectors are accepted if they are parallel to the reference plane, 
  // within the tolorance EPS
  //--------------------------------------------------------------------------
  void PostproAnalysis::contacts_orientation_histograms(const int & iter)
  {
    std::string name = "Contact orientation Histograms"; UTIL::print_start(name);
    P_contact_xy_.reset();
    P_contact_xz_.reset();
    P_contact_yz_.reset();
    double angle;
    //for(const auto & c : contacts) { 
    for(const auto & cptr : inside_contacts_refs_) { 
      Vec3d_t Vec1 = cptr.get().Normal_; 
      if (std::fabs(Vec1.dot(Vec_uz)) <= PROJECTION_EPS_) {	
	angle = std::atan2(Vec1[1], Vec1[0]); 
	P_contact_xy_.increment(angle); P_contact_xy_.increment(angle - sign(angle)*M_PI); 
	P_contact_xy_full_.increment(angle); P_contact_xy_full_.increment(angle - sign(angle)*M_PI); 
      }
      if (std::fabs(Vec1.dot(Vec_uy)) <= PROJECTION_EPS_) {	
	angle = std::atan2(Vec1[2], Vec1[0]); 
	P_contact_xz_.increment(angle); P_contact_xz_.increment(angle - sign(angle)*M_PI);
	P_contact_xz_full_.increment(angle); P_contact_xz_full_.increment(angle - sign(angle)*M_PI);
      }
      if (std::fabs(Vec1.dot(Vec_ux)) <= PROJECTION_EPS_) {	
	angle = std::atan2(Vec1[2], Vec1[1]); 
	P_contact_yz_.increment(angle); P_contact_yz_.increment(angle - sign(angle)*M_PI);
	P_contact_yz_full_.increment(angle); P_contact_yz_full_.increment(angle - sign(angle)*M_PI);
      }
    }
    if ( true == print_snapshot(iter) ) {
      std::stringstream ss; ss.setf(std::ios::fixed); ss.fill('0');
      ss.str(""); ss << "POSTPRO/histograms_contacts/contact_xy-" << std::setprecision(16) << time << ".dat";
      P_contact_xy_.print(ss.str());
      ss.str(""); ss << "POSTPRO/histograms_contacts/contact_xz-" << std::setprecision(16) << time << ".dat";
      P_contact_xz_.print(ss.str());
      ss.str(""); ss << "POSTPRO/histograms_contacts/contact_yz-" << std::setprecision(16) << time << ".dat";
      P_contact_yz_.print(ss.str());
    }
    
    UTIL::print_done(name);
  }


  //--------------------------------------------------------------------------
  // penetration histograms
  //--------------------------------------------------------------------------
  void PostproAnalysis::penetration_histograms(void) 
  { 
    // start
    std::string name = "Penetration histograms"; UTIL::print_start(name);
    // process
    //for (const auto & c : contacts) {
    for (const auto & cptr : inside_contacts_refs_) {
      const double delta = cptr.get().delta()*1000/(2*min_rad_); // 1000 = scale factor
      P_delta_.increment(delta);
      P_delta2_.increment(delta*delta);
    }
    // end
    UTIL::print_done(name);
  }

  //--------------------------------------------------------------------------
  // ngrains
  //--------------------------------------------------------------------------
  void PostproAnalysis::print_vmin_cubic(void) 
  { 
    // start
    std::string name = "Vmin for cubic whole samples."; UTIL::print_start(name);
    vmincubicfile.print(time, filtered_by_volume_ids_.size()*std::pow(2*min_rad_, 3)/std::sqrt(2.0));
    // end
    UTIL::print_done(name);
  }

  
  //--------------------------------------------------------------------------
  // Total energy
  //--------------------------------------------------------------------------
  void PostproAnalysis::energy(void)
  {
    std::string name = "Energy"; UTIL::print_start(name);
    double EpH = 0;
    //for(const auto & c : contacts) {
    for(const auto & cptr : inside_contacts_refs_) {
      EpH += Epot(cptr);
    }
    const double norm = (EpH > 0) ? EpH : 1.0e-20;
    energyfile.print(time, (kinetic_energy() + gravitational_energy() + EpH)/norm, kinetic_energy()/norm, 
			  rotational_energy()/norm, translational_energy()/norm, gravitational_energy()/norm,
			  EpH, mean_velocity_trans(), mean_velocity_rot(), 
			  particle[0].rad*particle[0].W[0], particle[0].rad*particle[0].W[1], particle[0].rad*particle[0].W[2], 
			  particle[1].rad*particle[1].W[0], particle[1].rad*particle[1].W[1], particle[1].rad*particle[1].W[2]);
    ekpartfile.print(time, kinetic_energy()/norm);
    ekpartnormfile.print(time, kinetic_energy()/norm, EpH);
    UTIL::print_done(name);
  }

  //--------------------------------------------------------------------------
  // Coordination number from fabric and particles
  //--------------------------------------------------------------------------
  void PostproAnalysis::coordination_number(void)
  {
    // grain.nc includes only grains, not planewalls
    std::string name = "Coordination Number"; UTIL::print_start(name);
    double ncmean = 0;
    if (filtered_grain_ids_.size() >= 1.0) {
      for (const auto & id : filtered_grain_ids_) {
	ncmean += particle[id].nc;
      }
      ncmean /= filtered_grain_ids_.size();
    }
    zfile.print(time, ncmean, Macro_.Fabric_.trace());
    int floating = ngrains_ - filtered_grain_ids_.size();
    floatingfile.print(time, double(floating)/ngrains_, floating, ngrains_);
    UTIL::print_done(name);
  }

  //--------------------------------------------------------------------------
  // Correlation of contacts
  //--------------------------------------------------------------------------
  void PostproAnalysis::init_correlation_nc(void)
  {
    UTIL::print_start("Initializing correlation of contacts.");
    for(const auto c : contacts_init_) 
      ref_contacts_.insert(c.uid());
    print_log("Number of reference contacts : ", ref_contacts_.size());
    if (true == ref_contacts_.empty() ) {
      print_warning("No reference contacts (0 read). No correlation of nc will be performed");
    }
    UTIL::print_done("Initializing correlation of contacts.");
  }

  void PostproAnalysis::correlation_nc(void)
  {
    std::string name = "Correlation of contacts"; UTIL::print_start(name);
    
    if (false == ref_contacts_.empty()) {
      std::set<int> current_contacts, intersection;
      for(const auto & c : contacts) { current_contacts.insert(c.uid()); }
      print_log("Number of current contacts : ", current_contacts.size());
      std::set_intersection(ref_contacts_.begin(), ref_contacts_.end(), 
			    current_contacts.begin(), current_contacts.end(), 
			    std::inserter(intersection, intersection.begin()));
      ref_contacts_.swap(intersection);
      print_log("Remaining contacts from initial set : ", ref_contacts_.size());
      double gamma; compute_gamma_def(gamma);
      nccorrfile.print(gamma, ref_contacts_.size()/contacts_init_.size(), ref_contacts_.size(), time);
    }

    UTIL::print_done(name);
  }

  //--------------------------------------------------------------------------
  // Time correlation of pressure
  //--------------------------------------------------------------------------
  void PostproAnalysis::time_correlation_qoverp(void)
  {
    std::string name = "Time correlation of p"; UTIL::print_start(name);
    
    ASSERT(time_vec.size() == qoverp_vec.size());

    std::vector<double> tc_vec(time_vec.size(), 0.0);
    const int ntime = time_vec.size();
    for (int itime = 0; itime < ntime; ++itime){
      double sum1 = 0, sum2 = 0, sum3 = 0;
      const int limit = ntime - itime;
      for (int jtime = 0; jtime < limit; ++jtime) {
	sum1 += qoverp_vec[jtime]*qoverp_vec[jtime + itime];
	sum2 += qoverp_vec[jtime];
	sum3 += qoverp_vec[jtime + itime];
      }
      tc_vec[itime] = (sum1 - (sum2*sum3/limit))/limit;
    }
    // normalize 
    double maximum = *std::max_element(tc_vec.begin(), tc_vec.end()); // WARNING : could be undefined for size==0 vectors
    for (int i = 0; i < ntime; ++i)
      tc_vec[i] /= maximum;
    // print
    UTIL::OFileStream pcorrfile;
    // print as a funtion of time
    pcorrfile.open("POSTPRO/time_correlation_VS_time.dat"); 
    for (int i = 0; i < ntime; ++i) 
      pcorrfile.print(time_vec[i], tc_vec[i]);
    pcorrfile.close();
    // print as a funtion of eq
    pcorrfile.open("POSTPRO/time_correlation_VS_eq.dat"); 
    for (int i = 0; i < ntime; ++i) 
      pcorrfile.print(eq_vec[i], tc_vec[i]);
    pcorrfile.close();
    // print as a funtion of gamma
    pcorrfile.open("POSTPRO/time_correlation_VS_gamma.dat"); 
    for (int i = 0; i < ntime; ++i) 
      pcorrfile.print(gamma_vec[i], tc_vec[i]);
    pcorrfile.close();
    
    // end
    UTIL::print_done(name);
  }

  //--------------------------------------------------------------------------
  // Print Cundall force equilibrium parameter
  //--------------------------------------------------------------------------
  void PostproAnalysis::cundall_param(void)
  {
    std::string name = "Computing CUNDALL force equilibrium parameter"; UTIL::print_start(name);
    double sumFc, sumTc;
    DEM::util::sum_force_torque_contacts(contacts, sumFc, sumTc);
    const double value = DEM::util::unbalanced_force_torque(particle, ngrains_, sumFc, sumTc, true);
    cundallfile.print(time, value);
    UTIL::print_done(name);
  }
   
  //--------------------------------------------------------------------------
  // Histogram of number of contacts per particle
  //--------------------------------------------------------------------------
  void PostproAnalysis::number_of_contacts_histograms(void)
  {
    std::string name = "Histogram of number of contacts per particle"; UTIL::print_start(name);
    for (auto id : filtered_grain_ids_)
      P_nc_.increment(particle[id].nc);
    for (int id = 0; id < ngrains_; ++id)
      P_nc_full_.increment(particle[id].nc);
    UTIL::print_done(name);
  }

  //--------------------------------------------------------------------------
  // Printing histograms : this allows to print every iteration and/or at the end 
  //--------------------------------------------------------------------------
  void PostproAnalysis::print_histograms(void)
  {
    // start
    std::string name = "Printing Histograms"; UTIL::print_start(name);
    // process
    double mean, sigma;
    histoTotalVol_.print("POSTPRO/histoTotalVol.dat");
    histoTotalVol_.print_mean_sigma("POSTPRO/histoTotalVol_mean_sigma.dat");
    histoVoroVol_.print("POSTPRO/histoVoroVol.dat");
    histoVoroVol_scaled_.print("POSTPRO/histoVoroVol_scaled.dat");
    //voronoi_print_scaled_histogram(histoVoroVol_, "POSTPRO/histoVoroVol_scaled.dat");
    histoVoroVol_.print_ccdf("POSTPRO/voronoi_ccdf.dat");
    P_contact_xy_full_.print("POSTPRO/histograms_contacts/contact_xy.dat");
    P_contact_xz_full_.print("POSTPRO/histograms_contacts/contact_xz.dat");
    P_contact_yz_full_.print("POSTPRO/histograms_contacts/contact_yz.dat");
    P_contact_xy_full_.print("POSTPRO/contact_xy.dat");
    P_contact_xz_full_.print("POSTPRO/contact_xz.dat");
    P_contact_yz_full_.print("POSTPRO/contact_yz.dat");
    P_X.print("POSTPRO/P_X.dat"); 
    P_X.print_mean_sigma("POSTPRO/P_X_mean_sigma.dat");
    P_nc_.print("POSTPRO/P_nc.dat");
    P_nc_.print_mean_sigma("POSTPRO/P_nc-mean_sigma.dat");
    P_nc_full_.print("POSTPRO/P_nc_full.dat");
    P_nc_full_.print_mean_sigma("POSTPRO/P_nc_full-mean_sigma.dat");
    P_delta_.print("POSTPRO/Pdelta.dat"); 
    P_delta_.mean_and_sigma(mean, sigma); if (0 == mean) mean = 1.0; 
    P_delta_.print("POSTPRO/Pdelta_normalized_mean.dat", 1.0/mean, 0.0, mean, 0.0);
    P_delta2_.print("POSTPRO/Pdelta2.dat"); 
    P_delta2_.mean_and_sigma(mean, sigma); if (0 == mean) mean = 1.0; 
    P_delta2_.print("POSTPRO/Pdelta2_normalized_mean.dat", 1.0/mean, 0.0, mean, 0.0); // normalize penetration histograms by mean
    P_packingfraction_.print("POSTPRO/P_packingfraction.dat");
    P_packingfraction_.print_mean_sigma("POSTPRO/P_packingfraction-mean_sigma.dat");
    P_qoverp_.print("POSTPRO/P_qoverp.dat");
    P_qoverp_.print_mean_sigma("POSTPRO/P_qoverp-mean_sigma.dat");
    
    // end
    UTIL::print_done(name);
  }

} // namespace DEM

