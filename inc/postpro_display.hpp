/** This file implements routines to perform post-processing display on the simulation data */

#ifndef __POSTPRO_DISPLAY__
#define __POSTPRO_DISPLAY__


#include "postpro_base.hpp"
#include "animator.hpp"
#include "voro++/voro++.hh"
#include <iostream>
#include <fstream>
#include <iomanip>

namespace DEM {
  
  class PostproDisplay : public Postpro {
  public: // methods
    PostproDisplay(); // default constructor
    void draw_gnuplot(void);
    void draw_paraview(void);
    void draw_povray(void);
    void draw_voronoi(void);
    
  };
  

} // namespace DEM

#endif // POST_PRO
