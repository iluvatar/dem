#ifndef __DICTIONARY__
#define __DICTIONARY__

/*
  Dictionaries:
  typedefs 
  BasicDictionary : map from string to double
  TagDictionary, int tag -> map string, double
  //-----------------------------------------
*/

#include "utility.hpp"
#include "file_util.hpp"
#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>
#include <cstdlib>
#include <string>
#include <unordered_map>
#include <map>

namespace dictionary {
  //typedef std::unordered_map<std::string, double> BasicDictionary;
  //typedef std::unordered_map<std::string, double>::iterator BasicDictionary_it;
  //typedef std::unordered_map<int, BasicDictionary> TagDictionary;
  //typedef std::unordered_map<int, BasicDictionary>::iterator TagDictionary_it;
  typedef std::map<std::string, double> BasicDictionary;
  typedef std::map<std::string, double>::iterator BasicDictionary_it;
  typedef std::map<int, BasicDictionary> TagDictionary;
  typedef std::map<int, BasicDictionary>::iterator TagDictionary_it;
  
  //-----------------------------------------
  // helper functions
  //-----------------------------------------
  void read_file_or_die(BasicDictionary & dict, std::string filename);
  void read_file_or_die(TagDictionary & dict, std::string filename);
  const double & get_value_or_die(BasicDictionary & dict, const std::string & key);
  void add_tagged_values(TagDictionary & tag_dict, const int & tag, std::string s); 
  void tokenize_or_die(const int tag, std::string & s, std::vector<std::string> & tokenVec); // splits the std::string to get key and values
  void print(BasicDictionary & dict);
  void print(const TagDictionary & dict, const std::string filename);

} // namespace dictionary
#endif
