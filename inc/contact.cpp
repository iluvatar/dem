#include "contact.hpp"

namespace DEM
{

  //---------------------------------------------------------------
  // Input/Output operators
  //---------------------------------------------------------------
  //#if !(WITH_NETCDF)
  void
  Contact::print(std::FILE * pf)
  {
    Vec3d_t Fn = normFn_*Normal_;
    // c-style
    std::fprintf(pf, "%d\n%d\n%ld%d\n", uid1_, uid2_, uid_, residual_);
    std::fprintf(pf, "%25.17le\t%25.17le\n", delta_, normFn_);
    std::fprintf(pf, "%25.17le\t%25.17le\t%25.17le\n", Fn[0], Fn[1], Fn[2]);
    std::fprintf(pf, "%25.17le\t%25.17le\t%25.17le\n", Ft_[0], Ft_[1], Ft_[2]);
    std::fprintf(pf, "%25.17le\t%25.17le\t%25.17le\n", T_[0], T_[1], T_[2]);
    std::fprintf(pf, "%25.17le\t%25.17le\t%25.17le\n", Normal_[0], Normal_[1], Normal_[2]);
    std::fprintf(pf, "%25.17le\t%25.17le\t%25.17le\n", SpringTorsion_[0], SpringTorsion_[1], SpringTorsion_[2]);
    std::fprintf(pf, "%25.17le\t%25.17le\t%25.17le\n", SpringSlide_[0], SpringSlide_[1], SpringSlide_[2]);
    std::fprintf(pf, "%25.17le\t%25.17le\t%25.17le\n", SpringRolling_[0], SpringRolling_[1], SpringRolling_[2]);
  }
  //#endif

  //#if !(WITH_NETCDF)
  void
  Contact::read (std::FILE * pf)
  {
    // c-style txt
    updated_ = false;
    sliding_ = false;
    std::cout << "WARNING : Reading of contact files in txt mode is not supported for OLD files. Check your read data." << std::endl;
    int count = std::fscanf(pf, "%d%d%ld%d", &uid1_, &uid2_, &uid_, &residual_);
    if (std::feof(pf)) {
      return;
    }
    ASSERT(3 == count);
    count = std::fscanf(pf, "%lf%lf", &delta_, &normFn_); ASSERT(2 == count);
    count = std::fscanf(pf, "%lf%lf%lf", &Ft_[0], &Ft_[1], &Ft_[2]); ASSERT(3 == count);
    count = std::fscanf(pf, "%lf%lf%lf", &T_[0], &T_[1], &T_[2]); ASSERT(3 == count);
    count = std::fscanf(pf, "%lf%lf%lf", &Normal_[0], &Normal_[1], &Normal_[2]); ASSERT(3 == count);
    count = std::fscanf(pf, "%lf%lf%lf", &SpringTorsion_[0], &SpringTorsion_[1], &SpringTorsion_[2]); ASSERT(3 == count);
    count = std::fscanf(pf, "%lf%lf%lf", &SpringSlide_[0], &SpringSlide_[1], &SpringSlide_[2]); ASSERT(3 == count);
    count = std::fscanf(pf, "%lf%lf%lf", &SpringRolling_[0], &SpringRolling_[1], &SpringRolling_[2]); ASSERT(3 == count);
  }
  //#endif

  //---------------------------------------------------------------
  bool Contact::operator==(const Contact & other)
  {
    return uid1_ == other.uid1_ &&
      uid2_ == other.uid2_ &&
      uid_ == other.uid_ &&
      delta_ == other.delta_ &&
      normFn_ == other.normFn_ &&
      Normal_ == other.Normal_ &&
      Ft_ == other.Ft_ &&
      T_ == other.T_ &&
      SpringTorsion_ == other.SpringTorsion_ &&
      SpringSlide_ == other.SpringSlide_ &&
      SpringRolling_ == other.SpringRolling_ &&
      residual_ == other.residual_
      ;
  }

  //---------------------------------------------------------------
  inline
  void
  Contact::set_reduced_radii(const Particle & Body1, const Particle & Body2,
			     double & radinew, double & radjnew, double & radij)                     // Reduced Radius
  {
    if (Particle::SPHERE == Body2.type)
      {
	// fast-approximate values (exact only for mono-disperse systems)
	//radinew = Body1.rad - 0.5*delta_;
	//radjnew = Body2.rad - 0.5*delta_;
	// slow-exact values : ref: Oquendo, W. F. ; Msc Thesis
	radinew = Body1.rad - 0.5*delta_*(2*Body2.rad - delta_)/(Body1.rad + Body2.rad - delta_);
	radjnew = Body2.rad - 0.5*delta_*(2*Body1.rad - delta_)/(Body1.rad + Body2.rad - delta_);
	// reduced radius
	radij = reduced_value(radinew, radjnew);
      }
    else                                                                    // WALL
      {
	radinew = Body1.rad - delta_;
	radjnew = 0;
	radij = Body1.rad - delta_;
      }
  }

  //---------------------------------------------------------------
  inline
  bool 
  Contact::friction_force(const double & kt, const double & mus, const double & muk, const double & gammat,
			  const Vec3d_t & Vt, 
			  Vec3d_t & Spring, Vec3d_t & Ft, 
			  const double & alpha) // alpha's default value = 1.0
  {
    // Luding 2008
    // Ftest => Starts with Static friction
    bool sliding = false;
    Ft = Spring*(-alpha*kt) - Vt*(gammat);
    const double Ft_norm2 = Ft.squaredNorm();
    // cohesion
    if (Ft_norm2 > SQUARE(mus*normFn_)) {  // kinetic. TODO : Check continuity when switching kinetic/static 
      if (Vt.squaredNorm() > 1.0e-6) sliding = true; // candidate for sliding. WARNING: Magic constant
      // Luding Model
      Ft *= muk*normFn_/std::sqrt(Ft_norm2);
      Spring = (Ft + Vt*(gammat))/(-alpha*kt); // Reset the lenght of the spring after changing Ft
    }
    return sliding;
  }

  //---------------------------------------------------------------
  void
  Contact::pre_reset()
  {
    updated_ = false;
    sliding_ = false;
    delta_ = 0.0;
  }

  //---------------------------------------------------------------
  void
  Contact::reset()
  {
    pre_reset();
    normFn_ = 0;
    Normal_ = Vecnull;
    T_ = Ft_ = Vecnull;
    SpringTorsion_ = SpringSlide_ = SpringRolling_ = Vecnull;
  }

  //---------------------------------------------------------------
  void
  Contact::pre_setup(const Particle * ptr1, const Particle * ptr2, const double & delta, const Vec3d_t & Normal)
  {
    ASSERT(nullptr != ptr1); ASSERT(nullptr != ptr2); 
    uid1_ = ptr1->uid; uid2_ = ptr2->uid;
    ptr1_ = const_cast<Particle *>(ptr1); 
    ptr2_ = const_cast<Particle *>(ptr2);
    ASSERT(nullptr != ptr1_); ASSERT(nullptr != ptr2_); 
    uid_ = get_uid(uid1_, uid2_);
    delta_ = delta;
    Normal_ = Normal;
    updated_ = false;
    sliding_ = false;
    
    if (true == starting_) {
      // cohesion
      Pi_ = ptr1->R - Normal_*(ptr1->rad - delta_/2);
      Pj_ = ptr2->R + Normal_*(ptr2->rad - delta_/2);

      starting_ = false;
    }
  }

  /**
     @ brief Computes the actual forces at this contact. Takes care of residual behaviour in material stuff.
     @param[in] MaterialVars : Class containing the full list of composite materials, will return a constant reference for the specific material
     @param[in] dt : Time step, used for updating the fricitonal springs.
     @return A boolean indicating a true contact  
     Force Model from Luding: Cohesive, frictional powders: contact models for tension, Granular Matter (2008) 10:235–246
     Cohesion model from : Cohesive granular texture, F. Radjai, I. Preechawuttipong, R. Peyroux, inside Continuous and Discontinuous Modelling of Cohesive-Frictional Materials, page 161
  **/
  bool 
  Contact::collide(const Materials & MaterialVars, const double & dt)
  {
#if DEBUG
    ASSERT(nullptr != ptr1_); ASSERT(nullptr != ptr2_); 
#endif
      
    //------------------------------------------------------------------------------------------
    // references to bodies
    Particle & Body1 = *ptr1_;
    Particle & Body2 = *ptr2_;
    //------------------------------------------------------------------------------------------
    // reference to materials
    const Materials::BasicMat_t & material = MaterialVars(Body1.materialTag, Body2.materialTag, residual_);

    // reduced radii taking into account cohesion
    double radinew = 0, radjnew = 0, radij = 0;
    set_reduced_radii(Body1, Body2, radinew, radjnew, radij);
    radij = reduced_value(Body1.rad, Body2.rad); // ignores the penetration
    
    // distance check for openned contact, taking into account cohesion
    if (delta_ <= EPS_) {
      broken_ = residual_ = true;
      reset();
      return false;
    }

    if (material[STRESSC] < 0.0) { // NO COHESION
      broken_ = true;
    }

    // cohesion breaking : tangential displacement should be small
    if (false == broken_) {
      Pi_ += dt*(ptr1_->V + ptr1_->W.cross(Pi_ - ptr1_->R)); // update Pi_ contact position
      Pj_ += dt*(ptr2_->V + ptr2_->W.cross(Pj_ - ptr2_->R)); // update Pj_ contact position
      if ((Pi_ - Pj_).squaredNorm() > SQUARE(2*material[OMEGAC]*radij) ) {
	//std::cout << "COHESION : Breaking contact" << std::endl;
	//std::cin.get();
        residual_ = true;
        broken_ = true;
      }
    }

    //------------------------------------------------------------------------------------------
    // resets, every time 
    sliding_ = false;
    T_ = Ft_ = Vecnull;
    updated_ = false;
    normFn_ = 0;
    //------------------------------------------------------------------------------------------
    // Normal repulsive force
    double frep = 0;
    if (delta_ > EPS_) {
      frep = material[K]*delta_;                                            // linear elastic
    }

    // cohesion
    double fcoh = 0.0, fc = 0;
    if (false == broken_) {
      fcoh = material[STRESSC]*2*radij*std::sqrt(radij*delta_) ; 
      fc = SQUARE(material[STRESSC]*2*radij)*radij/(4*material[K]);
    }
    
    // disiipative force
    double fdis = 0.0;
    Vec3d_t Vij(Body1.V - Body2.V);
    fdis = material[GAMMA]*(Vij.dot(Normal_));                           // dissipative

    // Total normal force 
    double fn = frep - fcoh - fdis; 
    if (material[STRESSC] < 0.0 && fn < 0) fn = 0; // does not allow attractive forces when no cohesion

    // total normal force magnitude, for friction, = fn + |fc|
    normFn_ = fn + fc;    
    //if (fc > 0) {
    //normFn_ = fn*(1.0 + 2.0*(fc/fn) + 2.0*(fc/fn)*std::sqrt(1.0 + (fn/fc))); 
    //}

    // aux vars
    Vec3d_t Vaux, Faux, Taux;
    double alphaMcNamara = 1.0;                                      // alpha = McNamara correction
    
    // since contact is really active, start it
    updated_ = true;
    ++Body1.nc; ++Body2.nc;                                             // update again since nc is reseted for each time step
    double dist = radinew + radjnew - delta_;
    Vec3d_t Vn((Vij.dot(Normal_))*Normal_);
    //------------------------------------------------------------------------------------------
    // NORMAL INTERACTIONS
    //------------------------------------------------------------------------------------------
    Faux = (fn)*Normal_; // actual total normal force
    Body1.F += Faux;
    Body2.F -= Faux;                                                     // third Newton law
    //------------------------------------------------------------------------------------------
    // NORMAL TORSION; only torque 
    ///*// Simple viscous model : decouples torsion from sliding and rolling since its parameters could be related
    Vaux = radij*(Normal_.dot(Body1.W - Body2.W))*Normal_;
    Taux = -4.0*M_PI*delta_*material[GAMMA]*Vaux; // My model from the Msc
    Body1.T += Taux;
    Body2.T -= Taux;
    T_ += Taux;
    //------------------------------------------------------------------------------------------
    // TANGENTIAL FORCES AND TORQUES
    //------------------------------------------------------------------------------------------
    ///*// Static-Kinetic SLIDING Friction force,
    if ( std::fabs(material[MUSF]) >= EPS_FRICTION_ ) {
      alphaMcNamara = (Body1.rad + Body2.rad)/dist;
      Vaux = alphaMcNamara*(Vij - Vn); Vaux += (Normal_.cross(radinew*Body1.W + radjnew*Body2.W));
      SpringSlide_ += (Vaux - Normal_*(SpringSlide_.dot(Vij)/dist))*dt; // LUDING
      //SpringSlide_ -= Normal_*((SpringSlide_.dot(Vij)/dist)*dt); // LUDING
      //SpringSlide_ -= Normal_*(SpringSlide_.dot(Normal_));
      sliding_ = friction_force(material[KTF], material[MUSF], material[MUKF], material[GAMMATF], Vaux, SpringSlide_, Faux, alphaMcNamara);
      Body1.F += Faux;
      Body2.F -= Faux;
      Ft_ += Faux;
      Taux = (-radinew)*(Normal_.cross(Faux));
      Body1.T += Taux;
      T_ += Taux;
      Taux = (-radjnew)*(Normal_.cross(Faux));
      Body2.T += Taux;
    }
    //*/
    //------------------------------------------------------------------------------------------
    ///*// ROLLING, only torque,
    if ( std::fabs(material[MUSR]) >= EPS_FRICTION_ ) {
      alphaMcNamara = 1.0;
      Vaux = radij*Normal_.cross(Body1.W - Body2.W); 
      SpringRolling_ += Vaux*dt ; // - (SpringRolling_.dot(Normal_))*Normal_; // LUDING
      //SpringRolling_ -= Normal_*(SpringRolling_.dot(Normal_)); // LUDING
      friction_force(material[KTR], material[MUSR], material[MUKR], material[GAMMATR], Vaux, SpringRolling_, Faux, alphaMcNamara);
      //const double arm = radij; //Luding, same branch vector for both particles
      const double arm = radinew + radjnew; // As Estrada in CD: branch vector = rad1 + rad2 - delta 
      Body1.T += -arm*(Normal_.cross(Faux));
      Body2.T -= -arm*(Normal_.cross(Faux));
      T_ += Taux;
    }   
    return true;
  }
} // end namespace  
                  
