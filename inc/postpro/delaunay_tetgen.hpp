// Computes the delaunay tessellation for a sample

#ifndef __POSTPRO_DELAUNAY__
#define __POSTPRO_DELAUNAY__

#include "postpro_base_functor.hpp"
#include "../histogram1d.hpp"
#include "../nlfit.hpp"
#include "../functions.hpp"
#include "../file_util.hpp"
#include "../particle.hpp"
#include <vector>
#include "tetgen.h"

namespace DEM {
  namespace POSTPRO {
    //----------------------------------------------------------
    // Class Delaunay : performs delaunay tessellations and computes SM quantities
    //----------------------------------------------------------
    class Delaunay : public Base_functor {
    public :
      Delaunay(){};
      int init_tasks(const double & minrad, const double & maxrad) ;
      int end_tasks () ;
      double total_volume() { return total_vol_; };
      double ntetra()       { return ntetra_; };
      template <class IndexList> 
      int operator()(const std::vector<Particle> & bodies, const IndexList & indexes, const int & nwalls, const int & ncmin,
		     const double & inner_volume, const double & VMIN_FACTOR) 
      {
	// This will use the whole system to perform the tessellation, and then use only cells
	// which are build from internal particles
      
	if (indexes.size() <= 3) return EXIT_FAILURE;
	if (ncmin <= 0) return EXIT_FAILURE;
	UTIL::print_start("Delaunay");
	_UNUSED(nwalls);
	
	//----------------------------------------------------------    
	// COMPUTE TETRAHEDRONS AND THEIR VOLUMES

	// declare tetgenio objects
	tetgenio tet_in, tet_out;

	// // INCLUDING ONLY FILTERED PARTICLES
	// // fill tetgenio in object with particles data
	// tet_in.numberofpoints = indexes.size(); // NOTE: Only including accepted particles
	// print_log("Number of points to process  : ", tet_in.numberofpoints);
	// if (nullptr != tet_in.pointlist) { delete [] tet_in.pointlist; tet_in.pointlist = nullptr; }
	// tet_in.pointlist = new REAL [tet_in.numberofpoints * 3];
	// long ii = 0;
	// for (const auto & idx : indexes) {
	//   for (int dir = 0; dir < 3; ++dir) { 
	//     tet_in.pointlist[3*ii + dir] = bodies[idx].R[dir]; 
	//   }
	//   ++ii;
	// }
	
	// INCLUDING ALL PARTICLES
	// fill tetgenio in object with particles data
	tet_in.numberofpoints = bodies.size(); // NOTE: Only including accepted particles
	print_log("Number of points to process  : ", tet_in.numberofpoints);
	if (nullptr != tet_in.pointlist) { delete [] tet_in.pointlist; tet_in.pointlist = nullptr; }
	tet_in.pointlist = new REAL [tet_in.numberofpoints * 3];
	for (int idx = 0; idx < tet_in.numberofpoints; ++idx) {
	  for (int dir = 0; dir < 3; ++dir) { 
	    tet_in.pointlist[3*idx + dir] = bodies[idx].R[dir]; 
	  }
	}
      
	// tetrahedralize the system
	char tetparam[] = "KCQKG";
	tetrahedralize(tetparam, &tet_in, &tet_out);

#if DEBUG
	tet_in.save_nodes("POSTPRO/delaunay_in");      // save input  nodes
	tet_out.save_nodes("POSTPRO/delaunay_out");    // save output nodes
	tet_out.save_elements("POSTPRO/delaunay_out"); // save output tetra
	tet_out.save_faces("POSTPRO/delaunay_out");    // save output faces
#endif
	// release memory after usage
	delete [] tet_in.pointlist; tet_in.pointlist = nullptr;


	// for each tetrahedron on tetgenio out, compute the volume and add to histogram
	std::vector<double> Vdela; Vdela.reserve(3*bodies.size()); // array to store delaunay volumes
	Vec3d_t Vertices[4];
	int counter = 0;
	ntetra_ = tet_out.numberoftetrahedra;
	total_vol_ = 0;
	print_log("ntetra = ", ntetra_);
	for (long idx = 0; idx < tet_out.numberoftetrahedra; ++idx) {
	  // select tetra to process: 
	  // 1) at least min_inside_vertex inside 
	  // 2) At least min_nc_vertex vertexes has ncmin contacts

	  const int min_inside_vertex = 3;
	  const int min_nc_vertex = ncmin;
	  int ninside = 0;
	  int nnc = 0;
	  for (int ii = 0; ii < 4; ++ii) {
	    int vertex_idx = tet_out.tetrahedronlist[4*idx + ii];
	    if (indexes.end() != indexes.find(vertex_idx)) { ++ninside; } 
	    if (bodies[vertex_idx].nc >= ncmin) { ++nnc; }
	  }
	  if (nnc < min_nc_vertex || ninside < min_inside_vertex) continue;
	  counter++;
	  // compute volume
	  for (int ii = 0; ii < 4; ++ii) {
	    int vertex_idx  = tet_out.tetrahedronlist[4*idx + ii];
	    Vertices[ii] = Vec3d_t(tet_out.pointlist[vertex_idx*3 + 0], tet_out.pointlist[vertex_idx*3 + 1], tet_out.pointlist[vertex_idx*3 + 2]);
	  }
	  const double vol = std::fabs((Vertices[0] - Vertices[3]).dot((Vertices[1] - Vertices[3]).cross(Vertices[2] - Vertices[3])))/6;
	  // add to histograms
	  if (vol/CUBE(d_) >= VMIN_FACTOR*Vmin_) { // CHECK
	    histo_.increment(vol/CUBE(d_)); // NOTE: For Delaunay cells, we could have smaller than Vmin volumes, specially at borders ;)
	    Vdela.push_back(vol/CUBE(d_));
	  }
	  total_vol_ += vol/CUBE(d_);
#ifdef DEBUG
	  if (vol/CUBE(d_) < Vmin_) {
	    //print_debug("Vmin      = ", Vmin_);
	    //print_debug("vol       = ", vol);
	    //print_debug("vol/Vmin  = ", vol/Vmin_);
	  }
#endif
	  // full histo
	  histo_full_.increment(vol/CUBE(d_)); 
	}
	print_log("ntetra selected = ", counter);
	const int ncells = counter;
	ncells_out_.print(ncells);
       
	if (histo_.empty()) { 
	  print_log("Delaunay volume histogram is empty. Returning ...");
	  histo_.print("POSTPRO/histo_delaunay_vol.dat");
	  histo_full_.print("POSTPRO/histo_full_delaunay_vol.dat");
	  return EXIT_SUCCESS;
	}


	//scaled histo
	double mean, sigma; 
	histo_.mean_and_sigma(mean, sigma);
	for (const auto & vol : Vdela) {
	  const double aux = (vol - Vmin_)/(mean - Vmin_);
	  histo_scaled_.increment(aux); 
	}
	
	//----------------------------------------------------------    
	// SM VARIABLES
	// compute the compactivity, entropy, and k
	//histo_full_.mean_and_sigma(mean, sigma);
#if DEBUG
	histo_.print("POSTPRO/histo_delaunay_vol.dat");
	histo_scaled_.print("POSTPRO/histo_delaunay_scaled.dat");
	histo_full_.print("POSTPRO/histo_full_delaunay_vol.dat");
	print_log("mean  = ", mean );
	print_log("Vmin_ = ", Vmin_);
	ASSERT(mean > Vmin_);
#endif
	double X = 0, K = 0;
	if (sigma > 0) {
	  X = UTIL::statmech_X(mean, sigma, Vmin_);
	  K = UTIL::statmech_K(mean, sigma, Vmin_);
	}
	print_log("mean  =", mean);
	print_log("K     =", K);
	print_log("X     =", X);
	print_log("Vmin_ =", Vmin_);
	double newVmin = Vmin_;
      
	//----------------------------------------------------------    
	// ADJUST VALUES BY FITTING TO IMPROVE SM PARAMETERS COMPUTATIONS 

      
	/*// get valid data from histo (only values where pdf != 0)
	  const int size = histo_.get_valid_bins_size(); 
	  double *xdata, *ydata; xdata = new double [size]; ydata = new double [size]; 
	  histo_.get_valid_data(xdata, ydata);
      
	  // compute k by maximum likelihood (aproximated):
	  // http://en.wikipedia.org/wiki/Gamma_distribution#Maximum_likelihood_estimation
	  if (size > 0) {
	  int n = 0;
	  int *count = new int [size]; 
	  histo_.get_valid_counters(xdata, count);
	  double s = 0, sum1 = 0, sum2 = 0;
	  print_log("Vmin orig  = ", Vmin_);
	  print_log("Vmin histo = ", xdata[0]*0.99);
	  for (int ii = 0; ii < size; ++ii) {
	  const double xi = xdata[ii] - xdata[0]*0.99; 
	  sum1 += count[ii]*xi;
	  sum2 += count[ii]*std::log(xi);
	  n += count[ii];
	  }
	  s = std::log(sum1/n) - sum2/n;
	  K = (3.0 - s + std::sqrt(SQUARE(s-3.0) + 24*s))/(12*s);
	  print_log("K from likelihood =", K);
	  print_log("X new  =", (mean - Vmin_)/K);
	  delete [] count; count = nullptr;
	  }
	  delete [] xdata; xdata = nullptr;
	  delete [] ydata; ydata = nullptr;
	//*/
     
	///*// fit the ccdf = 1 - cdf to estimate parameters 
	// This is better than fitting the pdf, since it "cleans" the data
	histo_.print_ccdf("POSTPRO/delaunay_ccdf.dat");
	histo_full_.print_ccdf("POSTPRO/delaunay_full_ccdf.dat");
	std::vector<double> xval, ccdf;
	histo_.save_ccdf(xval, ccdf);
	double param_err[3] = {0};
	double param[3] = {1.2*K, 0.95*Vmin_, mean}; 
	int status = NLFIT::nlfit(3, xval.size(), &xval[0], &ccdf[0], qgamma, param, param_err, 3);
	if (EXIT_SUCCESS == status) { // update parameters
	  K = param[0]; newVmin = param[1]; mean = param[2]; X = (mean - newVmin)/K; 
#if DEBUG
	  std::cout << "K     new = " << param[0] << " +- " << param_err[0] << std::endl;
	  std::cout << "Vmin  new = " << param[1] << " +- " << param_err[1] << std::endl;
	  std::cout << "Vmean new = " << param[2] << " +- " << param_err[2] << std::endl;
	  std::cout << "X     new = " << X << std::endl;
#endif
	}
	else {
#if DEBUG
	  for(int ii = 0; ii < 3; ++ii) {
	    std::cerr << "par[" << ii << "] = " << param[ii] << " +- " << param_err[ii] << std::endl;
	  }
#endif
	  print_error("Bad fitting on Delaunay.");
	  print_error("Keeping the old estimated values for Delaunay parameters.");
	}
	//*/      
      
	/*// fit the pdf values to estimate K, Vmin // replaced in favor of ccdf fit
	  double param_err[3] = {0};
	  double param[3] = {std::max(K, 4.0), 0.95*Vmin_, mean}; // Last one is fixed, since npar = 2 at nlfit
	  int status = NLFIT::nlfit(2, size, x, y, kgamma_k_Vm_Vmean, param, param_err, 3);
	  if (EXIT_SUCCESS == status) { // update parameters
	  K = param[0]; newVmin = param[1]; mean = param[2];  X = (mean - newVmin)/K;
	  #if DEBUG
	  std::cout << "K    new = " << K       << " +- " << param_err[0] << std::endl;
	  std::cout << "Vm   new = " << newVmin << " +- " << param_err[1] << std::endl;
	  std::cout << "mean new = " << mean    << " +- " << param_err[2] << std::endl;
	  std::cout << "X    new = " << X << std::endl;
	  #endif
	  }
	  else {
	  #if DEBUG
	  for(int ii = 0; ii < 3; ++ii) {
	  std::cerr << "par[" << ii << "] = " << param[ii] << " +- " << param_err[ii] << std::endl;
	  }
	  #endif
	  print_error("Bad fitting on Delaunay.");
	  print_error("Keeping the old estimated values for Delaunay parameters.");
	  }
	//*/

	// other constants
	const double lambda_cube = M_E/100.0; // Normalized. FIX WITH RIGHT VALUE FOR K_{RCP}^{DELA}
	const double S = K*(1.0 + std::log((mean - newVmin)/(K*lambda_cube)));
	const double vmin_elemental = newVmin/K;

	//----------------------------------------------------------    
	// PRINTING

	// print parameters
	// print delaunay raw volumes
	print_log("Printing Delaunay volumes (for accepetd internal particles)");
	std::ofstream foutdela; foutdela.open("POSTPRO/delaunay_volumes.dat");
	for(const auto & id : indexes) {
	  foutdela << Vdela[id] << "\n";
	}
	foutdela.close();
	
	phi_out_.print((indexes.size()*M_PI/6.0)/(ncells*(K*X+newVmin)));
	Xout_.print(X, Vmin_); 
	Kout_.print(K);
	Sout_.print(S); 
	SuKout_.print(( (K > 0) ? S/K : 0.0 )); 
	vmineleout_.print(vmin_elemental, vmin_elemental, CUBE(d_));  
	Xuvmineleout_.print(( (vmin_elemental > 0) ? X/vmin_elemental : 0 )); 
	Vmean_out_.print(mean, CUBE(d_));
	Vsigma_out_.print(sigma, CUBE(d_));
	Vsigma2_out_.print(sigma*sigma, CUBE(d_*d_));
	Vmin_out_.print(newVmin);
	const double C = (mean > 0) ? K*inner_volume*1.1/mean : 0 ; // 1.1 fixes scale problem associated with k (too small) or Vmean (too large)
	Cout_.print(C);
	CuNout_.print( ((bodies.size() > 0) ? C/indexes.size() : 0) );
	VT_out_.print(total_vol_, CUBE(d_));
	print_log("Total internal Volume from Delaunay (/d^3) = ", total_vol_);

	// print histograms
	histo_.print_mean_sigma("POSTPRO/histo_delaunay_vol_mean_sigma.dat");
	histo_.print("POSTPRO/histo_delaunay_vol.dat");
	histo_scaled_.print("POSTPRO/histo_delaunay_scaled.dat");
	//histo_.print("POSTPRO/histo_delaunay_scaled.dat", 
	//1.0/(mean - newVmin), -newVmin/(mean - newVmin), mean - newVmin, 0);
	double meanfull, sigmafull;
	histo_full_.mean_and_sigma(meanfull, sigmafull);
	histo_full_.print("POSTPRO/histo_delaunay_full_vol_orig.dat");
	histo_full_.print("POSTPRO/histo_delaunay_full_scaled.dat", 
			  1.0/(meanfull - newVmin), -newVmin/(meanfull - newVmin), meanfull - newVmin, 0);

	// print theoretical distro
	print_debug("Start printing theoretical.");
	double xmin, xmax, dx;
	UTIL::OFileStream fout;
	fout.open("POSTPRO/delaunay_distro.dat"); 
	xmin = 0.99*newVmin; xmax = 3.5*Vmin_; dx = (xmax - xmin)/150;
	//ASSERT(dx > 0);
	if (dx > 0) {
	  for(double x = xmin + dx; x <= xmax; x += dx) {
	    double var = (x - newVmin)/X;
	    fout.print(x, std::exp(-var + (K-1.0)*std::log(var))/(tgamma(K)*(X)));
	  }
	}
	fout.close();
	print_debug("Start printing theoretical 2.");
	fout.open("POSTPRO/delaunay_distro_scaled.dat"); 
	xmin = 0.01; xmax = 5.5; dx = (xmax - xmin)/150;
	// ASSERT(dx > 0);
	if (dx > 0) {
	  for(double x = xmin; x <= xmax; x += dx) {
	    fout.print(x, (std::exp(-K*(x - std::log(K*x))))/(tgamma(K)*x));
	  }
	}
	fout.close();
	print_debug("Done printing theoretical.");

	// print other data

	UTIL::print_done("Delaunay");
	return EXIT_SUCCESS; 
      }

    private :
      UTIL::OFileStream Xout_, Kout_, Sout_, SuKout_, vmineleout_, Xuvmineleout_, 
	  Vmean_out_, Vmin_out_, Cout_, CuNout_, Vsigma_out_, Vsigma2_out_, VT_out_, ncells_out_, phi_out_;
	double d_         = 0;
	double Vmin_      = 0;
	double total_vol_ = 0;
	int ntetra_       = 0;
      Histogram1D histo_, histo_scaled_, histo_full_; // histo_full_ includes cells with volume < Vmin
      };

    } // namespace POSTPRO
} // namespace DEM

#endif // __POSTPRO_DELAUNAY__
