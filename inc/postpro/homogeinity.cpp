// Computes homogeinity per corona

#include "postpro/homogeinity.hpp"

namespace DEM {
  namespace POSTPRO {
    
    // NOTE: Using the last particle state, which could be smaller than other samples. Therefore, coronas will not include all particles.
    int Homogeinity::init_tasks(const std::vector<Particle> & particles, const int & ngrains, const double & d, const int & dim) {
      ngrains_ = ngrains;
      d_ = d;
      const Vec3d_t RT(particles[ngrains_+RIGHT ].x(), 
		       particles[ngrains_+FRONT ].y(), 
		       particles[ngrains_+TOP   ].z());
      const Vec3d_t LB(particles[ngrains_+LEFT  ].x(), 
		       particles[ngrains_+BACK  ].y(), 
		       particles[ngrains_+BOTTOM].z());

      // create coronas with a least a widht = 2d, constant volume
      //UTIL::compute_coronas_limits_constant_volume(LB, RT, 2.0*d_, dim, ncoronas_, corona_leftbottom_, corona_righttop_);
      // create coronas with a constant widht = 1.5d, changing volume
      ncoronas_ = std::max(int(UTIL::min_component(RT-LB, dim)/(1.9*d_)), 2); // suggestion, since width is bit necesarily a divisor of Lmin
      UTIL::compute_coronas_limits_constant_width(LB, RT, dim, ncoronas_, corona_leftbottom_, corona_righttop_);
      
      // reserve memory
      stress_tensors_.resize(ncoronas_);
      full_stress_tensor_.setZero(); for (auto & stress : stress_tensors_) stress.setZero();
      p_.resize(ncoronas_);
      q_.resize(ncoronas_);
      phi_.resize(ncoronas_);
      zc_.resize(ncoronas_);
      counter_.resize(ncoronas_);

      // compute other vars
      Vec3d_t L = RT - LB;
      Vtot_ = (2 == dim) ? L[0]*L[2] : L[0]*L[1]*L[2]; 
      V0_ = Vtot_/ncoronas_;

      ready_ = true;						                            
      return EXIT_SUCCESS;					                
    }
    
    int 
    Homogeinity::operator()(const std::vector<Contact> & contacts, const std::vector<Particle> & particles, 
			    const bool periodic[3], const int & dim, const int & ncmin) {
      UTIL::print_start("Computing homogeinity");
      ASSERT(true == ready_);
      ASSERT(2 == dim || 3 == dim);
      
      // compute p/p0 and q/p0 per corona, where p0 is the global p 
      print_log("\t\tComputing p and q ...");
      full_stress_tensor_.setZero();
      for (auto & stress : stress_tensors_) stress.setZero();
      for (const auto & contact : contacts) {
	const auto id1 = contact.uid1_;
	const auto id2 = contact.uid2_;
	if (particles[id1].nc < ncmin) continue; // ignore low coordination
	if (particles[id2].nc < ncmin) continue; // ignore low coordination
	double factor = 0.5; if (id2 >= ngrains_) factor = 1.0;
	const Vec3d_t C = particles[id1].R - contact.Normal_*(particles[id1].rad - factor*contact.delta_); // middle point position
	const Vec3d_t Branch = contact.Normal_*(particles[id1].rad + particles[id2].rad - contact.delta_); // branch vector
	full_stress_tensor_ += contact.F()*Branch.transpose();
	const double eps = 1.0e-10; // precision to check for cercany among points
	bool imfound = false;
	for (int im = 0; im < ncoronas_; ++im) {
	  if ( UTIL::is_vector_inside(C, corona_leftbottom_[im], corona_righttop_[im], eps) ) {
	    stress_tensors_[im] += contact.F()*Branch.transpose();
	    imfound = true;
	    break;
	  }
	}
	if (false == imfound) {
#if DEBUG_HOMOGEINITY_CORONAS
	  UTIL::is_vector_inside(C, corona_leftbottom_[ncoronas_-1], corona_righttop_[ncoronas_-1], eps);
	  print_log("C = ", C);
	  print_log("id1 = ", id1);
	  print_log("id2 = ", id2);
	  print_log("LBmin = ", corona_leftbottom_[ncoronas_-1]);
	  print_log("RTmax = ", corona_righttop_[ncoronas_-1]);
	  print_log("left   = ", particles[ngrains_ + LEFT  ].x());
	  print_log("right  = ", particles[ngrains_ + RIGHT ].x());
	  print_log("back   = ", particles[ngrains_ + BACK  ].y());
	  print_log("front  = ", particles[ngrains_ + FRONT ].y());
	  print_log("bottom = ", particles[ngrains_ + BOTTOM].z());
	  print_log("top    = ", particles[ngrains_ + TOP   ].z());
#endif // DEBUG_HOMOGEINITY_CORONAS 
	}
	//ASSERT(true == imfound); 
      }      
      full_stress_tensor_ /= Vtot_;
      for (int im = 0; im < ncoronas_; ++im) {
	//stress_tensors_[im] /= V0_;
	double corona_volume = UTIL::box_volume(corona_leftbottom_[im], corona_righttop_[im], dim);
	if (im >= 1) corona_volume -= UTIL::box_volume(corona_leftbottom_[im-1], corona_righttop_[im-1], dim);
	stress_tensors_[im] /= corona_volume;
      }
      double p_global, q_global;
      get_p_q(full_stress_tensor_, p_global, q_global);
      for (int im = 0; im < ncoronas_; ++im) {
	get_p_q(stress_tensors_[im], p_[im], q_[im]);
	p_[im] /= p_global;
	q_[im] /= p_global;
      }
      UTIL::print_done("Computing p and q.");
      
      // Compute zc and phi per corona
      print_log("\t\tComputing zc and phi per corona.");
      for (auto & x : phi_) x = 0; 
      for (auto & x : zc_) x = 0; 
      std::fill(counter_.begin(), counter_.end(), 0);
      for (const auto & body : particles ) {
	if (body.uid >= ngrains_) continue; // ignore walls
	if (body.nc < ncmin) continue; 
	bool imfound = false;
	for (int im = 0; im < ncoronas_; ++im) {
	  if ( UTIL::is_vector_inside(body.R, corona_leftbottom_[im], corona_righttop_[im]) ) {
	    zc_[im] += body.nc;
	    double vol = 4*M_PI*std::pow(body.rad, dim); if (3 == dim) vol /= 3.0;  
	    phi_[im] += vol;
	    ++counter_[im];
	    imfound = true;
	    break;
	  }
	}
	if (false == imfound) {
#if DEBUG_HOMOGEINITY_CORONAS
	  print_warning("Homogeinity :: Body not found inside any corona.");
	  print_log("R = ", body.R);
	  print_log("id = ", body.uid);
	  print_log("left   = ", particles[ngrains_ + LEFT  ].x());
	  print_log("right  = ", particles[ngrains_ + RIGHT ].x());
	  print_log("back   = ", particles[ngrains_ + BACK  ].y());
	  print_log("front  = ", particles[ngrains_ + FRONT ].y());
	  print_log("bottom = ", particles[ngrains_ + BOTTOM].z());
	  print_log("top    = ", particles[ngrains_ + TOP   ].z());
#endif // DEBUG_HOMOGEINITY_CORONAS
	} 
	//ASSERT(true == imfound);
      }
      for (int im = 0; im < ncoronas_; ++im) {
	if (counter_[im] > 0) {
	  zc_[im] /= counter_[im];
	  //phi_[im] /= V0_;
	  double corona_volume = UTIL::box_volume(corona_leftbottom_[im], corona_righttop_[im], dim);
	  if (im >= 1) corona_volume -= UTIL::box_volume(corona_leftbottom_[im-1], corona_righttop_[im-1], dim);
	  phi_[im] /= corona_volume;
	}
      }
      UTIL::print_done("Computing zc and phi per corona.");

      print_log("Deltafinal = ", corona_righttop_[ncoronas_-1][0] - corona_righttop_[ncoronas_-2][0]);
      std::fill(counter_.begin(), counter_.end(), 0);
      for (const auto & body : particles ) {
	if (body.uid >= ngrains_) continue; // ignore walls
	bool imfound = false;
	for (int im = 0; im < ncoronas_; ++im) {
	  if ( UTIL::is_vector_inside(body.R, corona_leftbottom_[im], corona_righttop_[im]) ) {
	    ++counter_[im];
	    imfound = true;
	    break;
	  }
	}
	if (false == imfound) {
	  print_log("R = ", body.R);
	  print_log("id = ", body.uid);
	  print_log("LBmin = ", corona_leftbottom_[ncoronas_-1]);
	  print_log("RTmax = ", corona_righttop_[ncoronas_-1]);
	  print_log("left   = ", particles[ngrains_ + LEFT  ].x());
	  print_log("right  = ", particles[ngrains_ + RIGHT ].x());
	  print_log("back   = ", particles[ngrains_ + BACK  ].y());
	  print_log("front  = ", particles[ngrains_ + FRONT ].y());
	  print_log("bottom = ", particles[ngrains_ + BOTTOM].z());
	  print_log("top    = ", particles[ngrains_ + TOP   ].z());
	  return EXIT_FAILURE;
	}	  
	//ASSERT(true == imfound);
      }

      
      _UNUSED(periodic[0]);

      // print data
      print_data();
      
      UTIL::print_done("Computing homogeinity");
      return EXIT_SUCCESS; 
    }

    int  Homogeinity::print_data() {
      p_out_.open("POSTPRO/homogeinity_p.dat");
      q_out_.open("POSTPRO/homogeinity_q.dat");
      zc_out_.open("POSTPRO/homogeinity_zc.dat");
      phi_out_.open("POSTPRO/homogeinity_phi.dat");
      counter_fout_.open("POSTPRO/homogeinity_counter.dat");
      for (int im  = 0; im < ncoronas_; ++im) {
	const double x = corona_righttop_[im][0]/d_;
	p_out_.print(x, p_[im]);
	q_out_.print(x, q_[im]);
	phi_out_.print(x, phi_[im]);
	zc_out_.print(x, zc_[im]);
	counter_fout_.print(x, counter_[im]);
      }
      p_out_.close();
      q_out_.close();
      zc_out_.close();
      phi_out_.close();
      counter_fout_.close();

      return EXIT_SUCCESS;
    }
    
    int Homogeinity::end_tasks() {
      print_data();
      return EXIT_SUCCESS; 
    }

  } // namespace POSTPRO
} // namespace DEM

