// Computes stress histograms, per grain

#include "postpro/stress_histograms_grains.hpp"

namespace DEM {
  namespace POSTPRO {
        
    int StressHistogram::init_tasks(const int & ngrains) {
      ngrains_ = ngrains;
      stress_tensors_.resize(ngrains_); 
      for (int ii = 0; ii < 3; ++ii) {
	for (int jj = 0; jj < 3; ++jj) { 
	  histos_[ii][jj].construct(-10.0, 10.0, 200); // min, max, bin
	}
      }
      histo_qoverp_.construct(-1.6, 1.6, 50);	     // min, max, bin 
      histo_forces_.construct(0.1, 6, 70);
      ready_ = true;						                            
      return 0;					                
    }
    
    int StressHistogram::end_tasks() {
      // print histograms
      std::string names_[3][3];                                     // To store histograms names
      names_[0][0] = "POSTPRO/stress_histo_xx.dat"; names_[0][1] = "POSTPRO/stress_histo_xy.dat"; names_[0][2] = "POSTPRO/stress_histo_xz.dat";  
      names_[1][0] = "POSTPRO/stress_histo_yx.dat"; names_[1][1] = "POSTPRO/stress_histo_yy.dat"; names_[1][2] = "POSTPRO/stress_histo_yz.dat";  
      names_[2][0] = "POSTPRO/stress_histo_zx.dat"; names_[2][1] = "POSTPRO/stress_histo_zy.dat"; names_[2][2] = "POSTPRO/stress_histo_zz.dat";  
      for (int ii = 0; ii < 3; ii++) {
	for (int jj = 0; jj < 3; jj++) {
	  histos_[ii][jj].print(names_[ii][jj], 1, 0, 1, 0, false, true); // name, mx, bx, my, by, fullinfo
	}
      }
      histo_qoverp_.print("POSTPRO/histo_qoverp_grains.dat");

      histo_forces_.print("POSTPRO/histo_forces_grains.dat");
      
      return EXIT_SUCCESS; 
    }

  } // namespace POSTPRO
} // namespace DEM

