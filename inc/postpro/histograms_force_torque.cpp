// Computes the orientation and magnitude histograms for forces and torques

#include "postpro/histograms_force_torque.hpp"

namespace DEM {
  namespace POSTPRO {
    
    int HistogramsForceTorque::init_tasks(const YAML::Node & config) 
    {
      histogram_[P_Fn].construct(config["P_Fn"][0].as<double>(), config["P_Fn"][1].as<double>(), config["P_Fn"][2].as<int>(), "P_Fn"); 
      hsnapshot_[P_Fn].construct(config["P_Fn"][0].as<double>(), config["P_Fn"][1].as<double>(), config["P_Fn"][2].as<int>(), "P_Fn_snapshot"); 
      histogram_[P_Ft].construct(config["P_Ft"][0].as<double>(), config["P_Ft"][1].as<double>(), config["P_Ft"][2].as<int>(), "P_Ft"); 
      hsnapshot_[P_Ft].construct(config["P_Ft"][0].as<double>(), config["P_Ft"][1].as<double>(), config["P_Ft"][2].as<int>(), "P_Ft_snapshot"); 
      histogram_[P_T].construct(config["P_T"][0].as<double>(), config["P_T"][1].as<double>(), config["P_T"][2].as<int>(), "P_T"); 
      hsnapshot_[P_T].construct(config["P_T"][0].as<double>(), config["P_T"][1].as<double>(), config["P_T"][2].as<int>(), "P_T_snapshot"); 
      histogram_[P_F_xy].construct(config["P_F_xy"][0].as<double>()*M_PI/180.0, config["P_F_xy"][1].as<double>()*M_PI/180.0, config["P_F_xy"][2].as<int>(), "P_F_xy"); 
      hsnapshot_[P_F_xy].construct(config["P_F_xy"][0].as<double>()*M_PI/180.0, config["P_F_xy"][1].as<double>()*M_PI/180.0, config["P_F_xy"][2].as<int>(), "P_F_xy_snapshot"); 
      histogram_[P_F_xz].construct(config["P_F_xz"][0].as<double>()*M_PI/180.0, config["P_F_xz"][1].as<double>()*M_PI/180.0, config["P_F_xz"][2].as<int>(), "P_F_xz"); 
      hsnapshot_[P_F_xz].construct(config["P_F_xz"][0].as<double>()*M_PI/180.0, config["P_F_xz"][1].as<double>()*M_PI/180.0, config["P_F_xz"][2].as<int>(), "P_F_xz_snapshot"); 
      histogram_[P_F_yz].construct(config["P_F_yz"][0].as<double>()*M_PI/180.0, config["P_F_yz"][1].as<double>()*M_PI/180.0, config["P_F_yz"][2].as<int>(), "P_F_yz"); 
      hsnapshot_[P_F_yz].construct(config["P_F_yz"][0].as<double>()*M_PI/180.0, config["P_F_yz"][1].as<double>()*M_PI/180.0, config["P_F_yz"][2].as<int>(), "P_F_yz_snapshot"); 
      histogram_[P_Fn_xy].construct(config["P_Fn_xy"][0].as<double>()*M_PI/180.0, config["P_Fn_xy"][1].as<double>()*M_PI/180.0, config["P_Fn_xy"][2].as<int>(), "P_Fn_xy"); 
      hsnapshot_[P_Fn_xy].construct(config["P_Fn_xy"][0].as<double>()*M_PI/180.0, config["P_Fn_xy"][1].as<double>()*M_PI/180.0, config["P_Fn_xy"][2].as<int>(), "P_Fn_xy_snapshot"); 
      histogram_[P_Fn_xz].construct(config["P_Fn_xz"][0].as<double>()*M_PI/180.0, config["P_Fn_xz"][1].as<double>()*M_PI/180.0, config["P_Fn_xz"][2].as<int>(), "P_Fn_xz"); 
      hsnapshot_[P_Fn_xz].construct(config["P_Fn_xz"][0].as<double>()*M_PI/180.0, config["P_Fn_xz"][1].as<double>()*M_PI/180.0, config["P_Fn_xz"][2].as<int>(), "P_Fn_xz_snapshot"); 
      histogram_[P_Fn_yz].construct(config["P_Fn_yz"][0].as<double>()*M_PI/180.0, config["P_Fn_yz"][1].as<double>()*M_PI/180.0, config["P_Fn_yz"][2].as<int>(), "P_Fn_yz"); 
      hsnapshot_[P_Fn_yz].construct(config["P_Fn_yz"][0].as<double>()*M_PI/180.0, config["P_Fn_yz"][1].as<double>()*M_PI/180.0, config["P_Fn_yz"][2].as<int>(), "P_Fn_yz_snapshot"); 
      histogram_[P_Ft_xy].construct(config["P_Ft_xy"][0].as<double>()*M_PI/180.0, config["P_Ft_xy"][1].as<double>()*M_PI/180.0, config["P_Ft_xy"][2].as<int>(), "P_Ft_xy"); 
      hsnapshot_[P_Ft_xy].construct(config["P_Ft_xy"][0].as<double>()*M_PI/180.0, config["P_Ft_xy"][1].as<double>()*M_PI/180.0, config["P_Ft_xy"][2].as<int>(), "P_Ft_xy_snapshot");
      histogram_[P_Ft_xz].construct(config["P_Ft_xz"][0].as<double>()*M_PI/180.0, config["P_Ft_xz"][1].as<double>()*M_PI/180.0, config["P_Ft_xz"][2].as<int>(), "P_Ft_xz"); 
      hsnapshot_[P_Ft_xz].construct(config["P_Ft_xz"][0].as<double>()*M_PI/180.0, config["P_Ft_xz"][1].as<double>()*M_PI/180.0, config["P_Ft_xz"][2].as<int>(), "P_Ft_xz_snapshot"); 
      histogram_[P_Ft_yz].construct(config["P_Ft_yz"][0].as<double>()*M_PI/180.0, config["P_Ft_yz"][1].as<double>()*M_PI/180.0, config["P_Ft_yz"][2].as<int>(), "P_Ft_yz"); 
      hsnapshot_[P_Ft_yz].construct(config["P_Ft_yz"][0].as<double>()*M_PI/180.0, config["P_Ft_yz"][1].as<double>()*M_PI/180.0, config["P_Ft_yz"][2].as<int>(), "P_Ft_yz_snapshot"); 
      
      // create output dirs
      int status;
      UTIL::print_start("Creating output dir for force and torque histograms");
      status = std::system("mkdir POSTPRO/histograms_F_T"); 
      if (0 != status) print_warning("Possible error creating directory POSTPRO/histograms_F_T");
      UTIL::print_done("Creating output dir for force and torque histograms");

      return EXIT_SUCCESS;
    }
    
   
    int HistogramsForceTorque::end_tasks() {
      // print final snapshosts
      print(hsnapshot_, "LAST");
      // Print histograms from all time
      print(histogram_, "");
      
      return EXIT_SUCCESS;
    }
    


  } // namespace POSTPRO
} // namespace DEM

