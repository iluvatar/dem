// Computes compactivity and related for filtered particles 

#include "postpro/voronoi-no_floating.hpp"

namespace DEM {
  namespace POSTPRO {
    
    int VoronoiNoFloating::init_tasks(const int & ngrains, const double & d, const int & dim) {
      const std::string msg = "Init tasks for VoronoiNoFloating"; UTIL::print_start(msg);
      ngrains_ = ngrains;
      d_ = d; d3_ = d_*d_*d_;
      Vmin_ = UTIL::voronoi_vmin(dim); // normalized by d_^D // WARNING: Actual value could be smaller because of interpenetrations 
      int status = std::system("mkdir POSTPRO/voronoi_new"); 
      if (0 != status) print_warning("Possible error creating directory POSTPRO/voronoi_new");
      
      // histograms
      histo_.construct(0.95*Vmin_, 2.8*Vmin_, 60, "voronoi_new");

      ready_ = true;
      UTIL::print_done(msg);
      return EXIT_SUCCESS;					                
    }
    

    int VoronoiNoFloating::print_data() {
      const std::string msg = "Printing voronoi for filtered particles."; UTIL::print_start(msg);
      
      histo_.print("POSTPRO/voronoi_new/histoVoroVol.dat"); 
      print_scaled_histo_multivoro(histo_, 1, "POSTPRO/voronoi_new/histoVoroVol_scaled.dat"); 
      histo_.print_mean_sigma("POSTPRO/voronoi_new/histoVoroVol_mean_sigma.dat"); 
      
      // set print file names and open streams
      std::stringstream name;
      name.str(""); name << "POSTPRO/voronoi_new/voronoi_X.dat"   ; X_fout_.open(name.str());
      name.str(""); name << "POSTPRO/voronoi_new/voronoi_K.dat"   ; K_fout_.open(name.str());
      name.str(""); name << "POSTPRO/voronoi_new/voronoi_KuN.dat" ; KuN_fout_.open(name.str());
      name.str(""); name << "POSTPRO/voronoi_new/voronoi_S.dat"   ; S_fout_.open(name.str());
      name.str(""); name << "POSTPRO/voronoi_new/voronoi_SuK.dat" ; SuK_fout_.open(name.str());
      name.str(""); name << "POSTPRO/voronoi_new/voronoi_CuN.dat" ; CuN_fout_.open(name.str());
      name.str(""); name << "POSTPRO/voronoi_new/ngrains.dat"     ; ngrains_fout_.open(name.str());

      // print stat mech vars
      X_fout_.print(X_);
      K_fout_.print(K_);
      KuN_fout_.print(KuN_);
      S_fout_.print(S_);
      SuK_fout_.print(SuK_);
      CuN_fout_.print(CuN_);
      ngrains_fout_.print(ngrains_);
      
      // close streams
      X_fout_.close();
      K_fout_.close();
      KuN_fout_.close();
      S_fout_.close();
      SuK_fout_.close();
      CuN_fout_.close();
      ngrains_fout_.close();

      UTIL::print_done(msg);
      return EXIT_SUCCESS;
    }
    
    int VoronoiNoFloating::end_tasks() {
      print_data();
      return EXIT_SUCCESS; 
    }
    // ---------------------------------------------------------------------
    // AUXILIARY FUNCTIONS
    // ---------------------------------------------------------------------

  void VoronoiNoFloating::print_scaled_histo_multivoro(Histogram1D & histo, const double & size, const std::string & filename) 
  {
    // compute voronoi parameters
    double Vmean, Vsigma; 
    histo.mean_and_sigma(Vmean, Vsigma);
    const double Vmin = size*Vmin_;
    double mx = 1.0, bx = 0.0, my = 1.0, by = 0.0;
    if (Vmean > Vmin) {
      mx = 1.0/(Vmean-Vmin), bx = -Vmin/(Vmean-Vmin), my = Vmean-Vmin, by = 0.0;
    }
    else {
      print_warning("voronoi multi: Vmean < Vmin");
      print_warning("size  = ", size);
      print_warning("Vmean = ", Vmean);
      print_warning("Vmin  = ", Vmin);
    }
    // print
    histo.print(filename.c_str(), mx, bx, my, by);
  }

    
    
  } // namespace POSTPRO
} // namespace DEM


