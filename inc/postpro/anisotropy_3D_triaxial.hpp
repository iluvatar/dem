#ifndef __ANISOTROPY_3D_TRIAXIAL__
#define __ANISOTROPY_3D_TRIAXIAL__

#include "matrix3d.hpp"
#include "vector3d.hpp"
#include "utility.hpp"
#include "file_util.hpp"
#include <array>
#include <cmath>
#include <algorithm>
#include <numeric>
#include <map>

/**
   Computes the anisotropies (contacts, branch, and force) in 3D. DEFINED FOR TRIAXIAL ONLY
*/
namespace DEM {
  namespace POSTPRO {

    class Anisotropy3DTriaxial {
      // data
    private: 

      // constants
      enum ANI {AC = 0, ALN, AFN, AFT, ATOTAL}; // Anisotropies to compute
      static const int nbins_ = 25; /// Angular partition for distibutions

      // typedefs
      typedef std::array<Mat3d_t, ANI::ATOTAL> MatArray_t; // Typedef for storing tensors
      typedef std::array<std::array<int,     nbins_>, ANI::ATOTAL> HistoArrayI_t; // to store counters
      typedef std::array<std::array<double,  nbins_>, ANI::ATOTAL> HistoArrayD_t; // to store counters 
      typedef std::array<std::array<Vec3d_t, nbins_>, ANI::ATOTAL> HistoArrayVec3d_t; // to store counters 
      
      MatArray_t X_;                                       //< Fabric tensors for computing contact anisotropies 
      HistoArrayVec3d_t fsum_histo_;			   //< Angular Histograms 
      HistoArrayD_t fsum_histo_D_;			   //< Angular Histograms 
      HistoArrayI_t nc_;                                   //< Number of contacts per orientation 
      double min_ = 0, max_ = 0, delta_angle_ = 0;	   //< Angular partition parameters 
      double avg[ANI::ATOTAL] = {0};
      
      std::array<UTIL::OFileStream, ANI::ATOTAL> a_fout_;          //< anisotropies ofstreams
      std::array<UTIL::OFileStream, ANI::ATOTAL> X_fout_;          //< X-tensors ofstreams
      UTIL::OFileStream qoverp_fout_;				   //< To print q/p  = 2/5*(ac + aln + afn + aft)

      // methods
    public :  
      Anisotropy3DTriaxial();
      ~Anisotropy3DTriaxial();
      void print(const double & time);
      void reset_fhistos(void); 
      void reset_Xtensors(void); 
      template <class ContactPtrList_t> 
      void operator()(const ContactPtrList_t & contacts_ptrs,  //< list of internal contacts references
		      const std::vector<Particle> & particle,
		      const double & EPS,
		      const double & time)
      {
	// reset
	reset_Xtensors();
	
	// compute histograms 
	const int nc = contacts_ptrs.size();
	if (nc > 0) {
	  for (const auto & cptr : contacts_ptrs) { // for each contact
	    const Vec3d_t N = cptr.get().N();
	    const Vec3d_t T = cptr.get().Ft().normalized();
	    const Vec3d_t Branch = particle[cptr.get().uid2()].R - particle[cptr.get().uid1()].R; 
	    const Vec3d_t Vec[ANI::ATOTAL] = {N, Branch, cptr.get().Fn(), cptr.get().Ft()};
	    
	    // projection planes properties
	    const int nplane = 15;
	    //static const std::array<Vec3d_t, nplane> Np = {Vec_uy, Vec_ux, (Vec_uy - Vec_ux)*M_SQRT1_2}; //< plane normal vector for each projection
	    //static const std::array<Vec3d_t, nplane> x  = {Vec_ux, Vec_uy, (Vec_ux + Vec_uy)*M_SQRT1_2}; //< horizontal vector for the given projection  
	    //static const std::array<Vec3d_t, nplane> y  = {Vec_uz, Vec_uz, Vec_uz}; //< vertical   vector for the given projection  

	    const double minphi = 0.0;
	    const double maxphi = 0.99*M_PI;
	    const double dphi = (maxphi - minphi)/nplane;

	    for (int plane = 0; plane < nplane; ++plane) {
	      const double phi = minphi + plane*dphi;
	      const Vec3d_t Np = Vec_ux*(-std::sin(phi)) + Vec_uy*(std::cos(phi));
	      const Vec3d_t x = Vec_ux*(std::cos(phi)) + Vec_uy*(std::sin(phi));
	      const Vec3d_t y = Vec_uz;
	      for (int ani = ANI::AC; ani < ANI::ATOTAL; ++ani) {
		//if (std::fabs(Np[plane].dot(Vec[ani].normalized())) < EPS) {
		if (std::fabs(Np.dot(Vec[ani].normalized())) < EPS) {
		  //double theta = std::atan2(Vec[ani].dot(y[plane]), Vec[ani].dot(x[plane])); if (theta < 0) theta += M_PI;
		  double theta = std::atan2(Vec[ani].dot(y), Vec[ani].dot(x)); if (theta < 0) theta += M_PI;
		  const int bin = (theta - min_)/delta_angle_; ASSERT(0 <= bin && bin < nbins_);
		  switch (ani) {
		  case ALN: case AFN: 
		    //fsum_histo_D_[ani][bin] += std::hypot(Vec[ani].dot(x[plane]), Vec[ani].dot(y[plane]));
		    fsum_histo_D_[ani][bin] += std::hypot(Vec[ani].dot(x), Vec[ani].dot(y));
		    break;
		  default:
		    //fsum_histo_[ani][bin] += Vec[ani] - Vec[ani].dot(Np[plane])*Np[plane];
		    fsum_histo_[ani][bin] += Vec[ani] - Vec[ani].dot(Np)*Np;
		}
		  nc_[ani][bin]++;
		  avg[ani] += Vec[ani].norm();
		}
	      }
	    }

	    // Add values to tensors
	    for (int ia = 0; ia <= 2; ++ia) {
	      for (int ib = 0; ib <= 2; ++ib) {
		X_[ANI::AC](ia, ib) += N[ia]*N[ib];  
		X_[ANI::ALN](ia, ib) += N.dot(Branch)*N[ia]*N[ib];  
		X_[ANI::AFN](ia, ib) += N.dot(cptr.get().Fn())*N[ia]*N[ib];  
		X_[ANI::AFT](ia, ib) += T.dot(cptr.get().Ft())*N[ia]*T[ib];  
	      }
	    }
	  }

	  for (auto & X : X_) {
	    X /= nc;
	  }
	  for (int ani = ANI::AC; ani < ANI::ATOTAL; ++ani) {
	    switch (ani) {
	    case ANI::AC:
	      break;
	    case ANI::AFT:
	      X_[ANI::AFT] /= avg[ANI::AFN]/nc;
	      break;
	    default:
	      X_[ani] /= avg[ani]/nc;
	    }
	  }
	} // END: nc > 0	  

	print(time); // anisotropies are computed and printed here
      }

    }; // class Anisotropy3dTriaxial 

  } // namespace POSTPRO
} // namespace DEM

#endif // __ANISOTROPY_3D_TRIAXIA__
