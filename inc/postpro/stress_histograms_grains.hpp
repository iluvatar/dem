// Computes stress histograms, per grain

#ifndef __STRESS_HISTOGRAMS__
#define __STRESS_HISTOGRAMS__

#include "postpro_base_functor.hpp"
#include "../matrix3d.hpp"
#include "../stress_tensor.hpp"
#include "../histogram1d.hpp"
#include "../utility.hpp"
#include <vector>
#include <string>
#include <algorithm>

namespace DEM {
  namespace POSTPRO {
    
    class StressHistogram : public Base_functor {
    public :
      StressHistogram() {};
      ~StressHistogram() { /*end_tasks();*/ }
      int init_tasks(const int & ngrains) ;
      int end_tasks() ;
      template <class ContactList_t, class ParticleList_t, class IndexList_t> 
      int 
      operator()(const ContactList_t & contacts,		   // List of contacts
		 const ParticleList_t & particles,		   // list of particles
		 const int & dim,				   // dimension of simulation (2 or 3)
		 const int & ncmin,				   // minimum contact number per valid particle
		 const IndexList_t & indexes,			   // Indexes to accept (filtered)
		 const std::vector<double> & voronoi_volumes)	   // Voronoi volumes for the system
      {
	UTIL::print_start("Stress Histograms per Grain");
	ASSERT(true == ready_);
	ASSERT(2 == dim || 3 == dim);
	ASSERT(0 <= ncmin);
      
	// compute stress tensors
	global_stress_tensor_.setZero();
	for (auto & mat : stress_tensors_) 
	  mat.setZero();						   
	for (const auto & contact : contacts) {
	  const auto id1 = contact.uid1_;
	  const auto id2 = contact.uid2_;
	  const Vec3d_t Branch = contact.Normal_*(particles[id1].rad + particles[id2].rad - contact.delta_);
	  const Mat3d_t Val = contact.F()*Branch.transpose();
	  bool add_to_global = false;
	  if ( (indexes.end() != indexes.find(id1)) && (particles[id1].nc >= ncmin) ) { stress_tensors_[id1] += Val; add_to_global = true;}
	  if ( (indexes.end() != indexes.find(id2)) && (particles[id2].nc >= ncmin) ) { stress_tensors_[id2] += Val; add_to_global = true;}
	  if (add_to_global) global_stress_tensor_ += Val;
	}
	// NORMALIZE STRESS : WARNING: Which volume?
	// normalize by sphere volume of each grain
	//const double factor = (dim == 2) ? (4.0*M_PI) : (4.0*M_PI/3.0);
	//for (int id = 0; id < ngrains_; ++id) {
	//stress_tensors_[id] /= factor*std::pow(particles[id].rad, dim); 
	//}
	// normalize by volume of voronoi cell
	for (int id = 0; id < ngrains_; ++id) {
	  stress_tensors_[id] /= voronoi_volumes[id]; 
	}
	/*
	  double total_volume = 0.0;
	  for (auto & id : indexes) {
	  total_volume += voronoi_volumes[id];
	  }//*/
      
	// Histograms      
	// global p and q
	double qglobal = 0, pglobal = 0;
	get_p_q(global_stress_tensor_, pglobal, qglobal);
	//if (contacts.size() > 0 && !global_stress_tensor_.isZero()) ASSERT(pglobal != 0);
	if (pglobal > 0) {
	  // accumulate stress on histograms
	  for (auto & id : indexes) {
	    double p = 0, q = 0;
	    if (particles[id].nc < ncmin || particles[id].nc == 0) continue;			   // ignore particles with small nc
	    get_p_q(stress_tensors_[id], q, p);
	    if (p == 0) continue;
	    for (int ii = 0; ii < 3; ++ii) {
	      for (int jj = 0; jj < 3; ++jj) {
		if (pglobal > 0) histos_[ii][jj].increment(stress_tensors_[id](ii, jj)/pglobal);
	      }
	    }
	    if (p != 0) histo_qoverp_.increment(q/p);
	  }
	}
	UTIL::print_done("Accumulate stress on histograms");
	UTIL::print_done("Accumulate q/p");

	/////////////////////////////////////////////////////////////////
	UTIL::print_start("Accumulate forces on histograms per grain");
	histo_forces_.reset();
	const int NNEIGHBORS = 12;
	std::vector<double> normal_forces_per_grain(ngrains_*NNEIGHBORS); // WARNING: Up to 12 neighbors per grain. With granulometry could be more.
	for(auto & val : normal_forces_per_grain) val = 0;
	
	// compute mean force
	std::vector<double> mean_force (ngrains_, 0);
	std::vector<int> count(ngrains_, 0);
	for (const auto & contact : contacts) {
	  const auto id1 = contact.uid1_;
	  const auto id2 = contact.uid2_;
	  const double fnorm = contact.Fn().norm();
	  //if ( (indexes.end() != indexes.find(id1)) && (particles[id1].nc >= ncmin) ) {
	  if ( (indexes.end() != indexes.find(id1)) ) {
	    mean_force[id1] += fnorm; ++count[id1];
	    for(int jj = 0; jj < NNEIGHBORS; ++jj) {
	      if (0 == normal_forces_per_grain[id1*NNEIGHBORS + jj]) {
		normal_forces_per_grain[id1*NNEIGHBORS + jj] = fnorm;
		break;
	      }
	    }
	  }
	  //if ( (indexes.end() != indexes.find(id2)) && (particles[id2].nc >= ncmin) ) {
	  if ( (indexes.end() != indexes.find(id2))) {
	    mean_force[id2] += fnorm; ++count[id2];
	    for(int jj = 0; jj < NNEIGHBORS; ++jj) {
	      if (0 == normal_forces_per_grain[id2*NNEIGHBORS + jj]) {
		normal_forces_per_grain[id2*NNEIGHBORS + jj] = fnorm;
		break;
	      }
	    }
	  }
	}
	double sum = 0; int cnt = 0;
	for(int id =0; id<ngrains_; ++id) {
	  if(count[id] > 0) {
	    sum += mean_force[id];
	    cnt++;
	  }
	}
	for(int id =0; id<ngrains_; ++id) {
	  mean_force[id] /= sum/cnt ;
	}
	// accumulate on histogram
	histo_forces_.increment(mean_force);
	// print
	histo_forces_.print("POSTPRO/histo_forces_grains.dat");
	std::ofstream fout("POSTPRO/normal_forces_per_grain.dat");
	fout.precision(16); fout.setf(std::ios::scientific);
	for (int id = 0; id < ngrains_; ++id){
	  fout << id << "    ";
	  for (int jd = 0; jd < NNEIGHBORS; ++jd){
	    if (0 != normal_forces_per_grain[id*NNEIGHBORS + jd])
	      fout << normal_forces_per_grain[id*NNEIGHBORS + jd] << "   ";
	    else break;
	  }
	  fout << "\n";
	}
	fout.close();
	UTIL::print_done("Accumulate forces on histograms per grain");	
	UTIL::print_done("Stress Histograms per Grain");
	return EXIT_SUCCESS; 
      }
    private :
      Mat3d_t global_stress_tensor_;				    // global stress tensor, to normalize with it
      std::vector<Mat3d_t> stress_tensors_;                         // vector to store the stress tensors
      Histogram1D histos_[3][3];                                    // histograms per direction
      Histogram1D histo_qoverp_;	                            // histogram for q/p
      Histogram1D histo_forces_;                                      
      int ngrains_ = 0;                                             // number of grains
      bool ready_ = false;                                          // flag to assert system ready
    };    

  } // namespace POSTPRO
} // namespace DEM

#endif // __STRESS_HISTOGRAMS__
