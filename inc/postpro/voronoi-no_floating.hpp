// Computes voronoi compactivity and related taking into account only non-floating particles

#ifndef __VORONOI_NO_FLOATING__
#define __VORONOI_NO_FLOATING__

#include "postpro_base_functor.hpp"
#include "../file_util.hpp"
#include "../utility.hpp"
#include "../histogram1d.hpp"
#include "../functions.hpp"
#include "../particle.hpp"
#include "voro++/voro++.hh"
#include <vector>
#include <array>
#include <algorithm>
#include <string>
#include <sstream>
#include <fstream>

namespace DEM {
  namespace POSTPRO {
    // Auxiliary class to create dodecahedron base cells for irregular packings
    // Golden ratio constants
    const double Phi=0.5*(1+sqrt(5.0));
    const double phi=0.5*(1-sqrt(5.0));
    // Create a wall class that, whenever called, will replace the Voronoi cell
    // with a prescribed shape, in this case a dodecahedron
    class wall_initial_shape : public voro::wall {
    public: 
      wall_initial_shape() {
	// Create a dodecahedron
	//v.init(-2,2,-2,2,-2,2);
	//v.plane(0,Phi,1);v.plane(0,-Phi,1);v.plane(0,Phi,-1);
	//v.plane(0,-Phi,-1);v.plane(1,0,Phi);v.plane(-1,0,Phi);
	//v.plane(1,0,-Phi);v.plane(-1,0,-Phi);v.plane(Phi,1,0);
	//v.plane(-Phi,1,0);v.plane(Phi,-1,0);v.plane(-Phi,-1,0);
	v.init(-2*scale_,2*scale_,-2*scale_,2*scale_,-2*scale_,2*scale_);
	v.plane(0,Phi*scale_,1*scale_);v.plane(0,-Phi*scale_,1*scale_);v.plane(0,Phi*scale_,-1*scale_);
	v.plane(0,-Phi*scale_,-1*scale_);v.plane(1*scale_,0,Phi*scale_);v.plane(-1*scale_,0,Phi*scale_);
	v.plane(1*scale_,0,-Phi*scale_);v.plane(-1*scale_,0,-Phi*scale_);v.plane(Phi*scale_,1*scale_,0);
	v.plane(-Phi*scale_,1*scale_,0);v.plane(Phi*scale_,-1*scale_,0);v.plane(-Phi*scale_,-1*scale_,0);
      };
      bool point_inside(double x,double y,double z) {_UNUSED(x); _UNUSED(y); _UNUSED(z); return true;}
      bool cut_cell(voro::voronoicell &c,double x,double y,double z) {
	_UNUSED(x); _UNUSED(y); _UNUSED(z); 
	// Set the cell to be equal to the dodecahedron
	c=v;
	return true;
      }
      bool cut_cell(voro::voronoicell_neighbor &c,double x,double y,double z) {
	_UNUSED(x); _UNUSED(y); _UNUSED(z); 
	
	// Set the cell to be equal to the dodecahedron
	c=v;
	return true;
      }
    private :
      voro::voronoicell v;
      static constexpr double scale_ = 0.0012; 
    };
    
    
    class VoronoiNoFloating : public Base_functor {
  public : // methods
    VoronoiNoFloating() {};
    ~VoronoiNoFloating() { print_log("VoronoiNoFloating destructor called"); /*end_tasks();*/ }
    int init_tasks(const int & ngrains, const double & d, const int & dim) ;
    int end_tasks();
    int print_data();
    template <class IndexList_t>	   
    int operator()(const IndexList_t & filtered_ids, 
		   const std::vector<Particle> & particles,  
		   const double walls_min[3],	       // input: minimum walls limits
		   const double walls_max[3],	       // input: maximum walls limits
		   const bool periodic[3],		       // input: periodicity flags on each direction
		   const int & dim)	         	       // input: number of dimensions (2 or 3)
      {
	const std::string msg = "Computing voronoi variables for only selected sub-system (filtered grain id's)"; UTIL::print_start(msg);
	ASSERT(true == ready_);
	ASSERT(2 == dim || 3 == dim);
	
	// accumulate voronoi volumes
	ngrains_ = 0.0;
	// TODO RECOMPUTE VORONOI VOLUMES USING ONLY FILTERD IDS
	voro::container_poly conp(walls_min[0], walls_max[0],
				  walls_min[1], walls_max[1],
				  walls_min[2], walls_max[2],
				  20, 20 , 20,
				  periodic[0], periodic[1], periodic[2], 8); 
	UTIL::print_done("\tcontainer declaration.");
	conp.clear(); 

	// Create the "initial shape" wall class and add it to the container
	wall_initial_shape(wis);
	conp.add_wall(wis); // UNCOMMENT????
	UTIL::print_done("\tadding auxiliary wall for dodecahedron initial shapes: irregular packings.");
	
	// fill the container with the selected particles
	const int npoints = filtered_ids.size();
	// container filling
	int idx = 0;
	for(const auto & id : filtered_ids) {
	  conp.put(idx, particles[id].R[0], particles[id].R[1], particles[id].R[2], particles[id].rad);
	  idx++;
	}
	UTIL::print_done("\tcontainer filling.");

	// Setup memory and arrays
	std::vector<double> vorovol(npoints, 0.0);
	//std::vector<std::vector<int> >  neigh; neigh.resize(npoints);
	//for (auto & n : neigh) { n.clear(); n.shrink_to_fit(); }

	// process voronoi cells
	voro::voronoicell_neighbor c; // cell which can store neighbor information
	voro::c_loop_all cla(conp);
	if(cla.start()) do if(conp.compute_cell(c, cla)) {
	      auto id = cla.pid();
	      //c.neighbors(neigh[id]); // write # of neighbors
	      vorovol[id] = c.volume();
	      //c.centroid(centroids[id][0], centroids[id][1], centroids[id][2]);
	    } while (cla.inc());

	// Normalize volume for 2D
	if (2 == dim) { 
	  const double ly = walls_max[1] - walls_min[1];
	  for(auto & vol : vorovol) vol /= ly;
	}
	for(auto & vol : vorovol) vol /= d3_;
	UTIL::print_done("\tvoronoi cells processing.");


	// add volumes to histogram
	for(const auto & vol : vorovol) {
	  histo_.increment(vol); 
	}
	UTIL::print_done("\tAccumulating volumes on histograms");
      
	// compute stat mech parameters 
	const double lambda_cube = M_E/100.0;; // = e (pi d^3/6 phiRCP - Vmin)/kRCP
	X_ = K_ = KuN_ = S_ = CuN_ = SuK_ = 0;
	double Vmean = 0, Vsigma = 0;
	histo_.mean_and_sigma(Vmean, Vsigma);
	if (Vsigma > 0 && (Vmean > Vmin_)) {
	  X_ = UTIL::statmech_X(Vmean, Vsigma, Vmin_);
	  K_ = UTIL::statmech_K(Vmean, Vsigma, Vmin_);
	  KuN_ = K_; 
	  S_ = UTIL::statmech_S(Vmean, K_, Vmin_, lambda_cube); 
	  SuK_ = (K_ > 0) ? S_/K_ : 0.0; 
	  CuN_ = K_*ngrains_/ngrains_;  // WARNING: How much is this ??????
	}
	UTIL::print_done("\tComputing voronoi stat mech with filtered ids ");
	
	// print
	print_data();
      
	UTIL::print_done(msg);
	return EXIT_SUCCESS; 
      }

    private : // methods
      void print_scaled_histo_multivoro(Histogram1D & histo, const double & size, const std::string & filename);
      
    private : // data
      Histogram1D histo_; 
      double X_, K_, KuN_, S_, CuN_, SuK_;  //< Stat mech values
      
      int ngrains_ = 0;						          //< Number of grains
      double d_ = 0, d3_ = 0;					          //< diammeter, d^3
      double Vmin_ = 1.0e100;					          //< Minimum voronoi volume
      bool ready_ = false;                                                //< flag to assert system ready
      UTIL::OFileStream X_fout_, K_fout_, KuN_fout_, SuK_fout_,
	S_fout_, CuN_fout_, ngrains_fout_; 
    };

    
  } // namespace POSTPRO
} // namespace DEM

#endif // __VORONOI_NO_FLOATING__

