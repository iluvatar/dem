// Computes stat mech vars for a voronoi based on the contact points

#ifndef __CONTACTS_VORONOI__
#define __CONTACTS_VORONOI__

#include "postpro_base_functor.hpp"
#include "../file_util.hpp"
#include "../utility.hpp"
#include "../histogram1d.hpp"
#include <vector>
#include <algorithm>
#include <string>
#include <sstream>

namespace DEM {
  namespace POSTPRO {
    
    class ContactsVoronoi : public Base_functor {
    public :
      ContactsVoronoi() {};
      ~ContactsVoronoi() { print_log("Contacts Voronoi destructor called"); /*end_tasks();*/ }
      int init_tasks(const double & d, const int & dim) ;
      int end_tasks() ;
      int print_data();
      template <class PointList_t> 
      int 
      operator()(const PointList_t & cpoints, 
		 const double wmin[3], const double wmax[3], 
		 const bool periodic[3], const int & dim)
      {
	UTIL::print_start("Computing statmech voronoi parameters for Contacts Voronoi Tessellation");
	ASSERT(true == ready_);
	ASSERT(2 == dim || 3 == dim);
      
	// memory arrays
	const int ncpoints = cpoints.size();
	std::vector<double> voronoi_volumes(ncpoints); 
	std::vector<Vec3d_t> points(ncpoints); 
	std::vector<double> weights(ncpoints); 
	std::vector<Vec3d_t> centroids(ncpoints);
	std::vector<std::vector<int> > neigh(ncpoints);
	// fill points information
	for(int idx = 0; idx < ncpoints; ++idx) {
	  points[idx]  = cpoints[idx];
	  weights[idx] = d_/2.0; 
	}
     
	// tessellate 
	UTIL::compute_voronoi_volumes(points, weights, wmin, wmax, periodic, dim, 
				      voronoi_volumes, centroids, neigh, false);

	// Normalize volume 
	for(auto & vol : voronoi_volumes) vol /= d3_; // ALWAYS Normalize
            
	// accumulate voronoi volumes 
	//double Vtotal = 0;
	for (const auto & vol : voronoi_volumes) {
	  histo_vorovol_.increment(vol); 
	  //Vtotal += vol;
	}
	UTIL::print_done("Accumulating volumes on histograms.");
      
	// compute stat mech parameters 
	const double lambda_cube = M_E/100.0;; // = e (pi d^3/6 phiRCP - Vmin)/kRCP
	voro_X_ = voro_K_ = voro_S_ = voro_CuN_ = 0;
	double Vmean = 0, Vsigma = 0;
	histo_vorovol_.mean_and_sigma(Vmean, Vsigma);
	if (Vsigma > 0 && (Vmean > Vmin_)) {
	  voro_X_ = UTIL::statmech_X(Vmean, Vsigma, Vmin_);
	  voro_K_ = UTIL::statmech_K(Vmean, Vsigma, Vmin_);
	  voro_S_ = UTIL::statmech_S(Vmean, voro_K_, Vmin_, lambda_cube); 
	  //if (filtered_ids.size() > 0) voro_CuN_ = voro_K_*Vtotal/(Vmean*filtered_ids.size()); 
	  print_log("Vmin   = ", Vmin_);
	  print_log("Vmean  = ", Vmean);
	  print_log("Vsigma = ", Vsigma);
	  print_log("X      = ", voro_X_);
	}
	UTIL::print_done("Computing stat mech vars for Contacts Voronoi");

	// print
	print_data();
      
	UTIL::print_done("Computing stat mech vars for Contacts Voronoi");
	return EXIT_SUCCESS; 
      }
      
    private :
      Histogram1D histo_vorovol_;			           // Histograms of voronoi volumes 
      double voro_X_, voro_K_, voro_S_, voro_CuN_;                 // Stat mech values
      double d_ = 0, d3_ = 0;					   // diammeter, d^3
      double Vmin_ = 1.0e100;					   // Minimum voronoi volume
      bool ready_ = false;                                         // flag to assert system ready
      UTIL::OFileStream voro_X_fout_, voro_K_fout_, 
	voro_S_fout_, voro_CuN_fout_; 
    };


  } // namespace POSTPRO
} // namespace DEM

#endif // __CONTACTS_VORONOI__
