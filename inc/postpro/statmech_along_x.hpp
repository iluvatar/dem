// Computes stat mech vars along xaxis inside sample

#ifndef __STATMECH_ALONG_X_AXIS__
#define __STATMECH_ALONG_X_AXIS__

#include "postpro_base_functor.hpp"
#include "../file_util.hpp"
#include "../utility.hpp"
#include "../histogram1d.hpp"
#include "../particle.hpp"
#include <vector>
#include <algorithm>
#include <string>
#include <sstream>
#include <map>

namespace DEM {
  namespace POSTPRO {
    
    class StatMechAlongXAxis : public Base_functor {
    public :
      StatMechAlongXAxis() {};
      ~StatMechAlongXAxis() { print_log("StatMechAlongXAxis destructor called"); /*end_tasks();*/ }
      int 
      init_tasks(const std::vector<Particle> & particles, const int & ngrains, const double & dmin, const double & dmax, const int & dim) ;
      int end_tasks() ;
      int print_data();
      template <class IndexList_t> 
      int 
      operator()(const std::vector<Particle> & particles,		   // list of particles
		 const int & dim,				   // dimension of simulation (2 or 3)
		 const int & ncmin,				   // minimum number of contacts
		 const IndexList_t & filtered_ids,		   // filtered ids (for example, inside volume)
		 const std::vector<double> & voronoi_volumes)	   // voronoi volumes 
      {
	const std::string msg = "Computing statmech voronoi parameters along x axis"; UTIL::print_start(msg);
	ASSERT(true == ready_);
	ASSERT(2 == dim || 3 == dim);
	ASSERT(ncmin >= 0);
      
	print_warning("Using all internal particles, regardless their contact number per particle");

	// accumulate voronoi volumes per bin
	std::fill(VT_.begin(), VT_.end(), 0.0);
	std::fill(ngrains_bin_.begin(), ngrains_bin_.end(), 0.0);
	std::cout << "Number of filtered ids = " << filtered_ids.size() << std::endl;
	// for (auto & id : filtered_ids) { // only selected particles : could affect the number of samples at the border
	for (int id = 0; id < ngrains_; ++id) { // all particles
	  // if (particles[id].nc < ncmin) continue; // use only internal with enough contacts
	  const int bin = int((particles[id].R[0] - xmin_)/dx_); 
	  //ASSERT(0 <= bin && bin < nbins_);
	  if (bin < 0 || nbins_ <= bin) continue; // outside of box, probably reference state is smaller than current
	  histos_vorovol_[bin].increment(voronoi_volumes[id]); 
	  ++ngrains_bin_[bin];
	  VT_[bin] += voronoi_volumes[id];
	}
	UTIL::print_done("Accumulating volumes on histograms");
      
	// compute stat mech parameters per bin
	const double lambda_cube = M_E/100.0;; // = e (pi d^3/6 phiRCP - Vmin)/kRCP
	for (int ii = 0; ii < nbins_; ++ii) {
	  X_[ii] = K_[ii] = S_[ii] = CuN_[ii] = SuK_[ii] = 0;
	  double Vmean = 0, Vsigma = 0;
	  histos_vorovol_[ii].mean_and_sigma(Vmean, Vsigma);
	  if (Vsigma > 0 && (Vmean > Vmin_)) {
	    X_[ii] = UTIL::statmech_X(Vmean, Vsigma, Vmin_);
	    K_[ii] = UTIL::statmech_K(Vmean, Vsigma, Vmin_);
	    S_[ii] = UTIL::statmech_S(Vmean, K_[ii], Vmin_, lambda_cube); 
	    SuK_[ii] = (K_[ii] > 0) ? S_[ii]/K_[ii] : 0.0; 
	    CuN_[ii] = K_[ii]*ngrains_bin_[ii]/ngrains_; 
#ifdef DEBUG
	    print_log("ii   = ", ii);
	    print_log("Vmin   = ", Vmin_);
	    print_log("Vmean  = ", Vmean);
	    print_log("Vsigma = ", Vsigma);
	    print_log("X      = ", X_[ii]);
#endif
	  }
	}
	UTIL::print_done("Computing stat mech vars per bin");

	// print
	print_data();
      
	UTIL::print_done(msg);
	return EXIT_SUCCESS; 
      }

    private :
      std::vector<Histogram1D> histos_vorovol_;			   // Histograms of voronoi volumes per bin
      std::vector<double> X_, K_, S_, CuN_, SuK_, VT_,             // Stat mech values per bin
	ngrains_bin_;
      int nbins_ = 0;						   // Number of bins
      int ngrains_ = 0;						   // Number of grains
      double d_ = 0, d3_ = 0;					   // diammeter, d^3
      double xmin_ = 0, xmax_ = 0, dx_ = 0;			   // x partition
      double Vmin_ = 0.0, Vmax_ = 0.0;		       		   // Minimum voronoi volume
      bool ready_ = false;                                         // flag to assert system ready
      UTIL::OFileStream X_fout_, K_fout_, SuK_fout_,
	S_fout_, CuN_fout_, VT_fout_, ngrains_bin_fout_; 
    };


  } // namespace POSTPRO
} // namespace DEM

#endif // __STATMECH_ALONG_X_AXIS__
