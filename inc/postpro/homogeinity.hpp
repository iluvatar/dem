// Computes homogeinity (on p, q, z, packfrac) per corona

#ifndef __HOMOGEINITY__
#define __HOMOGEINITY__

#include "postpro_base_functor.hpp"
#include "../vector3d.hpp"
#include "../matrix3d.hpp"
#include "../stress_tensor.hpp"
#include "../utility.hpp"
#include "../file_util.hpp"
#include "../particle.hpp"
#include "../contact.hpp"
#include <vector>
#include <string>

namespace DEM {
  namespace POSTPRO {
    
    class Homogeinity : public Base_functor {
    public :
      Homogeinity() { }
      ~Homogeinity() { /*end_tasks();*/ }
      int init_tasks(const std::vector<Particle> & particles, const int & ngrains, const double & d, const int & dim) ;
      int operator()(const std::vector<Contact> & contacts,		   // List of contacts
		     const std::vector<Particle> & particles,		   // list of particles
		     const bool periodic[3],			   // periodic flags, for tessellation
		     const int & dim,				   // dimension of simulation (2 or 3)
		     const int & ncmin);			   // minimum number of contacts
      int end_tasks() ;
      int print_data();
    private :
      Mat3d_t full_stress_tensor_;				    // Global stress tensor
      std::vector<Mat3d_t> stress_tensors_;                         // vector to store the stress tensors per corona
      int ngrains_ = 0;		                                    // Number of grains 
      bool ready_ = false;                                          // flag to assert system ready
      int ncoronas_ = 0;					    // number of coronas 
      double d_ = 0;						    // Minimum diammeter
      double Vtot_ = 0;						    // Total maximum volume
      double V0_ = 0;						    // Corona volume
      std::vector<Vec3d_t> corona_righttop_;			    // Corona right-top limit
      std::vector<Vec3d_t> corona_leftbottom_;			    // Corona left-bottom limit
      std::vector<double> p_;					    // p per corona (p/pglobal) 
      std::vector<double> q_;					    // q per corona (q/pglobal)
      std::vector<double> zc_;					    // Mean coordination per corona
      std::vector<double> phi_;					    // Packing fraction per corona
      std::vector<int> counter_;				    // Counter of particles per corona
      UTIL::OFileStream p_out_, q_out_, zc_out_, phi_out_, counter_fout_;
    };
  } // namespace POSTPRO
} // namespace DEM

#endif // __HOMOGEINITY__
