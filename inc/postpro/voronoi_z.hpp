// Computes compactivity and related for particles with 0, 1, 2, 3, 4,
// 5, ... contacts

#ifndef __VORONOI_Z__
#define __VORONOI_Z__

#include "postpro_base_functor.hpp"
#include "../file_util.hpp"
#include "../utility.hpp"
#include "../histogram1d.hpp"
#include "../functions.hpp"
#include "../particle.hpp"
#include <vector>
#include <array>
#include <algorithm>
#include <string>
#include <sstream>

namespace DEM {
  namespace POSTPRO {
    
    class VoronoiZ : public Base_functor {
    public : // methods
      VoronoiZ() {};
      ~VoronoiZ() { print_log("VoronoiZ destructor called"); /*end_tasks();*/ }
      int init_tasks(const int & ngrains, const double & d, const int & dim) ;
      int end_tasks() ;
      int print_data();
      template <class IndexList_t>	   
      int operator()(const int & dim, const IndexList_t & filtered_ids, const std::vector<Particle> & particles,  
		     const std::vector<double> & voronoi_volumes)
      {
	const std::string msg = "Computing voronoi variables for each z"; UTIL::print_start(msg);
	ASSERT(true == ready_);
	ASSERT(2 == dim || 3 == dim);
      
	// accumulate voronoi volumes
	std::fill(ngrains_z_.begin(), ngrains_z_.end(), 0.0);
	for(const auto & id : filtered_ids) {
#if DEBUG 
	  ASSERT(0 <= id && id < particles.size());
#endif
	  const int z = particles[id].nc;
	  if (0 <= z && z <= NZ_) {
	    histo_[z].increment(voronoi_volumes[id]); 
	    ++ngrains_z_[z];
	  }
	}
	UTIL::print_done("Accumulating volumes on histograms per z");
      
	// compute stat mech parameters per group
	const double lambda_cube = M_E/100.0;; // = e (pi d^3/6 phiRCP - Vmin)/kRCP
	for (int ii = 0; ii < NZ_; ++ii) {
	  X_[ii] = K_[ii] = KuN_[ii] = S_[ii] = CuN_[ii] = SuK_[ii] = 0;
	  double Vmean = 0, Vsigma = 0;
	  histo_[ii].mean_and_sigma(Vmean, Vsigma);
	  if (Vsigma > 0 && (Vmean > Vmin_)) {
	    X_[ii] = UTIL::statmech_X(Vmean, Vsigma, Vmin_);
	    K_[ii] = UTIL::statmech_K(Vmean, Vsigma, Vmin_);
	    KuN_[ii] = K_[ii]; 
	    S_[ii] = UTIL::statmech_S(Vmean, K_[ii], Vmin_, lambda_cube); 
	    SuK_[ii] = (K_[ii] > 0) ? S_[ii]/K_[ii] : 0.0; 
	    CuN_[ii] = 0.0; // K_[ii]*ngrains_bin_[ii]/ngrains_;  // WARNING: How much is this ??????
	  }
	}
	UTIL::print_done("Computing voronoi stat mech per z");

	// print
	print_data();
      
	UTIL::print_done(msg);
	return EXIT_SUCCESS; 
      }

    private : // methods
      void print_scaled_histo_multivoro(Histogram1D & histo, const double & size, const std::string & filename);
      
    private : // data
      static const int NZ_ = 13;					  //< Number of coordination numbers
      std::array<Histogram1D, NZ_> histo_; 
      std::array<double, NZ_> X_, K_, KuN_, S_, CuN_, SuK_, ngrains_z_;  //< Stat mech values per z
      
      int ngrains_ = 0;						          //< Number of grains
      double d_ = 0, d3_ = 0;					          //< diammeter, d^3
      double Vmin_ = 1.0e100;					          //< Minimum voronoi volume
      bool ready_ = false;                                                //< flag to assert system ready
      UTIL::OFileStream X_fout_, K_fout_, KuN_fout_, SuK_fout_,
	S_fout_, CuN_fout_, ngrains_z_fout_; 
    };

    
  } // namespace POSTPRO
} // namespace DEM

#endif // __VORONOI_Z__

