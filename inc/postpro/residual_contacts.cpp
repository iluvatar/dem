/**
   Computes and prints the amount of initial and residual contacts
**/

#include "postpro/residual_contacts.hpp"

namespace DEM {
  namespace POSTPRO {
        
    int ResidualContacts::init_tasks() {
      fout_residual_.open("POSTPRO/time-residual_contacts_fraction.dat");
      fout_residual_.print("# time \t nresidual/ncontacts \t nresidual");
      fout_initial_.open("POSTPRO/time-initial_contacts_fraction.dat");
      fout_residual_.print("# time \t ninitial/ncontacts \t ninitial");
      ready_ = true;
      return EXIT_SUCCESS; 
    }
    
    int ResidualContacts::operator()(const double & time, const std::vector<Contact> & contacts) {
      UTIL::print_start("ResidualContacts");
      ASSERT(true == ready_);

      // init vals
      const int ncontacts = contacts.size();
      int nresidual = 0, ninitial = 0;
      
      // count 
      if (ncontacts >= 1) {
	for (const auto & c : contacts) {
	  nresidual += c.residual_;
	}
	ninitial = ncontacts - nresidual;
      } // contacts.size >= 1      

      // print 
      double frac = (ncontacts >= 1) ? nresidual/double(ncontacts) : 0; 
      fout_residual_.print(time, frac, nresidual);
      frac = (ncontacts >= 1) ? ninitial/double(ncontacts) : 0; 
      fout_initial_.print(time, frac, ninitial);
      print_log("     Done :: printing");      
            
      // end
      UTIL::print_done("ResidualContacts");
      return EXIT_SUCCESS; 
    }

    int ResidualContacts::end_tasks() {
      fout_residual_.close();
      fout_initial_.close();
      return EXIT_SUCCESS; 
    }

  } // namespace POSTPRO
} // namespace DEM

