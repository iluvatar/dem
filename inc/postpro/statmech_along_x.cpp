// Computes stat mech vars alogn xaxis inside sample

#include "postpro/statmech_along_x.hpp"

namespace DEM {
  namespace POSTPRO {
    
    // NOTE: Using the reference particle state
    int StatMechAlongXAxis::init_tasks(const std::vector<Particle> & particles_ref, const int & ngrains, const double & dmin, const double & dmax, const int & dim) {
      const std::string msg = "Init tasks for StatMechAlongXAxis"; UTIL::print_start(msg);
      ngrains_ = ngrains;
      d_ = dmin; d3_ = d_*d_*d_;
      Vmin_ = UTIL::voronoi_vmin(dim); // normalized by d_^D // WARNING: Actual value could be smaller because of interpenetrations 
      Vmax_ = Vmin_*CUBE(dmax/dmin); // normalized by d_^D 

      nbins_ = 10; // WARNING: Completely arbitrary

      // memory
      histos_vorovol_.resize(nbins_);
      X_.resize(nbins_);
      K_.resize(nbins_);
      S_.resize(nbins_);
      SuK_.resize(nbins_);
      VT_.resize(nbins_);
      ngrains_bin_.resize(nbins_);
      CuN_.resize(nbins_);

      // construct histograms
      for (int ii = 0; ii < nbins_; ++ii) {
	histos_vorovol_[ii].construct(0.35*Vmin_, 2.3*Vmax_, 50); // min, max, nbins
      }

      // compute box lengths and dx
      xmin_ = particles_ref[ngrains_ + LEFT ].R[0];
      xmax_ = particles_ref[ngrains_ + RIGHT].R[0]; ASSERT(xmax_ > xmin_);
      dx_ = (xmax_ - xmin_)/nbins_; ASSERT(dx_ > 0);

      ready_ = true;
      UTIL::print_done(msg);
      return EXIT_SUCCESS;					                
    }
    
    int StatMechAlongXAxis::print_data() {
      UTIL::print_start("Printing statmech data along X axis.");
      // set print file names and open streams
      std::stringstream name;
      name.str(""); name << "POSTPRO/statmech_along_x_axis-voronoi_X.dat"  ; X_fout_.open(name.str());
      name.str(""); name << "POSTPRO/statmech_along_x_axis-voronoi_K.dat"  ; K_fout_.open(name.str());
      name.str(""); name << "POSTPRO/statmech_along_x_axis-voronoi_S.dat"  ; S_fout_.open(name.str());
      name.str(""); name << "POSTPRO/statmech_along_x_axis-voronoi_SuK.dat"; SuK_fout_.open(name.str());
      name.str(""); name << "POSTPRO/statmech_along_x_axis-voronoi_VT.dat" ; VT_fout_.open(name.str());
      name.str(""); name << "POSTPRO/statmech_along_x_axis-voronoi_CuN.dat"; CuN_fout_.open(name.str());
      name.str(""); name << "POSTPRO/statmech_along_x_axis-voronoi_ngrains.dat" ; ngrains_bin_fout_.open(name.str());

      // print stat mech vars
      for (int ii = 0; ii < nbins_; ++ii) {
	const double xnew = ii*dx_/(xmax_ - xmin_); // in [0, 1)]
	X_fout_.print(xnew, X_[ii]);
	K_fout_.print(xnew, K_[ii]);
	S_fout_.print(xnew, S_[ii]);
	SuK_fout_.print(xnew, SuK_[ii]);
	CuN_fout_.print(xnew, CuN_[ii]);
	VT_fout_.print(xnew, VT_[ii]);
	ngrains_bin_fout_.print(xnew, ngrains_bin_[ii]);
      }

      // close streams
      X_fout_.close();
      K_fout_.close();
      S_fout_.close();
      SuK_fout_.close();
      CuN_fout_.close();
      VT_fout_.close();
      ngrains_bin_fout_.close();

      // print histograms
      for (int ii = 0; ii < nbins_; ++ii) {
	std::stringstream name;
	name.str(""); name << "POSTPRO/statmech-histoVoroVol-alongxaxis-bin_" << ii << ".dat";
	histos_vorovol_[ii].print(name.str());
	// scaled versions
	double Vmean = 0, Vsigma = 0;
	histos_vorovol_[ii].mean_and_sigma(Vmean, Vsigma);
	double mx = 1.0, bx = 0.0, my = 1.0, by = 0.0;
	if (Vmean > Vmin_) {
	  mx = 1.0/(Vmean-Vmin_), bx = -Vmin_/(Vmean-Vmin_), my = Vmean-Vmin_, by = 0.0;
	}
	name.str(""); name << "POSTPRO/statmech-histoVoroVol_scaled-alongxaxis-bin_" << ii << ".dat";
	histos_vorovol_[ii].print(name.str(), mx, bx, my, by);
      }

      UTIL::print_done("Printing statmech data along x axis.");
      return EXIT_SUCCESS;
    }
    
    int StatMechAlongXAxis::end_tasks() {
      print_data();
      return EXIT_SUCCESS; 
    }

  } // namespace POSTPRO
} // namespace DEM

