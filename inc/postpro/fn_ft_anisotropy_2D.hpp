#ifndef __FN_FT_ANISOTROPY_2D__
#define __FN_FT_ANISOTROPY_2D__

#include "matrix3d.hpp"
#include "vector3d.hpp"
#include "utility.hpp"
#include "file_util.hpp"
#include <array>
#include <cmath>
#include <algorithm>
#include <numeric>

/**
   Class to compute the force anisotropies in 2D, projected onto several planes
*/
namespace DEM {
  namespace POSTPRO {

    class FnFtAnisotropy2D {
      // data
    private: 
      enum PLANE {XY = 0, XZ, YZ, TOTALPLANE}; //< Stores the total number of plane projections to process
      enum DIR {N = 0, T, TOTALDIR}; //< Normal or tangential direction
      static const int nbins_ = 25;

      typedef std::array<Mat3d_t, PLANE::TOTALPLANE> MatArray_t;
      typedef std::array<std::array<Vec3d_t, nbins_>, PLANE::TOTALPLANE> HistoArrayVec3d_t;
      typedef std::array<std::array<int, nbins_>, PLANE::TOTALPLANE> HistoArrayI_t;
      
      MatArray_t H_[DIR::TOTALDIR];                                         //< Fabric tensors for computing contact anisotropies 
      HistoArrayVec3d_t fsum_histo_[DIR::TOTALDIR];			     
      HistoArrayI_t nc_[DIR::TOTALDIR];
      double min_ = 0, max_ = 0, delta_angle_ = 0;
      
      std::array<UTIL::OFileStream, PLANE::TOTALPLANE> a_fout_[DIR::TOTALDIR]; //< anisotropies ofstreams
      std::array<UTIL::OFileStream, PLANE::TOTALPLANE> H_fout_[DIR::TOTALDIR]; //< H-tensors ofstreams

      // methods
    public :  
      FnFtAnisotropy2D();
      ~FnFtAnisotropy2D();
      void print(const double & time);
      void reset_fhistos(void); 
      void reset_Htensors(void); 
      template <class ContactPtrList_t> 
      void operator()(const ContactPtrList_t & contacts_ptrs,  //< list of internal contacts
		      const double & EPS,                      //< Tolerance
		      const double & time)
      {
	static const std::array<Vec3d_t, PLANE::TOTALPLANE> N = {Vec_uz, Vec_uy, Vec_ux}; //< normal vectors for each projection
	static const std::array<Vec3d_t, PLANE::TOTALPLANE> x = {Vec_ux, Vec_ux, Vec_uy}; //< horizontal vectors for the given projection  
	static const std::array<Vec3d_t, PLANE::TOTALPLANE> y = {Vec_uy, Vec_uz, Vec_uz}; //< vertical   vectors for the given projection  

	// reset
	reset_Htensors();

	// compute fn and ft orientation histograms, cummulating all history
	if (contacts_ptrs.size() > 0) {
	  // accumulate data per orientation
	  for (const auto & cptr : contacts_ptrs) {
	    const Vec3d_t F[2] = { cptr.get().Fn(), cptr.get().Ft() };
	    for (int dir = DIR::N; dir < DIR::TOTALDIR; ++dir) {
	      for (int plane = PLANE::XY; plane < PLANE::TOTALPLANE; ++plane) {
		if (std::fabs(N[plane].dot(F[dir].normalized())) < EPS) {
		  double angle = std::atan2(F[dir].dot(y[plane]), F[dir].dot(x[plane]));
		  if (angle < 0) angle += M_PI;
		  const int angle_bin = int((angle - min_)/delta_angle_); ASSERT(0 <= angle_bin && angle_bin < nbins_);		
		  fsum_histo_[dir][plane][angle_bin] += (F[dir] - N[plane].dot(F[dir])*N[plane]); // vectorial sum!
		  nc_[dir][plane][angle_bin]++;
		}
	      }
	    }
	  }

	  // Compute the tensors from the histograms previously computed
	  for (const auto & cptr : contacts_ptrs) {
	    const Vec3d_t F[2] = { cptr.get().Fn(), cptr.get().Ft() };
	    for (int dir = DIR::N; dir < DIR::TOTALDIR; ++dir) {
	      for (int plane = PLANE::XY; plane < PLANE::TOTALPLANE; ++plane) {
		if (std::fabs(N[plane].dot(F[dir].normalized())) < EPS) {
		  for (int ia = 0; ia <= 1; ++ia) {
		    for (int ib = 0; ib <= 1; ++ib) {
		      for (int angle_bin = 0; angle_bin < nbins_; ++angle_bin) {
			const double theta = min_ + angle_bin*delta_angle_;
			const double Nlocal[2] = { std::cos(theta), std::sin(theta) };
			const double Tlocal[2] = {-std::sin(theta), std::cos(theta) };
			if (DIR::N == dir)
			  H_[dir][plane](ia, ib) += fsum_histo_[dir][plane][angle_bin].norm()*Nlocal[ia]*Nlocal[ib]*delta_angle_/nc_[dir][plane][angle_bin];
			else if (DIR::T == dir)
			  H_[dir][plane](ia, ib) += fsum_histo_[dir][plane][angle_bin].norm()*Nlocal[ia]*Tlocal[ib]*delta_angle_/nc_[dir][plane][angle_bin];
		      }
		    }
		  }
		}
	      }
	    }
	  }
	} // ncontacts > 0

	print(time); // anisotropies are computed and printed here
      }

    }; // class FnFtAnisotropy2D 

  } // namespace POSTPRO
} // namespace DEM

#endif // __FN_FT_ANISOTROPY_2D__
