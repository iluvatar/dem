// Computes compactivity as function of the number of particles in the statistics

#include "postpro/voronoi_nparticles.hpp"

namespace DEM {
  namespace POSTPRO {
    
    int VoronoiNparticles::init_tasks(const int & ngrains, const double & d, const int & dim) {
      const std::string msg = "Init tasks for VoronoiNparticles"; UTIL::print_start(msg);
      ngrains_ = ngrains;
      d_ = d; d3_ = d_*d_*d_;
      Vmin_ = UTIL::voronoi_vmin(dim); // normalized by d_^D // WARNING: Actual value could be smaller because of interpenetrations 
      int status = std::system("mkdir POSTPRO/voronoi_nparticles"); 
      if (0 != status) print_warning("Possible error creating directory POSTPRO/voronoi_nparticles");

      //NCLUSTER_ = std::ilogb(ngrains) + 1; // last one for the whole sample
      NCLUSTER_ = 20; // 20 bins
      histo_.resize(NCLUSTER_);
      ngrains_group_.resize(NCLUSTER_);
      
      // cluster sizes
      for(int ii = 0; ii < NCLUSTER_; ii++) {
	//ngrains_cluster_[ii] = 1 + ii*(double(ngrains_ - 1.0)/NCLUSTER_);
	//ngrains_group_[ii] = std::pow(2, ii);
	ngrains_group_[ii] = 10 + ii*(ngrains_ - 10.0)/NCLUSTER_;
      }
      ngrains_group_[NCLUSTER_ - 1] = ngrains_;
      
      // histograms
      for(int ii = 0; ii < NCLUSTER_; ++ii) {
	std::stringstream name; name << "voronoi_nparticles - " << ngrains_group_[ii];
	histo_[ii].construct(0.9*Vmin_, 2.1*Vmin_, 50, name.str());
      }

      // other arrays
      X_.resize(NCLUSTER_);
      K_.resize(NCLUSTER_);
      KuN_.resize(NCLUSTER_);
      S_.resize(NCLUSTER_);
      SuK_.resize(NCLUSTER_);
      VT_.resize(NCLUSTER_);
      CuN_.resize(NCLUSTER_);

      ready_ = true;
      UTIL::print_done(msg);
      return EXIT_SUCCESS;					                
    }
    

    int VoronoiNparticles::print_data() {
      const std::string msg = "Printing multigroup voronoi data."; UTIL::print_start(msg);
      
      std::stringstream ss; 
      for(int ii = 0; ii < NCLUSTER_; ++ii) {
	ss.str(""); ss << "POSTPRO/voronoi_nparticles/P_group_" << std::setfill('0') << std::setw(6) << ngrains_group_[ii] << ".dat";
	histo_[ii].print(ss.str()); 
	ss.str(""); ss << "POSTPRO/voronoi_nparticles/P_group_" << std::setfill('0') << std::setw(6) << ngrains_group_[ii] << "_scaled.dat";
	print_scaled_histo_multivoro(histo_[ii], 1, ss.str()); 
	ss.str(""); ss << "POSTPRO/voronoi_nparticles/P_group_" << std::setfill('0') << std::setw(6) << ngrains_group_[ii] << "_mean_sigma.dat";
	histo_[ii].print_mean_sigma(ss.str()); 
      }
      
      // set print file names and open streams
      std::stringstream name;
      name.str(""); name << "POSTPRO/multigroup-voronoi_X.dat"   ; X_fout_.open(name.str());
      name.str(""); name << "POSTPRO/multigroup-voronoi_K.dat"   ; K_fout_.open(name.str());
      name.str(""); name << "POSTPRO/multigroup-voronoi_KuN.dat" ; KuN_fout_.open(name.str());
      name.str(""); name << "POSTPRO/multigroup-voronoi_S.dat"   ; S_fout_.open(name.str());
      name.str(""); name << "POSTPRO/multigroup-voronoi_SuK.dat" ; SuK_fout_.open(name.str());
      name.str(""); name << "POSTPRO/multigroup-voronoi_VT.dat"  ; VT_fout_.open(name.str());
      name.str(""); name << "POSTPRO/multigroup-voronoi_CuN.dat" ; CuN_fout_.open(name.str());

      // print stat mech vars
      for (int ii = 0; ii < NCLUSTER_; ++ii) {
	X_fout_.print(ngrains_group_[ii], X_[ii]);
	K_fout_.print(ngrains_group_[ii], K_[ii]);
	KuN_fout_.print(ngrains_group_[ii], KuN_[ii]);
	S_fout_.print(ngrains_group_[ii], S_[ii]);
	SuK_fout_.print(ngrains_group_[ii], SuK_[ii]);
	CuN_fout_.print(ngrains_group_[ii], CuN_[ii]);
	VT_fout_.print(ngrains_group_[ii], VT_[ii]);
      }
      
      // close streams
      X_fout_.close();
      K_fout_.close();
      KuN_fout_.close();
      S_fout_.close();
      SuK_fout_.close();
      CuN_fout_.close();
      VT_fout_.close();

      UTIL::print_done(msg);
      return EXIT_SUCCESS;
    }
    
    int VoronoiNparticles::end_tasks() {
      print_data();
      return EXIT_SUCCESS; 
    }
    // ---------------------------------------------------------------------
    // AUXILIARY FUNCTIONS
    // ---------------------------------------------------------------------

  void VoronoiNparticles::print_scaled_histo_multivoro(Histogram1D & histo, const double & size, const std::string & filename) 
  {
    // compute voronoi parameters
    double Vmean, Vsigma; 
    histo.mean_and_sigma(Vmean, Vsigma);
    const double Vmin = size*Vmin_;
    double mx = 1.0, bx = 0.0, my = 1.0, by = 0.0;
    if (Vmean > Vmin) {
      mx = 1.0/(Vmean-Vmin), bx = -Vmin/(Vmean-Vmin), my = Vmean-Vmin, by = 0.0;
    }
    else {
      print_warning("voronoi multi: Vmean < Vmin");
      print_warning("size  = ", size);
      print_warning("Vmean = ", Vmean);
      print_warning("Vmin  = ", Vmin);
    }
    // print
    histo.print(filename.c_str(), mx, bx, my, by);
  }

    
    
  } // namespace POSTPRO
} // namespace DEM


