/**
   Computes and prints the amount of initial and residual contacts
**/

#ifndef __POSTPRO_RESIDUAL__
#define __POSTPRO_RESIDUAL__

#include "postpro_base_functor.hpp"
#include "../file_util.hpp"
#include "../contact.hpp"
#include <vector>

namespace DEM {
  namespace POSTPRO {
    
    class ResidualContacts : public Base_functor {
    public :
      ResidualContacts() {}
      ~ResidualContacts() { end_tasks(); }
      int init_tasks() ;
      int operator()(const double & time, const std::vector<Contact> & contacts) ;
      int end_tasks() ;
    private :
      UTIL::OFileStream fout_residual_, fout_initial_;
      bool ready_ = false;
    };
    

  } // namespace POSTPRO
} // namespace DEM

#endif // __POSTPRO_RESIDUAL__
