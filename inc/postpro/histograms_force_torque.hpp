// Computes the orientation and magnitude histograms for forces and torques

#ifndef __POSTPRO_FORCE_TORQUE__
#define __POSTPRO_FORCE_TORQUE__

#include "postpro_base_functor.hpp"
#include "../histogram1d.hpp"
#include "../contact.hpp"
#include <string>
#include <vector>
#include <functional>
#include "yaml-cpp/yaml.h" 

namespace DEM {
  namespace POSTPRO {
    
    class HistogramsForceTorque : public Base_functor {
    public :
      int init_tasks(const YAML::Node & config) ;
      int end_tasks() ;
      template <typename contactlist_t>
      int operator()(const double & time, const contactlist_t & contacts, const bool & print_snapshots, const double & EPS) 
      {
	std::string name = "force and torque histograms"; UTIL::print_start(name);
      
	// reset snapshot histograms
	if (print_snapshots) std::for_each(hsnapshot_, hsnapshot_ + TOTAL, std::mem_fn(&Histogram1D::reset));

	// forces magnitudes
	for (const auto & c : contacts) {
	  histogram_[P_Fn].increment(c.get().normFn_);  histogram_[P_Ft].increment(c.get().Ft_.norm()); histogram_[P_T].increment(c.get().T_.norm());
	  if (print_snapshots) {
	    hsnapshot_[P_Fn].increment(c.get().normFn_);  hsnapshot_[P_Ft].increment(c.get().Ft_.norm()); hsnapshot_[P_T].increment(c.get().T_.norm()); 
	  }
	}
	// forces orientations
	Vec3d_t Vec1;
	double angle;
	for(const auto & c : contacts) { 
	  // F
	  Vec1 = c.get().F();
	  if (std::fabs(Vec1.normalized().dot(Vec_uz)) <= EPS) {	
	    angle = std::atan2(Vec1[1], Vec1[0]); 
	    histogram_[P_F_xy].increment(angle); histogram_[P_F_xy].increment(angle - sign(angle)*M_PI); 
	    if (print_snapshots) { hsnapshot_[P_F_xy].increment(angle); hsnapshot_[P_F_xy].increment(angle - sign(angle)*M_PI); }
	  }
	  if (std::fabs(Vec1.normalized().dot(Vec_uy)) <= EPS) {	
	    angle = std::atan2(Vec1[2], Vec1[0]); 
	    histogram_[P_F_xz].increment(angle); histogram_[P_F_xz].increment(angle - sign(angle)*M_PI);
	    if (print_snapshots) { hsnapshot_[P_F_xz].increment(angle); hsnapshot_[P_F_xz].increment(angle - sign(angle)*M_PI); }
	  }
	  if (std::fabs(Vec1.normalized().dot(Vec_ux)) <= EPS) {	
	    angle = std::atan2(Vec1[2], Vec1[1]); 
	    histogram_[P_F_yz].increment(angle); histogram_[P_F_yz].increment(angle - sign(angle)*M_PI);
	    if (print_snapshots) { hsnapshot_[P_F_yz].increment(angle); hsnapshot_[P_F_yz].increment(angle - sign(angle)*M_PI); }
	  }
	  // Fn
	  Vec1 = c.get().Fn();	
	  if (std::fabs(Vec1.normalized().dot(Vec_uz)) <= EPS) {	
	    angle = std::atan2(Vec1[1], Vec1[0]); 
	    histogram_[P_Fn_xy].increment(angle); histogram_[P_Fn_xy].increment(angle - sign(angle)*M_PI); 
	    if (print_snapshots) { hsnapshot_[P_Fn_xy].increment(angle); hsnapshot_[P_Fn_xy].increment(angle - sign(angle)*M_PI); }
	  }
	  if (std::fabs(Vec1.normalized().dot(Vec_uy)) <= EPS) {	
	    angle = std::atan2(Vec1[2], Vec1[0]); 
	    histogram_[P_Fn_xz].increment(angle); histogram_[P_Fn_xz].increment(angle - sign(angle)*M_PI);
	    if (print_snapshots) { hsnapshot_[P_Fn_xz].increment(angle); hsnapshot_[P_Fn_xz].increment(angle - sign(angle)*M_PI); }
	  }
	  if (std::fabs(Vec1.normalized().dot(Vec_ux)) <= EPS) {	
	    angle = std::atan2(Vec1[2], Vec1[1]); 
	    histogram_[P_Fn_yz].increment(angle); histogram_[P_Fn_yz].increment(angle - sign(angle)*M_PI);
	    if (print_snapshots) { hsnapshot_[P_Fn_yz].increment(angle); hsnapshot_[P_Fn_yz].increment(angle - sign(angle)*M_PI); }
	  }
	  // Ft
	  Vec1 = c.get().Ft_;
	  if (std::fabs(Vec1.normalized().dot(Vec_uz)) <= EPS) {	
	    angle = std::atan2(Vec1[1], Vec1[0]); 
	    histogram_[P_Ft_xy].increment(angle); histogram_[P_Ft_xy].increment(angle - sign(angle)*M_PI); 
	    if (print_snapshots) { hsnapshot_[P_Ft_xy].increment(angle); hsnapshot_[P_Ft_xy].increment(angle - sign(angle)*M_PI); }
	  }
	  if (std::fabs(Vec1.normalized().dot(Vec_uy)) <= EPS) {	
	    angle = std::atan2(Vec1[2], Vec1[0]); 
	    histogram_[P_Ft_xz].increment(angle); histogram_[P_Ft_xz].increment(angle - sign(angle)*M_PI);
	    if (print_snapshots) { hsnapshot_[P_Ft_xz].increment(angle); hsnapshot_[P_Ft_xz].increment(angle - sign(angle)*M_PI); }
	  }
	  if (std::fabs(Vec1.normalized().dot(Vec_ux)) <= EPS) {	
	    angle = std::atan2(Vec1[2], Vec1[1]); 
	    histogram_[P_Ft_yz].increment(angle); histogram_[P_Ft_yz].increment(angle - sign(angle)*M_PI);
	    if (print_snapshots) { hsnapshot_[P_Ft_yz].increment(angle); hsnapshot_[P_Ft_yz].increment(angle - sign(angle)*M_PI); }
	  }
	}
            
	// print snapshosts
	if (print_snapshots) { print(hsnapshot_, time); }
      
	UTIL::print_done(name);
	return EXIT_SUCCESS;
      }

    private :
      // data
      enum { P_Fn = 0, P_Ft, P_T, P_F_xy, P_F_xz, P_F_yz, P_Fn_xy, P_Fn_xz, P_Fn_yz, P_Ft_xy, P_Ft_xz, P_Ft_yz, TOTAL };
      Histogram1D histogram_[TOTAL], hsnapshot_[TOTAL];
      // methods
      template <typename suff_t>
      int print(Histogram1D histo [TOTAL], const suff_t & suffix) 
      {
	double mean, sigma;
	histo[P_Fn].mean_and_sigma(mean, sigma); if (0 == mean) mean = 1.0; 
	histo[P_Fn].print(UTIL::composite_name("POSTPRO/histograms_F_T/P_Fn", suffix), 1.0/mean, 0.0, mean, 0.0);
	histo[P_Ft].mean_and_sigma(mean, sigma); if (0 == mean) mean = 1.0; 
	histo[P_Ft].print(UTIL::composite_name("POSTPRO/histograms_F_T/P_Ft", suffix), 1.0/mean, 0.0, mean, 0.0);
	histo[P_T].mean_and_sigma(mean, sigma); if (0 == mean) mean = 1.0; 
	histo[P_T].print(UTIL::composite_name("POSTPRO/histograms_F_T/P_T", suffix), 1.0/mean, 0.0, mean, 0.0);
	histo[P_F_xy].print(UTIL::composite_name("POSTPRO/histograms_F_T/P_F_xy", suffix));
	histo[P_F_xz].print(UTIL::composite_name("POSTPRO/histograms_F_T/P_F_xz", suffix));
	histo[P_F_yz].print(UTIL::composite_name("POSTPRO/histograms_F_T/P_F_yz", suffix));
	histo[P_Fn_xy].print(UTIL::composite_name("POSTPRO/histograms_F_T/P_Fn_xy", suffix));
	histo[P_Fn_xz].print(UTIL::composite_name("POSTPRO/histograms_F_T/P_Fn_xz", suffix));
	histo[P_Fn_yz].print(UTIL::composite_name("POSTPRO/histograms_F_T/P_Fn_yz", suffix));
	histo[P_Ft_xy].print(UTIL::composite_name("POSTPRO/histograms_F_T/P_Ft_xy", suffix));
	histo[P_Ft_xz].print(UTIL::composite_name("POSTPRO/histograms_F_T/P_Ft_xz", suffix));
	histo[P_Ft_yz].print(UTIL::composite_name("POSTPRO/histograms_F_T/P_Ft_yz", suffix));
      
	return EXIT_SUCCESS;
      }
    }; // class HistogramsForceTorque


  } // namespace POSTPRO
} // namespace DEM

#endif // __POSTPRO_FORCE_TORQUE__
