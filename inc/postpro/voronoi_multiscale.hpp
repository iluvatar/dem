// Computes stat mech vars alogn xaxis inside sample

#ifndef __VORONOI_MULTISCALE__
#define __VORONOI_MULTISCALE__

#include "postpro_base_functor.hpp"
#include "../file_util.hpp"
#include "../utility.hpp"
#include "../histogram1d.hpp"
#include "../functions.hpp"
#include <vector>
#include <algorithm>
#include <string>
#include <sstream>
#include <queue>
#include <set>

namespace DEM {
  namespace POSTPRO {
    
    class VoronoiMultiscale : public Base_functor {
    public :
      VoronoiMultiscale() {};
      ~VoronoiMultiscale() { print_log("VoronoiMultiscale destructor called"); /*end_tasks();*/ }
      int init_tasks(const int & ngrains, const double & d, const int & dim, const std::vector<double> & voronoi_volumes_ref) ;
      int end_tasks() ;
      int print_data();
      template <class IndexList_t, class NeighborList_t> 
      int 
      operator()(const int & dim,				   // dimension of simulation (2 or 3)
		 const int & ncmin,				   // minimum number of contacts
		 const IndexList_t & filtered_ids,		   // filtered ids (for example, inside volume)
		 const std::vector<double> & voronoi_volumes,	   // voronoi volumes 
		 const NeighborList_t & neigh)	                   // neighbor information
      {
	const std::string msg = "Computing voronoi multiscale variables"; UTIL::print_start(msg);
	ASSERT(true == ready_);
	ASSERT(2 == dim || 3 == dim);
	ASSERT(ncmin >= 0);
	print_warning("Using all internal particles, regardless their contact number per particle");
      
	// accumulate voronoi volumes per cluster
	for(int ii = 0; ii < NCLUSTER_ - 1; ++ii) {
	  print_log("Cluster size = ", ngrains_cluster_[ii]);
	  find_cluster_count_volumes(ngrains_cluster_[ii], filtered_ids, neigh, voronoi_volumes, histo_multivoro_[ii]);
	}
	histo_multivoro_[NCLUSTER_-1].increment(std::accumulate(voronoi_volumes.begin(), voronoi_volumes.end(), 0.0));
	//histo_multivoro_[NCLUSTER_-1].increment(std::accumulate(voronoi_volumes.begin(), voronoi_volumes.end(), 0.0)/ngrains_cluster_[NCLUSTER_-1]);
	UTIL::print_done("Accumulating multi-scale volumes on histograms");
      
	// compute stat mech parameters per cluster
	const double lambda_cube = M_E/100.0;; // = e (pi d^3/6 phiRCP - Vmin)/kRCP
	for (int ii = 0; ii < NCLUSTER_; ++ii) {
	  X_[ii] = K_[ii] = S_[ii] = CuN_[ii] = SuK_[ii] = 0;
	  const double Vmin_multi = ngrains_cluster_[ii]*Vmin_;
	  double Vmean = 0, Vsigma = 0;
	  histo_multivoro_[ii].mean_and_sigma(Vmean, Vsigma);
	  if (Vsigma > 0 && (Vmean > Vmin_multi)) {
	    X_[ii] = UTIL::statmech_X(Vmean, Vsigma, Vmin_multi);
	    K_[ii] = UTIL::statmech_K(Vmean, Vsigma, Vmin_multi);
	    KuN_[ii] = K_[ii]/ngrains_cluster_[ii]; 
	    S_[ii] = UTIL::statmech_S(Vmean, K_[ii], Vmin_multi, lambda_cube); 
	    SuK_[ii] = (K_[ii] > 0) ? S_[ii]/K_[ii] : 0.0; 
	    CuN_[ii] = 0.0; // K_[ii]*ngrains_bin_[ii]/ngrains_;  // WARNING: How much is this ??????
	  }
	}
	UTIL::print_done("Computing voro stat mech per cluster");

	// print
	print_data();
      
	UTIL::print_done(msg);
	return EXIT_SUCCESS; 
      }    

    private : // methods
      template <class IndexList_t, class NeighborList_t>
      void find_cluster_count_volumes(const int & clustersize, const IndexList_t & indexes, const NeighborList_t & neigh,  
				      const std::vector<double> & voronoi_volumes,  Histogram1D & histo)
      {
	std::string name = "Multiscale voronoi partition volumes : FInding cluster"; UTIL::print_start(name);
	print_log("clustersize = ", clustersize);
	print_log("Selected grains = ", indexes.size());
	if (int(indexes.size()) < clustersize) return;

	// declare local utility list and vars
	std::set<int> non_visited; // stores non-visited indexes
	std::set<int> processed; // stores processed indexes which will be returned to non-visited in case no cluster is found
	for (const auto & idx : indexes) { non_visited.insert(idx); }
      
	// find clusters
	int nsamples = 0;
	for (const auto & id : indexes) {
	  if (int(non_visited.size()) < clustersize) break;
	  if (true == non_visited.empty()) break;
	  if (non_visited.end() == non_visited.find(id)) continue;
	  processed.clear();
	  int counter = 0;
	  double volume = 0;
	  find_voronoi_volume_BFS(neigh, voronoi_volumes, id, clustersize, non_visited, processed, counter, volume);
	  if (counter == clustersize) { // cluster found
	    //histo.increment(volume/clustersize); 
	    histo.increment(volume); 
	    nsamples++;
	  }	
	  else { // recover processed indexes since they did not build a cluster of size clustersize
	    for (const auto & p : processed) {
	      non_visited.insert(p);
	    }
	  }
	}
	print_log("Total number of samples for this clustersize = ", nsamples);
    
	UTIL::print_done(name);
      }

      template <class NeighborList_t>
      void find_voronoi_volume_BFS(const NeighborList_t & neigh, const std::vector<double> & voronoi_volumes, 
				   const int & id, const int & maxcount,  
				   std::set<int> & non_visited, std::set<int> & processed,
				   int & count, double & current_vol) 
      {
	if (non_visited.end() == non_visited.find(id)) return;
      
	current_vol += voronoi_volumes[id];
	non_visited.erase(id);
	processed.insert(id);
	count++;
	if (count == maxcount) return;
	std::queue<int> q;
	q.push(id);
	while (!q.empty()) {
	  const int v = q.front();
	  q.pop();
	  for (const auto & jd : neigh[v]) {
	    if (non_visited.end() != non_visited.find(jd)) {
	      current_vol += voronoi_volumes[jd];
	      non_visited.erase(jd);
	      processed.insert(jd);
	      count++;
	      q.push(jd);
	      if (count == maxcount) return;
	    }
	  }
	}    
      }

      void print_scaled_histo_multivoro(Histogram1D & histo, const double & size, const std::string & filename);
      
    private : // data
      int NCLUSTER_ = 0;					   // Number of clusters, uniform partition 
      std::vector<Histogram1D> histo_multivoro_; 
      std::vector<int> ngrains_cluster_;
      std::vector<double> X_, K_, KuN_, S_, CuN_, SuK_, VT_;       // Stat mech values per bin
      int ngrains_ = 0;						   // Number of grains
      double d_ = 0, d3_ = 0;					   // diammeter, d^3
      double Vmin_ = 1.0e100;					   // Minimum voronoi volume
      bool ready_ = false;                                         // flag to assert system ready
      UTIL::OFileStream X_fout_, K_fout_, KuN_fout_, SuK_fout_,
	S_fout_, CuN_fout_, VT_fout_; 
    };

    
  } // namespace POSTPRO
} // namespace DEM

#endif // __VORONOI_MULTISCALE__

