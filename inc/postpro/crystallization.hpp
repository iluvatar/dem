// Computes the amount of crystallization for a sample

#ifndef __POSTPRO_CRYSTALLIZATION__
#define __POSTPRO_CRYSTALLIZATION__

#include "postpro_base_functor.hpp"
#include "../file_util.hpp"
#include "../contact.hpp"
#include <vector>
#include <numeric>

namespace DEM {
  namespace POSTPRO {
    
    class Crystallization : public Base_functor {
    public :
      Crystallization() {}
      ~Crystallization() { /*end_tasks();*/ }
      int init_tasks() ;
      int operator()(const double & time, const std::vector<Contact> & contacts) ;
      int end_tasks() ;
    private :
      UTIL::OFileStream q4out_, q6out_, crystalout_;
      std::vector<double> lvals_;
      std::vector<double> qvals_; // global indexes since we are averaging over contacts
      bool ready_ = false;
    };
    
  } // namespace POSTPRO
} // namespace DEM

#endif // __POSTPRO_CRYSTALLIZATION__
