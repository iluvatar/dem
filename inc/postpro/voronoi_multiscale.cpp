// Computes stat mech vars alogn xaxis inside sample

#include "postpro/voronoi_multiscale.hpp"

namespace DEM {
  namespace POSTPRO {
    
    int VoronoiMultiscale::init_tasks(const int & ngrains, const double & d, const int & dim, const std::vector<double> & voronoi_volumes_ref) {
      const std::string msg = "Init tasks for VoronoiMultiscale"; UTIL::print_start(msg);
      ngrains_ = ngrains;
      d_ = d; d3_ = d_*d_*d_;
      Vmin_ = UTIL::voronoi_vmin(dim); // normalized by d_^D // WARNING: Actual value could be smaller because of interpenetrations 
      int status = std::system("mkdir POSTPRO/histo_multivoro"); 
      if (0 != status) print_warning("Possible error creating directory POSTPRO/histo_multivoro");

      NCLUSTER_ = std::ilogb(ngrains) + 1; // last one for the whole sample
      histo_multivoro_.resize(NCLUSTER_);
      ngrains_cluster_.resize(NCLUSTER_);

      // cluster sizes
      for(int ii = 0; ii < NCLUSTER_ - 1; ii++) {
	//ngrains_cluster_[ii] = 1 + ii*(double(ngrains_ - 1.0)/NCLUSTER_);
	ngrains_cluster_[ii] = std::pow(2, ii);
      }
      ngrains_cluster_[NCLUSTER_ - 1] = ngrains_;
      // histograms
      for(int ii = 0; ii < NCLUSTER_; ++ii) {
	std::stringstream name; name << "P_multivoro - " << ngrains_cluster_[ii];
	//const double cluster_min = ngrains_cluster_[ii]*0.90*Vmin_;
	//const double cluster_max = ngrains_cluster_[ii]*2.00*Vmin_;
	//const double param_min[3] = {2000, ngrains_cluster_[ii]*0.90*Vmin_, ngrains_cluster_[ii]*0.99*Vmin_};
	//const double param_max[3] = {2000, ngrains_cluster_[ii]*1.60*Vmin_, ngrains_cluster_[ii]*1.10*Vmin_};
	//const double cluster_min = exponential_ramp(ngrains_cluster_[ii], param_min);
	//const double cluster_max = exponential_ramp(ngrains_cluster_[ii], param_max);
	//const double Vmean_ref = std::accumulate(voronoi_volumes_ref.begin(), voronoi_volumes_ref.end(), 0.0)/ngrains_; 
	//const double cluster_min = 0.99*Vmin_*ngrains_cluster_[ii];
	//const double cluster_max = 1.3*Vmean_ref*ngrains_cluster_[ii];
	double mean = 0, sigma = 0;
	mean_and_sigma_vector(voronoi_volumes_ref, mean, sigma);
	//const double cluster_min = 0.99*Vmin_*ngrains_cluster_[ii];
	//const double cluster_max = 1.40*Vmin_*ngrains_cluster_[ii];
	//const double cluster_min = 0.99*Vmin_ - 0.0001*ngrains_cluster_[ii];
	//const double cluster_max = 1.60*Vmin_ + 0.0001*ngrains_cluster_[ii];
	//histo_multivoro_[ii].construct(cluster_min, cluster_max, 70*(1+0.010*ngrains_cluster_[ii]), name.str());
	const double param_min[3] = {1000, 1.01*Vmin_*ngrains_cluster_[ii], 1.32*Vmin_*ngrains_cluster_[ii]};
	const double param_max[3] = {1000, 1.70*Vmin_*ngrains_cluster_[ii], 1.34*Vmin_*ngrains_cluster_[ii]};
	const double param_bins[3] = {1000, 70, 150};
	const double cluster_min = exponential_ramp(ngrains_cluster_[ii], param_min);
	const double cluster_max = exponential_ramp(ngrains_cluster_[ii], param_max);
	const int cluster_nbins = exponential_ramp(ngrains_cluster_[ii], param_bins);
	histo_multivoro_[ii].construct(cluster_min, cluster_max, cluster_nbins, name.str());
      }
      // other arrays
      X_.resize(NCLUSTER_);
      K_.resize(NCLUSTER_);
      KuN_.resize(NCLUSTER_);
      S_.resize(NCLUSTER_);
      SuK_.resize(NCLUSTER_);
      VT_.resize(NCLUSTER_);
      CuN_.resize(NCLUSTER_);

      ready_ = true;
      UTIL::print_done(msg);
      return EXIT_SUCCESS;					                
    }
    

    int VoronoiMultiscale::print_data() {
      const std::string msg = "Printing multscale voronoi data."; UTIL::print_start(msg);
      
      std::stringstream ss; 
      for(int ii = 0; ii < NCLUSTER_; ++ii) {
	ss.str(""); ss << "POSTPRO/histo_multivoro/P_multivoro_" << std::setfill('0') << std::setw(6) << ngrains_cluster_[ii] << ".dat";
	histo_multivoro_[ii].print(ss.str()); 
	ss.str(""); ss << "POSTPRO/histo_multivoro/P_multivoro_" << std::setfill('0') << std::setw(6) << ngrains_cluster_[ii] << "_scaled.dat";
	print_scaled_histo_multivoro(histo_multivoro_[ii], ngrains_cluster_[ii], ss.str()); 
	ss.str(""); ss << "POSTPRO/histo_multivoro/P_multivoro_" << std::setfill('0') << std::setw(6) << ngrains_cluster_[ii] << "_mean_sigma.dat";
	histo_multivoro_[ii].print_mean_sigma(ss.str()); 
	ss.str(""); ss << "POSTPRO/histo_multivoro/P_multivoro_" << std::setfill('0') << std::setw(6) << ngrains_cluster_[ii] << "_Vmin_scaled.dat";
	histo_multivoro_[ii].print(ss.str(), 1.0/(ngrains_cluster_[ii]*Vmin_), 0.0, ngrains_cluster_[ii]*Vmin_, 0.0);
	ss.str(""); ss << "POSTPRO/histo_multivoro/P_multivoro_" << std::setfill('0') << std::setw(6) << ngrains_cluster_[ii] << "_Vmin_scaled-max.dat";
	histo_multivoro_[ii].print(ss.str(), 1.0/(ngrains_cluster_[ii]*Vmin_), 0.0, 1.0, 0.0, false, false, 3);
      }
      
      // set print file names and open streams
      std::stringstream name;
      name.str(""); name << "POSTPRO/multiscale-voronoi_X.dat"   ; X_fout_.open(name.str());
      name.str(""); name << "POSTPRO/multiscale-voronoi_K.dat"   ; K_fout_.open(name.str());
      name.str(""); name << "POSTPRO/multiscale-voronoi_KuN.dat" ; KuN_fout_.open(name.str());
      name.str(""); name << "POSTPRO/multiscale-voronoi_S.dat"   ; S_fout_.open(name.str());
      name.str(""); name << "POSTPRO/multiscale-voronoi_SuK.dat" ; SuK_fout_.open(name.str());
      name.str(""); name << "POSTPRO/multiscale-voronoi_VT.dat"  ; VT_fout_.open(name.str());
      name.str(""); name << "POSTPRO/multiscale-voronoi_CuN.dat" ; CuN_fout_.open(name.str());

      // print stat mech vars
      for (int ii = 0; ii < NCLUSTER_; ++ii) {
	X_fout_.print(ngrains_cluster_[ii], X_[ii]);
	K_fout_.print(ngrains_cluster_[ii], K_[ii]);
	KuN_fout_.print(ngrains_cluster_[ii], KuN_[ii]);
	S_fout_.print(ngrains_cluster_[ii], S_[ii]);
	SuK_fout_.print(ngrains_cluster_[ii], SuK_[ii]);
	CuN_fout_.print(ngrains_cluster_[ii], CuN_[ii]);
	VT_fout_.print(ngrains_cluster_[ii], VT_[ii]);
      }

      // close streams
      X_fout_.close();
      K_fout_.close();
      KuN_fout_.close();
      S_fout_.close();
      SuK_fout_.close();
      CuN_fout_.close();
      VT_fout_.close();

      UTIL::print_done(msg);
      return EXIT_SUCCESS;
    }
    
    int VoronoiMultiscale::end_tasks() {
      print_data();
      return EXIT_SUCCESS; 
    }
    // ---------------------------------------------------------------------
    // AUXILIARY FUNCTIONS
    // ---------------------------------------------------------------------
    void VoronoiMultiscale::print_scaled_histo_multivoro(Histogram1D & histo, const double & size, const std::string & filename) 
    {
      // compute voronoi parameters
      double Vmean, Vsigma; 
      histo.mean_and_sigma(Vmean, Vsigma);
      const double Vmin = size*Vmin_;
      double mx = 1.0, bx = 0.0, my = 1.0, by = 0.0;
      if (Vmean > Vmin) {
	mx = 1.0/(Vmean-Vmin), bx = -Vmin/(Vmean-Vmin), my = Vmean-Vmin, by = 0.0;
      }
      else {
	print_warning("voronoi multi: Vmean < Vmin");
	print_warning("size  = ", size);
	print_warning("Vmean = ", Vmean);
	print_warning("Vmin  = ", Vmin);
      }
      // print
      histo.print(filename.c_str(), mx, bx, my, by);
    }
    
  } // namespace POSTPRO
} // namespace DEM


