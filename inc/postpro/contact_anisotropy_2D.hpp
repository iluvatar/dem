#ifndef __CONTACT_ANISOTROPY_2D__
#define __CONTACT_ANISOTROPY_2D__

#include "matrix3d.hpp"
#include "vector3d.hpp"
#include "utility.hpp"
#include "file_util.hpp"
#include <iostream>
#include <fstream>
#include <iomanip>
#include <vector>
#include <array>
#include <cmath>

/**
   Class to compute the contact anisotropy in 2D, projected onto several planes
*/
namespace DEM {
  namespace POSTPRO {

    class ContactAnisotropy2D {
      // data
    private: 
      enum PLANE {XY = 0, XZ, YZ, XZYZ, TOTALPLANE};                               //< Stores the total number of projections to process
      std::array<Mat3d_t, PLANE::TOTALPLANE> Fabric_;                              //< Fabric tensors for computing contact anisotropies 
      std::array<UTIL::OFileStream, PLANE::TOTALPLANE> ac_fout_;			 //< fabric anisotropy ofstream

      // methods
    public :  
      ContactAnisotropy2D();
      ~ContactAnisotropy2D();
      void print(const double & time);
      template <class ContactPtrList_t> 
      void operator()(const ContactPtrList_t & contacts_ptrs,  //< list of internal contacts
		      const double & EPS,                      //< Tolerance
		      const double & time)
      {
	// normal vectors for each projection
	const std::array<Vec3d_t, PLANE::TOTALPLANE> n = {Vec_uz, Vec_uy, Vec_ux};
    
	// reset 
	for (auto & F  : Fabric_) {
	  F.setZero();
	}
	int nc[PLANE::TOTALPLANE] = {0};
	const auto ncontacts = contacts_ptrs.size();
	
	// compute fabric tensors
	if (ncontacts > 0) {
	  for (const auto & cptr : contacts_ptrs) {
	    for (int plane = PLANE::XY; plane < PLANE::TOTALPLANE; ++plane) {
	      const Vec3d_t & N = cptr.get().Normal();
	      // compute projection 
	      double projection = N.dot(n[plane]);
	      if (PLANE::XZYZ == plane) projection = std::min(N.dot(n[PLANE::XZ]), N.dot(n[PLANE::YZ]));
	      // add
	      if (std::fabs(projection) < EPS) {
		Fabric_[plane] += N*(N.transpose());
		nc[plane]++;
	      }
	    }
	  }
	  // normalize 
	  for (int plane = XY; plane < PLANE::TOTALPLANE; ++plane) {
	    if (nc[plane] > 0) {
	      if (PLANE::XZYZ == plane) 
		Fabric_[plane] /= nc[PLANE::XZ] + nc[PLANE::YZ];
	      else 
		Fabric_[plane] /= nc[plane];
	    }    
	  }
	}
	// print
	print(time);
      }

    }; // class ContactAnisotropy2D 

  } // namespace POSTPRO
} // namespace DEM

#endif // __CONTACT_ANISOTROPY_2D__
