// Computes statmech variables, like X, at different scale levels

#ifndef __STATMECH_INTERNAL_EQUILIBRIUM__
#define __STATMECH_INTERNAL_EQUILIBRIUM__

#include "postpro_base_functor.hpp"
#include "../workspace_dem_config.hpp"
#include "../matrix3d.hpp"
#include "../utility.hpp"
#include "../histogram1d.hpp"
#include "../file_util.hpp"
#include "../particle.hpp"
#include <vector>
#include <string>
#include <sstream>
#include <iomanip>

namespace DEM {
  namespace POSTPRO {
    
    class StatMechInternal : public Base_functor {
    public :
      StatMechInternal() {};
      ~StatMechInternal() { print_log("StatMechInternal destructor called"); /*end_tasks();*/ }
      int init_tasks(const std::vector<Particle> & particles, 
		     const int & ngrains, const double & dmin, const double & dmax, const int & dim) ;
      int 
      operator()(const std::vector<Particle> & particles,		   // list of particles
		 const int & dim,				   // dimension of simulation (2 or 3)
		 const int & ncmin,				   // minimum number of contacts
		 const std::vector<double> & voronoi_volumes);	   // voronoi volumes for the system
      int end_tasks() ;
      int print_data();
    private :
      int ngrains_ = 0;		                                    // Number of grains 
      double d_ = 0, d3_ = 0, Vmin_ = 0, Vmax_ = 0;	            // diammeter related 
      bool ready_ = false;                                          // flag to assert system ready
      int ncoronas_ = 0;					    // number of coronas 
      std::vector<Vec3d_t> corona_righttop_;			    // Corona right-top limit
      std::vector<Vec3d_t> corona_leftbottom_;			    // Corona left-bottom limit
      std::vector<double> voro_X_;				    // X per corona  
      std::vector<Histogram1D> histo_voro_vol_;			    // histograms per corona
      std::vector<double> voro_K_;				    // K per corona 
      UTIL::OFileStream voro_X_out_, voro_K_out_;
    };


  } // namespace POSTPRO
} // namespace DEM

#endif // __STATMECH_INTERNAL_EQUILIBRIUM__
