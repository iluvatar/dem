#include "fn_ft_anisotropy_2D.hpp"

namespace DEM {
  namespace POSTPRO {

    FnFtAnisotropy2D::FnFtAnisotropy2D()
    {
      // Define the angular partition parameters
      min_ = 0.0; 
      max_ = +M_PI;
      delta_angle_ = (max_ - min_)/nbins_;
	  
      reset_fhistos();
      reset_Htensors();

      a_fout_[DIR::N][PLANE::XY].open("POSTPRO/an_xy.dat"); a_fout_[DIR::N][PLANE::XY].print("# time", "an_xy", "Eival0", "Eival1", "Eival2");
      a_fout_[DIR::N][PLANE::XZ].open("POSTPRO/an_xz.dat"); a_fout_[DIR::N][PLANE::XZ].print("# time", "an_xz", "Eival0", "Eival1", "Eival2");
      a_fout_[DIR::N][PLANE::YZ].open("POSTPRO/an_yz.dat"); a_fout_[DIR::N][PLANE::YZ].print("# time", "an_yz", "Eival0", "Eival1", "Eival2");
      a_fout_[DIR::T][PLANE::XY].open("POSTPRO/at_xy.dat"); a_fout_[DIR::T][PLANE::XY].print("# time", "at_xy", "Eival0", "Eival1", "Eival2");
      a_fout_[DIR::T][PLANE::XZ].open("POSTPRO/at_xz.dat"); a_fout_[DIR::T][PLANE::XZ].print("# time", "at_xz", "Eival0", "Eival1", "Eival2");
      a_fout_[DIR::T][PLANE::YZ].open("POSTPRO/at_yz.dat"); a_fout_[DIR::T][PLANE::YZ].print("# time", "at_yz", "Eival0", "Eival1", "Eival2");
   
      H_fout_[DIR::N][PLANE::XY].open("POSTPRO/Hn_xy.dat"); H_fout_[DIR::N][PLANE::XY].print("# time ", "H00", "H01", "H10", "H11");
      H_fout_[DIR::N][PLANE::XZ].open("POSTPRO/Hn_xz.dat"); H_fout_[DIR::N][PLANE::XZ].print("# time ", "H00", "H01", "H10", "H11");
      H_fout_[DIR::N][PLANE::YZ].open("POSTPRO/Hn_yz.dat"); H_fout_[DIR::N][PLANE::YZ].print("# time ", "H00", "H01", "H10", "H11");
      H_fout_[DIR::T][PLANE::XY].open("POSTPRO/Ht_xy.dat"); H_fout_[DIR::T][PLANE::XY].print("# time ", "H00", "H01", "H10", "H11");
      H_fout_[DIR::T][PLANE::XZ].open("POSTPRO/Ht_xz.dat"); H_fout_[DIR::T][PLANE::XZ].print("# time ", "H00", "H01", "H10", "H11");
      H_fout_[DIR::T][PLANE::YZ].open("POSTPRO/Ht_yz.dat"); H_fout_[DIR::T][PLANE::YZ].print("# time ", "H00", "H01", "H10", "H11");
    }

    FnFtAnisotropy2D::~FnFtAnisotropy2D()
    {
      for (auto & foutdir : a_fout_) {
	for (auto & fout : foutdir) {
	  fout.close();
	}
      }
      for (auto & foutdir : H_fout_) {
	for (auto & fout : foutdir) {
	  fout.close();
	}
      }
    }

    void FnFtAnisotropy2D::reset_fhistos(void)
    {
      for (auto & fsumdir : fsum_histo_) {
	for (auto & fsum_dir_plane : fsumdir ) {
	  std::fill(fsum_dir_plane.begin(), fsum_dir_plane.end(), Vecnull);
	}
      }
      for (auto & ncdir : nc_) {
	for (auto & nc_dir_plane : ncdir ) {
	  std::fill(nc_dir_plane.begin(), nc_dir_plane.end(), 0.0);
	}
      }
    } 

    void FnFtAnisotropy2D::reset_Htensors(void)
    {
      for (auto & Hdir  : H_) 
	for (auto & Hplane : Hdir) 
	  Hplane.setZero();
    }


    void 
      FnFtAnisotropy2D::print(const double & time)
    {
      // compute and print anisotropies
      std::array<double, 3> Eival;
      for (int dir = DIR::N; dir < DIR::TOTALDIR; ++dir) {
	for (int plane = PLANE::XY; plane < PLANE::TOTALPLANE; ++plane) {
	  eigen_values(H_[dir][plane], Eival[0], Eival[1], Eival[2], true);
	  const double a = ((Eival[2] + Eival[1]) > 0) ? 2*(Eival[2] - Eival[1])/(Eival[2] + Eival[1]) : 0;
	  a_fout_[dir][plane].print(time, a, Eival[0], Eival[1], Eival[2]); 
	}
      }

      // util ofstream
      UTIL::OFileStream fout;

      // print mean force versus orientation
      const std::string fname[DIR::TOTALDIR][PLANE::TOTALPLANE] = { {"POSTPRO/fnmean_xy.dat", "POSTPRO/fnmean_xz.dat", "POSTPRO/fnmean_yz.dat"},
								    {"POSTPRO/ftmean_xy.dat", "POSTPRO/ftmean_xz.dat", "POSTPRO/ftmean_yz.dat"} }; 
      for (int dir = DIR::N; dir < DIR::TOTALDIR; ++dir) {
	for (int plane = PLANE::XY; plane < PLANE::TOTALPLANE; ++plane) {
	  fout.open(fname[dir][plane]); fout.print("# angle", "fmean(angle)", "fmean_x", "fmean_y");
	  for (int bin = 0; bin < int(fsum_histo_[dir][plane].size()); ++bin) {
	    const double angle = min_ + bin*delta_angle_;
	    const double val = (nc_[dir][plane][bin] > 0) ? fsum_histo_[dir][plane][bin].norm()/nc_[dir][plane][bin] : 0;
	    ASSERT(nc_[dir][plane][bin] >= 0);
	    fout.print(angle, val, val*std::cos(angle), val*std::sin(angle));
	  }
	  // pi-mirror image printing
	  for (int bin = 0; bin < int(fsum_histo_[dir][plane].size()); ++bin) {
	    const double angle = min_ + bin*delta_angle_ + M_PI;
	    const double val = (nc_[dir][plane][bin] > 0) ? fsum_histo_[dir][plane][bin].norm()/nc_[dir][plane][bin] : 0;
	    ASSERT(nc_[dir][plane][bin] >= 0);
	    fout.print(angle, val, val*std::cos(angle), val*std::sin(angle));
	  }
	  fout.close();
	}
      }
      // print orientation pdf
      const std::string fn[DIR::TOTALDIR][PLANE::TOTALPLANE] = { {"POSTPRO/fnpdf_xy.dat", "POSTPRO/fnpdf_xz.dat", "POSTPRO/fnpdf_yz.dat"},
								 {"POSTPRO/ftpdf_xy.dat", "POSTPRO/ftpdf_xz.dat", "POSTPRO/ftpdf_yz.dat"} }; 
      for (int dir = DIR::N; dir < DIR::TOTALDIR; ++dir) {
	for (int plane = PLANE::XY; plane < PLANE::TOTALPLANE; ++plane) {
	  fout.open(fn[dir][plane]); fout.print("# angle", "pdf(angle)");
	  const double nctotal = std::accumulate(nc_[dir][plane].begin(), nc_[dir][plane].end(), 0.0); ASSERT(nctotal >= 0);
	  for (int bin = 0; bin < int(fsum_histo_[dir][plane].size()); ++bin) {
	    const double angle = min_ + bin*delta_angle_;
	    const double val = (nctotal > 0) ? nc_[dir][plane][bin]/(nctotal*delta_angle_) : 0;
	    fout.print(angle, val);
	  }
	  // pi-mirror image
	  for (int bin = 0; bin < int(fsum_histo_[dir][plane].size()); ++bin) {
	    const double angle = min_ + bin*delta_angle_ + M_PI;
	    const double val = (nctotal > 0) ? nc_[dir][plane][bin]/(nctotal*delta_angle_) : 0;
	    fout.print(angle, val);
	  }
	  fout.close();
	}
      }
      // print tensors
      for (int dir = DIR::N; dir < DIR::TOTALDIR; ++dir) {
	for (int plane = PLANE::XY; plane < PLANE::TOTALPLANE; ++plane) {
	  H_fout_[dir][plane].print_val(time);
	  for (int ix = 0; ix <= 1; ++ix) {
	    for (int iy = 0; iy <= 1; ++iy) {
	      H_fout_[dir][plane].print_val(H_[dir][plane](ix, iy));
	    }
	  }
	  H_fout_[dir][plane].print_newline();
	}
      }

    } // print

  } // namespace POSTPRO
} // namespace DEM
