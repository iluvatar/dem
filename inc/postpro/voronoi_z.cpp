// Computes compactivity and related for particles with 0, 1, 2, 3, 4,
// 5, ... contacts

#include "postpro/voronoi_z.hpp"

namespace DEM {
  namespace POSTPRO {
    
    int VoronoiZ::init_tasks(const int & ngrains, const double & d, const int & dim) {
      const std::string msg = "Init tasks for VoronoiZ"; UTIL::print_start(msg);
      ngrains_ = ngrains;
      d_ = d; d3_ = d_*d_*d_;
      Vmin_ = UTIL::voronoi_vmin(dim); // normalized by d_^D // WARNING: Actual value could be smaller because of interpenetrations 
      int status = std::system("mkdir POSTPRO/voronoi_z"); 
      if (0 != status) print_warning("Possible error creating directory POSTPRO/voronoi_z");
      
      // histograms
      for(int ii = 0; ii < NZ_; ++ii) {
	std::stringstream name; name << "voronoi_z - " << ii;
	histo_[ii].construct(0.9*Vmin_, 2.1*Vmin_, 50, name.str());
      }

      ready_ = true;
      UTIL::print_done(msg);
      return EXIT_SUCCESS;					                
    }
    

    int VoronoiZ::print_data() {
      const std::string msg = "Printing voronoi data per z."; UTIL::print_start(msg);
      
      std::stringstream ss; 
      for(int ii = 0; ii < NZ_; ++ii) {
	ss.str(""); ss << "POSTPRO/voronoi_z/P_z_" << std::setfill('0') << std::setw(3) << ii << ".dat";
	histo_[ii].print(ss.str()); 
	ss.str(""); ss << "POSTPRO/voronoi_z/P_z_" << std::setfill('0') << std::setw(3) << ii << "_scaled.dat";
	print_scaled_histo_multivoro(histo_[ii], 1, ss.str()); 
	ss.str(""); ss << "POSTPRO/voronoi_z/P_z_" << std::setfill('0') << std::setw(3) << ii << "_mean_sigma.dat";
	histo_[ii].print_mean_sigma(ss.str()); 
      }
      
      // set print file names and open streams
      std::stringstream name;
      name.str(""); name << "POSTPRO/z-voronoi_X.dat"   ; X_fout_.open(name.str());
      name.str(""); name << "POSTPRO/z-voronoi_K.dat"   ; K_fout_.open(name.str());
      name.str(""); name << "POSTPRO/z-voronoi_KuN.dat" ; KuN_fout_.open(name.str());
      name.str(""); name << "POSTPRO/z-voronoi_S.dat"   ; S_fout_.open(name.str());
      name.str(""); name << "POSTPRO/z-voronoi_SuK.dat" ; SuK_fout_.open(name.str());
      name.str(""); name << "POSTPRO/z-voronoi_CuN.dat" ; CuN_fout_.open(name.str());
      name.str(""); name << "POSTPRO/z-ngrains.dat" ; ngrains_z_fout_.open(name.str());

      // print stat mech vars
      for (int ii = 0; ii < NZ_; ++ii) {
	X_fout_.print(  ii, X_[ii]);
	K_fout_.print(  ii, K_[ii]);
	KuN_fout_.print(ii, KuN_[ii]);
	S_fout_.print(  ii, S_[ii]);
	SuK_fout_.print(ii, SuK_[ii]);
	CuN_fout_.print(ii, CuN_[ii]);
	ngrains_z_fout_.print(ii, ngrains_z_[ii]);
      }
      
      // close streams
      X_fout_.close();
      K_fout_.close();
      KuN_fout_.close();
      S_fout_.close();
      SuK_fout_.close();
      CuN_fout_.close();
      ngrains_z_fout_.close();

      UTIL::print_done(msg);
      return EXIT_SUCCESS;
    }
    
    int VoronoiZ::end_tasks() {
      print_data();
      return EXIT_SUCCESS; 
    }
    // ---------------------------------------------------------------------
    // AUXILIARY FUNCTIONS
    // ---------------------------------------------------------------------

  void VoronoiZ::print_scaled_histo_multivoro(Histogram1D & histo, const double & size, const std::string & filename) 
  {
    // compute voronoi parameters
    double Vmean, Vsigma; 
    histo.mean_and_sigma(Vmean, Vsigma);
    const double Vmin = size*Vmin_;
    double mx = 1.0, bx = 0.0, my = 1.0, by = 0.0;
    if (Vmean > Vmin) {
      mx = 1.0/(Vmean-Vmin), bx = -Vmin/(Vmean-Vmin), my = Vmean-Vmin, by = 0.0;
    }
    else {
      print_warning("voronoi multi: Vmean < Vmin");
      print_warning("size  = ", size);
      print_warning("Vmean = ", Vmean);
      print_warning("Vmin  = ", Vmin);
    }
    // print
    histo.print(filename.c_str(), mx, bx, my, by);
  }

    
    
  } // namespace POSTPRO
} // namespace DEM


