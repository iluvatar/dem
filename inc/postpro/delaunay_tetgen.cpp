// Computes the delaunay tessellation for a sample

#include "postpro/delaunay_tetgen.hpp"

namespace DEM {
  namespace POSTPRO {
    //----------------------------------------------------------    
    //----------------------------------------------------------
    int Delaunay::init_tasks(const double & minrad, const double & maxrad) {
      Xout_.open("POSTPRO/delaunay_X.dat"); 
      Kout_.open("POSTPRO/delaunay_K.dat"); 
      Sout_.open("POSTPRO/delaunay_S.dat"); 
      SuKout_.open("POSTPRO/delaunay_SuK.dat"); 
      vmineleout_.open("POSTPRO/delaunay_vmin_elemental.dat"); 
      Xuvmineleout_.open("POSTPRO/delaunay_Xuvminelemental.dat"); 
      Vmean_out_.open("POSTPRO/delaunay_Vmean.dat"); 
      Vsigma_out_.open("POSTPRO/delaunay_Vsigma.dat"); 
      Vsigma2_out_.open("POSTPRO/delaunay_Vsigma2.dat"); 
      Vmin_out_.open("POSTPRO/delaunay_Vmin.dat"); 
      Cout_.open("POSTPRO/delaunay_C.dat"); 
      CuNout_.open("POSTPRO/delaunay_CuN.dat"); 
      VT_out_.open("POSTPRO/delaunay_VT.dat"); 
      ncells_out_.open("POSTPRO/delaunay_ncells.dat"); 
      phi_out_.open("POSTPRO/delaunay_phi.dat"); 
      d_    = 2*minrad;
      Vmin_ = M_SQRT2/12.0; // (d^3), CONSTANT, depends on dim (here 3D)
      histo_.construct(0.3*Vmin_, 3.0*Vmin_*CUBE(maxrad/minrad), 70);
      histo_scaled_.construct(-0.3, 3.1, 80);
      histo_full_.construct(0.01*Vmin_, 4.5*Vmin_, 250);
      return EXIT_SUCCESS; 
    }

    //----------------------------------------------------------    
    //----------------------------------------------------------    
    int Delaunay::end_tasks() {
      Xout_.close();
      Kout_.close();
      SuKout_.close();
      vmineleout_.close();
      Xuvmineleout_.close();
      Vmean_out_.close();
      Vsigma_out_.close();
      Vsigma2_out_.close();
      Vmin_out_.close();
      Cout_.close();
      CuNout_.close();
      VT_out_.close();
      ncells_out_.close();
      phi_out_.close();
      return EXIT_SUCCESS; 
    }


  } // namespace POSTPRO
} // namespace DEM
