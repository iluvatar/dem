// Computes stat mech vars per material inside sample

#ifndef __STATMECH_MATERIAL__
#define __STATMECH_MATERIAL__

#include "postpro_base_functor.hpp"
#include "../file_util.hpp"
#include "../utility.hpp"
#include "../histogram1d.hpp"
#include "../particle.hpp"
#include <vector>
#include <algorithm>
#include <string>
#include <sstream>
#include <map>

namespace DEM {
  namespace POSTPRO {
    
    class StatMechMaterials : public Base_functor {
    public :
      StatMechMaterials() {};
      ~StatMechMaterials() { print_log("StatMechMaterials destructor called"); /*end_tasks();*/ }
      int 
      init_tasks(const std::vector<Particle> & particles, const int & ngrains, const double & d, const int & dim) ;
      int end_tasks() ;
      int print_data();
      template <class ParticleList_t, class IndexList_t> 
      int 
      operator()(const ParticleList_t & particles,		   // list of particles
		 const int & dim,				   // dimension of simulation (2 or 3)
		 const int & ncmin,				   // minimum number of contacts
		 const IndexList_t & filtered_ids,		   // filtered ids (for example, inside volume)
		 const std::vector<double> & voronoi_volumes)	   // voronoi volumes 
      {
	UTIL::print_start("Computing statmech voronoi parameters per material tag");
	ASSERT(true == ready_);
	ASSERT(2 == dim || 3 == dim);
	ASSERT(ncmin >= 0);
      
	print_warning("Using all internal particles, regardless their contact number per particle");

	// accumulate voronoi volumes per tag
	double Vtotal = 0;
	std::fill(ngrains_tag_.begin(), ngrains_tag_.end(), 0);
	for (auto & id : filtered_ids) {
	  // if (particles[id].nc < ncmin) continue; // use only internal with enough contacts
	  const int mtag = particles[id].materialTag;
	  histos_vorovol_[tag_index_[mtag]].increment(voronoi_volumes[id]); 
	  ++ngrains_tag_[tag_index_[mtag]];
	  Vtotal += voronoi_volumes[id];
	}
	UTIL::print_done("Accumulating volumes on histograms");
      
	// compute stat mech parameters per tag
	const double lambda_cube = M_E/100.0;; // = e (pi d^3/6 phiRCP - Vmin)/kRCP
	for (int itag = 0; itag < ntags_; ++itag) {
	  voro_X_[itag] = voro_K_[itag] = voro_S_[itag] = voro_CuN_[itag] = 0;
	  double Vmean = 0, Vsigma = 0;
	  histos_vorovol_[itag].mean_and_sigma(Vmean, Vsigma);
	  if (Vsigma > 0 && (Vmean > Vmin_)) {
	    voro_X_[itag] = UTIL::statmech_X(Vmean, Vsigma, Vmin_);
	    voro_K_[itag] = UTIL::statmech_K(Vmean, Vsigma, Vmin_);
	    voro_S_[itag] = UTIL::statmech_S(Vmean, voro_K_[itag], Vmin_, lambda_cube); 
	    if (ngrains_tag_[itag] > 0) voro_CuN_[itag] = voro_K_[itag]*Vtotal/(Vmean*ngrains_tag_[itag]); 
	    print_log("Vmin   = ", Vmin_);
	    print_log("Vmean  = ", Vmean);
	    print_log("Vsigma = ", Vsigma);
	    print_log("X      = ", voro_X_[itag]);
	  }
	}
	UTIL::print_done("Computing stat mech vars per tag");

	// print
	print_data();
      
	UTIL::print_done("Computing stat mech vars per tag");
	return EXIT_SUCCESS; 
      }
    private :
      std::vector<Histogram1D> histos_vorovol_;			   // Histograms of voronoi volumes per tag
      std::vector<int> tags_;                                      // Actual tags
      std::vector<double> voro_X_, voro_K_, voro_S_, voro_CuN_;    // Stat mech values per tag
      std::map<int, int> tag_index_;				   // index for each tag, for reference in the arrays
      int ntags_ = 0;						   // Number of tags
      int ngrains_ = 0;						   // Number of grains
      std::vector<int> ngrains_tag_;				   // Number of grains per tag
      double d_ = 0, d3_ = 0;					   // diammeter, d^3
      double Vmin_ = 1.0e100;					   // Minimum voronoi volume
      bool ready_ = false;                                         // flag to assert system ready
      std::vector<UTIL::OFileStream> voro_X_fout_, voro_K_fout_, 
	voro_S_fout_, voro_CuN_fout_; // size ntags_
    };


  } // namespace POSTPRO
} // namespace DEM

#endif // __STATMECH_MATERIAL__
