// Computes stat mech vars for a random radical voronoi tessellation

#ifndef __RANDOM_RADICAL_VORONOI__
#define __RANDOM_RADICAL_VORONOI__

#include "postpro_base_functor.hpp"
#include "../file_util.hpp"
#include "../utility.hpp"
#include "../histogram1d.hpp"
#include "../random.hpp"
#include "../particle.hpp"
#include <vector>
#include <algorithm>
#include <string>
#include <sstream>

namespace DEM {
  namespace POSTPRO {
    
    class RandomRadicalVoronoi : public Base_functor {
    public :
      RandomRadicalVoronoi() {};
      ~RandomRadicalVoronoi() { print_log("Random Radical Voronoi destructor called"); /*end_tasks();*/ }
      int init_tasks(const int & ngrains, const double & d, const int & dim) ;
      int end_tasks() ;
      int print_data();
      template <class IndexList_t> 
      int 
      operator()(const std::vector<Particle> & particles, 
		 const double wmin[3], const double wmax[3], 
		 const bool periodic[3], const int & dim, const int & ncmin, 
		 const IndexList_t & filtered_ids)
      {
	UTIL::print_start("Computing statmech voronoi parameters for Random Radical Voronoi Tessellation");
	ASSERT(true == ready_);
	ASSERT(2 == dim || 3 == dim);
	ASSERT(ncmin >= 0);
      
	print_warning("Using all internal particles, regardless their contact number per particle");
      
	// memory arrays
	std::vector<double> voronoi_volumes(ngrains_); 
	std::vector<Vec3d_t> points(ngrains_); 
	std::vector<double> weights(ngrains_); 
	std::vector<Vec3d_t> centroids(ngrains_);
	std::vector<std::vector<int> > neigh(ngrains_);
	// fill points information
	for(int idx = 0; idx < ngrains_; ++idx) {
	  points[idx]  = particles[idx].R;
	  weights[idx] = std::max(0.1, ranmt_.gauss(1.0, 0.1))*d_/2.0; // gaussian around 1, sigma = 0.2
	}
     
	// tessellate 
	UTIL::compute_voronoi_volumes(points, weights, wmin, wmax, periodic, dim, 
				      voronoi_volumes, centroids, neigh, false);

	// Normalize volume 
	for(auto & vol : voronoi_volumes) vol /= d3_; // ALWAYS Normalize
      
	UTIL::print_done("Relaxing Random Radical Voronoi.");
      
	// accumulate voronoi volumes 
	double Vtotal = 0;
	for (auto & id : filtered_ids) {
	  // if (particles[id].nc < ncmin) continue; // use only internal with enough contacts
	  histo_vorovol_.increment(voronoi_volumes[id]); 
	  Vtotal += voronoi_volumes[id];
	}
	UTIL::print_done("Accumulating volumes on histograms.");
      
	// compute stat mech parameters 
	const double lambda_cube = M_E/100.0;; // = e (pi d^3/6 phiRCP - Vmin)/kRCP
	voro_X_ = voro_K_ = voro_S_ = voro_CuN_ = 0;
	double Vmean = 0, Vsigma = 0;
	histo_vorovol_.mean_and_sigma(Vmean, Vsigma);
	if (Vsigma > 0 && (Vmean > Vmin_)) {
	  voro_X_ = UTIL::statmech_X(Vmean, Vsigma, Vmin_);
	  voro_K_ = UTIL::statmech_K(Vmean, Vsigma, Vmin_);
	  voro_S_ = UTIL::statmech_S(Vmean, voro_K_, Vmin_, lambda_cube); 
	  if (filtered_ids.size() > 0) voro_CuN_ = voro_K_*Vtotal/(Vmean*filtered_ids.size()); 
	  print_log("Vmin   = ", Vmin_);
	  print_log("Vmean  = ", Vmean);
	  print_log("Vsigma = ", Vsigma);
	  print_log("X      = ", voro_X_);
	}
	UTIL::print_done("Computing stat mech vars for Random Radical Voronoi");

	// print
	print_data();
      
	UTIL::print_done("Computing stat mech vars for Random Radical Voronoi");
	return EXIT_SUCCESS; 
      }

    private :
      Random ranmt_;		                                   // random generator - mersenne twister 
      Histogram1D histo_vorovol_;			           // Histograms of voronoi volumes 
      double voro_X_=0, voro_K_=0, voro_S_=0, voro_CuN_=0;                 // Stat mech values
      int ngrains_ = 0;						   // Number of grains
      double d_ = 0, d3_ = 0;					   // diammeter, d^3
      double Vmin_ = 1.0e100;					   // Minimum voronoi volume
      bool ready_ = false;                                         // flag to assert system ready
      UTIL::OFileStream voro_X_fout_, voro_K_fout_, 
	voro_S_fout_, voro_CuN_fout_; 
    };

  } // namespace POSTPRO
} // namespace DEM

#endif // __RANDOM_RADICAL_VORONOI__
