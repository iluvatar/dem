// Computes the amount of crystallization for a sample

#include "postpro/crystallization.hpp"

namespace DEM {
  namespace POSTPRO {
    
    int Crystallization::init_tasks() {
      lvals_.resize(2); qvals_.resize(2);
      lvals_[0] = 4;
      lvals_[1] = 6;
      q4out_.open("POSTPRO/q4.dat");
      q6out_.open("POSTPRO/q6.dat");
      crystalout_.open("POSTPRO/crystalized.dat");
      ready_ = true;
      return EXIT_SUCCESS;
    }
    
    int Crystallization::operator()(const double & time, const std::vector<Contact> & contacts) {
      UTIL::print_start("Crystallization");
      ASSERT(true == ready_);

      // init vals
      std::fill(qvals_.begin(), qvals_.end(), 0);
      std::vector<std::complex<double> > cdata; // stores the sph sum per contact
      std::vector<double> ddata; // stores the sph sum per contact
      print_log("     Done :: declaring init values and filling with 0");
      
      // compute each q_l
      if (contacts.size() >= 1) {
	const int nlvals = lvals_.size();
	for (int ii = 0; ii < nlvals; ++ii) {
	  const int l = lvals_[ii];
	  cdata.resize(2*l + 1); std::fill(cdata.begin(), cdata.end(), 0);
	  for (const auto & c : contacts) {
	    const double theta = std::acos (c.Normal_[2]);
	    const double phi   = std::atan2(c.Normal_[1], c.Normal_[0]);	
	    for (int m = -l; m <= l; ++m) {
	      cdata[m + l] += UTIL::spherical_harmonic(l, m, theta, phi);
	    }
	  }
	  ddata.resize(cdata.size());
	  const int ncdata = cdata.size();
	  for (int idx = 0; idx < ncdata; ++idx) {
	    ddata[idx] = std::norm(cdata[idx]);
	  }
	  qvals_[ii] = std::sqrt(4*M_PI*std::accumulate(ddata.begin(), ddata.end(), 0.0)/((2*l + 1)*(std::pow(contacts.size(), 2))));
	}
      }
      print_log("     Done :: Computing q4 and q6");

      // print 
      q4out_.print(time, qvals_[0]);
      q6out_.print(time, qvals_[1]);
      print_log("     Done :: printing");
      
      // check if crystallization
      static const double MIN_Q4_HCP = 0.08;
      static const double MAX_Q4_HCP = 0.16;
      static const double MIN_Q4_FCC = 0.175;
      static const double MAX_Q4_FCC = 1.0e30;
      static const double MIN_Q6_HCP = 0.46;
      static const double MAX_Q6_HCP = 0.50;
      static const double MIN_Q6_FCC = 0.54;
      static const double MAX_Q6_FCC = 1.0e30;
      int crystallized = 0;
      crystalout_.print_val(time); 
      print_debug("qvals_4 = ", qvals_[0]);
      print_debug("qvals_6 = ", qvals_[1]);
      if ((UTIL::is_inside(qvals_[0], MIN_Q4_HCP, MAX_Q4_HCP) && UTIL::is_inside(qvals_[1], MIN_Q6_HCP, MAX_Q6_HCP)) || 
	  (UTIL::is_inside(qvals_[0], MIN_Q4_FCC, MAX_Q4_FCC) && UTIL::is_inside(qvals_[1], MIN_Q6_FCC, MAX_Q6_FCC)) ) {
	crystallized = 1;
      }
      crystalout_.print(crystallized);
      print_debug("Crystallized = ", crystallized);
      print_log("     Done :: Checking if crystallized.");
            
      UTIL::print_done("Crystallization");
      return crystallized; 
    }

    int Crystallization::end_tasks() {
      q4out_.close();
      q6out_.close();
      crystalout_.close();
      return EXIT_SUCCESS; 
    }

  } // namespace POSTPRO
} // namespace DEM
