// Computes stat mech vars for a centroidal voronoi tessellation

#include "centroidal_voronoi_tessellation.hpp"

namespace DEM {
  namespace POSTPRO {
    
    int CentroidalVoronoi::init_tasks(const int & ngrains, const double & d, const int & dim) {
      const std::string msg = "Init tasks for CentroidalVoronoi"; UTIL::print_start(msg);
      ngrains_ = ngrains;
      d_ = d; d3_ = d_*d_*d_;
      Vmin_ = UTIL::voronoi_vmin(dim); // normalized by d_^D // WARNING: Actual value could be smaller 

      // construct histograms
      histo_vorovol_.construct(0.65, 1.53544920809, 80); // min, max, nbins
      histo_vorovol_scaled_.construct(0.0, 2.53544920809, 80); // min, max, nbins
      
      // set print file names
      std::stringstream name;
      name.str(""); name << "POSTPRO/centroidal_voronoi-voronoi_X.dat"; 
      voro_X_fout_.open(name.str());
      name.str(""); name << "POSTPRO/centroidal_voronoi-voronoi_K.dat"; 
      voro_K_fout_.open(name.str());
      name.str(""); name << "POSTPRO/centroidal_voronoi-voronoi_S.dat"; 
      voro_S_fout_.open(name.str());
      name.str(""); name << "POSTPRO/centroidal_voronoi-voronoi_SuK.dat"; 
      voro_SuK_fout_.open(name.str());
      name.str(""); name << "POSTPRO/centroidal_voronoi-voronoi_CuN.dat"; 
      voro_CuN_fout_.open(name.str());
      name.str(""); name << "POSTPRO/centroidal_voronoi-voronoi_Vmean.dat"; 
      cvt_Vmean_fout_.open(name.str());
      name.str(""); name << "POSTPRO/centroidal_voronoi-voronoi_Vmin.dat"; 
      cvt_Vmin_fout_.open(name.str());

      ready_ = true;
      
      UTIL::print_done(msg);
      return EXIT_SUCCESS;					                
    }
    

    int CentroidalVoronoi::print_data() {
      UTIL::print_start("Printing statmech data for Centroidal Voronoi.");
      // print stat mech vars
      voro_X_fout_.print(voro_X_);
      voro_K_fout_.print(voro_K_);
      voro_S_fout_.print(voro_S_);
      voro_SuK_fout_.print(voro_S_/voro_K_);
      voro_CuN_fout_.print(voro_CuN_);
      cvt_Vmean_fout_.print(cvt_Vmean_);
      cvt_Vmin_fout_.print(Vmin_);

      // print histogram
      std::stringstream name;
      name.str(""); name << "POSTPRO/centroidal_voronoi-histoVoroVol.dat";
      histo_vorovol_.print(name.str());
      // scaled version
      name.str(""); name << "POSTPRO/centroidal_voronoi-histoVoroVol_scaled.dat";
      histo_vorovol_scaled_.print(name.str());
      // double Vmean = 0, Vsigma = 0;
      // histo_vorovol_.mean_and_sigma(Vmean, Vsigma);
      // double mx = 1.0, bx = 0.0, my = 1.0, by = 0.0;
      // if (Vmean > Vmin_) {
      // 	mx = 1.0/(Vmean-Vmin_), bx = -Vmin_/(Vmean-Vmin_), my = Vmean-Vmin_, by = 0.0;
      // }
      // name.str(""); name << "POSTPRO/centroidal_voronoi-histoVoroVol_scaled.dat";
      // histo_vorovol_.print(name.str(), mx, bx, my, by);

      UTIL::print_done("Printing statmech data for CVT.");
      return EXIT_SUCCESS;
    }
    
    int CentroidalVoronoi::end_tasks() {
      print_data();
      return EXIT_SUCCESS; 
    }

  } // namespace POSTPRO
} // namespace DEM
