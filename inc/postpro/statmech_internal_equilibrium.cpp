// Computes statmech variables, like X, at different scale levels

#include "postpro/statmech_internal_equilibrium.hpp"

namespace DEM {
  namespace POSTPRO {
    
    // NOTE : Using the last particle state to set up limits
    int StatMechInternal::init_tasks(const std::vector<Particle> & particles,
				     const int & ngrains, 
				     const double & dmin, 
				     const double & dmax, 
				     const int & dim) {
      ngrains_ = ngrains;
      d_ = dmin; d3_ = d_*d_*d_;
      Vmin_ = UTIL::voronoi_vmin(dim)*6/M_PI;			   // normalized by d^3
      Vmax_ = UTIL::voronoi_vmin(dim)*6*CUBE(dmax/dmin)/M_PI;      // normalized by d^3
      
      const Vec3d_t RT(particles[ngrains_+RIGHT].x(), particles[ngrains_+FRONT].y(), particles[ngrains_+TOP   ].z());
      const Vec3d_t LB(particles[ngrains_+LEFT ].x(), particles[ngrains_+BACK ].y(), particles[ngrains_+BOTTOM].z());

      //// create coronas with a least a widht = 2d
      //UTIL::compute_coronas_limits_constant_volume(LB, RT, 2.0*d, dim, ncoronas_, corona_leftbottom_, corona_righttop_);
      // create coronas with a constant widht = 1.5d, changing volume
      ncoronas_ = std::max(int(UTIL::min_component(RT-LB, dim)/(2.0*d_)), 2); // suggestion, since width is bit necesarily a divisor of Lmin
      UTIL::compute_coronas_limits_constant_width(LB, RT, dim, ncoronas_, corona_leftbottom_, corona_righttop_);

      voro_X_.resize(ncoronas_);
      voro_K_.resize(ncoronas_);
      histo_voro_vol_.resize(ncoronas_);
      for (int im = 0; im < ncoronas_; ++im) {
	histo_voro_vol_[im].construct(0.2*Vmin_, 2.0*Vmax_, 50);
      }

      ready_ = true;						                            
      return EXIT_SUCCESS;					                
    }
    
    int 
    StatMechInternal::operator()(const std::vector<Particle> & particles, 
				 const int & dim, const int & ncmin, 
				 const std::vector<double> & voronoi_volumes) {
      UTIL::print_start("Computing internal-domains statistical mechanics variables");
      ASSERT(true == ready_);
      ASSERT(2 == dim || 3 == dim);
      
      // use voronoi volumes
      std::vector<double> volumes = voronoi_volumes;
      volumes.erase(volumes.begin() + ngrains_, volumes.end()); // remove walls data = 0.0

      // add voro-volume of particle to its corona
      // std::vector<std::set<int> > ids_per_corona(ncoronas_);
      for (const auto & body : particles ) {
	if (body.uid >= ngrains_) continue; // ignore walls
	if (body.nc < ncmin) continue; 
	bool imfound = false;
	for (int im = 0; im < ncoronas_; ++im) {
	  if ( UTIL::is_vector_inside(body.R, corona_leftbottom_[im], corona_righttop_[im]) ) {
	    histo_voro_vol_[im].increment(volumes[body.uid]); 
	    imfound = true;
	    break;
	  }
	}
	if (false == imfound) {
#if DEBUG_STATMECH_INTERNAL_EQUI
	  print_error("Internal Equilibrium :: Particle not found in any corona.");
	  print_error("ngrains = ", ngrains_);
	  print_error("id = ", body.uid);
	  print_error("R = ", body.R);
	  print_error("LBmax = ", corona_leftbottom_[ncoronas_ - 1]);
	  print_error("RTmax = ", corona_righttop_[ncoronas_ - 1]);
	  print_error("left   = ", particles[ngrains_ + LEFT  ].x());
	  print_error("right  = ", particles[ngrains_ + RIGHT ].x());
	  print_error("back   = ", particles[ngrains_ + BACK  ].y());
	  print_error("front  = ", particles[ngrains_ + FRONT ].y());
	  print_error("bottom = ", particles[ngrains_ + BOTTOM].z());
	  print_error("top    = ", particles[ngrains_ + TOP   ].z());
#endif // DEBUG_STATMECH_INTERNAL_EQUI
	}
	//ASSERT(true == imfound);
      }
      UTIL::print_done("voronoi volume on each corona histogram ...");
      
      // compute stat mech vars per corona
      double vmin = UTIL::voronoi_vmin(dim);
      ASSERT(vmin > 0.0);
      for (int im = 0; im < ncoronas_; ++im) {
	double mean_voro, sigma_voro;
	histo_voro_vol_[im].mean_and_sigma(mean_voro, sigma_voro);
	if (mean_voro > vmin && sigma_voro > 0) {
	  voro_K_[im] = UTIL::statmech_K(mean_voro, sigma_voro, vmin);
	  voro_X_[im] = UTIL::statmech_X(mean_voro, sigma_voro, vmin);
	}
      }      
      UTIL::print_done("voronoi statmech per corona.");

      // print data
      print_data();

      UTIL::print_done("Computing internal stat mech vars");
      return EXIT_SUCCESS; 
    }

    int  StatMechInternal::print_data() {
      UTIL::print_start("Printing internal (coronas) statmech data");
      voro_X_out_.open("POSTPRO/statmech_internal_voronoi_X.dat");
      voro_K_out_.open("POSTPRO/statmech_internal_voronoi_K.dat");
      for (int im  = 0; im < ncoronas_; ++im) {
	const double x = corona_righttop_[im][0]/d_;
	voro_X_out_.print(x, voro_X_[im]);
	voro_K_out_.print(x, voro_K_[im]);
      }
      voro_X_out_.close();
      voro_K_out_.close();
      for (int im = 0; im < ncoronas_; ++im) {
	std::stringstream name; 
	name << "POSTPRO/statmech_internal_histo_voro_vol-" << std::setfill('0') << std::setw(2) << im << ".dat";
	histo_voro_vol_[im].print(name.str());
      }

      UTIL::print_done("Printing internal (coronas) statmech data");
      return EXIT_SUCCESS;
    }
    
    int StatMechInternal::end_tasks() {
      print_data();
      return EXIT_SUCCESS; 
    }

  } // namespace POSTPRO
} // namespace DEM

