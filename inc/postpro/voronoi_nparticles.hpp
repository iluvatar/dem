// Computes compactivity as function of the number of particles in the statistics

#ifndef __VORONOI_NPARTICLES__
#define __VORONOI_NPARTICLES__

#include "postpro_base_functor.hpp"
#include "../file_util.hpp"
#include "../utility.hpp"
#include "../histogram1d.hpp"
#include "../functions.hpp"
#include <vector>
#include <algorithm>
#include <string>
#include <sstream>

namespace DEM {
  namespace POSTPRO {
    
    class VoronoiNparticles : public Base_functor {
    public :
      VoronoiNparticles() {};
      ~VoronoiNparticles() { print_log("VoronoiNparticles destructor called"); /*end_tasks();*/ }
      int init_tasks(const int & ngrains, const double & d, const int & dim) ;
      int end_tasks() ;
      int print_data();
      template <class IndexList_t> 
      int 
      operator()(const int & dim,				   // dimension of simulation (2 or 3)
		 const int & ncmin,				   // minimum number of contacts
		 const IndexList_t & filtered_ids,		   // filtered ids (for example, inside volume)
		 const std::vector<double> & voronoi_volumes)	   // voronoi volumes 
      {
	const std::string msg = "Computing voronoi multiscale variables"; UTIL::print_start(msg);
	ASSERT(true == ready_);
	ASSERT(2 == dim || 3 == dim);
	ASSERT(ncmin >= 0);
	print_warning("Using all internal particles, regardless their contact number per particle");
      
	// accumulate voronoi volumes
	// groups size <= filtered_ids.size()
	int counter = 1;
	const int nfgrains = filtered_ids.size();
	for(const auto & id : filtered_ids) {
	  const double vol = voronoi_volumes[id];
	  for(int ii = 0; ii < NCLUSTER_; ++ii) {
	    if (ngrains_group_[ii] > nfgrains) continue;
	    if (counter <= ngrains_group_[ii]) {
	      histo_[ii].increment(vol);
	    }  
	  }	
	  ++counter;
	}
	// groups size > filtered_ids.size()
	counter = 0;
	for(int id = 0; id < ngrains_; ++id) {
	  const double vol = voronoi_volumes[id];
	  for(int ii = 0; ii < NCLUSTER_; ++ii) {
	    if (ngrains_group_[ii] <= nfgrains) continue;
	    if (counter <= ngrains_group_[ii]) {
	      histo_[ii].increment(vol);
	    }  
	  }	
	  ++counter;
	}
	//histo_[NCLUSTER_-1].increment(voronoi_volumes);
	UTIL::print_done("Accumulating multi-scale volumes on histograms");
      
	// compute stat mech parameters per group
	const double lambda_cube = M_E/100.0;; // = e (pi d^3/6 phiRCP - Vmin)/kRCP
	for (int ii = 0; ii < NCLUSTER_; ++ii) {
	  X_[ii] = K_[ii] = S_[ii] = CuN_[ii] = SuK_[ii] = 0;
	  double Vmean = 0, Vsigma = 0;
	  histo_[ii].mean_and_sigma(Vmean, Vsigma);
	  if (Vsigma > 0 && (Vmean > Vmin_)) {
	    X_[ii] = UTIL::statmech_X(Vmean, Vsigma, Vmin_);
	    K_[ii] = UTIL::statmech_K(Vmean, Vsigma, Vmin_);
	    KuN_[ii] = K_[ii]/ngrains_group_[ii]; 
	    S_[ii] = UTIL::statmech_S(Vmean, K_[ii], Vmin_, lambda_cube); 
	    SuK_[ii] = (K_[ii] > 0) ? S_[ii]/K_[ii] : 0.0; 
	    CuN_[ii] = 0.0; // K_[ii]*ngrains_bin_[ii]/ngrains_;  // WARNING: How much is this ??????
	  }
	}
	UTIL::print_done("Computing voronoi stat mech per group");

	// print
	print_data();
      
	UTIL::print_done(msg);
	return EXIT_SUCCESS; 
      }
    
    private : // methods
      void print_scaled_histo_multivoro(Histogram1D & histo, const double & size, const std::string & filename);
      
    private : // data
      int NCLUSTER_ = 0;					   // Number of clusters, uniform partition 
      std::vector<Histogram1D> histo_; 
      std::vector<int> ngrains_group_;
      std::vector<double> X_, K_, KuN_, S_, CuN_, SuK_, VT_;       // Stat mech values per bin
      int ngrains_ = 0;						   // Number of grains
      double d_ = 0, d3_ = 0;					   // diammeter, d^3
      double Vmin_ = 1.0e100;					   // Minimum voronoi volume
      bool ready_ = false;                                         // flag to assert system ready
      UTIL::OFileStream X_fout_, K_fout_, KuN_fout_, SuK_fout_,
	S_fout_, CuN_fout_, VT_fout_; 
    };

    
  } // namespace POSTPRO
} // namespace DEM

#endif // __VORONOI_NPARTICLES__

