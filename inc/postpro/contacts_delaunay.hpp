// Computes the delaunay tessellation for a sample from the contacts positions

#ifndef __CONTACTS_DELAUNAY_HPP__
#define __CONTACTS_DELAUNAY_HPP__

#include "postpro_base_functor.hpp"
#include "../histogram1d.hpp"
#include "../nlfit.hpp"
#include "../functions.hpp"
#include "../file_util.hpp"
#include "../vector3d.hpp"
#include "tetgen.h"

namespace DEM {
  namespace POSTPRO {
    //----------------------------------------------------------
    // Class ContactsDelaunay : performs delaunay tessellations from contcts positions and computes SM quantities
    //----------------------------------------------------------
    class ContactsDelaunay : public Base_functor {
    public :
      ContactsDelaunay() {};
      int init_tasks(const double & minrad) ;
      int end_tasks () ;
      int operator()(const std::vector<Vec3d_t> & cpoints) ;
      double total_volume() { return total_vol_; };
      double ntetra()       { return ntetra_; };
    private :
      UTIL::OFileStream Xout_, Kout_, Sout_, SuKout_, vmineleout_, Xuvmineleout_, 
	Vmean_out_, Vmin_out_, Cout_, CuNout_, Vsigma_out_, Vsigma2_out_, VT_out_, ncells_out_, phi_out_;
      double d_         = 0;
      double Vmin_      = 0;
      double total_vol_ = 0;
      int ntetra_       = 0;
      Histogram1D histo_, histo_full_; // histo_full_ includes cells with volume < Vmin
    };


  } // namespace POSTPRO
} // namespace DEM
#endif // __CONTACTS_DELAUNAY_HPP__
