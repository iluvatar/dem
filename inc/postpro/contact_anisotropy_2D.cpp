#include "contact_anisotropy_2D.hpp"

namespace DEM {
  namespace POSTPRO {

    ContactAnisotropy2D::ContactAnisotropy2D()
    {
      ac_fout_[PLANE::XY].open("POSTPRO/ac_xy.dat"); ac_fout_[PLANE::XY].print("# time", "ac_xy", "F0", "F1", "F2");
      ac_fout_[PLANE::XZ].open("POSTPRO/ac_xz.dat"); ac_fout_[PLANE::XZ].print("# time", "ac_xz", "F0", "F1", "F2");
      ac_fout_[PLANE::YZ].open("POSTPRO/ac_yz.dat"); ac_fout_[PLANE::YZ].print("# time", "ac_yz", "F0", "F1", "F2");
      ac_fout_[PLANE::XZYZ].open("POSTPRO/ac_xzyz.dat"); ac_fout_[PLANE::XZYZ].print("# time", "ac_xzyz", "F0", "F1", "F2");
    }

    ContactAnisotropy2D::~ContactAnisotropy2D()
    {
      for (auto & fout : ac_fout_) {
	fout.close();
      }
    }

    void 
    ContactAnisotropy2D::print(const double & time)
    {
      for (int plane = PLANE::XY; plane < PLANE::TOTALPLANE; ++plane) {
	double E[3] = {0};
	eigen_values(Fabric_[plane], E[0], E[1], E[2]);
	ac_fout_[plane].print(time, 2*(E[2]-E[1]), E[0], E[1], E[2]); 
      }
    }

  } // namespace POSTPRO
} // namespace DEM
