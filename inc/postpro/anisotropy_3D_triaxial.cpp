#include "anisotropy_3D_triaxial.hpp"

namespace DEM {
  namespace POSTPRO {

    Anisotropy3DTriaxial::Anisotropy3DTriaxial()
    {
      // Define the angular partition parameters
      min_ = 0.0; 
      max_ = +M_PI;
      delta_angle_ = (max_ - min_)/nbins_;
	  
      reset_fhistos();
      reset_Xtensors();

      a_fout_[ANI::AC].open("POSTPRO/ani_ac.dat"); a_fout_[ANI::AC].print("# time", "ani_ac", "Eival0", "Eival1", "Eival2");
      a_fout_[ANI::ALN].open("POSTPRO/ani_aln.dat"); a_fout_[ANI::ALN].print("# time", "ani_aln", "Eival0", "Eival1", "Eival2");
      a_fout_[ANI::AFN].open("POSTPRO/ani_afn.dat"); a_fout_[ANI::AFN].print("# time", "ani_afn", "Eival0", "Eival1", "Eival2");
      a_fout_[ANI::AFT].open("POSTPRO/ani_aft.dat"); a_fout_[ANI::AFT].print("# time", "ani_aft", "Eival0", "Eival1", "Eival2");
   
      qoverp_fout_.open("POSTPRO/ani_qoverp.dat"); qoverp_fout_.print("# time", "q/p from anisotropies");

      X_fout_[ANI::AC].open("POSTPRO/ani_X_C.dat"); X_fout_[ANI::AC].print("# time ", "X00", "X01", "X02", "X10", "X11", "X12", "X20", "X21", "X22");
      X_fout_[ANI::ALN].open("POSTPRO/ani_X_LN.dat"); X_fout_[ANI::ALN].print("# time ", "X00", "X01", "X02", "X10", "X11", "X12", "X20", "X21", "X22");
      X_fout_[ANI::AFN].open("POSTPRO/ani_X_FN.dat"); X_fout_[ANI::AFN].print("# time ", "X00", "X01", "X02", "X10", "X11", "X12", "X20", "X21", "X22");
      X_fout_[ANI::AFT].open("POSTPRO/ani_X_FT.dat"); X_fout_[ANI::AFT].print("# time ", "X00", "X01", "X02", "X10", "X11", "X12", "X20", "X21", "X22");
    }

    Anisotropy3DTriaxial::~Anisotropy3DTriaxial()
    {
      for (auto & fout : a_fout_) {
	fout.close();
      }
      qoverp_fout_.close();
      for (auto & fout : X_fout_) {
	fout.close();
      }
    }

    void Anisotropy3DTriaxial::reset_fhistos(void)
    {
      for (auto & fsum : fsum_histo_) {
	  std::fill(fsum.begin(), fsum.end(), Vecnull);
      }
      for (auto & fsum : fsum_histo_D_) {
	std::fill(fsum.begin(), fsum.end(), 0.0);
      }
      for (auto & nc : nc_) {
	std::fill(nc.begin(), nc.end(), 0.0);
      }
    } 

    void Anisotropy3DTriaxial::reset_Xtensors(void)
    {
      for (auto & X : X_) 
	X.setZero();
    }


    void 
      Anisotropy3DTriaxial::print(const double & time)
    {
      // compute and print anisotropies
      std::array<double, 3> Eival;
      const bool absolute_values = false;
      double tr, trfn = 0, a[ANI::ATOTAL] = {0.0}; 

      // Contact anisotropy
      eigen_values(X_[ANI::AC], Eival[0], Eival[1], Eival[2], absolute_values);
      tr = Eival[2] + Eival[1] + Eival[0]; 
#if DEBUG
      if (std::fabs(tr - 1.0) > 1.0e-14) {
          std::cerr << "WARNING: anisotropy trace is not close to one. tr = " << tr << std::endl;
      }
#endif
      a[ANI::AC] = (tr > 0) ? (5.0/2.0)*(Eival[2] - Eival[0])/tr : 0;
      a_fout_[ANI::AC].print(time, a[ANI::AC], Eival[0], Eival[1], Eival[2]); 

      // Ln anisotropy
      eigen_values(X_[ANI::ALN], Eival[0], Eival[1], Eival[2], absolute_values);
      tr = Eival[2] + Eival[1] + Eival[0];
      a[ANI::ALN] = (tr > 0) ? (5.0/2.0)*(Eival[2] - Eival[0])/tr - a[ANI::AC] : 0;
      a_fout_[ANI::ALN].print(time, a[ANI::ALN], Eival[0], Eival[1], Eival[2]); 

      // Fn anisotropy
      eigen_values(X_[ANI::AFN], Eival[0], Eival[1], Eival[2], absolute_values);
      tr = trfn = Eival[2] + Eival[1] + Eival[0];
      a[ANI::AFN] = (tr > 0) ? (5.0/2.0)*(Eival[2] - Eival[0])/tr - a[ANI::AC] : 0;
      a_fout_[ANI::AFN].print(time, a[ANI::AFN], Eival[0], Eival[1], Eival[2]); 

      // Ft anisotropy
      eigen_values(X_[ANI::AFN] + X_[ANI::AFT], Eival[0], Eival[1], Eival[2], absolute_values);
      tr = Eival[2] + Eival[1] + Eival[0]; 
      a[ANI::AFT] = (tr > 0) ? (5.0/2.0)*(Eival[2] - Eival[0])/tr - a[ANI::AC] - a[ANI::AFN] : 0;
      a_fout_[ANI::AFT].print(time, a[ANI::AFT], Eival[0], Eival[1], Eival[2]); 

      // q/p from anisotropies
      qoverp_fout_.print(time, (2.0/5.0)*(a[ANI::AC] + a[ANI::ALN] + a[ANI::AFN] + a[ANI::AFT]));

      // util ofstream
      UTIL::OFileStream fout, fout_theo;

      // print mean force versus orientation
      const std::string fname[ANI::ATOTAL] = { "POSTPRO/ani_Pc.dat", "POSTPRO/ani_Pln.dat", "POSTPRO/ani_Pfn.dat", "POSTPRO/ani_Pft.dat" }; 
      const std::string fname_theo[ANI::ATOTAL] = { "POSTPRO/ani_Pc_theo.dat", "POSTPRO/ani_Pln_theo.dat", "POSTPRO/ani_Pfn_theo.dat", "POSTPRO/ani_Pft_theo.dat" }; 
      const int nc = std::accumulate(nc_[ANI::AC].begin(), nc_[ANI::AC].end(), 0); ASSERT(nc >= 0); 
#if DEBUG
      //ASSERT(nc == std::accumulate(nc_[ANI::ALN].begin(), nc_[ANI::ALN].end(), 0));
      if (nc == std::accumulate(nc_[ANI::ALN].begin(), nc_[ANI::ALN].end(), 0)) {
          std::cerr << "WARNING: Number of contacts nc is not equal to the anisotropy contacts count. "<< std::endl;
          std::cerr << "nc     = " << nc << std::endl;
          std::cerr << "nc_ani = " << std::accumulate(nc_[ANI::ALN].begin(), nc_[ANI::ALN].end(), 0) << std::endl;
      }
#endif
      const double theta0 = M_PI/2; // phase factor to rotate distributions
      double val, val_theo;
      for (int ani = ANI::AC; ani < ANI::ATOTAL; ++ani) {
	fout.open(fname[ani]); fout.print("# angle", "P(angle)", "x-component", "y-component");
	fout_theo.open(fname_theo[ani]); fout_theo.print("# angle", "Ptheo(angle)");	
	for (int bin = 0; bin < 2*nbins_; ++bin) {	
	  const double theta = min_ + bin*delta_angle_; 
	  switch (ani) {
	  case AC:
	    val = (nc > 0) ? (1.0*nc_[ani][bin%nbins_]*0.174532)/(nc) : 0; // pdf
	    //val_theo = (1 + a[ani]*(3*std::cos(theta-theta0)*std::cos(theta-theta0) - 1))/(1*M_PI); 
	    val_theo = (1 + a[ani]*(3*std::cos(theta-theta0)*std::cos(theta-theta0) - 1))/(4*M_PI); 
	    break;
	  case ALN: case AFN:
	    val = (nc_[ani][bin%nbins_] > 0) ? fsum_histo_D_[ani][bin%nbins_]/nc_[ani][bin%nbins_] : 0;
	    val_theo = (nc > 0) ? (avg[ani]/nc)*(1 + a[ani]*(3*std::cos(theta - theta0)*std::cos(theta - theta0) - 1)) : 0.0;
	    break;
	  case AFT: 
	    val = (nc_[ani][bin%nbins_] > 0) ? fsum_histo_[ani][bin%nbins_].norm()/nc_[ani][bin%nbins_] : 0;
	    val_theo = (nc > 0) ? (avg[ani]/nc)*a[ani]*std::sin(2.0*(theta-theta0)) : 0.0 ; 
	    break;
	  default:
	    val = val_theo = 0.0;
	  }
	  fout.print(theta, val, val*std::cos(theta), val*std::sin(theta));
	  fout_theo.print(theta, val_theo, val_theo*std::cos(theta), val_theo*std::sin(theta));
	}
	fout.close();	
	fout_theo.close();	
      }

      // print tensors
      for (auto & fout : X_fout_) {
	fout.print_val(time);
      }
      for (int a = ANI::AC; a < ANI::ATOTAL; ++a) {
	for (int ix = 0; ix <= 2; ++ix) {
	  for (int iy = 0; iy <= 2; ++iy) {
	    X_fout_[a].print_val(X_[a](ix, iy));
	  }
	}
      }
      for (auto & fout : X_fout_) {
	fout.print_newline();
      }

    } // print

  } // namespace POSTPRO
} // namespace DEM
