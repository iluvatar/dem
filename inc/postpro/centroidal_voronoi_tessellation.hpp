// Computes stat mech vars for a centroidal voronoi tessellation

#ifndef __CENTROIDAL_VORONOI__
#define __CENTROIDAL_VORONOI__

#include "postpro_base_functor.hpp"
#include "../file_util.hpp"
#include "../utility.hpp"
#include "../histogram1d.hpp"
#include "../nlfit.hpp"
#include "../functions.hpp"
#include <vector>
#include <algorithm>
#include <string>
#include <sstream>

namespace DEM {
  namespace POSTPRO {
    
    class CentroidalVoronoi : public Base_functor {
    public :
      CentroidalVoronoi() {};
      ~CentroidalVoronoi() { print_log("Centroidal Voronoi destructor called"); /*end_tasks();*/ }
      int init_tasks(const int & ngrains, const double & d, const int & dim) ;
      int end_tasks() ;
      int print_data();
      template <class ParticleList_t, class IndexList_t> 
      int 
      operator()(const ParticleList_t & particles, 
		 const double wmin[3], const double wmax[3], 
		 const bool periodic[3], const int & dim, const int & ncmin, 
		 const IndexList_t & filtered_ids)
      {
	UTIL::print_start("Computing statmech voronoi parameters for Centroidal Voronoi Tessellation");
	ASSERT(true == ready_);
	ASSERT(2 == dim || 3 == dim);
	ASSERT(ncmin >= 0);
      
	print_warning("Using all internal particles, regardless their contact number per particle");
      
	// memory arrays
	std::vector<double> voronoi_volumes(ngrains_); 
	std::vector<Vec3d_t> points(ngrains_); 
	std::vector<double> weights(ngrains_); 
	std::vector<Vec3d_t> centroids(ngrains_);
	std::vector<std::vector<int> > neigh(ngrains_);
	// fill points information
	for(int idx = 0; idx < ngrains_; ++idx) {
	  points[idx]  = particles[idx].R;
	  weights[idx] = particles[idx].rad;
	}
     
	// tessellate and relax: make cell generator coincide with centroid
	const double eps = 5.0e-7; // relax criteria
	const int niter_min = 1;   // Minimum number of relaxation iterations
	const int niter_max = 40; // Maximum number of relaxation iterations
	int iter = 0;
	for (iter = 1; iter <= niter_max; ++iter) {
	  print_log("CVT iter = ", iter);
	  UTIL::compute_voronoi_volumes(points, weights, wmin, wmax, periodic, dim, 
					voronoi_volumes, centroids, neigh, false);
	  if (iter >= niter_min) {
	    // compute cummulative difference between points and centroids
	    double val = 0; 
	    for (int ii = 0; ii < ngrains_; ++ii) {
	      val += centroids[ii].squaredNorm();
	    }
	    val = std::sqrt(val)/ngrains_;
	    print_log("val = ", val);
	    // check for termination
	    if (val <= eps) break; 
	  }
	  for (int ii = 0; ii < ngrains_; ++ii) {
	    centroids[ii] += points[ii]; // translate to global reference, since centroids are local relative to particle center 
	  }
	  points.swap(centroids); // next generators are the centroids
#ifdef DEBUG
	  for (int ii = 0; ii < ngrains_; ++ii) {
	    if (centroids[ii][0] <= wmin[0] || centroids[ii][0] >= wmax[0] || 
		centroids[ii][1] <= wmin[1] || centroids[ii][1] >= wmax[1] || 
		centroids[ii][2] <= wmin[2] || centroids[ii][2] >= wmax[2] ) {
	      print_log("ii = ", ii);
	      print_log("centroid = ", centroids[ii]);
	      std::cin.get();
	    }
	  }
#endif // DEBUG
	}
	print_log("FINAL niter CVT = ", iter); 
	if (niter_max == iter) print_warning("Maximum iterations reached -> niter_max = iter = ", niter_max);

	// Normalize volume 
	for(auto & vol : voronoi_volumes) vol /= d3_; // ALWAYS Normalize
      
	UTIL::print_done("Relaxing CVT.");

	// restarting voronoi histogram
	double min = 1.0e100, max = -1.0e100;
	for (const auto & val : voronoi_volumes) {
	  if (val < min) min = val;
	  if (val > max) max = val;
	}
	histo_vorovol_.reset();
	histo_vorovol_.construct(0.85*min, 1.2*max, 80);
		
	// accumulate voronoi volumes 
	double Vtotal = 0;
	for (auto & id : filtered_ids) {
	  // if (particles[id].nc < ncmin) continue; // use only internal with enough contacts
	  histo_vorovol_.increment(voronoi_volumes[id]); 
	  Vtotal += voronoi_volumes[id];
	}
	UTIL::print_done("Accumulating volumes on histograms.");

	// print volumes
	histo_vorovol_.print("POSTPRO/tmp.txt");
	print_log("Printing raw cvt volumes");
	std::ofstream fout; fout.open("POSTPRO/cvt_volumes.dat");
	for (const auto & id : filtered_ids) {
	  fout << voronoi_volumes[id] << "\n";
	}
	fout.close();
	UTIL::print_done("Printing raw cvt volumes");
	
	// fit vmin
	print_log("Fitting Vmin");
	double Vmean, Vstd;
	histo_vorovol_.mean_and_sigma(Vmean, Vstd);
	double param_err[3] = {0, 0, 0};                                    
	double param[3] = {0.697, Vmean, Vstd};
	double *xdata, *ydata;
	const int size = histo_vorovol_.get_valid_bins_size();
	//ASSERT(size > 0);
	if (size <= 0) {
	  print_error("size : ", size);
	  return 1;
	}
	xdata = new double [size];
	ydata = new double [size];
	histo_vorovol_.get_valid_data(xdata, ydata);	
	NLFIT::nlfit(1, size, xdata, ydata, kgamma_Vmin, param, param_err, 3);
        Vmin_ = param[0];
		
	// accumulate scaled voronoi volumes 
	histo_vorovol_.mean_and_sigma(Vmean, Vstd);
	for (auto & id : filtered_ids) {
	  const double aux = (voronoi_volumes[id] - Vmin_)/(Vmean - Vmin_);
	  histo_vorovol_scaled_.increment(aux); 
	}

	// compute stat mech parameters 
	const double lambda_cube = M_E/100.0;; // = e (pi d^3/6 phiRCP - Vmin)/kRCP
	voro_X_ = voro_K_ = voro_S_ = voro_CuN_ = cvt_Vmean_ = 0;
	double Vsigma = 0;
	histo_vorovol_.mean_and_sigma(cvt_Vmean_, Vsigma);
	if (Vsigma > 0 && (cvt_Vmean_ > Vmin_)) {
	  voro_X_ = UTIL::statmech_X(cvt_Vmean_, Vsigma, Vmin_);
	  voro_K_ = UTIL::statmech_K(cvt_Vmean_, Vsigma, Vmin_);
	  voro_S_ = UTIL::statmech_S(cvt_Vmean_, voro_K_, Vmin_, lambda_cube); 
	  if (filtered_ids.size() > 0) voro_CuN_ = voro_K_*Vtotal/(cvt_Vmean_*filtered_ids.size()); 
	  print_log("Vmin   = ", Vmin_);
	  print_log("Vmean  = ", cvt_Vmean_);
	  print_log("Vsigma = ", Vsigma);
	  print_log("X      = ", voro_X_);
	}
	UTIL::print_done("Computing stat mech vars for CVT");

	// print
	print_data();
      
	UTIL::print_done("Computing stat mech vars for CVT");
	return EXIT_SUCCESS; 
      }
    private :
      Histogram1D histo_vorovol_;			           //< Histograms of voronoi volumes 
      Histogram1D histo_vorovol_scaled_;	       	           //< Histograms of voronoi volumes, scaled 
      double voro_X_=0, voro_K_=0, voro_S_=0, voro_CuN_=0;         //< Stat mech values
      double cvt_Vmean_ = 0;					   //< Average cvt cell volume 
      int ngrains_ = 0;						   //< Number of grains
      double d_ = 0, d3_ = 0;					   //< diammeter, d^3
      double Vmin_ = 1.0e100;					   //< Minimum voronoi volume
      bool ready_ = false;                                         //< flag to assert system ready
      UTIL::OFileStream voro_X_fout_, voro_K_fout_, 
	voro_S_fout_, voro_SuK_fout_, voro_CuN_fout_,
	cvt_Vmean_fout_, cvt_Vmin_fout_; 
    };


  } // namespace POSTPRO
} // namespace DEM

#endif // __CENTROIDAL_VORONOI__
