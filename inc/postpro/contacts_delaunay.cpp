// Computes the delaunay tessellation for a sample from the contacts positions

#include "postpro/contacts_delaunay.hpp"

namespace DEM {
  namespace POSTPRO {
    //----------------------------------------------------------    
    //----------------------------------------------------------
    int ContactsDelaunay::init_tasks(const double & minrad) {
      Xout_.open("POSTPRO/contacts_delaunay_X.dat"); 
      Kout_.open("POSTPRO/contacts_delaunay_K.dat"); 
      Sout_.open("POSTPRO/contacts_delaunay_S.dat"); 
      SuKout_.open("POSTPRO/contacts_delaunay_SuK.dat"); 
      vmineleout_.open("POSTPRO/contacts_delaunay_vmin_elemental.dat"); 
      Xuvmineleout_.open("POSTPRO/contacts_delaunay_Xuvminelemental.dat"); 
      Vmean_out_.open("POSTPRO/contacts_delaunay_Vmean.dat"); 
      Vsigma_out_.open("POSTPRO/contacts_delaunay_Vsigma.dat"); 
      Vsigma2_out_.open("POSTPRO/contacts_delaunay_Vsigma2.dat"); 
      Vmin_out_.open("POSTPRO/contacts_delaunay_Vmin.dat"); 
      Cout_.open("POSTPRO/contacts_delaunay_C.dat"); 
      CuNout_.open("POSTPRO/contacts_delaunay_CuN.dat"); 
      VT_out_.open("POSTPRO/contacts_delaunay_VT.dat"); 
      ncells_out_.open("POSTPRO/contacts_delaunay_ncells.dat"); 
      phi_out_.open("POSTPRO/contacts_delaunay_phi.dat"); 
      d_    = 2*minrad;
      Vmin_ = M_SQRT2/12.0; // (d^3), CONSTANT, depends on dim (here 3D)
      histo_.construct(0.85*Vmin_, 2.6*Vmin_, 100);
      histo_full_.construct(0.01*Vmin_, 4.5*Vmin_, 250);
      return EXIT_SUCCESS; 
    }

    //----------------------------------------------------------    
    //----------------------------------------------------------    
    int ContactsDelaunay::end_tasks() {
      Xout_.close();
      Kout_.close();
      SuKout_.close();
      vmineleout_.close();
      Xuvmineleout_.close();
      Vmean_out_.close();
      Vsigma_out_.close();
      Vsigma2_out_.close();
      Vmin_out_.close();
      Cout_.close();
      CuNout_.close();
      VT_out_.close();
      ncells_out_.close();
      phi_out_.close();
      return EXIT_SUCCESS; 
    }

    //----------------------------------------------------------    
    // "Main" function: performs the key operations 
    //----------------------------------------------------------    
    int ContactsDelaunay::operator()(const std::vector<Vec3d_t> & cpoints) 
    {      
      UTIL::print_start("Contacts Delaunay");
      if (cpoints.size() <= 2) {
	print_error("0 or 1 or 2 points to analyse. Returning.");
	return EXIT_FAILURE;
      }

      //----------------------------------------------------------    
      // COMPUTE TETRAHEDRONS AND THEIR VOLUMES
      // declare tetgenio objects
      tetgenio tet_in, tet_out;
      // fill tetgenio in object with particles data
      print_log("Number of points to process (ncontacts) : ", cpoints.size());
      tet_in.numberofpoints = cpoints.size();

      if (nullptr != tet_in.pointlist) { delete [] tet_in.pointlist; tet_in.pointlist = nullptr; }
      tet_in.pointlist = new REAL [tet_in.numberofpoints * 3];
      long ii = 0;
      for (auto idx = 0; idx < tet_in.numberofpoints; ++idx) {
	for (int dir = 0; dir < 3; ++dir) { 
	  tet_in.pointlist[3*ii + dir] = cpoints[idx][dir]; 
	}
	++ii;
      }
      // tetrahedralize the system
      char tetparam[] = "KCQKG";
      tetrahedralize(tetparam, &tet_in, &tet_out);
#if DEBUG
      tet_in.save_nodes("POSTPRO/delaunay_in");      // save input  nodes
      tet_out.save_nodes("POSTPRO/delaunay_out");    // save output nodes
      tet_out.save_elements("POSTPRO/delaunay_out"); // save output tetra
      tet_out.save_faces("POSTPRO/delaunay_out");    // save output faces
#endif
      // release some memory after usage
      delete [] tet_in.pointlist; tet_in.pointlist = nullptr;

      // for each tetrahedron on tetgenio out, compute the volume and add to histogram
      ntetra_ = tet_out.numberoftetrahedra;
      total_vol_ = 0;
      print_log("ntetra = ", ntetra_);
      Vec3d_t Vertices[4];
      for (long idx = 0; idx < tet_out.numberoftetrahedra; ++idx) {
	// compute volume
	for (int ii = 0; ii < 4; ++ii) {
	  int vertex_idx  = tet_out.tetrahedronlist[4*idx + ii];
	  Vertices[ii] = Vec3d_t(tet_out.pointlist[vertex_idx*3 + 0], tet_out.pointlist[vertex_idx*3 + 1], tet_out.pointlist[vertex_idx*3 + 2]);
	}
	const double vol = std::fabs((Vertices[0] - Vertices[3]).dot((Vertices[1] - Vertices[3]).cross(Vertices[2] - Vertices[3])))/6;
	// add to histograms
	if (vol/CUBE(d_) >= 0.99*Vmin_)
	  histo_.increment(vol/CUBE(d_)); // NOTE: For Delaunay cells, we could have smaller than Vmin volumes, specially at borders ;)
	histo_full_.increment(vol/CUBE(d_)); 
	total_vol_ += vol/CUBE(d_);
      }
      ncells_out_.print(ntetra_);
      
      if (histo_.empty()) { 
	print_log("Delaunay volume histogram is empty. Returning ...");
	histo_.print("POSTPRO/contacts_delaunay-histo_delaunay_vol.dat");
	histo_full_.print("POSTPRO/contacts_delaunay-histo_full_delaunay_vol.dat");
	return EXIT_SUCCESS;
      }

      //----------------------------------------------------------    
      // SM VARIABLES
      // compute the compactivity, entropy, and k
      double mean, sigma; 
      histo_.mean_and_sigma(mean, sigma);
      //histo_full_.mean_and_sigma(mean, sigma);
#if DEBUG
      histo_.print("POSTPRO/contacts_delaunay-histo_delaunay_vol.dat");
      histo_full_.print("POSTPRO/contacts_delaunay-histo_full_delaunay_vol.dat");
      print_log("mean  = ", mean );
      print_log("Vmin_ = ", Vmin_);
      ASSERT(mean > Vmin_);
#endif
      double X = 0, K = 0;
      if (sigma > 0) {
	X = UTIL::statmech_X(mean, sigma, Vmin_);
	K = UTIL::statmech_K(mean, sigma, Vmin_);
      }
      print_log("mean  =", mean);
      print_log("K     =", K);
      print_log("X     =", X);
      print_log("Vmin_ =", Vmin_);
      double newVmin = Vmin_;
      
      //----------------------------------------------------------    
      // ADJUST VALUES BY FITTING TO IMPROVE SM PARAMETERS COMPUTATIONS 

      
      /*// get valid data from histo (only values where pdf != 0)
      const int size = histo_.get_valid_bins_size(); 
      double *xdata, *ydata; xdata = new double [size]; ydata = new double [size]; 
      histo_.get_valid_data(xdata, ydata);
      
      // compute k by maximum likelihood (aproximated):
      // http://en.wikipedia.org/wiki/Gamma_distribution#Maximum_likelihood_estimation
      if (size > 0) {
	int n = 0;
	int *count = new int [size]; 
	histo_.get_valid_counters(xdata, count);
	double s = 0, sum1 = 0, sum2 = 0;
	print_log("Vmin orig  = ", Vmin_);
	print_log("Vmin histo = ", xdata[0]*0.99);
	for (int ii = 0; ii < size; ++ii) {
	  const double xi = xdata[ii] - xdata[0]*0.99; 
	  sum1 += count[ii]*xi;
	  sum2 += count[ii]*std::log(xi);
	  n += count[ii];
	}
	s = std::log(sum1/n) - sum2/n;
	K = (3.0 - s + std::sqrt(SQUARE(s-3.0) + 24*s))/(12*s);
	print_log("K from likelihood =", K);
	print_log("X new  =", (mean - Vmin_)/K);
	delete [] count; count = nullptr;
      }
      delete [] xdata; xdata = nullptr;
      delete [] ydata; ydata = nullptr;
      //*/
     
      ///*// fit the ccdf = 1 - cdf to estimate parameters 
      // This is better than fitting the pdf, since it "cleans" the data
      histo_.print_ccdf("POSTPRO/contacts_delaunay_ccdf.dat");
      histo_full_.print_ccdf("POSTPRO/contacts_delaunay_full_ccdf.dat");
      std::vector<double> xval, ccdf;
      histo_.save_ccdf(xval, ccdf);
      double param_err[3] = {0};
      double param[3] = {1.2*K, 0.95*Vmin_, mean}; 
      int status = NLFIT::nlfit(3, xval.size(), &xval[0], &ccdf[0], qgamma, param, param_err, 3);
      if (EXIT_SUCCESS == status) { // update parameters
	K = param[0]; newVmin = param[1]; mean = param[2]; X = (mean - newVmin)/K; 
#if DEBUG 
	std::cout << "K     new = " << param[0] << " +- " << param_err[0] << std::endl;
	std::cout << "Vmin  new = " << param[1] << " +- " << param_err[1] << std::endl;
	std::cout << "Vmean new = " << param[2] << " +- " << param_err[2] << std::endl;
	std::cout << "X     new = " << X << std::endl;
#endif  
      }
      else {
#if DEBUG
	for(int ii = 0; ii < 3; ++ii) {
	  std::cerr << "par[" << ii << "] = " << param[ii] << " +- " << param_err[ii] << std::endl;
	}
#endif
	print_error("Bad fitting on Delaunay.");
	print_error("Keeping the old estimated values for Delaunay parameters.");
      }
      //*/      
      
      /*// fit the pdf values to estimate K, Vmin // replaced in favor of ccdf fit
      double param_err[3] = {0};
      double param[3] = {std::max(K, 4.0), 0.95*Vmin_, mean}; // Last one is fixed, since npar = 2 at nlfit
      int status = NLFIT::nlfit(2, size, x, y, kgamma_k_Vm_Vmean, param, param_err, 3);
      if (EXIT_SUCCESS == status) { // update parameters
	K = param[0]; newVmin = param[1]; mean = param[2];  X = (mean - newVmin)/K;
#if DEBUG
	std::cout << "K    new = " << K       << " +- " << param_err[0] << std::endl;
	std::cout << "Vm   new = " << newVmin << " +- " << param_err[1] << std::endl;
	std::cout << "mean new = " << mean    << " +- " << param_err[2] << std::endl;
	std::cout << "X    new = " << X << std::endl;
#endif
      }
      else {
#if DEBUG
	for(int ii = 0; ii < 3; ++ii) {
	  std::cerr << "par[" << ii << "] = " << param[ii] << " +- " << param_err[ii] << std::endl;
	}
#endif
	print_error("Bad fitting on Delaunay.");
	print_error("Keeping the old estimated values for Delaunay parameters.");
      }
      //*/

      // other constants
      const double lambda_cube = M_E/100.0; // Normalized. FIX WITH RIGHT VALUE FOR K_{RCP}^{DELA}
      const double S = K*(1.0 + std::log((mean - newVmin)/(K*lambda_cube)));
      const double vmin_elemental = newVmin/K;

      //----------------------------------------------------------    
      // PRINTING

      // print parameters
      //phi_out_.print((indexes.size()*M_PI/6.0)/(ncells*(K*X+newVmin)));
      Xout_.print(X, Vmin_); 
      Kout_.print(K);
      Sout_.print(S); 
      SuKout_.print(( (K > 0) ? S/K : 0.0 )); 
      vmineleout_.print(vmin_elemental, vmin_elemental, CUBE(d_));  
      Xuvmineleout_.print(( (vmin_elemental > 0) ? X/vmin_elemental : 0 )); 
      Vmean_out_.print(mean, CUBE(d_));
      Vsigma_out_.print(sigma, CUBE(d_));
      Vsigma2_out_.print(sigma*sigma, CUBE(d_*d_));
      Vmin_out_.print(newVmin);
      //const double C = (mean > 0) ? K*inner_volume*1.1/mean : 0 ; // 1.1 fixes scale problem associated with k (too small) or Vmean (too large)
      //Cout_.print(C);
      //CuNout_.print( ((bodies.size() > 0) ? C/indexes.size() : 0) );
      VT_out_.print(total_vol_, CUBE(d_));
      print_log("Total internal Volume from Delaunay (/d^3) = ", total_vol_);

      // print histograms
      histo_.print_mean_sigma("POSTPRO/contacts_delaunay-histo_delaunay_vol_mean_sigma.dat");
      histo_.print("POSTPRO/contacts_delaunay-histo_delaunay_vol.dat");
      histo_.print("POSTPRO/contacts_delaunay-histo_delaunay_scaled.dat", 
		   1.0/(mean - newVmin), -newVmin/(mean - newVmin), mean - newVmin, 0);
      double meanfull, sigmafull;
      histo_full_.mean_and_sigma(meanfull, sigmafull);
      histo_full_.print("POSTPRO/contacts_delaunay-histo_delaunay_full_vol_orig.dat");
      histo_full_.print("POSTPRO/contacts_delaunay-histo_delaunay_full_scaled.dat", 
			1.0/(meanfull - newVmin), -newVmin/(meanfull - newVmin), meanfull - newVmin, 0);

      // print theoretical distro
      print_debug("Start printing theoretical.");
      double xmin, xmax, dx;
      UTIL::OFileStream fout;
      fout.open("POSTPRO/contacts_delaunay-delaunay_distro.dat"); 
      xmin = 0.99*newVmin; xmax = 3.5*Vmin_; dx = (xmax - xmin)/150; ASSERT(dx > 0);
      for(double x = xmin + dx; x <= xmax; x += dx) {
	double var = (x - newVmin)/X;
	fout.print(x, std::exp(-var + (K-1.0)*std::log(var))/(tgamma(K)*(X)));
      }
      fout.close();
      print_debug("Start printing theoretical 2.");
      fout.open("POSTPRO/contacts_delaunay-distro_scaled.dat"); 
      xmin = 0.01; xmax = 5.5; dx = (xmax - xmin)/150; ASSERT(dx > 0);
      for(double x = xmin; x <= xmax; x += dx) {
	fout.print(x, (std::exp(-K*(x - std::log(K*x))))/(tgamma(K)*x));
      }
      fout.close();
      print_debug("Done printing theoretical.");

      // print other data

      UTIL::print_done("Delaunay for Contacts");
      return EXIT_SUCCESS; 
    }


  } // namespace POSTPRO
} // namespace DEM
