// this file implements a base functor for postpro functions to derive from
// this way, all interfaces are uniform 

#ifndef __POSTPRO_BASE_FUNCTOR__
#define __POSTPRO_BASE_FUNCTOR__

#include "../utility.hpp"
#include "../workspace_dem_config.hpp"
#include <iomanip>

namespace DEM {
  namespace POSTPRO {

    class Base_functor {
    public :
      int init_tasks() ; 
      int operator()() ;
      int end_tasks() ;
    protected:
    };
    
  } // namespace POSTPRO
} // namespace DEM
#endif
