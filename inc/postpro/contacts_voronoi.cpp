#include "postpro/contacts_voronoi.hpp"

namespace DEM {
  namespace POSTPRO {
    
    int ContactsVoronoi::init_tasks(const double & d, const int & dim) {
      const std::string msg = "Init tasks for ContactsVoronoi"; UTIL::print_start(msg);
      d_ = d; d3_ = CUBE(d_);
      Vmin_ = UTIL::voronoi_vmin(dim)*6/M_PI; // normalized by d_^D // WARNING: Actual value could be smaller 
      
      // scaling
      

      // construct histograms
      histo_vorovol_.construct(0.6*Vmin_, 6.5*Vmin_, 200); // min, max, nbins
      
      // set print file names
      std::stringstream name;
      name.str(""); name << "POSTPRO/contacts_voronoi-voronoi_X.dat"; 
      voro_X_fout_.open(name.str());
      name.str(""); name << "POSTPRO/contacts_voronoi-voronoi_K.dat"; 
      voro_K_fout_.open(name.str());
      name.str(""); name << "POSTPRO/contacts_voronoi-voronoi_S.dat"; 
      voro_S_fout_.open(name.str());
      name.str(""); name << "POSTPRO/contacts_voronoi-voronoi_CuN.dat"; 
      voro_CuN_fout_.open(name.str());
      
      ready_ = true;
      
      UTIL::print_done(msg);
      return EXIT_SUCCESS;					                
    }
    

    int ContactsVoronoi::print_data() {
      UTIL::print_start("Printing statmech data for Contacts Voronoi.");
      // print stat mech vars
      voro_X_fout_.print(voro_X_);
      voro_K_fout_.print(voro_K_);
      voro_S_fout_.print(voro_S_);
      voro_CuN_fout_.print(voro_CuN_);

      // print histogram
      std::stringstream name;
      name.str(""); name << "POSTPRO/contacts_voronoi-histoVoroVol.dat";
      histo_vorovol_.print(name.str());
      // scaled version
      double Vmean = 0, Vsigma = 0;
      histo_vorovol_.mean_and_sigma(Vmean, Vsigma);
      double mx = 1.0, bx = 0.0, my = 1.0, by = 0.0;
      if (Vmean > Vmin_) {
	mx = 1.0/(Vmean-Vmin_), bx = -Vmin_/(Vmean-Vmin_), my = Vmean-Vmin_, by = 0.0;
      }
      name.str(""); name << "POSTPRO/contacts_voronoi-histoVoroVol_scaled.dat";
      histo_vorovol_.print(name.str(), mx, bx, my, by);

      UTIL::print_done("Printing statmech data for Contacts Voronoi.");
      return EXIT_SUCCESS;
    }
    
    int ContactsVoronoi::end_tasks() {
      print_data();
      return EXIT_SUCCESS; 
    }

  } // namespace POSTPRO
} // namespace DEM

