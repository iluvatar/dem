// Computes stat mech vars per material inside sample

#include "postpro/statmech_per_material.hpp"
#include "../file_util.hpp"
#include "../utility.hpp"
#include "../histogram1d.hpp"
#include <vector>
#include <algorithm>
#include <string>
#include <sstream>
#include <map>

namespace DEM {
  namespace POSTPRO {
    
    // NOTE: Using the reference particle state
    int StatMechMaterials::init_tasks(const std::vector<Particle> & particles, const int & ngrains, const double & d, const int & dim) {
      const std::string msg = "Init tasks for StatMechMaterials"; UTIL::print_start(msg);
      ngrains_ = ngrains;
      d_ = d; d3_ = d_*d_*d_;
      Vmin_ = UTIL::voronoi_vmin(dim); // normalized by d_^D // WARNING: Actual value could be smaller 

      // find all tags
      for (int id = 0; id < ngrains_; ++id) {
	const int mtag = particles[id].materialTag;
	if (tags_.end() == std::find(tags_.begin(), tags_.end(), mtag)) {
	  print_log("New mtag = ", mtag);
	  tags_.push_back(mtag);
	}
      }
      std::sort(tags_.begin(), tags_.end());
      ntags_ = tags_.size();
      ASSERT(ntags_ > 0);
      for (int itag = 0; itag < ntags_; ++itag) {
	tag_index_[tags_[itag]] = itag;
      }

      // memory
      histos_vorovol_.resize(ntags_);
      voro_X_.resize(ntags_);
      voro_K_.resize(ntags_);
      voro_S_.resize(ntags_);
      voro_CuN_.resize(ntags_);
      ngrains_tag_.resize(ntags_);
      voro_X_fout_.resize(ntags_);
      voro_K_fout_.resize(ntags_);
      voro_S_fout_.resize(ntags_);
      voro_CuN_fout_.resize(ntags_);

      // construct histograms
      for (int itag = 0; itag < ntags_; ++itag) {
	histos_vorovol_[itag].construct(0.65, 1.3, 30); // min, max, nbins
      }

      // set print file names
      for (int itag = 0; itag < ntags_; ++itag) {
	std::stringstream name;
	name.str(""); name << "POSTPRO/statmech-voronoi_X-materialtag_" << tags_[itag] << ".dat"; 
	voro_X_fout_[itag].open(name.str());
	name.str(""); name << "POSTPRO/statmech-voronoi_K-materialtag_" << tags_[itag] << ".dat"; 
	voro_K_fout_[itag].open(name.str());
	name.str(""); name << "POSTPRO/statmech-voronoi_S-materialtag_" << tags_[itag] << ".dat"; 
	voro_S_fout_[itag].open(name.str());
	name.str(""); name << "POSTPRO/statmech-voronoi_CuN-materialtag_" << tags_[itag] << ".dat"; 
	voro_CuN_fout_[itag].open(name.str());
      }

      ready_ = true;
      
      UTIL::print_done(msg);
      return EXIT_SUCCESS;					                
    }
    
    int StatMechMaterials::print_data() {
      UTIL::print_start("Printing statmech data per material tag.");
      // print stat mech vars
      for (int itag = 0; itag < ntags_; ++itag) {
	voro_X_fout_[itag].print(voro_X_[itag]);
	voro_K_fout_[itag].print(voro_K_[itag]);
	voro_S_fout_[itag].print(voro_S_[itag]);
	voro_CuN_fout_[itag].print(voro_CuN_[itag]);
      }

      // print histograms
      for (int itag = 0; itag < ntags_; ++itag) {
	std::stringstream name;
	name.str(""); name << "POSTPRO/statmech-histoVoroVol-materialtag_" << tags_[itag] << ".dat";
	histos_vorovol_[itag].print(name.str());
	// scaled versions
	double Vmean = 0, Vsigma = 0;
	histos_vorovol_[itag].mean_and_sigma(Vmean, Vsigma);
	double mx = 1.0, bx = 0.0, my = 1.0, by = 0.0;
	if (Vmean > Vmin_) {
	  mx = 1.0/(Vmean-Vmin_), bx = -Vmin_/(Vmean-Vmin_), my = Vmean-Vmin_, by = 0.0;
	}
	name.str(""); name << "POSTPRO/statmech-histoVoroVol_scaled-materialtag_" << tags_[itag] << ".dat";
	histos_vorovol_[itag].print(name.str(), mx, bx, my, by);
      }

      UTIL::print_done("Printing statmech data per material tag.");
      return EXIT_SUCCESS;
    }
    
    int StatMechMaterials::end_tasks() {
      print_data();
      return EXIT_SUCCESS; 
    }

  } // namespace POSTPRO
} // namespace DEM

