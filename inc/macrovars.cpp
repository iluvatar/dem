#include "macrovars.hpp"

MacroVars::MacroVars()
{
  sigma_fout_.open("POSTPRO/sigma.dat"); sigma_fout_.print("# time", "S0", "S1", "S2", "S00", "S01", "S02", "S10", "S11", "S12", "S20", "S21", "S22");
  fabric_fout_.open("POSTPRO/fabric.dat"); fabric_fout_.print("# time", "F0", "F1", "F2", "F00", "F01", "F02", "F10", "F11", "F12", "F20", "F21", "F22");
  a_fout_.open("POSTPRO/fabric_anisotropy.dat"); a_fout_.print("# time", "fabric_anisotropy");
}

MacroVars::~MacroVars()
{
  sigma_fout_.close();
  fabric_fout_.close();
  a_fout_.close();
}

void 
MacroVars::stress_invariants(double & I1, double & I2, double & I3, double & J1, double & J2, double & J3) 
{
  double S0, S1, S2;
  eigen_values(Sigma_, S0, S1, S2);
  I1 = S0 + S1 + S2;
  I2 = S0*S1 + S1*S2 + S2*S0;
  I3 = S0*S1*S2;
  J1 = 0;
  J2 = I1*I1/3.0 - I2;
  J3 = 2*I1*I1/27.0 -I1*I2/3.0 + I3;
}

void 
MacroVars::p_and_q(const int dim, double & p, double & q)
{
  p = q = 0;
  get_p_q(Sigma_, p, q);
}

void 
MacroVars::print_macrovar(const double & time, const Mat3d_t & M, UTIL::OFileStream & fout)
{
  double e[3] = {0};
  eigen_values(M, e[0], e[1], e[2]);
  fout.print_val(time, e[0], e[1], e[2]);
  for(int i = 0; i < 3; i++) {
    for(int j = 0; j < 3; j++) {
      fout.print_val(M(i, j));
    }
  }
  fout.print_val(deviation_from_symmetric(M));
  fout.print_newline();
}

void 
MacroVars::print(const double & time)
{
  print_macrovar(time, Sigma_, sigma_fout_);
  print_macrovar(time, Fabric_, fabric_fout_);
  double E[3] = {0};
  eigen_values(Fabric_, E[0], E[1], E[2]);
  a_fout_.print(time, 2*(E[2]-E[1]));
}
