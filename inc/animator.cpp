#include "animator.hpp"

namespace DEM {

  //------------------------------------------------------
  // gnuplot
  //------------------------------------------------------
  void Visualization::print_header_gnuplot(const std::vector<Particle> & particle, std::string filename) // Gnuplot preamble
  {
    const int t_id = particle.size() - DEM::NWALLS + TOP;
    const int r_id = particle.size() - DEM::NWALLS + RIGHT;
    std::cout << "set size ratio -1" << std::endl;
    std::cout << "set term gif giant size 960,720 animate optimize" << std::endl;
    std::cout << "set out '" << filename << "'" << std::endl;
    //std::cout << "set term svg dynamic" << std::endl;
    //std::cout << "set out 'POSTPRO/frame.svg'" << std::endl;
    std::cout << "set grid" << std::endl;
    std::cout << "set parametric" << std::endl;
    std::cout << "set trange [0:1]" << std::endl;
    std::cout << "set yrange [" << -0.1*particle[t_id].R[2] << ":" << 1.1*particle[t_id].R[2] << "]" << std::endl;  // vertical frame
    std::cout << "set xrange ["<<-0.2*particle[r_id].R[0] <<":" << 1.2*particle[r_id].R[0] << "]" << std::endl;  // horizontal frame
    std::cout << "unset key" << std::endl; // avoid legend
  }

  void Visualization::print_system_gnuplot(const std::vector<Particle> & particle, double time)
  {
    const int ngrains = particle.size() - DEM::NWALLS; 
    const int b_id = ngrains + BOTTOM;
    const int t_id = ngrains + TOP;
    const int l_id = ngrains + LEFT;
    const int r_id = ngrains + RIGHT;
    const double xi = particle[l_id].R[0];
    const double xf = particle[r_id].R[0];
    const double zi = particle[b_id].R[2];
    const double zf = particle[t_id].R[2];
    //std::cout << "set yrange [" << -0.3*fabs(zf) << ":" << 1.3*fabs(zf) << "]" << std::endl;  // vertical frame
    std::cout << "set title '" << time << " time units'"<< std::endl;
    std::cout << "set label 1 \"P\" at " << particle[t_id].R[0] << "," << particle[t_id].R[2] << std::endl; // ceil
    //for (int id = 0; id < ngrains; ++id) {
    //std::cout << "unset label " << id + 2 << std::endl;
    //}
    //for (int id = 0; id < ngrains; ++id) {
    //std::cout << "set label " <<  id + 2 << " \" " << id << " \" at " << particle[id].R[0] << " , " << particle[id].R[2] << std::endl; 
    //} 
    std::cout << "plot " ;
    std::cout << xi << " + t*" << xf - xi << ", "<< zi << " lt -1"; // floor
    std::cout << " , "  ;
    std::cout << xi << " + t*" << xf - xi << ", "<< zf << " lt -1"; // ceil
    std::cout << " , "  ;
    std::cout << xi << "," << zi << " + t*" << zf - zi << " lt -1"; // l-wall 
    std::cout << " , "  ;
    std::cout << xf << "," << zi << " + t*" << zf - zi << " lt -1"; // r-wall
    std::cout << " , "  ;
    for (int id = 0; id < ngrains; ++id) {
      print_grain_gnuplot(particle[id], particle[id].bcTag, 0);
      std::cout << " , " ;
      /* // periodic images
	 if(true == dom.periodic_x()) {
	 if(dom.particle[id].R[0] < 4*dom.maxRad) print_grain_gnuplot(dom, dom.particle[id], -1, +dom.length(0));
	 if(dom.particle[id].R[0] > dom.length(0) - 4*dom.maxRad) print_grain_gnuplot(dom, dom.particle[id], -1, -dom.length(0));
	}*/
    }	
    std::cout << " 1/0 , 1/0 " ; // just to fix the last ","
    std::cout << std::endl ;
  }

  int Visualization::get_color_gnuplot(int tag) 
  {
    int color = 1;
    if (0 == tag) color = 3;
    if (-1 == tag) color = 6;
    if (10 == tag) color = 2;
    if (11 == tag) color = 4;
    return color;
  }


  void
  Visualization::print_grain_gnuplot(const Particle & grain, int colorTag, double xshift) 
  {
    std::cout << grain.R[0] + xshift << " + " << grain.rad << "*cos(t*6.28)" 
	 << " , "
	 << grain.R[2] << " + " << grain.rad << "*sin(t*6.28) ";
    std::cout << " lt "<< get_color_gnuplot(colorTag) << " lw 1" ; 
    // draw y-orientation
    std::cout << " , "; 
    std::cout << grain.R[0] + xshift << " + t*" << grain.rad << "*cos(" << -grain.PHI[1] << ") , " 
	 << grain.R[2] << " + t*" << grain.rad << "*sin(" << -grain.PHI[1] << ")  "
	 << " lt 6 lw 2" ;
  }

  //------------------------------------------------------
  // povray
  //------------------------------------------------------
  void Visualization::print_system_povray(const std::vector<Particle> & particle, const std::string & filename, 
					  const double & xi, const double & xf, const double & yi, const double & yf, const double & zi, const double & zf,  
					  const double & campos_x, const double & campos_y, const double & campos_z)
  {
    const int ngrains = particle.size() - DEM::NWALLS; 

    // Povray axes are left-hand defined. Furthermore:
    // x_pov = x_sim
    // y_pov = z_sim
    // z_pov = y_sim
    // For .pov files 
    std::ofstream fout(filename.c_str()); // open the file for writing the povray commands
    fout << "#include \"colors.inc\"" << std::endl << std::endl; // include library
    fout << "#include \"stones.inc\"" << std::endl << std::endl; // include library
    fout << "// ---------------------------------------------------" << std::endl;
    fout << "// declarations " << std::endl;
    fout << "#declare CAM_POS_X = " << campos_x << ";" << std::endl;   // pov = sim
    fout << "#declare CAM_POS_Y = " << campos_y << ";" << std::endl;   // pov = sim
    fout << "#declare CAM_POS_Z = " << campos_z << ";" << std::endl;  // pov = sim
    fout << "#declare X0 = " << xi << ";" << std::endl;      // pov = sim
    fout << "#declare XF = " << xf << ";" << std::endl;      // pov = sim
    fout << "#declare Y0 = " << zi << ";" << std::endl;      // Z0_sim
    fout << "#declare YF = " << zf << ";" << std::endl;      // ZF_sim 
    fout << "#declare Z0 = " << yi << ";" << std::endl;      // Y0_sim
    fout << "#declare ZF = " << yf << ";" << std::endl;      // YF_sim
    fout << std::endl << std::endl; 
    fout << "#include \"inc/header_povray.inc\"" << std::endl << std::endl;
    fout << "// ---------------------------------------------------" << std::endl;
    fout << "// Objects" << std::endl << std::endl;
    // Container : plane PlaneWalls
    fout << " object { " << std::endl;
    fout << " MyCube " << std::endl;
    fout << " } " << std::endl;
    // grains
    for (int id = 0; id < ngrains; ++id) {
      fout << " object { " << std::endl;
      fout << " MySphere " << std::endl;
      fout << "   scale " << particle[id].rad << std::endl;        // scale to current radius
      fout << " rotate <" << -particle[id].PHI[0]*(180.0/M_PI) << ", 0, 0 >" << std::endl; // rotation in x-axis
      fout << " rotate <0, " << -particle[id].PHI[2]*(180.0/M_PI) << ", 0 >" << std::endl; // rotation in z-axis (y_pov)
      fout << " rotate <0, 0, " << -particle[id].PHI[1]*(180.0/M_PI) << " >" << std::endl; // rotation in y-axis (z_pov)
      fout << " translate <"    
	   << particle[id].R[0] << " , "
	   << particle[id].R[2] << " , " // z_sim = y_pov
	   << particle[id].R[1] << " "   // y_sim = z_pov
	   << "> " << std::endl;
      fout << " } " << std::endl;
      fout << std::endl;
    }
    fout.close();
  }

  //------------------------------------------------------
  // paraview
  //------------------------------------------------------
  void 
  Visualization::start_paraview_pvd(void)
  {
    pvd_started_ = true;
    fpvd_.open("DISPLAY/data.pvd");
    fpvd_ << "<?xml version=\"1.0\"?>" << "\n";
    fpvd_ << "<VTKFile type=\"Collection\" version=\"0.1\" byte_order=\"LittleEndian\"> " << "\n";
    fpvd_ << "  <Collection> " << "\n";
    fpvd_contacts_.open("DISPLAY/contacts_data.pvd");
    fpvd_contacts_ << "<?xml version=\"1.0\"?>" << "\n";
    fpvd_contacts_ << "<VTKFile type=\"Collection\" version=\"0.1\" byte_order=\"LittleEndian\"> " << "\n";
    fpvd_contacts_ << "  <Collection> " << "\n";
    fpvd_planes_.open("DISPLAY/planes_data.pvd");
    fpvd_planes_ << "<?xml version=\"1.0\"?>" << "\n";
    fpvd_planes_ << "<VTKFile type=\"Collection\" version=\"0.1\" byte_order=\"LittleEndian\"> " << "\n";
    fpvd_planes_ << "  <Collection> " << "\n";
  }
  
  void 
  Visualization::finish_paraview_pvd(void)
  {
    if (pvd_started_) {
      fpvd_ << "  </Collection> " << "\n";
      fpvd_ << "</VTKFile> " << "\n";
      fpvd_.close();
      fpvd_contacts_ << "  </Collection> " << "\n";
      fpvd_contacts_ << "</VTKFile> " << "\n";
      fpvd_contacts_.close();
      fpvd_planes_ << "  </Collection> " << "\n";
      fpvd_planes_ << "</VTKFile> " << "\n";
      fpvd_planes_.close();
      pvd_started_ = false;
    }
  }

  void 
  Visualization::print_paraview_vtp_header(std::ofstream & fout)
  {
    fout << "<?xml version=\"1.0\"?>" << "\n";
    fout << "<VTKFile type=\"PolyData\" version=\"0.1\" byte_order=\"LittleEndian\">" << "\n";
    fout << "  <PolyData>" << "\n";
  }
  
  void 
  Visualization::print_paraview_vtp_footer(std::ofstream & fout)
  {
    fout << "  </PolyData>" << "\n";
    fout << "</VTKFile>" << "\n";
  }

  void 
  Visualization::print_paraview_vtp(const std::vector<Particle> & particle, const std::vector<Contact> & contacts, const std::vector<Particle> & particle_init, const std::string & filename, const double time, const bool periodic[])
  {
    ASSERT(std::string::npos != filename.find(".vtp"));
    // print filenames (without "DISPLAY/") into pvd file
    std::string fn;
    if (pvd_started_) {
      fn = filename; fn.replace(fn.find("DISPLAY/"), 8, "");
      fpvd_ << "      <DataSet timestep=\"" << std::setprecision(16) << time << "\" file=\"" << fn << "\" /> " << "\n";
      fn = filename; fn.replace(fn.find("DISPLAY/"), 8, "");
      fn = "contacts-" + fn;
      fpvd_contacts_ << "      <DataSet timestep=\"" << std::setprecision(16) << time << "\" file=\"" << fn << "\" /> " << "\n";
      fn = filename; fn.replace(fn.find("DISPLAY/"), 8, "");
      fn = "planes-" + fn;
      fpvd_planes_ << "      <DataSet timestep=\"" << std::setprecision(16) << time << "\" file=\"" << fn << "\" /> " << "\n";
    }
    // print full and actual filenames
    std::ofstream fout; 
    // spheres
    fn = filename;
    print_log("Printing to : ", fn);
#ifdef USE_VTK
    print_VTK_vtp_points(particle, particle_init, fn, time);
#else // NO USE_VTK
    fout.open(fn.c_str());
    print_paraview_vtp_header(fout);
    fout << "    <Piece NumberOfPoints=\""<< particle.size() - DEM::NWALLS << "\">" << "\n"; //" NumberOfVerts=”#”	NumberOfLines=”#” NumberOfStrips=”#” NumberOfPolys=”#”>
    print_paraview_vtp_points(particle, particle_init, fout);
    fout << "    </Piece>" << "\n";
    print_paraview_vtp_footer(fout);
    fout.close();
#endif // USE_VTK
    // contacts
    fn = filename; 
    fn.replace(fn.find("DISPLAY/"), 8, ""); // because DISPLAY is after "contacts-"
    fn = "DISPLAY/contacts-" + fn;
    print_log("Printing to : ", fn);
#ifdef USE_VTK
    print_VTK_vtp_contact_lines(particle, contacts, periodic, fn, time);
#else // NO USE_VTK
    fout.open(fn.c_str());
    print_paraview_vtp_header(fout);
    fout << "    <Piece NumberOfPoints=\""<< 2*contacts.size() << "\" NumberOfLines=\"" << contacts.size() << "\" >" << "\n"; //" NumberOfVerts=”#” NumberOfLines=”#” NumberOfStrips=”#” NumberOfPolys=”#”>
    print_paraview_vtp_contact_lines(particle, contacts, fout, periodic);
    fout << "    </Piece>" << "\n";
    print_paraview_vtp_footer(fout);
    fout.close();
#endif // USE_VTK
    // planes
    fn = filename; 
    fn.replace(fn.find("DISPLAY/"), 8, ""); // because DISPLAY is after "contacts-"
    fn = "DISPLAY/planes-" + fn;
    print_log("Printing to : ", fn);
    fout.open(fn.c_str());
    print_paraview_vtp_header(fout);
    print_paraview_vtp_planes(particle, particle_init, fout);
    print_paraview_vtp_footer(fout);
    fout.close();
  }

#ifdef USE_VTK 
  int 
  Visualization::print_VTK_vtp_points(const std::vector<Particle> & particle, const std::vector<Particle> & particle_init, std::string & filename, double time)
  {
    // avoids unused vars warnings
    _UNUSED(particle); 
    _UNUSED(particle_init); 
    _UNUSED(filename); 
    // SEE: http://www.vtk.org/Wiki/VTK/Examples/Cxx/PolyData/MiscPointData
    // some vars
    const int ngrains = particle.size() - DEM::NWALLS; 

    
    // Create a vtkPoints object and store the points in it
    vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New(); 
    points->SetDataTypeToDouble();
    points->SetNumberOfPoints(ngrains);
    for (int ii = 0; ii < ngrains; ++ii) {
      points->SetPoint(ii, particle[ii].R.data()); 
    }

    // Create a polydata to store everything in
    vtkSmartPointer<vtkPolyData> polyData = vtkSmartPointer<vtkPolyData>::New();

    // add time
    // inspired from http://www.parresianz.com/c++/xml/vtk/vtk-output/
    auto array = vtkSmartPointer<vtkDoubleArray>::New();
    array->SetName("TIME");
    array->SetNumberOfTuples(1);
    array->SetTuple1(0, time);
    polyData->GetFieldData()->AddArray(array);

    // Add the points to the dataset
    polyData->SetPoints(points);

    // ADD POINT DATA
    // SCALARS
    // id
    vtkSmartPointer<vtkIntArray> id = vtkSmartPointer<vtkIntArray>::New();
    id->SetNumberOfComponents(1); id->SetNumberOfTuples(ngrains); id->SetName("id");
    for (int ii = 0; ii < ngrains; ++ii) { id->SetValue(ii, ii); }
    polyData->GetPointData()->AddArray(id);
    // color id
    srand48(0);
    vtkSmartPointer<vtkIntArray> color_id = vtkSmartPointer<vtkIntArray>::New();
    color_id->SetNumberOfComponents(1); color_id->SetNumberOfTuples(ngrains); color_id->SetName("color_id");
    for (int ii = 0; ii < ngrains; ++ii) { color_id->SetValue(ii, int(drand48()*100)); }
    polyData->GetPointData()->AddArray(color_id);
    // materialTag
    vtkSmartPointer<vtkIntArray> materialTag = vtkSmartPointer<vtkIntArray>::New();
    materialTag->SetNumberOfComponents(1); materialTag->SetNumberOfTuples(ngrains); materialTag->SetName("materialTag");
    for (int ii = 0; ii < ngrains; ++ii) { materialTag->SetValue(ii, particle[ii].materialTag); }
    polyData->GetPointData()->AddArray(materialTag);
    // bcTag
    vtkSmartPointer<vtkIntArray> bcTag = vtkSmartPointer<vtkIntArray>::New();
    bcTag->SetNumberOfComponents(1); bcTag->SetNumberOfTuples(ngrains); bcTag->SetName("bcTag");
    for (int ii = 0; ii < ngrains; ++ii) { bcTag->SetValue(ii, particle[ii].bcTag); }
    polyData->GetPointData()->AddArray(bcTag);
    // groupTag
    vtkSmartPointer<vtkIntArray> groupTag = vtkSmartPointer<vtkIntArray>::New();
    groupTag->SetNumberOfComponents(1); groupTag->SetNumberOfTuples(ngrains); groupTag->SetName("groupTag");
    for (int ii = 0; ii < ngrains; ++ii) { groupTag->SetValue(ii, particle[ii].groupTag); }
    polyData->GetPointData()->AddArray(groupTag);
    // type
    vtkSmartPointer<vtkIntArray> type = vtkSmartPointer<vtkIntArray>::New();
    type->SetNumberOfComponents(1); type->SetNumberOfTuples(ngrains); type->SetName("type");
    for (int ii = 0; ii < ngrains; ++ii) { type->SetValue(ii, particle[ii].type); }
    polyData->GetPointData()->AddArray(type);
    // alive
    vtkSmartPointer<vtkIntArray> alive = vtkSmartPointer<vtkIntArray>::New();
    alive->SetNumberOfComponents(1); alive->SetNumberOfTuples(ngrains); alive->SetName("alive");
    for (int ii = 0; ii < ngrains; ++ii) { alive->SetValue(ii, particle[ii].alive); }
    polyData->GetPointData()->AddArray(alive);
    // nc
    vtkSmartPointer<vtkIntArray> nc = vtkSmartPointer<vtkIntArray>::New();
    nc->SetNumberOfComponents(1); nc->SetNumberOfTuples(ngrains); nc->SetName("nc");
    for (int ii = 0; ii < ngrains; ++ii) { nc->SetValue(ii, particle[ii].nc); }
    polyData->GetPointData()->AddArray(nc);
    // radius
    vtkSmartPointer<vtkDoubleArray> radius = vtkSmartPointer<vtkDoubleArray>::New();
    radius->SetNumberOfComponents(1); radius->SetNumberOfTuples(ngrains); radius->SetName("radius");
    for (int ii = 0; ii < ngrains; ++ii) { radius->SetValue(ii, particle[ii].rad); }
    polyData->GetPointData()->AddArray(radius);
    // mass
    vtkSmartPointer<vtkDoubleArray> mass = vtkSmartPointer<vtkDoubleArray>::New();
    mass->SetNumberOfComponents(1); mass->SetNumberOfTuples(ngrains); mass->SetName("mass");
    for (int ii = 0; ii < ngrains; ++ii) { mass->SetValue(ii, particle[ii].mass); }
    polyData->GetPointData()->AddArray(mass);
    // inerI
    vtkSmartPointer<vtkDoubleArray> inerI = vtkSmartPointer<vtkDoubleArray>::New();
    inerI->SetNumberOfComponents(1); inerI->SetNumberOfTuples(ngrains); inerI->SetName("inerI");
    for (int ii = 0; ii < ngrains; ++ii) { inerI->SetValue(ii, particle[ii].inerI); }
    polyData->GetPointData()->AddArray(inerI);

    // VECTORS 
    // Velocity
    vtkSmartPointer<vtkDoubleArray> Velocity = vtkSmartPointer<vtkDoubleArray>::New();
    Velocity->SetNumberOfComponents(3); Velocity->SetNumberOfTuples(ngrains); Velocity->SetName("Velocity");
    for (int ii = 0; ii < ngrains; ++ii) {
      //Velocity->SetTupleValue(ii, particle[ii].V.data()); 
      Velocity->SetTuple(ii, particle[ii].V.data()); 
    }
    polyData->GetPointData()->AddArray(Velocity);
    // Position
    vtkSmartPointer<vtkDoubleArray> Position = vtkSmartPointer<vtkDoubleArray>::New();
    Position->SetNumberOfComponents(3); Position->SetNumberOfTuples(ngrains); Position->SetName("Position");
    for (int ii = 0; ii < ngrains; ++ii) {
      Position->SetTuple(ii, particle[ii].R.data()); 
    }
    polyData->GetPointData()->AddArray(Position);
    // Velocity0
    vtkSmartPointer<vtkDoubleArray> Velocity0 = vtkSmartPointer<vtkDoubleArray>::New();
    Velocity0->SetNumberOfComponents(3); Velocity0->SetNumberOfTuples(ngrains); Velocity0->SetName("Velocity0");
    for (int ii = 0; ii < ngrains; ++ii) {
      Velocity0->SetTuple(ii, particle[ii].V.data()); 
    }
    polyData->GetPointData()->AddArray(Velocity0);
    // DeltaR1
    vtkSmartPointer<vtkDoubleArray> DeltaR1 = vtkSmartPointer<vtkDoubleArray>::New();
    DeltaR1->SetNumberOfComponents(3); DeltaR1->SetNumberOfTuples(ngrains); DeltaR1->SetName("DeltaR1");
    for (int ii = 0; ii < ngrains; ++ii) {
      Vec3d_t Rij(particle[ii].dx() - particle_init[ii].dx(), particle[ii].dy() - particle_init[ii].dy(), particle[ii].dz() - particle_init[ii].dz()); 
      DeltaR1->SetTuple(ii, Rij.data()); 
    }
    polyData->GetPointData()->AddArray(DeltaR1);
    // F
    vtkSmartPointer<vtkDoubleArray> F = vtkSmartPointer<vtkDoubleArray>::New();
    F->SetNumberOfComponents(3); F->SetNumberOfTuples(ngrains); F->SetName("Force");
    for (int ii = 0; ii < ngrains; ++ii) {
      F->SetTuple(ii, particle[ii].F.data()); 
    }
    polyData->GetPointData()->AddArray(F);
    // PHI
    vtkSmartPointer<vtkDoubleArray> PHI = vtkSmartPointer<vtkDoubleArray>::New();
    PHI->SetNumberOfComponents(3); PHI->SetNumberOfTuples(ngrains); PHI->SetName("PHI");
    for (int ii = 0; ii < ngrains; ++ii) {
      PHI->SetTuple(ii, particle[ii].PHI.data()); 
    }
    polyData->GetPointData()->AddArray(PHI);
    // W
    vtkSmartPointer<vtkDoubleArray> W = vtkSmartPointer<vtkDoubleArray>::New();
    W->SetNumberOfComponents(3); W->SetNumberOfTuples(ngrains); W->SetName("Angular Velocity");
    for (int ii = 0; ii < ngrains; ++ii) {
      W->SetTuple(ii, particle[ii].W.data()); 
    }
    polyData->GetPointData()->AddArray(W);
    // T
    vtkSmartPointer<vtkDoubleArray> T = vtkSmartPointer<vtkDoubleArray>::New();
    T->SetNumberOfComponents(3); T->SetNumberOfTuples(ngrains); T->SetName("Torque");
    for (int ii = 0; ii < ngrains; ++ii) {
      T->SetTuple(ii, particle[ii].T.data()); 
    }
    polyData->GetPointData()->AddArray(T);

    // Write the file
    vtkSmartPointer<vtkXMLPolyDataWriter> writer = vtkSmartPointer<vtkXMLPolyDataWriter>::New();
    writer->SetFileName(filename.c_str());
#if VTK_MAJOR_VERSION <= 5
    writer->SetInput(polyData);
#else
    writer->SetInputData(polyData);
#endif
    //// Optional - set the mode.
    writer->SetDataModeToBinary(); // default
    //writer->SetDataModeToAscii();
    writer->SetCompressorTypeToZLib(); // or ToNone
    // write
    writer->Write();    

    return EXIT_SUCCESS;
  }

  int 
  Visualization::print_VTK_vtp_contact_lines(const std::vector<Particle> & particle, const std::vector<Contact> & contacts, const bool periodic[], const std::string & filename, double time)
  {
    // avois unused vars warnings
    _UNUSED(particle); 
    _UNUSED(contacts); 
    _UNUSED(periodic); 
    _UNUSED(filename); 
    // SEE : 
    //http://www.vtk.org/Wiki/VTK/Examples/Cxx/GeometricObjects/Line
    //http://www.vtk.org/Wiki/VTK/Examples/Cxx/PolyData/IterateOverLines

    // some vars
    const int ngrains = particle.size() - DEM::NWALLS; 
    const int ncontacts = contacts.size();
    // periodic limits
    double l[3] = {0};
    if (true == periodic[0]) l[0] = particle[ngrains + RIGHT].R[0] - particle[ngrains + LEFT].R[0]; 
    if (true == periodic[1]) l[1] = particle[ngrains + FRONT].R[1] - particle[ngrains + BACK].R[1]; 
    if (true == periodic[2]) l[2] = particle[ngrains + TOP].R[2] - particle[ngrains + BOTTOM].R[2]; 

    // Create a vtkPoints object and store the points in it
    vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
    points->SetDataTypeToDouble();
    Vec3d_t Ri, Rij, Rj;
    for (auto & c : contacts) {
      Ri = particle[c.uid1_].R;
      Rij = Ri;
      // correct walls
      if ((c.uid2_) < ngrains) Rij -= particle[c.uid2_].R;
      else Rij -= Ri - (particle[c.uid1_].rad - (c.delta_))*(c.Normal_); // UPDATE if def of radinew changes
      // correct periodicity
      if ((c.uid2_) < ngrains) 
	for (int dir = 0; dir <= 2; ++dir) 
	  if (true == periodic[dir]) 
	    Rij[dir] -= l[dir]*lrint(Rij[dir]/l[dir]);
      // finally write ;)
      Rj = Ri - Rij;
      points->InsertNextPoint(Ri[0], Ri[1], Ri[2]); 
      points->InsertNextPoint(Rj[0], Rj[1], Rj[2]); 
    }
    const auto npoints = points->GetNumberOfPoints();

    // Create a cell array to store the lines in and add the lines to it
    vtkSmartPointer<vtkCellArray> lines = vtkSmartPointer<vtkCellArray>::New();
    
    // create each line according to contact
    for(int ip = 0; ip < npoints; ip += 2) {
      vtkSmartPointer<vtkLine> line = vtkSmartPointer<vtkLine>::New();
      line->GetPointIds()->SetId(0, ip);
      line->GetPointIds()->SetId(1, ip+1);
      lines->InsertNextCell(line);
    }

    // Create a polydata to store everything in
    vtkSmartPointer<vtkPolyData> polyData = vtkSmartPointer<vtkPolyData>::New();
    // Add the points to the dataset
    polyData->SetPoints(points);
    // Add the lines to the dataset
    polyData->SetLines(lines);
    ASSERT(ncontacts == polyData->GetNumberOfLines());
    
    // add time
    // inspired from http://www.parresianz.com/c++/xml/vtk/vtk-output/
    auto array = vtkSmartPointer<vtkDoubleArray>::New();
    array->SetName("TIME");
    array->SetNumberOfTuples(1);
    array->SetTuple1(0, time);
    polyData->GetFieldData()->AddArray(array);


    // ADD CELL DATA
    // SEE: http://www.vtk.org/Wiki/VTK/Examples/Cxx/PolyData/MiscCellData
    // SCALARS
    // uid1
    vtkSmartPointer<vtkIntArray> uid1 = 
      vtkSmartPointer<vtkIntArray>::New();
    uid1->SetNumberOfComponents(1); 
    uid1->SetNumberOfTuples(ncontacts); 
    uid1->SetName("uid1");
    for (int ic = 0; ic < ncontacts; ++ic) {
      uid1->SetValue(ic, contacts[ic].uid1_); 
    }
    polyData->GetCellData()->AddArray(uid1);
    // uid2
    vtkSmartPointer<vtkIntArray> uid2 = 
    vtkSmartPointer<vtkIntArray>::New();
    uid2->SetNumberOfComponents(1); 
    uid2->SetNumberOfTuples(ncontacts); 
    uid2->SetName("uid2");
    for (int ic = 0; ic < ncontacts; ++ic) {
      uid2->SetValue(ic, contacts[ic].uid2_); 
    }
    polyData->GetCellData()->AddArray(uid2);
    // residual_
    vtkSmartPointer<vtkIntArray> residual = 
      vtkSmartPointer<vtkIntArray>::New();
    residual->SetNumberOfComponents(1); 
    residual->SetNumberOfTuples(ncontacts); 
    residual->SetName("residual");
    for (int ic = 0; ic < ncontacts; ++ic) {
      residual->SetValue(ic, contacts[ic].residual_); 
    }
    polyData->GetCellData()->AddArray(residual);
    // force magnitude
    vtkSmartPointer<vtkDoubleArray> fmag = 
      vtkSmartPointer<vtkDoubleArray>::New();
    fmag->SetNumberOfComponents(1); 
    fmag->SetNumberOfTuples(ncontacts); 
    fmag->SetName("Force Magnitude");
    for (int ic = 0; ic < ncontacts; ++ic) {
      fmag->SetValue(ic, contacts[ic].F().norm()); 
    }
    polyData->GetCellData()->AddArray(fmag);
    // delta
    vtkSmartPointer<vtkDoubleArray> delta = 
    vtkSmartPointer<vtkDoubleArray>::New();
    delta->SetNumberOfComponents(1); 
    delta->SetNumberOfTuples(ncontacts); 
    delta->SetName("delta");
    for (int ic = 0; ic < ncontacts; ++ic) {
      delta->SetValue(ic, contacts[ic].delta_); 
    }
    polyData->GetCellData()->AddArray(delta);
    // normFn
    vtkSmartPointer<vtkDoubleArray> normFn = 
    vtkSmartPointer<vtkDoubleArray>::New();
    normFn->SetNumberOfComponents(1); 
    normFn->SetNumberOfTuples(ncontacts); 
    normFn->SetName("normFn");
    for (int ic = 0; ic < ncontacts; ++ic) {
      normFn->SetValue(ic, contacts[ic].normFn_); 
    }
    polyData->GetCellData()->AddArray(normFn);


    // VECTORS
    // Normal
    vtkSmartPointer<vtkDoubleArray> Normal = 
    vtkSmartPointer<vtkDoubleArray>::New();
    Normal->SetNumberOfComponents(3); 
    Normal->SetNumberOfTuples(ncontacts); 
    Normal->SetName("Normal");
    for (int ic = 0; ic < ncontacts; ++ic) {
      Normal->SetTuple(ic, contacts[ic].Normal_.data()); 
    }
    polyData->GetCellData()->AddArray(Normal);
    // Fn
    vtkSmartPointer<vtkDoubleArray> Fn = 
    vtkSmartPointer<vtkDoubleArray>::New();
    Fn->SetNumberOfComponents(3); 
    Fn->SetNumberOfTuples(ncontacts); 
    Fn->SetName("Fn");
    for (int ic = 0; ic < ncontacts; ++ic) {
      Fn->SetTuple(ic, contacts[ic].Fn().data()); 
    }
    polyData->GetCellData()->AddArray(Fn);
    // Ft
    vtkSmartPointer<vtkDoubleArray> Ft = 
    vtkSmartPointer<vtkDoubleArray>::New();
    Ft->SetNumberOfComponents(3); 
    Ft->SetNumberOfTuples(ncontacts); 
    Ft->SetName("Ft");
    for (int ic = 0; ic < ncontacts; ++ic) {
      Ft->SetTuple(ic, contacts[ic].Ft_.data()); 
    }
    polyData->GetCellData()->AddArray(Ft);
    // F
    vtkSmartPointer<vtkDoubleArray> F = 
    vtkSmartPointer<vtkDoubleArray>::New();
    F->SetNumberOfComponents(3); 
    F->SetNumberOfTuples(ncontacts); 
    F->SetName("F");
    for (int ic = 0; ic < ncontacts; ++ic) {
      F->SetTuple(ic, contacts[ic].F().data()); 
    }
    polyData->GetCellData()->AddArray(F);
    // T
    vtkSmartPointer<vtkDoubleArray> T = 
    vtkSmartPointer<vtkDoubleArray>::New();
    T->SetNumberOfComponents(3); 
    T->SetNumberOfTuples(ncontacts); 
    T->SetName("T");
    for (int ic = 0; ic < ncontacts; ++ic) {
      T->SetTuple(ic, contacts[ic].T_.data()); 
    }
    polyData->GetCellData()->AddArray(T);
    // SpringTorsion
    vtkSmartPointer<vtkDoubleArray> SpringTorsion = 
    vtkSmartPointer<vtkDoubleArray>::New();
    SpringTorsion->SetNumberOfComponents(3); 
    SpringTorsion->SetNumberOfTuples(ncontacts); 
    SpringTorsion->SetName("SpringTorsion");
    for (int ic = 0; ic < ncontacts; ++ic) {
      SpringTorsion->SetTuple(ic, contacts[ic].SpringTorsion_.data()); 
    }
    polyData->GetCellData()->AddArray(SpringTorsion);
    // SpringSlide
    vtkSmartPointer<vtkDoubleArray> SpringSlide = 
    vtkSmartPointer<vtkDoubleArray>::New();
    SpringSlide->SetNumberOfComponents(3); 
    SpringSlide->SetNumberOfTuples(ncontacts); 
    SpringSlide->SetName("SpringSlide");
    for (int ic = 0; ic < ncontacts; ++ic) {
      SpringSlide->SetTuple(ic, contacts[ic].SpringSlide_.data()); 
    }
    polyData->GetCellData()->AddArray(SpringSlide);
    // SpringRolling
    vtkSmartPointer<vtkDoubleArray> SpringRolling = 
    vtkSmartPointer<vtkDoubleArray>::New();
    SpringRolling->SetNumberOfComponents(3); 
    SpringRolling->SetNumberOfTuples(ncontacts); 
    SpringRolling->SetName("SpringRolling");
    for (int ic = 0; ic < ncontacts; ++ic) {
      SpringRolling->SetTuple(ic, contacts[ic].SpringRolling_.data()); 
    }
    polyData->GetCellData()->AddArray(SpringRolling);

  
    // Write the file
    vtkSmartPointer<vtkXMLPolyDataWriter> writer = vtkSmartPointer<vtkXMLPolyDataWriter>::New();
    writer->SetFileName(filename.c_str());
#if VTK_MAJOR_VERSION <= 5
    writer->SetInput(polyData);
#else
    writer->SetInputData(polyData);
#endif
    //// Optional - set the mode.
    writer->SetDataModeToBinary(); // default
    //writer->SetDataModeToAscii();
    writer->SetCompressorTypeToZLib(); // or ToNone
    // write
    writer->Write();    

    return EXIT_SUCCESS;
  }
#endif // USE_VTK

#ifndef USE_VTK
  void 
  Visualization::print_paraview_vtp_points(const std::vector<Particle> & particle, const std::vector<Particle> & particle_init, std::ofstream & fout)
  {
    srand48(0);
    const int ngrains = particle.size() - DEM::NWALLS; 
    print_log("particle.size() = ", particle.size());
    print_log("particle_init.size() = ", particle_init.size());
    ASSERT(ngrains == int(particle_init.size() - DEM::NWALLS));
    // Print the points (disks or spheres by glyph filter)
    // points
    fout << "      <Points>" << "\n";
    fout << "        <DataArray type=\"Float64\" NumberOfComponents=\"3\" format=\"ascii\"> " << "\n";
    fout << "          " ;
    for (int id = 0; id < ngrains; ++id) {
      fout << particle[id].R << "  "; 
    }
    fout << "\n";
    fout << "        </DataArray>" << "\n";
    fout << "      </Points>" << "\n";
    // point attributes
    fout << "      <PointData Scalars=\"radius\" Vectors=\"Velocity\">" << "\n";
    
    print_paraview_vtp_body_data(particle, particle_init, fout, 0, ngrains);
    
    // footer
    fout << "      </PointData>" << "\n";
  }

  void 
  Visualization::print_paraview_vtp_contact_lines(const std::vector<Particle> & particle, const std::vector<Contact> & contacts, std::ofstream & fout, const bool periodic[])
  {
    std::vector<Contact>::const_iterator it_c;
    const int ncontacts = contacts.size();
    const long ngrains = particle.size() - DEM::NWALLS; 
   
    double l[3] = {0};
    if (true == periodic[0]) l[0] = particle[ngrains + RIGHT].R[0] - particle[ngrains + LEFT].R[0]; 
    if (true == periodic[1]) l[1] = particle[ngrains + FRONT].R[1] - particle[ngrains + BACK].R[1]; 
    if (true == periodic[2]) l[2] = particle[ngrains + TOP].R[2] - particle[ngrains + BOTTOM].R[2]; 

    // Points
    fout << "      <Points>" << "\n";
    fout << "        <DataArray type=\"Float64\" NumberOfComponents=\"3\" format=\"ascii\"> " << "\n";
    fout << "          " ;
    for (it_c = contacts.begin(); it_c != contacts.end(); ++it_c) { // I NEED TO PRINT DIFFERENT FOR WALLS
      Vec3d_t Ri = particle[it_c->uid1_].R;
      Vec3d_t Rij = Ri;
      ASSERT(it_c->delta_ > 0); // checking real penetration
      if ((it_c->uid2_) < ngrains) Rij -= particle[it_c->uid2_].R;
      else Rij -= Ri - (particle[it_c->uid1_].rad - (it_c->delta_))*(it_c->Normal_); // UPDATE if def of radinew changes
      if ((it_c->uid2_) < ngrains) 
	for (int dir = 0; dir <= 2; ++dir) 
	  if (true == periodic[dir]) 
	    Rij[dir] -= l[dir]*lrint(Rij[dir]/l[dir]);
      // finally write :)
      fout << "  " << Ri << "  "  << Ri - Rij;
    }
    fout << "\n";
    fout << "        </DataArray>" << "\n";
    fout << "      </Points>" << "\n";
    // Contacts: Lines to be filter with tubes in paraview
    fout << "      <Lines>" << "\n";
    fout << "        <DataArray type=\"Int32\" Name=\"connectivity\" format=\"ascii\"> " << "\n";
    fout << "          ";
    for(int count = 0; count < 2*ncontacts; count+=2) { // connected in the same print order : 0 (->) 1 2 (->) 3 ... 
      fout << "  " << count << "  " << count + 1;
    }
    fout << "\n";
    fout << "        </DataArray> " << "\n";
    fout << "        <DataArray type=\"Int32\" Name=\"offsets\" format=\"ascii\"> " << "\n";
    fout << "          ";
    for (int count1 = 0, count2 = 2; count1 < ncontacts; ++count1, count2 += 2) {
      fout << "  " << count2;
    }
    fout << "\n";
    fout << "        </DataArray> " << "\n";
    fout << "      </Lines>" << "\n";

    // POINT DATA : 

    //fout << "      <PointData Scalars=\"force_mag\" Vectors=\"Vij\">" << "\n";
    fout << "      <PointData>" << "\n";
    // point attributes : scalars
    fout << "        <DataArray type=\"Float64\" Name=\"force_mag\" format=\"ascii\">" << "\n";
    fout << "          " ;
    for (it_c = contacts.begin(); it_c != contacts.end(); ++it_c) {
      fout << it_c->F().norm() << "  ";
      fout << it_c->F().norm() << "  ";
    }
    fout << "\n";
    fout << "        </DataArray>" << "\n";
    fout << "        <DataArray type=\"Float64\" Name=\"Ft\"  NumberOfComponents=\"3\" format=\"ascii\">" << "\n";
    fout << "          " ;
    for (it_c = contacts.begin(); it_c != contacts.end(); ++it_c) {
      fout << it_c->Ft_ << "  ";
      Vec3d_t tmp = -1.0*(it_c->Ft_);
      fout << tmp << "  ";
    }
    fout << "\n";
    fout << "        </DataArray>" << "\n";
    fout << "      </PointData>" << "\n";

    // CELL DATA : Contact info

    //fout << "      <CellData Scalars=\"forceMagnitude\" Vectors=\"Vij\" Normals=\"Normal\">" << "\n";
    fout << "      <CellData>" << "\n";
    // scalars
    fout << "        <DataArray type=\"Float64\" Name=\"forceMagnitude\" format=\"ascii\">" << "\n";
    fout << "          " ;
    for (it_c = contacts.begin(); it_c != contacts.end(); ++it_c) {
      fout << it_c->F().norm() << "  ";
    }
    fout << "\n"; 
    fout << "        </DataArray>" << "\n";

    fout << "        <DataArray type=\"Int32\" Name=\"uid1\" format=\"ascii\">" << "\n";
    fout << "          " ;
    for (it_c = contacts.begin(); it_c != contacts.end(); ++it_c) {
      fout << it_c->uid1_ << "  ";
    }
    fout << "\n"; 
    fout << "        </DataArray>" << "\n";

    fout << "        <DataArray type=\"Int32\" Name=\"uid2\" format=\"ascii\">" << "\n";
    fout << "          " ;
    for (it_c = contacts.begin(); it_c != contacts.end(); ++it_c) {
      fout << it_c->uid2_ << "  ";
    }
    fout << "\n"; 
    fout << "        </DataArray>" << "\n";

    fout << "        <DataArray type=\"Int32\" Name=\"residual\" format=\"ascii\">" << "\n";
    fout << "          " ;
    for (it_c = contacts.begin(); it_c != contacts.end(); ++it_c) {
      fout << it_c->residual_ << "  ";
    }
    fout << "\n"; 
    fout << "        </DataArray>" << "\n";

    fout << "        <DataArray type=\"Float64\" Name=\"delta\" format=\"ascii\">" << "\n";
    fout << "          " ;
    for (it_c = contacts.begin(); it_c != contacts.end(); ++it_c) {
      fout << it_c->delta_ << "  ";
    }
    fout << "\n"; 
    fout << "        </DataArray>" << "\n";
    fout << "        <DataArray type=\"Float64\" Name=\"normFn\" format=\"ascii\">" << "\n";
    fout << "          " ;
    for (it_c = contacts.begin(); it_c != contacts.end(); ++it_c) {
      fout << it_c->normFn_ << "  ";
    }
    fout << "\n"; 
    fout << "        </DataArray>" << "\n";

    // vectors
    fout << "        <DataArray type=\"Float64\" Name=\"Normal\" NumberOfComponents=\"3\" format=\"ascii\">" << "\n";
    fout << "          " ;
    for (it_c = contacts.begin(); it_c != contacts.end(); ++it_c) {
      fout << it_c->Normal_ << "  ";
    }
    fout << "\n";
    fout << "        </DataArray>" << "\n"; 
    /*fout << "        <DataArray type=\"Float64\" Name=\"Vij\" NumberOfComponents=\"3\" format=\"ascii\">" << "\n";
    fout << "          " ;
    for (it_c = contacts.begin(); it_c != contacts.end(); ++it_c) {
      fout << it_c->Vij << "  ";
    }
    fout << "\n";
    fout << "        </DataArray>" << "\n";
    fout << "        <DataArray type=\"Float64\" Name=\"Vn\" NumberOfComponents=\"3\" format=\"ascii\">" << "\n";
    fout << "          " ;
    for (it_c = contacts.begin(); it_c != contacts.end(); ++it_c) {
      fout << it_c->Vn << "  ";
    }
    fout << "\n";
    fout << "        </DataArray>" << "\n";
    fout << "        <DataArray type=\"Float64\" Name=\"Vs\" NumberOfComponents=\"3\" format=\"ascii\">" << "\n";
    fout << "          " ;
    for (it_c = contacts.begin(); it_c != contacts.end(); ++it_c) {
      fout << it_c->Vs << "  ";
    }
    fout << "\n";
    fout << "        </DataArray>" << "\n";
    fout << "        <DataArray type=\"Float64\" Name=\"Vr\" NumberOfComponents=\"3\" format=\"ascii\">" << "\n";
    fout << "          " ;
    for (it_c = contacts.begin(); it_c != contacts.end(); ++it_c) {
      fout << it_c->Vr << "  ";
    }
    fout << "\n";
    fout << "        </DataArray>" << "\n";
    fout << "        <DataArray type=\"Float64\" Name=\"Vo\" NumberOfComponents=\"3\" format=\"ascii\">" << "\n";
    fout << "          " ;
    for (it_c = contacts.begin(); it_c != contacts.end(); ++it_c) {
      fout << it_c->Vo << "  ";
    }
    fout << "\n";
    fout << "        </DataArray>" << "\n";

    fout << "        <DataArray type=\"Float64\" Name=\"Fs\" NumberOfComponents=\"3\" format=\"ascii\">" << "\n";
    fout << "          " ;
    for (it_c = contacts.begin(); it_c != contacts.end(); ++it_c) {
      fout << it_c->Fs << "  ";
    }
    fout << "\n";
    fout << "        </DataArray>" << "\n";

    fout << "        <DataArray type=\"Float64\" Name=\"Ts\" NumberOfComponents=\"3\" format=\"ascii\">" << "\n";
    fout << "          " ;
    for (it_c = contacts.begin(); it_c != contacts.end(); ++it_c) {
      fout << it_c->Ts << "  ";
    }
    fout << "\n";
    fout << "        </DataArray>" << "\n";

    fout << "        <DataArray type=\"Float64\" Name=\"Tr\" NumberOfComponents=\"3\" format=\"ascii\">" << "\n";
    fout << "          " ;
    for (it_c = contacts.begin(); it_c != contacts.end(); ++it_c) {
      fout << it_c->Tr << "  ";
    }
    fout << "\n";
    fout << "        </DataArray>" << "\n";

    fout << "        <DataArray type=\"Float64\" Name=\"To\" NumberOfComponents=\"3\" format=\"ascii\">" << "\n";
    fout << "          " ;
    for (it_c = contacts.begin(); it_c != contacts.end(); ++it_c) {
      fout << it_c->To << "  ";
    }
    fout << "\n";
    fout << "        </DataArray>" << "\n";
    */
    fout << "        <DataArray type=\"Float64\" Name=\"Fn\" NumberOfComponents=\"3\" format=\"ascii\">" << "\n";
    fout << "          " ;
    for (it_c = contacts.begin(); it_c != contacts.end(); ++it_c) {
      fout << it_c->Fn() << "  ";
    }
    fout << "\n";
    fout << "        </DataArray>" << "\n";
    fout << "        <DataArray type=\"Float64\" Name=\"Ft\" NumberOfComponents=\"3\" format=\"ascii\">" << "\n";
    fout << "          " ;
    for (it_c = contacts.begin(); it_c != contacts.end(); ++it_c) {
      fout << it_c->Ft_ << "  ";
    }
    fout << "\n";
    fout << "        </DataArray>" << "\n";
    fout << "        <DataArray type=\"Float64\" Name=\"F\" NumberOfComponents=\"3\" format=\"ascii\">" << "\n";
    fout << "          " ;
    for (it_c = contacts.begin(); it_c != contacts.end(); ++it_c) {
      fout << it_c->F() << "  ";
    }
    fout << "\n";
    fout << "        </DataArray>" << "\n";
    fout << "        <DataArray type=\"Float64\" Name=\"T\" NumberOfComponents=\"3\" format=\"ascii\">" << "\n";
    fout << "          " ;
    for (it_c = contacts.begin(); it_c != contacts.end(); ++it_c) {
      fout << it_c->T_ << "  ";
    }
    fout << "\n";
    fout << "        </DataArray>" << "\n";
    fout << "        <DataArray type=\"Float64\" Name=\"SpringTorsion\" NumberOfComponents=\"3\" format=\"ascii\">" << "\n";
    fout << "          " ;
    for (it_c = contacts.begin(); it_c != contacts.end(); ++it_c) {
	fout << it_c->SpringTorsion_ << "  ";
    }
    fout << "\n";
    fout << "        </DataArray>" << "\n";
    fout << "        <DataArray type=\"Float64\" Name=\"Springslide\" NumberOfComponents=\"3\" format=\"ascii\">" << "\n";
    fout << "          " ;
    for (it_c = contacts.begin(); it_c != contacts.end(); ++it_c) {
      fout << it_c->SpringSlide_ << "  ";
    }
    fout << "\n";
    fout << "        </DataArray>" << "\n";
    fout << "        <DataArray type=\"Float64\" Name=\"SpringRolling\" NumberOfComponents=\"3\" format=\"ascii\">" << "\n";
    fout << "          " ;
    for (it_c = contacts.begin(); it_c != contacts.end(); ++it_c) {
      fout << it_c->SpringRolling_ << "  ";
    }
    fout << "\n";
    fout << "        </DataArray>" << "\n";

    // end cell data
    fout << "      </CellData>" << "\n";
  }

#endif // n USE_VTK

  void
  Visualization::print_paraview_vtp_body_data(const std::vector<Particle> & particle, 
					      const std::vector<Particle> & particle_init, 
					      std::ofstream & fout, 
					      const int & start, const int & end, const int & reps)
  {
    // point attributes : scalars
    fout << "        <DataArray type=\"Int32\" Name=\"id\" format=\"ascii\">" << "\n";
    fout << "          " ;
    for (int id = start; id < end; ++id) {
      for (int irep = 0; irep < reps; ++irep) 
	fout << id << "  "; 
    }
    fout << "\n";
    fout << "        </DataArray>" << "\n";
    fout << "        <DataArray type=\"Float64\" Name=\"color_id\" format=\"ascii\">" << "\n";
    fout << "          " ;
    for (int id = start; id < end; ++id) {
      const int color = drand48()*100;
      for (int irep = 0; irep < reps; ++irep) 
	fout << color << "  "; 
    }
    fout << "\n";
    fout << "        </DataArray>" << "\n";
    fout << "        <DataArray type=\"Float64\" Name=\"radius\" format=\"ascii\">" << "\n";
    fout << "          " ;
    for (int id = start; id < end; ++id) {
      for (int irep = 0; irep < reps; ++irep) 
	fout << particle[id].rad << "  "; 
    }
    fout << "\n";
    fout << "        </DataArray>" << "\n";
    fout << "        <DataArray type=\"Float64\" Name=\"diameter\" format=\"ascii\">" << "\n";
    fout << "          " ;
    for (int id = start; id < end; ++id) {
      for (int irep = 0; irep < reps; ++irep) 
	fout << 2*particle[id].rad << "  "; 
    }
    fout << "\n"; 
    fout << "        </DataArray>" << "\n";
    fout << "        <DataArray type=\"Float64\" Name=\"mass\" format=\"ascii\">" << "\n";
    fout << "          " ;
    for (int id = start; id < end; ++id) {
      for (int irep = 0; irep < reps; ++irep) 
	fout << particle[id].mass << "  "; 
    }
    fout << "\n";
    fout << "        </DataArray>" << "\n";
    fout << "        <DataArray type=\"Float64\" Name=\"inerI\" format=\"ascii\">" << "\n";
    fout << "          " ;
    for (int id = start; id < end; ++id) {
      for (int irep = 0; irep < reps; ++irep) 
	fout << particle[id].inerI << "  "; 
    }
    fout << "\n";
    fout << "        </DataArray>" << "\n";
    fout << "        <DataArray type=\"Int32\" Name=\"materialTag\" format=\"ascii\">" << "\n";
    fout << "          " ;
    for (int id = start; id < end; ++id) {
      for (int irep = 0; irep < reps; ++irep) 
	fout << particle[id].materialTag << "  "; 
    }
    fout << "\n";
    fout << "        </DataArray>" << "\n";
    fout << "        <DataArray type=\"Int32\" Name=\"bcTag\" format=\"ascii\">" << "\n";
    fout << "          " ;
    for (int id = start; id < end; ++id) {
      for (int irep = 0; irep < reps; ++irep) 
	fout << particle[id].bcTag << "  "; 
    }
    fout << "\n";
    fout << "        </DataArray>" << "\n";
    fout << "        <DataArray type=\"Int32\" Name=\"groupTag\" format=\"ascii\">" << "\n";
    fout << "          " ;
    for (int id = start; id < end; ++id) {
      for (int irep = 0; irep < reps; ++irep) 
	fout << particle[id].groupTag << "  "; 
    }
    fout << "\n";
    fout << "        </DataArray>" << "\n";
    fout << "        <DataArray type=\"Int32\" Name=\"type\" format=\"ascii\">" << "\n";
    fout << "          " ;
    for (int id = start; id < end; ++id) {
      for (int irep = 0; irep < reps; ++irep) 
	fout << particle[id].type << "  "; 
    }
    fout << "\n";
    fout << "        </DataArray>" << "\n";
    fout << "        <DataArray type=\"Int32\" Name=\"alive\" format=\"ascii\">" << "\n";
    fout << "          " ;
    for (int id = start; id < end; ++id) {
      for (int irep = 0; irep < reps; ++irep) 
	fout << particle[id].alive << "  "; 
    }
    fout << "\n";
    fout << "        </DataArray>" << "\n";
    fout << "        <DataArray type=\"Int32\" Name=\"nc\" format=\"ascii\">" << "\n";
    fout << "          " ;
    for (int id = start; id < end; ++id) {
      for (int irep = 0; irep < reps; ++irep) 
	fout << particle[id].nc << "  "; 
    }
    fout << "\n";
    fout << "        </DataArray>" << "\n";
    // point attributes: Vectors
    fout << "        <DataArray type=\"Float64\" Name=\"Velocity\" NumberOfComponents=\"3\" format=\"ascii\">" << "\n";
    fout << "          " ;
    for (int id = start; id < end; ++id) {
      for (int irep = 0; irep < reps; ++irep) 
	fout << particle[id].V << "  "; 
    }
    fout << "\n";
    fout << "        </DataArray>" << "\n";
    fout << "        <DataArray type=\"Float64\" Name=\"Position\" NumberOfComponents=\"3\" format=\"ascii\">" << "\n";
    fout << "          " ;
    for (int id = start; id < end; ++id) {
      for (int irep = 0; irep < reps; ++irep) 
    	fout << particle[id].R << "  "; 
    }
    fout << "\n";
    fout << "        </DataArray>" << "\n";
    fout << "        <DataArray type=\"Float64\" Name=\"Velocity0\" NumberOfComponents=\"3\" format=\"ascii\">" << "\n";
    fout << "          " ;
    for (int id = start; id < end; ++id) {
      for (int irep = 0; irep < reps; ++irep) 
    	fout << particle[id].V << "  "; 
    }
    fout << "\n";
    fout << "        </DataArray>" << "\n";
    fout << "        <DataArray type=\"Float64\" Name=\"Delta_R1\" NumberOfComponents=\"3\" format=\"ascii\">" << "\n";
    fout << "          " ;
    for (int id = start; id < end; ++id) {
      for (int irep = 0; irep < reps; ++irep) 
    	fout << particle[id].dx() - particle_init[id].dx() << "  " 
    	     << particle[id].dy() - particle_init[id].dy() << "  " 
    	     << particle[id].dz() - particle_init[id].dz() << "  "; 
    }
    fout << "\n";
    fout << "        </DataArray>" << "\n";
    fout << "        <DataArray type=\"Float64\" Name=\"Delta_R2\" NumberOfComponents=\"3\" format=\"ascii\">" << "\n";
    fout << "          " ;
    for (int id = start; id < end; ++id) {
      for (int irep = 0; irep < reps; ++irep) 
    	fout << particle[id].dx() - particle_init[id].R0[0] << "  " 
    	     << particle[id].dy() - particle_init[id].R0[1] << "  " 
    	     << particle[id].dz() - particle_init[id].R0[2] << "  "; 
    }
    fout << "\n";
    fout << "        </DataArray>" << "\n";
    fout << "        <DataArray type=\"Float64\" Name=\"Delta_R3\" NumberOfComponents=\"3\" format=\"ascii\">" << "\n";
    fout << "          " ;
    for (int id = start; id < end; ++id) {
      for (int irep = 0; irep < reps; ++irep) 
    	fout << particle[id].dx() << "  " << particle[id].dy() << "  " << particle[id].dz() << "  "; 
    }
    fout << "\n";
    fout << "        </DataArray>" << "\n";
    fout << "        <DataArray type=\"Float64\" Name=\"Force_Total\" NumberOfComponents=\"3\" format=\"ascii\">" << "\n";
    fout << "          " ;
    for (int id = start; id < end; ++id) {
      for (int irep = 0; irep < reps; ++irep) 
    	fout << particle[id].F << "  "; 
    }
    fout << "\n";
    fout << "        </DataArray>" << "\n";

    // Fixed Force
    fout << "        <DataArray type=\"Float64\" Name=\"Force_fixed\" NumberOfComponents=\"3\" format=\"ascii\">" << "\n";
    fout << "          " ;
    for (int id = start; id < end; ++id) {
      for (int irep = 0; irep < reps; ++irep) 
    	fout << particle[id].F_fixed << "  "; 
    }
    fout << "\n";
    fout << "        </DataArray>" << "\n";

    fout << "        <DataArray type=\"Float64\" Name=\"PHI\" NumberOfComponents=\"3\" format=\"ascii\">" << "\n";
    fout << "          " ;
    for (int id = start; id < end; ++id) {
      Vec3d_t tmp = particle[id].PHI*(180.0/M_PI);
      for (int irep = 0; irep < reps; ++irep) 
    	fout << tmp << "  "; 
    }
    fout << "\n";
    fout << "        </DataArray>" << "\n";
    fout << "        <DataArray type=\"Float64\" Name=\"Angular_Velocity\" NumberOfComponents=\"3\" format=\"ascii\">" << "\n";
    fout << "          " ;
    for (int id = start; id < end; ++id) {
      for (int irep = 0; irep < reps; ++irep) 
    	fout << particle[id].W << "  "; 
    }
    fout << "\n";
    fout << "        </DataArray>" << "\n";
    fout << "        <DataArray type=\"Float64\" Name=\"Torque\" NumberOfComponents=\"3\" format=\"ascii\">" << "\n";
    fout << "          " ;
    for (int id = start; id < end; ++id) {
      for (int irep = 0; irep < reps; ++irep) 
    	fout << particle[id].T << "  "; 
    }
    fout << "\n";
    fout << "        </DataArray>" << "\n";
  }

  void 
  Visualization::print_paraview_vtp_planes(const std::vector<Particle> & particle, const std::vector<Particle> & particle_init, std::ofstream & fout)
  {
    const int ngrains = particle.size() - DEM::NWALLS; 
    const double xmin = particle[ngrains + LEFT  ].x();
    const double xmax = particle[ngrains + RIGHT ].x();
    const double ymin = particle[ngrains + BACK  ].y();
    const double ymax = particle[ngrains + FRONT ].y();
    const double zmin = particle[ngrains + BOTTOM].z();
    const double zmax = particle[ngrains + TOP   ].z();
    double lmin[3], lmax[3];
    const double alpha = 1.05; ASSERT(alpha >= 1.0);  // extension factor for length dimension
    const double beta  = 0.01; ASSERT(beta  >= 0.0); // thinkness factor
    // bottom plane
    lmin[0] = 0.5*(xmax + xmin - alpha*(xmax - xmin)); lmax[0] = 0.5*(xmax + xmin + alpha*(xmax - xmin)); 
    lmin[1] = 0.5*(ymax + ymin - alpha*(ymax - ymin)); lmax[1] = 0.5*(ymax + ymin + alpha*(ymax - ymin)); 
    lmin[2] = zmin - beta*std::min(lmax[0] - lmin[0], lmax[1] - lmin[1]); lmax[2] = zmin; 
    print_paraview_vtp_planebox(lmin, lmax, particle, particle_init, fout, ngrains + DEM::BOTTOM);
    // top plane
    lmin[0] = 0.5*(xmax + xmin - alpha*(xmax - xmin)); lmax[0] = 0.5*(xmax + xmin + alpha*(xmax - xmin)); 
    lmin[1] = 0.5*(ymax + ymin - alpha*(ymax - ymin)); lmax[1] = 0.5*(ymax + ymin + alpha*(ymax - ymin)); 
    lmin[2] = zmax; lmax[2] = zmax + beta*std::min(lmax[0] - lmin[0], lmax[1] - lmin[1]); 
    print_paraview_vtp_planebox(lmin, lmax, particle, particle_init, fout, ngrains + DEM::TOP);
    // left plane
    lmin[1] = 0.5*(ymax + ymin - alpha*(ymax - ymin)); lmax[1] = 0.5*(ymax + ymin + alpha*(ymax - ymin)); 
    lmin[2] = 0.5*(zmax + zmin - alpha*(zmax - zmin)); lmax[2] = 0.5*(zmax + zmin + alpha*(zmax - zmin)); 
    lmin[0] = xmin - beta*std::min(lmax[2] - lmin[2], lmax[1] - lmin[1]); lmax[0] = xmin; 
    print_paraview_vtp_planebox(lmin, lmax, particle, particle_init, fout, ngrains + DEM::LEFT);
    // right plane
    lmin[1] = 0.5*(ymax + ymin - alpha*(ymax - ymin)); lmax[1] = 0.5*(ymax + ymin + alpha*(ymax - ymin)); 
    lmin[2] = 0.5*(zmax + zmin - alpha*(zmax - zmin)); lmax[2] = 0.5*(zmax + zmin + alpha*(zmax - zmin)); 
    lmin[0] = xmax; lmax[0] = xmax + beta*std::min(lmax[2] - lmin[2], lmax[1] - lmin[1]); 
    print_paraview_vtp_planebox(lmin, lmax, particle, particle_init, fout, ngrains + DEM::RIGHT);
    // back plane
    lmin[0] = 0.5*(xmax + xmin - alpha*(xmax - xmin)); lmax[0] = 0.5*(xmax + xmin + alpha*(xmax - xmin)); 
    lmin[2] = 0.5*(zmax + zmin - alpha*(zmax - zmin)); lmax[2] = 0.5*(zmax + zmin + alpha*(zmax - zmin)); 
    lmin[1] = ymin - beta*std::min(lmax[2] - lmin[2], lmax[0] - lmin[0]); lmax[1] = ymin; 
    print_paraview_vtp_planebox(lmin, lmax, particle, particle_init, fout, ngrains + DEM::BACK);
    // front plane
    lmin[0] = 0.5*(xmax + xmin - alpha*(xmax - xmin)); lmax[0] = 0.5*(xmax + xmin + alpha*(xmax - xmin)); 
    lmin[2] = 0.5*(zmax + zmin - alpha*(zmax - zmin)); lmax[2] = 0.5*(zmax + zmin + alpha*(zmax - zmin)); 
    lmin[1] = ymax; lmax[1] = ymax + beta*std::min(lmax[2] - lmin[2], lmax[0] - lmin[0]); 
    print_paraview_vtp_planebox(lmin, lmax, particle, particle_init, fout, ngrains + DEM::FRONT);


    // // start this piece
    // fout << "    <Piece NumberOfPoints=\""<< 4*DEM::NWALLS << "\" NumberOfPolys=\"" << DEM::NWALLS << "\" >" << "\n"; //" NumberOfVerts=”#” NumberOfLines=”#” NumberOfStrips=”#” NumberOfPolys=”#”>
    // // Print the plane data 
    // // points
    // fout << "      <Points>" << "\n";
    // fout << "        <DataArray type=\"Float64\" Name=\"Points\" NumberOfComponents=\"3\" format=\"ascii\"> " << "\n";
    // fout << "          " ;
    // // bottom plane
    // fout << xmin << "\t" << ymin << "\t" << zmin << "\t";
    // fout << xmin << "\t" << ymax << "\t" << zmin << "\t";
    // fout << xmax << "\t" << ymax << "\t" << zmin << "\t";
    // fout << xmax << "\t" << ymin << "\t" << zmin << "\t";
    // // top plane
    // fout << xmin << "\t" << ymin << "\t" << zmax << "\t";
    // fout << xmin << "\t" << ymax << "\t" << zmax << "\t";
    // fout << xmax << "\t" << ymax << "\t" << zmax << "\t";
    // fout << xmax << "\t" << ymin << "\t" << zmax << "\t";
    // // left plane
    // fout << xmin << "\t" << ymin << "\t" << zmin << "\t";
    // fout << xmin << "\t" << ymin << "\t" << zmax << "\t";
    // fout << xmin << "\t" << ymax << "\t" << zmax << "\t";
    // fout << xmin << "\t" << ymax << "\t" << zmin << "\t";
    // // right plane
    // fout << xmax << "\t" << ymin << "\t" << zmin << "\t";
    // fout << xmax << "\t" << ymin << "\t" << zmax << "\t";
    // fout << xmax << "\t" << ymax << "\t" << zmax << "\t";
    // fout << xmax << "\t" << ymax << "\t" << zmin << "\t";
    // // back plane
    // fout << xmin << "\t" << ymin << "\t" << zmin << "\t";
    // fout << xmin << "\t" << ymin << "\t" << zmax << "\t";
    // fout << xmax << "\t" << ymin << "\t" << zmax << "\t";
    // fout << xmax << "\t" << ymin << "\t" << zmin << "\t";
    // // front plane
    // fout << xmin << "\t" << ymax << "\t" << zmin << "\t";
    // fout << xmin << "\t" << ymax << "\t" << zmax << "\t";
    // fout << xmax << "\t" << ymax << "\t" << zmax << "\t";
    // fout << xmax << "\t" << ymax << "\t" << zmin << "\t";
    // // end points
    // fout << "\n";
    // fout << "        </DataArray>" << "\n";
    // fout << "      </Points>" << "\n";

    // // polys
    // fout << "      <Polys>" << "\n";
    // // connectivity
    // fout << "        <DataArray type=\"Int32\" Name=\"connectivity\" format=\"ascii\"> " << "\n";
    // fout << "          " ;
    // for (int id = 0; id < 4*DEM::NWALLS; ++id) {
    //   fout << id << "  "; 
    // }
    // fout << "\n";
    // // end connectivity
    // fout << "        </DataArray>" << "\n";
    // // offsets
    // fout << "        <DataArray type=\"Int32\" Name=\"offsets\" format=\"ascii\"> " << "\n";
    // fout << "          " ;
    // for (int id = 0; id < DEM::NWALLS; ++id) {
    //   fout << 4*(id+1) << "  "; 
    // }
    // fout << "\n";
    // // end offsets
    // fout << "        </DataArray>" << "\n";
    // // end polys
    // fout << "      </Polys>" << "\n";

    // // point attributes
    // //fout << "      <PointData Scalars=\"nc\" Vectors=\"Velocity\">" << "\n";
    // // cell attributes
    // //fout << "      <CellData Scalars=\"nc\" Vectors=\"Velocity\">" << "\n";
    // fout << "      <CellData>" << "\n";

    // print_paraview_vtp_body_data(particle, particle_init, fout, ngrains, ngrains + DEM::NWALLS);

    // // footer
    // //fout << "      </PointData>" << "\n";
    // fout << "      </CellData>" << "\n";

    // // end this piece
    // fout << "    </Piece>" << "\n";
  }

void 
Visualization::print_paraview_vtp_planebox(const double lmin[3], const double lmax[3], 
					   const std::vector<Particle> & particle, 
					   const std::vector<Particle> & particle_init, 
					   std::ofstream & fout,
					   const int & wall_id)
  {
    // start this piece, 6 planes, 4 points per plane, 24 points, 
    fout << "    <Piece NumberOfPoints=\""<< 24 << "\" NumberOfPolys=\"" << 6 << "\" >" << "\n"; //" NumberOfVerts=”#” NumberOfLines=”#” NumberOfStrips=”#” NumberOfPolys=”#”>
    // Print the planebox data 
    // points
    fout << "      <Points>" << "\n";
    fout << "        <DataArray type=\"Float64\" Name=\"Points\" NumberOfComponents=\"3\" format=\"ascii\"> " << "\n";
    fout << "          " ;
    // bottom plane
    fout << lmin[0] << "\t" << lmin[1] << "\t" << lmin[2] << "\t";
    fout << lmin[0] << "\t" << lmax[1] << "\t" << lmin[2] << "\t";
    fout << lmax[0] << "\t" << lmax[1] << "\t" << lmin[2] << "\t";
    fout << lmax[0] << "\t" << lmin[1] << "\t" << lmin[2] << "\t";
    // top plane
    fout << lmin[0] << "\t" << lmin[1] << "\t" << lmax[2] << "\t";
    fout << lmin[0] << "\t" << lmax[1] << "\t" << lmax[2] << "\t";
    fout << lmax[0] << "\t" << lmax[1] << "\t" << lmax[2] << "\t";
    fout << lmax[0] << "\t" << lmin[1] << "\t" << lmax[2] << "\t";
    // left plane
    fout << lmin[0] << "\t" << lmin[1] << "\t" << lmin[2] << "\t";
    fout << lmin[0] << "\t" << lmin[1] << "\t" << lmax[2] << "\t";
    fout << lmin[0] << "\t" << lmax[1] << "\t" << lmax[2] << "\t";
    fout << lmin[0] << "\t" << lmax[1] << "\t" << lmin[2] << "\t";
    // right plane
    fout << lmax[0] << "\t" << lmin[1] << "\t" << lmin[2] << "\t";
    fout << lmax[0] << "\t" << lmin[1] << "\t" << lmax[2] << "\t";
    fout << lmax[0] << "\t" << lmax[1] << "\t" << lmax[2] << "\t";
    fout << lmax[0] << "\t" << lmax[1] << "\t" << lmin[2] << "\t";
    // back plane
    fout << lmin[0] << "\t" << lmin[1] << "\t" << lmin[2] << "\t";
    fout << lmin[0] << "\t" << lmin[1] << "\t" << lmax[2] << "\t";
    fout << lmax[0] << "\t" << lmin[1] << "\t" << lmax[2] << "\t";
    fout << lmax[0] << "\t" << lmin[1] << "\t" << lmin[2] << "\t";
    // front plane
    fout << lmin[0] << "\t" << lmax[1] << "\t" << lmin[2] << "\t";
    fout << lmin[0] << "\t" << lmax[1] << "\t" << lmax[2] << "\t";
    fout << lmax[0] << "\t" << lmax[1] << "\t" << lmax[2] << "\t";
    fout << lmax[0] << "\t" << lmax[1] << "\t" << lmin[2] << "\t";
    // end points
    fout << "\n";
    fout << "        </DataArray>" << "\n";
    fout << "      </Points>" << "\n";

    // polys
    fout << "      <Polys>" << "\n";
    // connectivity
    fout << "        <DataArray type=\"Int32\" Name=\"connectivity\" format=\"ascii\"> " << "\n";
    fout << "          " ;
    for (int id = 0; id < 24; ++id) {
      fout << id << "  "; 
    }
    fout << "\n";
    // end connectivity
    fout << "        </DataArray>" << "\n";
    // offsets
    fout << "        <DataArray type=\"Int32\" Name=\"offsets\" format=\"ascii\"> " << "\n";
    fout << "          " ;
    for (int id = 0; id < 6; ++id) {
      fout << 4*(id+1) << "  "; 
    }
    fout << "\n";
    // end offsets
    fout << "        </DataArray>" << "\n";
    // end polys
    fout << "      </Polys>" << "\n";

    // point attributes
    //fout << "      <PointData Scalars=\"nc\" Vectors=\"Velocity\">" << "\n";
    // cell attributes
    //fout << "      <CellData Scalars=\"nc\" Vectors=\"Velocity\">" << "\n";
    fout << "      <CellData>" << "\n";
    print_paraview_vtp_body_data(particle, particle_init, fout, wall_id, wall_id + 1, 6); // 6 repetitions, one per plane forming the wall
    // footer
    //fout << "      </PointData>" << "\n";
    fout << "      </CellData>" << "\n";
    
    // end this piece
    fout << "    </Piece>" << "\n";
  }


} // end namespace DEM
