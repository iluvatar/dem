#include "workspace_dem_helper.hpp"
#include "walls_normalvectors.hpp"

namespace DEM{
  namespace helper {
    //-----------------------------------------------------------
    // implementations
    //-----------------------------------------------------------
    void freeze(WorkspaceDEM & dom) {
      std::cout << "\nFREEZING SYSTEM VELOCITIES\n" << std::endl;
      for(auto & body : dom.particle) {
	body.W = body.V = Vecnull;
	body.PHI = Vecnull; // just because if there is no W, then rotations has no sense. Can be removed.  
      }
      std::cout << "\nDONE\n" << std::endl;
    }

    void reset_R0(WorkspaceDEM & dom) {
      for(auto & body : dom.particle)
	body.R0 = body.R;
    }

    void shift_grains_by(WorkspaceDEM & dom, const Vec3d_t & Shift)
    {
      const int ngrains = dom.particle.size() - DEM::NWALLS; 
      for ( int id = 0; id < ngrains; ++id ) 
	dom.particle[id].R += Shift;
    }

    int remove_penetrations_by_scaling(WorkspaceDEM & dom)
    {
      std::cout << "\nSCALING THE SYSTEM TO REMOVE INTERPENETRATIONS\n" << std::endl;
      std::vector<Particle> particle_backup = dom.particle;
      
      // some vars
      const int ngrains = dom.particle.size() - DEM::NWALLS;
      double alphamax = 1.0;
      bool periodic[3] = {0}; dom.copy_periodic_flags(periodic);
      std::cout << periodic[0] << " " << periodic[1] << " " << periodic[2] << std::endl;
      dom.set_alive_walls();

      // get current size of domain
      double lmin[3] = {0}, lmax[3] = {0};
      dom.store_box_limits(lmin, lmax);

      // Compute the maximum scale factor, O(N^2) but run only once
      std::cout << "\nCOMPUTING INTERPENETRATIONS\n" << std::endl;
      DistanceToSphere distance_ss;
      distance_ss.set_periodic(periodic, lmax[0], lmax[1], lmax[2]);
      double delta, alpha;
      int ii = 0, jj = 0;
      double ai = 0, aj = 0;
      Vec3d_t N; 
      for ( int id = 0; id < ngrains; ++id ) {
	for ( int jd = id + 1; jd < ngrains; ++jd ) {
	  distance_ss(dom.particle[id], dom.particle[jd], delta, N);
	  if ( delta <= 0 ) continue;
	  alpha = 1.0/(1.0 - delta/(dom.particle[id].rad + dom.particle[jd].rad));
	  if ( alpha > alphamax ) { alphamax = alpha; ii = id; jj = jd; ai = dom.particle[id].rad; aj = dom.particle[jd].rad; }
	}
      } 
      DistanceToWall distance_sw;
      for ( int jd = ngrains + BOTTOM; jd <= ngrains + FRONT; ++jd) {  // add walls to list
	if ( false == dom.particle[jd].alive ) { std::cout << "NON-alive wall detected" << std::endl; continue; }
	N = NORMAL_VEC_WALL[jd-ngrains];
	for ( int id = 0; id < ngrains; ++id ) { 
	  distance_sw(dom.particle[id], dom.particle[jd], delta, N);
	  if ( delta <= 0 ) continue;
	  alpha = 1.0/(1.0 - delta/(dom.particle[id].rad + dom.particle[jd].rad));
	  if ( alpha > alphamax ) { alphamax = alpha; ii = id; jj = jd; ai = dom.particle[id].rad; aj = dom.particle[jd].rad; }
	}
      }
      if ( alphamax < 1.0 ) {
	std::cerr << "Error: alphamax = " << alphamax << std::endl;
	return EXIT_FAILURE;
      }
      if (alphamax > 1.2) std::cout << "WARNING : SCALING TOO MUCH ..." << std::endl;
      std::cout << "\nALPHAMAX = " << alphamax << "\n" << std::endl;
      std::cout << "ii = " << ii <<  std::endl;
      std::cout << "jj = " << jj <<  std::endl;
      std::cout << "ai = " << ai <<  std::endl;
      std::cout << "aj = " << aj <<  std::endl;
      std::cout << "Ri = " << dom.particle[ii].R <<  std::endl;
      std::cout << "Rj = " << dom.particle[jj].R <<  std::endl;
      delta = ai + aj - std::fabs((dom.particle[ii].R - dom.particle[jj].R)[2]);
      std::cout << "delta = " << delta <<  std::endl;
      std::cout << "alpha = " << 1.0/(1.0 - delta/(ai + aj)) <<  std::endl;

      // scale all position vectors
      std::cout << "\nSCALING\n" << std::endl;
      if ( alphamax > 1.0 ) {
	for ( auto & body : dom.particle ) { 
	  body.R *= alphamax;
	} 
      }

      // translate all bodies (grains + walls) to default start of box : Bottom, left, and back at zero 
      std::cout << "\nTRANSLATING SYSTEMS TO RESET WALLS POSITIONS\n" << std::endl;
      dom.store_box_limits(lmin, lmax); // NEW domain size after scaling
      Vec3d_t Shift(lmin[0], lmin[1], lmin[2]);
      for ( auto & body : dom.particle ) { 
	body.R -= Shift;
      }
     
      // erase non normal components for walls
      for ( int jd = ngrains + BOTTOM; jd <= ngrains + FRONT; ++jd) {
	N = NORMAL_VEC_WALL[jd-ngrains];
	dom.particle[jd].R = N*(N.dot(dom.particle[jd].R));
      } 

      // Erase other vars: Forces, Torques, nc
      for ( auto & body : dom.particle ) { 
	body.F = body.T = Vecnull;
	body.nc = 0;
      }

      // periodicity
      std::cout << "\nAPPLYING PERIODICITY\n" << std::endl;
      dom.apply_periodicity();

      // check no penetrations
      std::cout << "\nCHECKING NO PENETRATIONS AMONG PARTICLES\n" << std::endl;
      dom.store_box_limits(lmin, lmax); // NEW domain size after scaling
      distance_ss.set_periodic(periodic, lmax[0], lmax[1], lmax[2]);
      for ( int id = 0; id < ngrains; ++id ) {
	for ( int jd = id + 1; jd < ngrains; ++jd ) {
	  distance_ss(dom.particle[id], dom.particle[jd], delta, N);
	  if ( delta > 1.0e-08 ) {
	    std::cerr << "ERROR: Interpenetration detected." << std::endl;
	    std::cerr << "ngrains = " << ngrains << std::endl;
	    std::cerr << "id = " << id << std::endl;
	    std::cerr << "jd = " << jd << std::endl;
	    std::cerr << "delta = " << delta << std::endl;
	    std::cerr << "Recovering original state and returning." << std::endl;
	    dom.particle = particle_backup;
	    return EXIT_FAILURE;
	  } 
	}
      } 

      // delete contacts
      std::cout << "\nDELETING CONTACTS.\n" << std::endl;
      dom.clear_contacts();


      std::cout << "\nDONE.\n" << std::endl;
      return EXIT_SUCCESS;
    }


    int create_cubic_internal_nucleus(const int & TAG0, const int & TAG1, 
				      const double & FRACTION)
    {
      std::cout << "Creating internal nucleus with material tag = tag0." << std::endl;
      std::cout << "Material tag0     = " << TAG0 << std::endl;
      std::cout << "Material tag1     = " << TAG1 << std::endl;
      
      // read bodies and contacts data
      std::vector<Particle> particles;
      print_log("Reading file : ", "INPUT/" BODIES_INFO_FILE);
#if WITH_NETCDF
      read_particles_netcdf("INPUT/" BODIES_INFO_FILE, particles);
#endif // WITH_NETCDF
     const int ngrains = particles.size() - DEM::NWALLS; ASSERT(ngrains >= 0);
      std::vector<DEM::Contact> contacts;
      print_log("Reading file : ", "INPUT/" CONTACTS_INFO_FILE);
#if WITH_NETCDF
      read_contacts_netcdf("INPUT/" CONTACTS_INFO_FILE, contacts);
#endif // WITH_NETCDF
      // set box limits
      const double lmin[3] = { particles[ngrains + LEFT ].x(), particles[ngrains + BACK ].y(), particles[ngrains + BOTTOM].z() };
      const double lmax[3] = { particles[ngrains + RIGHT].x(), particles[ngrains + FRONT].y(), particles[ngrains + TOP   ].z() };
      // compute nucleus limits
      double nucleus_min[3] = {0};
      double nucleus_max[3] = {0};
      for (int dir = 0; dir < 3; ++dir) {
	nucleus_min[dir] = lmin[dir] + (1.0 - FRACTION)*(lmax[dir] - lmin[dir])/2.0;
	nucleus_max[dir] = lmin[dir] + (1.0 + FRACTION)*(lmax[dir] - lmin[dir])/2.0;
      }
      // set material tags according to position
      int ngrains0 = 0, ngrains1 = 0;
      for (int id = 0; id < ngrains; ++id) {
	if (UTIL::is_inside(particles[id].R[0], nucleus_min[0], nucleus_max[0]) && 
	    UTIL::is_inside(particles[id].R[1], nucleus_min[1], nucleus_max[1]) && 
	    UTIL::is_inside(particles[id].R[2], nucleus_min[2], nucleus_max[2]) ) { // in nucleus  
	  particles[id].materialTag = TAG0;
	  ++ngrains0;
	}
	else { // external shell
	  particles[id].materialTag = TAG1;
	  ++ngrains1;
	}
      }
      
      std::cout << "Total number of particles = " << ngrains << std::endl;
      std::cout << "Tag0 particles = " << ngrains0 << " \t(" << (double (ngrains0))/ngrains << " %)" << std::endl;
      std::cout << "Tag1 particles = " << ngrains1 << " \t(" << (double (ngrains1))/ngrains << " %)" << std::endl;

      // write info
#if WITH_NETCDF
      write_particles_netcdf(particles, "OUTPUT/" BODIES_INFO_FILE);
      write_contacts_netcdf(contacts, "OUTPUT/" CONTACTS_INFO_FILE);
#endif // WITH_NETCDF
      
      std::cout << "#############################################" << std::endl;
      std::cout << "DONE." << std::endl;

      return EXIT_SUCCESS;
    }


    int create_right_nucleus(const int & TAG0, const int & TAG1,
			     const double & FRACTION)
    
    {
      std::cout << "Partitioning system into two parts (along x axis) with different materials." << std::endl;
      std::cout << "Material tag0 (left )     = " << TAG0 << std::endl;
      std::cout << "Material tag1 (right)     = " << TAG1 << std::endl;
      
      // read bodies and contacts data
      std::vector<Particle> particles;
      print_log("Reading file : ", "INPUT/" BODIES_INFO_FILE);

#if WITH_NETCDF
      read_particles_netcdf("INPUT/" BODIES_INFO_FILE, particles);
#endif // WITH_NETCDF

      const int ngrains = particles.size() - DEM::NWALLS; ASSERT(ngrains >= 0);
      std::vector<DEM::Contact> contacts;
      print_log("Reading file : ", "INPUT/" CONTACTS_INFO_FILE);

#if WITH_NETCDF
      read_contacts_netcdf("INPUT/" CONTACTS_INFO_FILE, contacts);      
#endif // WITH_NETCDF
      
      // process particles and select along x
      const double xcut = particles[ngrains + LEFT].x() + (FRACTION)*(particles[ngrains + RIGHT].x() - particles[ngrains + LEFT].x());
      int ngrains0 = 0, ngrains1 = 0;
      for (int id = 0; id < ngrains ; ++id) {
	if (particles[id].x() <= xcut) {
	  particles[id].materialTag = TAG0;
	  ++ngrains0;
	}
	else {
	  particles[id].materialTag = TAG1;
	  ++ngrains1;
	}
      }
      
      std::cout << "Total number of particles = " << ngrains << std::endl;
      std::cout << "Tag0 particles = " << ngrains0 << " \t(" << (double (ngrains0))/ngrains << " %)" << std::endl;
      std::cout << "Tag1 particles = " << ngrains1 << " \t(" << (double (ngrains1))/ngrains << " %)" << std::endl;
      
      // write info
#if WITH_NETCDF
      write_particles_netcdf(particles, "OUTPUT/" BODIES_INFO_FILE);
      write_contacts_netcdf(contacts, "OUTPUT/" CONTACTS_INFO_FILE);
#endif // WITH_NETCDF

      std::cout << "#############################################" << std::endl;
      std::cout << "DONE." << std::endl;

      return EXIT_SUCCESS;
    }

    int create_mixed_nucleus(const int & TAG0, const int & TAG1, 
			     const double & FRACTION)
    {
      std::cout << "Creating a weigthed mix of materials." << std::endl;
      std::cout << "Material tag0     = " << TAG0 << std::endl;
      std::cout << "Material tag1     = " << TAG1 << std::endl;
      std::cout << "Fraction for tag0 = " << FRACTION << std::endl;
      
      // read bodies and contacts data
      std::vector<Particle> particles;
      print_log("Reading file : ", "INPUT/" BODIES_INFO_FILE);

#if WITH_NETCDF
      read_particles_netcdf("INPUT/" BODIES_INFO_FILE, particles);
#endif // WITH_NETCDF

      const int ngrains = particles.size() - DEM::NWALLS; // ASSERT(ngrains >= 0);
      std::vector<DEM::Contact> contacts;
      print_log("Reading file : ", "INPUT/" CONTACTS_INFO_FILE);

#if WITH_NETCDF
      read_contacts_netcdf("INPUT/" CONTACTS_INFO_FILE, contacts);
#endif // WITH_NETCDF

      const int ngrains0 = FRACTION*ngrains;
      const int ngrains1 = ngrains - ngrains0;
      std::vector<int> ids(ngrains, 0);
      std::iota(ids.begin(), ids.end(), 0); // fills with 0, 1, 2, 3, ..., ngrains-1
      std::random_shuffle(ids.begin(), ids.end()); 
      for (int id = 0; id < ngrains; ++id) {
	particles[ids[id]].materialTag = (id <= ngrains0) ? TAG0 : TAG1;
      }
      
      std::cout << "Total number of particles = " << ngrains << std::endl;
      std::cout << "Tag0 particles = " << ngrains0 << " \t(" << (double (ngrains0))/ngrains << " %)" << std::endl;
      std::cout << "Tag1 particles = " << ngrains1 << " \t(" << (double (ngrains1))/ngrains << " %)" << std::endl;
      
      // write info
#if WITH_NETCDF
      write_particles_netcdf(particles, "OUTPUT/" BODIES_INFO_FILE);
      write_contacts_netcdf(contacts, "OUTPUT/" CONTACTS_INFO_FILE);
#endif // WITH_NETCDF

      std::cout << "#############################################" << std::endl;
      std::cout << "DONE." << std::endl;

      return EXIT_SUCCESS;
    }


    /*
    void create_top_bottom_grain_walls(WorkspaceDEM & dom)
    {
      srand48(0);
      print_log("CREATING TOP AND BOTTOM SPECIAL GRAINS ");
      const int N_Grain = dom.particle.size() - DEM::NWALLS; 
      double max_rad = dom.particle[0].rad;
      for (int id = 0; id < N_Grain; ++id) {
	if (dom.particle[id].rad > max_rad) 
	  max_rad = dom.particle[id].rad;
      }      
      double newrad, delta;
      Vec3d_t Normal; 
      DistanceToWall distance_sw;
      const double zmax = dom.particle[N_Grain + TOP].R[2];
      for (int id = 0; id < N_Grain; ++id) {
	// bottom
	Normal = NORMAL_VEC_WALL[BOTTOM];
	distance_sw(dom.particle[id], dom.particle[N_Grain + BOTTOM], delta, Normal);
	if (delta > -0.5*max_rad) {
	  newrad = (dom.particle[id].rad)*(0.3 + 0.7*drand48()); // scale the radius 
	  dom.change_radius_mass_inerI(dom.particle[id], newrad);
	  dom.particle[id].R[2] = dom.particle[id].rad;
	  dom.particle[id].groupTag = BOTTOM_GRAIN_GROUP_TAG;
	  dom.particle[id].bcTag = BOTTOM_GRAIN_SINGLE_TAG;
	}
	// top
	Normal = NORMAL_VEC_WALL[TOP];
	distance_sw(dom.particle[id], dom.particle[N_Grain + TOP], delta, Normal);
	if (delta > -0.5*max_rad) {
	  newrad = (dom.particle[id].rad)*(0.3 + 0.7*drand48()); // scale the radius 
	  dom.change_radius_mass_inerI(dom.particle[id], newrad);
	  dom.particle[id].R[2] = zmax - dom.particle[id].rad;
	  dom.particle[id].groupTag = TOP_GRAIN_GROUP_TAG;
	  dom.particle[id].bcTag = TOP_GRAIN_SINGLE_TAG;
	}
      }
    }
    */
  } // namespace : helper
} // namespace : DEM

