#include "workspace_dem_util.hpp"

namespace DEM {
  namespace util {
    //-----------------------------------------------------------
    // implementations
    //-----------------------------------------------------------
    double unbalanced_force_torque( const std::vector<Particle> & particle, 
				    const int & ngrains,
				    const double & sumFc, const double & sumTc, 
				    const bool & print_info_screen ) 
    {
      // compute mean
      double avgFb = 0, avgTb = 0;
      int count = 0;
      for (int id = 0; id < ngrains; ++id) { 
	if (true == particle[id].alive) {
	  avgFb += particle[id].F.norm();
	  avgTb += particle[id].T.norm();
	  count++;
	}
      }
      avgFb /= (count > 1) ? count : 1;
      avgTb /= (count > 1) ? count : 1;

      // sum forces accepting only those larger than 1.0e-4 average
      double sumFb = 0, sumTb = 0, result = 0;
      for (int id = 0; id < ngrains; ++id) { 
	if (true == particle[id].alive) {
	  if (particle[id].F.norm() > 1.0e-4*avgFb) sumFb += particle[id].F.norm();
	  if (particle[id].T.norm() > 1.0e-4*avgTb) sumTb += particle[id].T.norm();
	}
      }

      // compute cundall parameter
      result = 0;
      result += (sumFc > 0 && sumFb > 1.0e-8) ? sumFb/sumFc : 0;
      result += (sumTc > 0 && sumTb > 1.0e-8) ? sumTb/sumTc : 0;
      result *= 0.5;
      if(print_info_screen) {
#if DEBUG
	print_log("sumFb, sumFc         = ", sumFb, sumFc);
	print_log("sumTb, sumTc         = ", sumTb, sumTc);
#endif
	print_log("Cundall-F, Cundall-T = ", (sumFc > 0) ? sumFb/sumFc : 0, (sumTc > 0) ? sumTb/sumTc : 0);
      }
      return result;
    }

  } // end namesapce util
} // end namespace DEM

