#ifndef __UTILITY__
#define __UTILITY__

#include "matrix3d.hpp"
#include "vector3d.hpp"
#include "particle.hpp"
#include "util/assert.hpp"
#include <cstdio>
#include <iostream>
#include <algorithm>
#include <vector>
#include <string>
#include <sstream>
#include <iomanip>
#include <unistd.h> // for sleep
#include <cmath>
#include <tr1/cmath> // for sph_legendre ; can be eliminated if using -std=c++17
#include <complex>
#include <cstdlib>
#include "voro++/voro++.hh"



// number of digits for printing
const int PRINT_DIGITS = 17;

// define some std::strings for colorized output
const std::string black_init="# \033[1;30m ";
const std::string red_init="# \033[1;31m ";
const std::string green_init="# \033[1;32m ";
const std::string yellow_init="# \033[1;33m ";
const std::string blue_init="# \033[1;34m ";
const std::string purple_init="# \033[1;35m ";
const std::string cyan_init="# \033[1;36m ";
const std::string lightgray_init="# \033[1;37m ";
const std::string color_end=" \033[0m ";

// some util macro
#define SQUARE(x) ((x)*(x))
#define CUBE(x)   ((x)*(x)*(x))
#define _UNUSED(x) ((void)x) // avoid unused var warnings, when compiling with ifdefs

//-----------------------------------------------------------------------------------------------
// printing messages
// printing messages
namespace UTIL{
  template <typename T>
  void set_print_format(T & stream) 
  {
    //stream.setf(std::ios_base::fixed | std::ios_base::scientific, std::ios_base::floatfield); // choose better representation
    stream.setf(std::ios::scientific);
    stream.precision(PRINT_DIGITS);
  }
}

template <typename T1>
void print_log(const T1 & message) 
{
  UTIL::set_print_format(std::cout);
  std::cout << blue_init << " LOG : " << color_end << std::setw(35) << message << std::endl;
}

template <typename T1, typename T2>
void print_log(const T1 & message, const T2 & var) 
{
  UTIL::set_print_format(std::cout);
  std::cout << blue_init << " LOG : " << color_end 
	    << std::setw(35) << std::left << message 
	    << std::setw(35) << std::left << var 
	    << std::endl;
}

template <typename T1, typename T2, typename T3>
void print_log(const T1 & message, const T2 & var, const T3 & var2) 
{
  UTIL::set_print_format(std::cout);
  std::cout << blue_init << " LOG : " << color_end 
	    << std::setw(35) << std::left << message 
	    << std::setw(35) << std::left << var 
	    << std::setw(35) << std::left << var2 
	    << std::endl;
}

template <typename T1, typename T2, typename T3, typename T4>
void print_log(const T1 & message, const T2 & var, const T3 & var2, const T4 & var3) 
{
  UTIL::set_print_format(std::cout);
  std::cout << blue_init << " LOG : " << color_end 
	    << std::setw(35) << std::left << message 
	    << std::setw(35) << std::left << var 
	    << std::setw(35) << std::left << var2 
	    << std::setw(35) << std::left << var3 
	    << std::endl;
}

namespace UTIL {
  template <typename T1>
  void print_start(const T1 & message) 
  {
    UTIL::set_print_format(std::cout);
    std::cout << cyan_init << "START : " << color_end << std::setw(35) << message << std::endl;
  }
  
  template <typename T1>
  void print_done(const T1 & message) 
  {
    UTIL::set_print_format(std::cout);
    std::cout << green_init << "DONE  : " << color_end << std::setw(35) << message << std::endl;
  }
}


template <typename T1>
void print_debug(const T1 & message) 
{
#ifdef DEBUG
  print_log(message);
#else
  _UNUSED(message); // avoids unused variable warning
#endif
}

template <typename T1, typename T2>
void print_debug(const T1 & message, const T2 & var) 
{
#ifdef DEBUG
  print_log(message, var);
#else
  _UNUSED(message); _UNUSED(var); // avoids unused variable warning
#endif
}

template <typename T1, typename T2>
void print_log_vec(const T1 & message, const std::vector<T2> & vec) 
{
  UTIL::set_print_format(std::cout);
  std::cout << blue_init << " LOG : " << color_end 
	    << std::setw(35) << std::left << message;
  for (auto & val : vec) std::cout << val << "\t"; 
  std::cout << std::endl;
}

template <typename T1, typename T2>
void print_log_array(const T1 & message, const T2 vec[], const int & size) 
{
  UTIL::set_print_format(std::cout);
  std::cout << blue_init << " LOG : " << color_end 
	    << std::setw(35) << std::left << message;
  for (int ii = 0; ii < size; ++ii) std::cout << vec[ii] << "\t"; 
  std::cout << std::endl;
}


template <typename T1, typename T2>
void print_warning(T1 message, T2 var)
{
  UTIL::set_print_format(std::cout);
  std::cout << yellow_init << " WARNING : " 
	    << std::setw(50) << std::left << message 
	    << std::setw(50) << std::left << var 
	    << color_end << std::endl;
}

template <typename T1>
void print_warning(T1 message)
{
  UTIL::set_print_format(std::cout);
  std::cout << yellow_init << " WARNING : " << std::setw(40) <<  message << color_end << std::endl;
}

template <typename T1>
void print_warning_wait(T1 message, const double & time_to_wait )
{
  UTIL::set_print_format(std::cout);
  std::cout << yellow_init << " WARNING : " << std::setw(40) << message << color_end << std::endl;
  sleep(time_to_wait);
}


template <typename T1, typename T2>
void print_error_vec(const T1 & message, const std::vector<T2> & vec) 
{
  UTIL::set_print_format(std::cerr);
  std::cerr << red_init << " ERROR : " << color_end 
	    << std::setw(50) << std::left << message;
  for (auto & val : vec) std::cerr << val << "\t"; 
  std::cerr << std::endl;
}

template <typename T1, typename T2>
void print_error(const T1 & message, const T2 vec [], const int & size) 
{
  UTIL::set_print_format(std::cerr);
  std::cerr << red_init << " ERROR : " << color_end 
	    << std::setw(50) << std::left << message;
  for (int ii = 0; ii < size; ++ii) std::cerr << vec[ii] << "\t"; 
  std::cerr << std::endl;
}


template <typename T1, typename T2>
void print_error(T1 message, T2 var) 
{
  UTIL::set_print_format(std::cerr);
  std::cerr << red_init << " ERROR : " 
	    << std::setw(50) << std::left << message 
	    << std::setw(50) << std::left << var 
	    << color_end << std::endl;
}

template <typename T1>
void print_error(T1 message) 
{ 
  UTIL::set_print_format(std::cerr);
  std::cerr << red_init << " ERROR : " << std::setw(40) << message << color_end << std::endl;
}

template <typename T1>
void print_error_and_exit(T1 message) 
{
  UTIL::set_print_format(std::cerr);
  std::cerr << red_init << " ERROR : " << message << color_end << std::endl;
  std::cerr << red_init << " EXITING. " << color_end << std::endl;
  exit(1);
}

template <typename T1, typename T2>
void print_error_and_exit(T1 message, T2 var) 
{
  UTIL::set_print_format(std::cerr);
  std::cerr << red_init << " ERROR : " 
	    << std::setw(50) << std::left << message 
	    << std::setw(50) << std::left << var 
	    << color_end << std::endl;
  std::cerr << red_init << " EXITING. " << color_end << std::endl;
  exit(1);
}

//-----------------------------------------------------------------------------------------------
// printing some vector
template <typename type>
void print(const std::vector<type> & v);

//-----------------------------------------------------------------------------------------------
// printing some array
template <typename type>
void print(const type * a, const int & size);

//-----------------------------------------------------------------------------------------------
// ramp function
//-----------------------------------------------------------------------------------------------
namespace UTIL {
  template <class T1, class T2>
  double ramp_value(const T1 & iter, const T2 & maxiter, const double & val, const double & val0 = 0.0) ;
}


//-----------------------------------------------------------------------------------------------
// pairing function
//-----------------------------------------------------------------------------------------------
long get_uid(const long & i1, const long & i2);

long get_uid(const int & i1, const int & i2, const int & k1);

//-----------------------------------------------------------------------------------------------
namespace UTIL {
  template <class T>
  inline
  T maximum(T a, T b, T c);

  template <class T>
  inline
  T minimum(T a, T b, T c);
}

//-----------------------------------------------------------------------------------------------
template <class T>
inline
T min(T a, T b, T c);

//-----------------------------------------------------------------------------------------------
template <class Real>
Real reduced_value(Real a, Real b);    // get reduced of two values

//-----------------------------------------------------------------------------------------------
template <class T>
inline
int sign(const T & a)
{
  if (a > 0) return 1;
  else if (a < 0) return -1;
  else return 0;
}
//template int sign<double> (const double &);
//template int sign<int> (const int &);

//-----------------------------------------------------------------------------------------------
// set string filename with or other stuff at the end
namespace UTIL {
  std::string composite_name(const std::string & base, const double & suffix, const std::string & ext = ".dat") ;
  std::string composite_name(const std::string & base, const std::string & suffix, const std::string & ext = ".dat") ;
} // namespace UTIL

  //-----------------------------------------------------------------------------------------------
template <class T, class Real>
inline
int non_zero(const T & a, const Real & eps = 1.0e-16)
{
  return (std::fabs(a) > eps);
}

namespace UTIL {
  std::complex<double> spherical_harmonic(const int & l, const int & m, const double & theta, const double & phi);
} // namespace UTIL


  //-----------------------------------------------------------------------------------------------
  // bool to locate a point inside domain
namespace UTIL {
  
  bool is_inside(const double & val, const double & min, const double & max, const double & eps = 0.0) ;
  bool is_vector_inside(const Vec3d_t & V, const Vec3d_t & LeftBottom, const Vec3d_t & RightTop, const double & eps = 0.0) ;
  double max_component(const Vec3d_t & V) ;
  double min_component(const Vec3d_t & V) ;
  double min_component(const Vec3d_t & V, const int & dim) ;
}

//-----------------------------------------------------------------------------------------------
void mean_and_sigma(const double * vec, const int & size, double & mean_out, double & sigma_out);

//-----------------------------------------------------------------------------------------------
void mean_and_sigma_vector(const std::vector<double> & vec, double & mean_out, double & sigma_out);

//-----------------------------------------------------------------------------------------------
// stat mech utils
namespace UTIL {
  double 
  voronoi_vmin(const int & dim) ;

  template <class Histogram>
  double 
  voronoi_vmin(const int & dim, Histogram & histo) ;
  
  double statmech_X(const double & mean, const double & sigma, const double & min);
  double statmech_K(const double & mean, const double & sigma, const double & min);
  double statmech_S(const double & mean, const double & K, const double & min, const double & lambda_cube);
}

//-----------------------------------------------------------------------------------------------
// voro++ tessellation
namespace UTIL {
  int compute_voronoi_volumes(const std::vector<Vec3d_t> & points,     // input: array of particles to process
			      const std::vector<double> & weigths,     // input: weights for radical voronoi
			      const double walls_min[3],	       // input: minimum walls limits
			      const double walls_max[3],	       // input: maximum walls limits
			      const bool periodic[3],		       // input: periodicity flags on each direction
			      const int & dim,	         	       // input: number of dimensions (2 or 3)
			      std::vector<double> & voronoi_volumes,   // output: Voronoi volumes per particle 
			      std::vector<Vec3d_t> & centroids,        // output: centroids of the voronoi cells 
			      std::vector<std::vector<int> > & neigh,  // output: to save neighbor information
			      const bool & print = false);              // output: print system to povray

  int compute_voronoi_volumes(const std::vector<Particle> & particles, // input: array of particles to process
			      const double walls_min[3],	     // input: minimum walls limits
			      const double walls_max[3],	     // input: maximum walls limits
			      const bool periodic[3],		     // input: periodicity flags on each direction
			      const int & dim,	         	     // input: number of dimensions (2 or 3)
			      std::vector<double> & voronoi_volumes, // output: Voronoi volumes per particle 
			      std::vector<std::vector<int> > & neigh);  // output: to save neighbor information
} // namespace UTIL


namespace UTIL {
  //-----------------------------------------------------------------------------------------------
  // compute coronas limits. RT= RightTop, LB = LeftBottom
  //-----------------------------------------------------------------------------------------------
  int compute_coronas_limits_constant_volume(const Vec3d_t & LB,                     // LeftBottom coordinates of full box
					     const Vec3d_t & RT,		           // RigthTop coordinates of full box
					     const double & deltamin,		   // Minimum size of corona
					     const int & dim,			   // Dimension of domain, 2 or 3
					     int & m, 				   // output : Number of coronas
					     std::vector<Vec3d_t> & corona_LB,	   // output : Coronas limits LB
					     std::vector<Vec3d_t> & corona_RT) ;     // output : Coronas limits RT

  int compute_coronas_limits_constant_width(const Vec3d_t & LB,                      // LeftBottom coordinates of full box
					    const Vec3d_t & RT,		           // RigthTop coordinates of full box
					    const int & dim,			   // Dimension of domain, 2 or 3
					    const int & m, 			   // Fixed number of coronas
					    std::vector<Vec3d_t> & corona_LB,	   // output : Coronas limits LB
					    std::vector<Vec3d_t> & corona_RT) ;      // output : Coronas limits RT

  double box_volume(const Vec3d_t & LeftBottom, const Vec3d_t & RightTop, const int & dim) ;
 
} // namspace UTIL


#endif // __UTILITY__
