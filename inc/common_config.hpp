// This files stores common flags across several simulation parts

#ifndef __COMMON_CONFIG_HPP__
#define __COMMON_CONFIG_HPP__ 1


enum DEM_TYPE { MD=0, CD };                     // type of DEM simulation

// basenames for file printing
#define BODIES_INFO "bodiesInfo"
#define CONTACTS_INFO "contactsInfo"


// netcdf or txt?
#if WITH_NETCDF
#include "netCDF_io.hpp"
#define EXT "nc"
#define BODIES_INFO_FILE BODIES_INFO "." EXT
#define CONTACTS_INFO_FILE CONTACTS_INFO "." EXT
#else 
#define EXT "txt"
#define BODIES_INFO_FILE BODIES_INFO "." EXT
#define CONTACTS_INFO_FILE CONTACTS_INFO "." EXT
#endif



#endif // ____COMMON_CONFIG_HPP__ 
