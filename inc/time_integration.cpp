#include "time_integration.hpp"

namespace DEM {
  namespace time_integ {

    TimeIntegrator::TimeIntegrator() 
    {
      // get memory
      global_dof_control_.resize(SIZE_DOF_CONTROL, false);
      // read dof control info
      YAML::Node config = YAML::LoadFile("INPUT/general_conf.yaml");
      if   (0 != config["TYPE01"].as<int>()) { 
	global_dof_control_[0] = true;
	std::cout << "TIME_INTEGRATION: Isochoric Biaxial ACTIVATED" << std::endl; 
      }
      else if (0 != config["TYPE02"].as<int>()) {
	global_dof_control_[1] = true;
	std::cout << "TIME_INTEGRATION: Isotropic ACTIVATED" << std::endl;
      }
      else if (0 != config["TYPE03"].as<int>()) {
	global_dof_control_[2] = true;
	std::cout << "TIME_INTEGRATION: Isochoric triaxial ACTIVATED" << std::endl;
      }
    }

    int 
    TimeIntegrator::LeapFrogStart(std::vector<Particle> &  bodies, const double & dt, const int & iter, DEM::Fixes::BoundaryFix & bcfixer)
    {
      // process all bodies
      for (auto & body : bodies) {
	if (false == body.alive) continue;
	body.V -= 0.5*dt*body.F/body.mass;  // V(-dt/2) = V(0) -dt/2*a(0)
	body.W -= 0.5*dt*body.T/body.inerI; // V(-dt/2) = V(0) -dt/2*a(0)
      }
      
      apply_bcfix(bodies, dt, iter, 0.0, bcfixer); // phi = 0.0 is just to avoid using it
      global_fix(bodies);

      // setup dr stuff to detect update of particles
      dR.resize(3*(bodies.size() - DEM::NWALLS), 0.0);
      
      return EXIT_SUCCESS;
    }

    bool 
    TimeIntegrator::LeapFrogStep(std::vector<Particle> & bodies, const double & dt, const int & iter, const double & phi, DEM::Fixes::BoundaryFix & bcfixer, const double & p, const double & q, const double & dupdate)
    {
      bool update = false; // test if neighbor list must be updated
      
      const int nbodies = bodies.size();
      const int ngrains = bodies.size() - DEM::NWALLS;
      // particles
      Vec3d_t dP;
      for (int id = 0; id < ngrains; ++id) {
	if (false == bodies[id].alive) continue;
	bodies[id].V += dt*(bodies[id].F/bodies[id].mass);  // V(t+dt/2) = V(t-dt/2) + dt*a(t) 
	dP = dt*bodies[id].V;
	bodies[id].R += dP;                                 // R(t+dt) = R(t) + dt*V(t+dt/2)
	dR[id*3 + 0] += dP[0]; dR[id*3 + 1] += dP[1]; dR[id*3 + 2] += dP[2]; 
	bodies[id].W += dt*(bodies[id].T/bodies[id].inerI); // V(t+dt/2) = V(t-dt/2) + dt*a(t) 
	bodies[id].PHI += dt*bodies[id].W;                  // R(t+dt) = R(t) + dt*V(t+dt/2)
      }
      // compute the two maximal displacements
      double drmax1 = 0.0, drmax2 = 0.0, dr = 0.0;
      for(int id = 0; id < ngrains; ++id) {
	dr = std::sqrt(dR[id*3+0]*dR[id*3+0] + dR[id*3+1]*dR[id*3+1] + dR[id*3+2]*dR[id*3+2]);
	if(dr > drmax1) {
	  drmax2 = drmax1;
	  drmax1 = dr;
	}
	else if (dr > drmax2){
	  drmax2 = dr;
	}
      }
      if(drmax2 + drmax1 > dupdate) {
	update = true;
	std::fill(dR.begin(), dR.end(), 0.0);
      }
      
      // walls velocities and fixes
      apply_bcfix(bodies, dt, iter, phi, bcfixer, p, q);
      global_fix(bodies);

      // walls positions
      for (int id = ngrains; id < nbodies; ++id) {
	if (false == bodies[id].alive) continue;
	bodies[id].R += dt*bodies[id].V; 
      }

      return update;
    }

    int
    TimeIntegrator::apply_bcfix(std::vector<Particle> & bodies, const double & dt, const int & iter, const double & phi, DEM::Fixes::BoundaryFix & bcfixer, const double p, const double q)
    {
      const int nbodies = bodies.size();
      const int ngrains = bodies.size() - DEM::NWALLS;
      // walls velocities and bcfixer
      for (int id = ngrains; id < nbodies; ++id) {
	if (false == bodies[id].alive) continue;
	const int wd = id - (bodies.size() - DEM::NWALLS);
	// NO angular velocity
	bodies[id].W = Vecnull; 
	// Linear velocity control functions
	for (auto dir = 0; dir < 3; ++dir) {
	  // velocity control
	  if (bcfixer.is_velocity_controlled(wd, dir)) {
	    bodies[id].V[dir] = bcfixer.get_fixed_value(wd, dir, iter);
	  }
	  else if (bcfixer.is_velocity_stress_controlled(wd, dir)) {
	    bodies[id].V[dir] = bcfixer.get_fixed_value(wd, dir, iter, q);
	  }
	  else { // not velocity controlled (maybe stress controlled, but effect already applied when forces where computed)
	    bodies[id].V[dir] += dt*(bodies[id].F[dir]/bodies[id].mass);
	    // maximum velocity control
	    // NEW VERSION : MaxVel decreases up to limit, as time passes, 
	    // WARNING: Maximum initial value = magic number
	    const double actual_wmaxvel = UTIL::ramp_value(phi, phithreshold_, wmaxvel_, 50*wmaxvel_);
	    if (bodies[id].V[dir] > actual_wmaxvel) 
	      bodies[id].V[dir] = actual_wmaxvel;
	    else if (bodies[id].V[dir] < -actual_wmaxvel) 
	      bodies[id].V[dir] = -actual_wmaxvel; 
	  }
	}
      }
      return EXIT_SUCCESS;
    }

    int
    TimeIntegrator::global_fix(std::vector<Particle> & bodies)
    {
      //const int nbodies = bodies.size();
      const int ngrains = bodies.size() - DEM::NWALLS;
      // TODO : Make this a function and call it also at the leap frog start function
      // TYPE01: biaxial isochoric deformation
      //       : Vx left = Vy back = Vz bottom = 0; Vx rigth = Vy front; Vz top = (-Rz right/Rx top)*Vx Right  
      if ( true == global_dof_control_[TYPE01] ) {
	// bodies[ngrains + BOTTOM].V[2] = bodies[ngrains + LEFT].V[0] = bodies[ngrains + BACK].V[1] = 0.0; // boundary condition
	// bodies[ngrains + FRONT].V[1] = bodies[ngrains + RIGHT].V[0]; // WARNING: Only if dim == 2 // boundary condition
	const double lx = bodies[ngrains + RIGHT].R[0] - bodies[ngrains + LEFT  ].R[0];
	const double lz = bodies[ngrains + TOP  ].R[2] - bodies[ngrains + BOTTOM].R[2];
	bodies[ngrains + TOP].V[2] = (-lz/lx)*bodies[ngrains + RIGHT].V[0]; 
      }
      // TYPE02: isotropic, opposite walls, opposite velocities
      else if ( true == global_dof_control_[TYPE02] ) {
	bodies[ngrains + LEFT].V[0] = -bodies[ngrains + RIGHT].V[0];
	bodies[ngrains + BACK].V[1] = -bodies[ngrains + FRONT].V[1];
	bodies[ngrains + BOTTOM].V[2] = -bodies[ngrains + TOP].V[2];
      }
      // TYPE03: isocoric, triaxial, V_x = (-x/z)Vz/2, V_y = (-y/z) Vz/2
      else if ( true == global_dof_control_[TYPE03] ) {
	const double lx = bodies[ngrains + RIGHT].R[0] - bodies[ngrains + LEFT  ].R[0];
	const double ly = bodies[ngrains + FRONT].R[1] - bodies[ngrains + BACK  ].R[1];
	const double lz = bodies[ngrains + TOP  ].R[2] - bodies[ngrains + BOTTOM].R[2];
	const double Vz = bodies[ngrains + TOP].V[2];
	bodies[ngrains + LEFT  ].V = bodies[ngrains + RIGHT  ].V = Vecnull;
	bodies[ngrains + BACK  ].V = bodies[ngrains + FRONT  ].V = Vecnull;
	bodies[ngrains + BOTTOM].V = bodies[ngrains + TOP    ].V = Vecnull;
	bodies[ngrains + TOP   ].V[2] = Vz;
	bodies[ngrains + RIGHT ].V[0] = -0.5*(lx/lz)*Vz;
	bodies[ngrains + FRONT ].V[1] = -0.5*(ly/lz)*Vz;
      }

      return EXIT_SUCCESS;
    }
    
    void 
    TimeIntegrator::phithreshold(const double & phithreshold)
    {
      ASSERT(phithreshold > 0.0);
      phithreshold_ = phithreshold;
    }
  } // namespace time_integ
} // namespace DEM

