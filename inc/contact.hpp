#ifndef __CONTACT_HPP__
#define __CONTACT_HPP__ 1

#include "vector3d.hpp"
#include "particle.hpp"
#include "materials.hpp"
#include <fstream>

namespace DEM
{
  //---------------------------------------------------------------
  // Class Contact
  //---------------------------------------------------------------
  class Contact
  {
  public:                                                                 // methods
    Contact() : updated_(false), uid1_(0), uid2_(0), uid_(0), ptr1_(nullptr), ptr2_(nullptr), delta_(0),
		normFn_(0), Normal_(Vecnull), Ft_(Vecnull), T_(Vecnull),
		SpringTorsion_(Vecnull), SpringSlide_(Vecnull), SpringRolling_(Vecnull),
		sliding_(false), residual_(true), starting_(true)
    {};
    // util
    void pre_setup(const Particle * ptr1, const Particle * ptr2, const double & delta, const Vec3d_t & Normal);
    bool collide(const Materials & MaterialVars, const double & dt);    // Computes force, returns true if successull collision
    void pre_reset();
    void reset();

    // get
    Vec3d_t F(void) {return normFn_*Normal_ + Ft_;};
    Vec3d_t F(void) const {return normFn_*Normal_ + Ft_;};
    Vec3d_t Fn(void) {return normFn_*Normal_;};
    Vec3d_t Fn(void) const {return normFn_*Normal_;};
    Vec3d_t Ft(void) {return Ft_;};
    Vec3d_t Ft(void) const {return Ft_;};
    Vec3d_t T(void) {return T_;};
    Vec3d_t T(void) const {return T_;};
    Vec3d_t N(void) {return Normal();};
    Vec3d_t N(void) const {return Normal();};
    Vec3d_t Normal(void) {return Normal_;};
    Vec3d_t Normal(void) const {return Normal_;};
    double delta(void) const {return delta_;};
    long uid(void) { return uid_; }
    long uid(void) const { return uid_; }
    long uid1(void) { return uid1_; }
    long uid1(void) const { return uid1_; }
    long uid2(void) { return uid2_; }
    long uid2(void) const { return uid2_; }
    // logical
    bool operator<(const Contact & other) const { return (uid1_ < other.uid1_) ; } // sort by first particle index and then second
    bool operator<(const Contact & other) { return (uid1_ < other.uid1_); }
    bool operator==(const Contact & other);
    // friend classes
    friend class Interaction;                                           // free access for Interaction class
    friend class Postpro;                                               // free access for Postpro base class
    friend class PostproAnalysis;                                       // free access for Postpro class
    friend class Visualization;                                         // free access for Visualization class
    // i/o functions
    void print(std::FILE * pf);
    void read (std::FILE * pf);

  public:                                           // data
    bool updated_ = false;                          //< updated or not?
    int uid1_ = 0, uid2_ = 0;                       //< Unique Indexes of contact and contacting boodies
    long uid_ = 0;                                  //< Unique composite contact index, computed at utility.hpp 
    Particle *ptr1_ = nullptr, *ptr2_ = nullptr;    //< NON-owning pointers to particles
    double delta_  = 0.0;                           //< Interpenetration
    double normFn_ = 0.0;                           //< normal force magnitud
    Vec3d_t Normal_ ;                               //< Normal unitary vector, from jd to id
    Vec3d_t Ft_;                                    //< tangential force
    Vec3d_t T_;                                     //< Total Torque
    // Springs for Cundall friction model
    Vec3d_t SpringTorsion_, SpringSlide_, SpringRolling_;
    bool sliding_  = false;                         //< Sliding or not?
    bool residual_ = true;                          //< Residual contact? if new, true
    bool starting_ = true;

    // cohesion
    Vec3d_t Pi_, Pj_;
    bool broken_ = false;

  private:                                                                // methods
    bool friction_force(const double & kt, const double & mus, const double & muk, const double & gammat,  // materials
			   const Vec3d_t & Vt,  // tangential vel
			   Vec3d_t & Spring, Vec3d_t & Ft,  // output
			   const double & alpha = 1.0); // mcnamara correction
    void set_reduced_radii(const Particle & Body1, const Particle & Body2,
			   double & radinew, double & radjnew, double & radij);            // Reduced Radius
  private :
    static constexpr double EPS_ = 1.0e-32;             //< EPS for small values, like delta
    static constexpr double EPS_FRICTION_ = 1.0e-32;    //< EPS for small values of friction coeffs
  };
  
} // end namespace  

#endif
