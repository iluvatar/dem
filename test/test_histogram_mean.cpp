#include "../inc/utility.hpp"
#include "../inc/histogram1d.hpp"
#include "../inc/random.hpp"
#include "iostream"
#include "iomanip"

const double MIN = 0.0; 
const double MAX = 10000.0; 

int main(int argc, char **argv) 
{
  if (3 != argc) {
    std::cerr << "Bad number of arguments for " << argv[0] << ". Exiting." << std::endl;
    return EXIT_FAILURE;
  }
  
  const int SIZE = std::atoi(argv[1]);
  const int BINS = std::atoi(argv[2]);

  
  // random number
  Random ran2;
  ran2.seed(0);

  // create array
  double * data;
  data = new double [SIZE];

  // create histogram
  Histogram1D histo;
  histo.construct(MIN, MAX, BINS);

  // fill array
  for (int ii = 0; ii < SIZE; ++ii) {
    data[ii] = ran2.uniform(MIN, MAX);
  }
  
  // load array to histogram
  histo.increment(data, SIZE);

  // mean and sigma
  double mean1, mean2, sigma;
  std::cout.setf(std::ios::scientific);
  // print mean from array
  mean_and_sigma(data, SIZE, mean1, sigma);
  std::cout << "Mean from array     = " << std::setprecision(16) << mean1 << std::endl;
  // print mean from histogram
  histo.mean_and_sigma(mean2, sigma);
  std::cout << "Mean from histogram = " << std::setprecision(16) << mean2 << std::endl;
  histo.print("histo.dat");

  if (std::fabs((mean2-mean1)/mean2) > 1.0e-12) return EXIT_FAILURE;

  return EXIT_SUCCESS;
}
