#!/bin/bash

EXENAME=test_ncio_contacts.x 

# compile the test
echo "Compiling ..."
g++-4.7 -g -ggdb -DDEBUG -O3 -std=c++11   `nc-config --cflags ` `nc-config --libs` -lnetcdf_c++4 -Wall test_ncio_contacts.cpp -o $EXENAME
echo "Done compiling."

# create a temp directory
TMPDIR=$(mktemp -d ncio.XXXXX)
mv $EXENAME $TMPDIR
cd $TMPDIR

# run the test for several sizes and several number of files
NFILES=100
NCONTACTS=3000
for(( nfiles = 1; nfiles <= $NFILES; nfiles *= 10 )); do
    echo "nfiles = $nfiles"
    for(( ncontacts = 1; ncontacts <= $NCONTACTS; ncontacts *= 3 )); do
	echo "ncontacts = $ncontacts"
	./$EXENAME $ncontacts $nfiles
	if [[ "$?" != "0" ]]; then 
	    echo "ERROR RUNNING NC WRITE AND READ TEST FOR CONTACTS"
	    exit 1;
	fi
	rm -rf test_w_*
    done
    echo
done

# remove tmpdir
cd ..
rm -rf $TMPDIR
rm -rf *.dSYM

echo "TEST SUCCESSFULL : ncio contacts"