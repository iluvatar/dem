// Test for delaunay_tetgen 

#include "../inc/postpro/delaunay_tetgen.hpp"
#include "../inc/particle.hpp"
#include <vector>
#include <set>

int test_01();
int test_02();

int main(int argc, char **argv)
{
  ASSERT(2 == argc);
  const int seed = std::abs(std::atoi(argv[1]));
  std::srand(seed);
  srand48(seed);

  ASSERT(EXIT_SUCCESS == test_01());
  ASSERT(EXIT_SUCCESS == test_02());

  return EXIT_SUCCESS;
}

int test_01()
{
  std::cout << "\nDELAUNAY TEST :: START :: Test 01 : Box with 9 particles, volume of box" << std::endl;

  const int NBODIES = 9;
  const double LX = 2.0;
  const double LY = std::sqrt(2.0);
  const double LZ = 30.0;

  // create dummy particles inside a box
  std::vector<Particle> bodies(NBODIES); 
  bodies[0].R = Vec3d_t(0 , 0 , 0 );
  bodies[1].R = Vec3d_t(LX, 0 , 0 );
  bodies[2].R = Vec3d_t(LX, LY, 0 );
  bodies[3].R = Vec3d_t(0 , LY, 0 );
  bodies[4].R = Vec3d_t(0 , 0 , LZ);
  bodies[5].R = Vec3d_t(LX, 0 , LZ);
  bodies[6].R = Vec3d_t(LX, LY, LZ);
  bodies[7].R = Vec3d_t(0 , LY, LZ);
  bodies[8].R = Vec3d_t(LX/2 , LY/2, LZ);
  std::random_shuffle(bodies.begin(), bodies.end());
  // create valid indexes 
  std::set<int> indexes; 
  for (int ii = 0; ii < bodies.size(); ++ii) indexes.insert(ii);

  // call delaunay
  DEM::POSTPRO::Delaunay delaunay;
  std::cout << "DONE :: Declaring delaunay object" << std::endl;
  ASSERT(EXIT_SUCCESS == delaunay.init_tasks(0.5)); // rad = 0.5 -> unit diammeter
  std::cout << "DONE :: Initializing delaunay object" << std::endl;
  ASSERT(EXIT_SUCCESS == delaunay(bodies, indexes, 0)); // nwalls = 0
  std::cout << "DONE :: Processing bodies with delaunay object" << std::endl;
  
  // CHECKS
  // check delaunay cells volume == LX LY LZ
  std::cout << "LX*LY*LZ        = " << LX*LY*LZ << std::endl;
  std::cout << "delaunay volume = " << delaunay.total_volume() << std::endl;
  ASSERT(std::fabs(delaunay.total_volume() - LX*LY*LZ) < 1.0e-10*LX*LY*LZ );
  // How many delaunay cells? 
  //ASSERT(6 == delaunay.ntetra());
  
  // print data to csv (for paraview)
  std::ofstream fout("POSTPRO/poinst.csv");
  fout << "x, y , z \n"; 
  for (auto & body : bodies) fout << body.R[0] << ", " << body.R[1] << ", " << body.R[2] << "\n";
  fout.close();
  
  std::cout << "\nDELAUNAY TEST :: DONE :: Test 01" << std::endl << std::endl;

  return EXIT_SUCCESS;
}

int test_02()
{
  std::cout << "\nDELAUNAY TEST :: START :: Test 02 : Random points in unit cube\n" << std::endl;
  // create dummy particles inside a box
  const int size = 2000;
  std::vector<Particle> bodies(size); 
  bodies[0].R = Vec3d_t(0, 0, 0);
  bodies[1].R = Vec3d_t(1, 0, 0);
  bodies[2].R = Vec3d_t(1, 1, 0);
  bodies[3].R = Vec3d_t(0 ,1, 0);
  bodies[4].R = Vec3d_t(0 ,0, 1);
  bodies[5].R = Vec3d_t(1, 0, 1);
  bodies[6].R = Vec3d_t(1, 1, 1);
  bodies[7].R = Vec3d_t(0, 1, 1);
  for (int ii = 8; ii < size; ++ii) {
    bodies[ii].R = Vec3d_t(drand48(), drand48(), drand48());
  }
  // create valid indexes 
  std::set<int> indexes; 
  for (int ii = 0; ii < size; ++ii) indexes.insert(ii);
  
  // call delaunay
  DEM::POSTPRO::Delaunay delaunay;
  std::cout << "DONE :: Declaring delaunay object" << std::endl;
  ASSERT(EXIT_SUCCESS == delaunay.init_tasks(0.5)); // rad = 0.5, diammeter = 1.0
  std::cout << "DONE :: Initializing delaunay object" << std::endl;
  ASSERT(EXIT_SUCCESS == delaunay(bodies, indexes, 0));
  std::cout << "DONE :: Processing bodies with delaunay object" << std::endl;
  
  // CHECKS
  // check delaunay cells volume == 1
  std::cout << "LX*LY*LZ        = " << 1 << std::endl;
  std::cout << "delaunay volume = " << delaunay.total_volume() << std::endl;
  std::cout << "diff            = " << std::fabs(delaunay.total_volume() - 1.0) << std::endl;
  ASSERT(std::fabs(delaunay.total_volume() - 1.0) < 1.0e-12 );

  std::cout << "\nDELAUNAY TEST :: DONE  :: Test 02" << std::endl << std::endl;
  return EXIT_SUCCESS;
}

bool is_inside(const double & val, const double & min, const double & max)
{
  return (min <= val && val <= max);
}

bool is_inside(const Vec3d_t & P, const double & min, const double & max)
{
  return (is_inside(P[0], min, max) && is_inside(P[1], min, max) && is_inside(P[2], min, max));
}

struct is_not_inside {
  double min_, max_;
  bool operator()(const Particle & body) {
    return !is_inside(body.R, min_, max_);
  }
};

