#!/bin/bash

BASENAME=crystallization
echo "Running test : ${BASENAME}"

EXENAME=test_${BASENAME}.x

# compile the test
echo "Compiling ..."
g++-4.7 -std=c++11 -I/usr/local/include/eigen3 test_${BASENAME}.cpp -o $EXENAME -ltet -Wno-write-strings  -DDEBUG
if [[ 0 != $? ]]; then 
    echo "Error compiling test. Exiting. ";
    exit 1;
fi
echo "Done compiling."

# create a temp directory
TMPDIR=$(mktemp -d ${BASENAME}.XXXXX)
mv $EXENAME $TMPDIR
cd $TMPDIR
mkdir POSTPRO

# run the test for several seeds
NSEEDS=10 
for(( seed = 1; seed <= $NSEEDS; seed += 1 )); do
    echo "seed = $seed"
    ./$EXENAME $seed  
    if [[ "$?" != "0" ]]; then
        echo "ERROR RUNNING TEST :: ${BASENAME}"
        exit 1;
    fi
    echo
done

# remove tmpdir
cd ..
rm -rf $TMPDIR

echo "TEST SUCCESSFULL : ${BASENAME}"

