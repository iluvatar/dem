#include "../inc/nlfit.hpp"
#include <iostream>


// example function
double
func(const double & x, const double * par)
{
  // linear function
  //return par[0]*x + par[1]; 

  // cuadratic function
  return par[0]*x*x + par[1]*x + par[2]; 
}


int main(int argc, char *argv[])
{
  const int m=15, n=3;
  
  // declare and fill the data
  double *param; param = new double [n];
  param[0] = param[1] = param[2] = 1;
  double *eps; eps = new double[n];
  double *xdata; xdata = new double [m];
  
  double *ydata; ydata = new double [m];
  for (int ii = 0; ii < m; ++ii) {
    xdata[ii] = 2*ii;
    //ydata[ii] = 0.35*xdata[ii] + 100.98; // linear
    ydata[ii] = -0.35*xdata[ii]*xdata[ii] + 0.0*xdata[ii] - 32.76; // cuadratic
  }
  param[1] = 10; param[2] = -45.76;

  // do the fitting
  //NLFIT::nlfit(n, m, xdata, ydata, func, param, eps, n);
  NLFIT::nlfit(1, m, xdata, ydata, func, param, eps, n); // only fit first parameter
  
  // print the parameters x
  for (int ii = 0; ii < n; ++ii) {
    std::cout << param[ii] << "\t" << eps[ii] << std::endl;
  }
  
  
  return EXIT_SUCCESS;
}
