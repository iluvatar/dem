// Test for delaunay_tetgen 

#include "../inc/postpro/crystallization.hpp"
#include "../inc/contact.hpp"
#include <vector>
#include <set>

int test_01(); // test if crystallization is     detected for a system on a square     grid
int test_02(); // test if crystallization is     detected for a system on a triangular grid
int test_03(); // test if crystallization is NOT detected for a system on a random     grid

int main(int argc, char **argv)
{
  ASSERT(2 == argc);
  const int seed = std::abs(std::atoi(argv[1]));
  std::srand(seed);
  srand48(seed);

  ASSERT(EXIT_SUCCESS == test_01());
  ASSERT(EXIT_SUCCESS == test_02());
  ASSERT(EXIT_SUCCESS == test_03());

  return EXIT_SUCCESS;
}

int test_01()
{
  std::cout << "\nCRYSTALLIZATION TEST :: START :: Test 01 :: Rectangular lattice" << std::endl;
  
  const int NX = 10;
  const int NZ = 30;
  const int NCONTACTS = 2*NX + NZ*(NX + 1); 
  std::vector<DEM::Contact> contacts(NCONTACTS); 
  
  // Assign normal vectors to contacts, according to particles inside square grid
  for (int ii = 0; ii < NX; ++ii) {
    contacts[ii].Normal_ = Vec_uz;                   // BOTTOM
    contacts[NCONTACTS - 1 - ii].Normal_ = Vec_uz;   // TOP
  }
  for (int ii = NX; ii < NCONTACTS - NX; ++ii) {
    contacts[ii].Normal_ = Vec_ux;
  }
  
  // call crystallization and check return result (should be 1, it is crystallized)
  DEM::POSTPRO::Crystallization crystal;
  crystal.init_tasks();
  ASSERT(1 == crystal(0, contacts));

  std::cout << "\nCRYSTALLIZATION TEST :: END   :: Test 01 :: Rectangular lattice" << std::endl;
  return EXIT_SUCCESS;
}

int test_02()
{
  std::cout << "\nCRYSTALLIZATION TEST :: START :: Test 02 :: Triangular lattice" << std::endl;
  
  const int NX = 10;
  const int NZ = 30;
  const int NCONTACTS = NX*NZ; 
  std::vector<DEM::Contact> contacts(NCONTACTS); 
  
  // Assign normal vectors to contacts, according to particles inside triangular grid
  Vec3d_t n[2] = { Vec3d_t(0.5, 0.0, std::sqrt(3.0)/2.0), Vec3d_t(-0.5, 0.0, std::sqrt(3.0)/2.0) };
  for (int iz = 0; iz < NZ; ++iz) {
    for (int ix = 0; ix < NX; ++ix) {
      contacts[ix + NX*iz].Normal_ = n[ix % 2];
    }
  }
  
  // call crystallization and check return result (should be 1, it is crystallized)
  DEM::POSTPRO::Crystallization crystal;
  crystal.init_tasks();
  ASSERT(1 == crystal(0, contacts));
  
  std::cout << "\nCRYSTALLIZATION TEST :: END   :: Test 02 :: Triangular lattice" << std::endl;
  return EXIT_SUCCESS;
}


int test_03()
{
  std::cout << "\nCRYSTALLIZATION TEST :: START :: Test 03 :: Random lattice" << std::endl;
  
  const int NCONTACTS = 10000; 
  std::vector<DEM::Contact> contacts(NCONTACTS); 
  
  // Assign normal vectors to contacts, according to particles inside random grid
  for (auto & c : contacts) {
    c.Normal_ = Vec3d_t(-1 + 2*drand48(), -1 + 2*drand48(), -1 + 2*drand48());
  }
  
  // call crystallization and check return result (should be 1, it is crystallized)
  DEM::POSTPRO::Crystallization crystal;
  crystal.init_tasks();
  ASSERT(0 == crystal(0, contacts));
  
  std::cout << "\nCRYSTALLIZATION TEST :: END   :: Test 03 :: Random lattice" << std::endl;
  return EXIT_SUCCESS;
}
