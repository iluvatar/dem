#!/bin/bash

for a in ./test_*sh; do 
    REPORT=${a%.sh}.txt
    printf "Running   \t::\t\t $a\n"
    ./$a &> $REPORT
    if [[ 0 != $? ]]; then
	echo "ERROR running test : $a"
	printf "Report in \t::\t\t $REPORT\n"
	echo "Exiting without running the remaining tests"
	exit 1;
    else
	rm -f $REPORT
    fi
    echo ""
done
