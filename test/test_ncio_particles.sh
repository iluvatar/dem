#!/bin/bash

EXENAME=test_ncio_particles.x 

# compile the test
echo "Compiling ..."
g++-4.7 -std=c++11 -g -ggdb -DDEBUG -O3   `nc-config --cflags ` `nc-config --libs` -lnetcdf_c++4 -Wall test_ncio_particles.cpp -o $EXENAME
echo "DONE: Compiling ."

# create a temp directory
TMPDIR=$(mktemp -d ncio.XXXXX)
mv $EXENAME $TMPDIR
cd $TMPDIR

# run the test for several sizes and several number of files
NFILES=100
NBODIES=3000
for(( nfiles = 1; nfiles <= $NFILES; nfiles *= 10 )); do
    echo "nfiles = $nfiles"
    for(( nbodies = 1; nbodies <= $NBODIES; nbodies *= 3 )); do
	echo "nbodies = $nbodies"
	./$EXENAME $nbodies $nfiles
	if [[ "$?" != "0" ]]; then 
	    echo "ERROR RUNNING NC WRITE AND READ TEST FOR PARTICLES"
	    exit 1;
	fi
	rm -rf test_w_*
    done
    echo
done

# remove tmpdir
cd ..
rm -rf $TMPDIR
rm -rf *.dSYM

echo "TEST SUCCESSFULL : ncio particles"