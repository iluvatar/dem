// This files tests netCDF io for contacts

#include "../inc/netCDF_io.hpp"
#include "../inc/contact.hpp"
#include <cstdlib>

// filenames
#define OUTPUT_FN_FULL_NC "test_w_contacts_%010ld.nc"
#define OUTPUT_FN_CHANGING_NC "test_w_contacts_changing_time_%010ld.nc"
#define OUTPUT_FN_FULL_TXT "test_w_contacts_%010ld.txt"


// call as: program_name nbodies
int main (int argc, char **argv) 
{
  // check arguments
  if (argc != 3) {
    std::cerr << "Bad usage. Call as : " << argv[0] << " ncontacts  nfiles " << std::endl;
    return EXIT_FAILURE;
  }

  // set ncontacts
  const long NCONTACTS = std::atol(argv[1]);
  // set nfiles
  const long NFILES  = std::atol(argv[2]);

  // for each file 
  for (long ifile = 0; ifile < NFILES; ++ifile ) {
    char filename[100] ;

    // create and fill an array of n-contacts
    DEM::Contact tmp_contact;
    std::vector<DEM::Contact> contacts(NCONTACTS);
    for (long id = 0; id < NCONTACTS; ++id) {
      // set some values to tmp contact
      tmp_contact.uid1_ = id;
      tmp_contact.uid2_ = NCONTACTS*drand48();
      tmp_contact.uid_ = get_uid(tmp_contact.uid1_, tmp_contact.uid2_);
      tmp_contact.delta_ = drand48();
      tmp_contact.normFn_ = drand48();
      tmp_contact.Normal_.setRandom(); 
      tmp_contact.Fn_.setRandom(); 
      tmp_contact.Ft_.setRandom(); 
      tmp_contact.T_.setRandom();
      tmp_contact.SpringTorsion_.setRandom(); 
      tmp_contact.SpringSlide_.setRandom(); 
      tmp_contact.SpringRolling_.setRandom();
      // add contact to vector
      contacts[id] = tmp_contact;
    }
    
    // write the array to a net cdf file
    std::sprintf(filename, OUTPUT_FN_FULL_NC, ifile);
    write_contacts_netcdf(contacts, filename);
    
    // create array for read contacts
    std::vector<DEM::Contact> read_contacts(NCONTACTS);
    
    // read the file from a netcdf one
    std::sprintf(filename, OUTPUT_FN_FULL_NC, ifile);
    read_contacts_netcdf(filename, read_contacts);
    
    // check the values read per particle
    for (long i = 0; i < NCONTACTS; ++i) {
      if (!(contacts[i] == read_contacts[i])) { // comparing floats??? yes, in binary, we don't loose precision
	std::cerr << "ERROR comparing written and read contacts" << std::endl;
	return EXIT_FAILURE;
      }
    }
    ///*
    // print equivalent data to text file, to compare sizes
    std::sprintf(filename, OUTPUT_FN_FULL_TXT, ifile);
    std::FILE * fp = std::fopen(filename, "w");
    for (long i = 0; i < NCONTACTS; ++i) {
      contacts[i].print(fp);
    }
    std::fclose(fp);
    //*/
  }
  
  return EXIT_SUCCESS;
}
