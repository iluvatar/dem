#!/bin/bash

EXENAME=test_delaunay_tetgen.x

# compile the test
echo "Compiling ..."
g++-4.7 -std=c++11 -I/usr/local/include/eigen3 test_delaunay_tetgen.cpp -o $EXENAME -ltet -Wno-write-strings  -DDEBUG -DTETLIBRARY
if [[ 0 != $? ]]; then 
    echo "Error compiling test. Exiting. ";
    exit 1;
fi
echo "Done compiling."

# create a temp directory
TMPDIR=$(mktemp -d delaunay_tetgen.XXXXX)
mv $EXENAME $TMPDIR
cd $TMPDIR
mkdir POSTPRO

# run the test for several seeds
NSEEDS=10 
for(( seed = 1; seed <= $NSEEDS; seed += 1 )); do
    echo "seed = $seed"
    ./$EXENAME $seed  
    if [[ "$?" != "0" ]]; then
        echo "ERROR RUNNING TEST :: delaunay tetgen"
        exit 1;
    fi
    echo
done

# remove tmpdir
cd ..
rm -rf $TMPDIR

echo "TEST SUCCESSFULL : delaunay"

