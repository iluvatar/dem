#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# create a new 'PVD Reader'
datapvd = PVDReader(FileName='/Users/oquendo/Desktop/work/academic/codes/dem-workspace/dem/test/simuls/test-02-isotropic/DISPLAY/data.pvd')

# get animation scene
animationScene1 = GetAnimationScene()

# update animation scene based on data timesteps
animationScene1.UpdateAnimationUsingDataTimeSteps()

# set active source
SetActiveSource(datapvd)

# get active view
renderView1 = GetActiveViewOrCreate('RenderView')
# uncomment following to set a specific view size
# renderView1.ViewSize = [1036, 1076]

# get color transfer function/color map for 'radius'
radiusLUT = GetColorTransferFunction('radius')

# show data in view
datapvdDisplay = Show(datapvd, renderView1)
# trace defaults for the display properties.
datapvdDisplay.Representation = 'Surface'
datapvdDisplay.ColorArrayName = ['POINTS', 'radius']
datapvdDisplay.LookupTable = radiusLUT
datapvdDisplay.OSPRayScaleArray = 'radius'
datapvdDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
datapvdDisplay.SelectOrientationVectors = 'Velocity'
datapvdDisplay.ScaleFactor = 0.0016333247558187253
datapvdDisplay.SelectScaleArray = 'radius'
datapvdDisplay.GlyphType = 'Arrow'
datapvdDisplay.GlyphTableIndexArray = 'radius'
datapvdDisplay.DataAxesGrid = 'GridAxesRepresentation'
datapvdDisplay.PolarAxes = 'PolarAxesRepresentation'
datapvdDisplay.GaussianRadius = 0.0008166623779093627
datapvdDisplay.SetScaleArray = ['POINTS', 'radius']
datapvdDisplay.ScaleTransferFunction = 'PiecewiseFunction'
datapvdDisplay.OpacityArray = ['POINTS', 'radius']
datapvdDisplay.OpacityTransferFunction = 'PiecewiseFunction'

# show color bar/color legend
datapvdDisplay.SetScalarBarVisibility(renderView1, True)

# reset view to fit data
renderView1.ResetCamera()

# show data in view
datapvdDisplay = Show(datapvd, renderView1)

# reset view to fit data
renderView1.ResetCamera()

# show color bar/color legend
datapvdDisplay.SetScalarBarVisibility(renderView1, True)

# update the view to ensure updated data information
renderView1.Update()

# hide data in view
Hide(datapvd, renderView1)

# create a new 'Glyph'
glyph1 = Glyph(Input=datapvd,
    GlyphType='Arrow')
glyph1.Scalars = ['POINTS', 'radius']
glyph1.Vectors = ['POINTS', 'Velocity']
glyph1.ScaleFactor = 0.0016333247558187253
glyph1.GlyphTransform = 'Transform2'

# Properties modified on glyph1
glyph1.GlyphType = 'Sphere'
glyph1.ScaleMode = 'scalar'
glyph1.ScaleFactor = 2.0
glyph1.GlyphMode = 'All Points'

# show data in view
glyph1Display = Show(glyph1, renderView1)
# trace defaults for the display properties.
glyph1Display.Representation = 'Surface'
glyph1Display.ColorArrayName = ['POINTS', 'radius']
glyph1Display.LookupTable = radiusLUT
glyph1Display.OSPRayScaleArray = 'radius'
glyph1Display.OSPRayScaleFunction = 'PiecewiseFunction'
glyph1Display.SelectOrientationVectors = 'GlyphVector'
glyph1Display.ScaleFactor = 0.0017597013240447268
glyph1Display.SelectScaleArray = 'radius'
glyph1Display.GlyphType = 'Arrow'
glyph1Display.GlyphTableIndexArray = 'radius'
glyph1Display.DataAxesGrid = 'GridAxesRepresentation'
glyph1Display.PolarAxes = 'PolarAxesRepresentation'
glyph1Display.GaussianRadius = 0.0008798506620223634
glyph1Display.SetScaleArray = ['POINTS', 'radius']
glyph1Display.ScaleTransferFunction = 'PiecewiseFunction'
glyph1Display.OpacityArray = ['POINTS', 'radius']
glyph1Display.OpacityTransferFunction = 'PiecewiseFunction'

# reset view to fit data
renderView1.ResetCamera()

# show color bar/color legend
glyph1Display.SetScalarBarVisibility(renderView1, True)

# update the view to ensure updated data information
renderView1.Update()

# set scalar coloring
ColorBy(glyph1Display, ('POINTS', 'nc'))

# Hide the scalar bar for this color map if no visible data is colored by it.
HideScalarBarIfNotNeeded(radiusLUT, renderView1)

# rescale color and/or opacity maps used to include current data range
glyph1Display.RescaleTransferFunctionToDataRange(True, False)

# show color bar/color legend
glyph1Display.SetScalarBarVisibility(renderView1, True)

# get color transfer function/color map for 'nc'
ncLUT = GetColorTransferFunction('nc')

# Rescale transfer function
ncLUT.RescaleTransferFunction(0.0, 8.0)

# get opacity transfer function/opacity map for 'nc'
ncPWF = GetOpacityTransferFunction('nc')

# Rescale transfer function
ncPWF.RescaleTransferFunction(0.0, 8.0)

# reset view to fit data
renderView1.ResetCamera()

#### saving camera placements for all active views

# current camera placement for renderView1
renderView1.CameraPosition = [0.05306030264472812, -0.028077352732458222, 0.028763806696100864]
renderView1.CameraFocalPoint = [0.008817758736768155, 0.00882243872911204, 0.008838377563733957]
renderView1.CameraViewUp = [-0.30154652462527426, 0.1471330191854021, 0.9420305558482733]
renderView1.CameraParallelScale = 0.01581609441379306

#### uncomment the following to render all views
RenderAllViews()
# alternatively, if you want to write images, you can use SaveScreenshot(...).
