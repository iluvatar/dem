#!/bin/bash

EXENAME=test_histogram_mean.x

# compile the test
echo "Compiling ..."
g++-4.7 -std=c++11 test_histogram_mean.cpp -o $EXENAME
if [[ 0 != $? ]]; then 
    echo "Error compiling test. Exiting. ";
    exit 1;
fi
echo "Done compiling."

# create a temp directory
TMPDIR=$(mktemp -d histogram_mean.XXXXX)
mv $EXENAME $TMPDIR
cd $TMPDIR

# run the test for several sizes and several number of files
NSIZE=100000
NBINS=50000
for(( size = 1; size <= $NSIZE; size *= 2 )); do
    echo "size = $size"
    for(( bins = 1; bins <= $NBINS; bins *= 2 )); do
        echo "bins = $bins"
        ./$EXENAME $size $bins
        if [[ "$?" != "0" ]]; then
            echo "ERROR RUNNING HISTOGRAM MEAN TEST"
            exit 1;
        fi
    done
    echo
done

# remove tmpdir
cd ..
rm -rf $TMPDIR

echo "TEST SUCCESSFULL : histogram mean"

