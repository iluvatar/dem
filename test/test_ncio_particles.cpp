// This files tests netCDF io for particles and contacts

#include "../inc/netCDF_io.hpp"
#include "../inc/particle.hpp"
#include <cstdlib>

// filenames
#define OUTPUT_FN_FULL_NC "test_w_particles_%010ld.nc"
#define OUTPUT_FN_CHANGING_NC "test_w_particles_changing_time_%010ld.nc"
#define OUTPUT_FN_FULL_TXT "test_w_particles_%010ld.txt"


// call as: program_name nbodies
int main (int argc, char **argv) 
{
  // check arguments
  if (argc != 3) {
    std::cerr << "Bad usage. Call as : " << argv[0] << " nbodies  nfiles " << std::endl;
    return EXIT_FAILURE;
  }

  // set nbodies
  const long NBODIES = std::atol(argv[1]);
  // set nfiles
  const long NFILES  = std::atol(argv[2]);

  // for each file 
  for (long ifile = 0; ifile < NFILES; ++ifile ) {
    char filename[100] ;

    // create and fill an array of n-bodies
    Particle body;
    std::vector<Particle> particles(NBODIES);
    for (long i = 0; i < NBODIES; ++i) {
      // set some values to tmp body
      body.uid = i;
      body.R0.setRandom(); 
      body.R.setRandom(); body.V.setRandom(); body.F.setRandom();
      body.PHI.setRandom(); body.W.setRandom(); body.T.setRandom();
      body.mass = (1.0/3.0)*i;
      body.rad = 2.243242342324*i;
      // add body to vector
      particles[i] = body;
    }
    
    // write the array to a net cdf file
    std::sprintf(filename, OUTPUT_FN_FULL_NC, ifile);
    write_particles_netcdf(particles, filename);
    
    // create array for read particles
    std::vector<Particle> read_particles(NBODIES);
    
    // read the file from a netcdf one
    std::sprintf(filename, OUTPUT_FN_FULL_NC, ifile);
    read_particles_netcdf(filename, read_particles);
    
    // check the values read per particle
    for (long i = 0; i < NBODIES; ++i) {
      if (!(particles[i] == read_particles[i])) { // comparing floats??? yes, in binary, we don't loose precision
	std::cerr << "ERROR" << std::endl;
	return EXIT_FAILURE;
      }
    }
    ///*
    // print equivalent data to text file, to compare sizes
    std::sprintf(filename, OUTPUT_FN_FULL_TXT, ifile);
    std::FILE * fp = std::fopen(filename, "w");
    for (long i = 0; i < NBODIES; ++i) {
      particles[i].print(fp);
    }
    std::fclose(fp);
    //*/
  }
  
  return EXIT_SUCCESS;
}
