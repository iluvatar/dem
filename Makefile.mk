PREFIX ?= $(HOME)/local
# define path directories
IDIR=inc/
SDIR=src/
BDIR=bin/

# Choosing compiler
# gnu cc
CXX ?= g++-7
# intel cc
ifdef INTEL
$(info INTEL COMPILER Activated)
override CXX       = icpc -gcc-name=gcc-4.7 -gxx-name=g++-4.7
endif

# compilation modes : default mode is WITH optimization
DEBUG=
PROFILE=
GPROF=
OPT=

# set compilation mode flags
#default mode : OPT
ifndef DEBUG
ifndef PROFILE
OPT=1
endif
endif
# warning
WARN_FLAGS ?= -Wall -Wextra -Wpedantic -Wduplicated-cond -Wduplicated-branches -Wlogical-op -Wrestrict -Wnull-dereference -Wold-style-cast -Wuseless-cast  -Wdouble-promotion -Wformat=2  -Wno-ignored-qualifiers -Wno-write-strings -Wno-format -fno-strict-aliasing -Wno-deprecated-declarations -Wno-duplicated-branches -Wno-sign-compare #-Wshadow -Werror -Wno-reorder -Wno-missing-field-initializers -Wstric-overflow
#debug
# -Og requires gcc >= 4.8
ifdef DEBUG  
$(info Debug enabled)
DEBUG_FLAGS = -fopenmp -g -ggdb -Og -DDEBUG -save-temps -fsanitize=address -fno-omit-frame-pointer # -D_GLIBCXX_DEBUG -D_GLIBCXX_DEBUG_PEDANTIC
.PRECIOUS = *.o
endif
# profile
ifdef PROFILE
$(info Profile enabled)
PROFILE_FLAGS = -fopenmp -p -g -Og -DNDEBUG #-lprofiler # -lprofiler requires google performance tools 
ifdef GPROF
PROFILE_FLAGS +=  -pg
endif
endif
# optimization
ifdef OPT
$(info Optimization enabled)
ifeq (g++, $(findstring g++, $(CXX)))
OPT_FLAGS = -O2 -fopenmp -finline -finline-functions -flto -march=native # -march=native -ftree-vectorize  -mfpmath=sse,387 -msse -msse2 - -funroll-loops  #-mtune=native -ffast-math -fno-math-errno -funsafe-math-optimizations -funswitch-loops -O3  # commented are problematic
ifndef DEBUG
OPT_FLAGS += -DNDEBUG
endif
endif
ifeq (icpc, $(findstring icpc, $(CXX)))
OPT_FLAGS = -O3 -finline  -ipo -openmp  
endif
endif

# External libraries
# voro++
VORO_LIB_INCLUDE = ${PREFIX}/include/voro++/voro++.hh
# netcdf
ifdef WITH_NETCDF
#NETCDF_FLAGS= -L${PREFIX}/lib -I${PREFIX}/include `nc-config --cflags ` `nc-config --libs` -lnetcdf_c++4 -DWITH_NETCDF=1 
NETCDF_FLAGS=  -L${PREFIX}/lib -isystem ${PREFIX}/include -lnetcdf_c++ -lnetcdf -DWITH_NETCDF=1 
NETCDF_DEPS=$(IDIR)/netCDF_io.hpp
endif
# tetgen
TETGEN_FLAGS=-DTETLIBRARY
ifdef WITH_VTK
VTK_VERSION=8.1
#VTK_FLAGS=-DUSE_VTK -I${PREFIX}/include/vtk -Wno-deprecated -L${PREFIX}/lib/vtk -lvtkCommon -lvtkIO -lvtkzlib -lvtkGraphics -lvtkImaging -lvtkFiltering -lvtkGenericFiltering -lvtksys -lvtkfreetype -lvtkexpat -lvtkzlib -ldl # VTK 5
VTK_FLAGS=-DUSE_VTK -isystem${PREFIX}/include/vtk-${VTK_VERSION} -L${PREFIX}/lib/vtk -L${PREFIX}/lib/vtk-${VTK_VERSION} -isystem /usr/include/vtk-${VTK_VERSION} -L/usr/lib/vtk -L/usr/lib/vtk-${VTK_VERSION} -lvtkCommonCore-${VTK_VERSION} -lvtkIOCore-${VTK_VERSION}  -lvtkImagingCore-${VTK_VERSION} -lvtkFiltersCore-${VTK_VERSION}  -lvtksys-${VTK_VERSION}   -lvtkCommonDataModel-${VTK_VERSION}   -lvtkIOXML-${VTK_VERSION} -ldl  -lvtkexpat-${VTK_VERSION} -lvtkzlib-${VTK_VERSION} -lvtkfreetype-${VTK_VERSION} # VTK 6.0  or 7.0
endif
# yaml includes and paths
YAML_FLAGS= -L${PREFIX}/lib -isystem ${PREFIX}/include 

# CXX compiler flags
export GCC_COLORS=1 # colorize gcc output for gcc >= 4.9
CXXFLAGS  = -std=c++14  -pipe  -isystem $(IDIR) -I$(IDIR)/postpro -L$(HOME)/local/lib -isystem $(HOME)/local/include -isystem ${PREFIX}/include -L${PREFIX}/lib -isystem ${PREFIX}/include/eigen3 -isystem /usr/include/eigen3 -isystem /usr/local/include/  -isystem $(HOME)/local/include/eigen3 $(WARN_FLAGS) $(DEBUG_FLAGS) $(PROFILE_FLAGS) $(OPT_FLAGS) $(NETCDF_FLAGS) $(TETGEN_FLAGS) $(YAML_FLAGS) -Wl,-rpath,$(HOME)/local/lib 

# Additional Libraries
LDLIBS    = -lvoro++ -ltet -lyaml-cpp $(NETCDF_FLAGS)  # last compile argument to fix library finding

