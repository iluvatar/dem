//--------------------------------------------------------------------------------------------------
// Last Modification: 
//--------------------------------------------------------------------------------------------------
/// helper to setup system for temperature equilibration tests
#include <cstdlib>
#include <iostream>
#include "../inc/workspace_dem_helper.hpp"

//--------------------------------------------------------------------------------------------------
// Main
//--------------------------------------------------------------------------------------------------
int main (int argc, char **argv)
{
  // check numer of args
  if (argc != 5) {
    std::cerr << "ERROR calling program. Usage:" << std::endl;
    std::cerr << argv[0] << " MODE MATERIALTAG0 MATERIALTAG1 FRACTION" << std::endl;
    std::cerr << "MODE = 0 (Cubic nucleus), 1 (fraction/1-fraction across x axis), 2 (fraction completely mixed)" << std::endl;
    std::cerr << "MATERIALTAG0 = Material tag for nucleus (mode 0), left (mode 1), or FRACTION*N (mode 2)" << std::endl;
    std::cerr << "MATERIALTAG1 = Material tag for shell (mode 0), right (mode 1), or (1-FRACTION)*N (mode 2)" << std::endl;
    std::cerr << "FRACTION = Fraction for nucleus size (mode 0), for system x-axis cut (mode 1), or for amount of mixed particles (mode 2)." << std::endl;
    std::exit(1);
  }
  
  const int MODE = std::atoi(argv[1]);   
  ASSERT(0 <= MODE && MODE <= 2);
  const int TAG0 = std::atoi(argv[2]);
  const int TAG1 = std::atoi(argv[3]);
  const double FRACTION = std::atof(argv[4]);
  ASSERT(0 <= FRACTION && FRACTION <= 1.0);
    
  // call helper
  switch (MODE) {
  case 0:
    ASSERT(EXIT_SUCCESS == DEM::helper::create_cubic_internal_nucleus(TAG0, TAG1, FRACTION));
    break;
  case 1:
    ASSERT(EXIT_SUCCESS == DEM::helper::create_right_nucleus(TAG0, TAG1, FRACTION));
    break;
  case 2:
    ASSERT(EXIT_SUCCESS == DEM::helper::create_mixed_nucleus(TAG0, TAG1, FRACTION));
    break;
  default:
    print_error_and_exit("Non-existing mode = ", MODE);
  }


  return EXIT_SUCCESS;
}





