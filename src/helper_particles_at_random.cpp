//--------------------------------------------------------------------------------------------------
// Tries to put particles at random without overlapping 
// prints text files as: x y z id
// For more info, check help() function

//--------------------------------------------------------------------------------------------------
#include <iostream>
#include <cstdlib>
#include <vector>
#include <fstream>
#include "../inc/contact.hpp"
#include "../inc/netCDF_io.hpp"
#include "../inc/workspace_dem_config.hpp"

//--------------------------------------------------------------------------------------------------
// Declarations
void help(void);
void print_particles(const int & nparticles, 
		     const std::vector<double> & x, const std::vector<double> & y, const std::vector<double> & z,
		     const double & L, 
		     const std::string & fname, const std::string & cfname);
bool intersecting(const std::vector<double> & x, const std::vector<double> & y, const std::vector<double> & z, 
		  const double & xpos, const double & ypos, const double & zpos);


//--------------------------------------------------------------------------------------------------
// constants
const double RAD = 0.001;


//--------------------------------------------------------------------------------------------------
// Main
int main (int argc, char ** argv)
{
  if (8 != argc) { help(); return EXIT_FAILURE; }
  
  srand48(std::atoi(argv[5]));

  const int nparticles = std::atoi(argv[1]); if (nparticles <= 0) { std::cerr << "Bad number of particles\n"; help(); return EXIT_FAILURE; }
  const int ntries = std::atoi(argv[2]); if (nparticles <= 0) { std::cerr << "Bad number of tries\n"; help(); return EXIT_FAILURE; }
  const double L = std::atof(argv[3]);  if (L <= 2.0*RAD) { std::cerr << "Bad L\n"; help(); return EXIT_FAILURE; }
  const bool always_print = std::atoi(argv[4]);
  
  std::vector<double> x, y, z;
  
  for (int id = 0; id < nparticles; ++id) {
    int itry = 0;
    for (itry = 0; itry < ntries; ++itry) {
      const double xpos = RAD + drand48()*(L-2*RAD);
      const double ypos = RAD + drand48()*(L-2*RAD);
      const double zpos = RAD + drand48()*(L-2*RAD);
      if (false == intersecting(x, y, z, xpos, ypos, zpos)) {
	x.push_back(xpos); y.push_back(ypos); z.push_back(zpos);
	break; 
      }
    }
    if (itry == ntries) { // cannot put
      std::cerr << "Cannot put all particles" << std::endl;
      if (true == always_print) {
	print_particles(nparticles, x, y, z, L, argv[6], argv[7]);
	return EXIT_FAILURE;
      }
    }
  }
  
  print_particles(nparticles, x, y, z, L, argv[6], argv[7]);


  // end
  return EXIT_SUCCESS;
}

//--------------------------------------------------------------------------------------------------
// Definitions
void help(void)
{
  std::clog << "\nUsage : \nexename nparticles ntries L always_print outbodies.dat" << std::endl;
  std::clog << "exename         : this executable's name " << std::endl;
  std::clog << "nparticles      : Number of particles to put " << std::endl;
  std::clog << "ntries          : Number of tries per particle" << std::endl;
  std::clog << "L               : Side lenght for cubic box" << std::endl;
  std::clog << "always_print    : Print output even if not all particles can be put" << std::endl;
  std::clog << "seed            : Seed for random number generator" << std::endl;
  std::clog << "outbodies.dat   : Output file" << std::endl;
  std::clog << "outcontacts.dat : Output file" << std::endl;
  std::clog << "WARNING WARNING WARNING : Not taking care of dimensions or periodicity" << std::endl;
}

bool intersecting(const std::vector<double> & x, const std::vector<double> & y, const std::vector<double> & z, 
		  const double & xpos, const double & ypos, const double & zpos)
{ // ORDER N^2 :(
  for (size_t id = 0; id < x.size(); ++id) {
    const double d2 = (x[id]-xpos)*(x[id]-xpos) + (y[id]-ypos)*(y[id]-ypos) + (z[id]-zpos)*(z[id]-zpos); 
    if (d2 < 4*RAD*RAD) return true;
  }
  return false;
}

void print_particles(const int & nparticles, 
		     const std::vector<double> & x, const std::vector<double> & y, const std::vector<double> & z,
		     const double & L, 
		     const std::string & bfname, const std::string & cfname)
{
  std::cout << "Particles put       = " << x.size() << std::endl;
  std::cout << "Particles remaining = " << nparticles - x.size() << std::endl;
  std::vector<Particle> bodies(x.size() + 6);
  for (size_t id = 0; id < x.size(); ++id) {
    bodies[id].rad = RAD;
    bodies[id].R[0] = x[id]; bodies[id].R[1] = y[id]; bodies[id].R[2] = z[id];  
  }
  bodies[x.size() + 1].R[2] = L ; // top
  bodies[x.size() + 3].R[0] = L ; // left
  bodies[x.size() + 5].R[1] = L ; // front
#if WITH_NETCDF
  write_particles_netcdf(bodies, bfname);
#endif  

#if WITH_NETCDF
  std::vector<DEM::Contact> contacts_out;
  write_contacts_netcdf(contacts_out, cfname);
#endif

  // OLD printing to text file
  //std::ofstream fout(fname); if (!fout) { std::cerr << "Cannot write to :  " << fname << std::endl; exit(1); }
  //for (int id = 0; id < x.size(); ++id) {
  //fout << x[id] << "\t" << y[id] << "\t" << z[id] << "\t" << "\n";
  //}
  //fout.close();
}

