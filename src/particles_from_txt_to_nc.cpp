#include "../inc/particle.hpp"
#include "../inc/netCDF_io.hpp"
#include "../inc/utility.hpp"
#include <vector>
#include <string>

void read_particles_txt(const std::string & filename, std::vector<Particle> & particles);

int main(int argc, char **argv)
{
  // check args 
  if (3 != argc) {
    std::cerr << "ERROR.\nUsage : " << argv[0] << " input_particles_file.txt output_particles_file.nc \n";
    return 1;
  }

  std::vector<Particle> particles;

  read_particles_txt(argv[1], particles);

#if WITH_NETCDF
  write_particles_netcdf(particles, argv[2]);
#endif

  return 0;
}


void read_particles_txt(const std::string & filename, std::vector<Particle> & particles)
{
  std::FILE * fp = std::fopen(filename.c_str(), "r"); ASSERT(fp);
  long nparticles, ngrains, nwalls;
  std::fscanf(fp, "%ld%ld%ld", &nparticles, &ngrains, &nwalls); ASSERT(fp);
  print_log("nparticles = ", nparticles);
  Particle body;
  for(int id = 0; id < nparticles; ++id) {
    body.read(fp);
    particles.push_back(body);
  }
  std::fclose(fp);
}
