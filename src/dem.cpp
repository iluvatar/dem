#include "../inc/workspace_dem.hpp"
#include <iostream>
#include <exception>

//--------------------------------------------------------------------------------------------------
// Main
//--------------------------------------------------------------------------------------------------
int main ()
{
  try { 
    std::cout << "Declaring workspaceDEM" << std::endl;
    DEM::WorkspaceDEM dom;
    try { 
      std::cout << "Evolving workspaceDEM" << std::endl;
      dom.evolve();
      std::cout << "DONE: Evolving workspaceDEM." << std::endl;
    }
    catch (std::exception &e) {
      std::cerr << "Exception in dom evolve" << std::endl;
      std::cerr << "Exception: " << e.what() << std::endl;
    }
  }
  catch (std::exception &e) {
    std::cerr << "Exception in dom creation" << std::endl;
    std::cerr << "Exception: " << e.what() << std::endl;
  }
  
  return 0;
}





