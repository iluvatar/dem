#include "../inc/postpro_analysis.hpp"                                                         

int main(void)
{
  try { 
    DEM::PostproAnalysis postprocessor;
    try { 
      postprocessor.process();
    }
    catch (std::exception &e) {
      std::cerr << "Exception in postprocessor process" << std::endl;
      std::cerr << "Exception: " << e.what() << std::endl;
    }
  }
  catch (std::exception &e) {
    std::cerr << "Exception in PostproAnalisis creation" << std::endl;
    std::cerr << "Exception: " << e.what() << std::endl;
  }
  
  return 0;
}
