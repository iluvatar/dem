//--------------------------------------------------------------------------------------------------
// Last Modification: 
//--------------------------------------------------------------------------------------------------
#include <iostream>
#include "../inc/prepro.hpp"

//--------------------------------------------------------------------------------------------------
// Main
//--------------------------------------------------------------------------------------------------
int main ()
{
  try { 
    DEM::Initialization Initializer;
    try { 
      Initializer.build_and_print();
    }
    catch (std::exception &e) {
      std::cerr << "Exception in Initializer build_and_print" << std::endl;
      std::cerr << "Exception: " << e.what() << std::endl;
    }
  }
  catch (std::exception &e) {
    std::cerr << "Exception in Initializer creation" << std::endl;
    std::cerr << "Exception: " << e.what() << std::endl;
  }
  
  return 0;
}





