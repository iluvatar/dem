#include "../inc/contact.hpp"
#include "../inc/netCDF_io.hpp"
#include <vector>
#include <string>
#include <cstdlib>

void read_contacts_txt(const std::string & filename, std::vector<DEM::Contact> & contacts);

int main(int argc, char **argv)
{
  // check args 
  if (3 != argc) {
    std::cerr << "ERROR.\nUsage : " << argv[0] << " input_contacts_file.txt output_contacts_file.nc \n";
    return 1;
  }

  std::vector<DEM::Contact> contacts;

  read_contacts_txt(argv[1], contacts);
  
#if WITH_NETCDF
  write_contacts_netcdf(contacts, argv[2]);
#endif

  contacts.clear();

  return 0;
}


void read_contacts_txt(const std::string & filename, std::vector<DEM::Contact> & contacts)
{
  DEM::Contact contact;
  std::FILE * fp = std::fopen(filename.c_str(), "r");
  int count = 0;
  while (!std::feof(fp)) {
    contact.read(fp);
    if (!std::feof(fp)) contacts.push_back(contact);
    ++count;
    if (count > 1000000) {
      std::cerr << "ERROR: Reading without bound!!!!" << std::endl;
      std::cin.get();
      exit(1);
    }
  }
  std::fclose(fp);
  std::cout << "Contacts read : " << contacts.size() << std::endl;
}
