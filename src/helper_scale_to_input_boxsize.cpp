//--------------------------------------------------------------------------------------------------
// Scales bodies positions respect to other bodies and writes output
// reads nc files
/*
  Input  : start_bodiesInfo  : ref state for bodies
  Input  : end_bodiesInfo    : last state for bodies, to be scaled
  Output : scaled_bodiesInfo : scaled data
  Output : contacts_info     : empty contacts info
  
  WARNING WARNING WARNING: Not taking care of dimension or periodicity!!!!
 */

//--------------------------------------------------------------------------------------------------
#include <iostream>
#include <cstdlib>
#include <vector>
#include "../inc/netCDF_io.hpp"
#include "../inc/contact.hpp"
#include "../inc/workspace_dem_config.hpp"
#include "../inc/walls_normalvectors.hpp"

//--------------------------------------------------------------------------------------------------
// Declarations
void help(void);

//--------------------------------------------------------------------------------------------------
// Main
int main (int argc, char ** argv)
{
  if (5 != argc) { help(); return EXIT_FAILURE; }
  
  std::vector<Particle> bodies_ref, bodies;
  std::vector<DEM::Contact> contacts_out;
  double wmin_ref[3] = {0}; double wmax_ref[3] = {0};
  double wmin_in[3] = {0}; double wmax_in[3] = {0};
  double scale[3] = {1.0, 1.0, 1.0};

  // read input-ref-info and store walls limits

#if WITH_NETCDF
  read_particles_netcdf(argv[1], bodies_ref);
#endif
  const int ngrains = bodies_ref.size() - DEM::NWALLS; ASSERT(ngrains >= 0);
  wmin_ref[0] = bodies_ref[ngrains + DEM::LEFT   ].R[0]; wmax_ref[0] = bodies_ref[ngrains + DEM::RIGHT].R[0];   
  wmin_ref[1] = bodies_ref[ngrains + DEM::BACK   ].R[1]; wmax_ref[1] = bodies_ref[ngrains + DEM::FRONT].R[1]; 
  wmin_ref[2] = bodies_ref[ngrains + DEM::BOTTOM ].R[2]; wmax_ref[2] = bodies_ref[ngrains + DEM::TOP  ].R[2]; 
  

  // read input-to-scale info and store walls limits
#if WITH_NETCDF
  read_particles_netcdf(argv[2], bodies);
#endif
  wmin_in[0] = bodies[ngrains + DEM::LEFT   ].R[0]; wmax_in[0] = bodies[ngrains + DEM::RIGHT].R[0];   
  wmin_in[1] = bodies[ngrains + DEM::BACK   ].R[1]; wmax_in[1] = bodies[ngrains + DEM::FRONT].R[1]; 
  wmin_in[2] = bodies[ngrains + DEM::BOTTOM ].R[2]; wmax_in[2] = bodies[ngrains + DEM::TOP  ].R[2]; 

  // compute scale factors
  for (int dir = 0; dir < 3; ++dir) {
    scale[dir] = (wmax_ref[dir] - wmin_ref[dir])/(wmax_in[dir] - wmin_in[dir]);
    std::cout << "# scale [" << dir << "] = " << scale[dir] << std::endl;
  }

  // scale all positions
  for (auto & body : bodies) {
    for (int dir = 0; dir < 3; ++dir) {
      body.R[dir] *= scale[dir];
    }
  }

  // shift to origin
  Vec3d_t Shift(bodies[ngrains + DEM::LEFT].R[0], bodies[ngrains + DEM::BACK].R[1], bodies[ngrains + DEM::BOTTOM].R[2]);
  for ( auto & body : bodies ) { 
    body.R -= Shift;
  }

  // erase non normal components for walls
  for ( auto jd = ngrains + DEM::BOTTOM; jd <= ngrains + DEM::FRONT; ++jd) {
    Vec3d_t N = DEM::NORMAL_VEC_WALL[jd-ngrains];
    bodies[jd].R = N*(N.dot(bodies[jd].R));
  } 
  
  // Erase other vars: Forces, Torques, nc
  for ( auto & body : bodies ) { 
    body.F = body.T = Vecnull;
    body.nc = 0;
  }
  
  // print position-scaled data
#if WITH_NETCDF
  write_particles_netcdf(bodies, argv[3]);
  write_contacts_netcdf(contacts_out, argv[4]);
#endif

  // end
  return EXIT_SUCCESS;
}

//--------------------------------------------------------------------------------------------------
// Definitions
void help(void)
{
  std::clog << "\nUsage : \nexename bodiesInfo_ref.nc bodiesInfo_toscale.nc "
	    << "bodiesInfo_output.nc contactsInfo_output.nc" << std::endl;
  std::clog << "exename                 : this executable's name " << std::endl;
  std::clog << "bodiesInfo_ref.nc       : Reference file to get reference walls positions " << std::endl;
  std::clog << "bodiesInfo_toscale.nc   : Bodies file to be scaled according to reference file" << std::endl;
  std::clog << "bodiesInfo_output.nc    : Where to write the position-scaled data" << std::endl;
  std::clog << "contactsInfo_output.nc  : Where to write the assumed empty contacts data" << std::endl;
  std::clog << std::endl;
  std::clog << "WARNING WARNING WARNING : Not taking care of dimensions or periodicity" << std::endl;
}
