* DONE Improve cube densification setup to include number of tries 
  CLOSED: [2018-10-02 Tue 11:41]
* TODO Add config to prepro for depth size for surface detection
* TODO Update setup_simul example with latest prepro options
* TODO Upgrade yaml to 0.6.X to avoid boost dep
* TODO Improve makefile
  Check this example to track deps
  #+BEGIN_SRC makefile
CC = clang
CFLAGS += -Wall -Wextra
libs = -lm
objs = hellomake.o
hellomake:
	$(objs) $(CC) $(CFLAGS) -o $@ $(objs) $(libs)
.PHONY: clean
clean:
	rm -f hellomake *.o *.d
deps := $(objs:.o=.d)
%.d: %.c
	$(CC) $(CFLAGS) -MM -MF $@ $< -include $(deps) 
  #+END_SRC
* TODO Add a default option to print to text when no netcdf is activated on particles to nc and related files
* TODO Configuration file to select what postpro routines to run
* TODO Add a struct to postpro to save configuration and to share it across postpro functions
* DONE Add optio to print and read file while densifying in prepro
  CLOSED: [2018-09-18 Tue 09:09]
  The idea is to be able to print partial data when densifying or
  using any other pre-processing technique that takes a lot of time
  (like now when I am pre-processing systems wuth 300000
  particles). The file could have the following structure:
  #+BEGIN_EXAMPLE
  N_PARTICLES_TOTAL
  N_PARTICLES_PRINTED # must be less or equal to N_PARTICLES_TOTAL
  x1 y1 z1
  x2 y2 z2
  x3 y3 z3
  ...
  xn yn zn
  #+END_EXAMPLE
  where $n \le N$, that is, partial info is allowed to be printed for
  the particles already processed. By using this it is possible to
  restart a prepro simulation.
  What do I need?
  - In =prepro.conf= add a new parameter which is the filename to be
    read, and another to be printed. Can be called
    =prepro_pos_input.txt= and =prepro_pos_output.txt=. Default values
    can be null. Maybe always print it .
  - While densifying, always print after adding something like 10
    particles. 
  - In densification, if =prepro_pos_input.txt= was read, create the
    surface and densify only for the remaining particles. If no
    densification but grid used, then ignore the read file.
* DONE Optimize densification with openmp
  CLOSED: [2018-09-18 Tue 20:54]
  When densifying large samples it would be useful to use openmp to
  parallelize, for instance, the number of trials. There shpuld a be a
  reduce operation of the height to get the minimum. Can this work?
  let's try will all the code tht is already there...
* DONE Improve materials_conf.yaml syntax to allow for extensions, like including cohesion. Better use keys. 
* DONE Print paraview planes data as multiblock to allow to extend a little the data on each axis to easy visualization 
* DONE Remove temporal material input file for testing dependence on scale  = kt/kn
* DONE Postpro: correlation analysis for number of contacts
  - Define a set with the current set of contacts: identification by
    unique ID
  - For each next time step:
    - create a new set of current contacts
    - compute the intersection
    - print the new size
    - swap the contents to make the reference set equal to the new intersected set 
* DONE In postpro, only take into account the voronoi voumes of selected 
  particles, replacing the vector by a set.
* DONE Review the driven degrees of freedom stuff
* DONE Review and rethink the whole materials stuff
* DONE Re add freeze function
  CLOSED: [2011-12-29 Thu 13:49]
* DONE Create a makefile
  CLOSED: [2011-12-29 Thu 13:49]
* DONE Multi-scale analysis of voronoi volumes
  CLOSED: [2011-09-14 Wed 15:18]
  - Find neighbors for each cell : Use voro++
    Structure: vector<set<int> > connectivity: vector[id] is a set of jd neighbors
    of particle id
    + For each cell:
      * get particle id
      * get neighbors
      * store neighbors into connectivity
  - Select the connectivity in clusters of size sizecluster
    cluster is a structure : vector<set<int>>, set should be of size
    clustersize
    + make sure clustersize is smaller or eaual to the minimum size
      of neighbors set
    + For each particle
      * if particle neighbors is non null, for each selection until clustersize
        ** select a neighor at random
	** from each of the neighbors of that neighbor, delete that
	neighbor as neighbor
	** delete the neighbor of the selected neighbor
	** connect the two particles in the cluster structure
	** delete the selected neighbor from the neighbor of the particle
  - Find the volume distribution: cluster has now the clusters, but
    the total number of particles could be smaller than the total
    number of grains, since clustersize could not be a divisor of
    ngrains
    * for each cluster of size clustersize:
      ** compute the total volume
      ** store the total volume in histogram
  - Maybe, normalize the distribution: Do this only when printing? NO,
    DO NOT DO IT :)
* DONE Review tagger, do not reset completely the force as is done now
 CLOSED: [2011-09-13 Tue 16:08]
* DONE remove using namespace std; 
 CLOSED: [2011-07-22 Fri 17:39]
* DONE Generalize periodic to 3D : Not necessary yet ...
 CLOSED: [2011-07-22 Fri 16:42]
* DONE Generalize gravity to be a vector (write the three components on file?)
 CLOSED: [2011-07-22 Fri 15:35]
* DONE Add number of particles specification to prepro_conf
  CLOSED: [2011-05-30 Mon 14:04]
* DONE Allow to stop by energy
 CLOSED: [2011-05-28 Sat 12:56]
* DONE Postpro: Paraview plot of Voronoi tesselation
  CLOSED: [2011-05-09 Mon 16:24]
* DONE Modify run scripts to use python: this will easy some processing, like modifying the nth line only
  CLOSED: [2011-05-09 Mon 16:24]
* DONE Postpro: Print povray voronoi tesselation for each time, a lot of frames
  CLOSED: [2011-05-09 Mon 16:24]
* DONE Modify postpro to not use domain, only reading files
  CLOSED: [2011-05-09 Mon 16:24]
* DONE paraview printing of contact info
  CLOSED: [2011-05-09 Mon 16:24]
* DONE Die if not reading config files
  CLOSED: [2011-05-09 Mon 16:24]
* DONE To change reading with dictionary to reading strictly formatted
  CLOSED: [2011-05-09 Mon 16:24]
* DONE Histogram parameters in postpro
  CLOSED: [2011-05-09 Mon 16:24]
* DONE Paraview format : move from legacy VTK to xml VTK with embedded time info.
  CLOSED: [2011-05-09 Mon 16:24]
* DONE review particle.nc computation 
  CLOSED: [2011-05-09 Mon 16:24]
* DONE To improve run scripts
  CLOSED: [2011-05-09 Mon 16:24]
* DONE review paraview dr, nc, etc.
  CLOSED: [2011-05-09 Mon 16:24]
* DONE To correct the postpro functions (make them work ;) )
  CLOSED: [2011-05-09 Mon 16:24]
* DONE Profile
  CLOSED: [2011-05-09 Mon 16:24]
* DONE Redesign top-bottom grain walls, delete bctag-group.
  CLOSED: [2011-05-09 Mon 16:24]
* DONE Add cohesion
  CLOSED: [2011-05-09 Mon 16:24]
* DONE To add fixed stress
  CLOSED: [2011-05-09 Mon 16:24]
* DONE To fix current strange bouncings with polydisperse systems
  CLOSED: [2011-05-09 Mon 16:24]
* DONE Paraview printing (postpro)
  CLOSED: [2011-05-09 Mon 16:24]
* DONE move functions like reset, freeze, or create_top_bottom_walls ... to a workspace helper
  CLOSED: [2011-05-09 Mon 16:24]
* DONE Avoid using boost::serialization, print txt files
  CLOSED: [2011-05-09 Mon 16:24]
* DONE To put all postpro (except gnuplot) inside a class or something to read only once the data
  CLOSED: [2011-05-09 Mon 16:24]
* DONE Improve cell list by keeping a list of jd for each cell, updating it as needed when a particle moves from its old cell
  CLOSED: [2011-05-09 Mon 16:24]
* DONE Improve cell list by recomputing domains when a particle goes outside the simulation area. Limits? maybe 5 duplications = 2^5 = 32 times the original domain extent
  CLOSED: [2011-05-09 Mon 16:24]
* DONE clean config files stuff: materials on one file, exec parameters in general config, etc
  CLOSED: [2011-05-09 Mon 16:24]
* DONE Delete Lx and Lz stuff from config and store/access that info by the walls
  CLOSED: [2011-05-09 Mon 16:24]
* DONE To optimize tagging with functor -> delete tags function from particle
  CLOSED: [2011-05-09 Mon 16:24]
* DONE To implement flow capability: File to control runtime execution and good finalization
  CLOSED: [2011-05-09 Mon 16:24]
* DONE To delete fixed stress in favor of only fixed force
  CLOSED: [2011-05-09 Mon 16:24]
* DONE To change input file names to something_conf
  CLOSED: [2011-05-09 Mon 16:24]
* DONE To calibrate tangential parameters: Less rolling K
  CLOSED: [2011-05-09 Mon 16:24]
* DONE Check if mur = 0 implies no rolling interactions, it should -> YES
  CLOSED: [2011-05-09 Mon 16:24]
* DONE What to expect when mur != 0? -> OK!
  CLOSED: [2011-05-09 Mon 16:24]

