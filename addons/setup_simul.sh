#!/bin/bash

echo "Creating dirs ..."
mkdir ./INPUT ./OUTPUT ./POSTPRO ./DISPLAY 
echo "Dirs created"

# finding the path to the real repo with the simul directory inside
SCRIPT_PATH="${BASH_SOURCE[0]}";
if ([ -h "${SCRIPT_PATH}" ]) then
  while([ -h "${SCRIPT_PATH}" ]) do SCRIPT_PATH=`readlink "${SCRIPT_PATH}"`; done
fi
pushd . > /dev/null
cd `dirname ${SCRIPT_PATH}` > /dev/null
SCRIPT_PATH=`pwd`;
REPO_DIR=`pwd`;
popd  > /dev/null
REPO_DIR=${REPO_DIR}/../
echo "Paths for original repository setted: "
echo "REPO_DIR = $REPO_DIR"
SIMUL_DIR=${REPO_DIR}/simul
echo "SIMUL_DIR = $SIMUL_DIR"

# copy model input files
cp $SIMUL_DIR/INPUT/* INPUT/
echo "Copied model input files"

# change names to be ready for use
for a in INPUT/*-example; do 
    name=${a%-example}
    cp -f $a ${name}; 
done
echo "New input files ready"

echo "Please set up the INPUT/* files to what is needed"
echo "Done."
