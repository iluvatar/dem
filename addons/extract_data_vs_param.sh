#!/bin/bash

# WARNING : Could generate false negatives when working with symlinked directories.
# Example: mus_000 is actually a symlink to another directory. Then this script could
# say that some files are not present at that directory even if they are. Coudl also be
# related to slow response from data disk.
# If this happens, just run several times the script until all output data is generated.

# check args
if [[ "$#" -ne "1" ]]; then
    printf "ERROR.\nUSAGE:\n$0 AVERAGED_FLAG\n"
    printf "AVERAGED_FLAG : 0/1. Indicates if data is averaged, in which PARAM-AVERAGED subdir should be used to find the data\n"
    exit 1
fi

# extract parameter name, as the last directory pattern
PAR_NAME=$(echo $(basename $(find ./ -type d -maxdepth 1 | tail -n 1)) | awk 'BEGIN { FS="_" } ; { print $1 }')
#set subdir: defaults to actual dir
AVERAGED="$1"
SUB_DIR=""
if [[ "1" == "$AVERAGED" ]]; then
    SUB_DIR="PARAM-AVERAGED"
fi

# data file names
declare -a FILENAME=('P_X_mean_sigma.dat' 'P_nc-mean_sigma.dat' 'meanPenetration.dat' 'packing_fraction.dat' 'sigma.dat' 'voronoi_K.dat' \
    'voronoi_S.dat' 'voronoi_X.dat' 'z.dat' 'voronoi_C.dat' 'voronoi_CuN.dat' \
    'voronoi_vmin_elemental.dat' 'delaunay_K.dat' 'delaunay_S.dat' 'delaunay_X.dat' \
    'voronoi_SuK.dat' 'voronoi_Xuvminelemental.dat' 'delaunay_SuK.dat' 'delaunay_Xuvminelemental.dat' \
    'delaunay_C.dat' 'delaunay_CuN.dat' 'delaunay_vmin_elemental.dat' \
    'voronoi_Vmean.dat'  'voronoi_Vsigma.dat'  'voronoi_Vsigma2.dat' \
    'delaunay_Vmean.dat' 'delaunay_Vsigma.dat' 'delaunay_Vsigma2.dat' 'voronoi_VT.dat' 'delaunay_VT.dat' 'voronoi_ST.dat' \
    'centroidal_voronoi-voronoi_X.dat' 'centroidal_voronoi-voronoi_S.dat' 'centroidal_voronoi-voronoi_SuK.dat' 'centroidal_voronoi-voronoi_CuN.dat' \
    'P_packingfraction-mean_sigma.dat'  'P_qoverp-mean_sigma.dat');
# columns to select
declare -a C_YES_AVERAGED=('$1,$3'  '$1,$3'  '$2,$6'  '$2,$5'  '$2,$3,$4,$16,$17,$18'  '$1,$2' \
    '$1,$2'  '$1,$3'  '$2,$5'  '$1,$2'  '$1,$2' \
    '$1,$4'  '$1,$2'  '$1,$2'  '$1,$3' \
    '$1,$2'  '$1,$2'  '$1,$2'  '$1,$2' \
    '$1,$2'  '$1,$2'  '$1,$4'  \
    '$1,$3'  '$1,$3'  '$1,$3'  \
    '$1,$3'  '$1,$3'  '$1,$3'  '$1,$2'  '$1,$3' '$1,$2' \
    '$1,$2'  '$1,$2'  '$1,$2'  '$1,$2' \
    '$1,$3'  '$1,$3' )
declare -a C_NON_AVERAGED=('$1,$2'  '$1,$2'  '$2'     '$2'     '$2,$3,$4'              '$1'    \
    '$1'     '$1'     '$2'     '$1'     '$1'      \
    '$1'     '$1'     '$1'     '$1' \
    '$1'     '$1'     '$1'     '$1' \
    '$1'     '$1'     '$1'  \
    '$1'     '$1'     '$1'  \
    '$1'     '$1'     '$1'     '$1'   '$1'    '$1' \
    '$1'     '$1'     '$1'     '$1' \
    '$1,$2'  '$1,$2' ) 


# extract data
#for DATA_NAME in ${FILENAME[@]}; do
for ((i=0; i<${#FILENAME[@]}; i++)); do
    # set output data file name
    DATA_NAME=${FILENAME[$i]}
    echo "Processing : $DATA_NAME"
    OUTPUT=$PAR_NAME-${DATA_NAME%.dat}.dat
    rm -f $OUTPUT 2>/dev/null
    for a in ${PAR_NAME}_*; do
	if [ ! -d $a ]; then
	    continue
	fi
	filename="$a/$SUB_DIR/POSTPRO/$DATA_NAME"
	if [ ! -e "$filename" ]; then
	    echo "ERROR: File $filename does not exists. Ignoring."
	    echo
	    break
	fi
	if [[ -d "$a" ]]; then 
	    val=${a#${PAR_NAME}_}; 
	    val=${val%_}; 
	    #data=$(tail -n1 $filename)
	    data=""
	    if [[ "0" == "$AVERAGED" ]]; then 
		data=$(tail -n1 "$filename" | awk '{print '${C_NON_AVERAGED[$i]}'}')
	    else 
		data=$(tail -n1 "$filename" | awk '{print '${C_YES_AVERAGED[$i]}'}')
	    fi
	    echo "$val    $data " >> $OUTPUT
	fi
    done
    echo "Done. Printed to $OUTPUT"
done

echo "DONE extracting data versus param."
