#!/bin/env pvbatch --use-offscreen-rendering
import glob, string, os, commands, sys
from paraview.simple import *

# check arguments
if 2 != len(sys.argv) :
    print "ERROR. Usage:\n" + sys.argv[0] + " filename.vtp"
    sys.exit(1)
if not os.path.exists(sys.argv[1]) :
    print "ERROR. Filename " + sys.argv[1] + " does not exists."
    sys.exit(1)

# process filename
Connect()
filename=sys.argv[1]
fullfn = commands.getoutput("ls $PWD/" + filename)
fn = filename.replace('DISPLAY/', '')
os.system("cp " + str(os.path.dirname(os.path.realpath(sys.argv[0]))) + "/../addons/paraview_state.pvsm tmp.pvsm")
os.system("sed -i.bck 's/DATA.vtp/" + fullfn.replace('/','\/') + "/1' tmp.pvsm") # replace first intance with full path
os.system("sed -i.bck 's/DATA.vtp/" + fullfn.replace('/','\/') + "/1' tmp.pvsm") # replace second intance with full path
os.system("sed -i.bck 's/DATA.vtp/" + fn + "/1' tmp.pvsm") # replace third with just the filename path
servermanager.LoadState("tmp.pvsm")
#pm = servermanager.ProxyManager()
#reader = pm.GetProxy('sources', fullfn)
#reader.FileName = fullfn
#reader.FileNameChanged()
#reader.UpdatePipeline()

# This uses the same view as the initial state
#view = servermanager.GetRenderView()
#view.UseOffscreenRenderingForScreenshots = 0
#SetActiveView(view)
#view.StillRender()

# this scales to actual size of the image
SetActiveView(servermanager.GetRenderView())
GetActiveView().UseOffscreenRenderingForScreenshots = 1 
Render()

WriteImage(filename.replace(".vtp", ".png"))
os.system("rm -f tmp.pvsm")
os.system("rm -f tmp.pvsm.bck")
#Delete(reader)
#Delete(view)
