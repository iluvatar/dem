# general vars
# update vars to appropiate value
#CXX=g++-4.9
#CC=gcc-4.9
CXX=${CXX:-g++-8}
CC=${CC:-gcc-8}
echo "###########################################################################################################"
echo "WARNING :: DO NOT FORGET TO COMPILE DEPS WITH THE SAME COMPILER YOU ARE GOING TO BUILD THE DEM EXECUTABLES"
echo "CXX=$CXX"
echo "CC=$CC"
echo "###########################################################################################################"
sleep 5

#export PREFIX=/usr/local
export PREFIX=${PREFIX:-$HOME/local}
NPREFIX=${PREFIX//\//\\\/}
if [ "$PREFIX" == "/usr/local" ]; then
    if [ 0 -ne $EUID ]; then
	echo "To use $PREFIX as prefix, you should run this script as root. Exiting."
	exit 1
    fi
fi
echo "###########################################################################################################"
echo "PREFIX=$PREFIX"
echo "###########################################################################################################"


WGET="wget -c --tries=5 "
JOBS=${JOBS:-2}
export CPPFLAGS="-I$PREFIX/include" 
export LDFLAGS="-L$PREFIX/lib" 
export CXXFLAGS="${CPPFLAGS}"
export CFLAGS="${CPPFLAGS}"
export LDFLAGS="-L${PREFIX}/lib"
export LIBS="-ldl"
#export CONFIGURE_FLAGS="CXX=$CXX CC=$CC CPPFLAGS=$CPPFLAGS CXXFLAGS=$CXXFLAGS LDFLAGS=$LDFLAGS "
#CMAKE_FLAGS="CXX=$CXX CC=$CC CPPFLAGS=$CPPFLAGS CXXFLAGS=$CXXFLAGS LDFLAGS=$LDFLAGS "
#CMAKE_FLAGS="CXX=$CXX CC=$CC -DCMAKE_CPP_FLAGS=$CPPFLAGS -DCMAKE_CXX_FLAGS=$CXXFLAGS -DCMAKE_LD_FLAGS=$LDFLAGS "

# helper functions
function compile_with_make 
{
    echo "Compiling with $JOBS jobs ..."
    make -j $JOBS 1> log-make-$1.txt
    make install  1> log-install-$1.txt
}
function compile_with_cmake 
{
    rm -rf build &> /dev/null
    mkdir build 
    cd build
    cmake ../ -DCMAKE_INSTALL_PREFIX:PATH=$PREFIX -DCMAKE_CXX_COMPILER=$CXX  -DCMAKE_CC_COMPILER=$CC $2 1> log-cmake-$1.txt
    compile_with_make $1
    cd ..
}
function start_msg
{
    echo
    echo "################################################"
    echo "START : $1"
}

function done_msg
{
    echo "DONE  : $1"
    echo "################################################"
    echo
}

 # Install functions

# eigen
function install_eigen 
{
    start_msg "eigen"
    if [ ! -d ${PREFIX}/include/eigen3 ]; then  
	PVER=3.3.4
	$WGET https://bitbucket.org/eigen/eigen/get/$PVER.tar.gz -O $PVER.tar.gz 1> log-wget-eigen.txt
	if [ "1a47e78efe365a97de0c022d127607c3" != "$(md5sum $PVER.tar.gz | awk '{print $1}')" ]; then echo "Bad download. Exiting."; return ; fi
	tar xf $PVER.tar.gz 1>log-tar-eigen.txt
	cd eigen-eigen-5a0156e40feb
	compile_with_cmake eigen
	cd ../
    fi
    done_msg "eigen"
}

# voro++
function install_voro
{
    start_msg "voro++"
    if [ ! -d ${PREFIX}/include/voro++ ]; then  
	PNAME=voro++
	PVER=0.4.6
	PACK=${PNAME}-${PVER}
	$WGET http://math.lbl.gov/voro++/download/dir/${PACK}.tar.gz 1> log-wget-${PNAME}.txt
	if [ "2338b824c3b7b25590e18e8df5d68af9" != "$(md5sum ${PACK}.tar.gz | awk '{print $1}')" ]; then echo "Bad download. Exiting."; return; fi
	tar xf ${PACK}.tar.gz 1>log-tar-${PACK}.txt
	cd ${PACK}
	sed -i.bck 's/PREFIX.*/PREFIX='$NPREFIX'/' config.mk
	sed -i.bck 's/CXX.*/CXX='$CXX'/' config.mk
	compile_with_make $PNAME
	cd ..
    fi
    done_msg "voro++"
}

# yaml-cpp
function install_yaml
{
    start_msg "yaml-cpp ... !!!!!"
    if [ ! -d ${PREFIX}/include/yaml-cpp ]; then 
	PNAME=yaml-cpp
	PVER=0.6.2
	PACK=${PNAME}-${PVER}
	$WGET https://github.com/jbeder/yaml-cpp/archive/${PACK}.tar.gz -O ${PACK}.tar.gz 1> log-wget-${PNAME}.txt
	if [ "5b943e9af0060d0811148b037449ef82" != "$(md5sum ${PACK}.tar.gz | awk '{print $1}')" ]; then echo "Bad download. Exiting."; return; fi
	tar xf ${PACK}.tar.gz 1>log-tar-${PACK}.txt
	cd ${PNAME}-${PACK}
	compile_with_cmake $PNAME
	cd ..
    fi
    done_msg "yaml-cpp"
}

# hdf5
function install_hdf5
{
    start_msg "hdf5 ... REQUIRES zlib !!!! (zlib1g-dev on ubuntu)"
    if [ ! -e ${PREFIX}/lib/libhdf5_cpp.a ]; then 
	PNAME=hdf5
	PVER_MAJ=1.10
	PVER_MIN=5
	PVER=${PVER_MAJ}.${PVER_MIN}
	PACK=${PNAME}-${PVER}
	$WGET https://support.hdfgroup.org/ftp/HDF5/releases/hdf5-${PVER_MAJ}/hdf5-${PVER}/src/${PACK}.tar.bz2 1> log-wget-${PNAME}.txt
	if [ "7c19d6b81ee2a3ba7d36f6922b2f90d3" != "$(md5sum ${PACK}.tar.bz2 | awk '{print $1}')" ]; then echo "Bad download. Exiting."; return; fi
	tar xf ${PACK}.tar.bz2 1>log-tar-${PACK}.txt
	cd ${PACK}
	CC=$CC CXX=$CXX CPPFLAGS="$CPPFLAGS" LDFLAGS="$LDFLAGS"  ./configure --prefix=$HOME/local  --enable-hl --enable-cxx 
	compile_with_make $PNAME
	#compile_with_cmake $PNAME "-DHDF5_BUILD_HL_LIB=ON -DENABLE_CXX=ON -DENABLE_PARALLEL=ON -DHDF5_ENABLE_Z_LIB_SUPPORT=ON -DHDF5_BUILD_CPP_LIB=ON" 
	#ln -sf $PREFIX/lib/libhdf5-shared.so.$PVER $PREFIX/lib/libhdf5.so
	#ln -sf $PREFIX/lib/libhdf5_hl-shared.so.$PVER $PREFIX/lib/libhdf5_hl.so
	cd ../
    fi
    done_msg "hdf5"
}

# netcdf + C++ bindings
# netcdf 
function install_netcdf_c
{
    start_msg "netcdf-c ... REQUIRES m4"
    if [ ! -f ${PREFIX}/include/netcdf.h ]; then 
	PNAME=netcdf-c
	PVER=4.4.1.1
	PACK=${PNAME}-${PVER}
	$WGET https://github.com/Unidata/netcdf-c/archive/v${PVER}.tar.gz -O ${PACK}.tar.gz 1> log-wget-${PNAME}.txt
	if [ "9210fd5355bee868684d9b8f83064aa6" != $(md5sum ${PACK}.tar.gz | awk '{print $1}') ]; then echo "Bad download. Exiting."; return; fi
	tar xf ${PACK}.tar.gz 1>log-tar-${PACK}.txt
	cd ${PACK}
	CC=$CC CXX=$CXX LDFLAGS="$LDFLAGS" CPPFLAGS="$CPPFLAGS" ./configure --prefix=$PREFIX --enable-netcdf-4  1> log-${PNAME}-configure.txt
	compile_with_make $PNAME 
	cd ..
    fi
    done_msg "netcdf-c"
}

# netcdf c++
function install_netcdf_cxx 
{
    start_msg "netcdf-c++ ..."
    if [ ! -f ${PREFIX}/include/netcdf ]; then 
	PNAME=netcdf-cxx4
	PVER=4.3.0
	PACK=${PNAME}-${PVER}
	$WGET https://github.com/Unidata/netcdf-cxx4/archive/v${PVER}.tar.gz -O ${PACK}.tar.gz  1> log-wget-${PNAME}.txt
	if [ "f840a75d3c68390fb7392be5c3f663cd" != "$(md5sum ${PACK}.tar.gz | awk '{print $1}')" ]; then echo "Bad download. Exiting."; return; fi
	tar xf ${PACK}.tar.gz 1>log-tar-${PACK}.txt
	cd ${PACK}
	CC=$CC CXX=$CXX LDFLAGS="$LDFLAGS" CPPFLAGS="$CPPFLAGS" ./configure --prefix=$PREFIX --program-suffix="" &> log-netcdf-cxx-configure.txt
	compile_with_make $PNAME
	ln -sf ${PREFIX}/lib/libnetcdf_c++{4,}.dylib
	ln -sf ${PREFIX}/lib/libnetcdf_c++{4,}.so
	ln -sf ${PREFIX}/lib/libnetcdf_c++{4,}.a
	cd ..
    fi
    done_msg "netcdf-c++"
}

# tetgen : http://wias-berlin.de/software/tetgen/
function install_tetgen
{
    start_msg "tetgen ..."
    echo "Please subscribe yourself as user at http://wias-berlin.de/software/tetgen/"
    if [ ! -f ${PREFIX}/include/tetgen.h ]; then 
	PNAME=tetgen1.5.1
	PVER=beta1
	PACK=${PNAME}-${PVER}
	$WGET http://wias-berlin.de/software/tetgen/1.5/src/${PACK}.tar.gz  1> log-wget-${PNAME}.txt
	if [ "3d55c197bcbfc611b7ced6f343643756" != "$(md5sum ${PACK}.tar.gz | awk '{print $1}')" ]; then echo "Bad download. Exiting."; return; fi
	gunzip ${PACK}.tar.gz 1>log-gunzip-${PACK}.txt
	tar xf ${PACK}.tar 1>log-tar-${PACK}.txt
	cd ${PACK}
	compile_with_cmake $PNAME
	mkdir -p $PREFIX/lib
	cp build/libtet.a $PREFIX/lib/
	mkdir -p $PREFIX/include
	cp tetgen.* $PREFIX/include/
	cd ..
    fi
    done_msg "tetgen"
}

# # VTK5
# function install_vtk5 
# {
#     #echo "OPTIONAL : Please install vtk5. In MacOsX, use : brew install vtk5 --without-python "
#     echo "VTK5 : Make sure you zlib installed"
#     echo "VTK5 : And do not forget to link include and lib dirs. For example: ln -s ${PREFIX}/lib/vtk-5.10 ${PREFIX}/lib/vtk"
#     echo "VTK5 : In Mac Os X, YOU MUST USE ccmake TO CONFIGURE VTK5 TO USE_SYSTEM_TIFF ON (in Advanced settings) "
#     # HELP : http://vtk.1045678.n5.nabble.com/Compilation-error-with-Clang-OSX-10-9-td5724091.html
#     if [ ! -d ${PREFIX}/lib/vtk-5.10 ]; then 
# 	PNAME=vtk
# 	PVER=5.10.1
# 	PACK=${PNAME}-${PVER}
# 	$WGET http://www.vtk.org/files/release/5.10/${PACK}.tar.gz   1> log-wget-${PNAME}.txt
# 	if [ "264b0052e65bd6571a84727113508789" != "$(md5sum ${PACK}.tar.gz | awk '{print $1}')" ]; then echo "Bad download. Exiting."; return; fi
# 	tar xf ${PACK}.tar.gz 1>log-tar-${PACK}.txt
# 	cd VTK5.10.1 #${PACK}
# 	compile_with_cmake $PNAME "-DUSE_SYSTEM_TIFF=ON"
# 	cd ../
# 	ln -sf ${PREFIX}/lib/vtk-5.10 ${PREFIX}/lib/vtk
# 	ln -sf ${PREFIX}/include/vtk-5.10 ${PREFIX}/include/vtk
#     fi
# }

# # VTK6
# function install_vtk6 
# {
#     start_msg "VTK6 ..."
#     # REF : http://public.kitware.com/pipermail/vtkusers/2014-March/083368.html
#     if [ ! -d ${PREFIX}/include/vtk-6.3 ] && [ ! -d /usr/include/vtk-6.3 ]; then 
# 	echo "VTK6 : Make sure you zlib installed"
# 	#echo "VTK6 : And do not forget to link include and lib dirs. For example: ln -s ${PREFIX}/lib/vtk-5.10 ${PREFIX}/lib/vtk"
# 	echo "VTK6 : In Mavericks, YOU MUST USE ccmake TO CONFIGURE VTK6 TO remove the content -fobjc-gc  from VTK_REQUIRED_OBJCXX_FLAGS  (in Advanced settings) "
# 	PNAME=VTK
# 	PVER=6.3.0
# 	PACK=${PNAME}-${PVER}
# 	$WGET http://www.vtk.org/files/release/6.3/${PACK}.tar.gz   1> log-wget-${PNAME}.txt
# 	if [ "0231ca4840408e9dd60af48b314c5b6d" != "$(md5sum ${PACK}.tar.gz | awk '{print $1}')" ]; then echo "Bad download. Exiting."; return; fi
# 	tar xf ${PACK}.tar.gz 1>log-tar-${PACK}.txt
# 	cd ${PACK}
# 	compile_with_cmake $PNAME "-DVTK_REQUIRED_OBJCXX_FLAGS= -DBUILD_EXAMPLES=OFF -DBUILD_TESTING=OFF"
# 	cd ../
# 	ln -sf ${PREFIX}/include/vtk-6.3 ${PREFIX}/include/vtk
#     fi
#     done_msg "VTK6 ..."
# }

# # VTK7
# function install_vtk7 
# {
#     start_msg "VTK7 ... : This is not working on mac os x . Please use homebrew."
#     exit 1
#     # REF : http://public.kitware.com/pipermail/vtkusers/2014-March/083368.html
#     if [ ! -d ${PREFIX}/include/vtk-7.0 ] && [ ! -d /usr/include/vtk-7.0 ]; then 
# 	#echo "VTK7 : Make sure you zlib installed"
# 	#echo "VTK7 : In Mavericks, YOU MUST USE ccmake TO CONFIGURE VTK7 TO remove the content -fobjc-gc  from VTK_REQUIRED_OBJCXX_FLAGS  (in Advanced settings) "
# 	PNAME=VTK
# 	PVER=7.0.0
# 	PACK=${PNAME}-${PVER}
# 	$WGET http://www.vtk.org/files/release/7.0/${PACK}.tar.gz   1> log-wget-${PNAME}.txt
# 	if [ "5fe35312db5fb2341139b8e4955c367d" != "$(md5sum ${PACK}.tar.gz | awk '{print $1}')" ]; then echo "Bad download. Exiting."; return; fi
# 	tar xf ${PACK}.tar.gz 1>log-tar-${PACK}.txt
# 	cd ${PACK}
# 	compile_with_cmake $PNAME "-DVTK_REQUIRED_OBJCXX_FLAGS= -DBUILD_EXAMPLES=OFF -DBUILD_TESTING=OFF"
# 	cd ../
# 	ln -sf ${PREFIX}/include/vtk-7.0 ${PREFIX}/include/vtk
#     fi
#     done_msg "VTK7 ..."
# }

# VTK8
function install_vtk8
{
    start_msg "VTK8 ... ."
    if [ ! -d ${PREFIX}/include/vtk-8.0 ] && [ ! -d /usr/include/vtk-8.0 ]; then 
	PNAME=VTK
	PVERMAJOR=8.1
	PVERMINOR=1
	PVER=${PVERMAJOR}.${PVERMINOR}
	PACK=${PNAME}-${PVER}
	$WGET http://www.vtk.org/files/release/$PVERMAJOR/${PACK}.tar.gz   1> log-wget-${PNAME}.txt
	if [ "cf078a71c298c76b13707c7c27704248" != "$(md5sum ${PACK}.tar.gz | awk '{print $1}')" ]; then
	    echo "Bad download. Exiting.";								     return;
	fi
	tar xf ${PACK}.tar.gz 1>log-tar-${PACK}.txt
	cd ${PACK}
	compile_with_cmake $PNAME "-DVTK_REQUIRED_OBJCXX_FLAGS= -DBUILD_EXAMPLES=OFF -DBUILD_TESTING=OFF"
	cd ../
	#ln -sf ${PREFIX}/include/vtk-PVERMAJOR ${PREFIX}/include/vtk
    fi
    done_msg "VTK8"
}

# create aux dir
TDIR=dem_dependencies
echo "Storing all packages downloads and related files into directory : $TDIR"
mkdir $TDIR 2> /dev/null
cd $TDIR

# install packages
# eigen3
#sudo apt-get install libeigen3-dev
# yaml-cpp
#sudo apt-get install libyaml-cpp0.5 libyaml-cpp-dev
#hdf5
#sudo apt-get install libhdf5-dev
# netcdf
#sudo apt-get install libnetcdfc++4 libnetcdf-dev netcdf-bin
# tetgen
#sudo apt-get install libtet1.5 libtet1.5-dev tetgen

# Compile from source: call install function
echo "What package do you want to install onto $PREFIX? write the integer"
echo "(1) eigen   (2) voro++      (3) yaml   (4) tetgen   (5)  hdf5  "
echo "(6) netcdf  (7) netcdfc++   (8) vtk8 "
echo "(0) ALL (but vtk)"
read opt
case $opt in
    0)
	install_eigen
	install_voro
	install_yaml
	install_tetgen
	install_hdf5
	install_netcdf_c
	install_netcdf_cxx;;
    1) 
	install_eigen;;
    2)
	install_voro;;
    3) 
	install_yaml;;
    4) 
	install_tetgen;;
    5)
	install_hdf5;;
    6)
	install_netcdf_c;;
    7)
	install_netcdf_cxx;;
    8)
	install_vtk8;;
    *)
	echo "Option not recognized -> $opt"
esac

# exit from dem_dependencies
cd ../ 
