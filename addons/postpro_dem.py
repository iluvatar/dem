# Run on the terminal : conda install netcdf4
import netCDF4 as nc
import numpy as np
import sys

######################################################################
# read configuration files
# run conda install pyyaml
import yaml
# postpro file
postpro_file = open("INPUT/postpro_conf.yaml")
postpro_conf = yaml.load(postpro_file, Loader = yaml.FullLoader)
print("INPUT/postpro_conf.yaml:")
print(postpro_conf)
TMIN = postpro_conf["tmin"]
TMAX = postpro_conf["tmax"]
SCREEN_FACTOR = postpro_conf["SCREEN_FACTOR"]
NC_MIN = postpro_conf["NC_MIN"]
STRIDE = 1
try:
    STRIDE = postpro_conf["stride"]
except:
    None
    # materials file
materials_file = open("INPUT/materials_conf.yaml")
materials_conf = yaml.load(materials_file, Loader = yaml.FullLoader)
print("INPUT/materials_conf.yaml:")
print(materials_conf)
try:
    MUS = np.float(materials_conf[0][3]) # update this from materials_conf.yaml
    KT = np.float(materials_conf[0][2]) # update this from materials_conf.yaml
except:
    MUS = np.float(materials_conf[0]['MUSF']) # update this from materials_conf.yaml
    KT = np.float(materials_conf[0]['K']) # update this from materials_conf.yaml


######################################################################
# Read the files : for now only a single body and contact file
# test bodies and contacts file
#bname="OUTPUT/bodiesInfo-00000.0843114999981749.nc"
#cname="OUTPUT/contactsInfo-00000.0843114999981749.nc"
# variables for bodies:
#   uid alive type bcTag groupTag materialTag R0x R0y R0z Rx Ry Rz Vx Vy Vz Fx Fy Fz F_fixedx F_fixedy F_fixedz PHIx PHIy PHIz Wx Wy Wz Tz Ty Tz rad mass inerI nc
# variables for contacts:
#   uid1_ uid2_ uid_ delta_ normFn_ Fn_x Fn_y Fn_z Ft_x Ft_y Ft_z T_x T_y T_z Normal_x Normal_y Normal_z SpringTorsion_x SpringTorsion_y SpringTorsion_z SpringSlide_x SpringSlide_y SpringSlide_z SpringRolling_x SpringRolling_y SpringRolling_z   
import os.path
bname = sys.argv[1]
cname = bname.replace("bodies","contacts")
#time=np.float(bname.replace("OUTPUT/bodiesInfo-", "").replace(".nc", ""))
time = os.path.basename(bname).replace("bodiesInfo-", "").replace(".nc", "")
print("time: {}".format(time))
# read the bodies file
bdata = nc.Dataset(bname, mode='r')
#print(bdata.variables) # uncomment this if you want to see all the variables defined inside the datafile
# describe the contacts file
cdata = nc.Dataset(cname, mode='r')
#print(cdata.variables) # uncomment this if you want to see all the variables defined inside the datafile
ncontacts = cdata.variables['uid1_'][:].size
#input()

######################################################################
# Compute masks
MIN_X = (bdata.variables["Rx"][:-6] - bdata.variables["rad"][:-6]).min()
MAX_X = (bdata.variables["Rx"][:-6] + bdata.variables["rad"][:-6]).max()
MIN_Y = (bdata.variables["Ry"][:-6] - bdata.variables["rad"][:-6]).min()
MAX_Y = (bdata.variables["Ry"][:-6] + bdata.variables["rad"][:-6]).max()
MIN_Z = (bdata.variables["Rz"][:-6] - bdata.variables["rad"][:-6]).min()
MAX_Z = (bdata.variables["Rz"][:-6] + bdata.variables["rad"][:-6]).max()
print("Total of particles:", bdata.variables["Rx"].size)
MAX_RAD = (bdata.variables["rad"][:-6]).max()
print("SCREEN_LENGHT: ", SCREEN_FACTOR*MAX_RAD)
MASK_POS  = bdata.variables["Rx"][:] > MIN_X + SCREEN_FACTOR*MAX_RAD
MASK_POS &= bdata.variables["Rx"][:] < MAX_X - SCREEN_FACTOR*MAX_RAD
print("MIN_X + SL: ", MIN_X + SCREEN_FACTOR*MAX_RAD)
print("MAX_X - SL: ", MAX_X - SCREEN_FACTOR*MAX_RAD)
print("MIN_Y + SL: ", MIN_Y + SCREEN_FACTOR*MAX_RAD)
print("MAX_Y - SL: ", MAX_Y - SCREEN_FACTOR*MAX_RAD)
print("MIN_Z + SL: ", MIN_Z + SCREEN_FACTOR*MAX_RAD)
print("MAX_Z - SL: ", MAX_Z - SCREEN_FACTOR*MAX_RAD)
print("Remaining after filtering xpos : ", np.count_nonzero(MASK_POS))
MASK_POS &= bdata.variables["Ry"][:] > MIN_Y + SCREEN_FACTOR*MAX_RAD
MASK_POS &= bdata.variables["Ry"][:] < MAX_Y - SCREEN_FACTOR*MAX_RAD
print("Remaining after filtering ypos : ", np.count_nonzero(MASK_POS))
MASK_POS &= bdata.variables["Rz"][:] > MIN_Z + SCREEN_FACTOR*MAX_RAD
MASK_POS &= bdata.variables["Rz"][:] < MAX_Z - SCREEN_FACTOR*MAX_RAD
print("Remaining after filtering zpos : ", np.count_nonzero(MASK_POS))

######################################################################
# POSTPRO START
######################################################################

######################################################################
# Coordination number
zmean = np.average(bdata.variables["nc"][MASK_POS])
print("Coordination number: ", zmean)

######################################################################
# Packing fraction
PVol = np.sum(4*np.pi*bdata.variables['rad'][MASK_POS]**3/3.0)
print("PVol: ", PVol)
CVol = (MAX_X - MIN_X - 2*SCREEN_FACTOR*MAX_RAD)*(MAX_Y - MIN_Y - 2*SCREEN_FACTOR*MAX_RAD)*(MAX_Z - MIN_Z - 2*SCREEN_FACTOR*MAX_RAD)
print("CVol: ", CVol)
PHI = PVol/CVol
print("Packing fraction: ", PHI)

np.savetxt("ixyzrad.txt", np.column_stack((bdata.variables["uid"][:], bdata.variables["Rx"][:], bdata.variables["Ry"][:], bdata.variables["Rz"][:], bdata.variables["rad"][:])), fmt="%d\t%.18e\t%.18e\t%.18e\t%.18e")
np.savetxt("xyzrad.csv", np.column_stack((bdata.variables["Rx"][:-6], bdata.variables["Ry"][:-6], bdata.variables["Rz"][:-6], bdata.variables["rad"][:-6], bdata.variables["nc"][:-6])), delimiter=",") # For paraview visualization
import subprocess as sp
command = f"voro++ -r {MIN_X} {MAX_X} {MIN_Y} {MAX_Y} {MIN_Z} {MAX_Z} ixyzrad.txt"
print("Executing command -> ", command)
sp.run(command, shell=True, check=True)
uid, x, y, z, vol, r = np.loadtxt("ixyzrad.txt.vol", unpack=True)
indexes = np.argsort(uid)
vol = vol[indexes]
vol = np.append(vol, [False, False, False, False, False, False]) # Add walls flags
PVol = np.sum(4*np.pi*bdata.variables['rad'][MASK_POS]**3/3.0)
CVol = np.sum(vol[MASK_POS])
PHI = PVol/CVol
print("Packing fraction from Voro++: ", PHI)


######################################################################
# compute the mean force per particle, defined as the sum of the normal forces
print("# Computing mean force per particle (sum of normal forces) ...")
meanFn = np.zeros_like(bdata.variables["Rx"])
counterFn = np.zeros_like(bdata.variables["Rx"])
for ic, val in enumerate(cdata.variables["normFn_"]):
    meanFn[cdata.variables["uid1_"][ic]] += val 
    counterFn[cdata.variables["uid1_"][ic]] += 1 
    meanFn[cdata.variables["uid2_"][ic]] += val 
    counterFn[cdata.variables["uid2_"][ic]] += 1 
meanFn /= counterFn
print(meanFn[MASK_POS])
print(np.average(meanFn[MASK_POS]))
np.savetxt("POSTPRO/meanFn-phi.txt", np.array([np.average(meanFn[MASK_POS]), PHI, zmean]), newline='\t')
    
######################################################################
# compute the number of sliding contacts
normSpringSliding = np.sqrt(cdata.variables['SpringSlide_x'][:]**2 + cdata.variables['SpringSlide_y'][:]**2 + cdata.variables['SpringSlide_z'][:]**2) # compute an array of norms for the sliding spring
nsliding = normSpringSliding[normSpringSliding > MUS*cdata.variables['normFn_'][:]/KT].size
print("Number of sliding contacts:{}".format(nsliding))
print("Total number of contacts: {}".format(ncontacts))

######################################################################
# compute number of particles with N contacts. This returns a dynamic array that changes at each time step
nc, nccount = np.unique(bdata.variables['nc'][:-6], return_counts=True) # use bdata.variables['nc'][:-6] to avoid walls
print("Unique possible contacts: {}".format(nc))
print("Counts of each number of contacts: {}".format(nccount))
MAXNC=12
bins=np.zeros(MAXNC)
for ii in np.arange(nc.size):
    bins[nc[ii]] = nccount[ii]
print("Histogram of contacts: {}".format(bins))

######################################################################
# Average normal, tangential and total force
# NOTE: I am cumputing the average of the norm, not the norm of the average
avgFn = np.average(cdata.variables['normFn_'][:])
avgFt = np.average(np.sqrt(cdata.variables['Ft_x'][:]**2 + cdata.variables['Ft_y'][:]**2 + cdata.variables['Ft_z'][:]**2))
Fn_x = cdata.variables['normFn_'][:]*cdata.variables['Normal_x'][:]
Fn_y = cdata.variables['normFn_'][:]*cdata.variables['Normal_y'][:]
Fn_z = cdata.variables['normFn_'][:]*cdata.variables['Normal_z'][:]
Ftot_x = Fn_x + cdata.variables['Ft_x'][:]
Ftot_y = Fn_y + cdata.variables['Ft_y'][:]
Ftot_z = Fn_z + cdata.variables['Ft_z'][:]
avgFtot = np.average(np.sqrt(Ftot_x**2 + Ftot_y**2 + Ftot_z**2))
print("Average force normal     : {}".format(avgFn))
print("Average force tangential : {}".format(avgFt))
print("Average force total      : {}".format(avgFtot))

