for a in DISPLAY/voronoi_p_*.pov; do 
    echo $a
    time=${a%.pov}; 
    time=${time#DISPLAY/voronoi_p_}; 
    echo $time; 
    ../bin/povray_voronoi.py $a DISPLAY/voronoi_c_${time}.pov voronoi_${time}.pov; 
done 
