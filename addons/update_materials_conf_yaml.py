#!/usr/bin/env python
import yaml
import os

import sys

data_orig = yaml.load(open('INPUT/materials_conf.yaml').read())
os.system('cp INPUT/materials_conf.yaml INPUT/materials_conf.yaml.old' )
data_new = open('INPUT/materials_conf.yaml', 'w')

for i in range(len(data_orig)) :
    data_new.write('- TAG  : ' + str(data_orig[i][0]) + '\n')
    data_new.write('  RHO  : ' + str(data_orig[i][1]) + '\n')
    data_new.write('  K    : ' + str(data_orig[i][2]) + '\n')
    data_new.write('  E    : ' + str(data_orig[i][3]) + '\n')
    data_new.write('  MUSF : ' + str(data_orig[i][4]) + '\n')
    data_new.write('  MUSR : ' + str(data_orig[i][5]) + '\n')
    if len(data_orig[i]) == 7 :
        data_new.write('  RES  : ' + str(data_orig[i][6]) + '\n')
    else :
        data_new.write('  RES  : ' + str(data_orig[i][0]) + '\n')
    # print cohesion data
    data_new.write('  # Cohesion properties\n')
    data_new.write('  STRESSC   : 0.0e6   # Cohesion strength stress \n')
    data_new.write('  OMEGAC    : 0.000   # Maximum relative displacement to broke a cohesive contact = OMEGAC*2*radij, typically 0.020\n')
    data_new.write('\n')
    

data_new.close()

print("####################################################")
print("Done updating old materials.conf.yaml to new syntax.")
print("OLD config saved to INPUT/materials_conf.yaml.old")
print("NEW config saved to INPUT/materials_conf.yaml ")
print("Please double-check the written file.")

