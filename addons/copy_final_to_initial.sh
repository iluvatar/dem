#!/bin/bash
cd "$(pwd)"
echo "Copying OUTPUT final states to INPUT ... "
for name in OUTPUT/bodiesInfo.* OUTPUT/contactsInfo.* ; do  
    command="cp -f $name INPUT/"
    echo $command
    $command 2> /dev/null
done
echo "Done"
