#!/usr/bin/env python

import os, sys, shutil
import numpy as np
import glob

###############################################################
# some utility functions
def get_number_of_columns(filename) :
    if os.stat(filename).st_size == 0:
        return 0
    file=open(filename, 'r')
    for line in file:
        if "#" != line[0]:
            return len(line.split())
    return 0

def symbol_format(i):
    return ';s' + str(i) + ' symbol ' + str((i+1)%10) + '; s' + str(i) + ' symbol size 0.5; s' + str(i) + ' symbol fill pattern 1; s' + str(i) + ' symbol fill color ' + str((i+2)%8) + ';'

def column_spec(x, y, dataname, nc_single_max=0):
    bxy =' -bxy ' + str(x) + ':' + str(y)
    nc = get_number_of_columns(INPUT_DIR_NAME + os.sep  + dataname)
    xtype =" -block " 
    if ERROR_BAR_FLAG and nc >= 2*y and nc > x and nc > y and nc > 2*nc_single_max:
        bxy = bxy + ':' + str(y + nc/2)
        xtype = " -settype xydy -block "
    return bxy + ' ', xtype

###############################################################
# Some global vars
ROOT_PATH_DIR=""
ERROR_BAR_FLAG=False

###############################################################
# check args 
if len(sys.argv) < 3 :
    sys.exit('ERROR. Usage:\n %s ROOT_DIR error_bars_flag [Filename1 Filename2 ... ] \nROOT_DIR = Directory which includes POSTPRO subdir\nerror_bars_flag = 0 or 1, in case the data is enough to plot also error bars \n[Filename1 Filename2 ...]  = List of filenames to plot, instead of default list' % sys.argv[0])
ROOT_PATH_DIR = sys.argv[1]
if not os.path.isdir(ROOT_PATH_DIR + os.sep + 'POSTPRO') :
    sys.exit('ERROR. Dir POSTPRO is not in specified path or current directory. Exiting')
if int(sys.argv[2]) != 0:
    ERROR_BAR_FLAG=True

###############################################################
# output global vars
OUTPUT_DIR_NAME = ROOT_PATH_DIR + os.sep + 'XMGRACE_PLOTS'
INPUT_DIR_NAME = ROOT_PATH_DIR + os.sep + 'POSTPRO'

###############################################################
# create the output directories
if os.path.exists(OUTPUT_DIR_NAME):
    shutil.rmtree(OUTPUT_DIR_NAME)
os.makedirs(OUTPUT_DIR_NAME)

###############################################################
# define the files to look for
print("Setting filenames list") 
file_names = [];
if len(sys.argv[3:]) == 0 :
    print("Using default filenames list.")
    file_names = ['voidratio.dat', 'packing_fraction.dat', 'P_X.dat', 'P_multivoro.dat', 'P_multivoro_scaled.dat', 'P_nc.dat', 'energy.dat', \
                  'gamma_qoverp.dat', 'meanPenetration.dat', 'mean_Vx.dat', 'mean_Vx_normalized.dat', 'mean_x_translation.dat', 'nc_correlation.dat', \
                  'z.dat', 'histoVoroVol_scaled.dat', 'sigma.dat', 'Pdelta.dat', 'Pdelta2.dat', 'eq_qoverp.dat', 'pq.dat', \
                  'walls_pos.dat', 'walls_vel.dat', 'histo_delaunay_scaled.dat', \
                  'contact_xy.dat', 'contact_xz.dat', 'contact_yz.dat', 'P_Fn_xy.dat', 'P_Fn_xz.dat', 'P_Fn_yz.dat', 'P_Ft_xy.dat', 'P_Ft_xz.dat', 'P_Ft_yz.dat', \
                  'histo_delaunay_vol.dat', 'histoVoroVol.dat', 'crystalized.dat']
else :
    file_names = sys.argv[3:]

file_names.sort()
print("filenames = " + str(file_names))

###############################################################
# declare maps
xmgrace_data_call = {}; xmgrace_pexec = {}; xmgrace_save = {};

###############################################################
# set the options per file
def set_options( filename ) :
    "Sets the options per parameter"
    # voidratio
    if filename == 'voidratio.dat':
        basename = filename.replace('.dat', ''); dataname = basename + '.dat'; gracename = basename + '.agr'; 
        BXY, TYPE = column_spec(4, 2, filename)
        xmgrace_data_call[dataname] = TYPE + INPUT_DIR_NAME + os.sep  + dataname + BXY
        xmgrace_pexec[dataname] = ' \'s0 legend \"void ratio\"; ; ' + symbol_format(0) + 'title \"void ratio as a function of time\"; xaxis label \"time (s)\"; yaxis label \"Voidratio\" \' '
        xmgrace_save[dataname] =  gracename ;
    # packing_fraction
    elif filename == 'packing_fraction.dat' :
        basename = filename.replace('.dat', ''); dataname = basename + '.dat'; gracename = basename + '.agr'; 
        BXY, TYPE = column_spec(3, 2, filename)
        xmgrace_data_call[dataname] = TYPE + INPUT_DIR_NAME + os.sep  + dataname + BXY
        xmgrace_pexec[dataname] = ' \'s0 legend \"packing fraction\"; ' + symbol_format(0) + 'title \"Packing fraction as a function of time\"; xaxis label \"time (s)\"; yaxis label \"Packing fraction\" \' '
        xmgrace_save[dataname] =  gracename
    # P_X
    elif filename == 'P_X.dat' :
        basename = filename.replace('.dat', ''); dataname = basename + '.dat'; gracename = basename + '.agr'; 
        BXY, TYPE = column_spec(1, 2, filename)
        xmgrace_data_call[dataname] = TYPE + INPUT_DIR_NAME + os.sep  + dataname + BXY
        xmgrace_pexec[dataname] = ' \'s0 legend \"P_X/d^3\" ; ' + symbol_format(0) + ' title \"PDF voronoi compactivity\"; xaxis label \"X/d^3\"; yaxis label \"PDF\" \' '
        xmgrace_save[dataname] =  gracename
    # P_multivoro
    elif filename == 'P_multivoro.dat' :
        basename = filename.replace('.dat', ''); dataname = basename + '.dat'; gracename = basename + '.agr'; 
        xmgrace_data_call[dataname] = ""
        xmgrace_pexec[dataname] = ' \'title \"Voronoi-Volume distribution\\n as a function of cluster size\"; xaxis label \"Voronoi Volume/N(dmin^3)\"; yaxis label \"PDF\" '
        i = 0
        for fn in glob.glob(INPUT_DIR_NAME + os.sep + 'histograms_multivoro' + os.sep + basename + '_?????.dat'):
            size=fn.replace('.dat', '')
            size=size.split('_')[2]
            BXY, TYPE = column_spec(1, 2, fn.replace(INPUT_DIR_NAME + os.sep, ''))
            xmgrace_data_call[dataname] += TYPE + fn + BXY 
            xmgrace_pexec[dataname] += ' ; s' + str(i) + ' legend \"multi_' + size.zfill(5) + '\"; ' + symbol_format(i)
            i += 1
        xmgrace_pexec[dataname] += ' \' '
        xmgrace_save[dataname] =  gracename
    # P_multivoro_scaled
    elif filename == 'P_multivoro_scaled.dat' : 
        basename = filename.replace('.dat', ''); dataname = basename + '.dat'; gracename = basename + '.agr'; 
        xmgrace_data_call[dataname] = ''
        xmgrace_pexec[dataname] = ' \' title \"Normalized Voronoi-Volume distribution\\n as a function of cluster size\"; xaxis label \"V-Vmin/<V>-Vmin\"; yaxis label \"PDF\" '
        i = 0
        for fn in glob.glob(INPUT_DIR_NAME + os.sep +  'histograms_multivoro' + os.sep + basename.replace('_scaled', '') + '_?????_scaled.dat'):
            size=fn.replace('_scaled.dat', '')
            size=size.split('_')[2]
            BXY, TYPE = column_spec(1, 2, fn.replace(INPUT_DIR_NAME + os.sep, ''))
            xmgrace_data_call[dataname] += TYPE + fn + BXY 
            xmgrace_pexec[dataname] += ' ; s' + str(i) + ' legend \"multi_' + size.zfill(5) + '\" ; ' + symbol_format(i) 
            i += 1
        xmgrace_pexec[dataname] += ' ; \' '
        xmgrace_save[dataname] =  gracename
    # P_nc
    elif filename == 'P_nc.dat' :
        basename = filename.replace('.dat', ''); dataname = basename + '.dat'; gracename = basename + '.agr'; 
        BXY, TYPE = column_spec(1, 2, filename)
        xmgrace_data_call[dataname] = TYPE + INPUT_DIR_NAME + os.sep  + dataname + BXY 
        xmgrace_pexec[dataname] = ' \'s0 legend \"P_nc\" ; ' + symbol_format(0) + '; title \"PDF for number of contacts\"; xaxis label \"nc\"; yaxis label \"PDF\" \' '
        xmgrace_save[dataname] =  gracename
    # energy
    elif filename == 'energy.dat' :
        basename = filename.replace('.dat', ''); dataname = basename + '.dat'; gracename = basename + '.agr'; 
        BXY, TYPE = column_spec(1, 3, filename)
        xmgrace_data_call[dataname] = TYPE + INPUT_DIR_NAME + os.sep  + dataname + BXY 
        xmgrace_pexec[dataname] = ' \'s0 legend \"Kinetic Energy/EpH\"; ' + symbol_format(0) + ' title \"Kinetic energy/EpH as a function of time\"; xaxis label \"time (s)\"; yaxis label \"Ek/EpH\" \' '
        xmgrace_save[dataname] =  gracename
    # gamma_goverp
    elif filename == 'gamma_qoverp.dat' : 
        basename = filename.replace('.dat', ''); dataname = basename + '.dat'; gracename = basename + '.agr'; 
        BXY, TYPE = column_spec(1, 2, filename)
        xmgrace_data_call[dataname] = TYPE + INPUT_DIR_NAME + os.sep  + dataname + BXY
        xmgrace_pexec[dataname] = ' \'s0 legend \"q/p\" ; ' + symbol_format(0) + 'title \"q/p as a function of gamma (shear deformation)\"; xaxis label \"gamma\"; yaxis label \"q/p\" \' '
        xmgrace_save[dataname] =  gracename
    # mean penetration
    elif filename == 'meanPenetration.dat' :
        basename = filename.replace('.dat', ''); dataname = basename + '.dat'; gracename = basename + '.agr'; 
        BXY, _ =  column_spec(1, 2, filename, 3)
        BXYtmp, TYPE = column_spec(1, 3, filename)
        BXY += BXYtmp
        xmgrace_data_call[dataname] = TYPE + INPUT_DIR_NAME + os.sep  + dataname + BXY
        xmgrace_pexec[dataname] = ' \'s0 legend \"mean penetration/max_d\"; ' + symbol_format(0) + 's1 legend \"max penetration/max_d\" ; ' + symbol_format(1) + '; title \"mean-max penetration/max_d\\n as a function of time\"; xaxis label \"time (s)\"; yaxis label \"mean-max penetration/max_d\" \' '
        xmgrace_save[dataname] =  gracename
    # Vx profile
    elif filename == 'mean_Vx.dat' :
        basename = filename.replace('.dat', ''); dataname = basename + '.dat'; gracename = basename + '.agr'; 
        xmgrace_data_call[dataname] = ' -settype xydy -block ' + INPUT_DIR_NAME + os.sep  + dataname + ' -bxy 1:2:3 '
        xmgrace_pexec[dataname] = ' \'s0 legend \"data\"; ' + symbol_format(0) + 'title \"Horizontal Velocity profile\"; xaxis label \"height\"; yaxis label \"Mean velocity\" \' '
        xmgrace_save[dataname] =  gracename
    # Vx profile normalized
    elif filename == 'mean_Vx_normalized.dat' :
        basename = filename.replace('.dat', ''); dataname = basename + '.dat'; gracename = basename + '.agr'; 
        xmgrace_data_call[dataname] = ' -settype xydy -block ' + INPUT_DIR_NAME + os.sep  + dataname + ' -bxy 1:2:3 '
        xmgrace_pexec[dataname] = ' \'s0 legend \"data\"; ' + symbol_format(0) + 'title \"Horizontal Velocity profile (Normalized)\"; xaxis label \"height/zf\"; yaxis label \"Mean velocity/Vxwall\" \' '
        xmgrace_save[dataname] =  gracename
    # x translation profile
    elif filename == 'mean_x_translation.dat' :
        basename = filename.replace('.dat', ''); dataname = basename + '.dat'; gracename = basename + '.agr'; 
        xmgrace_data_call[dataname] = ' -settype xydy -block ' + INPUT_DIR_NAME + os.sep  + dataname + ' -bxy 1:2:3 '
        xmgrace_pexec[dataname] = ' \'s0 legend \"data\"; ' + symbol_format(0) + 'title \"Horizontal translation profile\"; xaxis label \"height\"; yaxis label \"Mean horizontal translation\" \' '
        xmgrace_save[dataname] =  gracename
    # nc correlation + FIT
    elif filename == 'nc_correlation.dat' :
        basename = filename.replace('.dat', ''); dataname = basename + '.dat'; gracename = basename + '.agr'; 
        BXY, TYPE = column_spec(3, 2, filename)
        xmgrace_data_call[dataname] = TYPE + INPUT_DIR_NAME + os.sep  + dataname + BXY
        xmgrace_pexec[dataname] = ' \'s0 legend \"nc-persistance\" ; yaxes scale logarithmic; ' + symbol_format(0) + ' title \"Persistance of contacts from a reference set\\nLOG SCALE IN Y\"; xaxis label \"gamma\"; yaxis label \"Number of contacts\" ; fit formula \"y = a0*exp(-a1*x)\" ; fit with 2 parameters ; fit prec 1.0e-6 ; nonlfit(s0, 10000); \' -pexec \'  s1 on; s1 length s0.length ; s1.x = s0.x; s1.y = a0*exp(-a1*x) ; s1 legend \"FIT - exponential\";  \' '
        xmgrace_save[dataname] =  gracename
    # z
    elif filename == 'z.dat' :
        basename = filename.replace('.dat', ''); dataname = basename + '.dat'; gracename = basename + '.agr'; 
        BXY, _ =  column_spec(1, 2, filename)
        BXYtmp, TYPE = column_spec(1, 3, filename)
        BXY += BXYtmp
        xmgrace_data_call[dataname] = TYPE + INPUT_DIR_NAME + os.sep  + dataname + BXY
        xmgrace_pexec[dataname] = ' \'s0 legend \"<z> - particles\"; ' + symbol_format(0) + ' s1 legend \"<z> - Fabric tensor\" ; ' + symbol_format(1) + ' title \"Coordination number\"; xaxis label \"time (s)\"; yaxis label \"z\" \' '
        xmgrace_save[dataname] =  gracename
    # histoVoroVol + fit + theoretical
    elif filename == 'histoVoroVol_scaled.dat' :
        basename = filename.replace('.dat', ''); dataname = basename + '.dat'; gracename = basename + '.agr'; 
        BXY, TYPE = column_spec(1, 2, filename)
        xmgrace_data_call[dataname] = TYPE + INPUT_DIR_NAME + os.sep  + dataname + BXY + TYPE + INPUT_DIR_NAME + os.sep + 'voronoi_distro_scaled.dat' + BXY
        xmgrace_pexec[dataname] = ' \'s0 legend \"Data\" ; ' + symbol_format(0) + ' restrict (s0, s0.x>0);  title \"PDF - Scaled Voronoi Volumes\"; xaxis label \"(V-Vmin)/(<V>-Vmin)\"; yaxis label \"PDF\" ; fit formula \"y = a1*((a0^a0)/gamma(a0))*(x^(a0-1))*exp(-a0*x)\" ; fit with 2 parameters ; fit prec 1.0e-7 ; a0 = 10; a1 = 1.0; nonlfit(s0, 100000); \' -pexec \'  s2 on; s2 length s0.length ; s2.x = s0.x; s2.y = a1*((a0^a0)/gamma(a0))*(x)^(a0-1)*exp(-a0*x) ; s2 legend \"FIT - gamma-distro\"; s1 legend \"Theoretical distro from sample\";   \' '
        xmgrace_save[dataname] =  gracename
    # sigma
    elif filename == 'sigma.dat' :
        basename = filename.replace('.dat', ''); dataname = basename + '.dat'; gracename = basename + '.agr'; 
        BXY, _ =  column_spec(1, 2, filename)
        BXYtmp, _ = column_spec(1, 3, filename)
        BXY += BXYtmp
        BXYtmp, TYPE = column_spec(1, 4, filename)
        BXY += BXYtmp
        xmgrace_data_call[dataname] = TYPE + INPUT_DIR_NAME + os.sep  + dataname + BXY
        xmgrace_pexec[dataname] = ' \'s0 legend \"s0\"; ' + symbol_format(0) + '; s1 legend \"s1\" ; ' + symbol_format(1) + ' s2 legend \"s2\" ; ' + symbol_format(2) + ' s3 on; s3 length s0.length; s3.x = s0.x; s3.y = (s0.y + s1.y + s2.y)/3.0; s3 legend \"average\" ; ' + symbol_format(3) + '; title \"Eigen values of the Stress tensor\"; xaxis label \"time (s)\"; yaxis label \"Eigen Values S\" \' '
        xmgrace_save[dataname] =  gracename
    # Pdelta
    elif filename == 'Pdelta.dat' : 
        basename = filename.replace('.dat', ''); dataname = basename + '.dat'; gracename = basename + '.agr'; 
        BXY, TYPE = column_spec(1, 2, filename)
        xmgrace_data_call[dataname] = TYPE + INPUT_DIR_NAME + os.sep  + dataname + BXY
        xmgrace_pexec[dataname] = ' \'s0 legend \"P_delta\" ; ' + symbol_format(0) + ' ; title \"PDF interpenetration - delta \"; xaxis label \"delta*1000/d\"; yaxis label \"PDF\" \' '
        xmgrace_save[dataname] =  gracename
    # Pdelta2
    elif filename == 'Pdelta2.dat' :
        basename = filename.replace('.dat', ''); dataname = basename + '.dat'; gracename = basename + '.agr'; 
        BXY, TYPE = column_spec(1, 2, filename)
        xmgrace_data_call[dataname] = TYPE + INPUT_DIR_NAME + os.sep  + dataname + BXY
        xmgrace_pexec[dataname] = ' \'s0 legend \"P_delta2\" ; ' + symbol_format(0) + ' ; title \"PDF interpenetration^2 - delta^2 \"; xaxis label \"(delta*1000/d)^2\"; yaxis label \"PDF\" \' '
        xmgrace_save[dataname] =  gracename
    # eq_qoverp
    elif filename == 'eq_qoverp.dat' :
        basename = filename.replace('.dat', ''); dataname = basename + '.dat'; gracename = basename + '.agr'; 
        BXY, TYPE = column_spec(1, 2, filename)
        xmgrace_data_call[dataname] = TYPE + INPUT_DIR_NAME + os.sep  + dataname + BXY
        xmgrace_pexec[dataname] = ' \'s0 legend \"q/p\" ; ' + symbol_format(0) + ' ; title \"q/p vs eq \"; xaxis label \"eq\"; yaxis label \"q/p\" \' '
        xmgrace_save[dataname] =  gracename
    # q and p versus time
    elif filename == 'pq.dat' :
        basename = filename.replace('.dat', ''); dataname = basename + '.dat'; gracename = basename + '.agr'; 
        BXY, _ =  column_spec(1, 3, filename)
        BXYtmp, TYPE = column_spec(1, 4, filename)
        BXY += BXYtmp
        xmgrace_data_call[dataname] = TYPE + INPUT_DIR_NAME + os.sep  + dataname + BXY
        xmgrace_pexec[dataname] = ' \'s0 legend \"p\"; ' + symbol_format(0) + 's1 legend \"q\" ; ' + symbol_format(1) + 'title \"p and q as a function of time\"; xaxis label \"time (s)\"; yaxis label \"p - q\" \' '
        xmgrace_save[dataname] =  gracename
    # walls positions 
    elif filename == 'walls_pos.dat' :
        basename = filename.replace('.dat', ''); dataname = basename + '.dat'; gracename = basename + '.agr'; 
        BXY, _ =  column_spec(1, 7, filename, 18)
        BXYtmp, _ = column_spec(1, 8, filename, 18); BXY += BXYtmp
        BXYtmp, _ = column_spec(1, 11, filename, 18); BXY += BXYtmp
        BXYtmp, _ = column_spec(1, 15, filename, 18); BXY += BXYtmp
        BXYtmp, TYPE = column_spec(1, 18, filename, 18); BXY += BXYtmp
        BXYtmp, _ = column_spec(1, 5, filename, 18); BXY += BXYtmp
        BXYtmp, _ = column_spec(1, 4, filename, 18); BXY += BXYtmp
        xmgrace_data_call[dataname] = TYPE + INPUT_DIR_NAME + os.sep  + dataname + BXY
        xmgrace_pexec[dataname] = ' \'s0 legend \"Rz - TOP\" ; ' + symbol_format(0) + ' ; s1 legend \"Rx - LEFT\" ; ' + symbol_format(1) + ' ; s2 legend \"Rx - RIGHT\" ; ' + symbol_format(2) + ' ; s3 legend \"Ry - BACK\" ; ' + symbol_format(3) + ' ; s4 legend \"Ry - FRONT\" ; ' + symbol_format(4) + ' ; s5 legend \"Rx - TOP\" ; ' + symbol_format(5) + ' ; s6 legend \"Rz - BOTTOM\" ; ' + symbol_format(6) + ' ; title \"Walls positions as functions of time\"; xaxis label \"time (s)\"; yaxis label \"pos \" \' '
        xmgrace_save[dataname] =  gracename
    # walls velocities
    elif filename == 'walls_vel.dat' :
        basename = filename.replace('.dat', ''); dataname = basename + '.dat'; gracename = basename + '.agr'; 
        BXY, _ =  column_spec(1, 7, filename, 18)
        BXYtmp, _ = column_spec(1, 8, filename, 18); BXY += BXYtmp
        BXYtmp, _ = column_spec(1, 11, filename, 18); BXY += BXYtmp
        BXYtmp, _ = column_spec(1, 15, filename, 18); BXY += BXYtmp
        BXYtmp, TYPE = column_spec(1, 18, filename, 18); BXY += BXYtmp
        BXYtmp, _ = column_spec(1, 5, filename, 18); BXY += BXYtmp
        BXYtmp, _ = column_spec(1, 4, filename, 18); BXY += BXYtmp
        xmgrace_data_call[dataname] = TYPE + INPUT_DIR_NAME + os.sep  + dataname + BXY
        xmgrace_pexec[dataname] = ' \'s0 legend \"Vz - TOP\" ; ' + symbol_format(0) + ' ; s1 legend \"Vx - LEFT\" ; ' + symbol_format(1) + ' ; s2 legend \"Vx - RIGHT\" ; ' + symbol_format(2) + ' ; s3 legend \"Vy - BACK\" ; ' + symbol_format(3) + ' ; s4 legend \"Vy - FRONT\" ; ' + symbol_format(4) + ' ; s5 legend \"Vx - TOP\" ; ' + symbol_format(5) + ' ; s6 legend \"Vz - BOTTOM\" ; ' + symbol_format(6) + ' ; title \"Walls velocities as function of time\"; xaxis label \"time (s)\"; yaxis label \"vel \" \' '
        xmgrace_save[dataname] =  gracename
#    # voronoi_packing_fraction_mean_sigma
#    elif filename == 'voronoi_packing_fraction_mean_sigma.dat' :
#        basename = filename.replace('.dat', ''); dataname = basename + '.dat'; gracename = basename + '.agr'; 
#        BXY, TYPE = column_spec(1, 2, filename)
#        xmgrace_data_call[dataname] = TYPE + INPUT_DIR_NAME + os.sep  + dataname + BXY
#        xmgrace_pexec[dataname] = ' \'s0 legend \"packing fraction from voronoi tesselation\"; ' + symbol_format(0) + 'title \"Packing fraction as a function of time, from voronoi tesselation\"; xaxis label \"time (s)\"; yaxis label \"Packing fraction\" \' '
#        xmgrace_save[dataname] =  gracename
    # contact_xy
    elif filename == 'contact_xy.dat' :
        basename = filename.replace('.dat', ''); dataname = basename + '.dat'; gracename = basename + '.agr'; 
        #BXY, TYPE = column_spec(1, 2, filename)
        BXY  = ' -bxy 1:2 '
        TYPE = ' -block '
        xmgrace_data_call[dataname] = TYPE + INPUT_DIR_NAME + os.sep  + 'histograms_contacts' + os.sep + dataname + BXY 
        xmgrace_pexec[dataname] = ' \'s0 legend \"Contact Angle-xy\" ; ' + symbol_format(0) + '; title \"Contact orientation projected on XY\"; xaxis label \"angle_xy\"; yaxis label \"PDF\" ; G0 TYPE POLAR;  \' '
        xmgrace_save[dataname] =  gracename
    # contact_xz
    elif filename == 'contact_xz.dat' :
        basename = filename.replace('.dat', ''); dataname = basename + '.dat'; gracename = basename + '.agr'; 
        #BXY, TYPE = column_spec(1, 2, filename)
        BXY  = ' -bxy 1:2 '
        TYPE = ' -block '
        xmgrace_data_call[dataname] = TYPE + INPUT_DIR_NAME + os.sep  + 'histograms_contacts' + os.sep + dataname + BXY 
        xmgrace_pexec[dataname] = ' \'s0 legend \"Contact Angle-xz\" ; ' + symbol_format(0) + '; title \"Contact orientation projected on XZ\"; xaxis label \"angle_xz\"; yaxis label \"PDF\" ; G0 TYPE POLAR;  \' '
        xmgrace_save[dataname] =  gracename
    # contact_yz
    elif filename == 'contact_yz.dat' :
        basename = filename.replace('.dat', ''); dataname = basename + '.dat'; gracename = basename + '.agr'; 
        #BXY, TYPE = column_spec(1, 2, filename)
        BXY  = ' -bxy 1:2 '
        TYPE = ' -block '
        xmgrace_data_call[dataname] = TYPE + INPUT_DIR_NAME + os.sep  + 'histograms_contacts' + os.sep + dataname + BXY 
        xmgrace_pexec[dataname] = ' \'s0 legend \"Contact Angle-yz\" ; ' + symbol_format(0) + '; title \"Contact orientation projected on YZ\"; xaxis label \"angle_yz\"; yaxis label \"PDF\" ; G0 TYPE POLAR;  \' '
        xmgrace_save[dataname] =  gracename
    # histo_delaunay_scaled + fit + theoretical
    elif filename == 'histo_delaunay_scaled.dat' :
        basename = filename.replace('.dat', ''); dataname = basename + '.dat'; gracename = basename + '.agr'; 
        BXY, TYPE = column_spec(1, 2, filename)
        xmgrace_data_call[dataname] = TYPE + INPUT_DIR_NAME + os.sep  + dataname + BXY + TYPE + INPUT_DIR_NAME + os.sep + 'delaunay_distro_scaled.dat' + BXY
        xmgrace_pexec[dataname] = ' \'s0 legend \"Data\" ; ' + symbol_format(0) + ' restrict (s0, s0.x>0);  title \"PDF - Scaled Delaunay Volumes\"; xaxis label \"(V-Vmin)/(<V>-Vmin)\"; yaxis label \"PDF\" ; fit formula \"y = a1*((a0^a0)/gamma(a0))*(x^(a0-1))*exp(-a0*x)\" ; fit with 2 parameters ; fit prec 1.0e-7 ; a0 = 1.5; a1 = 1.0; nonlfit(s0, 100000); \' -pexec \'  s2 on; s2 length s0.length ; s2.x = s0.x; s2.y = a1*((a0^a0)/gamma(a0))*(x)^(a0-1)*exp(-a0*x) ; s2 legend \"FIT - gamma-distro\"; s1 legend \"Theoretical distro from sample\";   \'  '
        xmgrace_save[dataname] =  gracename
    # histo_delaunay_scaled + fit + theoretical
    elif filename == 'histo_delaunay_vol.dat' :
        basename = filename.replace('.dat', ''); dataname = basename + '.dat'; gracename = basename + '.agr'; 
        BXY, TYPE = column_spec(1, 2, filename)
        xmgrace_data_call[dataname] = TYPE + INPUT_DIR_NAME + os.sep  + dataname + BXY + TYPE + INPUT_DIR_NAME + os.sep + 'delaunay_distro.dat' + BXY
        xmgrace_pexec[dataname] = ' \'s0 legend \"Data\" ; ' + symbol_format(0) + ' restrict (s0, s0.x>0);  title \"PDF - Delaunay Volumes\"; xaxis label \"V/d^3\"; yaxis label \"PDF\" ; fit formula \"y = a3*exp(a0*ln(a0*((x-a1)/(a2-a1))) - a0*((x-a1)/(a2-a1)))/((x-a1)*gamma(a0))\" ; fit with 4 parameters ; fit prec 1.0e-7 ; a0 = 6.0; a1 = 0.10; a2 = 0.20; a3 = 1.0; nonlfit(s0, 100000); \' -pexec \'  s2 on; s2 length s0.length ; s2.x = s0.x; s2.y = a3*exp(a0*ln(a0*((x-a1)/(a2-a1))) - a0*((x-a1)/(a2-a1)))/((x-a1)*gamma(a0)) ; s2 legend \"FIT - gamma-distro\"; s1 legend \"Theoretical distro from sample\";   \' '
        xmgrace_save[dataname] =  gracename
    # histoVoroVol + fit + theoretical
    elif filename == 'histoVoroVol.dat' :
        basename = filename.replace('.dat', ''); dataname = basename + '.dat'; gracename = basename + '.agr'; 
        BXY, TYPE = column_spec(1, 2, filename)
        xmgrace_data_call[dataname] = TYPE + INPUT_DIR_NAME + os.sep  + dataname + BXY + TYPE + INPUT_DIR_NAME + os.sep + 'voronoi_distro.dat' + BXY
        xmgrace_pexec[dataname] = ' \'s0 legend \"Data\" ; ' + symbol_format(0) + ' restrict (s0, s0.x>0);'  + '  title \"PDF - Voronoi Volumes\"; xaxis label \"V/d^3\"; yaxis label \"PDF\" ; fit formula \"y = a3*exp(a0*ln(a0*((x-a1)/(a2-a1))) - a0*((x-a1)/(a2-a1)))/((x-a1)*gamma(a0))\" ; fit with 4 parameters ; fit prec 1.0e-2 ; a0 = 13.0; a1 = 0.4; a2 = 0.80; a3 = 1.0; a0 constraints on; a0min = 8; a0max = 28; nonlfit(s0, 10000); \' -pexec \'  s2 on; s2 length s0.length ; s2.x = s0.x; s2.y = a3*exp(a0*ln(a0*((x-a1)/(a2-a1))) - a0*((x-a1)/(a2-a1)))/((x-a1)*gamma(a0)) ; s2 legend \"FIT - gamma-distro\"; s1 legend \"Theoretical distro from sample\";   \' '
        xmgrace_save[dataname] =  gracename
    # P_Fn_xy
    elif filename == 'P_Fn_xy.dat' :
        basename = filename.replace('.dat', ''); dataname = basename + '.dat'; gracename = basename + '.agr'; 
        #BXY, TYPE = column_spec(1, 2, filename)
        BXY  = ' -bxy 1:2 '
        TYPE = ' -block '
        xmgrace_data_call[dataname] = TYPE + INPUT_DIR_NAME + os.sep  + "histograms_F_T" + os.sep + dataname + BXY 
        xmgrace_pexec[dataname] = ' \'s0 legend \"Fn Angle-xy\" ; ' + symbol_format(0) + '; title \"Normal force orientation projected on XY\"; xaxis label \"angle_xy\"; yaxis label \"PDF\" ; G0 TYPE POLAR;  \' '
        xmgrace_save[dataname] =  gracename
    # P_Fn_xz
    elif filename == 'P_Fn_xz.dat' :
        basename = filename.replace('.dat', ''); dataname = basename + '.dat'; gracename = basename + '.agr'; 
        #BXY, TYPE = column_spec(1, 2, filename)
        BXY  = ' -bxy 1:2 '
        TYPE = ' -block '
        xmgrace_data_call[dataname] = TYPE + INPUT_DIR_NAME + os.sep  +  "histograms_F_T" + os.sep + dataname + BXY 
        xmgrace_pexec[dataname] = ' \'s0 legend \"Fn Angle-xz\" ; ' + symbol_format(0) + '; title \"Normal force orientation projected on XZ\"; xaxis label \"angle_xz\"; yaxis label \"PDF\" ; G0 TYPE POLAR;  \' '
        xmgrace_save[dataname] =  gracename
    # P_Fn_yz
    elif filename == 'P_Fn_yz.dat' :
        basename = filename.replace('.dat', ''); dataname = basename + '.dat'; gracename = basename + '.agr'; 
        #BXY, TYPE = column_spec(1, 2, filename)
        BXY  = ' -bxy 1:2 '
        TYPE = ' -block '
        xmgrace_data_call[dataname] = TYPE + INPUT_DIR_NAME + os.sep  +  "histograms_F_T" + os.sep + dataname + BXY 
        xmgrace_pexec[dataname] = ' \'s0 legend \"Fn Angle-yz\" ; ' + symbol_format(0) + '; title \"Normal force orientation projected on YZ\"; xaxis label \"angle_yz\"; yaxis label \"PDF\" ; G0 TYPE POLAR;  \' '
        xmgrace_save[dataname] =  gracename
    # P_Ft_xy
    elif filename == 'P_Ft_xy.dat' :
        basename = filename.replace('.dat', ''); dataname = basename + '.dat'; gracename = basename + '.agr'; 
        #BXY, TYPE = column_spec(1, 2, filename)
        BXY  = ' -bxy 1:2 '
        TYPE = ' -block '
        xmgrace_data_call[dataname] = TYPE + INPUT_DIR_NAME + os.sep  +  "histograms_F_T" + os.sep + dataname + BXY 
        xmgrace_pexec[dataname] = ' \'s0 legend \"Fn Angle-xy\" ; ' + symbol_format(0) + '; title \"Tangential force orientation projected on XY\"; xaxis label \"angle_xy\"; yaxis label \"PDF\" ; G0 TYPE POLAR;  \' '
        xmgrace_save[dataname] =  gracename
    # P_Ft_xz
    elif filename == 'P_Ft_xz.dat' :
        basename = filename.replace('.dat', ''); dataname = basename + '.dat'; gracename = basename + '.agr'; 
        #BXY, TYPE = column_spec(1, 2, filename)
        BXY  = ' -bxy 1:2 '
        TYPE = ' -block '
        xmgrace_data_call[dataname] = TYPE + INPUT_DIR_NAME + os.sep  +  "histograms_F_T" + os.sep + dataname + BXY 
        xmgrace_pexec[dataname] = ' \'s0 legend \"Fn Angle-xz\" ; ' + symbol_format(0) + '; title \"Tangential force orientation projected on XZ\"; xaxis label \"angle_xz\"; yaxis label \"PDF\" ; G0 TYPE POLAR;  \' '
        xmgrace_save[dataname] =  gracename
    # P_Ft_yz
    elif filename == 'P_Ft_yz.dat' :
        basename = filename.replace('.dat', ''); dataname = basename + '.dat'; gracename = basename + '.agr'; 
        #BXY, TYPE = column_spec(1, 2, filename)
        BXY  = ' -bxy 1:2 '
        TYPE = ' -block '
        xmgrace_data_call[dataname] = TYPE + INPUT_DIR_NAME + os.sep  +  "histograms_F_T" + os.sep + dataname + BXY 
        xmgrace_pexec[dataname] = ' \'s0 legend \"Fn Angle-yz\" ; ' + symbol_format(0) + '; title \"Tangential force orientation projected on YZ\"; xaxis label \"angle_yz\"; yaxis label \"PDF\" ; G0 TYPE POLAR;  \' '
        xmgrace_save[dataname] =  gracename
    # crystalized
    elif filename == 'crystalized.dat' :
        basename = filename.replace('.dat', ''); dataname = basename + '.dat'; gracename = basename + '.agr'; 
        BXY, TYPE = column_spec(1, 2, filename)
        xmgrace_data_call[dataname] = TYPE + INPUT_DIR_NAME + os.sep  + dataname + BXY
        xmgrace_pexec[dataname] = ' \'s0 legend \"Crystallized flag (0 or 1)\"; ' + symbol_format(0) + 'title \"Crystallized flag as a function of time\"; xaxis label \"time (s)\"; yaxis label \"Crystallized flag\" \' '
        xmgrace_save[dataname] =  gracename
    else :
        basename = filename.replace('.dat', ''); dataname = basename + '.dat'; gracename = basename + '.agr'; 
        BXY, TYPE =  column_spec(1, 2, filename)
        xmgrace_data_call[dataname] = TYPE + INPUT_DIR_NAME + os.sep  + dataname + BXY
        xmgrace_pexec[dataname] = ' \'s0 legend \"' + filename + '\"; ' + symbol_format(0) + '; title \"'+ basename + '\"; xaxis label \"x axis\"; yaxis label \"y axis\" \' '
        xmgrace_save[dataname] =  gracename


###############################################################
# process the files
for filename in file_names :
    print("Procesing filename : {0:30}".format(filename))
    try :
        set_options(filename)
    except :
        print "File not found.\n"
        continue
    save_filename = OUTPUT_DIR_NAME + os.sep + xmgrace_save[filename]
    cmd = 'gracebat -nosafe -hardcopy -noprint ' + xmgrace_data_call[filename] + ' -pexec ' + xmgrace_pexec[filename] + ' -pexec \'autoscale\' -saveall ' + save_filename
    cmd_file = open(save_filename.replace('.agr', '.txt'), 'w'); cmd_file.write(cmd + '\n'); cmd_file.close()
    os.system(cmd)
    os.system('gracebat -nosafe ' + save_filename + ' -hardcopy -hdevice PDF -printfile ' + save_filename.replace('.agr', '.pdf') + ' -pexec \'autoscale\' -pexec \'DEVICE \"PDF\" FONT ANTIALIASING on\' ')
    #os.system('gracebat -nosafe ' + save_filename + ' -hardcopy -hdevice PNG -printfile ' + save_filename.replace('.agr', '.png') + ' -pexec \'autoscale\' -pexec \'DEVICE \"PNG\" FONT ANTIALIASING on; DEVICE \"PNG\" OP \"transparent:on\"\' ')
    os.system('gracebat -nosafe ' + save_filename + ' -hardcopy -hdevice JPEG -printfile ' + save_filename.replace('.agr', '.jpg') + ' -pexec \'autoscale\' -pexec \' PAGE SIZE 1280, 960; DEVICE "JPEG" DPI 200; DEVICE "JPEG" FONT ANTIALIASING on; DEVICE \"JPEG\" OP \"optimize:on\"; DEVICE \"JPEG\" OP \"quality:100\"\' ')
    print("")
