#!/bin/bash

if [[ "$#" -ne "1" ]]; then
    echo "ERROR."
    echo "USAGE: $0 SUFFIX"
    echo "SUFFIX = Simulation name, like \"-SSHEAR\" "
    echo "Exiting ..."
    exit 1
fi
SUFFIX=$1

function check_command_status () {
    if [[ $1 != 0 ]]; then 
	echo "Error running: $2"; exit 1
    fi
}

echo
echo "Backing up simul config files ..."
SUF="-AGITATION"; 
for a in INPUT/*$SUF; do 
    bname=${a%$SUF}; 
    cp -vf $bname ${bname}$SUFFIX; 
done; 

echo
echo "Running prepro ..."
prepro_initializer.x $PWD &> output-prepro.txt; 
check_command_status $? "PREPRO"
copy_final_to_initial.sh; 

echo
echo "Copy AGITATION files ..."
SUF="-AGITATION"; 
for a in INPUT/*$SUF; do 
    bname=${a%$SUF}; 
    cp -vf $a $bname; 
done; 

SUF="-AGITATION"; 
echo
echo "Running AGITATION ... Saving to output${SUF}.txt"
time dem.x AGITATION $PWD &> output${SUF}.txt; 
check_command_status $? "AGITATION"
copy_final_to_initial.sh ; 

echo "Sleeping for 10 seconds ..."
sleep 10

SCALE=${SCALE=1} # default is to scale
if [[ "1" == "$SCALE" ]]; then  
    echo
    echo "Scaling to delete contacts ..."
    time helper_remove_penetrations_by_scaling.x &> output-SCALING.txt ; copy_final_to_initial.sh ; 
    check_command_status $? "SCALING"
fi

FREEZE=${FREEZE=1} # default is to freeze
if [[ "1" == "$FREEZE" ]]; then  
    echo
    echo "Running Freezer ..."
    time helper_freezer.x &> output-FREEZER.txt ; copy_final_to_initial.sh ; 
    check_command_status $? "FREEZER"
fi

MIX_MATERIAL=${MIX_MATERIAL=0} # default is not to use mixing
if [[ "1" == "$MIX_MATERIAL" ]]; then  
    if [[ "0" == "$MODE" || "1" == "$MODE" || "2" == "$MODE" ]]; then  
	FRACTION=${FRACTION=0.5}
	MATERIAL_TAG0=${MATERIAL_TAG0=0}
	MATERIAL_TAG1=${MATERIAL_TAG1=2}
	echo
	echo "Running MIXED MATERIAL WIT MODE = $MODE, MATERIAL_TAG1=$MATERIAL_TAG1, MATERIAL_TAG2=$MATERIAL_TAG2 ..."
	echo "FRACTION = $FRACTION ..."
	time helper_material_mixing.x $MODE $MATERIAL_TAG0 $MATERIAL_TAG1 $FRACTION  &> output-materialmix.txt ; copy_final_to_initial.sh ; 
	check_command_status $? "MIX_MATERIAL"
    else 
	echo "Error. MODE = $MODE not accepted"
	exit 1
    fi
fi

echo
echo "Setting up $SUFFIX ..."
for a in INPUT/*$SUFFIX; do 
    bname=${a%$SUFFIX}; 
    cp -f $a $bname; 
done; 

echo 
echo "Running $SUFFIX ... Saving to output${SUFFIX}.txt"
time dem.x $SUFFIX $PWD &>output${SUFFIX}.txt
check_command_status $? "DEM.X"

echo "#######################################################################"
echo "DONE."
