import netCDF4 as nc
from glob import glob
import sys
import random as ran
import numpy as np

# check arguments and print usage
if len(sys.argv) != 3 :
    print "Error: Wrong number of arguments. Usage:"
    print sys.argv[0] + " input output "
    print "input : Netcdf Filename to be processed"
    print "output : Text file to be written with the contents Rx Ry Rz r for each particle"
    exit(1)

# read particle file
try : 
    fbodies = nc.Dataset(sys.argv[1], 'r') 
except :
    print "Error opening input datafile. Exiting."
    exit(1)

# Write data
nbodies = len(fbodies.variables['Rx'][:])
data = np.column_stack((fbodies.variables['Rx'][:], fbodies.variables['Ry'][:], fbodies.variables['Rz'][:], fbodies.variables['rad'][:]))
np.savetxt(sys.argv[2], data, header="# N = %d\n# Rx  Ry   Rz  rad"%(nbodies))

