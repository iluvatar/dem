#!/bin/bash

# This scripts makes the png snapshots of the systems
echo "Creating povray pngs and video."

DIR=${1:-./}
OLD_DIR=$PWD
cd $DIR

postpro_display_povray.x
povray_to_png.sh
pngs_to_avi.sh

cd $OLD_DIR

echo "Done."
