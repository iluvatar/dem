#!/bin/bash

#if [ "$#" -ne "2" ]; then
#    echo "ERROR"
#    echo "Usage : $0 DirWithPovFiles DirToSavePngFiles"
#    exit 1;
#fi

#POV_FILES_DIR=${1}
#PNG_OUT_DIR=${2}
POV_FILES_DIR="DISPLAY/"
PNG_OUT_DIR="DISPLAY/"

REPO_DIR="$(dirname $(readlink $0))/../"

mkdir ./tmp
for a in $POV_FILES_DIR/*pov; 
do 
    BASE=$( basename $a .pov);
    outFileName=${BASE}.png
    fullOutFileName=${PNG_OUT_DIR}/$BASE.png
    if [ ! -e $fullOutFileName ]; then
	echo "Procesing $a --> $fullOutFileName";
	#povray -D0   +L$(dirname $a) +I$a -P +H480 +W640 +O/tmp/$outFileName;
        povray -D0 +L"$REPO_DIR"  +L$(dirname $a) +I$a -P +H960 +W1280 +Q11 +A +O./tmp/$outFileName;
	mv ./tmp/$outFileName $fullOutFileName; 
    fi
done
rm -rf ./tmp

# Cambiar los valores que aconpanhan a H y W para cambiar la resolucion, respetando aspect ratio
