#!/bin/env bash

cwd=$(pwd)

if [[ ! -d "$1" ]]; then
    printf "ERROR.\nDirectory \"$1\" does not exist.\nUsage : $0 simul_dir \n"
    exit 1
fi

cd "$1"
if [[ ! -d "INPUT" || ! -d "OUTPUT" ]]; then
    printf "Directory \"$1\" is not a proper simul directory. Exiting.\n"
    cd $cwd
    exit 1
fi


printf "PROCESSING : $1 \n"
printf "Running prepro_initializer.x ... \n"
prepro_initializer.x
printf "Copying final to initial ...\n"
copy_final_to_initial.sh

printf "Done.\n"

cd $cwd