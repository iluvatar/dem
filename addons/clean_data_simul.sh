#!/bin/bash

echo "ALERT: This script will delete the contents of directories OUTPUT/ , POSTPRO/, and DISPLAY/, and renames INPUT/bodiesInfo.nc and INPUT/contactsInfo.nc "
echo "Do you want to continue? (yes/no) "

read answer

if [[ "yes" = $answer || "YES" = $answer ]]; then
    rm -rf OLD_previous_OUTPUT
    rm -rf previous_OUTPUT
    rm -rf OUTPUT/*
    rm -rf POSTPRO/*
    rm -rf POSTPRO_from*
    rm -rf DISPLAY/*
    mv INPUT/bodiesInfo.nc INPUT/bodiesInfo.nc.old
    mv INPUT/contactsInfo.nc  INPUT/contactsInfo.nc.old
    rm -f EXIT_STATUS_OK
    echo "DONE."
fi

