#!/bin/bash

if [[ "1" != "$#" ]]; then
    printf "ERROR. \nUsage: $0 filename.txt \n"
    exit 1
fi

echo "Processing file :  $1"

if [ ! -e "$1" ]; then
    echo "ERROR: File $1 does not exist. "
    exit 1
fi

NC_NAME="${1%.txt}.nc"
if [ ! -e "$NC_NAME" ]; then
    if [[ ${NC_NAME} == *bodies* ]]; then 
	bodies_from_txt_to_nc.x "$1" "${NC_NAME}"
    elif [[ ${NC_NAME} == *contacts* ]]; then 
	contacts_from_txt_to_nc.x "$1" "${NC_NAME}"
    else 
	echo "File $1 cannot be translated. Exiting. "
	exit 1
    fi
else 
    echo "netCDF file already exists. "
fi

echo "Compressing and deleting original file ..."
tar cvjf "${1}.tar.bz2" "${1}" && rm -f "${1}"
 
echo "Done."