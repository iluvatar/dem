#!/usr/bin/env bash
    
echo "This script updates the old walls_conf.yaml file to the new syntax which includes several control functions. By default, a ramp is chosen. PLEASE DOUBLE CHECK THE FINAL FILE."


# walls_conf.yaml
mv INPUT/walls_conf.yaml INPUT/walls_conf.yaml.bck
awk 'BEGIN {FS=","} {if ($2 == "")  print $0; else printf "%s, 0, 0.0, %e, %d]\n", $1, $2, $3} END {}' INPUT/walls_conf.yaml.bck > INPUT/walls_conf.yaml

echo "
# For each wall, and for each direction we have:
# Index 0 : 0/1/2 = free/velocity/stress controlled wall
# Index 1 : 0/1/2 = RAMP/OSC/LADDER = Ramp, or Harmonic, or ladder function
# The remaining values in the array correspond to the parameters for each function
#
# For the RAMP function, three parameters are needed : Min, Max, Nramp, and it is defined as
# if iter < Nramp then return (iter/Nramp)*(Max-Min) + Min
# else return Max
#
# For the OSC function, four parameters are needed : A, Omega, Delta, A0, and it is defined as
# A*sin(Omega*iter + delta) + A0
# NOTE : Omega is defined in time steps, not actual time. You can easily compute it from the desired
# Period and Delta (time step) as : Omega = 2\pi  Deltat /Period
#
# For the LADDER function, five parameters are needed : Min, Max, Nsteps, Nramp, Nladder. It correspond
# to a ladder of nsteps, starting from Min ad arriving to Max. The duration of each step is equal to
# Nramp/Nsteps . If Nladder is > 1, the ladder is repeated with opposite slope (it it was increasing,
# then it will be decreasing)
#
" >> INPUT/walls_conf.yaml

echo "Old file written to : INPUT/walls_conf.yaml.bck "
echo "New file written to : INPUT/walls_conf.yaml "
echo "################### DONE ##################"
