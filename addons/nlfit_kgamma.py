#!/usr/bin/env python

# imports
import sys, os
import numpy as np
from scipy.optimize import curve_fit
from scipy.special import gamma
from math import exp

# function to be fitted
#def kgamma(x, k, chi, vm) :
def kgamma(x, chi, vm) :
    k = 1.89
    nchi = chi/100.0 # to scale to one
    nvm = vm/10.0  # to scale to one
    var = (x-nvm)/nchi
    if (var <= 0).any() or k < 0:
        return 0
    else:
        return np.exp((k-1.0)*np.log(var)-var)/(nchi*gamma(k))
    
# parse the command line arguments
try:
    DATAFILE = sys.argv[1]
    OUTFILE  = sys.argv[2]
    K0    = float(sys.argv[3]) 
    CHI0  = float(sys.argv[4])*100
    VM0   = float(sys.argv[5])*10
except:
    print('Error')
    print('Usage: ' + sys.argv[0] + ' datafile outfile K0 CHI0 VM0')
    print('datafile  : File with the x and y data to be fitted by the kgamma function.')
    print('outfile   : File to write the output, in two colums (val and error), in the following order: k, chi, and vm')
    sys.exit(1)
    
# read the datafile specified as argument
try:
    x, y = np.loadtxt(DATAFILE, unpack=True)
except Exception, e:
    print e

# fit the data
p0=np.array([K0, CHI0, 1.0]) # ignores VM0
#p0=np.array([CHI0, 1.0]) # ignores VM0
popt, pcov = curve_fit(kgamma, x, y, p0)
# rescale back
popt[1] /= 100.0
popt[2] /= 10.0
pcov[1,1] /= 100.0
pcov[2,2] /= 10.0

# print to the output file
results=np.column_stack((popt, pcov.diagonal()));
print results
try:
    np.savetxt(OUTFILE, results, fmt='%28.16e')
except Exception, e:
    print e

