#!/bin/bash
printf "Sending the command\n--> $@ \nto run in background. Using\n--> $1 \nfor output filename ...\n"
nohup time "$@" 1> "$1"-stdout.txt 2> "$1"-stderr.txt & 
pid=$!
touch pid.$pid
printf "Done sending.\nPID = $pid\n" 