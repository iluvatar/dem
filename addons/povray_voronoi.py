#!/usr/bin/env python

import os, sys
import numpy as np

try:
    PARTICLES_FILE = sys.argv[1]
    CELLS_FILE = sys.argv[2]
    OUT_FILE = sys.argv[3]
except:
    print('Error')
    print('Usage: ' + sys.argv[0] + ' particle_file.pov cell_file.pov out_file.pov')
    sys.exit(1)

if not (os.path.exists(PARTICLES_FILE) and os.path.exists(CELLS_FILE)):
    print ('Files do not exist')
    sys.exit(1)

# read the header povray file
with open('../inc/import.pov', 'r') as f:
    data = f.read()
    data = data.replace("../simul/DISPLAY/voronoi_p.pov", PARTICLES_FILE)
    data = data.replace("../simul/DISPLAY/voronoi_c.pov", CELLS_FILE)
f.close()

# printing
if 'DISPLAY/' not in OUT_FILE:
    OUT_FILE = 'DISPLAY/' + OUT_FILE
with open(OUT_FILE, 'w') as f:
    f.write(data)
f.close()
print('Wrote to ' + OUT_FILE)

# process with povray
os.system('povray +W800 +H600 +O' + OUT_FILE.replace('.pov', '.png') + ' ' + OUT_FILE)
    
