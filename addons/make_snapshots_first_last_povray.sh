#!/bin/bash

# makes the snapshots for the first and last bodies output
echo "Creating povray snapshots of the first and last output"

OLD_DIR=$PWD
DIR=${1:-./}
cd $DIR

POSTPRO_CONF="INPUT/postpro_conf.yaml"

mv OUTPUT OUTPUT.orig
mkdir OUTPUT
cp -f ${POSTPRO_CONF} ${POSTPRO_CONF}.orig
# include all files by increasing a lot the time window
sed -i.orig 's/tmin.*/tmin : 0.0 /' $POSTPRO_CONF
sed -i.orig 's/tmax.*/tmax : 1000.0 /' $POSTPRO_CONF

first=$(ls OUTPUT.orig/bodiesInfo-* 2>/dev/null | head -n 1)
last=$(ls OUTPUT.orig/bodiesInfo-* 2>/dev/null | tail -n 1)
cp $first OUTPUT/
cp $last OUTPUT/

rm -f DISPLAY/povray*png
postpro_display_povray.x
povray_to_png.sh


rm -rf OUTPUT
mv OUTPUT.orig OUTPUT
rm -f ${POSTPRO_CONF}
mv ${POSTPRO_CONF}.orig ${POSTPRO_CONF}

cd $OLD_DIR
echo "Done" 