import netCDF4 as nc
from glob import glob
import sys
import random as ran
#import numpy as np

# check arguments and print usage
if len(sys.argv) != 4 :
    print "Error: Wrong number of arguments. Usage:"
    print sys.argv[0] + " filename Vmin Vmax"
    print "filename : Netcdf Filename to be processed"
    print "Vmin : Minimum Velocity"
    print "Vmax : Maximum velocity"
    exit(1)

# read particle file
fbodies = nc.Dataset(sys.argv[1], 'r+') # modify in place

# read velocity limits
VMIN = float(sys.argv[2])
VMAX = float(sys.argv[3])

# get lmin and lmax for z
nbodies = len(fbodies.variables['Rx'][:])
ngrains = nbodies - 6
BO = ngrains + 0
TO = ngrains + 1
LE = ngrains + 2
RI = ngrains + 3
BA = ngrains + 4
FR = ngrains + 5
LMIN = [fbodies.variables['Rx'][LE], fbodies.variables['Ry'][BA], fbodies.variables['Rz'][BO]] ; #LMIN = float(LMIN)
LMAX = [fbodies.variables['Rx'][RI], fbodies.variables['Ry'][FR], fbodies.variables['Rz'][TO]] ; #LMAX = float(LMAX)
print 'LMIN = ' + str(LMIN)
print 'LMAX = ' + str(LMAX)

# Set the linear velocity profile
for i in range(ngrains) :
    fbodies.variables['Vx'][i] = ((VMAX-VMIN)/(LMAX[2]-LMIN[2]))*(fbodies.variables['Rz'][i] - LMIN[2]) + VMIN

# print particle file
fbodies.sync()
fbodies.close()

# finish
print "#"*20
print "Done."
