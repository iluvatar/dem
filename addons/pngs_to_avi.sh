#!/bin/bash

#if [ "$#" -ne "1" ]; then
#    echo "ERROR"
#    echo "Usage : $0 DirWithPngFiles"
#    exit 1;
#fi

#INPUT_DIR=$1
INPUT_DIR="DISPLAY/"

#VCODEC=mpeg4
VCODEC=msmpeg4v2    # windows ready
#VCODEC=mpeg2video   # windows ready

# fps controla los frames per second 
mencoder "mf://${INPUT_DIR}/*.png" -mf fps=20 -o DISPLAY/output.avi -ovc lavc -lavcopts vcodec=${VCODEC}

echo -e " \033[1;34m Video saved in DISPLAY/output.avi  \033[0m "
echo -e " \033[1;34m You can reproduce your video with the command : \033[0m  "
echo -e " \033[1;34m mplayer DISPLAY/output.avi  \033[0m "
