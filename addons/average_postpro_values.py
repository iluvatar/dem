#!/usr/bin/env python

import os, sys, fnmatch, shutil
import numpy as np
from scipy import stats
from math import sqrt
import subprocess as SP

def help():
    print('Usage:\n' + sys.argv[0] + ' BASE_DIR REF_SUBDIR')
    print('BASE_DIR   : Directory containing the set of simulations which differ in one parameter.')
    print('REF_SUBDIR : Posible subdirectory which contains the files to be averaged.')
    print('Structure needed: BASE_DIR/param_0/REF_SUBDIR , BASE_DIR/param_1/REF_SUBDIR, etc, where param_i is a full simulation with its own REF_SUBDIR subdir.')

try: 
    REF_SUBDIR = sys.argv[2]
except:
    print('ERROR.\n'); help(); sys.exit(1)
if REF_SUBDIR != "./": 
    REF_SUBDIR=REF_SUBDIR.replace('/', '')
print('REF_SUBDIR name = ' + REF_SUBDIR)


try:
    ROOT_PATH = sys.argv[1]
except:
    print('ERROR.\n'); help(); sys.exit(1)

if not os.path.isdir(ROOT_PATH):
    print ('Path : ' + ROOT_PATH + ' does not exist')
    sys.exit(1)


#######################################################
# utility functions

# run command on shell and return output
def run_shell_command(command):
    pcom = SP.Popen(command, shell=True, stdout=SP.PIPE, stderr=SP.PIPE)
    stdout, stderr = pcom.communicate()
    if stderr != "":
        print("Error running command :\n" + command)
        print("STDERR : " + stderr)
        sys.exit(1)
    return stdout, stderr

# get the file list from first directory if none specified: pattern extension is .dat
def get_file_list() :
    file_list = sys.argv[3:]  
    if 0 == len(file_list) : # if not explicitly stated
        if REF_SUBDIR != "./":
            extra=' -iname ' + REF_SUBDIR
        else:
            extra=' -maxdepth 2 '
        cmd = 'find -L ' + ROOT_PATH + ' -type d ' + extra + ' | grep -v AVERA | grep -v from | head -n 2 | tail -n1'
        print("Executing command : " + cmd) 
        stdout, stderr = run_shell_command(cmd)
        dirname = str(stdout.split()[0])
        if "" == dirname:
            print('ERROR: Subsubdir ' + REF_SUBDIR + ' does not exist')
            sys.exit(1)
        cmd = 'ls ' + dirname + os.sep + '*.dat | grep -v multivoro | grep -v histograms' 
        print("Executing command : " + cmd) 
        stdout, stderr = run_shell_command(cmd)
        stdout = stdout.replace(dirname + os.sep, '')
        file_list = map(str,stdout.split())
    return sorted(file_list, key=str.lower)

#######################################################
# main function
files_names = get_file_list()
print 'file_list = ' + str(files_names)
# create the output directories
PARAM_NAME='PARAM-AVERAGED'
OUTPUT_DIR_NAME = ROOT_PATH + os.sep + PARAM_NAME
REF_SUBDIR_DIR_NAME = OUTPUT_DIR_NAME + os.sep + REF_SUBDIR
if os.path.exists(OUTPUT_DIR_NAME):
    shutil.rmtree(OUTPUT_DIR_NAME)
os.makedirs(REF_SUBDIR_DIR_NAME)

# process file by file
for base_filename in files_names :
    print("\nProcesing filename : {0:30}".format(base_filename))
    # get the files with the pattern
    files = []
    cmd = 'find -L ' + ROOT_PATH + os.sep + '*/' + REF_SUBDIR + ' -maxdepth 3 -type f -iname ' + base_filename + ' | grep -v AVERA | grep -v from'
    stdout, stderr = run_shell_command(cmd)
    files=stdout.split()

    # process each file
    if len(files) > 0:
        print("Number of files to be processed : {0:30}".format(len(files)))
        N = 0
        for datafile in files[0:]: 
            if os.stat(datafile).st_size == 0:
                continue
            try:
                data = np.nan_to_num(np.loadtxt(datafile, ndmin=2))
                np.putmask(data, data > 1.0e150, 1.0e150) # replace large values to allow squaring
                np.putmask(data, data < -1.0e150, -1.0e150) # replace large values to allow squaring
            except IOError:
                data = np.array([[0]])
            except Exception, e:
                print e
                continue
            if N == 0:
                mean = data
                mean2 = np.nan_to_num(data**2)
            N += 1
            # reshaping in case data is not uniform
            if N >= 2 : 
                if mean.shape[0] < data.shape[0]:
                    data = np.delete(data, np.s_[mean.shape[0]:], 0)
                if mean.shape[1] < data.shape[1]:
                    data = np.delete(data, np.s_[mean.shape[1]:], 1)
                if data.shape[0] < mean.shape[0]:
                    mean  = np.delete(mean,  np.s_[data.shape[0]:], 0)
                    mean2 = np.delete(mean2, np.s_[data.shape[0]:], 0)
                if data.shape[1] < mean.shape[1]:
                    mean  = np.delete(mean,  np.s_[data.shape[1]:], 1)
                    mean2 = np.delete(mean2, np.s_[data.shape[1]:], 1)
                mean  += data
                mean2 += data**2
        if N == 0 :
            N = 1
            mean  = np.array([[0]])
            mean2 = np.array([[0]])
        mean  /= N
        mean2 /= N
        # extract t-student trust interval 
        sigma  = np.sqrt(np.absolute((mean2 - mean*mean)/N))
        # sigma *= stats.t.interval(0.99, N)[1]/sqrt(N)   # 0.99 trust interval
        # print t-student merged data
        values = np.hstack((mean, sigma))
        np.savetxt(REF_SUBDIR_DIR_NAME + os.sep + base_filename, np.nan_to_num(values))
        # extract normal sigma  
        sigma  = np.sqrt(np.absolute((mean2 - mean*mean)))
        # print normal merged data
        values = np.hstack((mean, sigma))
        np.savetxt(REF_SUBDIR_DIR_NAME + os.sep + base_filename + '-normal', np.nan_to_num(values))
        print 'Done averaging for : ' + base_filename
    else :
        print "No files to analyze. Probably bad file name. Exiting."
        sys.exit(1)

# compress the new files
#os.system('cd ' + ROOT_PATH + '; rm -f ' + PARAM_NAME + '.zip; zip -r ' + PARAM_NAME + '.zip ' + PARAM_NAME)
