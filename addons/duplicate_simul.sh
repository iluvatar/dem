#!/bin/bash
echo "This script duplicates the config files from a simulation to another"

if [[ "$#" != "2" ]]; then
    echo "ERROR"
    echo "Usage: $0 origin destiny"
    exit 1
fi

DIRS="INPUT OUTPUT DISPLAY POSTPRO"
# create dirs
for a in $DIRS; do 
    mkdir "$2/$a" &> /dev/null
done
# copy the INPUT data
cp -af "$1"/INPUT/* "$2"/INPUT/

echo "#############################################"
echo "Done."