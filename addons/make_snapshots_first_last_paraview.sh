#!/bin/bash

# makes the snapshots for the first and last bodies output
echo "Creating povray snapshots of the first and last output"

OLD_DIR=$PWD
DIR=${1:-./}
cd $DIR

POSTPRO_CONF="INPUT/postpro_conf.yaml"

mv OUTPUT OUTPUT.orig
mkdir OUTPUT
cp -f $POSTPRO_CONF ${POSTPRO_CONF}.orig
# include all files by increasing a lot the time window
sed -i.orig 's/tmin.*/tmin : 0.0 /' $POSTPRO_CONF
sed -i.orig 's/tmax.*/tmax : 1000.0 /' $POSTPRO_CONF
sed -i.orig 's/stride.*/stride : 1 /' $POSTPRO_CONF

firstB=$(ls OUTPUT.orig/bodiesInfo-* 2>/dev/null | head -n 1)
lastB=$(ls OUTPUT.orig/bodiesInfo-* 2>/dev/null | tail -n 1)
firstC=$(ls OUTPUT.orig/contactsInfo-* 2>/dev/null | head -n 1)
lastC=$(ls OUTPUT.orig/contactsInfo-* 2>/dev/null | tail -n 1)
cp $firstB OUTPUT/
cp $lastB OUTPUT/
cp $firstC OUTPUT/
cp $lastC OUTPUT/
cp OUTPUT.orig/{bodies,contacts}Info.{nc,txt} OUTPUT/

rm -rf DISPLAY/data-*.vtp
postpro_display_paraview.x
for a in $(ls DISPLAY/data-*.vtp | grep -v contacts); do
    make_paraview_snapshot.py $a
done

rm -rf OUTPUT
mv OUTPUT.orig OUTPUT
mv -f ${POSTPRO_CONF}.orig $POSTPRO_CONF

cd $OLD_DIR
echo "Done" 
