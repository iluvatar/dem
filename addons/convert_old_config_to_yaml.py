#!/usr/bin/env python


import os
import sys
import subprocess as sp


def print_info():
    """This function prints the goal of the current script"""
    print("This script converts the old config files into the new yaml "
          "syntax. PLEASE DOUBLE CHECK THE FINAL FILES.")

# general_conf.yaml
ofile = open("INPUT/general_conf.yaml", 'w')
# general_conf
ofile.write("# general configuration\n")
lines = [line.strip().split() for line in open("INPUT/general_conf")]
ofile.write("dim : " + lines[0][0] + "\n")
ofile.write("periodic : [" + lines[1][0] + ", " + lines[1][1] + ", " +
            lines[1][2] + "]" + "\n")
ofile.write("time0 : " + lines[2][0] + "\n")
ofile.write("dt : " + lines[3][0] + "\n")
ofile.write("niter : " + lines[4][0] + "\n")
ofile.write("nprint : " + lines[5][0] + "\n")
ofile.write("run_until_relaxed : " + lines[6][0] + "\n")
ofile.write("Gravity : [" + lines[7][0] + ", " + lines[7][1] + ", " +
            lines[7][2] + "]" + "\n")
ofile.write("GLOBAL_DAMPING_GRAINS : " + lines[8][0] + "\n")
ofile.write("GLOBAL_DAMPING_WALLS : " + lines[8][0] + "\n")
ofile.write("WALLS_MAX_VEL : " + lines[9][0] + "\n")
if len(lines) >= 11:
    ofile.write("PHI_THRESHOLD : " + lines[10][0] + "\n")
else:
    ofile.write("PHI_THRESHOLD : " + str(0.99) + "\n")
# runtime_conf
ofile.write("\n# runtime configuration\n")
lines = [line.strip().split() for line in open("INPUT/runtime_conf")]
ofile.write("KEEP_RUNNING : " + lines[0][0] + "\n")
ofile.write("REREAD_STEPS : " + lines[1][0] + "\n")
# global_dof_conf
ofile.write("\n# global dof configuration\n")
try:
    lines = [line.strip().split() for line in open("INPUT/global_dof_conf")]
    ofile.write("TYPE01 : " + lines[0][0] + " # biaxial\n")
    ofile.write("TYPE02 : " + lines[1][0] + " # isotropic\n")
except:
    ofile.write("TYPE01 : " + str(0) + " # biaxial\n")
    ofile.write("TYPE02 : " + str(0) + " # isotropic\n")
# activation of particles: frecuency
ofile.write("\n#activation of particles: frecuency\n")
ofile.write("nactivation : 200000\n")
# close file
ofile.close()

# materials_conf.yaml
ofile = open("INPUT/materials_conf.yaml", 'w')
lines = [line.strip().split() for line in open("INPUT/materials_conf")]
for ii in range(1, int(lines[0][0])+1):
    ofile.write("- [" + lines[ii][0] + ', ' + lines[ii][1] + ', ' +
                lines[ii][2] + ', ' + lines[ii][3] + ', ' + lines[ii][4] +
                ', ' + lines[ii][5] + ',' + lines[ii][0] + "]\n")
ofile.close()

# postpro_conf.yaml
ofile = open("INPUT/postpro_conf.yaml", 'w')
lines = [line.strip().split() for line in open("INPUT/postpro_conf")]
ofile.write("tmin : " + lines[0][0] + "\n")
ofile.write("tmax : " + lines[0][1] + "\n")
ofile.write("SCREEN_FACTOR : " + lines[1][0] + "\n")
ofile.write("NC_MIN : " + lines[2][0] + "\n")
ofile.write("stride : 1  # Process every stride files\n")
ofile.write("PROJECTION_EPS : 0.08716  # Projection on plane tolerance"
            "(cosine of angle less than this parameter)\n")
PDF_NAMES = ["P_Fn", "P_Ft", "P_T", "P_F_xy", "P_F_xz", "P_F_yz", "P_Fn_xy",
             "P_Fn_xz", "P_Fn_yz", "P_Ft_xy", "P_Ft_xz", "P_Ft_yz",
             "P_contact_xy", "P_contact_xz", "P_contact_yz", ]
for ii in range(0, len(PDF_NAMES)):
    ofile.write(PDF_NAMES[ii] + " : [" + lines[3+ii][0] + ", " +
                lines[3+ii][1] + ", " + lines[3+ii][2] + "]\n")
ofile.write("VMIN_THRESHOLD : 0.98  # A cell volume is accepted only"
            " when larger than VMIN_THRESHOLD*Vmin")
ofile.close()

# prepro_conf.yaml
ofile = open("INPUT/prepro_conf.yaml", 'w')
lines = [line.strip().split() for line in open("INPUT/prepro_init_conf")]
NAMES = ["ngrains", "ncolumns", "nrows", "nyboxes", "voxel_size", "granu1",
         "granu2", "V0", "W0", "seed", "GEOMETRIC_DENSIFY", "NTRIAL",
         "SPECIAL_TRANSF", "VOXEL_DISORDER", "WALL_MASS_SCALE"]
# assert(len(NAMES) == len(lines))
for ii in range(0, len(lines)):
    if 5 == ii or 6 == ii:
        ofile.write(NAMES[ii] + " : [" + lines[ii][0] + ", " + lines[ii][1] +
                    ", " + lines[ii][2] + "]\n")
    else:
        ofile.write(NAMES[ii] + " : " + lines[ii][0] + "\n")
# ofile.write("WALL_MASS_SCALE : 5\n") # default value
ofile.close()

# walls_conf.yaml
ofile = open("INPUT/walls_conf.yaml", 'w')
lines = [line.strip().split() for line in open("INPUT/walls_conf")]
WALLS_NAMES = ["BOTTOM", "TOP", "LEFT", "RIGHT", "BACK", "FRONT"]
# print material tags
ofile.write("MATERIAL_TAG:\n")
for ii in range(0, len(WALLS_NAMES)):
    ofile.write("  " + WALLS_NAMES[ii] + " : " + lines[ii+1][0] + "\n")
# Boundary conditions : in latest syntax
ofile.write("BOUNDARY:\n")
for ii in range(0, len(WALLS_NAMES)):
    ofile.write("  " + WALLS_NAMES[ii] + " :\n")
    ofile.write("    x : [" + lines[8 + 4*ii + 1][0] + ", 0, 0.0, " +
                lines[8 + 4*ii + 1][1] + ", " + lines[8 + 4*ii + 1][2] + "]\n")
    ofile.write("    y : [" + lines[8 + 4*ii + 2][0] + ", 0, 0.0, " +
                lines[8 + 4*ii + 2][1] + ", " + lines[8 + 4*ii + 2][2] + "]\n")
    ofile.write("    z : [" + lines[8 + 4*ii + 3][0] + ", 0, 0.0, " +
                lines[8 + 4*ii + 3][1] + ", " + lines[8 + 4*ii + 3][2] + "]\n")
# close file
ofile.close()

# final message converting
print ("#"*50)
print ("Done converting old config to yaml. DO NOT FORGET TO CHECK IT. ")

# Updating materials yaml config
print ("#"*50)
print ("Updating yaml materials config through scripts")
# get the current script directory
dname = os.path.dirname(os.path.realpath(sys.argv[0]))
#print(dname)
#dname = "/home/oquendo/repos/dem-worskpace/dem/addons/"
#print(dname)
# call the scripts
status_mat = sp.call(dname+"/update_materials_conf_yaml.py")
if status_mat != 0:
    print ("ERROR RUNNING update_materials_conf_yaml.py .")
    print ("PLEASE RUN IT BY YOURSELF, IT IS MANDATORY.")
    exit(1)

print ("#"*50)
print ("Done updating yaml. DO NOT FORGET TO CHECK IT. ")
print ("#"*50)
