import netCDF4 as nc
from glob import glob
import sys
import random as ran
#import numpy as np

# read particle file
fbodies = nc.Dataset(sys.argv[1], 'r+')

# get lmin and lmax for x and y
nbodies = len(fbodies.variables['Rx'][:])
ngrains = nbodies - 6
BO = ngrains + 0
TO = ngrains + 1
LE = ngrains + 2
RI = ngrains + 3
BA = ngrains + 4
FR = ngrains + 5
LMIN = [fbodies.variables['Rx'][LE], fbodies.variables['Ry'][BA], fbodies.variables['Rz'][BO]] ; #LMIN = float(LMIN)
LMAX = [fbodies.variables['Rx'][RI], fbodies.variables['Ry'][FR], fbodies.variables['Rz'][TO]] ; #LMAX = float(LMAX)
print 'LMIN = ' + str(LMIN)
print 'LMAX = ' + str(LMAX)
LMEAN_X = 0.5*(LMIN[0] + LMAX[0])
LMEAN_Y = 0.5*(LMIN[1] + LMAX[1])

# compute maximum diammeter
dmax = 0.0
for i in range(ngrains) :
    if 2.0*fbodies.variables['rad'][i] > dmax :
        dmax = 2.0*fbodies.variables['rad'][i]
print 'dmax = ' + str(dmax)
ZMAX = 6*dmax
print 'ZMAX = ' + str(ZMAX)
        
# put all particles in center x, center y, and increasing z, starting at ngrains*dmax/2
for i in range(ngrains) :
    fbodies.variables['Rx'][i] = fbodies.variables['R0x'][i] = ran.gauss(LMEAN_X, 0.005*LMEAN_X) 
    fbodies.variables['Ry'][i] = fbodies.variables['R0y'][i] = ran.gauss(LMEAN_Y, 0.005*LMEAN_Y)
    fbodies.variables['Rz'][i] = fbodies.variables['R0z'][i] = ZMAX
    fbodies.variables['alive'][i] = 0

# set top wall z coordinate
fbodies.variables['Rz'][TO] = fbodies.variables['Rz'][ngrains-1] + dmax

# print particle file
fbodies.sync()
fbodies.close()

