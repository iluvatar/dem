#!/bin/bash
# This scripts sets the initial T0 time for postpro to a fraction of the final time simulated
printf "This script sets the initial and final time for
the postpro process to be fractions of the total simulated time\n\n"

POSTPRO_CONF="INPUT/postpro_conf.yaml"

# check arguments
if [[ "2" != "$#" ]]; then
    printf "ERROR.\nUsage: %s fraction_init fraction_end\nExiting.\n" $0
    printf "Example: %s 0.1 0.9  => Means start at fraction 0.1 and end at fraction 0.9 of total time.\n" $0
    printf "\nExiting.\n"
    exit 1
fi
result=$(echo "$1 <= 1.0" | bc -l)
if [[ "0" == "$result" ]]; then
    printf "ERROR.\nFraction should be less than 1.\nExiting.\n"
    exit 1
fi
result=$(echo "$1 <= $2" | bc -l)
if [[ "0" == "$result" ]]; then
    printf "ERROR.\nInitial fraction should be less than final.\nExiting.\n"
    exit 1
fi

# detect final time in OUTPUT (using bodiesInfo)
tend=$(ls OUTPUT/bodiesInfo-* | tail -n1)
tend=${tend#*Info-}
tend=${tend%.nc}
tend=${tend%.txt}
if [[ "" == "${tend}" ]]; then 
    printf "ERROR: Impossible to extract final time. Exiting.\n"
    exit 1
fi

# extract current tinit in postpro
tinit_old=$(cat ${POSTPRO_CONF} | grep tmin | awk 'BEGIN {FS="[ \t]*[:#][ \t]*"} {print $2}' | tr -d ' ')
#tinit_old=${tinit_old//[[:blank:]]/}

# extract current tend in postpro
tend_old=$(cat ${POSTPRO_CONF} | grep tmax | awk 'BEGIN {FS="[ \t]*[:#][ \t]*"} {print $2}' | tr -d ' ')
#tend_old=${tend_old//[[:blank:]]/}

# set tinit and tend in postpro_conf.yaml
tinit_new=$(echo $1*$tend | bc -l |  awk '{printf "%.16f", $1 }')
tend_new=$(echo $2*$tend | bc -l | awk '{printf "%.16f", $1 }' )
#tmpa=${tinit_old//\./\\.}
#tmpb=${tend_old//\./\\.}

# print info
printf "Old tinit = \t%s\n" $tinit_old
printf "Old tend  = \t%s\n" $tend_old
printf "New tinit = \t%s\n" $tinit_new
printf "New tend  = \t%s\n" $tend_new
printf "Total time = \t%s\n" $tend

# backup files
#sed -i_from_$tinit_old '1s/'$tmpa'/'$tinit_new'/' ${POSTPRO_CONF}
#sed -i_until_$tend_old '1s/'$tmpb'/'$tend_new'/' ${POSTPRO_CONF}
sed -i_from_${tinit_old}_until_${tend_old} 's/tmin.*/tmin : '$tinit_new' # NEW NEW NEW/' ${POSTPRO_CONF}
sed -i_from_${tinit_old}_until_$tend_old 's/tmax.*/tmax : '$tend_new' # NEW NEW NEW/' ${POSTPRO_CONF}


# backup old POSTPRO data
mv POSTPRO POSTPRO_from_${tinit_old}_until_${tend_old}
mkdir POSTPRO

# exit
echo "Done."
