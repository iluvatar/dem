#!/usr/bin/env bash
# Setup a simulation to be continued 

printf "This script will check for data printed corruption, copy the last output to input, and \nset other variables to allow for continuing a simulation\n"

# Checks
# Check if STARTED file exists
if [[ ! -f STARTED ]]; then
    echo "No STARTED file found. This script is not needed since the simulation has not started yet. Exiting"
    exit 1
fi
# Check if needed commands exits
command -v ncdump >/dev/null 2>&1 || { echo >&2 "Command not found : ncdump . Aborting"; exit 1; }

# delete corrupted netcdf files
echo "Checking corrupted netcdf files ..."
for pattern in bodies contacts; do
    for a in OUTPUT/${pattern}Info-*.nc; do
        ncdump -h $a &> /dev/null;
        if [[ $? != 0 ]]; then
            echo "Moving corrupted file: $a to OUTPUT-CORRUPTED/$a"
	    mkdir OUTPUT-CORRUPTED 2>/dev/null
            mv $a OUTPUT-CORRUPTED/
        fi
    done
done

# delete bodies (contacts) files whose pair contacts (bodies) file was not printed
echo "Moving files which are not printed in pairs (same time for contacts and bodies)"
for pattern in bodies contacts; do
    patternb="contacts";
    if [[ "$pattern" == "contacts" ]]; then
        patternb="bodies";
    fi
    for a in OUTPUT/${pattern}Info-*.nc; do
        b=${a/${pattern}/${patternb}};
        if [[ ! -e ${b} ]]; then
            #echo  $a $b;
	    mkdir OUTPUT-CORRUPTED 2>/dev/null
            mv $a OUTPUT-CORRUPTED/
            mv $b OUTPUT-CORRUPTED/
            #rm -f $a $b;
        fi;
    done;
done

# delete the last file to avoid corruption, if ext == txt
echo "Deleting the last txt file to avoid corruption (not needed for netcdf files)"
for pattern in bodies contacts; do
    for fname in $(ls OUTPUT/${pattern}Info-*.txt | tail -n 1); do
	echo $fname
	rm -f $fname
    done
done

# print the total number of files
echo "Total number of bodies   files (.txt, .nc) : $(ls OUTPUT/bodiesInfo-*.*   | wc -l)"
echo "Total number of contacts files (.txt, .nc) : $(ls OUTPUT/contactsInfo-*.* | wc -l)"

# extract last time from bodiesinfo-
EXT="txt"
tend=$(ls OUTPUT/bodiesInfo-*.${EXT} 2>/dev/null | tail -n 1);
if [[ "$tend" == "" ]]; then 
    EXT="nc"
    tend=$(ls OUTPUT/bodiesInfo-*.${EXT} 2>/dev/null | tail -n 1);
fi
if [[ "$tend" == "" ]]; then 
    echo "There are no time output files. "
    echo "Deleting START and EXIT_STATUS_OK files if present"
    rm -f STARTED EXIT_STATUS_OK &> /dev/null
    echo "Exiting."
    exit 1
fi

tend=${tend%.${EXT}};
tend=${tend#OUTP*-}
printf "%-40s = %30s\n" "Final time printed" $tend
# extract current initial time
tinit=$(cat INPUT/general_conf.yaml | grep  time0  | awk 'BEGIN {FS="[ \t]*[:#][ \t]*"} {print $2}')
printf "%-40s = %30s\n\n" "Current initial time" $tinit
# backup old initial input files
cp -f INPUT/bodiesInfo.${EXT} INPUT/bodiesInfo.${EXT}_from_${tinit}
cp -f INPUT/contactsInfo.${EXT} INPUT/contactsInfo.${EXT}_from_${tinit}
printf "Old initial input files copied to file_from_%s  . \n\n" $tinit
# copy the last system info into the initial one
cp -f OUTPUT/bodiesInfo-${tend}.${EXT} INPUT/bodiesInfo.${EXT}
cp -f OUTPUT/contactsInfo-${tend}.${EXT} INPUT/contactsInfo.${EXT}
printf "Last (time = %s) bodiesInfo and contactsInfo files copied to INPUT directory.\n\n" $tend
# replace the initial time with the current last time
cp -f INPUT/general_conf.yaml INPUT/general_conf.yaml_from_${tinit}
printf "Original INPUT/general_conf.yaml file copied to INPUT/general_conf.yaml_%s  . \n\n" $tinit
sed -i.bck 's/time0.*/time0 : '${tend}'  #  NEW INITIAL TIME /'  INPUT/general_conf.yaml 
printf "Initial time %s replaced to $tend in general_conf.yaml .\n\n" $tinit
# backup STARTED and EXIT_STATUS_OK files
if [[ -f STARTED ]]; then
    mv STARTED STARTED-$tinit
fi
if [[ -f EXIT_STATUS_OK ]]; then
    mv EXIT_STATUS_OK EXIT_STATUS_OK-$tend
fi

echo "################################################################"
printf "All Done. You can now run dem.x .\n\n"
