/// testing the new DEM 
#include <iostream>
#include <string>
#include <iomanip>
#include "../workspace_dem.hh"
//using namespace std;

int main (void)
{
  stringstream sout;
  CWorkspaceDEM dom;
  string fileName = "configFile.xml";
  dom.ReadConfig(fileName);
  const double FACTOR = 4.0e-3;
  const double SIGMA_CONF = FACTOR*dom.grainDynFeatures["K"];
  const double I = 1.0e-2; // Inertial number
  const double V_AXI = I*(10*dom.maxRad)*sqrt(SIGMA_CONF/dom.grainDynFeatures["RHO"])/dom.maxRad;
  
  //dom.SetGlobalDynVars(DT, G, MIN_RAD, MAX_RAD);
  //dom.SetGrainsDynVars(RHO, K,  GAMMA , MUSF, MUSR);
  //dom.SetPlaneWallsDynVars(RHO*100, 1*K,  1.0*GAMMA , MUSF*0.0, MUSR*0.0);
  ////dom.SetCylWallsDynVars(RHO*100, 1*K,  0.1*GAMMA , MUSF*1.0, MUSR*1.0);
  dom.CamPos = Vec3d_t(0.5*dom.Lx, -2.0*dom.Ly, 1.7*dom.Lz);
  
  // poner las tags
  string s_bc; Vec3d_t Nvec; int tag = 0, id;
  tag = 0;
  //s_bc = "WX = 0 WY = 0 WZ = 0"; dom.AddTag(tag, s_bc); // NO ROT TAG
  //sout << "Wy = 0 Wx = 0 Wz = 0"; sout << " Ry = "; sout << YF/2.0; s_bc = sout.str(); dom.AddTag(tag, s_bc);
  // floor
  id = 0;  tag = 1;
  s_bc = "Rx = 0.0 Ry = 0.0 WX = 0 WY = 0 WZ = 0"; dom.AddTag(tag, s_bc);
  dom.planeWall[id].tag = tag; Nvec = Vec_uz; dom.SetNormalPlaneWallVec(id, Nvec);
  dom.planeWall[id].R = Vec3d_t(0, 0, 0); dom.planeWall[id].S_fixed = Vec3d_t(0, 0, +SIGMA_CONF);
    // roof
  id = 1;  tag = 2;
  s_bc = "Vx = 0.0 Vy = 0.0  WX = 0 WY = 0 WZ = 0"; dom.AddTag(tag, s_bc); 
  dom.planeWall[id].tag = tag; Nvec = -1.0*Vec_uz; dom.SetNormalPlaneWallVec(id, Nvec);
  dom.planeWall[id].R = Vec3d_t(0, 0, dom.Lz); dom.planeWall[id].S_fixed = Vec3d_t(0, 0, -SIGMA_CONF);
  // l-wall
  id = 2;  tag = 3;
  s_bc = "Vz = 0.0 Vy = 0.0  WX = 0 WY = 0 WZ = 0"; dom.AddTag(tag, s_bc); 
  dom.planeWall[id].tag = tag; Nvec = +1*Vec_ux; dom.SetNormalPlaneWallVec(id, Nvec);
  dom.planeWall[id].R = Vec3d_t(0, 0, 0); dom.planeWall[id].S_fixed = Vec3d_t(+SIGMA_CONF, 0, 0);
  // r-wall
  id = 3;  tag = 4;
  s_bc = "Vz = 0.0 Vy = 0.0 WX = 0 WY = 0 WZ = 0"; dom.AddTag(tag, s_bc); 
  dom.planeWall[id].tag = tag; Nvec = -1*Vec_ux; dom.SetNormalPlaneWallVec(id, Nvec);
  dom.planeWall[id].R = Vec3d_t(dom.Lx, 0, 0); dom.planeWall[id].S_fixed = Vec3d_t(-SIGMA_CONF, 0, 0);
  // front-wall
  id = 4;  tag = 5;
  s_bc = "Vx = 0 Vz = 0.0 WX = 0 WY = 0 WZ = 0"; dom.AddTag(tag, s_bc); 
  dom.planeWall[id].tag = tag; Nvec = +1*Vec_uy; dom.SetNormalPlaneWallVec(id, Nvec);
  dom.planeWall[id].R = Vec3d_t(0, 0, 0); dom.planeWall[id].S_fixed = Vec3d_t(0, +SIGMA_CONF, 0);
  // back-wall
  id = 5;  tag = 6;
  s_bc = "Vz = 0.0 Vx = 0.0 WX = 0 WY = 0 WZ = 0"; dom.AddTag(tag, s_bc); 
  dom.planeWall[id].tag = tag; Nvec = -1*Vec_uy; dom.SetNormalPlaneWallVec(id, Nvec);
  dom.planeWall[id].R = Vec3d_t(0, dom.Ly, 0); dom.planeWall[id].S_fixed = Vec3d_t(0, -SIGMA_CONF, 0);
  //cylinder
  /*
    id = 0;  tag = 7;
    s_bc = "Vx = 0.0 Vy = 0.0 Vz=0.0  WX = 0 WY = 0 WZ = 0"; dom.AddTag(tag, s_bc);
    dom.cylWall[id].tag = tag;
    dom.cylWall[id].rad = 1.0*(((XF<YF)?XF:YF)/2.0);
    dom.SetCylElastic(); 
    dom.cylWall[id].Sradial_fixed = -20.0; 
  */
  string bodiesFN = "bodiesInfo.txt";
  //dom.ReadBodiesInfo(bodiesFN);
  //sout << "files_out"; 
  sout.precision(2); sout.setf(ios::scientific); 
  sout << "AXI_CONSTGAMMA_"; 
  sout << "_N_"; sout << dom.N_Sphere; 
  sout << "_K_"; sout  << dom.grainDynFeatures["K"];
  sout << "_GAMMA_"; sout  << dom.grainDynFeatures["GAMMA"];
  sout << "_SIGMACONF_"; sout << SIGMA_CONF; 
  sout << "_VAXI_"; sout << fabs(V_AXI); 
  sout << "_MUSF_"; sout << dom.grainDynFeatures["MUSF"];  
  sout << "_MUSR_"; sout << dom.grainDynFeatures["MUSR"]; 
  string basename = sout.str();   sout.str("");
  dom.Start(basename);
  // time evolution
  double tf = 200;

  // relax istropically
  dom.Evolve(tf, 0.1, /*byEnergy*/true, /*printFlag*/true);
  bodiesFN = "outBodiesInfo.txt";
  dom.PrintBodiesInfo(bodiesFN);
  
//   // Uniaxial compression: Change tags and run
//   tag = 1; // floor
//   sout.str(""); sout << "Vx = 0.0 Vy = 0.0 Vz = " ; sout << V_AXI; sout << "  WX = 0 WY = 0 WZ = 0"; 
//   s_bc = sout.str();  dom.AddTag(tag, s_bc); 
//   tag = 2; // roof
//   sout.str(""); sout << "Vx = 0.0 Vy = 0.0 Vz = " ; sout << -V_AXI; sout << "  WX = 0 WY = 0 WZ = 0"; 
//   s_bc = sout.str();  dom.AddTag(tag, s_bc); 
//   double I1, I2, I3, J1, J2, J3, sigma_dev, sigma_hydr, strain_dev;
//   const double z0 = dom.GetMaxZ() - dom.GetMinZ();
//   const double x0 = dom.GetMaxX() - dom.GetMinX();
//   string filename = basename + ".txt";
//   ofstream fout(filename.c_str());
//   fout.precision(7); fout.setf(ios::scientific);
//   sigma_dev = 0;
//   strain_dev = 0;
//   while(strain_dev <= 50.4) { // check
//     dom.Macro.NormalizeStress(dom.GetVolume());
//     dom.Macro.GetStressInvariants(I1, I2, I3, J1, J2, J3);
//     sigma_dev = sqrt(3*J2); // von Mises
//     sigma_hydr = I1/3;
//     strain_dev = 2*fabs((dom.GetMaxZ() - dom.GetMinZ() - z0 )/z0 - (dom.GetMaxX() - dom.GetMinX() - x0)/x0)/3;
//     clog << dom.time << "\t";
//     fout << setw(11) << dom.time << "\t";
//     fout << setw(11) << strain_dev << "\t";
//     fout << setw(11) << sigma_dev/sigma_hydr << "\t";
//     fout << setw(11) << sigma_dev << "\t";
//     fout << setw(11) << sigma_hydr << "\t";
//     fout << endl;
//     tf = 0.5;
//     dom.Evolve(tf, 0.5, /*byEnergy*/false, /*printFlag*/ true);
//   }
//   fout.close();

  return 0;
}







