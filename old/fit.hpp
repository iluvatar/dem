#ifndef __FIT_HPP__
#define __FIT_HPP__

//// fits with the o2scl library
//#include <gsl_fit.h>

// fits with the gsl
#include <cstdlib>
#include <cstdio>
#include <vector>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_multifit_nlin.h>
     
namespace NLFIT {

  struct data {
    size_t  n; // number of data
    size_t np; // number of parameters
    double *x; // data on x
    double *y; // data on y
    double (*f) (double * p, double & x); // function f
    double (*df)(double * p, double & x, size_t & ip); // function df for jacobian
  };
  
  int
  f_gsl(const gsl_vector * p, void * data, gsl_vector * fi)
  {
    size_t  n = ((struct data *)data)->n;
    double *x = ((struct data *)data)->x;
    double *y = ((struct data *)data)->y;
    double (*f)(double *p, double &x) = ((struct data *)data)->f;
    
    for (size_t i = 0; i < n; i++) {
      gsl_vector_set (fi, i, f(p->data, x[i]) - y[i]);
    }
    
    return GSL_SUCCESS;
  }
     
  int
  df_gsl(const gsl_vector * p, void * data, gsl_matrix * J)
  {
    size_t n  = ((struct data *)data)->n;
    size_t np = ((struct data *)data)->np;
    double *x = ((struct data *)data)->x;
    double (*df)(double *p, double &x, size_t & ip) = ((struct data *)data)->df;
    
    /* Jacobian matrix J(i,j) = dfi / dpj, */
    for (size_t i = 0; i < n; i++) {
      for (size_t jp = 0; jp < np; jp++) {
	gsl_matrix_set (J, i, jp, df(p->data, x[i], jp)); 
      }
    }

    return GSL_SUCCESS;
  }
  
  int
  fdf_gsl(const gsl_vector * x, void * data, gsl_vector * f, gsl_matrix * J)
  {
    f_gsl(x, data, f);
    df_gsl(x, data, J);
    
    return GSL_SUCCESS;
  }

  void print_state (size_t iter, gsl_multifit_fdfsolver * s);

  int 
  fit(const std::vector<double> & xdata, const std::vector<double> & ydata, 
      double (*func)(double *, double &), double (*dfunc) (double *, double &, size_t &), 
      std::vector<double> & par, std::vector<double> & dpar, 
      const double & eps = 1.0e-6, const size_t & max_iter = 5000) 
  {
    const gsl_multifit_fdfsolver_type *T;
    gsl_multifit_fdfsolver *s;
    int status;
    unsigned int i, iter = 0;
    const size_t n = xdata.size();
    const size_t np = par.size();
    
    gsl_matrix * covar = gsl_matrix_alloc (np, np);
    double * x = const_cast<double *>(&xdata[0]);
    double * y = const_cast<double *>(&ydata[0]);
    struct data d;
    d.n = n; d.np = np; d.x = x; d.y = y; 
    d.f = func; 
    d.df = dfunc; 
    gsl_multifit_function_fdf f;
    gsl_vector_view pvec = gsl_vector_view_array (const_cast<double *>(&par[0]), np);
    
    f.f = &f_gsl;
    f.df = &df_gsl;
    f.fdf = &fdf_gsl;
    f.n = n;
    f.p = np;
    f.params = &d;
    
    T = gsl_multifit_fdfsolver_lmsder;
    s = gsl_multifit_fdfsolver_alloc (T, n, np);
    gsl_multifit_fdfsolver_set (s, &f, &pvec.vector);
    
    
    print_state (iter, s);
    do {
      iter++;
      status = gsl_multifit_fdfsolver_iterate (s);
      printf ("status = %s\n", gsl_strerror (status));
      print_state (iter, s);
      if (status) break;
      status = gsl_multifit_test_delta (s->dx, s->x, eps, eps); 
   }
    while (status == GSL_CONTINUE && iter < max_iter);

    gsl_multifit_covar (s->J, 0.0, covar);

    double chi = gsl_blas_dnrm2(s->f);
    double dof = n - np;
    double c = GSL_MAX_DBL(1, chi / sqrt(dof)); 
    for (size_t ip = 0; ip < np; ++ip) {
      par[ip] = s->x->data[ip];
      dpar[ip] = c*sqrt(gsl_matrix_get(covar, ip, ip));
    }
    
#define FIT(i) gsl_vector_get(s->x, i)
#define ERR(i) sqrt(gsl_matrix_get(covar,i,i))
    { 
      double chi = gsl_blas_dnrm2(s->f);
      double dof = n - np;
      double c = GSL_MAX_DBL(1, chi / sqrt(dof)); 
      
      printf("chisq/dof = %g\n",  pow(chi, 2.0) / dof);
      
      printf ("A      = %.5f +/- %.5f\n", FIT(0), c*ERR(0));
      printf ("lambda = %.5f +/- %.5f\n", FIT(1), c*ERR(1));
      printf ("b      = %.5f +/- %.5f\n", FIT(2), c*ERR(2));
    }
    
    printf ("status = %s\n", gsl_strerror (status));
    
    gsl_multifit_fdfsolver_free (s);
    gsl_matrix_free (covar);
    
    return EXIT_SUCCESS;
  }
  
  void
  print_state (size_t iter, gsl_multifit_fdfsolver * s)
  {
    printf ("iter: %3u x = % 15.8f % 15.8f % 15.8f "
	    "|f(x)| = %g\n",
	    iter,
	    gsl_vector_get (s->x, 0), 
	    gsl_vector_get (s->x, 1),
	    gsl_vector_get (s->x, 2), 
	    gsl_blas_dnrm2 (s->f));
  }
  
}  // namespace NLFIT 


#endif // __FIT_HPP__
