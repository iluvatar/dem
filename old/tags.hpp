#ifndef __TAGS__
#define __TAGS__

#include "dictionary.hpp"
#include "vector3d.hpp"
#include "particle.hpp"
#include <vector>
#include <string>
#include <unordered_map>
#include <unordered_set>

//------------------------------------------------------------------
// Defines group and single tags
//------------------------------------------------------------------

namespace DEM {
  //------------------------------------------------------------------
  // Single tags : Individual tags fixing one or more variables
  //------------------------------------------------------------------
  class SingleTags {
  public:
    dictionary::TagDictionary dict_; // pair of string (coordinate and variable) and its value
    SingleTags(); // default constructor
    template <class T> void parse_tags(std::vector<T> & particle);
    template <class T> void process(std::vector<T> & particle, const size_t & iter);
    void add_tagged_values(const int tag, const std::string settings) { dictionary::add_tagged_values(dict_, tag, settings); }
    void set_steps(const int & tag, const std::string & var, const int & nsteps);  
    bool ramps_done(const size_t & iter);


  private:
    //enum NAMES {RX=0, RY, RZ, VX, VY, VZ, FX, FY, FZ, PHIX, PHIY, PHIZ, WX, WY, WZ};
    enum NAMES {RX=0, RY, RZ, VX, VY, VZ, PHIX, PHIY, PHIZ, WX, WY, WZ};
    std::unordered_map<std::string, int> index_;                  // ex: "VX" -> VX == 3, according to NAMES enum 
    std::vector<void (*)(Particle &, const double &) > table_;         // function array. ex: table_[RX] = set_RX
    std::unordered_map<int, std::unordered_set<int> > list_; // list of particles id for a given tag. Ex: list_[2] = particles with bctag==2 
    std::unordered_map<int, std::vector<size_t> > steps_;  // steps to get each limit value. Default = 1
    static void set_RX(Particle & object, const double & value) { object.R[0] = value; object.V[0] = object.F[0] = object.F_fixed[0] = 0;  }
    static void set_RY(Particle & object, const double & value) { object.R[1] = value; object.V[1] = object.F[1] = object.F_fixed[1] = 0;  }
    static void set_RZ(Particle & object, const double & value) { object.R[2] = value; object.V[2] = object.F[2] = object.F_fixed[2] = 0;  }
    static void set_VX(Particle & object, const double & value) { object.V[0] = value; object.F[0] = object.F_fixed[0] = 0;  }
    static void set_VY(Particle & object, const double & value) { object.V[1] = value; object.F[1] = object.F_fixed[1] = 0;  }
    static void set_VZ(Particle & object, const double & value) { object.V[2] = value; object.F[2] = object.F_fixed[2] = 0;  }
    //static void set_FX(Particle & object, const double & value) { object.F_fixed[0] = value;  }
    //static void set_FY(Particle & object, const double & value) { object.F_fixed[1] = value;  }
    //static void set_FZ(Particle & object, const double & value) { object.F_fixed[2] = value;  }
    static void set_PHIX(Particle & object, const double & value) { object.PHI[0] = value; object.W[0] = object.T[0] = 0;  }
    static void set_PHIY(Particle & object, const double & value) { object.PHI[1] = value; object.W[1] = object.T[1] = 0;  }
    static void set_PHIZ(Particle & object, const double &value) { object.PHI[2] = value; object.W[2] = object.T[2] = 0;  }
    static void set_WX(Particle & object, const double & value) { object.W[0] = value; object.T[0] = 0;  }
    static void set_WY(Particle & object, const double & value) { object.W[1] = value; object.T[1] = 0;  }
    static void set_WZ(Particle & object, const double & value) { object.W[2] = value; object.T[2] = 0;  }
  };
  
  SingleTags::SingleTags() {
    table_.resize(WZ+1); // last element in enum
    table_[RX] = set_RX; table_[RY] = set_RY; table_[RZ] = set_RZ;
    table_[VX] = set_VX; table_[VY] = set_VY; table_[VZ] = set_VZ;
    //table_[FX] = set_FX; table_[FY] = set_FY; table_[FZ] = set_FZ;
    table_[PHIX] = set_PHIX; table_[PHIY] = set_PHIY; table_[PHIZ] = set_PHIZ;
    table_[WX] = set_WX; table_[WY] = set_WY; table_[WZ] = set_WZ;
    index_["RX"] = RX; index_["RY"] = RY; index_["RZ"] = RZ;
    index_["VX"] = VX; index_["VY"] = VY; index_["VZ"] = VZ;
    //index_["FX"] = FX; index_["FY"] = FY; index_["FZ"] = FZ;
    index_["PHIX"] = PHIX; index_["PHIY"] = PHIY; index_["PHIZ"] = PHIZ;
    index_["WX"] = WX; index_["WY"] = WY; index_["WZ"] = WZ;
  }

  template <class T> 
  inline
  void 
  SingleTags::parse_tags(std::vector<T> & particle) 
  {
    int tag;
    for(size_t id = 0; id < particle.size(); ++id) {
      tag = particle[id].bcTag;
      if (0 != tag) { // ignore default tag == 0, it means no condition  
	std::cout << "# Tag = " << tag << std::endl;
	std::cout << "# id  = " << id << std::endl;
	list_[tag].insert(id);
      } }
  }

  void 
  SingleTags::set_steps(const int & tag, const std::string & var, const int & nsteps)
  {
    if(0 == steps_[tag].size()) steps_[tag].resize(WZ+1, 1);
    steps_[tag][index_[var]] = (nsteps >= 0) ? nsteps : 0;
  }

  bool 
  SingleTags::ramps_done(const size_t & iter)
  {
    for(const auto & val1: steps_) {
      for(const auto & val2: val1.second) {
	if (iter <= val2) return false;
      }      
    }
    return true;
  }


  template <class T>
  inline
  void SingleTags::process(std::vector<T> & particle, const size_t & iter)
  {
    static std::unordered_map<int, std::unordered_set<int> >::iterator it;
    static std::unordered_set<int>::iterator it1;
    for (it = list_.begin(); it != list_.end(); ++it) { // for each single tag
      int tag = it->first;
      for (it1 = it->second.begin(); it1 != it->second.end(); ++it1) {  // for each particle id with that tag
	size_t id = *it1; 
	for (dictionary::BasicDictionary::iterator it2 = dict_[tag].begin(); it2 != dict_[tag].end(); ++it2) { // for each values pair in tag
	  double value = it2->second;
	  const size_t idx = index_[it2->first];
	  const size_t nsteps = steps_[tag][idx]; 
	  if (nsteps > 0 && iter < nsteps) value *= ((double)(iter))/nsteps; 
	  table_[idx](particle[id], value); // set that value for that particle
	} 
      } 
    } 
  }
  

  //------------------------------------------------------------------
  // Group tags : Particles sharing the same total force and momentum
  //------------------------------------------------------------------
  class GroupTags {
  public:
    GroupTags() {;} ;
    template <class Object> void parse_tags(const std::vector<Object> & particle);
    template <class Object> void process(std::vector<Object> & particle);
    
  private:
    std::unordered_map<int, std::unordered_set<int> > list_;
  };

  template <class Object> 
  inline
  void
  GroupTags::parse_tags(const std::vector<Object> & particle)
  {
    int tag;
    for(size_t id = 0; id < particle.size(); ++id) {
      tag = particle[id].groupTag;
      if (0 != tag) // ignore default tag == 0, it means no group  
	list_[tag].insert(id);
    }
  }

  template <class Object> 
  inline
  void GroupTags::process(std::vector<Object> & particle)
  {
    static std::unordered_map<int, std::unordered_set<int> >::iterator it;
    static std::unordered_set<int>::iterator it1;
    for (it = list_.begin(); it != list_.end(); ++it) { // for each group tag
      Vec3d_t F(Vecnull), A(Vecnull), P(Vecnull), V(Vecnull);
      double M = 0;
      for (it1 = it->second.begin(); it1 != it->second.end(); ++it1) { 
	F += particle[*it1].F; // sum total force
	P += (particle[*it1].V)*(particle[*it1].mass); // sum total momentum
	M += particle[*it1].mass; // sum total mass
      }
      A = F/M; // total acceleration (equal for all bodies)
      V = P/M; // total velocity (equal for all bodies)
      for (it1 = it->second.begin(); it1 != it->second.end(); ++it1) {
	particle[*it1].F = (particle[*it1].mass)*A; // same force for all bodies
	particle[*it1].V = V; // same velocity for all bodies
      }
    }
  }


} // namespace DEM
#endif
