// This library computes pdf by using the kernel density estimator technique
#ifndef __KERNEL_HISTOGRAM_HH__
#define __KERNEL_HISTOGRAM_HH__


#include <iostream>
#include <fstream>
#include <iomanip>
#include <vector>
#include <string>
#include <algorithm>
#include <cmath>
#include <cassert>
#include "utility.hh" // for ASSERT function
#include "lookup_table.hh" // for exp function
//using namespace std;



class KernelHisto
{
private:
  double xmin, xmax, dx, samples, u_h; // u_h : 1.0/bandwidth
  long double sum_1, sum_2;
  int bins, hRecompCounter;
  vector<double> data, dataNormalized, xval;
  bool linear;
  LookupTable<exp> expTable;
public:
  explicit KernelHisto();
  KernelHisto & operator=(const KernelHisto & histo);
  void Construct(double xmin0, double xmax0, int bins0, bool linearFlag = true);
  ~KernelHisto();
  void ComputeInternalPoints(void);
  void CountValue(double x);
  void Normalize(void);
  void print(string filename);
  double GetMean(void);
  double GetSigma(void);
  double GetK(double x, double x0);
  void RecomputeBandwidth(void);
};

// kernel function
inline
double
KernelHisto::GetK(double x, double x0)
{
  double tmp = (x-x0)*u_h; 
  // return (0.5*M_SQRT1_2*M_2_SQRTPI)*exp(-0.5*tmp*tmp); // gaussian
  return (0.5*M_SQRT1_2*M_2_SQRTPI)*expTable(-0.5*tmp*tmp); // gaussian, memoized with lookup table
  // if (tmp < 1) return (3.0/4.0)*(1 - tmp*tmp); // Epanechnikov
  // if (tmp < 1) return (15.0/16.0)*(1 - tmp*tmp)*(1 - tmp*tmp); // quartic
  // if (tmp < 1) return (35.0/32.0)*(1 - tmp*tmp)*(1 - tmp*tmp)*(1 - tmp*tmp); // triweight
  // return 0;
}


KernelHisto::KernelHisto()
{
  data.clear();
  dataNormalized.clear();
  xval.clear();
  samples = 0;
  hRecompCounter = 0;
  sum_1 = sum_2 = 0;
}

void
KernelHisto::Construct(double xmin0, double xmax0, int bins0, bool linearFlag)
{
  ASSERT(xmax0 >= xmin0);
  ASSERT(bins0 > 0);
  linear = linearFlag;
  dx = (xmax0 - xmin0)/bins0;
  xmin = xmin0; xmax = xmax0 + dx; bins = bins0+1;
  data.reserve(bins); fill(data.begin(), data.end(), 0);
  dataNormalized.reserve(bins); fill(dataNormalized.begin(), dataNormalized.end(), 0);
  xval.reserve(bins); fill(xval.begin(), xval.end(), 0);
  ComputeInternalPoints();
  //u_h = (1.0/0.009); // bandwidth, 0.09 for gaussian kernel // good for uniform and exponential numbers
  //u_h = (1.0/0.09); // bandwidth, 0.09 for gaussian kernel, good for gaussian numbers
  u_h = (1.0/0.0020); // bandwidth, 0.09 for gaussian kernel, good for gaussian numbers
  samples = 0;
  // construct the lookup table
  expTable.Construct(max(-0.5*u_h*u_h*fabs(xmax - xmin)*fabs(xmax - xmin), -15.0), 0.0, 2000); // range estimated
  sum_1 = sum_2 = 0;
  hRecompCounter = 0;
}

KernelHisto::~KernelHisto()
{
  data.clear();
  dataNormalized.clear();
  xval.clear();
  samples = 0;
  sum_1 = sum_2 = 0;
  hRecompCounter = 0;
}

void 
KernelHisto::ComputeInternalPoints(void)
{
  if (true == linear) {
    for(int i = 0; i < bins; ++i) {
      xval[i] = xmin + i*dx;
    }
  }
  else { // logarithmic
    dx = pow(xmax-xmin+1, 1.0/bins);
    for(int i = 0; i < bins; ++i) {
      xval[i] = pow(dx, i) + xmin - 1.0;
    }
  }
}

KernelHisto &  
KernelHisto::operator=(const KernelHisto & histo)
{
  if(this != &histo) {
    xmin = histo.xmin;
    xmax = histo.xmax;
    dx = histo.dx;
    bins = histo.bins;
    linear = histo.linear;
    u_h = histo.u_h;
    samples = histo.samples;
    data = histo.data; 
    dataNormalized = histo.dataNormalized; 
    xval = histo.xval;
    sum_1 = histo.sum_1;
    sum_2 = histo.sum_2;
    hRecompCounter = histo.hRecompCounter;
  }
  return *this;
}

inline
void 
KernelHisto::CountValue(double x)
{
  clog.setf(ios::scientific);
  for(int i = 0; i < bins; ++i) {
    data[i] += GetK(xval[i], x);
  }
  ++samples; 
  sum_1 += x;
  sum_2 += x*x;
  
  ++hRecompCounter;
  if(hRecompCounter > 200000) {
    RecomputeBandwidth();
    hRecompCounter = 0;
  }
}

inline 
void 
KernelHisto::RecomputeBandwidth(void)
{
  double avg = sum_1/samples;
  double sigma = sqrt(sum_2/samples - avg*avg);
  double tmp = u_h;
  if (sigma > 0) tmp = pow((samples/4.0)*3.0, 0.2)/sigma; // optimal for gaussian distributed data
  if (tmp > u_h) u_h = tmp;
}

inline
void
KernelHisto::Normalize(void)
{
  if(samples > 0) {
    double tmp = u_h/samples;
    for(int i = 0; i < bins; ++i) {
      if(data[i] < 1.0e-16) dataNormalized[i] = data[i] = 0;
      else dataNormalized[i] = tmp*data[i];
    }
  } 
  else fill(dataNormalized.begin(), dataNormalized.end(), 0);
}

inline
void 
KernelHisto::print(string filename)
{
  ofstream fout(filename.c_str());
  double mean = GetMean(), sigma = GetSigma();
  fout << "# samples = " << samples << endl;
  fout << setw(15) << "# x"
       << setw(15) << "dataNormalized"
       << setw(15) << "mean" 
       << setw(15) << "sigma" 
       << setw(15) << "data[x]" 
       << endl;
  Normalize();
  for(int i = 0; i < bins; ++i) {
    fout << setw(15) << xval[i] 
	 << setw(15) << dataNormalized[i] 
	 << setw(15) << mean 
	 << setw(15) << sigma 
	 << setw(15) << data[i] 
	 << endl;
  }
}

inline
double 
KernelHisto::GetMean(void)
{
  Normalize();
  double sum = 0, sump = 0;
  for(int i = 0; i < bins; ++i) {
    sum += xval[i]*dataNormalized[i];
    sump += dataNormalized[i];
  }  
  if (sump > 0) sum /= sump;
  else sum = 0;
  return sum;
}

inline
double 
KernelHisto::GetSigma(void)
{
  Normalize();
  double sum = 0, sum2 = 0, sump = 0;
  for(int i = 0; i < bins; ++i) {
    sum += xval[i]*dataNormalized[i];
    sum2 += xval[i]*xval[i]*dataNormalized[i];
    sump += dataNormalized[i];
  }  
  if (sump > 0) sum /= sump;
  else sum = 0;
  if (sump > 0) sum2 /= sump;
  else sum2 = 0;
  return sqrt(sum2 - sum*sum);
}


#endif
