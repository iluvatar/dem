#!/bin/bash
#choose compiler
#COMPILER="llvm-g++"
COMPILER="g++"
if [ -e /usr/local/bin/g++-4.6 ]; then
    COMPILER="g++-4.6 " 
fi
#if [[ $(type -P icpc) != "" ]]; then 
#    COMPILER="icpc"; 
#fi
#set flags
GENERIC_FLAGS=" -I ./inc -L$HOME/lib -I $HOME/include  -DNDEBUG -Wall -Wextra "
GCC_FLAGS=" -O4 -msse4.1 -finline  -ftree-vectorize -fopenmp -finline-functions"
#GCC_FLAGS=" -O4 "
INTEL_FLAGS=" -O2 -finline  -msse3 -ipo -openmp "
FLAGS=$GENERIC_FLAGS
if [[ $(echo $COMPILER | grep g) != "" ]]; then
    FLAGS="$FLAGS $GCC_FLAGS"
elif [[ $(echo $COMPILER | grep icpc) != "" ]]; then
    FLAGS="$FLAGS $INTEL_FLAGS"
fi
# Libraries
LD_FLAGS="$LD_FLAGS  -lvoro++ "


cd $(pwd)

FILES=""
COMPILER_MORE_FLAGS=""
for i in "${@:1}"; do
    # source files
    tmp=$(echo $i | grep .cpp)
    if [ -n "$tmp" ]; then
	FILES="$FILES $tmp"
    fi
    # header file
    tmp=$(echo $i | grep .hh)
    if [ -n "$tmp" ]; then
	FILES="$FILES $tmp"
    fi
    # flags
    tmp=$(echo $i | grep -v .cpp | grep '-')
    if [ -n "$tmp" ]; then
	COMPILER_MORE_FLAGS="$COMPILER_MORE_FLAGS $tmp"
    fi
done
if [ -z "$FILES" ]; then
    FILES=$(ls ./*.cpp)
fi

for a in $FILES; do
    echo "File to compile           : $a";
    echo "Compiler                  : $COMPILER";
    echo "Compiler arguments        : $FLAGS";
    echo "Compiler libraries flags  : $LD_FLAGS";
    echo "Compiler USER      flags  : $COMPILER_MORE_FLAGS";
    base=$(basename $a .cpp)
    rm -f bin/$base.x
    command="$COMPILER $a $FLAGS $COMPILER_MORE_FLAGS $LD_FLAGS -o bin/$base.x"
    echo "FULL COMPILE COMMAND : "
    echo "$command"
    time $command
    if [[ "0" != "$?" ]]; then
	printf "\n\nERROR COMPILING $a\n\n"
    fi
    echo
done
