#include "../inc/fit.hpp"
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <vector>
#include <iostream>

#define N 40

double 
f1(double * p, double & x)
{
  return p[0]*exp(-p[1]*x) + p[2]; 
}

double 
df1(double * p, double & x, size_t & ip)
{
  switch(ip) {
  case 0 : 
    return exp(-p[1]*x); break;
  case 1 : 
    return -x*p[0]*exp(-p[1]*x); break;
  case 2 : 
    return 1; break;
  default :
    return 0;
  };
}


int main(void) 
{
  size_t n = N;
  std::vector<double> x(n, 0), y(n, 0), par(3, 0), dpar(3, 0);
  const gsl_rng_type * type;
  gsl_rng * r;
  gsl_rng_env_setup();
  type = gsl_rng_default;
  r = gsl_rng_alloc (type);
  for (size_t i = 0; i < n; i++) {
    double t = i;
    x[i] = t;
    y[i] = 1.0 + 5 * exp (-0.1 * t) 
      + gsl_ran_gaussian (r, 0.1);
    //printf ("%u %g \n", i, y[i]);
  };
   
  par[0] = 1; par[1] = 0.0; par[2] = 0.0;

  NLFIT::fit(x, y, f1, df1, par, dpar);
  std::cout << par[0] << std::endl;
  std::cout << par[1] << std::endl;
  std::cout << par[2] << std::endl;
  std::cout << dpar[0] << std::endl;
  std::cout << dpar[1] << std::endl;
  std::cout << dpar[2] << std::endl;

  return EXIT_SUCCESS;
}
