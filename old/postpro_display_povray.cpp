//--------------------------------------------------------------------------
// Plot for povray
//--------------------------------------------------------------------------

#include "../inc/postpro_display.hpp"

int main()
{
  DEM::PostproDisplay postprocessor;
  postprocessor.draw_povray();

  return 0;
}
