// This library implements a lookup table for a given function
// Also uses interpolation (linear)

#ifndef __LOOKUP_TABLE_HH__
#define __LOOKUP_TABLE_HH__

#include <cmath>
#include "utility.hh" // for ASSERT function
//using namespace std;

template <double func(double )>
class LookupTable{
private:
  double xmin, xmax, dx, udx;
  int bins;
  double *table;
  double *ydiffudx; // optimization
  double *xbin; // optimization
public:
  void Construct(double min0, double max0, int bins0);
  double operator()(double x);
}; 

template <double func(double )>
inline
double 
LookupTable<func>::operator()(double val) 
{
  if(val < xmin || val >= xmax) return 0;
  int bin = int((val-xmin)*udx); 
  return table[bin] + ydiffudx[bin]*(val - xbin[bin]); // linear interpolation
}

template <double func(double )>
void 
LookupTable<func>::Construct(double min0, double max0, int bins0) 
{
  ASSERT(max0 > min0); ASSERT(bins0 > 0);
  dx = (max0 - min0)/bins0; udx = 1.0/dx;
  xmin = min0; xmax = max0 + dx; bins = bins0 + 1;
  table = new double [bins];
  ydiffudx = new double [bins];
  xbin = new double [bins];
  for(int i = 0; i < bins; ++i) table[i] = func(xmin + i*dx);
  for(int i = 0; i < bins-1; ++i) ydiffudx[i] = (table[i+1] - table[i])*udx; ydiffudx[bins-1] = 0.0;
  for(int i = 0; i < bins; ++i) xbin[i] = xmin + i*dx;
}


#endif
