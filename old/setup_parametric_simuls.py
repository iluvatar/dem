#!/bin/env python 
import sys, os, shutil, fileinput

# This scripts generates the directories for parametric simulations.

# check arguments
if 2 != len(sys.argv):
    print "ERROR. "
    print "Usage : " + sys.argv[0] + " MODEL_DIR_NAME"
    sys.exit(1)
CWD=os.getcwd()
MODEL_DIR_NAME=CWD + os.sep + sys.argv[1] 
if not os.path.isdir(MODEL_DIR_NAME):
    print "ERROR. Directory " + MODEL_DIR_NAME + " does not exist in current working directory."
    sys.exit(1)

# values to iterate (parameters) : NAMES
# Parameter 0 is the main parameter
param0_name="mur"
#param1_name="init_trials"
param2_name="seed"
# values to iterate (parameters)
#param0 = (0.00, 0.01, 0.05, 0.08, 0.10, 0.20, 0.30, 0.40, 0.60, 0.80, 0.90, 0.99)
param0 = (0.000, 0.001, 0.002, 0.004, 0.008, 0.010, 0.020, 0.040, 0.080, 0.100, 0.200, 0.400)
#param1 = (0, 200)
param2 = (0, 10, 20, 40, 60, 120, 240, 480, 960)

# helper function to check data sizes
def check_equal_lenght(ref, val) :
    if len(ref) != len(val) :
        print "ERROR. Size of val list differs from size of ref parameter.\n"
        sys.exit(1)

# fill here the simulation parameters which depends on the parameters
#dt = [6.014E-07, 3.804E-07, 2.690E-07, 2.196E-07, 1.902E-07, 1.203E-07, 8.506E-08, 6.945E-08, \
#      6.014E-08, 3.804E-08, 2.690E-08, 2.196E-08, 1.902E-08, 1.203E-08, 8.506E-09, 6.945E-09]
#time_steps = [1577332]*len(param0)
#time_print_steps = [7887]*len(param0)
#walls_nsteps = [394333]*len(param0)
#materials_K_tag_0 = list(param0)
#materials_mus_tag_0 = list(param0)
materials_mur_tag_0 = list(param0)
#materials_K_tag_1 = materials_K_tag_0

# check sizes
#check_equal_lenght(param0, dt)
#check_equal_lenght(param0, materials_mus_tag_0)
check_equal_lenght(param0, materials_mur_tag_0)


# main function

#for par1 in param1:
#    print param1_name + ' = ' + str(par1)
#    dirname=param1_name_ + '_' + str(par1).zfill(3) + '_' + os.sep
#    os.mkdir(dirname)
#    os.chdir(dirname)
i = 0 # counter for param0 value
for par0 in param0:
    print param0_name + ' = ' + str(par0)
    dirname=param0_name + '_' + str("%.3f" % par0).zfill(5) + '_' + os.sep
    os.mkdir(dirname)
    os.chdir(dirname)
    for par2 in param2:
        print param2_name + ' = ' + str(par2)
        dirname=param2_name + '_' + str(par2).zfill(3) + '_' + os.sep
        shutil.copytree(MODEL_DIR_NAME, dirname)  # this will create dirname automatically
        os.chdir(dirname)
        # NOTE: fileinput module starts counting line AT number 1
        # START parameter replacement
        ## INPUT/prepro_conf
        for line in fileinput.input('INPUT/prepro_init_conf', inplace=1, backup='.orig') :
            if 10 == fileinput.filelineno(): line = str(par2) + '\t\t\t# NEW ' + param2_name + ' for random number generator\n'
            #elif 12 == fileinput.filelineno(): line = str(par1) + '\t\t\t# NEW ' + param1_name + ' \n'
            print line,
        ## INPUT/general_conf
        #for line in fileinput.input('INPUT/general_conf', inplace=1, backup='.orig') :
        #    if 4 == fileinput.filelineno(): line = str(dt[i]) + '\t\t\t# NEW dt\n'
        #   elif 5 == fileinput.filelineno(): line = str(time_steps[i]) + '\t\t\t# NEW time steps\n'
        #    elif 6 == fileinput.filelineno(): line = str(time_print_steps[i]) + '\t\t\t# NEW time print steps\n'
        #    print line,
        ## INPUT/walls_conf 
        #for line in fileinput.input('INPUT/walls_conf', inplace=1, backup='.orig') :
        #    if 16 == fileinput.filelineno() or 22 == fileinput.filelineno() or 31 == fileinput.filelineno():
        #        line = '2   -' + str(par0) + '   ' + str(walls_nsteps[i]) + '\t\t\t# NEW NEW NEW NEW\n'
        #    print line,
        # INPUT/materials_conf
        for line in fileinput.input('INPUT/materials_conf', inplace=1, backup='.orig') :
            line = line.split() # allows to edit a particular column
            #if 2 == fileinput.filelineno():   line[4] = str(materials_mus_tag_0[i])
            if 2 == fileinput.filelineno():   line[5] = str(materials_mur_tag_0[i])
            #elif 3 == fileinput.filelineno(): line[2] = str(materials_K_tag_1[i])
            print '  '.join(map(str,line)) # convert to string and separate by spaces
        # END   parameter replacement
        os.chdir('../')
    os.chdir('../')
    i += 1
#os.chdir('../')


print "Done."
