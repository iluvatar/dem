#include <iostream>
#include <fstream>
#include <cmath>
#include <cstdlib>
#include "../histogram1d.hh"
//using namespace std;

int main(int argc, char **argv)
{
  if(5 != argc) {
    cerr << "Error calling the program. Usage:" << endl
	 << argv[0] << " fileToRead fileToPrint minValue maxValue" << endl;
    exit(1);
  }

  double mean, sigma;
  // declarar histograma y rango
  CHistogram1D histo;
  int bins = 200;
  double xmin  = strtod(argv[3], NULL), xmax = strtod(argv[4], NULL);
  //double dx = (xmax - xmin)/bins;
  histo.Construct(xmin, xmax, bins+1);
  // read the data
  histo.Read(argv[1]);
  // normalize the histogram
  histo.Normalize();
  // compute mean and sigma
  mean = histo.GetMean();
  sigma = histo.GetSigma();
  clog << "# Mean = " << mean << endl;
  clog << "# Sigma = " << sigma << endl;
  histo.Print("histoFluctsOriginal.txt");
  // declare new histogram of fluctuations and build it
  CHistogram1D histoFlucs(histo);
  histoFlucs.ShiftBy(-mean);
  // normalize the flucs histogram
  histoFlucs.Normalize();
  // print histogram of fluctuations
  histoFlucs.Print(argv[2]);
  // compute mean and sigma of fluctuations
  mean = histoFlucs.GetMean();
  sigma = histoFlucs.GetSigma();
  clog << "# After shifting and selecting only mean +- 4sigma" << endl;
  clog << "# Mean = " << mean << endl;
  clog << "# Sigma = " << sigma << endl;
  clog << endl;
}
