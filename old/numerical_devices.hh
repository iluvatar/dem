#ifndef __NUMERICAL_DEVICES__
#define __NUMERICAL_DEVICES__

#include "workspace_dem.hh"
#include "utility.hh"
#include <map>
//using namespace std;

namespace DEM {
  class NumericalDevices{
  public:
    void box_2D(WorkspaceDEM & dom, bool updatePosFlag);
    void box_2D_only_ground(WorkspaceDEM & dom, bool updatePosFlag);
    void box_2D_isotropic(WorkspaceDEM & dom, const double force_conf, bool updatePosFlag) ;
    void box_2D_shear_periodic(WorkspaceDEM & dom, const double & force_conf, const double & Vshear, bool updatePosFlag);
    void box_2D_shear_periodic_pull(WorkspaceDEM & dom, const double & force_conf, const double & Vshear, 
				    const int id, double fpull, bool updatePosFlag);
    void box_2D_biaxial(WorkspaceDEM & dom, const double & force_confx, const double & Vz, bool updatePosFlag);
  };
  
  void 
  NumericalDevices::box_2D_shear_periodic(WorkspaceDEM & dom, const double & force_conf, const double & Vshear, bool updatePosFlag) 
  {
    ASSERT(2 == dom.dim);
    ASSERT(true == dom.periodic_x());
    print_log("# CONFIGURING   2D BOX SHEAR PERIODIC    SET UP");
    print_log("# force_conf = ", force_conf);
    print_log("# Vshear = ", Vshear);

    box_2D(dom, updatePosFlag);

    // local vars
    Vec3d_t Nvec;
    size_t id; 
    int tag;
    string s_bc; 
    stringstream sout;
    
    // group tags
    // for each top particle, also set its individual tag to no rotation and VX fixed
    tag = GRAIN_2D_CONST_VX_TAG;
    sout.str(""); sout << "PHIX = 0 PHIY = 0 PHIZ = 0 "; sout << " Ry = " << dom.length(1)/2.0; sout << " Vx = " << Vshear;
    s_bc = sout.str(); dictionary::add_tagged_values(dom.single_tagger.dict_, tag, s_bc);
    for (id = 0; id < dom.N_Grain; ++id)
      if (GROUP_TAG_TOP_WALL == dom.particle[id].bcTag_group)
	dom.particle[id].bcTag = tag;
    
    // for each bottom particle, also set its individual tag to no rotation and VX fixed
    tag = GRAIN_2D_FIXED_TAG;
    sout.str(""); sout << "PHIX = 0 PHIY = 0 PHIZ = 0 VX = 0 RY = " << dom.length(1)/2.0 << " RZ = " << dom.maxRad;
    s_bc = sout.str(); dictionary::add_tagged_values(dom.single_tagger.dict_, tag, s_bc);
    for (id = 0; id < dom.N_Grain; ++id)
      if (GROUP_TAG_BOTTOM_WALL == dom.particle[id].bcTag_group)
	dom.particle[id].bcTag = tag;

    // floor 
    id = dom.N_Grain + BOTTOM; tag = BOTTOM_WALL_TAG;
    s_bc = "Rx = 0.0 Ry = 0.0 Rz = 0.0 PHIX = 0 PHIY = 0 PHIZ = 0"; dictionary::add_tagged_values(dom.single_tagger.dict_,tag, s_bc); dom.particle[id].bcTag = tag; 
    if(true == updatePosFlag) {
      dom.particle[id].R = Vec3d_t(0, 0, 0);
    }
    
    // ceil
    ASSERT(force_conf >= 0);    
    id = dom.N_Grain + TOP; tag = TOP_WALL_TAG;
    sout.str(""); sout << "Vx = " << Vshear << " Ry = 0.0 PHIX = 0 PHIY = 0 PHIZ = 0 FZ = " << -force_conf; 
    s_bc = sout.str(); dictionary::add_tagged_values(dom.single_tagger.dict_,tag, s_bc); dom.particle[id].bcTag = tag; 
    if(true == updatePosFlag) {
      dom.particle[id].R = Vec3d_t(0, 0, dom.length(2)); 
    }
    
    print_log("# DONE : CONFIGURING   2D BOX SHEAR PERIODIC    SET UP");
  } 

  void 
  NumericalDevices::box_2D_only_ground(WorkspaceDEM & dom, bool updatePosFlag) 
  {
    ASSERT(2 == dom.dim);
    print_log("# CONFIGURING   2D BOX ONLY GROUND    SET UP");

    box_2D(dom, updatePosFlag);
    dom.particle[dom.N_Grain + TOP].alive = dom.particle[dom.N_Grain + LEFT].alive = dom.particle[dom.N_Grain + RIGHT].alive = false;  

    // local vars
    Vec3d_t Nvec;
    size_t id; 
    int tag;
    string s_bc; 
    stringstream sout;
    
    // floor 
    id = dom.N_Grain + BOTTOM; tag = BOTTOM_WALL_TAG;
    s_bc = "Rx = 0.0 Ry = 0.0 Rz = 0.0 PHIX = 0 PHIY = 0 PHIZ = 0"; dictionary::add_tagged_values(dom.single_tagger.dict_,tag, s_bc); dom.particle[id].bcTag = tag; 
    if(true == updatePosFlag) {
      dom.particle[id].R = Vec3d_t(0, 0, 0);
    }
    
   // l-wall
    id = dom.N_Grain + LEFT; tag = LEFT_WALL_TAG;
    sout.str(""); sout << "Vx = 0.0 Vy = 0.0 Vz = 0 WX = 0 WY = 0 WZ = 0";  
    s_bc = sout.str(); dictionary::add_tagged_values(dom.single_tagger.dict_,tag, s_bc); dom.particle[id].bcTag = tag; 
    if(true == updatePosFlag) {
      dom.particle[id].R = Vec3d_t(-20, 0, 0); 
    }
  
    // r-wall
    id = dom.N_Grain + RIGHT; tag = RIGHT_WALL_TAG;
    sout.str(""); sout << "Vx = 0.0 Vy = 0.0 Vz = 0 WX = 0 WY = 0 WZ = 0";  
    s_bc = sout.str(); dictionary::add_tagged_values(dom.single_tagger.dict_,tag, s_bc); dom.particle[id].bcTag = tag; 
    if(true == updatePosFlag) {
      dom.particle[id].R = Vec3d_t(dom.length(0), 0, 0);
      //dom.particle[id].R = Vec3d_t(20, 0, 0);
    }

    print_log("# DONE : CONFIGURING   2D BOX ONLY GROUND    SET UP");
  } 
  
  void 
  NumericalDevices::box_2D_shear_periodic_pull(WorkspaceDEM & dom, const double & force_conf, const double & Vshear, 
					       const int id, double fpull, bool updatePosFlag) 
  {
    box_2D_shear_periodic(dom, force_conf, Vshear, updatePosFlag);
    ASSERT(fpull > 0); dom.particle[id].F_fixed[0] = fpull;
  }

  void 
  NumericalDevices::box_2D(WorkspaceDEM & dom, bool updatePosFlag) 
  {
    ASSERT(2 == dom.dim);
    print_log("# CONFIGURING   2D BOX    SET UP");
    Vec3d_t Nvec;
    size_t id;
    int tag;
    string s_bc; 
    stringstream sout;

    dom.single_tagger.dict_.clear();

    // floor 
    id = dom.N_Grain + BOTTOM; tag = BOTTOM_WALL_TAG;
    sout.str(""); sout << "Vx = 0.0 Vy = 0.0 Vz = 0 WX = 0 WY = 0 WZ = 0";  
    s_bc = sout.str(); dictionary::add_tagged_values(dom.single_tagger.dict_,tag, s_bc); dom.particle[id].bcTag = tag; 
    if(true == updatePosFlag) {
      dom.particle[id].R = Vec3d_t(0, 0, 0);
    }

    // ceil
    id = dom.N_Grain + TOP; tag = TOP_WALL_TAG;
    sout.str(""); sout << "Vx = 0.0 Vy = 0.0 Vz = 0 WX = 0 WY = 0 WZ = 0";  
    s_bc = sout.str(); dictionary::add_tagged_values(dom.single_tagger.dict_,tag, s_bc); dom.particle[id].bcTag = tag; 
    if(true == updatePosFlag) {
      dom.particle[id].R = Vec3d_t(0, 0, dom.length(2)); 
    }
  
    // l-wall
    id = dom.N_Grain + LEFT; tag = LEFT_WALL_TAG;
    sout.str(""); sout << "Vx = 0.0 Vy = 0.0 Vz = 0 WX = 0 WY = 0 WZ = 0";  
    s_bc = sout.str(); dictionary::add_tagged_values(dom.single_tagger.dict_,tag, s_bc); dom.particle[id].bcTag = tag; 
    if(true == updatePosFlag) {
      dom.particle[id].R = Vec3d_t(0, 0, 0); 
    }
  
    // r-wall
    id = dom.N_Grain + RIGHT; tag = RIGHT_WALL_TAG;
    sout.str(""); sout << "Vx = 0.0 Vy = 0.0 Vz = 0 WX = 0 WY = 0 WZ = 0";  
    s_bc = sout.str(); dictionary::add_tagged_values(dom.single_tagger.dict_,tag, s_bc); dom.particle[id].bcTag = tag; 
    if(true == updatePosFlag) {
      dom.particle[id].R = Vec3d_t(dom.length(0), 0, 0);
    }

    print_log("# DONE : CONFIGURING   2D BOX    SET UP");
  } 

  void 
  NumericalDevices::box_2D_isotropic(WorkspaceDEM & dom, const double force_conf, bool updatePosFlag) 
  {
    ASSERT(2 == dom.dim);
    ASSERT(false == dom.periodic_x());
    print_log("# CONFIGURING   2D BOX ISOTROPIC    SET UP");

    Vec3d_t Nvec;
    size_t id; 
    int tag;
    string s_bc; 
    stringstream sout;
    dom.single_tagger.dict_.clear();
  
    box_2D(dom, updatePosFlag);

    // ceil
    ASSERT(force_conf > 0);
    id = dom.N_Grain + TOP; tag = TOP_WALL_TAG;
    sout.str(""); sout << "Rx = 0.0 Ry = 0.0 WX = 0 WY = 0 WZ = 0 Fz = " << -force_conf;
    s_bc = sout.str(); dictionary::add_tagged_values(dom.single_tagger.dict_,tag, s_bc); dom.particle[id].bcTag = tag; 
  
    // l-wall
    id = dom.N_Grain + LEFT; tag = LEFT_WALL_TAG;
    s_bc = "Rx = 0 Ry = 0.0 Rz = 0.0 WX = 0 WY = 0 WZ = 0"; dictionary::add_tagged_values(dom.single_tagger.dict_,tag, s_bc); dom.particle[id].bcTag = tag; 
  
    // r-wall
    ASSERT(force_conf > 0);
    id = dom.N_Grain + RIGHT; tag = RIGHT_WALL_TAG;
    sout.str(""); sout << "Ry = 0.0 Rz = 0.0 WX = 0 WY = 0 WZ = 0 Fx = " << -force_conf;
    s_bc = sout.str(); dictionary::add_tagged_values(dom.single_tagger.dict_,tag, s_bc); dom.particle[id].bcTag = tag; 

    print_log("# DONE : CONFIGURING   2D BOX ISOTROPIC    SET UP");
  } 

  void 
  NumericalDevices::box_2D_biaxial(WorkspaceDEM & dom, const double & force_confx, const double & Vz, bool updatePosFlag) 
  {
    ASSERT(2 == dom.dim);
    ASSERT(false == dom.periodic_x());
    print_log("# CONFIGURING   2D BOX  BIAXIAL   SET UP");
    Vec3d_t Nvec;
    size_t id; 
    int tag;
    string s_bc; 
    stringstream sout;

    dom.single_tagger.dict_.clear();
  
    box_2D_isotropic(dom, force_confx, updatePosFlag);

    // ceil
    ASSERT(Vz >= 0);
    if (Vz > 0) {
      id = dom.N_Grain + TOP; tag = TOP_WALL_TAG; 
      sout.str(""); sout << "Rx = 0.0 Ry = 0.0 Vz = " << -Vz << " WX = 0 WY = 0 WZ = 0 ";
      s_bc = sout.str(); dictionary::add_tagged_values(dom.single_tagger.dict_, tag, s_bc); dom.particle[id].bcTag = tag; 
    }

    print_log("# DONE : CONFIGURING   2D BOX  BIAXIAL   SET UP");
  } 

} // end of namespace DEM


#endif // NUMERICAL_DEVICES
