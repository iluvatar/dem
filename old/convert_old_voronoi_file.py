#!/usr/bin/env python

import sys, shutil

filename = sys.argv[1]
infile = open(filename)
values = []
for line in infile:
    vals = line.split()
    values.append(vals[2])
infile.close()
shutil.move(filename, filename+'.old')
outf = open(filename, 'w')
for value in values:
    outf.write('{0:>15.6f}\n'.format(float(value))) 
outf.close()
