#include <iostream>
#include <cstdlib>
#include "../random.hh"
#include "../histogram1d.hh"
//using namespace std;

int main(void)
{
  CRandom ran2(0);
  srand48(0);
  CHistogram1D linearHisto, logHisto;
  double xmin = 1.0, xmax = 100.0;
  linearHisto.Construct(xmin, xmax, int(30), true);
  logHisto.Construct(xmin, xmax, int(30), false);

  double tau = 10.0;
  double a = 10.0, b = 90;
  double mu=50, sigma = 10.0;
  for(double i = 0; i < 1.0e8; i++) {
    //double num = ran2.exponential(tau);
    //double num = -tau*log(1-drand48());
    //double num = ran2.uniform(a, b);
    double num = ran2.gauss(mu, sigma);
    linearHisto.Count(num);
    logHisto.Count(num);
  }
  linearHisto.Print("linearHisto.txt");
  logHisto.Print("logHisto.txt");

  return 0;
}
