#!/usr/bin/env python

import os
import sys
import fnmatch
import shutil
import numpy as np
from math import sqrt
import subprocess as SP

try:
    ROOT_PATH = sys.argv[1]
except:
    print('Error')
    print('Usage: '+sys.argv[0]+' BASE_DIR  ')
    print('BASE_DIR: Directory containing the files related to this computation, like POSTPRO or PARAM-AVERAGED/POSTPRO.')
    sys.exit(1)
if not os.path.isdir(ROOT_PATH):
    print('Path : ' + ROOT_PATH + ' does not exist')
    sys.exit(1)

# call average script
print "Calling average script first ..."
AVERAGE_PATH=ROOT_PATH.replace("PARAM-AVERAGED/POSTPRO/", "./")
pls = SP.Popen('average_postpro_values.py '+ AVERAGE_PATH, shell=True, stdout=SP.PIPE, stderr=SP.PIPE)
pls.communicate()
print "DONE :: Calling average script first ..."

# set datafile names
# '-normal' suffix means typical mean and sigma computation, no trust interval by t-student
VOLUME_FILE = ROOT_PATH + os.sep + 'histoVol.dat-normal' # extract 1 (mean) and 3 (sigma)
NGRAIN_FILE = ROOT_PATH + os.sep + 'ngrains_filtered_by_volume.dat' # extract 2 (mean)
VMIN_FILE = ROOT_PATH + os.sep + 'vmin_cubic.dat' # extract 2 (mean)


# Extract volume mean and sigma 
cmd = 'tail -n 1 ' + VOLUME_FILE + ' | awk \'{ print $1 }\' '
pls = SP.Popen(cmd, shell=True, stdout=SP.PIPE, stderr=SP.PIPE)
stdout, stderr = pls.communicate()
mean_vol = float(stdout)
cmd = 'tail -n 1 ' + VOLUME_FILE + ' | awk \'{ print $3 }\' '
pls = SP.Popen(cmd, shell=True, stdout=SP.PIPE, stderr=SP.PIPE)
stdout, stderr = pls.communicate()
sigma_vol = float(stdout)
print 'mean Vol    = %25.16le' % mean_vol
print 'sigma Vol   = %25.16le' % sigma_vol

# Extract ngrain
cmd = 'tail -n 1 ' + NGRAIN_FILE + ' | awk \'{ print $2 }\' '
pls = SP.Popen(cmd, shell=True, stdout=SP.PIPE, stderr=SP.PIPE)
stdout, stderr = pls.communicate()
ngrain = float(stdout)
print 'ngrain      = %25.16le' % ngrain

# Extract Vmin cubic = ngrain*d^3/sqrt(2)
cmd = 'tail -n 1 ' + VMIN_FILE + ' | awk \'{ print $2 }\' '
pls = SP.Popen(cmd, shell=True, stdout=SP.PIPE, stderr=SP.PIPE)
stdout, stderr = pls.communicate()
min_vol = float(stdout)
print 'vmin cubic  = %25.16le' % min_vol

# Compute k
if sigma_vol > 0 :
    k = np.power((mean_vol - min_vol)/sigma_vol, 2)
else :
    k = 0
print 'K           = %25.16le' % k
print 'K0          = %25.16le' % float(k/ngrain)

# Compute X
X = sigma_vol**2 / (mean_vol - min_vol)
print 'X/Nd^3      = %25.16le' % float(X/ngrain*d^3)

# Compute lambda**3
lambda_cube = min_vol/100.0
print 'lambda_cube = %25.16le' % lambda_cube

# Compute pressure P
if X > 0.0 :
    p = -np.log(X/lambda_cube)
else :
    p = 0.0
print 'p           = %25.16le' % p

# print to file
FILE = open(ROOT_PATH + os.sep + 'voronoi_global_p.dat', "w")
FILE.write(str(p) + '\n')
FILE.close()

FILE = open(ROOT_PATH + os.sep + 'voronoi_global_X.dat', "w")
FILE.write(str(X/min_vol) + '\n')
FILE.close()

FILE = open(ROOT_PATH + os.sep + 'voronoi_global_K.dat', "w")
FILE.write(str(k) + '\n')
FILE.close()

FILE = open(ROOT_PATH + os.sep + 'voronoi_global_K0.dat', "w")
FILE.write(str(k/ngrain) + '\n')
FILE.close()
