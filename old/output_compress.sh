#!/bin/bash

if [ "" == "$1" ]; then
    echo "ERROR. Usage : $0 dir_name"
    exit 1
fi

echo "This script will compress each output file individually and delete the original: filename -> filename.tar.bz2"
echo "Processing files inside dir $1"

FULL_PATH="$1"
if [ -d "${1}/OUTPUT" ]; then
    FULL_PATH="$1/OUTPUT"
fi

for pattern in bodiesInfo contactsInfo; do
    for a in "${FULL_PATH}/${pattern}"*.txt; do
        tar cvjf "$a".tar.bz2 "$a" && rm -f "$a"
    done
done

echo "DONE."

