#include <iostream>
#include "../../headers/post_pro.hh"
//using namespace std;

int main(void)
{
  vector<string> contactFilenames, bodiesFilenames; 
  file_names("contactsInfo-*.dat", contactFilenames);
  file_names("bodiesInfo-*.dat", bodiesFilenames);
  system("mkdir output-TEXT 2>/dev/null");
  system("cp output/*.txt output/*.xml output-TEXT/ ");
  string textFilename;
  size_t i, size = std::min(contactFilenames.size(), bodiesFilenames.size());
  for(i = 0; i < size; ++i) {
    clog << "# Processing : " << bodiesFilenames[i] << " , " << contactFilenames[i] ;
    WorkspaceDEM dom(bodiesFilenames[i], "input/configFile.xml");
    dom.read_contacts_info(contactFilenames[i]);
    dom.set_print_ready_flag(true);
    textFilename = bodiesFilenames[i];
    replace_all(textFilename, "output", "output-TEXT");
    replace_all(textFilename, ".dat", ".txt");
    dom.print_bodies_info(textFilename);
    textFilename = contactFilenames[i];
    replace_all(textFilename, "output", "output-TEXT");
    replace_all(textFilename, ".dat", ".txt");
    dom.print_contacts_info(textFilename);
    clog << " ...  Done. " << endl;
  }
  system("ln -s output output-DAT");
  return 0;
}

