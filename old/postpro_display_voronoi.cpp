//--------------------------------------------------------------------------
// Plot voronoi in  gnuplot
//--------------------------------------------------------------------------

#include "../inc/postpro_display.hpp"

int main()
{
  DEM::PostproDisplay postprocessor;
  postprocessor.draw_voronoi();

  return 0;
}
