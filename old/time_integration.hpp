#ifndef __INTEGRATION__
#define __INTEGRATION__

#include "utility.hpp"

// translational: requires R, V, F vectors and mass on each object
// rotational: requires PHI, W, T vectors and inerI on each object

namespace DEM {
  namespace time_integ {
    
    // Chooose time integration procedure    
#define LEAPFROG  1
#define VERLET    0
#define PVOPT     0
#define VVOPT     0
    
#define OPT_VAL 0.1931833275037836
    
    //---------------------------------------------------
    // Functor to start the time integration
    //---------------------------------------------------
    struct TimeIntegrationStart {
    public: // methods
      explicit TimeIntegrationStart(double & dt) : dt_(dt) {};
      template <class T> void operator()(T & object) {
#if LEAPFROG
	object.V -= 0.5*dt_*object.F/object.mass;  // V(-dt/2) = V(0) -dt/2*a(0)
	object.W -= 0.5*dt_*object.T/object.inerI; // V(-dt/2) = V(0) -dt/2*a(0)
	//object.Vaux_ = object.V - 0.5*dt_*object.F/object.mass;  // V(-dt/2) = V(0) -dt/2*a(0)
	//object.Waux_ = object.W - 0.5*dt_*object.T/object.inerI; // V(-dt/2) = V(0) -dt/2*a(0)
#elif VERLET
	object.Rold_ = object.R - object.V*dt_ + object.F*(0.5*dt_*dt_/object.mass); 
	object.PHIold_ = object.PHI - object.W*dt_ + object.T*(0.5*dt_*dt_/object.inerI); 
#endif
      }
      
    private: // data
      double dt_;
    };
  
    //---------------------------------------------------
    // Predictor functor
    //---------------------------------------------------
    struct Predict {
      Predict() : dt_(0) {};
      explicit Predict(double & dt) : dt_(dt) {};
      void set_dt(double & dt) {dt_ = dt;};
      double dt() {return dt_;};
      template <class T> void operator()(T & object, const double & vmax = 1.0e100) {
#if LEAPFROG
	// Vuax is used to keep V at time t, t + dt, but it does not work if the velocity is fixed
	object.V += dt_*(object.F/object.mass);  // V(t+dt/2) = V(t-dt/2) + dt*a(t) 
	//object.Vaux_ += dt_*(object.F/object.mass);  // V(t+dt/2) = V(t-dt/2) + dt*a(t) 
	//object.V = object.Vaux_ + 0.5*dt_*(object.F/object.mass);  // V(t+dt) = V(t+dt/2) + 0.5*dt*a(t) 
	if (vmax < 1.0e20) {
	  const double vnorm2 = object.V.squaredNorm();
	  if (vnorm2 > SQUARE(vmax) && vnorm2 > 0) {
	    object.V *= vmax/std::sqrt(vnorm2);  
	    //object.Vaux_ = object.V; 
	  }
	}
	object.R += dt_*object.V;                     // R(t+dt) = R(t) + dt*V(t+dt/2)
	//object.R += dt_*object.Vaux_;               // R(t+dt) = R(t) + dt*V(t+dt/2)
	//object.Waux_ += dt_*(object.T/object.inerI);  // V(t+dt/2) = V(t-dt/2) + dt*a(t) 
	object.W += dt_*(object.T/object.inerI);  // V(t+dt/2) = V(t-dt/2) + dt*a(t) 
	object.PHI += dt_*object.W;             // R(t+dt) = R(t) + dt*V(t+dt/2)
	//object.W = object.Waux_ + 0.5*dt_*(object.T/object.inerI);  // W(t) = W(t+dt/2) + 0.5*dt*alpha(t) 
	//object.PHI += dt_*object.Waux_;             // R(t+dt) = R(t) + dt*V(t+dt/2)
#elif VERLET
	if (vmax != 1.0e100) { print_error_and_exit("Cannot set maximum velocity with verlet"); }
	static Vec3d_t Aux;
	Aux = object.R;
	object.R = object.R*2 - object.Rold_ + object.F*(dt_*dt_/object.mass);
	//object.V = (object.R - object.Rold_)/(2*dt_); // V(t), O(dt^2)
	object.V = (object.R - object.Rold_ + object.F*(2*dt_*dt_/object.mass))/(2*dt_); // V(t+dt), O(dt^2)
	object.Rold_ = Aux;
	Aux = object.PHI;
	object.PHI = object.PHI*2 - object.PHIold_ + object.T*(dt_*dt_/object.inerI);
	//object.W = (object.PHI - object.PHIold_)/(2*dt_); // W(t) , O(dt^2)
	object.W = (object.PHI - object.PHIold_ + object.T*(2*dt_*dt_/object.inerI))/(2*dt_); // W(t+dt), O(dt^2)
	object.PHIold_ = Aux;
#elif PVOPT
	if (vmax != 1.0e100) { print_error_and_exit("Cannot set maximum velocity with p-verlet"); }
	object.V += OPT_VAL*dt_*object.F/object.mass;
	object.R += 0.5*dt_*object.V;
	object.W += OPT_VAL*dt_*object.T/object.inerI;
	object.PHI += 0.5*dt_*object.W;
#elif VVOPT
	if (vmax != 1.0e100) { print_error_and_exit("Cannot set maximum velocity with v-verlet"); }
	object.R   += OPT_VAL*dt_*object.V;
	object.PHI += OPT_VAL*dt_*object.W;
#endif
      }
    
    private: // data
      double dt_;
    };

    //--------------------------------------------------------------------------------------------------------
    // Correct (more phases can be added) functor
    //--------------------------------------------------------------------------------------------------------
    struct Correct1 {
    public: // function
      Correct1() : dt_(0) {};
      explicit Correct1(double & dt) : dt_(dt) {};
      void set_dt(double & dt) {dt_ = dt;};
      template <class T> void operator()(T & object) {
#if PVOPT
	if (vmax != 1.0e100) { print_error_and_exit("Cannot set maximum velocity with p-verlet"); } 
	object.V += (1.0-2.0*OPT_VAL)*dt_*object.F/object.mass;
	object.R += 0.5*dt_*object.V;
	object.W += (1.0-2.0*OPT_VAL)*dt_*object.T/object.inerI;
	object.PHI += 0.5*dt_*object.W;
#elif VVOPT
	if (vmax != 1.0e100) { print_error_and_exit("Cannot set maximum velocity with v-verlet"); }
	object.V   += 0.5*dt_*object.F/object.mass;
	object.R   += (1.0-2.0*OPT_VAL)*dt_*object.V;
	object.W   += 0.5*dt_*object.T/object.inerI;
	object.PHI += (1.0-2.0*OPT_VAL)*dt_*object.W;
#endif
      }
      
    private: // data
      double dt_;
    };

    struct Correct2 {
    public: // function
      Correct2() : dt_(0) {};
      explicit Correct2(double & dt) : dt_(dt) {};
      void set_dt(double & dt) {dt_ = dt;};
      template <class T> void operator()(T & object) {
#if PVOPT
	if (vmax != 1.0e100) { print_error_and_exit("Cannot set maximum velocity with p-verlet"); }
	object.V += OPT_VAL*dt_*object.F/object.mass;
	object.W += OPT_VAL*dt_*object.T/object.inerI;
#elif VVOPT
	if (vmax != 1.0e100) { print_error_and_exit("Cannot set maximum velocity with v-verlet"); }
	object.V   += 0.5*dt_*object.F/object.mass;
	object.R   += OPT_VAL*dt_*object.V;
	object.W   += 0.5*dt_*object.T/object.inerI;
	object.PHI += OPT_VAL*dt_*object.W;
#endif
      }
      
    private: // data
      double dt_;
    };


    //--------------------------------------------------------------------------------------------------------
    // Simple Euler integration (mainly for CD)
    //--------------------------------------------------------------------------------------------------------
    template <class T>
    void Euler(const double & dt, T & object) 
    {
      object.Rold_ = object.R; 
      object.PHIold_ = object.PHI; 
      object.R += dt*object.V;                 // R(t+dt) = R(t) + dt*V(t)
      object.V += dt*(object.F/object.mass);   // V(t+dt) = V(t-dt) + dt*a(t) 
      object.PHI += dt*object.W;               // PHI(t+dt) = PHI(t) + dt*W(t)
      object.W += dt*(object.T/object.inerI);  // W(t+dt) = W(t-dt) + dt*a(t) 
    }
    

  } // namespace time_integ
} // namespace DEM

#endif
