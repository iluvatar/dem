#ifndef __PLANEWALL__
#define __PLANEWALL__

#include "particle.hh"
#include "vector3d.hh"

class PlaneWall : public Particle {
public:
  // new data members (besides the particles ones -> R, V, F, ...)
  Vec3d_t NormalVec;
  // methods
  explicit PlaneWall() : Particle(), NormalVec(Vecnull) {materialTag = 1;};
  friend ostream & operator<<(ostream & output, const PlaneWall & planeWall);
  friend istream & operator>>(istream & input, PlaneWall & planeWall);
  // serialization
  friend class boost::serialization::access; 
  template <typename Archive> 
  void serialize(Archive &ar, const unsigned int version) 
  { 
    ar & boost::serialization::base_object<Particle>(*this);
    ar & this->NormalVec;
  } 
};
/*
ostream & operator<<(ostream & output, const PlaneWall & planeWall)
{
  const Particle & basePtr = planeWall; // to call base Particle version  to read the inherited values
  output << basePtr ;
  output << planeWall.NormalVec << "\n";
  return output; // enables cout << x << y;
}

istream & operator>>(istream & input, PlaneWall & planeWall)
{
  Particle & basePtr = planeWall; // to use the base Particle version  to print the inherited values
  input >> basePtr;
  input >> planeWall.NormalVec;
  return input;
}
*/

#endif
