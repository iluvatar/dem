/// testing the new DEM 
#include <cstdlib>
#include <iostream>
#include <string>
#include <iomanip>
#include "../../headers/workspace_dem.hh"
#include "../../headers/numerical_devices.hh"
#include "../../headers/dictionary.hh"
//using namespace std;

//--------------------------------------------------------------------------------------------------
// Main
//--------------------------------------------------------------------------------------------------
int main (int argc, char **argv)
{
  //--------------------------------------------------------------------------------------------------
  // parameters
  
  //--------------------------------------------------------------------------------------------------
  // simulation
  DEM::WorkspaceDEM dom("input/bodiesInfo.txt", "input/general_conf");
  dom.read_contacts_info("input/contactsInfo.txt");
  DEM::NumericalDevices numDevice; numDevice.box_2D_only_ground(dom, false);
  dom.shift_grains_by(Vec3d_t(0, 0, 4));
  dom.start();  
  dom.evolve();
  //--------------------------------------------------------------------------------------------------
  // final printing
  dom.print_bodies_info("output/bodiesInfo.txt");
  dom.print_contacts_info("output/contactsInfo.txt");
  dom.print_config("output/general_conf");
  
  return 0;
}





