#include "../inc/contact.hpp"
#include <fstream>
#include <cstdio>
#include <cstdlib>

int main(int argc, char **argv)
{
  if (argc != 5) {
    std::fprintf(stderr, "Error calling the tool\n");
    std::fprintf(stderr, "Usage :\n");
    std::fprintf(stderr, "%s read_or_print ncontacts filename language_flag\n", argv[0]);
    std::fprintf(stderr, "read_or_write: 0 for read, 1 for write\n");
    std::fprintf(stderr, "language_flag: 0 for c, 1 for c++\n");
    std::exit(1);
  }

  const int lang = std::atoi(argv[4]);
  const int ioflag = std::atoi(argv[1]);
  std::FILE * fp;
  std::ofstream fout;
  std::ifstream fin;
  
  if (0 == lang) {
    if (0 == ioflag) fp = std::fopen(argv[3], "r");
    else if (1 == ioflag) fp = std::fopen(argv[3], "w");
  }
  else if (1 == lang) {
    if (0 == ioflag) fin.open(argv[3]);
    else if (1 == ioflag) fout.open(argv[3]);
  }

  DEM::Contact contact;
  contact.fill_inner_vars();

  for (int i = 0; i < std::atoi(argv[2]); ++i ) {
    if (0 == lang) {
      if (0 == ioflag) contact.read(fp); // c
      else if (1 == ioflag) contact.print(fp); // c
    }
    else if (1 == lang) {
      if (0 == ioflag) fin >> contact; // c++
      else if (1 == ioflag) fout << contact;  
    }
  }

  return 0;
}
