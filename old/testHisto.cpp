#include <iostream>
#include "../histogram1d.hh"
#include "../random.hh" 
//using namespace std;

int main(void)
{
  // declarar histogram: linear and log
  CHistogram1D histoLinear, histoLog;
  // construct them
  const double min = 1.0e-2, max = 1.0e3; 
  const int bins = 100;
  histoLinear.Construct(min, max, bins, true);
  histoLog.Construct(min, max, bins, false);
  // declarar crandom
  CRandom ran2(0);
  // llenar histograms with random exponential
  double data;
  for(int i = 0; i < 100000; i++) {
    data = ran2.exponential(100.0);
    //data = ran2.gauss(10.0, 5);
    //data = ran2.uniform(1.0, 20);
    histoLinear.Count(data);
    histoLog.Count(data);
  }
  // print each histogram
  histoLinear.Print("histoLinear.txt");
  histoLog.Print("histoLog.txt");

  return 0;
}

