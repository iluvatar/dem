#!/bin/bash

black_init="# \033[1;30m ";
red_init="# \033[1;31m ";
green_init="# \033[1;32m ";
yellow_init="# \033[1;33m ";
blue_init="# \033[1;34m ";
purple_init="# \033[1;35m ";
cyan_init="# \033[1;36m ";
lightgray_init="# \033[1;37m ";
color_end=" \033[0m ";

# ---------------------------------------------------
# check number of arguments
if [ $# -lt 2 ]; then
    echo "Error calling script."
    echo "Usage:"
    echo "$0 TEST_SOURCE.cpp PARAMETERS_VALUES"
    exit 1
fi

# ---------------------------------------------------
# check existence of the called test
if [ ! -e "$1" ]; then
    echo "Error."
    echo "Source file $1 does not exists. Exiting."
    exit 1;
fi

# ---------------------------------------------------
# Set global variables
TEST_SOURCE=$1
TEST_NAME=$(basename $1 .cpp)
echo "Test Source : $TEST_SOURCE"
echo "Test Name   : $TEST_NAME"
#VALS=(0.0 0.01 0.1 1.0 5.0 10.0 50.0 10000)
VALS=(${@:2}) # read from positional parameters
declare -a PIDS # to wait for process to finish

# ---------------------------------------------------
# enter the directory
cd $(pwd)

# ---------------------------------------------------
# functions
function make_headers_source_replacements() {
    message="Performing headers and source replacements"
    printf "\n$blue_init $message $color_end\n"
    ## change the relation among musf and mukf
    #sed -i.bck s/'mat\[.*MUKF.*;'/'mat\["MUKF"\] = mat\["MUSF"\]*'$1';'/ ../headers/materials.hpp
    # change the random seed to construct the sample 
    sed -i.bck s/'DEM::Initialization Initializer.*'/'DEM::Initialization Initializer\('$1'\);'/ examples/prepro_initializer.cpp     
    printf "\n$blue_init DONE: $message $color_end\n"
}

function make_input_config_replacements() {
    message="Performing input config replacements"
    printf "\n$blue_init $message $color_end\n"
    # CONFIG FILE
    configFN="input/general_conf"
    cp -f ../../$configFN $configFN
    # SIMULATION PARAMETERS FILE
    SIMUL_PARAM_FN="input/simulParameters_conf"
    cp -f ../../$SIMUL_PARAM_FN $SIMUL_PARAM_FN
    #sed -i.bck s/'FPULL.*'/"FPULL       $1"/ $SIMUL_PARAM_FN
    #sed -i.bck s/'ID_PULLED.*'/"ID_PULLED       $1"/ $SIMUL_PARAM_FN
    cat $SIMUL_PARAM_FN
    printf "\n$blue_init DONE: $message $color_end\n"
}

function make_input_config_replacements_relax() {
    message="Performing input config replacements RELAX"
    printf "\n$blue_init $message $color_end\n"
    make_input_config_replacements $1
    # SIMULATION PARAMETERS FILE
    SIMUL_PARAM_FN="input/simulParameters_conf"
    #sed -i.bck s/'FPULL.*'/"FPULL       $1"/ $SIMUL_PARAM_FN
    #sed -i.bck s/'ID_PULLED.*'/"ID_PULLED       $1"/ $SIMUL_PARAM_FN
    #tf_new=$(cat $SIMUL_PARAM_FN | grep TF | awk '{print $2}')  # Run relax by 1/10 of the total time 
    #tf_new=$(echo $tf_new/10 | bc -l)
    #sed -i.bck s/'TF.*'/"TF         $tf_new"/ $SIMUL_PARAM_FN
    cat $SIMUL_PARAM_FN
    printf "\n$blue_init DONE: $message $color_end\n"
}

function print_info() {
    #printf "$blue_init INFO: DON'T FORGET TO COMPILE YOUR CHOSEN POSTPRO EXECUTABLES OTHERWISE THIS SCRIPT WILL RUN ALL OF THEM $color_end\n" 
    sleep 4
}

function compile() {
    message="Compiling $1"
    printf "\n $green_init $message $color_end\n"
    ../scripts/compile.sh $1
    if [[ "0" != "$?" ]] ; then 
	printf "\n$red_init Error compiling sources. Exiting. $color_end \n"; 
	exit 1; 
    fi
    printf "\n $green_init DONE: $message $color_end\n"
}

function run_executable() {
    message="Running in background : $1 \n PARAM = $2"
    printf "$green_init $message $color_end\n"
    nohup time ./$1 &> $1-stdlog.txt &
    PIDS[$3]=$!
    if [[ "0" != "$?" ]]; then 
	printf "$red_init ERROR running $1. Printing log: $color_end\n"
	cat $1-stdlog.txt
    fi
    printf "$green_init DONE: $message $color_end\n"
}

function get_working_dir_name() {
    eval "$2=${TEST_NAME}-test/PARAM-$1/"
}

function create_dir_structure_copy_input() {
    message="Creating directory structure"
    printf "\n $blue_init $message $color_end \n"
    rm -rf ${TEST_NAME}-test
    i=0
    for val in ${VALS[*]}; do
	get_working_dir_name $val working_dir
	printf "$yellow_init WORKING_DIR NAME = $working_dir $color_end \n"
	rm -rf ${working_dir}
	mkdir -p ${working_dir}
	mkdir -p ${working_dir}/input
	mkdir -p ${working_dir}/output  
	cp -a input/* $working_dir/input/
    done    
    printf "\n $blue_init DONE: $message $color_end \n"
}

function wait_until_finished() {
    date
    printf "\n $green_init Waiting for processes to finish : $1 ... $color_end"
    for pid in ${PIDS[*]}; do 
	wait $pid
    done
    printf "$green_init DONE $1 $color_end\n"
    date
}

function copy_final_to_initial() {
    message="Copying final states to initial ones"
    printf "\n $blue_init $message $color_end\n"
    for val in ${VALS[*]}; do
	get_working_dir_name $val working_dir
	cd $working_dir
	../../../scripts/copyFinalToInitial.sh
	cd ../../
    done
    printf "\n DONE: $blue_init $message $color_end\n"
}

function run_prepro() {
    message=$2
    printf "\n $blue_init PREPRO: $message $color_end\n"
    PIDS=()
    i=0
    basefilename=$(basename $1 .cpp)
    for val in ${VALS[*]}; do
	get_working_dir_name $val working_dir
	make_headers_source_replacements $val
	compile examples/${basefilename}.cpp
	mv ${basefilename}.x $working_dir/
	cd $working_dir
	make_input_config_replacements $val
	run_executable ./${basefilename}.x $val $i 
	i=$(($i+1))
	cd ../../
    done
    wait_until_finished "$2" 
    copy_final_to_initial
    printf "\n $blue_init DONE PREPRO: $message $color_end\n"
}

function run_tests() {
    message="Compiling tests and running"
    printf "\n $green_init $message $color_end \n"
    PIDS=()
    i=0
    for val in ${VALS[*]}; do
	get_working_dir_name $val working_dir
	exe_name=${TEST_NAME}-PARAM-${val}.x
	make_headers_source_replacements $val
	compile $TEST_SOURCE
	mv ${TEST_NAME}.x $working_dir/$exe_name
	cd $working_dir
	make_input_config_replacements $val
	run_executable $exe_name $val $i
	i=$(($i+1))
	cd ../../
    done
    wait_until_finished " simulations "
    printf "\n $green_init DONE: $message $color_end \n"
}

function run_tests_to_relax() {
    message="Compiling tests and running tests to relax"
    printf "\n $green_init $message $color_end \n"
    PIDS=()
    i=0
    for val in ${VALS[*]}; do
	get_working_dir_name $val working_dir
	exe_name=${TEST_NAME}-PARAM-${val}.x
	compile $TEST_SOURCE 
	mv ${TEST_NAME}.x $working_dir/$exe_name
	cd $working_dir
	make_input_config_replacements_relax $val
	run_executable $exe_name $val $i
	i=$(($i+1))
	cd ../../
    done
    wait_until_finished " simulations in relax (initial sample)"
    copy_final_to_initial
    printf "\n $green_init DONE: $message $color_end \n"
}



function postpro_data() {
    message="Postprocessing data"
    printf "\n$green_init $message $color_end \n"
    ../scripts/compile.sh examples/postpro_main.cpp
    PIDS=()
    i=0
    for val in ${VALS[*]}; do
	get_working_dir_name $val working_dir
	cd $working_dir
	rm -rf postpro
	mkdir -p postpro
	cp ../../postpro/postpro_conf postpro/
	cp input/simulParameters_conf postpro/
	cp input/general_conf postpro/
	cp output/bodiesInfo.txt postpro/
	cp output/contactsInfo.txt postpro/
	cp nohup* postpro/ 2>/dev/null
	cp *-stdlog.txt postpro/
        # Do not put the post_pro in the background because reading will be really slow
	time ../../postpro_main.x >> postpro/postpro-stdlog.txt 
	PIDS[0]=$!
	cd ../../
    done
    wait_until_finished " postpro "  
    printf "\n$green_init DONE:  $message $color_end \n"
}

function send_results() {
    message="Sending results"
    printf "\n $green_init $message $color_end \n"
    cd $TEST_NAME-test/
    for val in ${VALS[*]}; do
	name=PARAM-$val
	zip -r ${name}.zip $name/postpro 
	mutt -a ${name}.zip -s "UNAL - DATA $TEST_NAME $name $(date) $HOSTNAME" wfoquendop@unal.edu.co < /dev/null 
	mutt -a ${name}.zip -s "GMAIL - DATA $TEST_NAME $name $(date) $HOSTNAME" woquendo@gmail.com < /dev/null 
	if [[ "$?" != "0" ]]; then 
	    echo <<EOF > /tmp/tmp.txt
              ZIP_FILE_SIZE = $(ls -lh ${name}.zip)
              DIRECTORY_LIST = $(ls)
EOF
	      mutt -s "ERROR SENDING DATA : GMAIL - DATA $TEST_NAME $name $(date) $HOSTNAME" woquendo@gmail.com < /tmp/tmp.txt 
	fi	    
    done
    printf "\n $green_init DONE:  $message $color_end \n"
}

# ---------------------------------------------------
# different tests
function test_2D_shear() {
    print_info
    create_dir_structure_copy_input
    run_prepro examples/prepro_initializer.cpp "Initialization"
    run_prepro examples/prepro_create_top_bottom_grain_walls.cpp " Creating top-bottom grain walls"
    run_tests_to_relax
    run_prepro examples/prepro_reset_initial_positions.cpp "Resseting initial R0"
    run_prepro examples/prepro_freezer.cpp " Freezing "
    run_tests  
    postpro_data
    send_results
}

function test_2D_biaxial() {
    print_info
    create_dir_structure_copy_input
    run_prepro examples/prepro_initializer.cpp "Initialization"
    run_tests_to_relax
    run_prepro examples/prepro_freezer.cpp " Freezing "
    run_tests  
    postpro_data
    send_results
}

function test_2D_isotropic() {
    print_info
    create_dir_structure_copy_input
    run_prepro examples/prepro_initializer.cpp "Initialization"
    run_tests  
    postpro_data
    send_results
}


# ---------------------------------------------------
# main
# ---------------------------------------------------

test_2D_shear
#test_2D_biaxial
#test_2D_isotropic
