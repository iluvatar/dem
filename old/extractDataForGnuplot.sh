#!/bin/bash

# check number of arguments
if [ $# -ne 2 ]; then 
    echo "Error calling script. "
    echo "Usage:"
    echo "$0 DIRNAME PARAM_NAME  " 
    echo "Example:"
    echo "$0 Data2010 FORCE_CONF "
    exit 1
fi

# SET NAMES and VARS
BASE="$1/" 
#FILESOUT=files_out_shear
#FILESOUT=files_out
#FILESOUT=output
FILESOUT=POSTPRO
SCRIPT_OUTPUT=$BASE/DISPLAY
rm -rf $SCRIPT_OUTPUT
PARAM_LABEL=$(echo "$2" | tr ' ' '_')
# uncompress the data
for a in $(ls $BASE/*.zip 2>/dev/null); do
    filename_base=$(basename $a .zip)
    rm -rf $BASE/$filename_base/POSTPRO
    unzip $a -d $BASE/
done

# extract parameters values
declare -a P_VALS
for a in $(ls $BASE); do
    if [ -d $BASE/$a ]; then
	if [[ "$BASE/$a" =~ PARAM-* ]]; then
	    VALUE=$(echo $a | sed s/PARAM-// | sed s/'\/'//)
	    idx=${#P_VALS[*]}
	    P_VALS[$idx]=$VALUE
	    idx=$(($idx + 1))
	fi
    fi
done
PARAM_PREFIX="PARAM-"
if [[ "0" == ${#P_VALS[*]} ]]; then
    PARAM_PREFIX=""
    P_VALS=("/")
fi
mkdir -p $SCRIPT_OUTPUT
touch $SCRIPT_OUTPUT/.empty
cd $SCRIPT_OUTPUT
SCRIPT_OUTPUT="."
BASE="$(pwd)/../"

cat > $SCRIPT_OUTPUT/header.gp <<EOF
reset
set term x11 enhanced
unset clip 
set macro
a = "(\$1)"
b = "(\$2)"
PARAM="${P_VALS[*]}"
TC = ""
BASE = "$BASE"
set style data lp
EOF

##-------------------------------------
# Voronoi Tessellation
echo "Plotting Voronoi Tessellations"
# generate string with plotting commands
gp_command=""
for ((idx=0; idx<${#P_VALS[*]}; idx++)); do
    tc=${P_VALS[$idx]}
    gp_command="${gp_command} ; set term x11 $idx; set title 'Voro cell, $PARAM_LABEL = $tc';  plot '${BASE}${PARAM_PREFIX}${tc}/${FILESOUT}/voronoi_p.gnu' u 2:4:5 w circles lc rgb \"blue\" fs transparent solid 0.15 t '' , '${BASE}${PARAM_PREFIX}${tc}/${FILESOUT}/voronoi_v.gnu' u 1:3 w l lw 1 lt 1 t '' "
done
NUM_ROWS="0"
if [[ "/" == "${P_VALS[0]}" ]]; then 
    NUM_ROWS="1"
else 
    NUM_ROWS=$(echo \(${#P_VALS[*]}+1\)/2 | bc)
fi
NUM_COL=2
if [[ "1" == "${#P_VALS[*]}" ]]; then
    NUM_COL=1
fi

# gnuplot script and graph
GPFILE=$SCRIPT_OUTPUT/voronoiTessellation.gp
cat $SCRIPT_OUTPUT/header.gp > $GPFILE
cat >> $GPFILE <<EOF
FILE = "${FILESOUT}/voronoi_p.gnu"
#set xlabel "x"
#set ylabel "z"
unset xlabel
unset ylabel
set size ratio -1
set origin 0,0
#set multiplot layout $NUM_ROWS,$NUM_COL rowsfirst scale 1.1,1.1 
$gp_command 
#unset multiplot
EOF
gnuplot -persists < $GPFILE
#read

##-------------------------------------
# Voronoi volume
echo "Processing Volume (Voronoi and postpro::volume)"
GPFILE=$SCRIPT_OUTPUT/volume_by_voronoi_by_box.gp
cat $SCRIPT_OUTPUT/header.gp > $GPFILE
cat >> $GPFILE <<EOF
FILE = "${FILESOUT}/volume_by_voronoi_by_box.txt"
set key r t
set title 'Volume of whole the packing'
set xlabel "Time"
set ylabel "Volume"
plot for [TC in PARAM] BASE.'${PARAM_PREFIX}'.TC.'/'.FILE u 1:2  w lp pt 3 t 'voro, $PARAM_LABEL = '.TC  ,  \
for [TC in PARAM] BASE.'${PARAM_PREFIX}'.TC.'/'.FILE u 1:3  w lp pt 4 t 'function, $PARAM_LABEL = '.TC   
EOF
gnuplot -persists < $GPFILE
#read

##-------------------------------------
# Stress
echo "Processing Sigma (Stress)"
GPFILE=$SCRIPT_OUTPUT/sigma.gp
cat $SCRIPT_OUTPUT/header.gp > $GPFILE
cat >> $GPFILE <<EOF
FILE = "${FILESOUT}/sigma.txt"
set key r t
set title 'Stress Eigen Values'
set xlabel "Time"
set ylabel "Stress"
plot for [TC in PARAM] BASE.'${PARAM_PREFIX}'.TC.'/'.FILE u 1:2  w lp pt 3 t '$PARAM_LABEL = '.TC  ,  \
for [TC in PARAM] BASE.'${PARAM_PREFIX}'.TC.'/'.FILE u 1:3  w lp pt 4 t '$PARAM_LABEL = '.TC   ,  \
for [TC in PARAM] BASE.'${PARAM_PREFIX}'.TC.'/'.FILE u 1:4  w lp pt 5 t '$PARAM_LABEL = '.TC 
EOF
gnuplot -persists < $GPFILE
#read


##-------------------------------------
# Stress symmetric deviation
echo "Processing Stress Symmetric Deviation"
GPFILE=$SCRIPT_OUTPUT/stress_symmetric_deviation.gp
cat $SCRIPT_OUTPUT/header.gp > $GPFILE
cat >> $GPFILE <<EOF
FILE = "${FILESOUT}/sigma.txt"
set key r t
set title 'Stress Symmetric Deviation'
set xlabel "Time"
set ylabel "Symmetric Deviation"
plot for [TC in PARAM] BASE.'${PARAM_PREFIX}'.TC.'/'.FILE u 1:14  w lp pt 3 t '$PARAM_LABEL = '.TC 
EOF
gnuplot -persists < $GPFILE
#read

##-------------------------------------
# Mean penetration among particles
echo "Processing mean penetration"
GPFILE=$SCRIPT_OUTPUT/meanPenetration.gp
cat $SCRIPT_OUTPUT/header.gp > $GPFILE
cat >> $GPFILE <<EOF
FILE = "${FILESOUT}/meanPenetration.txt"
set key r t
set title 'Mean penetration'
a = "(\$1)"
b = "(\$2)"
c = "(\$3)"
set xlabel "time"
set ylabel "Penetration (mean and max)"
plot for [TC in PARAM] BASE.'${PARAM_PREFIX}'.TC.'/'.FILE u @a:@b t 'mean, $PARAM_LABEL = '.TC, \
for [TC in PARAM] BASE.'${PARAM_PREFIX}'.TC.'/'.FILE u @a:@c t 'max, $PARAM_LABEL = '.TC
EOF
gnuplot -persists < $GPFILE
#read

##-------------------------------------
# Volume histogram 
echo "Processing Volume histograms"
GPFILE=$SCRIPT_OUTPUT/volHisto.gp
cat $SCRIPT_OUTPUT/header.gp > $GPFILE
cat >> $GPFILE <<EOF
FILE = "${FILESOUT}/histoVol.txt"
set key r t
set title 'Histogram of Volume'
a = "(\$1)"
b = "(\$2)"
set xlabel "Volume/<meanRad vol>"
set ylabel "PDF"
plot for [TC in PARAM] BASE.'${PARAM_PREFIX}'.TC.'/'.FILE u @a:@b t '$PARAM_LABEL = '.TC
EOF
gnuplot -persists < $GPFILE
#read

##-------------------------------------
# Voronoi Volume histogram
echo "Processing Voronoi Volume histograms"
gp_command="1/0 t ''"
for ((idx=0; idx<${#P_VALS[*]}; idx++)); do
    tc=${P_VALS[$idx]}
    #K=$(cat $BASE${PARAM_PREFIX}$tc/postpro/voronoi_K_X.txt | grep K | awk '{print $3}')
    #xmin=$(cat $BASE${PARAM_PREFIX}$tc/postpro/voronoi_K_X.txt | grep xmin | awk '{print $3}')
    #xmean=$(cat $BASE${PARAM_PREFIX}$tc/postpro/voronoi_K_X.txt | grep xmean | awk '{print $3}')
    #python ../../../convert_old_voronoi_file.py $BASE${PARAM_PREFIX}$tc/postpro/voronoi_K_X.txt
    K=$(cat $BASE${PARAM_PREFIX}$tc/postpro/voronoi_K_X.txt | awk '{ if (NR==4) print $1}')
    xmin=$(cat $BASE${PARAM_PREFIX}$tc/postpro/voronoi_K_X.txt | awk '{ if (NR==1) print $1}')
    xmean=$(cat $BASE${PARAM_PREFIX}$tc/postpro/voronoi_K_X.txt |  awk '{ if (NR==2) print $1}')
    gp_command="${gp_command} , '$BASE${PARAM_PREFIX}$tc/'.FILE u @a:@b t '$PARAM_LABEL = $tc' w lp , f(x, $K, $xmin, $xmean) lw 2 "
done
GPFILE=$SCRIPT_OUTPUT/volVoroHisto.gp
cat $SCRIPT_OUTPUT/header.gp > $GPFILE
cat >> $GPFILE <<EOF
FILE = "${FILESOUT}/histoVoroVol.txt"
set key r t
set title 'Histogram of Voronoi Volume'
f(x,k, xmin, xmean) = (k**k/gamma(k))*((x-xmin)**(k-1)/(xmean - xmin)**k)*exp(-k*(x-xmin)/(xmean - xmin))
a = "(\$1)"
b = "(\$2)"
set xlabel "Volume (Area in 2d)"
set ylabel "PDF"
plot $gp_command
EOF
gnuplot -persists < $GPFILE
#read

# ##-------------------------------------
# # ANISOTROPY
# echo "Procesing anisotropy"
# # compute mean
# rm -f $SCRIPT_OUTPUT/anisotropy.txt
# #for tc in ${P_VALS[*]}; do
# for ((idx=0; idx<${#P_VALS[*]}; idx++)); do
#     tc=${P_VALS[$idx]}
#     FILE=$FILESOUT/fabric.txt
#     cat "$BASE/${PARAM_PREFIX}$tc/$FILE" | awk '{print $1, $2, $3}' > tmp.txt
#     NR=$(wc -l tmp.txt | awk '{print $1}') 
#     mean=0
#     sigma=inf
#     if [ "$NR" -ne "0" ]; then 
# 	mean=$(cat tmp.txt | awk '{s += sqrt(($3-$2)*($3-$2))} END {print s/NR}')
# 	mean2=$(cat tmp.txt | awk '{s += ($3-$2)*($3-$2)} END {print s/NR}')
# 	sigma=$(echo "sqrt(($mean2 - $mean*$mean)/$NR)" | bc -l)
# 	mean=$(echo $mean*2 | bc -l)
# 	sigma=$(echo $sigma*6 | bc -l)
#     fi
#     if [[ "/" != "$tc" ]]; then 
# 	echo "$tc   $mean    $sigma " >> $SCRIPT_OUTPUT/anisotropy.txt
#     else 
# 	echo "-1   $mean    $sigma " >> $SCRIPT_OUTPUT/anisotropy.txt
#     fi
# done
# # graph
# GPFILE=$SCRIPT_OUTPUT/anisotropy.gp
# cat $SCRIPT_OUTPUT/header.gp > $GPFILE
# cat >> $GPFILE <<EOF
# FILE = "$SCRIPT_OUTPUT/anisotropy.txt"
# set key r b
# set title 'Anisotropy'
# a = "(\$1)"
# b = "(\$2)"
# set xlabel "PARAM"
# set ylabel "a"
# plot FILE u 1:2:3 w yerrorlines 
# EOF
# gnuplot -persists < $GPFILE
# rm -f tmp.txt
# #read

# ##-------------------------------------
# # COORDINATION NUMBER
# echo "Processing coordination number"
# # compute mean
# rm -f $SCRIPT_OUTPUT/meanz.txt
# #for tc in ${P_VALS[*]}; do
# for ((idx=0; idx<${#P_VALS[*]}; idx++)); do
#     tc=${P_VALS[$idx]} 
#     FILE=$FILESOUT/fabric.txt
#     cat "$BASE/${PARAM_PREFIX}$tc/$FILE" | awk '{print $1, $2, $3, $4}' > tmp.txt
#     NR=$(wc -l tmp.txt | awk '{print $1}') 
#     meanz=0
#     sigma=inf
#     if [ "$NR" -ne "0" ]; then 
# 	meanz=$(cat tmp.txt | awk '{s += $2 + $3 + $4} END {print s/NR}')
# 	meanz2=$(cat tmp.txt | awk '{s += ($2 + $3 + $4)*($2 + $3 + $4)} END {print s/NR}')
# 	sigma=$(echo "sqrt(($meanz2 - $meanz*$meanz)/$NR)" | bc -l)
#     fi
#     if [[ "/" != "$tc" ]]; then 
# 	echo "$tc   $meanz    $sigma " >> $SCRIPT_OUTPUT/meanz.txt
#     else 
# 	echo "-1   $meanz    $sigma " >> $SCRIPT_OUTPUT/meanz.txt
#     fi
# done
# # graph
# GPFILE=$SCRIPT_OUTPUT/meanz.gp
# cat $SCRIPT_OUTPUT/header.gp > $GPFILE
# cat >> $GPFILE <<EOF
# FILE = "$SCRIPT_OUTPUT/meanz.txt"
# set key r b
# a = "(\$1)"
# b = "(\$2)"
# set title 'Mean coordination number'
# set xlabel "PARAM"
# set ylabel "<z>"
# plot FILE u 1:2:3 w yerrorlines 
# EOF
# gnuplot -persists < $GPFILE
# rm -f tmp.txt

# ##-------------------------------------
# # INTERNAL FRICTION
# echo "Processing internal friction"
# # compute mean
# rm -f $SCRIPT_OUTPUT/internalFriction.txt
# #for tc in ${P_VALS[*]}; do
# for ((idx=0; idx<${#P_VALS[*]}; idx++)); do
#     tc=${P_VALS[$idx]}
#     FILE=$FILESOUT/sigma.txt
#     cat "$BASE/${PARAM_PREFIX}$tc/$FILE" | awk '{if ($13>0) print $7/(-1*$13)}' > tmp.txt
#     NR=$(wc -l tmp.txt | awk '{print $1}')
#     mean=0
#     sigma=inf
#     if [ "$NR" -ne "0" ]; then 
# 	mean=$(cat tmp.txt | awk '{s += $1} END {print s/NR}')
# 	mean2=$(cat tmp.txt | awk '{s += $1*$1} END {print s/NR}')
# 	sigma=$(echo "sqrt(($mean2 - $mean*$mean)/$NR)" | bc -l)
#     fi
#     if [[ "/" != "$tc" ]]; then 
# 	echo "$tc   $mean    $sigma " >> $SCRIPT_OUTPUT/internalFriction.txt
#     else 
# 	echo "-1   $mean    $sigma " >> $SCRIPT_OUTPUT/internalFriction.txt
#     fi
# done
# rm -f tmp.txt
# # graph
# GPFILE=$SCRIPT_OUTPUT/internalFriction.gp
# cat $SCRIPT_OUTPUT/header.gp > $GPFILE
# cat >> $GPFILE <<EOF
# FILE = "$SCRIPT_OUTPUT/internalFriction.txt"
# set key r b
# a = "(\$1)"
# b = "(\$2)"
# set title 'Internal Friction'
# set xlabel "PARAM"
# set ylabel "u*"
# plot FILE u 1:2:3 w yerrorlines 
# EOF
# gnuplot -persists < $GPFILE



##-------------------------------------
# ENERGY
echo "Processing Kinetic Energy"
GPFILE=$SCRIPT_OUTPUT/EK.gp
cat $SCRIPT_OUTPUT/header.gp > $GPFILE
cat >> $GPFILE <<EOF
FILE = "${FILESOUT}/energy.txt"
set key r t
set title 'Kinetic Energy'
set log y
a = "(\$1)"
b = "(\$3)"
set xlabel "Time"
set ylabel "Kinetic Energy"
plot for [TC in PARAM] BASE.'${PARAM_PREFIX}'.TC.'/'.FILE u @a:@b t '$PARAM_LABEL = '.TC
EOF
gnuplot -persists < $GPFILE

##-------------------------------------
# TRANSLATION PROFILE
echo "Processing Translation profile"
GPFILE=$SCRIPT_OUTPUT/transProfile.gp
cat $SCRIPT_OUTPUT/header.gp > $GPFILE
cat >> $GPFILE <<EOF
FILE = "${FILESOUT}/meanXTranslation.txt"
set key r b
set title 'Mean Translation Profile'
a = "(\$6)"
b = "(\$1)"
c = "(\$7)"
set xlabel "<dxNet>/xWall"
set ylabel "z/zWall"
plot for [TC in PARAM] BASE.'${PARAM_PREFIX}'.TC.'/'.FILE u @a:@b:@c w xerrorlines t '$PARAM_LABEL = '.TC
EOF
gnuplot -persists < $GPFILE

##-------------------------------------
# GRAINS SIZE PROFILE
echo "Processing grain size profile"
GPFILE=$SCRIPT_OUTPUT/grain_size_profile.gp
cat $SCRIPT_OUTPUT/header.gp > $GPFILE
cat >> $GPFILE <<EOF
FILE = "${FILESOUT}/grain_size_profile.txt"
set key r b
set title 'Mean Grain Size Profile'
a = "(\$1)"
b = "(\$6)"
c = "(\$7)"
set xlabel "z/Lz"
set ylabel "<rad/max_rad>"
plot for [TC in PARAM] BASE.'${PARAM_PREFIX}'.TC.'/'.FILE u @a:@b:@c w yerrorlines t '$PARAM_LABEL = '.TC
EOF
gnuplot -persists < $GPFILE

##-------------------------------------
# DISPLACEMENT TO THE SQUARE
echo "Processing square displacement"
GPFILE=$SCRIPT_OUTPUT/square_displacement.gp
cat $SCRIPT_OUTPUT/header.gp > $GPFILE
cat >> $GPFILE <<EOF
FILE = "${FILESOUT}/square_displacement.txt"
set key r b
set title 'Square Displacement'
a = "(\$1)"
b = "(\$2 * \$2)"
set xlabel "time"
set ylabel "(x-x0)**2"
plot for [TC in PARAM] BASE.'${PARAM_PREFIX}'.TC.'/'.FILE u @a:@b t '$PARAM_LABEL = '.TC
EOF
gnuplot -persists < $GPFILE

##-------------------------------------
# DISPLACEMENT minus MEAN DX
echo "Processing displacement AND mean displacement per slab"
GPFILE=$SCRIPT_OUTPUT/square_displacement.gp
cat $SCRIPT_OUTPUT/header.gp > $GPFILE
cat >> $GPFILE <<EOF
FILE = "${FILESOUT}/square_displacement.txt"
set key r b
set title 'Displacement and mean dx '
a = "(\$1)"
b = "(\$2)"
c = "(\$3)"
set xlabel "time"
set ylabel "(x-x0)"
plot for [TC in PARAM] BASE.'${PARAM_PREFIX}'.TC.'/'.FILE u @a:@b t '$PARAM_LABEL = '.TC
set term x11 1
set ylabel "dx_mean"
plot for [TC in PARAM] BASE.'${PARAM_PREFIX}'.TC.'/'.FILE u @a:@c t '$PARAM_LABEL = '.TC
set term x11 2
set ylabel "(x-x0) - dx_mean"
plot for [TC in PARAM] BASE.'${PARAM_PREFIX}'.TC.'/'.FILE u @a:(@b-@c) t '$PARAM_LABEL = '.TC
EOF
gnuplot -persists < $GPFILE
#read

##-------------------------------------
# MOBILITY - BROWNIAN DISPLACEMENT
echo "Processing mobilty-square displacement"
GPFILE=$SCRIPT_OUTPUT/mobility_square_displacement.gp
cat $SCRIPT_OUTPUT/header.gp > $GPFILE
cat >> $GPFILE <<EOF
FILE = "${FILESOUT}/mobility_brownian_displacements.txt"
set key r b
set title 'Mobility'
a = "(\$2)"
b = "(\$3 * \$3)"
set xlabel "(x-x0)/F"
set ylabel "(x-x0)**2"
plot for [TC in PARAM] BASE.'${PARAM_PREFIX}'.TC.'/'.FILE u @a:@b t '$PARAM_LABEL = '.TC
EOF
gnuplot -persists < $GPFILE


##-------------------------------------
# q vs p 
echo "Processing q VS p"
GPFILE=$SCRIPT_OUTPUT/qVSp.gp
cat $SCRIPT_OUTPUT/header.gp > $GPFILE
cat >> $GPFILE <<EOF
FILE = "${FILESOUT}/pq.txt"
set key r b
a = "(\$3)"
b = "(\$4)"
set title 'q VS p'
set xlabel "p"
set ylabel "q"
plot for [TC in PARAM] BASE.'${PARAM_PREFIX}'.TC.'/'.FILE u @a:@b t '$PARAM_LABEL = '.TC
EOF
gnuplot -persists < $GPFILE


##-------------------------------------
# q over p vs gamma
echo "Processing q over p vs gamma"
GPFILE=$SCRIPT_OUTPUT/qpVSgamma.gp
cat $SCRIPT_OUTPUT/header.gp > $GPFILE
cat >> $GPFILE <<EOF
FILE = "${FILESOUT}/pq.txt"
set key r b
a = "(\$2)"
b = "(\$4/\$3)"
set title 'q/p VS gamma'
set xlabel "gamma"
set ylabel "q/p"
plot for [TC in PARAM] BASE.'${PARAM_PREFIX}'.TC.'/'.FILE u @a:@b t '$PARAM_LABEL = '.TC
EOF
gnuplot -persists < $GPFILE

##-------------------------------------
# q vs gamma
echo "Processing q vs gamma"
GPFILE=$SCRIPT_OUTPUT/qVSgamma.gp
cat $SCRIPT_OUTPUT/header.gp > $GPFILE
cat >> $GPFILE <<EOF
FILE = "${FILESOUT}/pq.txt"
set key r b
a = "(\$2)"
b = "(\$4)"
set title 'q VS gamma'
set xlabel "gamma"
set ylabel "q"
plot for [TC in PARAM] BASE.'${PARAM_PREFIX}'.TC.'/'.FILE u @a:@b t '$PARAM_LABEL = '.TC
EOF
gnuplot -persists < $GPFILE

##-------------------------------------
# q over p vs time
echo "Processing q over p vs time"
GPFILE=$SCRIPT_OUTPUT/qpVStime.gp
cat $SCRIPT_OUTPUT/header.gp > $GPFILE
cat >> $GPFILE <<EOF
FILE = "${FILESOUT}/pq.txt"
set key r b
a = "(\$1)"
b = "(\$4/\$3)"
set title 'q/p VS time'
set xlabel "time"
set ylabel "q/p"
plot for [TC in PARAM] BASE.'${PARAM_PREFIX}'.TC.'/'.FILE u @a:@b t '$PARAM_LABEL = '.TC
EOF
gnuplot -persists < $GPFILE

##-------------------------------------
# q vs time
echo "Processing q vs time"
GPFILE=$SCRIPT_OUTPUT/qVStime.gp
cat $SCRIPT_OUTPUT/header.gp > $GPFILE
cat >> $GPFILE <<EOF
FILE = "${FILESOUT}/pq.txt"
set key r b
a = "(\$1)"
b = "(\$4)"
set title 'q VS time'
set xlabel "time"
set ylabel "q"
plot for [TC in PARAM] BASE.'${PARAM_PREFIX}'.TC.'/'.FILE u @a:@b t '$PARAM_LABEL = '.TC
EOF
gnuplot -persists < $GPFILE

##-------------------------------------
# void ratio vs time
echo "Processing Void Ratio"
GPFILE=$SCRIPT_OUTPUT/voidVStime.gp
cat $SCRIPT_OUTPUT/header.gp > $GPFILE
cat >> $GPFILE <<EOF
FILE = "$FILESOUT/voidratio.txt"
set key r b
a = "(\$1)"
b = "(\$2)"
set title 'void ratio VS time'
set xlabel "time"
set ylabel "e"
plot for [TC in PARAM] BASE.'${PARAM_PREFIX}'.TC.'/'.FILE u @a:@b t '$PARAM_LABEL = '.TC
EOF
gnuplot -persists < $GPFILE

##-------------------------------------
# Packing fraction vs time
echo "Processing Packing Fraction"
GPFILE=$SCRIPT_OUTPUT/packingFractionVStime.gp
cat $SCRIPT_OUTPUT/header.gp > $GPFILE
cat >> $GPFILE <<EOF
FILE = "$FILESOUT/voidratio.txt"
set key r b
a = "(\$1)"
b = "(\$3)"
set title 'Packing Fraction VS time'
set xlabel "time"
set ylabel "Packing fraction = Vs/V"
plot for [TC in PARAM] BASE.'${PARAM_PREFIX}'.TC.'/'.FILE u @a:@b t '$PARAM_LABEL = '.TC
EOF
gnuplot -persists < $GPFILE

## -------------------------------------
## HISTOGRAMS 
function plotHisto {
    echo "Processing $2"
    GPFILE=$SCRIPT_OUTPUT/$1
    cat $SCRIPT_OUTPUT/header.gp > $GPFILE
    cat >> $GPFILE <<EOF
FILE = "$2"
set key r b
set log xy
a = "(\$3 > 0 ? (\$1/\$3) : 0)"
#a = "(\$1)"
b = "(\$2)"
set title "$1"
set xlabel "$3" 
set ylabel "$4"
plot [:][1.0e-6:] for [TC in PARAM] BASE.'${PARAM_PREFIX}'.TC.'/'.FILE u @a:@b t '$PARAM_LABEL = '.TC
#plot [:][:] for [TC in PARAM] BASE.'${PARAM_PREFIX}'.TC.'/'.FILE u @a:@b t '$PARAM_LABEL = '.TC
EOF
    gnuplot -persists < $GPFILE
    
}

##-------------------------------------
# PFN
echo "Processing histograms: Fn"
plotHisto PFn.gp $FILESOUT/PFn.txt "Fn/<Fn>" "PDF"
# PFt 
echo "Processing histograms: Ft"
plotHisto PFt.gp $FILESOUT/PFt.txt "Ft/<Ft>" "PDF"
# Torque
echo "Processing histograms: Torque"
plotHisto PT.gp $FILESOUT/PT.txt "T/<T>" "PDF"


# ##------------------------------------------------------------------------------
# ## Bose-Einstein fitting
# echo "Fitting Bose-Einstein for normal forces"
# #GPFILE=$SCRIPT_OUTPUT/bose.gp
# #cat $SCRIPT_OUTPUT/header.gp > $GPFILE
# echo > boseFit.txt
# for ((idx=0; idx<${#P_VALS[*]}; idx++)); do
#     tc=${P_VALS[$idx]}
#     FILE=$FILESOUT/PFn-histo.txt
#     filename=$BASE/${PARAM_PREFIX}$tc/$FILE 
#     GPFILE=$SCRIPT_OUTPUT/bose-tmp.gp
#     cat $SCRIPT_OUTPUT/header.gp > $GPFILE
#     cat >> $GPFILE <<EOF
# f(x, beta, mu, a1, a0) = a0 + a1*x*x/(exp(beta*(0.5*x*x/1.0e6 - mu))-1)
# set fit errorvariables
# beta=30; mu = -0.5; a1 = 0.001; a0 = 0.05;
# fit f(x, beta, mu, a1, a0) "$filename" u (\$1/\$3):2 via beta, mu, a1, a0
# set print "$(PWD)/boseFit-tmp.txt"
# print $tc, beta, beta_err, mu, mu_err, a1, a1_err, a0, a0_err
# plot f(x, beta, mu, a1, a0),  "$filename" w p pt 4
# EOF
# gnuplot -persist < $GPFILE
# cat boseFit-tmp.txt >> boseFit.txt
# done 
# read
