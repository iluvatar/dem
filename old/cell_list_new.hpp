#ifndef __CELL_LIST_NEW__
#define __CELL_LIST_NEW__


#include "utility.hpp"
#include <iostream>
#include <vector>
#include <algorithm>
#include <cmath>
#include <fstream>

//-----------------------------------------------------------------------------
// class CellList
/*
  Goal : to keep the size of the list in the same order of the number of objects
  Operates with head and array, as described by Allen-Tildesley 
  Each object IS REQUIRED to have an internal function x() returning its x position (same for y and z)
 */
//-----------------------------------------------------------------------------

class CellList {
public: // methods
  CellList();
  void start(const int & nbodies, const double lmin[3], const double lmax[3], const double & dmin, const bool periodic[3]);
  template <class T> 
  bool update(std::vector<T> & bodyArray, const double wmin[3], const double wmax[3], const bool & check_limits = true); 
  bool check_status(); 

public: // data
  std::vector<int> head_; // head particle for each cell. Size = ncells
  std::vector<int> list_; // actual list. Size = nbodies
  std::vector<int> body_cell_; // cell id for each body. Size = nbodies
  std::vector<std::vector<int> > next_cell_; // next cell ids for each cell. Size = ncells
  int INVALID_;

private: //methods
  void get_indexes(const int & cellId, int & ix, int & iy, int & iz);
  int get_cell_id(const int & ix, const int & iy, const int & iz);
  void set_neighbor_id(void);
  void update_list(const int & bodyID, const int & oldCellID, const int & newCellID); 
  template <class T> 
  void check_domain_limits_resize(std::vector<T> & bodyArray, 
				  const double wmin[3], const double wmax[3]);


private: //data
  int nbodies_, ncells_; 
  int n_[3]; // number of cells on each dimension
  int nynz_; // optimization, = ny*nz
  double d_, dmin_; // box size, minimun box size Roughly (max diammeter of particles)
  double ud_; // optimization, ud_ = 1.0/d_
  double lmin_[3], lmax_[3]; // limits of the domain
  bool periodic_[3];
};

CellList::CellList()
{
  INVALID_ = -1;
  nbodies_ = nynz_ = ncells_ = 0;
  for (auto & val : n_) val = 0;
  for (auto & val : lmin_) val = 0;
  for (auto & val : lmax_) val = 0;
  d_ = dmin_ = 0;
  for (auto & val : periodic_) val = false;
}

int 
CellList::get_cell_id(const int & ix, const int & iy, const int & iz) {
  /*
    cellID = ix*ny_*nz_ + iy*nz_ + iz
    ix = cellID/(ny_*nz_);
    iy = (cellID/nz_)%ny_;
    iz = cellID%(nz_);
  */
  if(0 <= ix && ix < n_[0] && 0 <= iy && iy < n_[1] && 0 <= iz && iz < n_[2]) 
    return ix*nynz_ + iy*n_[2] + iz;
  else 
    return INVALID_;
}

void 
CellList::get_indexes(const int & cellID, int & ix, int & iy, int & iz){
  ix = cellID/(nynz_);
  iy = (cellID/n_[2])%n_[1];
  iz = cellID%(n_[2]);
}

void 
CellList::start(const int & nbodies, const double lmin[3], const double lmax[3], const double & dmin, const bool periodic[3])
{
  print_log("INIT: Starting cell list");
  INVALID_ = -1;
  ASSERT(nbodies > 0);
  for (int ii = 0; ii < 3; ++ii) ASSERT(lmax[ii] > lmin[ii]);
  ASSERT(dmin > 0);
  nbodies_ = nbodies; 
  print_log("old min : ", lmin_, 3);
  print_log("new min : ", lmin, 3);
  print_log("old max : ", lmax_, 3);
  print_log("new max : ", lmax, 3);
  for (int ii = 0; ii < 3; ++ii) lmin_[ii] = lmin[ii]; 
  for (int ii = 0; ii < 3; ++ii) lmax_[ii] = lmax[ii]; 
  dmin_ = dmin;
  d_ = std::pow((lmax_[0]-lmin_[0])*(lmax_[1]-lmin_[1])*(lmax_[2]-lmin_[2])/nbodies_, 1.0/3.0);
  if (d_ < dmin_) d_ = dmin_; 
  print_log("dmin_ = ", dmin_);
  print_log("d_ = ", d_);
  ASSERT(d_ >= dmin_);  
  for (int ii = 0; ii < 3; ++ii) n_[ii] = std::ceil((lmax_[ii] - lmin_[ii])/d_); 
  print_log("n : ", n_, 3);
  for (auto & val : n_) ASSERT(val >= 1);
  for (int ii = 0; ii < 3; ++ii) if (true == periodic[ii]) ASSERT(n_[ii] >= 3);
  ncells_ = n_[0]*n_[1]*n_[2];
  nynz_ = n_[1]*n_[2];
  ud_ = 1.0/d_;
  print_log("INVALID  = ", INVALID_);
  print_log("nbodies_ = ", nbodies_);
  print_log("ncells_  = ", ncells_);
  head_.resize(ncells_); std::fill(head_.begin(), head_.end(), INVALID_);
  list_.resize(nbodies_); std::fill(list_.begin(), list_.end(), INVALID_);
  body_cell_.resize(nbodies_); std::fill(body_cell_.begin(), body_cell_.end(), INVALID_);
  next_cell_.resize(ncells_); std::for_each(next_cell_.begin(), next_cell_.end(), std::mem_fun_ref(&std::vector<int>::clear));
  for (int ii = 0; ii < 3; ++ii) periodic_[ii] = periodic[ii];
  set_neighbor_id();
  print_log("DONE: Starting cell list");
}

void
CellList::set_neighbor_id(void)
{
  print_log("INIT: Setting neighbor list per cell");
  print_log("periodic_ = ", periodic_, 3);
  // cubic neighborhood
  // z varies first, then y and then x!!!!
  /*
    Let sx, sy and sz be the shifts for the index on each dimension.
    The index for an arbitrary neighbor shifted cell is
    cellIDNEW(sx, sy, sz) = cellID + sx*ny_*nz_ + sy*nz_ + sz
  */
  std::for_each(next_cell_.begin(), next_cell_.end(), std::mem_fun_ref(&std::vector<int>::clear));

  const int dxmin = -1; const int dxmax = 1;
  const int dymin = -1; const int dymax = 1;
  const int dzmin = -1; const int dzmax = 1;
  for(int ix = 0; ix < n_[0]; ++ix) {
    for(int iy = 0; iy < n_[1]; ++iy) {
      for(int iz = 0; iz < n_[2]; ++iz) {
	int cellID = get_cell_id(ix, iy, iz);
	ASSERT(0 <= cellID && cellID < ncells_); 
	for(int Dx = dxmin; Dx <= dxmax; ++Dx) {
	  for(int Dy = dymin; Dy <= dymax; ++Dy) {
	    for(int Dz = dzmin; Dz <= dzmax; ++Dz) {
	      int newix = (ix + Dx + n_[0])%n_[0]; 
	      int newiy = (iy + Dy + n_[1])%n_[1]; 
	      int newiz = (iz + Dz + n_[2])%n_[2]; 
	      int cellIDnext = get_cell_id(newix, newiy, newiz);
	      if(INVALID_ != cellIDnext){
		ASSERT(0 <= cellIDnext && cellIDnext < ncells_); 
		next_cell_[cellID].push_back(cellIDnext); 
		next_cell_[cellIDnext].push_back(cellID); 
	      }
	    }
	  }
	}
      }
    }
  }
  // remove duplicates
  for (auto & vec : next_cell_) {
    std::sort(vec.begin(), vec.end());
    vec.resize(std::unique(vec.begin(), vec.end()) - vec.begin());
  }
  print_log("END: Setting neighbor list per cell");
}

template <class T>
bool CellList::update(std::vector<T> & bodyArray, 
		      const double wmin[3], const double wmax[3], 
		      const bool & check_limits) 
{
  //print_log("Update 1");
  if (true == check_limits) { // this is useful to avoid checking again when called after re-size
    check_domain_limits_resize(bodyArray, wmin, wmax);
  }
  
  bool upd_flag = false;
  
  //print_log("Update 2");
  for(int bodieID = 0; bodieID < nbodies_; ++bodieID) {
    int ix = static_cast<int>((bodyArray[bodieID].x() - lmin_[0])*ud_);
    int iy = static_cast<int>((bodyArray[bodieID].y() - lmin_[1])*ud_);
    int iz = static_cast<int>((bodyArray[bodieID].z() - lmin_[2])*ud_);
    int cellID = ix*nynz_ + iy*n_[2] + iz;
    int oldcellID = body_cell_[bodieID];
    if(cellID < 0 || cellID >= ncells_) {
      print_error("Bad id for particle in cell list.");
      print_error("R = ", bodyArray[bodieID].R);
      print_error("min_ = ", lmin_, 3);
      print_error("max_ = ", lmax_, 3);
      print_error_and_exit("Exiting.");
    }
    else if(oldcellID != cellID) {
      //print_log("HERE , id = ", bodieID);
      //print_log("HERE , oldCellID = ", oldcellID);
      //print_log("HERE , newCellID = ", cellID);
      //std::cin.get();
      upd_flag = true;
      update_list(bodieID, oldcellID, cellID);
    }
  }
  //for (auto val : body_cell_) { 
  //std::cout << val << std::endl;
  //std::cin.get();
  //}
  //print_log("Update 3");

  return upd_flag;
}

void
CellList::update_list(const int & bodyID, const int & oldCellID, const int & newCellID) 
{
  // erase from old cell
  if (INVALID_ != oldCellID) { // this happens just at the beginning when all values are invalid
    const int next_id = list_[bodyID]; 
    if (bodyID == head_[oldCellID]) {
      head_[oldCellID] = list_[bodyID];
    }
    else {
      int prev_id = head_[oldCellID];
      while (list_[prev_id] != bodyID) prev_id = list_[prev_id];
      ASSERT(INVALID_ != prev_id);
      list_[prev_id] = next_id;
    }
    list_[bodyID] = INVALID_;
  }
  // add to new cell
  list_[bodyID] = head_[newCellID];
  head_[newCellID] = bodyID;
  body_cell_[bodyID] = newCellID;
}

template <class T>
void 
CellList::check_domain_limits_resize(std::vector<T> & bodyArray, 
				     const double wmin[3], const double wmax[3]) 
{
  //print_log("Checking domain limits.");
  bool have_to_resize = false;

  // aux vars. WARNING: possible leak?
  double cmin[3] = {lmin_[0], lmin_[1], lmin_[2]}; // current min 
  double cmax[3] = {lmax_[0], lmax_[1], lmax_[2]}; // current max
  
  // check max
  for (int dim = 0; dim <= 2; ++dim) {
    if (cmax[dim] > wmax[dim] + dmin_ ) { cmax[dim] = wmax[dim]; have_to_resize = true; } 
    else while (cmax[dim] < wmax[dim]) { cmax[dim] += 1.0*dmin_; have_to_resize = true; } 
  }
  // check min
  for (int dim = 0; dim <= 2; ++dim) {
    if (cmin[dim] < wmin[dim] - dmin_ ) { cmin[dim] = wmin[dim]; have_to_resize = true; } 
    else while (cmin[dim] > wmin[dim]) { cmin[dim] -= 1.0*dmin_; have_to_resize = true; } 
  }
  
  // resize
  if (true == have_to_resize) {
    print_log("Resizing cell list.");
    start(nbodies_, cmin, cmax, dmin_, periodic_);
    update(bodyArray, cmin, cmax, false); // false = Do not check limits again 
    print_log("DONE: Resizing cell list.");
  }
  //print_log("DONE: Checking domain limits.");
}


bool CellList::check_status(void) 
{
  bool status = true;
  // check body cell id different to invalid
  for(auto it = body_cell_.begin(); it != body_cell_.end(); ++it) {
    if (INVALID_ == *it) { 
      status = false;
      print_error("Body cell id for a body is INVALID! (first error)");
      print_error("ID = ", it - body_cell_.begin());
      print_error_and_exit("Exiting.");
      break;
    } 
  }
  return status;
}




#endif // __CELL_LIST_NEW__
