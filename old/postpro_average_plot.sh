#!/bin/env bash

# check args
if [[ $# != 2 ]]; then
    printf "ERROR.\nUsage: $0 pattern1 subpattern2\n"
    printf "Example: $0 sigmaconf seed \n"
    printf "This means you have severeal sigmaconf_XXXX and inside each one, several seed_XXX \n\n"
    exit 1
fi

mv nohup.out nohup.out.orig 2>/dev/null

stage="tinit_postpro"
printf "\nRunning $stage\n"
nohup parallel -j8 --retries 10 -k --joblog joblog.$stage  'val=$((0 + $RANDOM%8)); sleep 0; cd {}; postpro_tinit_to_frac_output_tend.sh 0.99 ' ::: ${1}*/${2}*/ &
wait $!
status="$?"
echo "status = $status"
mv nohup.out nohup.out.$stage 2>/dev/null


stage="postpro"
printf "\nRunning $stage\n"
nohup parallel -j8 --retries 15 -k --joblog joblog.$stage  'val=$((0 + $RANDOM%8)); val=$(echo $val + 1*1.{#} | bc -l); sleep 0; cd {}; time postpro_main.x $val {} ' ::: ${1}*/${2}*/ &
wait $!
status="$?"
mv nohup.out nohup.out.$stage 2>/dev/null
if [[ "$status" != "0" ]]; then
    printf "ERRORS in postpro : %s errors\n" $status
    printf "Exiting.\n"
    exit $status
fi
echo "status = $status"

stage="average"
printf "\nRunning $stage\n"
nohup parallel -j8 --retries 10 -k --joblog joblog.$stage  'val=$((0 + $RANDOM%5)); sleep 0; time average_postpro_values.py {} ' ::: ${1}*/ &
wait $!
status="$?"
mv nohup.out nohup.out.$stage 2>/dev/null
echo "status = $status"

stage="plot_xmgrace"
printf "\nRunning $stage\n"
nohup parallel -j8 --retries 10 -k --joblog joblog.$stage  'val=$((0 + $RANDOM%5)); sleep 0; time plot_xmgrace.py {}/PARAM-AVERAGED 1 ' ::: ${1}*/ &
wait $!
status="$?"
mv nohup.out nohup.out.$stage 2>/dev/null
echo "status = $status"

printf "DONE.\n"
