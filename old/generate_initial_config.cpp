//--------------------------------------------------------------
// Last Modification: 2010 07 20
//--------------------------------------------------------------
/// testing the new DEM 
#include <cstdlib>
#include <iostream>
#include <string>
#include <iomanip>
#include "../workspace_dem.hh"
#include "../utility.hh"
#include "../histogram1d.hh"
//using namespace std;

//--------------------------------------------------------------
// Functions
//--------------------------------------------------------------
void CheckArguments(int argc, char **argv);

//--------------------------------------------------------------
// Main
//--------------------------------------------------------------
int main (int argc, char **argv)
{
  CheckArguments(argc, argv);
  //------------------------------------------------------------
  // time evolution
  double tf = 200;
  //------------------------------------------------------------
  // parameters
  const double SIGMACONF = strtod(argv[1], NULL);
  const double MU_C = strtod(argv[2], NULL);
  //------------------------------------------------------------
  string filename;
  CWorkspaceDEM dom("config/configFile-shear2D.xml", 0);
  dom.SetPlaneWallMaterialTag(1);
  dom.SetMuc(MU_C);
  dom.Set2DBoxShearPeriodic(SIGMACONF, 0.0, true);
  dom.histogramsFlag = false; // do not accumulate on histograms
  dom.Evolve(400.01, 0.1, /*byEnergy*/false, /*printFlag*/true, argv[1]); // evolve
  dom.SetMuc(0.0); // to reset rotations
  //------------------------------------------------------------
  // save the final state of the bodies
  filename = "outBodiesInfo.txt";
  dom.PrintBodiesInfo(filename);
  //------------------------------------------------------------
  //// save the config file
  //filename = "configShearOut.xml";
  //dom.PrintConfig(filename);

  return 0;
}

//--------------------------------------------------------------
// Functions impementations
//--------------------------------------------------------------
void 
CheckArguments(int argc, char **argv)
{
  if(argc != 3) {
    cerr << "Error. program must be called as " << endl
	 << argv[0] << " SIGMACONF MU_C" << endl
	 << "Exiting" << endl;
    exit(1);
  }
  else {
    clog << "# Running with the following parameters:" << endl
	 << "# sigmaconf = " << argv[1] << endl
	 << "# mu_c = " << argv[2] << endl;
    system("sleep 3");
  }
}






