#!/bin/bash

if [ "" == "$1" ]; then
    echo "ERROR. Usage : $0 dir_name"
    exit 1
fi

echo "This script will uncompress each output file individually and delete the original: filename.tar.bz2 -> filename"
echo "Processing files inside dir $1"

FULL_PATH="$1"
if [ -d "${1}/OUTPUT" ]; then
    FULL_PATH="$1/OUTPUT"
fi

for pattern in bodiesInfo contactsInfo; do
    for a in "${FULL_PATH}/${pattern}"*.txt.tar.bz2; do
        tar xvjf "$a" && rm -f "$a"
    done
done

echo "DONE."

