//--------------------------------------------------------------------------------------------------
// 
//--------------------------------------------------------------------------------------------------
/// testing the new DEM 
#include <cstdlib>
#include <iostream>
#include <string>
#include <iomanip>
#include "../../inc/workspace_dem_util.hh"
//using namespace std;

//--------------------------------------------------------------------------------------------------
// Main
//--------------------------------------------------------------------------------------------------
int main (int argc, char **argv)
{
  // simulation
  DEM::WorkspaceDEM dom;
  DEM::util::create_top_bottom_grain_walls(dom);

  return 0;
}





