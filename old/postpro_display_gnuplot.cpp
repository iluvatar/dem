//--------------------------------------------------------------------------
// Plot for gnuplot
//--------------------------------------------------------------------------

#include "../inc/postpro_display.hpp"

int main()
{
  DEM::PostproDisplay postprocessor;
  postprocessor.draw_gnuplot();

  return 0;
}
