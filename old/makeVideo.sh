#!/bin/bash

if [ "$#" -ne "1" ]; then
    echo "ERROR"
    echo "Usage : $0 DirWithPovAndPngFiles"
    exit 1;
fi

./makePngPovray.sh $1/povrayFiles $1/pngFiles
./makeVideoFromPng.sh   $1/pngFiles




