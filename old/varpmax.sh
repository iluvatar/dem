#!/bin/bash
#for press in 1 2 3 4 5 6 7 8 
#for press in 9 10 11 12 13 14 15 16
#for press in 17 18 19 20 21 22 23 24 
for press in 50 75 100 125 150 175 200 250
do
	mkdir vpmax$press
	cd vpmax$press
	cp ../ttt.inp .
	sed -i s/pp0/$press/g ttt.inp
	../ttt2 ttt > job.log &
	PID$press = $!
	cd ..
done

#for press in 1 2 3 4 5 6 7 8 
#for press in 9 10 11 12 13 14 15 16
#for press in 17 18 19 20 21 22 23 24 
for press in 50 75 100 125 150 175 200 250
do
	wait $(PID$press)
done
