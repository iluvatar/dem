/// testing the new DEM 
#include <cstdlib>
#include <iostream>
#include <string>
#include <iomanip>
#include "../../headers/workspace_dem.hh"
#include "../../headers/numerical_devices.hh"
#include "../../headers/dictionary.hh"
//using namespace std;

//--------------------------------------------------------------------------------------------------
// Main
//--------------------------------------------------------------------------------------------------
int main (int argc, char **argv)
{
  //--------------------------------------------------------------------------------------------------
  // parameters
  double FORCE_CONF, V, FPULL, ID_PULLED;
  int ID_PULLED;
  dictionary::BasicDictionary simulParams; 
  dictionary::read_file_or_die(simulParams, "input/simulParameters_conf");
  FORCE_CONF = dictionary::get_value_or_die(simulParams, "FORCE_CONF");
  V = dictionary::get_value_or_die(simulParams, "V");
  FPULL = dictionary::get_value_or_die(simulParams, "FPULL");
  ID_PULLED = dictionary::get_value_or_die(simulParams, "ID_PULLED");

  //--------------------------------------------------------------------------------------------------
  // simulation
  DEM::WorkspaceDEM dom("input/bodiesInfo.txt", "input/general_conf");
  dom.read_contacts_info("input/contactsInfo.txt");
  clog << "# I = " << dom.inertial_number(FORCE_CONF, V) << endl;
  DEM::NumericalDevices numDevice; numDevice.box_2D_shear_periodic_pull(dom, FORCE_CONF, V, ID_PULLED, FPULL, false);
  dom.start();  
  dom.evolve();
  //--------------------------------------------------------------------------------------------------
  // final printing
  dom.print_bodies_info("output/bodiesInfo.txt");
  dom.print_contacts_info("output/contactsInfo.txt");
  dom.print_config("output/general_conf");

  return 0;
}





