#/bin/bash

cd $(pwd)

for a in ./*.x; do
    echo "Running $a with arguments $@";
    base=$(basename $a .x)
    nohup time $a $@ 2>$base-stderr.txt 1>$base-stdout.txt &
done
