# include definitions
include Makefile.mk

# phony targets
.PHONY: all clean print clean_obj install

# clear suffix list and define new one for .cpp and .o files
.SUFFIXES: 
.SUFFIXES: .cpp .o .x .hpp

# define objects and exe targets
SRC_POSTPRO = $(wildcard $(IDIR)/postpro/*.cpp) 
OBJ_POSTPRO = $(SRC_POSTPRO:.cpp=.o)
HDR_POSTPRO = $(SRC_POSTPRO:.cpp=.hpp)
SRC = $(wildcard $(IDIR)/*.cpp) $(SRC_POSTPRO) 
OBJ = $(SRC:.cpp=.o) $(OBJ_POSTPRO) 
EXE_SRC = $(wildcard $(SDIR)/*.cpp) 
EXE = $(EXE_SRC:.cpp=.x)

# Main target
all: $(EXE)

# implicit rules
%.o : %.cpp %.hpp 
	@echo "\nProcessing : $@"
	$(CXX) -c $(CXXFLAGS) $< -o  $@ 
%.x : %.cpp
	@echo "\nProcessing : $@"
	$(CXX) $(CXXFLAGS) $^ $(LDLIBS) -o $@ 
	cp $@ $(BDIR)/

# explicit dependencies
DEPS_COMMON = $(IDIR)/vector3d.o $(IDIR)/particle.o $(IDIR)/utility.o $(IDIR)/netCDF_io.o $(IDIR)/contact.o $(IDIR)/matrix3d.o $(IDIR)/stress_tensor.o
DEPS_PREPRO = $(IDIR)/prepro.o $(DEPS_COMMON) 
DEPS_DEM = $(DEPS_COMMON) $(IDIR)/workspace_dem.o $(IDIR)/particle_activator.o $(IDIR)/interaction.o $(IDIR)/workspace_dem_helper.o \
	$(IDIR)/workspace_dem_util.o $(IDIR)/time_integration.o $(IDIR)/boundary_fix.o $(IDIR)/materials.o $(IDIR)/cell_list.o \
	$(IDIR)/functions.o $(IDIR)/file_util.o 
DEPS_POSTPRO = $(DEPS_DEM)  $(IDIR)/postpro_base.o $(IDIR)/histogram1d.o $(IDIR)/macrovars.o $(IDIR)/nlfit.o $(IDIR)/matrix3d.o $(OBJ_POSTPRO) 
DEPS_POSTPRO_DISPLAY = $(IDIR)/postpro_display.o $(IDIR)/postpro_base.o $(IDIR)/animator.o $(IDIR)/interaction.o $(DEPS_COMMON)

$(SDIR)/prepro_initializer.x : $(DEPS_PREPRO) 
$(SDIR)/dem.x : $(DEPS_DEM)
$(SDIR)/contacts_from_txt_to_nc.x $(SDIR)/particles_from_txt_to_nc.x: $(IDIR)/netCDF_io.o $(IDIR)/contact.o $(IDIR)/utility.o $(IDIR)/particle.o $(IDIR)/vector3d.o
$(SDIR)/helper_freezer.x $(SDIR)/helper_material_mixing.x $(SDIR)/helper_remove_penetrations_by_scaling.x $(SDIR)/helper_scale_to_input_boxsize.x $(SDIR)/helper_particles_at_random.x : $(DEPS_DEM)
$(SDIR)/postpro_main.x : $(DEPS_POSTPRO) $(IDIR)/postpro_analysis.o
$(SDIR)/postpro_display_povray.x $(SDIR)/postpro_display_voronoi.x $(SDIR)/postpro_display_gnuplot.x: $(DEPS_POSTPRO_DISPLAY) 
$(IDIR)/postpro_analysis.o : $(DEPS_POSTPRO)
$(IDIR)/netCDF_io.o : $(IDIR)/contact.o $(IDIR)/particle.o

# explicit rules
$(SDIR)/postpro_display_paraview.x : $(SDIR)/postpro_display_paraview.cpp $(DEPS_POSTPRO_DISPLAY) 
	@echo "\nProcessing : $@"
	$(CXX) $(CXXFLAGS) $^ $(LDLIBS) $(VTK_FLAGS) -o $@
	cp $@ $(BDIR)/
$(IDIR)/animator.o : $(IDIR)/animator.cpp $(IDIR)/animator.hpp 
	@echo "\nProcessing : $@"
	$(CXX) -c $(CXXFLAGS) $< $(LDLIBS) $(VTK_FLAGS) -o $@


# make clean
clean_exe : 
	-rm -f $(BDIR)/*.x $(SDIR)/*.x
clean_obj : 
	-rm -f *.o $(IDIR)/*.o $(IDIR)/postpro/*.o $(SDIR)/*.o
clean : clean_obj clean_exe
	-rm -f  *~ core $(IDIR)/*~  *.ii *.s *.out


# print some info
print:
	@echo $(NETCDF_FLAGS)
	@echo $(WITH_NETCDF)
	@echo $(WITH_VTK)
	@echo $(VTK_FLAGS)
	@echo $(SRC)
	@echo $(OBJ)

install:
	install -v bin/* $(PREFIX)/bin

# check syntax for flymake and emacs
CHK_SOURCES=$(SDIR)/*.cpp
check-syntax:  cppcheck
	$(CXX) $(CXXFLAGS) -o nul -S ${CHK_SOURCES} $(CFLAGS)
cppcheck:
	cppcheck --enable=all --quiet --template={file}:{line}:{severity}:{message} . ${CHK_SOURCES} #--check-config

# EXAMPLES
#$(ODIR)/dem.o: $(SDIR)/dem.cpp #$(DEPS)
#	$(CXX) -c -o $@ $< $(CXXFLAGS) $(OPT_FLAGS)
#$(BDIR)/dem.x: $(ODIR)/dem.o
#	$(CXX) -o $@ $^ $(CXXFLAGS) $(OPT_FLAGS) $(LIBS)
# DO NOT DELETE
