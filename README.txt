This file explains the structure and usage of this DEM code. Printing to netCDF data files is optional but encouraged!
Actually, the latest changes including residual contact behaviour
avoids reading old txt data. OpenMP is used for the preprocessing
densification and you are encourage to test it.

-------------------------------------------------------------
- STRUCTURE:
This repo is divided into seven sub-directories: old, doc, inc, addons, src, bin, and simul. 

old/ : Contains old files not currently in use but that could be useful some day. The files are very likely  not compatible with the current structure of the repos.

doc/ : Contains a Doxygen file from which a full html or latex documentation can be generated

inc/ : Contains the headers for the simulation codes, actually with both declarations and implementations.

src/ : Where the actual .cpp are. These are the files to be compiled to generate the binaries.

bin/ : Empty unless you compile some src/* file by using the Makefile file. 

addons/ : Several utility scripts.

simul/ : The actual simulations are performed here. After setup, this directory is subdivided into:
simul/DISPLAY : To store the gnuplot, paraview or povray files.
simul/INPUT : Simulation configuration.
simul/OUTPUT : Simulation output
simul/POSTPRO : Simulation postpro  output files
-------------------------------------------------------------

--------------------------------------------------------
External libraries:

NOTE : You can install the external libraries by using the script install_dependencies.sh inside the addons directory.

- eigen  : (Mandatory) For 3d vectors and small matrices, and for non linear fitting (unsupported NonLinearOptimization module)
  	   (http://eigen.tuxfamily.org/index.php?title=Main_Page)
- yaml-cpp : (Mandatory) To write/read configuration files (Ubuntu: Requires boost 1.48, otherwise yaml stuff does not compile).
  	     (https://code.google.com/p/yaml-cpp/)
- netcdf + the c++ bindings (Optional, activated when compiling with WITH_NETCDF=1) : For IO with compressed netcdf files.
  	       (Needs hdf5 : http://www.hdfgroup.org/HDF5/)
  	       (Install first the c library and then the c++ version from : 
  	        http://www.unidata.ucar.edu/downloads/netcdf/index.jsp
		The basic netcdf library must be built with netCDF4 enabled) 
- voro++ : (Postpro)   For postpro Voronoi tessellations. Linked as library (-lvoro++)
  	   (http://math.lbl.gov/voro++/)
- tetgen : (Postpro)   For postpro Delaunay tessellation. (-ltet + tetgen.h on include path and TETLIBRARY defined)
  	   (http://wias-berlin.de/software/tetgen/)
- VTK    : (Postpro, activated when compiling with WITH_VTK=1)   For visualization. Allows to print to compressed binary vtp files.
  	   (http://www.vtk.org/VTK/resources/software.html) NOTE:
	   Since 2018-10-01 the default version is VTK8 and it is
	   working both in linux and MacOsX (Yosemite). Use the addons/install_dependencies.sh script to install VTK8 from source, if
	   you wish, but it will take a lot of time (less when using multiple threads for compiling).
	   For older versions of MacOsX: VTK 6.1.0 on Mavericks (To compile successfully 
	   you need to use ccmake to remove the -fobjc-gc flag from VTK_REQUIRED_OBJCXX_FLAGS, see 
	   http://public.kitware.com/pipermail/vtkusers/2014-March/083368.html). 
	   To compile wth vtk5 on Mavericks, you should use ccmake, toggle advanced 
	   options, and select USE_SYSTEM_TIFF (or similar), then c(onfigure), g(enerate), and cmake and make, and cross your fingers. (See 
	   http://vtk.1045678.n5.nabble.com/Compilation-error-with-Clang-OSX-10-9-td5724091.html)  	 
--------------------------------------------------------

--------------------------------------------------------
NOTE : Memory consumption is moderate, but it is hugely increased when using netcdf for printing (also when linking to VTK)
NOTE : Current Debug options with address sanitizer needs gcc > 4.0.X . To use it and demangle function names, you should export the following var : export ASAN_SYMBOLIZER_PATH=/usr/bin/llvm-symbolizer , and run the needed executable as:  ASAN_OPTIONS=symbolize=1 exec.x   2>  err.txt  1> out.txt
--------------------------------------------------------

--------------------------------------------------------
DOCS : For a primitive documentation you can run doxygen $(repo_path)/doc/Doxyfile which will create a subdir called doc-dem-workspace on the current directory. You can also use the usgae.tex/pdf guide inside the doc directory as an starting points, besides this readme.
--------------------------------------------------------

--------------------------------------------------------
USAGE:
Simulations are performed inside simul sub-directory. Once the binaries (to be compiled into the bin directory by using the Makefile) and the helper scripts (inside addons directory) are placed into standard paths, the simul directory can be copied as a model in any place to run the simulation inside it.

Every simulation reads form INPUT config directory and puts the output into the OUTPUT directory. Therefore, to start a simulation from a previous one, the old OUTPUT particles and contacts files should be copied into the new INPUT (plus some modifications to the config files).

To setup an initial simulation inside an arbitray directory, use the script addons/setup_simul.sh . This will copy default config files.

Every new simulations needs to run three steps: prepro_initializer.x, for preprocessing (sample preparation); dem.x, which is the actual simulation; and postpro_main.x, which is the postprocessing. Further visualization tools are added, like postpro_display_paraview.x, which prints vtk files into the DISPLAY directory to be read by paraview. 

There are several configuration files inside the simuls/INPUT directory. They are going to be read with the yaml-cpp parser. Their meaning is as follows:
* general_conf.yaml: (prepro, simul, postpro) General configuration of the simulation. This informs the dimension, if the walls are periodic, if contact information should be read, the time step size, the number of time steps, how frequent to print, if stop when relaxed (by using the Cundall force criteria), and the gravity constant, etc. 
* materials_conf.yaml: (simul, postpro) Values of the material parameters, tagged. Each body has its own material tag (default value 0). At the beginning, composite material tags are computed, only once. The last key value for each material represents its residual behaviour. By default it is the same as the  initial material. Example: Let's assume there are three materials, with tags 0, 1, 2. The first one is frictionless, the last one is with friction. A material declared as  - [0, 2800, 2.0e5, 0.1, 0.0, 0.0, 2] means that old, already existing contacts are treated with tag 0, but new contacts will be trated with tag 2. Of course, the material with tag 2 should exist. 
* prepro_conf.yaml: (prepro) To build the sample. It defines the initial dimensions, the number of grains, the size distribution, the proportion of sizes, initial velocities (for random distribution), seeds, and the possibility to perform initial geometric densification. 
* postpro_conf.yaml: (postpro) Post-processing parameters, like the screen length, the size of histograms, etc.
* walls_conf.yaml: (simul) walls info. Material tags, and boundary conditions., like driven by velocity or stress

There are some helper scripts in the addons/ directory.
--------------------------------------------------------

-------------------------------------------------------------
To compile: Support for c++11 is mandatory. Minimum compiler version >= 4.8 . Default config is to compile with optimizations.
Just write (for parallel and faster builds, add -j 4 or whatever number of compile threads you want) 
make
If you want another compiler, try something similar to
make CXX=g++-4.9
If you want to profile:
make PROFILE=1
If you want to profile without optimizations:
make PROFILE=1 OPT=
If you want to debug:
make DEBUG=1
If you want to activate netCDF i/o for the whole simulation (prepro, pro, and postpro), try
make CXX=g++-4.9 WITH_NETCDF=1
Please read the Makefile for further info 
--------------------------------------------------------

-------------------------------------------------------------
William Fernando Oquendo Pati\~no
woquendo@gmail.com
