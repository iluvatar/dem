\documentclass[letterpaper,]{article}
\usepackage{graphicx}
\usepackage{booktabs}
\usepackage{graphicx}
\usepackage{fancybox}
\usepackage{hyperref}
\usepackage{amsmath}
\usepackage{framed}
\usepackage[usenames,dvipsnames,svgnames,table]{xcolor}
\definecolor{shadecolor}{rgb}{0.686,0.933,0.933}

%\usepackage[cm]{fullpage}
%\usepackage[letterpaper]{geometry}
\hoffset+0.2cm
\voffset-1.5cm
%\topmargin2.5mm
\setlength{\evensidemargin}{0.0cm}
\setlength{\oddsidemargin}{0.0cm}
\setlength{\marginparwidth}{0.1cm}
\setlength{\textwidth}{16.2cm}
\setlength{\textheight}{22.6cm}
\setlength{\marginparsep}{0.0cm}
%\addtolength{\headwidth}{\marginparsep}
%\addtolength{\headwidth}{\marginparwidth}
%\renewcommand{\chaptermark}[1]{\markboth{#1}{}}
%\renewcommand{\sectionmark}[1]{\markright{\thesection\ #1}}

\renewcommand{\rmdefault}{cmss}
\newcommand{\cmd}[1]{\begin{shaded} \texttt{#1} \end{shaded} }
\newcommand{\lab}[1]{\fcolorbox{white}{Dandelion!25}{\texttt{#1}}}

\title{Usage of the code \emph{dem}}
\author{W.F. Oquendo\thanks{woquendo@gmail.com}}
\date{\today}


\begin{document}

\maketitle

\begin{abstract}
  The Readme file at the root of the repository is the basic guide for
  setting up and using the code. This document presents a more detailed
  version on how to use and install the code.
\end{abstract}

\tableofcontents


\section{Usage after installation}
In the following it is assumed that the user has created a directory
called \verb+simul+ where it will run an exemplary simulation.

\subsection{Setting the environment}

\textbf{NOTE: Now the compilation includes the}
\verb+-Wl,-rpath,$HOME/local/lib+ \textbf{argument so you don't need to
  use the following.} Try running first the executable.

If you installed the dependencies on non-standard paths, like
\verb+$HOME/local+, you need to setup your environment to tell it where
to find the the libraries. To do so, you two options:
\begin{itemize}
\item Doing it every time : You need to run the command
  \verb+export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$HOME/local/lib+
\item To do it only one time : Add the path \verb+$HOME/local/lib+ to a
  new file inside \verb+/etc/ld.so.conf.d/+ or to the the file
  \verb+/etc/ld.so.conf+ .  
\end{itemize}

\subsection{Creating a default simulation setup}
After entering the empty directory simul, the user could use the
script from the repository which is at \cmd{addons/setup\_simul.sh} to
create the needed subdirs and copy the default config files into the
\verb+INPUT/+ subdir. If the user have already installed the
repository and both the executables and scripts are on the user
\texttt{PATH}, a simple run as
\cmd{setup\_simul.sh}
will do the trick.


\subsection{Editing config files inside INPUT/ directory}
Inside the \verb+INPUT/+ you will find several configuration files for
your simulations. The files are \verb+yaml+ type files and will be
parsed by the \verb+yaml-cpp+ parser. The user is invited to read
every config file to get familiar with it. The files' descriptions are as
follows: 
\begin{description}
\item[\lab{general\_conf.yaml}] This is the general configuration
  file for the simulation and includes settings like the time step,
  the number of iterations, termination conditions, etc.
\item[\lab{prepro\_conf.yaml}] Configuration for the preprocessing
  step which includes the number of particles, the distribution of
  them in boxes along the three axes, the radii span, some flags to
  activate special methods (like geometric densification), etc.
\item[\lab{postpro\_conf.yaml}] Configuration for the postprocessing
  stage, which includes minimum and maximum time window, the minimum
  number of contacts for a particle to be processed, the distance from
  the walls to ignore, etc.
\item[\lab{materials\_conf.yaml}] Materials configuration including
  stiffness, sliding and rolling friction, restitution coefficients,
  etc.
\item[\lab{walls\_conf.yaml}] Walls configuration. First, material
  tags. Second, boundary/kinematic conditions to allow to control
  either position, velocity, or force, for each direction. Also, a
  simple linear ramp is included to allow for a softer increase of the
  magnitudes imposed. 
\end{description}

\subsection{Running the  preprocessor}
The preprocessor allows to create the initial sample for the
simulation. After editing the preprocessing configuration file, you
can run the command \cmd{prepro\_initializer.x} which will creates two
files, \verb+OUTPUT/bodiesInfo.nc+ and \verb+OUTPUT/contactsInfo.nc+
inside the \verb+OUTPUT/+ directory. If you want to use those files
for your simulation, you should move or copy them to the \verb+INPUT/+
directory. There is an script useful for that,
\cmd{addons/copy\_final\_to\_initial.sh} \textbf{NOTE:} If the
executables where compiled without netcdf support, the files will be
pure text file with extension \verb+.txt+ . In the following it is
assumed that netcdf support was included.

Once you have created initial startup files, you can edit them further
by using python+netcdf. For example, to particularly edit \verb+alive+
flag for some particles to use the activator, or to change the
material for only the 100-th particlem and so on. 

\subsection{Running dem (processing)}
Once you have created the initial sample, you can perform the
simulation. Do not forget to edit the related config files, like
\texttt{INPUT/general\_conf.yaml} and/or
\texttt{INPUT/walls\_conf.yaml}. To start running the simulation, just
use the command 
\cmd{dem.x}  
If you want to store the output on given files, you can redirect as usual
\cmd{dem.x 1> out-dem.txt 2> err-dem.txt}
The output bodies and contacts files will be written inside the \verb+OUPUT/+ directory
with a time stamp, like \texttt{OUTPUT/bodiesInfo-0000.0001234.nc} and
\texttt{OUTPUT/contactsInfo-0000.0001234.nc}. 

If you want to stop the simulation before the required ending time
(maybe because it is not equilibrating or it is too slow and you want
to tune some parameters, etc) you can edit again the file
\texttt{INPUT/general\_conf.yaml} and assign 0 to the setting
\texttt{KEEP\_RUNNING}. There are some other settings which can also
bet changed during the execution, while others cannot.

\subsection{Running postpro}
During or after the processing stage, you can post-process the current
output data. The postprocessing stage is very rich but also
specifically tunned for the previous needs of the programming. If you
need to implement a new postprocessing function, you can write
yourself in c++ or python and also ask for a pull request into the
main repository.

\subsubsection{Main postprocessing}
This is done by the command \cmd{postpro\_main.x} and will write
everything on the \verb+POSTPRO/+ directory. There are many functions
extracting different kinds of information and the user is encouraged
to read the source code to understand what every routine is computing
and printing. 

From the text files written, you perform several analysis, and also
plot the needed figures.

\subsubsection{Paraview visualization}
If the user wants to visualize the system, he can use the command
\cmd{postpro\_display\_paraview.x}
which will print several \texttt{vtk} and \texttt{pvd} files into the
\texttt{DISPLAY/} directory. Those files allows for visualization
using the \texttt{paraview} tool. If compilation with vtk was
activated by using the flag \texttt{WITH\_VTK=1}, the vtk files will be
compressed, otherwise they could take a lot of hard disk space. 
To visualize the time sequence, you can open, in paraview:
\begin{description}
\item[\lab{DISPLAY/data.pvd}] to visualize the particles.
\item[\lab{DISPLAY/planes\_data.pvd} ]to visualize the walls.
\item[\lab{DISPLAY/constacts\_data.pvd}] to visualize the contacts.
\end{description}
After opening any of those time series (or a individual vtk file), you
will be able to color by several utility variable, apply thresholds,
etc. Inside the \texttt{addons} directory of the repo, there is a
paraview plugin to rapidly apply an spherical glyph on the system. It
is called \texttt{GlyphSphereFilter.cpd}.


\subsubsection{Some helper scripts}
The directory \texttt{addons} of the source code repository includes
several helper scripts. Some of them will be described below:
\begin{description}
\item[\lab{install\_dependencies.sh}]  Install the dependencies needed to compile the source code.
\item[\lab{clean\_data\_simul.sh}]  Removes all the \texttt{OUTPUT,
  POSTPRO, DISPLAY} data leaving \texttt{INPUT} untouched.
\item[\lab{average\_postpro\_values.py}]  Let's assume you have
  performed several simulations of the same system for, let.s say,
  different initial conditions, and that each simul is stored on dirs
  \texttt{dir01, dir02, \ldots}. After you have run postpro on all the
  simul dirs, you can use this script to average all the output data
  (actually any subdirectory) extracting averages and error bars. 
\item[\lab{duplicate\_simul.sh}] Allows to duplicate a simulation
  (\texttt{INPUT}) into a new directory.
\item[\lab{extract\_data\_vs\_param.sh}] Assuming that you have run
  simulations for different values of a given parameter, and they are
  on subdirs \texttt{param\_01, param\_02, \ldots}, you can use this
  script to extract some pre-defined (inside the script) variables as
  function of the values of the parameters.  
\item[\lab{plot\_xmgrace.py}]  Allows to create some default  plots
  of the postpro data, using xmgrace. Files are put inside the
  new directory \texttt{XMGRACE\_PLOTS} directory.   
\item[\lab{postpro\_tinit\_to\_frac\_output\_tend.sh}] Allows to
  modify the initial and final time to some values which are fractions
  of the the actual total time. Fr example, if the initial and final
  times are 0.0 and 2.345, and one runs this script with arguments
  0.1, and 0.9, it will choose as min time 0.2345 and max time 2.1105.    
\item[\lab{setup\_continuing\_simul.sh}]  Allows to continue an
  already stopped simulation. 
\end{description}

\section{Installation}
All the source code is publicly available at
\url{bitbucket.org/iluvatar/dem} .

\subsection{Repository cloning or download }
You can either download or clone the repository into a given local
directory. It is strongly advisable to clone the repository to be able
to update it as soon as new commits are made. To clone the repo,
simply run the following command inside a given empty directory
\cmd{git clone https://bitbucket.org/iluvatar/dem.git} \ \ . Of
course, you need to have git installed. 

\subsection{Compilation}
Once you have cloned the repository, you need to compile the
executables. To do that, a Makefile is provided. First, enter the repo
directory just cloned. Second, several prerequisites should be
fulfilled: you need to have installed the eigen c++ library, yaml-cpp,
netcdf and its c++ support if you want compressed and portable output,
voro++ for voronoi postprocessing, tetgen for delaunay tessellations,
and vtk. Furthermore, the compiler (g++) should support
c++11. Versions larger or equal to 4.8.x are encouraged since they
also support the memory sanitizer for debugging purposes.

\subsubsection{Installing dependencies}
Dependencies are better installed from the source code. Please, do not
use the package manager (apt-get, brew) to install them, since the
default version/flags are not always the best.

You can use the script \texttt{addons/install\_dependencies.sh} to
install the dependencies. Please update the values of the
corresponding variables, like the \texttt{PREFIX} or compiler
version. You should also check that all installations were successful.

\subsubsection{Actual compilation}
Once you have fulfilled the pre-requisites, just run
\cmd{make CXX=g++-4.9 OPT=1 WITH\_VTK=1 WITH\_NETCDF=1 -j 4}
where you have to replace \texttt{g++-4.9} by your actual compiler
name. After successful compilation, all the binaries will be put
inside the \texttt{bin/} directory. 

Other examples for compilation :
\begin{description}
\item[\lab{make CXX=g++-4.9 OPT=1 WITH\_NETCDF=1 -j 4}] Do not use
  VTK for paraview printing (larger files).
\item [\lab{make CXX=g++-4.9 DEBUG=1 WITH\_NETCDF=1 -j 4}] Do not
  optimize. Compile with debug activated. 
\item [\lab{make CXX=g++-4.9 OPT=1 WITH\_NETCDF=1 -j 8}] Compile with
  eight threads.
\end{description}
 
\subsection{Setting executables to default paths}
You can either copy the executables into the simulation directory, or
better add the directories \texttt{bin/} and \texttt{addons/} to your
path. For this, you can, for example, add the following lines to your
\texttt{.bashrc} or \texttt{.bash\_profile} and restart your terminal:
\begin{verbatim}
export PATH="$PATH:/path/to/the/repository/bin"
export PATH="$PATH:/path/to/the/repository/addons"
\end{verbatim}
where \texttt{/path/to/the/repository/} is the abosulte path to the
cloned repository. After restarting the terminal, you will be able to
use directly the commands inside the \texttt{bin/} and
\texttt{addons/} directories.  


\subsection{Updating or recompiling: git pull, git status}
If a change is uploaded to the repository, and you have cloned it (as
you should), you can go to the repo and issue the command 
\cmd{git pull}
to get the newest changes. After that, run again the make command and
you will have updated versions of your local executables.


\end{document}
